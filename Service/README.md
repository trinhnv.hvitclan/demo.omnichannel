# selfhost

.NET Core 2.1 self-hosting Kestrel Console App

[![](https://raw.githubusercontent.com/ServiceStack/Assets/master/csharp-templates/selfhost.png)](http://selfhost.web-templates.io/)

> Browse [source code](https://github.com/NetCoreTemplates/selfhost), view live demo [selfhost.web-templates.io](http://selfhost.web-templates.io) and install with [dotnet-new](http://docs.servicestack.net/dotnet-new):

    $ dotnet tool install -g web

    $ web new selfhost ProjectName

# windows
Thêm đoạn này vào <PropertyGroup> ở file *.csproj
<RuntimeIdentifier>win10-x64</RuntimeIdentifier>
<UseAppHost>true</UseAppHost>
<SelfContained>false</SelfContained>
<IsTransformWebConfigDisabled>true</IsTransformWebConfigDisabled>

## 1 Build Db
Vào thư mục của project Demo.omnichannel.Api và chạy lệnh lần lượt:
dotnet tool install --global dotnet-ef
dotnet ef migrations add InitialCreate --context EfDbContext --output-dir Migrations
dotnet ef migrations add WareHouseTable --context EfDbContext --output-dir Migrations
 
 dotnet ef database update --context EfDbContext
 
## 2 Build image Demo.omnichannel.Api
Vào thư mục Services
docker build -f "./Api/Demo.omnichannel.Api/Dockerfile" -t Demo-repo.kvpos.com:4434/kvdev/Demo.omnichannel:v1.0.0-tikidev .
docker push Demo-repo.kvpos.com:4434/kvdev/Demo.omnichannel:v1.0.0-tikidev

## 3 Build image Demo.omnichannel.MappingProduct
Vào thư mục Services
docker build -f "./src/ProcessServices/Demo.omnichannel.MappingProduct/Dockerfile" -t Demo-repo.kvpos.com:4434/kvdev/Demo.omnichannel.mappingproduct:v1.0.0-tikidev .
docker push Demo-repo.kvpos.com:4434/kvdev/Demo.omnichannel.mappingproduct:v1.0.0-tikidev

## 4 Build image Demo.omnichannel.ProductSubscribe
Vào thư mục Services
docker build -f "./src/ProcessServices/Demo.omnichannel.ProductSubscribe/Dockerfile" -t Demo-repo.kvpos.com:4434/kvdev/Demo.omnichannel.productsubscribe:v1.0.0-tikidev .
docker push Demo-repo.kvpos.com:4434/kvdev/Demo.omnichannel.productsubscribe:v1.0.0-tikidev

## 5 Build image Demo.omnichannel.OnHandService
Vào thư mục Services
docker build -f "./src/ProcessServices/Demo.omnichannel.OnHandService/Dockerfile" -t Demo-repo.kvpos.com:4434/kvdev/Demo.omnichannel.onhandservice:v1.0.0-tikidev .
docker push Demo-repo.kvpos.com:4434/kvdev/Demo.omnichannel.onhandservice:v1.0.0-tikidev  

## 6 Build image Demo.omnichannel.PriceService
Vào thư mục Services
docker build -f "./src/ProcessServices/Demo.omnichannel.PriceService/Dockerfile" -t Demo-repo.kvpos.com:4434/kvdev/Demo.omnichannel.priceservice:v1.0.0-tikidev .
docker push Demo-repo.kvpos.com:4434/kvdev/Demo.omnichannel.priceservice:v1.0.0-tikidev

## 7 Build image Demo.omnichannel.ScheduleService
Vào thư mục Services
docker build -f "./src/ScheduleServices/Demo.omnichannel.Schedule/Dockerfile" -t Demo-repo.kvpos.com:4434/kvdev/Demo.omnichannel.scheduleservice:v1.0.0-tikidev .
docker push Demo-repo.kvpos.com:4434/kvdev/Demo.omnichannel.scheduleservice:v1.0.0-tikidev

## 8 Build image Demo.omnichannel.OrderService
Vào thư mục Services
docker build -f "./src/ProcessServices/Demo.omnichannel.OrderService/Dockerfile" -t Demo-repo.kvpos.com:4434/kvdev/Demo.omnichannel.orderservice:v1.0.0-tikidev .
docker push Demo-repo.kvpos.com:4434/kvdev/Demo.omnichannel.orderservice:v1.0.0-tikidev

## 9 Build image Demo.omnichannel.ComparingService
Vào thư mục Services
docker build -f "./src/ProcessServices/Demo.Omnichannel.ComparingService/Dockerfile" -t comparingservice:v1.0.0-tikidev .
docker push Demo-repo.kvpos.com:4434/kvdev/Demo.omnichannel.orderservice:v1.0.0-tikidev
﻿using Demo.OmniChannel.ShareKernel.Common;
using Newtonsoft.Json;
using System;

namespace Demo.OmniChannel.Utilities
{
    public class ConvertHelper
    {
        public static bool ToBoolean(object obj)
        {
            bool retVal;

            try
            {
                retVal = Convert.ToBoolean(obj);
            }
            catch
            {
                retVal = false;
            }

            return retVal;
        }

        public static int ToInt32(object obj)
        {
            int retVal;

            try
            {
                retVal = Convert.ToInt32(obj);
            }
            catch
            {
                retVal = 0;
            }

            return retVal;
        }

        public static long ToInt64(object obj)
        {
            long retVal;

            try
            {
                retVal = Convert.ToInt64(obj);
            }
            catch
            {
                retVal = 0;
            }

            return retVal;
        }

        public static byte ToByte(object obj)
        {
            byte retVal;

            try
            {
                retVal = Convert.ToByte(obj);
            }
            catch
            {
                retVal = 0;
            }

            return retVal;
        }

        public static bool CheckUseStringItemId(byte? channelType)
        {
            switch (channelType)
            {
                case (byte)ChannelType.Sendo:
                    return true;
                default:
                    return false;
            }
        }

        public static T DeepCopy<T>(T self)
        {
            var serialized = JsonConvert.SerializeObject(self);
            return JsonConvert.DeserializeObject<T>(serialized);
        }

        public static decimal? GetAproximadamenteDecimal(decimal? input, int number)
        {
            if (input == null)
            {
                return null;
            }
            return Math.Round(input.Value, number);
        }

        public static string GetUseParrentItemId(byte? channelType, string parentChannelProductId,
            string ProductChannelId)
        {
            switch (channelType)
            {
                case (byte)ChannelType.Tiktok:
                    return ProductChannelId;
                default:
                    return ProductChannelId;
            }
        }
    }

}
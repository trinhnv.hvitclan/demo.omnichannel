﻿namespace Demo.OmniChannel.Utilities
{
    public class Kafka
    {
        public string KafkaIp { get; set; }
        public string TrackingKafkaIp { get; set; }
        public string GroupId { get; set; }
        public string ClientId { get; set; }
        public int RetryTime { get; set; }
        public string Prefix { get; set; }
        public bool IsLogHandler { get; set; }
        public bool IsAutoCommit { get; set; }
        public Topic[] AllTopics { get; set; }
    }

    public class Topic
    {
        public string Name { get; set; }
        public string ServiceName { get; set; }
        public bool IsActive { get; set; }
        public byte Type { get; set; }
        public int Size { get; set; }
        public int ChannelType { get; set; }
    }
}
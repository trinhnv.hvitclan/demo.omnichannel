﻿

namespace Demo.OmniChannel.Utilities.RabbitMq
{
    public class RabbitMqConfig
    {
        public string HostName { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int RequestedHeartbeat { get; set; }
        public string VirtualHost { get; set; }
        public string QueueName { get; set; }
        public string Exchange { get; set; }
        public string ExchangeType { get; set; }
        public string RoutingKey { get; set; }

    }
}

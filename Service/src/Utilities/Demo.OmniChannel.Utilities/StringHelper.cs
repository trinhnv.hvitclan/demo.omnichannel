﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Demo.OmniChannel.Utilities
{
    public class StringHelper
    {
        public static string Normallize(double number)
        {
            var result = number.ToString("#,##0.###", new CultureInfo("en-us"));
            return string.IsNullOrEmpty(result) ? "0" : result;
        }
        public static string NormallizeWfp(double number)
        {
            var result = number.ToString("#,###", new CultureInfo("en-us"));
            return string.IsNullOrEmpty(result) ? "0" : result;
        }
        public static string ConvertToUnsign(string str)
        {
            string[] signs = new string[] {
                "aAeEoOuUiIdDyY",
                "áàạảãâấầậẩẫăắằặẳẵ",
                "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
                "éèẹẻẽêếềệểễ",
                "ÉÈẸẺẼÊẾỀỆỂỄ",
                "óòọỏõôốồộổỗơớờợởỡ",
                "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
                "úùụủũưứừựửữ",
                "ÚÙỤỦŨƯỨỪỰỬỮ",
                "íìịỉĩ",
                "ÍÌỊỈĨ",
                "đ",
                "ĐÐ",
                "ýỳỵỷỹ",
                "ÝỲỴỶỸ"
            };
            for (int i = 1; i < signs.Length; i++)
            {
                for (int j = 0; j < signs[i].Length; j++)
                {
                    str = str.Replace(signs[i][j], signs[0][i - 1]);
                }
            }
            return str;
        }

        public static string ReplaceHexadecimalSymbols(string txt)
        {
            var r = new Regex("[^\u0009\u000a\u000d\u0020-\ud7ff\ue000-\ufffd]|([\ud800-\udbff\ufeff](?![\udc00-\udfff]))|((?<![\ud800-\udbff])[\udc00-\udfff])");
            return string.IsNullOrEmpty(txt) ? string.Empty : r.Replace(txt, "");
        }

        public static string SubStringWithLength(string str, int maxLength)
        {
            if (string.IsNullOrEmpty(str)) return str;
            if (str.Length > maxLength) return str.Substring(0, maxLength).Trim();
            return str;
        }

        public static string GetDisplayName<T>()
        {
            var displayName = typeof(T)
              .GetCustomAttributes(typeof(DisplayNameAttribute), true)
              .FirstOrDefault() as DisplayNameAttribute;

            if (displayName != null)
                return displayName.DisplayName;

            return "";
        }

        public static string GenerateUUID()
        {
            return "WN" + DateTime.Now.GetTimestamp() + new Random().Next().ToString();
        }
    }
}
﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.Utilities;

namespace Demo.OmniChannel.Utilities
{
    public static class CryptoHelper
    {
        public static string PassPhrase { get; set; }

        private const string SaltValue = "nrFbMe4a";        // can be any string
        private const string HashAlgorithm = "SHA1";    // can be "MD5" if need
        private const int PasswordIterations = 2;       // can be any number
        private const string InitVector = "N75UJeuvnDfAarGt"; // must be 16 bytes
        private const int KeySize = 256;                // can be 192 or 128

        public static string RijndaelEncrypt(string plainText)
        {
            if (string.IsNullOrWhiteSpace(PassPhrase))
            {
                throw new KvException("PassPhrase can not be null.");
            }

            return RijndaelEncryption.Encrypt(plainText, PassPhrase, SaltValue, HashAlgorithm, PasswordIterations, InitVector, KeySize);
        }

        public static string RijndaelDecrypt(string cipherText)
        {
            if (string.IsNullOrWhiteSpace(PassPhrase))
            {
                throw new KvException("PassPhrase can not be null.");
            }

            return RijndaelEncryption.Decrypt(cipherText, PassPhrase, SaltValue, HashAlgorithm, PasswordIterations, InitVector, KeySize);
        }

        public static byte[] GetBytes(string str)
        {
            return Encoding.ASCII.GetBytes(str);
        }

        public static string EncryptString(string plainText)
        {
            return Convert.ToBase64String(EncryptStringToBytes(plainText, GetBytes(PassPhrase), GetBytes(InitVector)));
        }

        public static string DecryptString(string encrypted)
        {
            return DecryptStringFromBytes(Convert.FromBase64String(encrypted), GetBytes(PassPhrase),
                GetBytes(InitVector));
        }

        public static byte[] EncryptStringToBytes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;
            // Create an Rijndael object 
            // with the specified key and IV. 
            using (var rijAlg = Rijndael.Create())
            {
                rijAlg.Key = Key;
                rijAlg.IV = IV;
                rijAlg.Padding = PaddingMode.ISO10126;
                // Create a decrytor to perform the stream transform.
                var encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption. 
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }

                        encrypted = msEncrypt.ToArray();
                    }
                }
            }


            // Return the encrypted bytes from the memory stream. 
            return encrypted;
        }

        public static string DecryptStringFromBytes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Declare the string used to hold 
            // the decrypted text. 
            string plaintext = null;

            // Create an Rijndael object 
            // with the specified key and IV. 
            using (var rijAlg = Rijndael.Create())
            {
                rijAlg.Key = Key;
                rijAlg.IV = IV;
                rijAlg.Padding = PaddingMode.ISO10126;

                // Create a decrytor to perform the stream transform.
                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for decryption. 
                using (var msDecrypt = new MemoryStream(cipherText))
                {
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var srDecrypt = new StreamReader(csDecrypt))
                        {
                            // Read the decrypted bytes from the decrypting stream 
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }

            return plaintext;
        }

        public static string EncryptPassword(string pass)
        {
            // Hash calculation for compatibility with existing KV database
            // Irrelevant functions should't use this method
            using (SHA1 sha = new SHA1CryptoServiceProvider())
            {
                var data = sha.ComputeHash(Encoding.UTF8.GetBytes(pass));
                var builder = new StringBuilder();
                foreach (var b in data)
                    builder.Append(b.ToString("x2"));
                return builder.ToString().ToUpperInvariant();
            }
        }

        public static string GenerateToken()
        {
            var newId = Guid.NewGuid().ToString();
            var key = string.Format(newId);
            return EncodeToken(key);
        }

        private static string EncodeToken(string key)
        {
            var code = EncryptPassword(key);
            return code;
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Demo.OmniChannel.Utilities
{
    public static class SystemHelper
    {
        /// <summary>
        /// Trong thời gian maintance không thực hiện jobs liên quan database
        /// </summary>
        public static bool IsValidMaintenanceTime(MaintenanceTimeConfig mtSetting)
        {
            if (mtSetting.IsEnable == false) return true;

            var currentTime = DateTime.Now.TimeOfDay;

            if ((mtSetting.Start <= mtSetting.End && currentTime >= mtSetting.Start && currentTime <= mtSetting.End) ||
                (mtSetting.Start > mtSetting.End && (currentTime >= mtSetting.Start || currentTime <= mtSetting.End)))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// GenerateSecureHash
        /// </summary>
        /// <param name="key"> ~ RetailerId</param>
        /// <param name="secretKey"> secretKey </param>
        /// <param name="unixTimestamp"></param>
        /// <returns></returns>
        public static string GenerateSecureHash(int key, string secretKey,long unixTimestamp)
        {
            var secretKeyInBytes = Encoding.UTF8.GetBytes(secretKey);
            var bytesToSign = Encoding.UTF8.GetBytes($"{key}.{unixTimestamp}");
            using (var sha = new HMACSHA256(secretKeyInBytes))
            {
                var calcSignatureBytes = sha.ComputeHash(bytesToSign);
                var signature = ByteToString(calcSignatureBytes);
                return signature;
            }
        }

        /// <summary>
        /// Lấy thông tin enum timeType
        /// </summary>
        /// <param name="timeType"></param>
        /// <returns></returns>
        public static string GetWarrantyTimeTypeText(int timeType)
        {
            switch (timeType)
            {
                case 1:
                    return "Ngày";
                case 2:
                    return "Tháng";
                case 3:
                    return "Năm";
                default:
                    return timeType.ToString() ?? "";
            }
        }


        #region Private method
        private static string ByteToString(byte[] buff)
        {
            string sbinary = "";

            for (int i = 0; i < buff.Length; i++)
            {
                sbinary += buff[i].ToString("X2"); // hex format
            }
            return (sbinary);
        }
        #endregion
    }
}

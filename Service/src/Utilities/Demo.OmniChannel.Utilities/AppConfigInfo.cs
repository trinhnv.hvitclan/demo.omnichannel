﻿
using System;

namespace Demo.OmniChannel.Utilities
{
    public class MaintenanceTimeConfig
    {
        public bool IsEnable { get; set; }
        public TimeSpan Start { get; set; }
        public TimeSpan End { get; set; }
    }
}
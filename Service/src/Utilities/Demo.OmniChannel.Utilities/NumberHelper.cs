﻿using System;
using System.Globalization;
using Demo.OmniChannel.ShareKernel.Common;

namespace Demo.OmniChannel.Utilities
{
    public class NumberHelper
    {
        public static readonly byte ProductPriceDecPlace = 2;
        public static readonly byte TotalPriceDecPlace = 0;
        public static decimal KvConstTolerance = (decimal)0.000001;
        public static double KvConstToleranceDouble = 0.000001;

        public static decimal? RoundProductPrice(decimal? price)
        {
            return RoundPrice(price, ProductPriceDecPlace);
        }

        public static decimal? RoundTotalPrice(decimal? price)
        {
            return RoundPrice(price, TotalPriceDecPlace);
        }

        private static decimal? RoundPrice(decimal? price, int numDecPlace)
        {
            if (!price.HasValue || price % 1 == 0) return price;
            return Math.Round(price.Value + (KvConstTolerance * (price.Value > 0 ? 1 : -1)), numDecPlace, MidpointRounding.AwayFromZero);
        }
        public static decimal RoundPriceByDec(decimal price)
        {
            if (price % 1 == 0) return price;
            return Math.Round(price + (KvConstTolerance * (price > 0 ? 1 : -1)), ProductPriceDecPlace, MidpointRounding.AwayFromZero);
        }

        public static double RoundQuantity(double quantity)
        {
            return RoundQuantity(quantity, TotalPriceDecPlace);
        }

        private static double RoundQuantity(double quantity, int numDecPlace)
        {
            return Math.Round(quantity, numDecPlace, MidpointRounding.AwayFromZero);
        }


        public static string NormallizePrice(double number)
        {
            var pFormat = Math.Abs(Math.Round(number, NumberHelper.ProductPriceDecPlace, MidpointRounding.AwayFromZero) % 1) < KvConstant.Tolerance ? "#,##0" : "#,##0.#0";
            var result = number.ToString(pFormat, new CultureInfo("en-us"));
            return string.IsNullOrEmpty(result) ? "0" : result;
        }

        public static int GetValueOrDefault(int? value, int? defaultValue)
        {
            if (value.HasValue && value > 0) return value.Value;
            return defaultValue ?? 0;
        }

        public static double GetValueOrDefault(double? value, double? defaultValue)
        {
            if (value.HasValue && value > 0) return value.Value;
            return defaultValue ?? 0;
        }

        public static long ParseStringToLong(string itemId, long defaultValue = 0)
        {
            try
            {
                return long.Parse(itemId);
            }
            catch
            {
                return defaultValue;
            }
        }
        public static decimal? CalDifValue(decimal? a, decimal? b)
        {
            if (a.HasValue && b.HasValue)
            {
                return a.Value - b.Value;
            }
            return null;
        }

    }
}
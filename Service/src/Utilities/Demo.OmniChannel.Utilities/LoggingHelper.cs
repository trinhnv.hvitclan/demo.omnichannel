﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Logging;

namespace Demo.OmniChannel.Utilities
{
    public static class LoggingHelper
    {
        public static void WriteLogDataReceived(IAppSettings settings, ILog log, string prefixMessage, int retailerId, object data)
        {
            var isEnable = settings?.Get<bool>("WriteLogDataReceived:IsEnable");

            if (isEnable != true) return;

            var traceLogRetailers = settings?.Get<List<int>>("WriteLogDataReceived:RetailerIds");

            if (traceLogRetailers == null || !traceLogRetailers.Any() || traceLogRetailers.Any(r => r == retailerId))
            {
                log.Info($"DataReceived {prefixMessage} RetailerId_{retailerId}. Data: {data?.ToJson()}");
            }
        }

        public static void WriteLogDataReceived(ILogger log, object data)
        {
            log.LogInformation($"DataReceived: {data?.ToJson()}");
        }
    }
}

﻿using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using PhoneNumbers;

namespace Demo.OmniChannel.Utilities
{
    public class PhoneNumberHelper
    {
        /// <summary>
        /// Get Perfect ContactNumber
        /// </summary>
        /// <param name="inputNumber"></param>
        /// <returns>return string remove space and -, default is string.empty</returns>
        public static string GetPerfectContactNumber(string inputNumber)
        {
            if (string.IsNullOrWhiteSpace(inputNumber))
                return string.Empty;

            if (inputNumber.StartsWith("84"))
            {
                inputNumber = inputNumber.Substring(2);
            }

            inputNumber = inputNumber.TrimStart('0');
            inputNumber = $"0{inputNumber}";

            var regexPattern = @"\s|\-";
            inputNumber = inputNumber.Normalize().Trim();
            inputNumber = Regex.Replace(inputNumber, regexPattern, string.Empty, RegexOptions.IgnoreCase);
            return inputNumber;
        }
        public static string GetHashNumber(string inputNumber)
        {
            if (string.IsNullOrWhiteSpace(inputNumber)) return null;
            if (inputNumber.Contains("*")) return null;
            var planText = GetPlainTextNumber(inputNumber);
            if (string.IsNullOrWhiteSpace(planText)) return null;

            var bytes = Encoding.Unicode.GetBytes(planText);
            var sha1 = new SHA1CryptoServiceProvider();
            var hashBytes = sha1.ComputeHash(bytes);

            var hashNumber = HexStringFromBytes(hashBytes);
            return hashNumber;
        }

        public static string GetPlainTextNumber(string inputNumber)
        {
            if (string.IsNullOrWhiteSpace(inputNumber)) return null;
            var regexPattern = @"\s|\-|\.|~|@|#|\$|\%|\&|\*|\(|\)|\,|!|\^|_|\+|=|\\|\/|\||\>|<|\[|\]|\`|\{|\}|\;|\:|\'|""|\?";
            inputNumber = inputNumber.ToLower().Normalize().Trim();
            inputNumber = Regex.Replace(inputNumber, regexPattern, string.Empty, RegexOptions.IgnoreCase);

            return inputNumber.ToLower();
        }
        public static string HexStringFromBytes(byte[] bytes)
        {
            var sb = new StringBuilder();
            foreach (byte b in bytes)

            { var hex = b.ToString("x2"); sb.Append(hex); }

            return sb.ToString().ToUpperInvariant();
        }

        public static string GetHashContactNumber(string inputNumber)
        {
            if (string.IsNullOrWhiteSpace(inputNumber)) return null;
            var planText = GetPlainTextNumber(inputNumber);
            if (string.IsNullOrWhiteSpace(planText)) return null;

            var bytes = Encoding.Unicode.GetBytes(planText);
            var sha1 = new SHA1CryptoServiceProvider();
            var hashBytes = sha1.ComputeHash(bytes);

            var hashNumber = HexStringFromBytes(hashBytes);
            return hashNumber;
        }

        public static string GetLastestHashContactNumber(string inputNumber)
        {
            var planText = GetPlainTextNumber(inputNumber);
            if (string.IsNullOrWhiteSpace(planText)) return null;
            var fourLastText = planText.Length > 4 ? planText.Substring(planText.Length - 4) : planText;
            return GetHashContactNumber(fourLastText);
        }

        public static bool IsValidContactNumber(string inputNumber, string regionCode)
        {
            if (string.IsNullOrWhiteSpace(inputNumber)) return false;

            PhoneNumberUtil phoneUtil = PhoneNumberUtil.GetInstance();
            try
            {
                phoneUtil.Parse(inputNumber, regionCode);
            }
            catch
            {
                return false;
            }

            return phoneUtil.IsValidNumber(phoneUtil.Parse(inputNumber, regionCode));
        }
    }
}
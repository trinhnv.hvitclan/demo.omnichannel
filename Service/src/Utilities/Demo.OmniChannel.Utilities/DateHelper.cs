﻿using System;
using System.Collections.Generic;

namespace Demo.OmniChannel.Utilities
{
    public static class DateHelper
    {
        /// <summary>
        /// Compare date to minute
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public static bool AlmostEquals(DateTime date1, DateTime date)
        {
            var dateParse1 = new DateTime(date1.Year, date1.Month, date1.Day, date1.Hour, date1.Minute, 0);
            var dateParse2 = new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, 0);
            var compareDate = DateTime.Compare(dateParse1, dateParse2);
            if (compareDate != 0) return false;
            return true;
        }

        public static string GetTimestamp(this DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssfff");
        }

        public static IEnumerable<Tuple<DateTime, DateTime>> SplitDateRange(DateTime start, DateTime end, int dayChunkSize)
        {
            DateTime chunkEnd;
            while ((chunkEnd = start.AddDays(dayChunkSize)) < end)
            {
                yield return Tuple.Create(start, chunkEnd);
                start = chunkEnd;
            }
            yield return Tuple.Create(start, end);
        }


        public static DateTime UnixTimeStampToDateTime(long unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dateTime = dateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dateTime;
        }

        public static bool CompareQuantity(int quantityPurchased, double quantity)
        {
            return !(quantityPurchased > quantity || quantityPurchased < quantity);
        }
        public static string TrimIfNotNull(string value)
        {
            if (value != null)
            {
                return value.Trim();
            }
            return null;
        }

        public static double DateTimeToUnixTimestamp(DateTime dateTime)
        {
            return (TimeZoneInfo.ConvertTimeToUtc(dateTime) -
                   new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
        }
    }
}

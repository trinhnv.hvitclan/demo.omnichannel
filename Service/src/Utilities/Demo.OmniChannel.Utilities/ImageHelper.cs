﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace Demo.OmniChannel.Utilities
{
    public static class ImageHelper
    {
        public static Stream GetThumbnailImage(Stream stream, int maxWidth, int maxHeight, long maxSize, out string contentType, bool IsUsingOriginalImage = false)
        {
            contentType = null;
            try
            {
                var image = Image.FromStream(stream);
                if (
                    !(image.RawFormat.Equals(ImageFormat.Jpeg) || image.RawFormat.Equals(ImageFormat.Gif) ||
                      image.RawFormat.Equals(ImageFormat.Png) || image.RawFormat.Equals(ImageFormat.Bmp)))
                    return null;

                var encoder = ImageCodecInfo.GetImageEncoders().SingleOrDefault(c => c.FormatID == image.RawFormat.Guid);
                if (encoder != null)
                {
                    contentType = encoder.MimeType;
                    var encoderParams = new EncoderParameters(1)
                    {
                        Param = { [0] = new EncoderParameter(Encoder.Quality, 98L) }
                    };

                    // Only allow to upload original image when this shop is allowed and the image is less than maxsize.
                    if (IsUsingOriginalImage && stream.Length <= maxSize)
                    {
                        return RotateOrFlip(stream, image, encoder, encoderParams);
                    }
                    else
                    {
                        #region Resize (if needed) and Compress image with encoderParams and PixelFormat.Format24bppRgb
                        return ResizeAndCompress(maxWidth, maxHeight, image, encoder, encoderParams);
                        #endregion
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }

            // If there are any exceptions then return NULL
            return null;
        }

        private static Stream RotateOrFlip(Stream originalStream, Image image, ImageCodecInfo encoder, EncoderParameters encoderParams)
        {
            #region Check to Rotate or Flip image
            foreach (var prop in image.PropertyItems)
            {
                if (prop.Id == 0x0112 && image.GetPropertyItem(prop.Id).Value.Length > 0) // value of EXIF
                {
                    int orientationValue = image.GetPropertyItem(prop.Id).Value[0];
                    var rotateFlipType = GetOrientationToFlipType(orientationValue);
                    if (rotateFlipType != RotateFlipType.RotateNoneFlipNone)
                    {
                        image.RotateFlip(rotateFlipType);
                        var ms = new MemoryStream();
                        image.Save(ms, encoder, encoderParams);
                        return ms;
                    }
                    break;
                }
            }
            #endregion

            // Return original image 
            return originalStream;
        }

        private static Stream ResizeAndCompress(int maxWidth, int maxHeight, Image image, ImageCodecInfo encoder, EncoderParameters encoderParams)
        {
            var width = image.Width;
            var height = image.Height;

            if (width > maxWidth)
            {
                height = (int)Math.Round(height * maxWidth / (double)width);
                width = maxWidth;
            }

            if (height > maxHeight)
            {
                width = (int)Math.Round(width * maxHeight / (double)height);
                height = maxHeight;
            }

            #region Check to Rotate or Flip image
            foreach (var prop in image.PropertyItems)
            {
                if (prop.Id == 0x0112 && image.GetPropertyItem(prop.Id).Value.Length > 0) // value of EXIF
                {
                    int orientationValue = image.GetPropertyItem(prop.Id).Value[0];
                    var rotateFlipType = GetOrientationToFlipType(orientationValue);
                    image.RotateFlip(rotateFlipType);
                    break;
                }
            }
            #endregion

            var bitmap = new Bitmap(width, height, PixelFormat.Format24bppRgb);
            bitmap.MakeTransparent();
            using (var graphics = Graphics.FromImage(bitmap))
            {
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.DrawImage(image, 0, 0, width, height);
                var ms = new MemoryStream();
                bitmap.Save(ms, encoder, encoderParams);
                return ms;
            }
        }

        private static RotateFlipType GetOrientationToFlipType(int orientationValue)
        {
            RotateFlipType rotateFlipType;

            switch (orientationValue)
            {
                case 1:
                    rotateFlipType = RotateFlipType.RotateNoneFlipNone;
                    break;
                case 2:
                    rotateFlipType = RotateFlipType.RotateNoneFlipX;
                    break;
                case 3:
                    rotateFlipType = RotateFlipType.Rotate180FlipNone;
                    break;
                case 4:
                    rotateFlipType = RotateFlipType.Rotate180FlipX;
                    break;
                case 5:
                    rotateFlipType = RotateFlipType.Rotate90FlipX;
                    break;
                case 6:
                    rotateFlipType = RotateFlipType.Rotate90FlipNone;
                    break;
                case 7:
                    rotateFlipType = RotateFlipType.Rotate270FlipX;
                    break;
                case 8:
                    rotateFlipType = RotateFlipType.Rotate270FlipNone;
                    break;
                default:
                {
                    rotateFlipType = RotateFlipType.RotateNoneFlipNone;
                    break;
                }
            }

            return rotateFlipType;
        }
    }
}

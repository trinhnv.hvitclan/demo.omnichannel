﻿using System;
using System.Collections.Generic;
using System.Linq;
using Demo.OmniChannel.ShareKernel.Exceptions;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Logging;
using ServiceStack.Web;

namespace Demo.OmniChannel.Utilities
{
    public static class ExceptionHelper
    {
        /// <summary>
        /// Write log exception message queue
        /// </summary>
        public static void WriteLogExceptionMq(Type mqType, object mqBody, Exception exception)
        {
            var logManager = LogManager.GetLogger(mqType);
            logManager.Error($"{nameof(KvMqException)} error: {exception.Message} - MqType: {mqType?.Name} - MqBody: {mqBody?.ToJson()}", new KvMqException(exception.Message, exception));
        }

        public static void WriteErrorApiHandler(IRequest httpReq, Exception exception)
        {
            var logManager = LogManager.GetLogger(httpReq.Dto.GetType());
            logManager.Error($"Api handler error Url: {httpReq.AbsoluteUri} - Message: {exception.Message} - Dto: {httpReq.Dto?.ToJson()}", new KvException(exception.Message, exception));
        }

        public static void TestException()
        {
            var a = 0;
            var b = 1 / a;
        }

        public static void HardCodeThrowException(IAppSettings appSettings, int retailerIdRequest)
        {
            var retailerIds = appSettings.Get<List<int>>("HardCodeTest:Exception:RetailerIds");

            if (retailerIds?.Any(x => x == retailerIdRequest) == true) throw new Exception($"HardCodeThrowException by setting retailer ids: {retailerIds.Join(",")}");
        }

        public static void HandleExceptions(IAppSettings settings, Exception ex, IRequest httpReq)
        {
            var onRestart = settings.Get("IsRestartError258", true);

            if (!onRestart) return;

            var logManager = LogManager.GetLogger(httpReq.Dto.GetType());

            if (ex.InnerException != null
                && ex.InnerException.Message.Contains("Unknown error 258"))
            {
                logManager.Warn($"Error SqlExceptions, message {ex.Message}," +
               $"InnerExcetpion {ex.InnerException?.Message}");
                Environment.Exit(1);
            }
        }
    }
}

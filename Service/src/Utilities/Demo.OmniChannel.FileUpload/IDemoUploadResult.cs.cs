﻿namespace Demo.OmniChannel.FileUpload
{
    public class DemoUploadResult
    {
        public string Url { get; set; }
        public string Error { get; set; }
        public UploadResultStatuses Status { get; set; }
    }
    public enum UploadResultStatuses
    {
        Success,
        Error
    }
}
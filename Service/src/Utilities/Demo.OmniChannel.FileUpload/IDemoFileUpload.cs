﻿using System.IO;
using System.Threading.Tasks;

namespace Demo.OmniChannel.FileUpload
{
    public interface IDemoFileUpload
    {
        Task<DemoUploadResult> UploadAvatarAsync(string fileName, MemoryStream stream, string contentType);
    }
}
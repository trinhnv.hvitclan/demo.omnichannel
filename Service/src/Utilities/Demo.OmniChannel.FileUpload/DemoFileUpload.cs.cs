﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Demo.OmniChannel.FileUpload
{
    public abstract class DemoFileUpload : IDemoFileUpload, IDisposable //NOSONAR
    {
        protected DemoFileUpload() { }

        public abstract Task<DemoUploadResult> UploadAvatarAsync(string fileName, MemoryStream stream, string contentType);
        public abstract void Dispose();
    }
}
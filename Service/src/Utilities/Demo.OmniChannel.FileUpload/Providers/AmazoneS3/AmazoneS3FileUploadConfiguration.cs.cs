﻿namespace Demo.OmniChannel.FileUpload.Providers.AmazoneS3
{
    public class AmazoneS3FileUploadConfiguration
    {
        public string AwsAccessKeyId { get; set; }
        public string AwsSecretAccessKey { get; set; }
        public string AwsCacheControlHeader { get; set; }
        public string AwsBucketName { get; set; }
        public string AwsCloudFrontUrl { get; set; }
        public int MaxAvatarSize { get; set; }
        public int MaxAvatarWidth { get; set; }
        public int MaxAvatarHeight { get; set; }
    }
}
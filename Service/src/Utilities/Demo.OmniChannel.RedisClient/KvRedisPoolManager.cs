﻿using System;
using System.Collections.Generic;
using ServiceStack.Logging;
using ServiceStack.Redis;

namespace KiotViet.OmmiChannel.RedisClient
{
    public class KvRedisPoolManager : IKvRedisPoolManager
    {
        private readonly List<IRedisClientsManager> _pools;
        static readonly ILog Log = LogManager.GetLogger(typeof(KvRedisPoolManager));
        public List<IRedisClientsManager> GetPools()
        {
            try
            {
                if (config == null) return null;
                var maxpoolsize = config.MaxPoolSize > 0 ? config.MaxPoolSize : 40;
                var pooltimeout = config.MaxPoolTimeout > 0 ? config.MaxPoolTimeout : 10;
                if (config.IsSentinel)
                {
                    IEnumerable<string> servers = config.Servers.Split(';');

                    var sentinel = new RedisSentinel(servers, config.SentinelMasterName)
                    {
                        RedisManagerFactory = (master, slaves) => new RedisManagerPool(master, new RedisPoolConfig { MaxPoolSize = maxpoolsize })
                    };
                    if (config.IsStrictPool)
                    {
                        sentinel.RedisManagerFactory = (master, slaves) =>
                            new PooledRedisClientManager(master, master, null, config.DbNumber, maxpoolsize,
                                null);
                    }
                    sentinel.OnFailover += x => Log.Error($"Redis fail over to {sentinel.GetMaster()}");
                    sentinel.HostFilter = host => $"{config.AuthPass}@{host}?db={config.DbNumber}&RetryTimeout=100";
                    var redisClient = sentinel.Start();
                    return redisClient;
                }
                var redisConnection = $"{config.AuthPass}@{config.Servers}";
                return new PooledRedisClientManager(new[] { redisConnection }, new[] { redisConnection }, null, config.DbNumber, maxpoolsize, pooltimeout);

            }
            catch (Exception e)
            {
                Log.Error(e.Message, e);
            }

            return null;
        }
    }

    public interface IKvRedisPoolManager
    {
        List<IRedisClientsManager> GetPools();
        RedisClientsManager GetClientsManager(KvRedisConfig config)
    }
}
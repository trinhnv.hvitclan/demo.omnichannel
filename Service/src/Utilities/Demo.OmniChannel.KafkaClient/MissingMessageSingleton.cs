﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Newtonsoft.Json;
using ServiceStack.Logging;

namespace Demo.OmniChannel.KafkaClient
{
    public sealed class MissingMessageSingleton
    {
        #region constructor

        public static MissingMessageSingleton Instance => Nested.instance;

        private static class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested()
            {
            }

            // ReSharper disable once InconsistentNaming
            internal static readonly MissingMessageSingleton instance = new MissingMessageSingleton();
        }

        public MissingMessageSingleton()
        {
            _messages = new BlockingCollection<KafkaMessage>(
                new ConcurrentQueue<KafkaMessage>());
            _logger = LogManager.GetLogger(GetType());
        }

        private readonly BlockingCollection<KafkaMessage> _messages;
        private readonly ILog _logger;

        #endregion

        public void PublishMessage(KafkaMessage msg)
        {
            try
            {
                _messages.Add(msg);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        public void LoopDequeue()
        {
            var cts = new CancellationTokenSource();
            _ = Task.Factory.StartNew(
                async () =>
                {
                    while (true)
                    {
                        cts.Token.ThrowIfCancellationRequested();
                        try
                        {
                            while (_messages.TryTake(out var item))
                            {
                                if (item == null) continue;
                                KafkaClient.Instance.PublishMessage(item.TopicName, item.Key, JsonConvert.SerializeObject(item), isV2: true);
                                await Task.Delay(1000, cts.Token);
                            }
                            await Task.Delay(300000, cts.Token);
                        }
                        catch (TaskCanceledException ex)
                        {
                            _logger.Error(ex);
                        }
                    }
                }, cts.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default).Unwrap();
        }
    }
}

﻿using System;
using Confluent.Kafka;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Serilog;

namespace Demo.OmniChannel.KafkaClient
{
    public sealed class KafkaClient
    {
        #region Constructor

        public static KafkaClient Instance => Nested.instance;
        private static class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested()
            {
            }
            // ReSharper disable once InconsistentNaming
            internal static readonly KafkaClient instance = new KafkaClient();
        }

        public KafkaClient()
        {
        }

        // build config
        private ConsumerConfig _consumerConfig;
        private ProducerConfig _producerConfig;
        private ProducerConfig _producerConfigV2;
        // config kafka from app.json
        private Kafka _kafka;
        private Kafka _kafkaV2;
        private IProducer<string, string> _producer;
        private IProducer<string, string> _producerV2;
        #endregion

        public void SetKafkaProducerConfig(Kafka kafka, bool isV2 = false)
        {
            if (isV2)
            {
                _kafkaV2 = kafka;
                _producerConfigV2 = new ProducerConfig
                {
                    BootstrapServers = kafka.KafkaIp,
                    MessageMaxBytes = 104857600
                };
            }
            else
            {
                _kafka = kafka;
                _producerConfig = new ProducerConfig
                {
                    BootstrapServers = kafka.KafkaIp,
                    MessageMaxBytes = 104857600
                };
            }
        }

        public Kafka GetKafka(bool isV2 = false)
        {
            return isV2 ? _kafkaV2 : _kafka;
        }

        public void SetKafkaConsumerConfig(Kafka kafka)
        {
            _kafka = kafka;
            _consumerConfig = new ConsumerConfig
            {
                BootstrapServers = !string.IsNullOrEmpty(kafka.TrackingKafkaIp) ? kafka.TrackingKafkaIp : kafka.KafkaIp,
                GroupId = kafka.GroupId,
                ClientId = kafka.ClientId,
                EnableAutoCommit = kafka.IsAutoCommit,
                StatisticsIntervalMs = 5000,
                SessionTimeoutMs = 6000,
                // The auto.offset.reset config kicks in ONLY if your consumer group does not have a valid offset committed somewhere
                // earliest: automatically reset the offset to the earliest offset
                // latest: automatically reset the offset to the latest offset
                // Latest should be the best for Tracking
                AutoOffsetReset = AutoOffsetReset.Latest,
                FetchMaxBytes = 104857600,
            };
        }

        public void InitProducer(bool isV2 = false)
        {
            if (isV2)
            {
                var producerBuilder = new ProducerBuilder<string, string>(_producerConfigV2);
                // set error handle
                producerBuilder.SetErrorHandler((_, e) =>
                {
                    Log.Logger.Error($"Kafka producer error: {e}", e);
                });
                _producerV2 = new ProducerBuilder<string, string>(_producerConfigV2).Build();
            }
            else
            {
                var producerBuilder = new ProducerBuilder<string, string>(_producerConfig);
                // set error handle
                producerBuilder.SetErrorHandler((_, e) =>
                {
                    Log.Logger.Error($"Kafka producer error: {e}", e);
                });
                // set log handle

                // build producer
                _producer = new ProducerBuilder<string, string>(_producerConfig).Build();
            }
        }

        public void PublishMessage(string topic, string key, string jsonData, bool isLog = true, bool isV2 = false)
        {
            if (isV2)
            {
                Log.Logger.Information($"MappingBusiness: Kafka producer will send to {topic} with key: {key} and data: {jsonData}");
                var t = _producerV2.ProduceAsync(topic, new Message<string, string> { Key = key, Value = jsonData });
                t.ContinueWith(task => {
                    if (task.IsFaulted)
                    {
                        // write to blocking collection -> have a thread to dequeue infinitely
                        MissingMessageSingleton.Instance.PublishMessage(new KafkaMessage(topic, key, jsonData));
                        // log
                        Log.Logger.Error($"Kafka produce error: {task.Exception?.InnerException?.Message}, {task.Exception?.StackTrace} {jsonData}. Will retry later");
                    }
                    else
                    {
                        if (isLog)
                        {
                            Log.Logger.Information($"Kafka producer send ok to {topic} with key: {key} and data: {jsonData}");
                        }
                    }
                });
            }
            else
            {
                try
                {
                    // async producer
                    // sync will kill performance
                    // using result callback instead of sync to wait result
                    void Handler(DeliveryReport<string, string> dr)
                    {
                        if (!isLog) return;
                        var msg = dr.Message.Value;
                        if (dr.Error.Code == ErrorCode.NoError)
                        {
                            // do something when success if you need
                        }
                        else
                        {
                            // do something when failed if you need
                        }
                    }
                    _producer.Produce(topic, new Message<string, string> { Key = Guid.NewGuid().ToString(), Value = jsonData }, Handler);
                }
                catch (Exception ex)
                {
                    Log.Logger.Error($"Kafka producer error: {ex}");
                }
            }
        }

        public ConsumerConfig GetKafkaConsumerConfig()
        {
            return _consumerConfig;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Microsoft.Extensions.Configuration;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Schedule.Services
{
    public class GetProductBranchService : BaseService
    {
        private ILog _logger => LogManager.GetLogger(typeof(GetProductBranchService));

        public GetProductBranchService(IConfiguration configuration,
            IMessageService messageService,
            IDbConnectionFactory dbConnectionFactory,
            KvRedisConfig mRedisConfig,
            ICacheClient cacheClient) : base(configuration, cacheClient, messageService, dbConnectionFactory)
        {
            var threadSize = mRedisConfig?.EventMessages.FirstOrDefault(x => !string.IsNullOrEmpty(x.Name) &&
                                                                              x.Name.Contains(this.GetType()
                                                                                  .Name))?.ThreadSize ?? 10;
            messageService.RegisterHandler<GetProductBranchByChannelMessage>(m =>
            {
                try
                {
                    m.Options = (int)MessageOption.None;
                    var task = Task.Run(async () => await ProcessMessage(m.GetBody()));
                    task.Wait();
                    return Task.CompletedTask;
                }
                catch (Exception e)
                {
                    ExceptionHelper.WriteLogExceptionMq(typeof(GetProductBranchByChannelMessage), m.Body, e);
                    return string.Empty;
                }
            }, threadSize);
        }

        private async Task ProcessMessage(GetProductBranchByChannelMessage message)
        {
            var getGetProductBranchServiceLog = new LogObject(_logger, message.LogId)
            {
                Action =
                    $"{nameof(GetProductBranchService)}",
                OmniChannelId = message.ChannelId,
                RetailerId = message.RetailerId
            };
            if (message.RetailerId <= 0 || message.ChannelId <= 0)
            {
                return;
            }

            var kvRetailer = await GetRetailer(message.RetailerId);
            if (kvRetailer == null || !kvRetailer.IsActive)
            {
                getGetProductBranchServiceLog.Description = "Empty Retailer or Retailer inactive";
                getGetProductBranchServiceLog.LogInfo();
                return;
            }

            var kvGroup = await GetRetailerGroup(kvRetailer.GroupId);
            if (kvGroup == null)
            {
                return;
            }

            var connectStringName = kvGroup.ConnectionString.Substring(kvGroup.ConnectionString.IndexOf('=') + 1);
            var range = Configuration?.GetValue<int>("GetProductBranchRange", 20) ?? 20;
            using (var db =
                DbConnectionFactory.OpenDbConnection(connectStringName.ToLower())
            )
            {
                var productRepository = new ProductRepository(db);
                var productBranch =
                    await productRepository.GetProductBranchByRange(message.RetailerId, message.BranchId, range);
                if (productBranch == null || !productBranch.Any())
                {
                    return;
                }
                var pageSize = GetSyncMultiPageSize(message.ChannelType);
                var totalRecords = productBranch.Count;
                var total = productBranch.Count / pageSize;
                total = totalRecords % pageSize == 0 ? total : total + 1;
                for (int i = 0; i < total; i++)
                {
                    var currentPage = productBranch.Skip(i * pageSize).Take(pageSize).ToList();
                    if (!currentPage.Any())
                    {
                        continue;
                    }
                    PublishMessageQueue(message, currentPage, connectStringName, message.LogId);
                }
                getGetProductBranchServiceLog.TotalItem = totalRecords;
                getGetProductBranchServiceLog.LogInfo(true);
            }
        }

        private void PublishMessageQueue(GetProductBranchByChannelMessage message, List<ProductBranch> currentPage,
            string connectStringName, Guid logId)
        {
            using (var mq = MessageService.MessageFactory.CreateMessageProducer())
            {
                switch (message.ChannelType)
                {
                    case (byte)ChannelType.Shopee:
                        {
                            mq.Publish(new ShopeeCurrentProductBranchMessage
                            {
                                ChannelId = message.ChannelId,
                                ChannelName = message.ChannelName,
                                ChannelType = message.ChannelType,
                                RetailerId = message.RetailerId,
                                BranchId = message.BranchId,
                                BasePriceBookId = message.BasePriceBookId,
                                PriceBookId = message.PriceBookId,
                                ConnectStringName = connectStringName,
                                SyncOnHandFormula = message.SyncOnHandFormula,
                                Products = currentPage.Select(p => new ProductBranchDto
                                {
                                    KvProductId = p.ProductId,
                                    BranchId = message.BranchId,
                                    Revision = p.Revision
                                }).ToList(),
                                LogId = logId
                            });
                            break;
                        }

                    case (byte)ChannelType.Lazada:
                        {
                            mq.Publish(new LazadaCurrentProductBranchMessage
                            {
                                ChannelId = message.ChannelId,
                                ChannelName = message.ChannelName,
                                ChannelType = message.ChannelType,
                                RetailerId = message.RetailerId,
                                BranchId = message.BranchId,
                                BasePriceBookId = message.BasePriceBookId,
                                PriceBookId = message.PriceBookId,
                                ConnectStringName = connectStringName,
                                SyncOnHandFormula = message.SyncOnHandFormula,
                                Products = currentPage.Select(p => new ProductBranchDto
                                {
                                    KvProductId = p.ProductId,
                                    BranchId = message.BranchId,
                                    Revision = p.Revision
                                }).ToList(),
                                LogId = logId
                            });
                            break;
                        }

                    case (byte)ChannelType.Tiki:
                        {
                            mq.Publish(new TikiCurrentProductBranchMessage
                            {
                                ChannelId = message.ChannelId,
                                ChannelName = message.ChannelName,
                                ChannelType = message.ChannelType,
                                RetailerId = message.RetailerId,
                                BranchId = message.BranchId,
                                BasePriceBookId = message.BasePriceBookId,
                                PriceBookId = message.PriceBookId,
                                ConnectStringName = connectStringName,
                                SyncOnHandFormula = message.SyncOnHandFormula,
                                Products = currentPage.Select(p => new ProductBranchDto
                                {
                                    KvProductId = p.ProductId,
                                    BranchId = message.BranchId,
                                    Revision = p.Revision
                                }).ToList(),
                                LogId = logId
                            });
                            break;
                        }
                    case (byte)ChannelType.Sendo:
                    {
                        mq.Publish(new SendoCurrentProductBranchMessage
                        {
                            ChannelId = message.ChannelId,
                            ChannelName = message.ChannelName,
                            ChannelType = message.ChannelType,
                            RetailerId = message.RetailerId,
                            BranchId = message.BranchId,
                            BasePriceBookId = message.BasePriceBookId,
                            PriceBookId = message.PriceBookId,
                            ConnectStringName = connectStringName,
                            SyncOnHandFormula = message.SyncOnHandFormula,
                            Products = currentPage.Select(p => new ProductBranchDto
                            {
                                KvProductId = p.ProductId,
                                BranchId = message.BranchId,
                                Revision = p.Revision
                            }).ToList(),
                            LogId = logId
                        });
                        break;
                    }
                    case (byte)ChannelType.Tiktok:
                        {
                            mq.Publish(new TiktokCurrentProductBranchMessage
                            {
                                ChannelId = message.ChannelId,
                                ChannelName = message.ChannelName,
                                ChannelType = message.ChannelType,
                                RetailerId = message.RetailerId,
                                BranchId = message.BranchId,
                                BasePriceBookId = message.BasePriceBookId,
                                PriceBookId = message.PriceBookId,
                                ConnectStringName = connectStringName,
                                SyncOnHandFormula = message.SyncOnHandFormula,
                                Products = currentPage.Select(p => new ProductBranchDto
                                {
                                    KvProductId = p.ProductId,
                                    BranchId = message.BranchId,
                                    Revision = p.Revision
                                }).ToList(),
                                LogId = logId
                            });
                            break;
                        }
                }
            }
        }
    }
}
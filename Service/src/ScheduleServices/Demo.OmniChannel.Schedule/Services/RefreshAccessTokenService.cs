﻿using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Impls;
using Demo.OmniChannel.Sdk.RequestDtos;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Microsoft.Extensions.Configuration;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using System;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.Services.Interfaces;

namespace Demo.OmniChannel.Schedule.Services
{
    public class RefreshAccessTokenService : BaseService
    {
        private readonly ChannelClient.Impls.ChannelClient _channelClient;
        private readonly IAppSettings _settings;
        private readonly IOmniChannelPlatformService _omniChannelPlatformService;


        public RefreshAccessTokenService(IConfiguration configuration,
            IMessageService messageService,
            IDbConnectionFactory dbConnectionFactory,
            KvRedisConfig mRedisConfig,
            ICacheClient cacheClient,
            ChannelClient.Impls.ChannelClient channelClient,
            IOmniChannelPlatformService omniChannelPlatformService,
            IAppSettings settings) : base(configuration, cacheClient, messageService, dbConnectionFactory)
        {
            var threadSize = mRedisConfig?.EventMessages.FirstOrDefault(x => !string.IsNullOrEmpty(x.Name) &&
                                                                             x.Name.Contains(this.GetType()
                                                                                 .Name))?.ThreadSize ?? 10;
            messageService.RegisterHandler<RefreshTokenMessage>(m =>
            {
                try
                {
                    m.Options = (int)MessageOption.None;
                    var task = Task.Run(async () => await ProcessMessage(m.GetBody()));
                    task.Wait();
                    return Task.CompletedTask;
                }
                catch (Exception e)
                {
                    ExceptionHelper.WriteLogExceptionMq(typeof(RefreshTokenMessage), m.Body, e);
                    return string.Empty;
                }
            }, threadSize);

            _channelClient = channelClient;
            _settings = settings;
            _omniChannelPlatformService = omniChannelPlatformService;
        }

        public async Task ProcessMessage(RefreshTokenMessage data)
        {
            var logId = Guid.NewGuid();
            var logObj = new LogObject(Logger, logId)
            {
                Action = "RefreshAccessToken",
                RequestObject = data,
                OmniChannelId = data.ChannelId
            };

            try
            {
                var channel = data.ChannelJson?.FromJson<Domain.Model.OmniChannel>();

                if (channel == null)
                {
                    using var db = await DbConnectionFactory.OpenAsync();
                    var omniChannelRepository = new OmniChannelRepository(db);
                    channel = await omniChannelRepository.GetByIdAsync(data.ChannelId, true);
                }

                if (channel == null || string.IsNullOrEmpty(channel.OmniChannelAuth?.RefreshToken))
                {
                    logObj.Description = "Channel or RefreshToken is null";
                    logObj.LogError();
                    return;
                }

                var channelClient = _channelClient.GetClientV2(channel.Type, _settings);
                var refreshTokenDecode = CryptoHelper.RijndaelDecrypt(channel.OmniChannelAuth.RefreshToken);
                var platform = await _omniChannelPlatformService.GetById(channel.PlatformId);

                var lockGetAccessTokenKey = string.Format(KvConstant.LockGetAccessTokenChannelId, channel.Id);
                CacheClient.Set(lockGetAccessTokenKey, true, TimeSpan.FromMinutes(1));

                var newToken = await channelClient.RefreshAccessToken(refreshTokenDecode, channel.Id, logId,
                    new ChannelAuth { ShopId = channel.OmniChannelAuth.ShopId }, platform);

                if (newToken == null)
                {
                    CacheClient.Remove(lockGetAccessTokenKey);

                    data.RetryCount++;
                    if (data.RetryCount <= NumberHelper.GetValueOrDefault(_settings?.Get<int>("RetryRefreshToken"), 2))
                    {
                        using var mq = MessageService.MessageFactory.CreateMessageProducer();
                        mq.Publish(data);
                    }

                    logObj.Description = "Refresh token failed";
                    logObj.LogError();
                }
                else
                {
                    var retailer = await GetRetailer(channel.RetailerId);
                    if (retailer == null)
                    {
                        logObj.Description = "Retailer is null";
                        logObj.LogError();

                        CacheClient.Remove(lockGetAccessTokenKey);
                        return;
                    }
                    var kvGroup = await GetRetailerGroup(retailer.GroupId);
                    if (kvGroup == null || string.IsNullOrEmpty(kvGroup.ConnectionString))
                    {
                        logObj.Description = "kvGroup.ConnectionString is null";
                        logObj.LogError();

                        CacheClient.Remove(lockGetAccessTokenKey);
                        return;
                    }

                    if (newToken.IsDeactivateChannel)
                    {
                        var connectStringName = kvGroup.ConnectionString.Substring(kvGroup.ConnectionString.IndexOf('=') + 1);
                        var kvContext = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, channel.RetailerId, connectStringName, channel.BranchId, _settings?.Get<int>("ExecutionContext"));
                        var context = new KvInternalContext
                        {
                            RetailerCode = kvContext.RetailerCode,
                            BranchId = channel.BranchId,
                            RetailerId = channel.RetailerId,
                            UserId = kvContext?.User?.Id ?? 0,
                        };
                        var integrationInternalClient = new IntegrationInternalClient(_settings);
                        await integrationInternalClient.DeactivateChannel(context, channel.Id, Enum.GetName(typeof(ShareKernel.Common.ChannelType), channel.Type));
                        logObj.Description = "DeactivateChannel successful";
                        logObj.LogInfo();
                    }
                    else
                    {
                        CacheClient.Remove(string.Format(KvConstant.ChannelAuthKey, channel.Id));

                        CacheClient.Set(string.Format(KvConstant.RefreshTokenKeyByChannelId, channel.Id),
                            new { newToken.RefreshToken, createDate = DateTime.Now }, TimeSpan.FromDays(30));

                        await UpdateAuthAsync(new ChannelAuthRequest
                        {
                            AccessToken = newToken.AccessToken,
                            ExpiresIn = newToken.ExpiresIn,
                            RefreshToken = newToken.RefreshToken,
                            Id = channel.OmniChannelAuth.Id,
                            OmniChannelId = channel.Id,
                            ShopId = newToken.ShopId,
                            RefreshExpiresIn = newToken.RefreshExpiresIn
                        });


                        logObj.Description = "Refresh token successful";
                        logObj.LogInfo();
                    }

                    CacheClient.Remove(lockGetAccessTokenKey);
                }
            }
            catch (Exception ex)
            {
                logObj.LogError(ex);
            }
        }

        private async Task UpdateAuthAsync(ChannelAuthRequest authData)
        {
            var auth = new OmniChannelAuth
            {
                ShopId = authData.ShopId,
                ExpiresIn = authData.ExpiresIn,
                RefreshExpiresIn = authData.RefreshExpiresIn,
                OmniChannelId = authData.OmniChannelId
            };

            if (!string.IsNullOrEmpty(authData.AccessToken))
            {
                auth.AccessToken = CryptoHelper.RijndaelEncrypt(authData.AccessToken);
            }

            if (!string.IsNullOrEmpty(authData.RefreshToken))
            {
                auth.RefreshToken = CryptoHelper.RijndaelEncrypt(authData.RefreshToken);
            }

            if (authData.ExpiresIn > 0)
            {
                auth.ExpiryDate = DateTime.Now.AddSeconds(authData.ExpiresIn - 30);
            }

            using var db = await DbConnectionFactory.OpenAsync();
            var omniChannelAuthRepository = new OmniChannelAuthRepository(db);
            var existAuth = await omniChannelAuthRepository.GetByIdAsync(authData.Id);
            if (existAuth == null)
            {
                auth.CreatedDate = DateTime.Now;
                await db.InsertAsync(auth);
            }
            else
            {
                existAuth.AccessToken = auth.AccessToken;
                existAuth.RefreshToken = auth.RefreshToken;
                existAuth.ExpiresIn = auth.ExpiresIn;
                existAuth.RefreshExpiresIn = auth.RefreshExpiresIn;
                existAuth.ShopId = auth.ShopId != 0 ? auth.ShopId : existAuth.ShopId;
                existAuth.ExpiryDate = auth.ExpiryDate;
                existAuth.ModifiedDate = DateTime.Now;

                await db.UpdateAsync(existAuth);
            }
        }
    }
}

﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Microsoft.Extensions.Configuration;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.Schedule.Services
{
    public class ReSyncPriceFailServices : BaseService
    {
        private readonly IProductMongoService _productMongoService;
        private readonly IMappingBusiness _mappingBusiness;
        private readonly IPriceBusiness _priceBusiness;
        private readonly ILog _logger = LogManager.GetLogger(typeof(ReSyncPriceFailServices));
        public ReSyncPriceFailServices(IConfiguration configuration,
            ICacheClient cacheClient,
            IMessageService messageService,
            KvRedisConfig mRedisConfig,
            IProductMongoService productMongoService,
            IDbConnectionFactory dbConnectionFactory,
            IMappingBusiness mappingBusiness,
            IPriceBusiness priceBusiness) : base(configuration,
            cacheClient,
            messageService,
            dbConnectionFactory)
        {
            var threadSize = mRedisConfig?.EventMessages.FirstOrDefault(x => !string.IsNullOrEmpty(x.Name) &&
                                                                             x.Name.Contains(this.GetType()
                                                                                 .Name))?.ThreadSize ?? 10;
            messageService.RegisterHandler<ReSyncPriceFailMessage>(m =>
            {
                try
                {
                    m.Options = (int)MessageOption.None;
                    var task = Task.Run(async () => await ProcessMessage(m.GetBody()));
                    task.GetAwaiter().GetResult();
                    return Task.CompletedTask;
                }
                catch (Exception e)
                {
                    Logger.Error(e.Message, e);
                    return string.Empty;
                }
            }, threadSize);
            _productMongoService = productMongoService;
            _mappingBusiness = mappingBusiness;
            _priceBusiness = priceBusiness;
        }

        private async Task ProcessMessage(ReSyncPriceFailMessage message)
        {
            var reSyncLogObject = new LogObject(_logger, message.LogId)
            {
                Action =
                    $"{nameof(ReSyncPriceFailServices)}",
                OmniChannelId = message.ChannelId,
                RetailerId = message.RetailerId
            };
            if (message.RetailerId <= 0 || message.ChannelId <= 0)
            {
                return;
            }

            var kvRetailer = await GetRetailer(message.RetailerId);
            if (kvRetailer == null || !kvRetailer.IsActive)
            {
                reSyncLogObject.Description = "Empty Retailer or Retailer inactive";
                reSyncLogObject.LogInfo();
                return;
            }

            var kvGroup = await GetRetailerGroup(kvRetailer.GroupId);
            if (kvGroup == null)
            {
                reSyncLogObject.Description = "Empty Group";
                reSyncLogObject.LogInfo();
                return;
            }
            var connectStringName = kvGroup.ConnectionString.Substring(kvGroup.ConnectionString.IndexOf('=') + 1);
            var timeRange = Configuration.GetValue("ReSyncPriceTimeOutRange", 5);
            var products =
                await _productMongoService.GetProductByStatusCode((int) HttpStatusCode.BadGateway, message.ChannelId, DateTime.Now.AddMinutes(-timeRange), DateTime.Now);
            if (products == null || !products.Any())
            {
                reSyncLogObject.LogInfo(true);
                return;
            }
            var channelProductIds = products.Select(p => p.CommonItemId).ToList();
            var productMaps = await _mappingBusiness.GetMappingByItemIds(message.ChannelId, message.RetailerId, channelProductIds);
            if (productMaps == null || !productMaps.Any())
            {
                reSyncLogObject.LogInfo(true);
                return;
            }
            var channel = new Domain.Model.OmniChannel
            {
                Id = message.ChannelId,
                Name = message.ChannelName,
                Type = message.ChannelType,
                RetailerId = message.RetailerId,
                BranchId = message.BranchId,
                PriceBookId = message.PriceBookId,
                BasePriceBookId = message.BasePriceBookId
            };
            await _priceBusiness.SyncMultiPrice(connectStringName, channel,
                productMaps.Select(p => p.CommonProductChannelId).ToList(), productMaps.Select(p => p.ProductKvId).ToList(),
                false, message.LogId, true, GetSyncMultiPageSize(message.ChannelType), productMaps, false, "Queue created from CurrentProductService run ReSync Lzd price");
            reSyncLogObject.TotalItem = productMaps.Count;
            reSyncLogObject.LogInfo(true);
        }
    }
}
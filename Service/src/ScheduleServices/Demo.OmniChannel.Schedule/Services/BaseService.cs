﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.ShareKernel.Common;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Schedule.Services
{
    public class BaseService: IHostedService
    {
        protected readonly IConfiguration Configuration;
        protected readonly ICacheClient CacheClient;
        protected readonly IMessageService MessageService;
        protected readonly IDbConnectionFactory DbConnectionFactory;
        protected readonly string KvMasterEntities;
        protected ILog Logger => LogManager.GetLogger(typeof(BaseService));

        public BaseService(IConfiguration configuration, ICacheClient cacheClient, IMessageService messageService, IDbConnectionFactory dbConnectionFactory)
        {
            Configuration = configuration;
            var kvSql = new KvSqlConnectString();
            Configuration.GetSection("SqlConnectStrings").Bind(kvSql);
            KvMasterEntities = kvSql.KvMasterEntities;

            CacheClient = cacheClient;
            MessageService = messageService;
            DbConnectionFactory = dbConnectionFactory;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        protected async Task<KvRetailer> GetRetailer(long retailerId)
        {
            var kvRetailer = CacheClient.Get<KvRetailer>(string.Format(KvConstant.RetailerIdCacheKey, retailerId));
            if (kvRetailer == null)
            {
                using (var dbMaster = DbConnectionFactory.Open(nameof(KvMasterEntities)))
                {
                    kvRetailer = await dbMaster.SingleAsync<KvRetailer>(x => x.Id == retailerId);
                    if (kvRetailer != null)
                    {
                        CacheClient.Set(string.Format(KvConstant.RetailerIdCacheKey, retailerId), kvRetailer, DateTime.Now.AddDays(7));
                    }
                }
            }
            return kvRetailer;
        }
        protected int GetSyncMultiPageSize(byte channelType)
        {
            int pageSize = 20;
            switch (channelType)
            {
                case (byte)ChannelType.Lazada:
                {
                    pageSize = Configuration?.GetValue<int>("SyncMultiLazadaPageSize", 20) ?? 20;
                    break;
                }

                case (byte)ChannelType.Shopee:
                {
                    pageSize = Configuration?.GetValue<int>("SyncMultiShopeePageSize", 50) ?? 50;
                    break;
                }
                case (byte)ChannelType.Tiki:
                {
                    pageSize = Configuration?.GetValue<int>("SyncMultiTikiPageSize", 50) ?? 50;
                    break;
                }
            }
            return pageSize;
        }
        protected async Task<KvGroup> GetRetailerGroup(int groupId)
        {
            var kvGroup = CacheClient.Get<KvGroup>(string.Format(KvConstant.KvGroupCacheKey, groupId));
            if (kvGroup == null)
            {
                using (var dbMaster = DbConnectionFactory.Open(nameof(KvMasterEntities)))
                {
                    kvGroup = await dbMaster.SingleAsync<KvGroup>(x => x.Id == groupId);
                    if (kvGroup != null)
                    {
                        CacheClient.Set(string.Format(KvConstant.KvGroupCacheKey, groupId), kvGroup, DateTime.Now.AddDays(7));
                    }
                }
            }
            return kvGroup;
        }
    }
}
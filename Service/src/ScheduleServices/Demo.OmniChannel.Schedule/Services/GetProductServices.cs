﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.ShareKernel.Dto;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Microsoft.Extensions.Configuration;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Schedule.Services
{
    public class GetProductServices :BaseService
    {
        private ILog _logger => LogManager.GetLogger(typeof(GetProductServices));
        public GetProductServices(IConfiguration configuration,
            ICacheClient cacheClient,
            IMessageService messageService,
            KvRedisConfig mRedisConfig,
            IDbConnectionFactory dbConnectionFactory) : base(configuration,
            cacheClient,
            messageService,
            dbConnectionFactory)
        {
            var threadSize = mRedisConfig?.EventMessages.FirstOrDefault(x => !string.IsNullOrEmpty(x.Name) &&
                                                                             x.Name.Contains(this.GetType()
                                                                                 .Name))?.ThreadSize ?? 10;
            messageService.RegisterHandler<GetProductByChannelMessage>(m =>
            {
                try
                {
                    m.Options = (int)MessageOption.None;
                    var task = Task.Run(async () => await ProcessMessage(m.GetBody()));
                    task.Wait();
                    return Task.CompletedTask;
                }
                catch (Exception e)
                {
                    Logger.Error(e.Message, e);
                    return string.Empty;
                }
            }, threadSize);
        }

        private async Task ProcessMessage(GetProductByChannelMessage message)
        {
            var getGetProductBranchServiceLog = new LogObject(_logger, message.LogId)
            {
                Action =
                    $"{nameof(GetProductServices)}",
                OmniChannelId = message.ChannelId,
                RetailerId = message.RetailerId
            };
            if (message.RetailerId <= 0 || message.ChannelId <= 0)
            {
                return;
            }

            var kvRetailer = await GetRetailer(message.RetailerId);
            if (kvRetailer == null || !kvRetailer.IsActive)
            {
                getGetProductBranchServiceLog.Description = "Empty Retailer or Retailer inactive";
                getGetProductBranchServiceLog.LogInfo();
                return;
            }

            var kvGroup = await GetRetailerGroup(kvRetailer.GroupId);
            if (kvGroup == null)
            {
                return;
            }
            var connectStringName = kvGroup.ConnectionString.Substring(kvGroup.ConnectionString.IndexOf('=') + 1);
            var range = Configuration?.GetValue<int>("GetProductRange", 20) ?? 20;
            using (var db = DbConnectionFactory.OpenDbConnection(connectStringName.ToLower())
            )
            {
                var productRepository = new ProductRepository(db);
                var products =
                    await productRepository.GetProductByRange(message.RetailerId, range);
                if (products == null || !products.Any())
                {
                    return;
                }
                var pageSize = GetSyncMultiPageSize(message.ChannelType);
                var totalRecords = products.Count;
                var total = products.Count / pageSize;
                total = totalRecords % pageSize == 0 ? total : total + 1;
                for (int i = 0; i < total; i++)
                {
                    var currentPage = products.Skip(i * pageSize).Take(pageSize).ToList();
                    if (!currentPage.Any())
                    {
                        continue;
                    }
                    PublishMessageQueue(message, currentPage, connectStringName, message.LogId);
                }
                getGetProductBranchServiceLog.TotalItem = totalRecords;
                getGetProductBranchServiceLog.LogInfo(true);
            }
        }

        private void PublishMessageQueue(GetProductByChannelMessage message, List<ProductDto> currentPage, string connectStringName, Guid messageLogId)
        {
            using (var mq = MessageService.MessageFactory.CreateMessageProducer())
            {
                switch (message.ChannelType)
                {
                    case (byte) ChannelType.Shopee:
                    {
                        mq.Publish(new ShopeeCurrentProductMessage
                        {
                            ChannelId = message.ChannelId,
                            ChannelName = message.ChannelName,
                            ChannelType = message.ChannelType,
                            RetailerId = message.RetailerId,
                            BasePriceBookId = message.BasePriceBookId,
                            BranchId = message.BranchId,
                            PriceBookId = message.PriceBookId,
                            ConnectStringName = connectStringName,
                            Products = currentPage.Select(p => new PriceDto
                            {
                                KvProductId = p.Id,
                                Revision = p.Revision
                            }).ToList(),
                            LogId = messageLogId
                        });
                            break;
                    }
                    case (byte)ChannelType.Tiki:
                        {
                            mq.Publish(new TikiCurrentProductMessage
                            {
                                ChannelId = message.ChannelId,
                                BranchId = message.BranchId,
                                ChannelName = message.ChannelName,
                                ChannelType = message.ChannelType,
                                RetailerId = message.RetailerId,
                                BasePriceBookId = message.BasePriceBookId,
                                PriceBookId = message.PriceBookId,
                                ConnectStringName = connectStringName,
                                Products = currentPage.Select(p => new PriceDto
                                {
                                    KvProductId = p.Id,
                                    Revision = p.Revision
                                }).ToList(),
                                LogId = messageLogId
                            });
                            break;
                        }
                    case (byte)ChannelType.Lazada:
                        {
                            mq.Publish(new LazadaCurrentProductMessage
                            {
                                ChannelId = message.ChannelId,
                                BranchId = message.BranchId,
                                ChannelName = message.ChannelName,
                                ChannelType = message.ChannelType,
                                RetailerId = message.RetailerId,
                                BasePriceBookId = message.BasePriceBookId,
                                PriceBookId = message.PriceBookId,
                                ConnectStringName = connectStringName,
                                Products = currentPage.Select(p => new PriceDto
                                {
                                    KvProductId = p.Id,
                                    Revision = p.Revision
                                }).ToList(),
                                LogId = messageLogId
                            });
                            break;
                        }
                    case (byte)ChannelType.Sendo:
                    {
                        mq.Publish(new SendoCurrentProductMessage
                        {
                            ChannelId = message.ChannelId,
                            BranchId = message.BranchId,
                            ChannelName = message.ChannelName,
                            ChannelType = message.ChannelType,
                            RetailerId = message.RetailerId,
                            BasePriceBookId = message.BasePriceBookId,
                            PriceBookId = message.PriceBookId,
                            ConnectStringName = connectStringName,
                            Products = currentPage.Select(p => new PriceDto
                            {
                                KvProductId = p.Id,
                                Revision = p.Revision,
                                KvMasterProductId = p.MasterProductId
                            }).ToList(),
                            LogId = messageLogId
                        });
                        break;
                    }

                    case (byte)ChannelType.Tiktok:
                        {
                            mq.Publish(new TiktokCurrentProductMessage
                            {
                                ChannelId = message.ChannelId,
                                BranchId = message.BranchId,
                                ChannelName = message.ChannelName,
                                ChannelType = message.ChannelType,
                                RetailerId = message.RetailerId,
                                BasePriceBookId = message.BasePriceBookId,
                                PriceBookId = message.PriceBookId,
                                ConnectStringName = connectStringName,
                                Products = currentPage.Select(p => new PriceDto
                                {
                                    KvProductId = p.Id,
                                    Revision = p.Revision,
                                    KvMasterProductId = p.MasterProductId
                                }).ToList(),
                                LogId = messageLogId
                            });
                            break;
                        }

                }
            }
        }
    }
}

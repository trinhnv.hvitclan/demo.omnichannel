﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Helper;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Models;
using Demo.OmniChannel.Utilities;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Schedule.Jobs.ShopeeV2
{
    [DisallowConcurrentExecution]
    public class ShopeeSyncRetryOrderV2Job : BaseSyncRetryOrderJob
    {
        private readonly IChannelBusiness _channelBusiness;

        public ShopeeSyncRetryOrderV2Job(IConfiguration config,
            ICacheClient cacheClient,
            IRetryOrderMongoService retryOrderMongoService,
            IDbConnectionFactory dbConnectionFactory,
            IMessageFactory messageFactory,
            IScheduleService scheduleService,
            IChannelBusiness channelBusiness,
            IAppSettings appSettings
            ) : base(config,
            cacheClient,
            retryOrderMongoService,
            messageFactory,
            dbConnectionFactory,
            scheduleService)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Shopee;
            _channelBusiness = channelBusiness;
            _ = new SelfAppConfig(appSettings);
        }

        public override async Task PushMessageQueue(RetryOrder order, Guid logId)
        {
            var syncRetryOrderJobLog = new LogObject(Logger, logId)
            {
                Action = $"{nameof(BaseSyncRetryOrderJob)}_SendWebhookShopee"
            };
            try
            {
                if (order == null)
                {
                    syncRetryOrderJobLog.Description = "Order retry is null";
                    return;
                }
                var channel = await _channelBusiness.GetChannel(order.ChannelId, order.RetailerId, logId);
                if (channel == null)
                {
                    syncRetryOrderJobLog.Description = "Channel is null !!";
                    return;
                }
                var req = new ShopeeWebhookModel
                {
                    ShopId = ConvertHelper.ToInt64(channel.IdentityKey),
                    Code = 3,
                    Timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds(),
                    Data = new DataBody
                    {
                        Ordersn = order.OrderId,
                        Status = "READY_TO_SHIP",
                        UpdateTime = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds()
                    }
                };
                await ShopeeHelperV2.SendToWebHookAsync(req);
                syncRetryOrderJobLog.Description = $"Push webhook retry success orderid {order.OrderId}";
                syncRetryOrderJobLog.LogInfo();
            }
            catch (Exception ex)
            {
                syncRetryOrderJobLog.LogError(ex);
            }

        }
    }
}

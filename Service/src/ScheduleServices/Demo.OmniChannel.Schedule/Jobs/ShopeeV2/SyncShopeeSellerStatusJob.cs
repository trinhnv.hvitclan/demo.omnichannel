﻿using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Microsoft.Extensions.Configuration;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Schedule.Jobs.ShopeeV2
{
    public class SyncShopeeSellerStatusJob : SyncBaseSellerStatusJob
    {
        public SyncShopeeSellerStatusJob(IConfiguration config,
          ICacheClient cacheClient,
          IDbConnectionFactory dbConnectionFactory,
          IAppSettings settings,
          IScheduleService scheduleService,
          IOmniChannelAuthService omniChannelAuthService,
          IOmniChannelPlatformService omniChannelPlatformService
          ) : base(config,
          cacheClient,
          dbConnectionFactory,
          settings,
          scheduleService,
          omniChannelAuthService,
          omniChannelPlatformService)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Shopee;
        }

        public override async Task<List<Domain.Model.OmniChannel>> GetChannelLst(IDbConnection dbConnection)
        {
            return await dbConnection.LoadSelectAsync<Domain.Model.OmniChannel>(x => x.Status == (byte)ChannelStatusType.NeedRegister && x.Type == ChannelType);
        }


        public override async Task<bool> CheckChannelValid(IBaseClient client, ChannelClient.Models.TokenResponse authen, Demo.OmniChannel.ChannelClient.Models.Platform platform)
        {
            var auth = new ChannelAuth { ShopId = authen.ShopId, AccessToken = authen.AccessToken };
            var shopInfo = await client.GetShopInfo(auth, null, true, platform);
            if (shopInfo != null)
            {
                return true;
            }
            return false;
        }

    }
}

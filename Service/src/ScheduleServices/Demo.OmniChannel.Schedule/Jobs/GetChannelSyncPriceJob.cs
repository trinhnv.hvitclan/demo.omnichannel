﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.Schedule.Jobs
{
    [DisallowConcurrentExecution]
    public class GetChannelSyncPriceJob : IJob
    {
        private readonly IMessageFactory _messageFactory;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly IAppSettings _settings;
        private readonly ICacheClient _cacheClient;
        private ILog _logger => LogManager.GetLogger(typeof(GetChannelSyncPriceJob));

        public GetChannelSyncPriceJob(IDbConnectionFactory dbConnectionFactory,
            IMessageFactory messageFactory,
            IAppSettings settings,
            ICacheClient cacheClient)
        {
            _messageFactory = messageFactory;
            _dbConnectionFactory = dbConnectionFactory;
            _settings = settings;
            _cacheClient = cacheClient;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var logId = Guid.NewGuid();
            var getChannelSyncPriceJobLog = new LogObject(_logger, logId)
            {
                Action =
                    $"{nameof(GetChannelSyncPriceJob)}"
            };
            using (var db = _dbConnectionFactory.OpenDbConnection())
            {
                var omniChannelRepository = new OmniChannelRepository(db);
                var channels = await omniChannelRepository.GetChannelHaveSyncPriceAsync();
                if (channels == null || !channels.Any())
                {
                    getChannelSyncPriceJobLog.Description = "Empty Channel";
                    getChannelSyncPriceJobLog.LogInfo();
                    return;
                }

                var isSkipReSyncFail = _cacheClient.Get<bool>(KvConstant.SkipResyncPriceFail);
                var isResyncFailStatusCode = false;
                foreach (var channel in channels)
                {
                    //if (channel.RetailerId != 18118)
                    //{
                    //    continue;
                    //}
                    using (var mq = _messageFactory.CreateMessageProducer())
                    {
                        mq.Publish(new GetProductByChannelMessage
                        {
                            RetailerId = channel.RetailerId,
                            BranchId = channel.BranchId,
                            ChannelId = channel.Id,
                            ChannelName = channel.Name,
                            ChannelType = channel.Type,
                            BasePriceBookId = channel.BasePriceBookId ?? 0,
                            PriceBookId = channel.PriceBookId ?? 0,
                            LogId = logId
                        });
                        if (isSkipReSyncFail)
                        {
                            continue;
                        }
                        isResyncFailStatusCode = true;
                        mq.Publish(new ReSyncPriceFailMessage
                        {
                            RetailerId = channel.RetailerId,
                            BranchId = channel.BranchId,
                            ChannelId = channel.Id,
                            ChannelName = channel.Name,
                            ChannelType = channel.Type,
                            BasePriceBookId = channel.BasePriceBookId ?? 0,
                            PriceBookId = channel.PriceBookId ?? 0,
                            LogId = logId
                        });
                    }
                }

                if (!isSkipReSyncFail && isResyncFailStatusCode)
                {
                    _cacheClient.Set(KvConstant.SkipResyncPriceFail, true,
                        TimeSpan.FromMinutes(_settings?.Get("ResyncPriceJobScanInterval", 1) ?? 1));
                }
                getChannelSyncPriceJobLog.TotalItem = channels.Count;
                getChannelSyncPriceJobLog.LogInfo();
            }
        }
    }
}
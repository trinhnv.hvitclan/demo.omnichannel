﻿using System;
using System.Data;
using System.Threading.Tasks;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Schedule.Jobs
{
    /// <summary>
    /// Sync New Schedules from DB to Cache 
    /// </summary>
    public class BaseDailySyncJobFromDbJob : BaseJob
    {
        public byte ChannelType { get; set; }
        public BaseDailySyncJobFromDbJob(IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IScheduleService scheduleService) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService)
        {
        }

        public override async Task Execute(IJobExecutionContext context)
        {
            if (!IsValidMaintenanceTime(this.GetType().Name)) return;

            var logObject = new OmniChannel.Infrastructure.Auditing.LogObject(Logger, Guid.NewGuid())
            {
                Action =
                    $"{EnumHelper.ToDescription((ChannelType) ChannelType)}_{nameof(BaseDailySyncJobFromDbJob)}"
            };

            long totalItemHandle = 0;
            long totalItemCompleted = 0;
            long newItem = 0;

            try
            {
                using (var db = DbConnectionFactory.OpenDbConnection())
                {
                    using (var scheduleRepo = new OmniChannelScheduleRepository(db))
                    {
                        await RunWithScheduleType(scheduleRepo, ScheduleType.SyncOrder);
                        await RunWithScheduleType(scheduleRepo, ScheduleType.SyncModifiedProduct);
                    }
                }

                logObject.TotalItem = totalItemHandle;
                logObject.Description = $"Total completed: {totalItemCompleted}/{totalItemHandle} (New item: {newItem}).";
                logObject.LogInfo();
            }
            catch (Exception e)
            {
                logObject.Description = $"Total completed: {totalItemCompleted}/{totalItemHandle} (New item: {newItem}).";
                logObject.LogError(e);
                throw;
            }

            // Local function
            async Task RunWithScheduleType(OmniChannelScheduleRepository scheduleRepo, ScheduleType scheduleType)
            {
                var total = await scheduleRepo.GetTotal(ChannelType, (byte) scheduleType, null, null);
                if (total == 0) return;

                totalItemHandle += total;

                var pageSize = 1000;
                var totalPage = Math.Ceiling((double)total / pageSize);

                for (var i = 0; i < totalPage; i++)
                {
                    var scIds = await scheduleRepo.GetIds(ChannelType, (byte) scheduleType, null, null, i * pageSize, pageSize);
                    foreach (var scId in scIds)
                    {
                        totalItemCompleted++;
                        var cacheKey = KvConstant.ScheduleDtoCacheKey.Fmt(scId);

                        try
                        {
                            var dataCache = CacheClient.Get<ScheduleDto>(cacheKey);
                            if (dataCache != null) continue;
                        }
                        catch (Exception e)
                        {
                            Logger.Error($"Get cache ScheduleDto error (CacheKey: {cacheKey}): {e}");
                            continue;
                        }

                        var dataDb = await scheduleRepo.GetScheduleForJobById(scId);
                        if (dataDb == null) continue;

                        dataDb.IsRunning = false;

                        CacheClient.Set(cacheKey, dataDb);
                        newItem++;
                    }
                }
            }
        }
    }
}
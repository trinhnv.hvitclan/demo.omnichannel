﻿using Demo.OmniChannel.ScheduleService.Interfaces;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Data;

namespace Demo.OmniChannel.Schedule.Jobs
{
    [DisallowConcurrentExecution]
    public class LazadaDailySyncFromCache : BaseDailySyncFromCacheJob
    {
        public LazadaDailySyncFromCache(IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IScheduleService scheduleService) : base(config,
            cacheClient, dbConnectionFactory, scheduleService)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Lazada;
        }
    }
    [DisallowConcurrentExecution]
    public class ShopeeDailySyncFromCache : BaseDailySyncFromCacheJob
    {
        public ShopeeDailySyncFromCache(IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IScheduleService scheduleService) : base(config,
            cacheClient,
            dbConnectionFactory, scheduleService)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Shopee;
        }
    }
    [DisallowConcurrentExecution]
    public class TikiDailySyncFromCache : BaseDailySyncFromCacheJob
    {
        public TikiDailySyncFromCache(IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IScheduleService scheduleService) : base(config,
            cacheClient,
            dbConnectionFactory, scheduleService)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Tiki;
        }
    }
    [DisallowConcurrentExecution]
    public class SendoDailySyncFromCache : BaseDailySyncFromCacheJob
    {
        public SendoDailySyncFromCache(IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IScheduleService scheduleService) : base(config,
            cacheClient,
            dbConnectionFactory, scheduleService)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Sendo;
        }
    }
}
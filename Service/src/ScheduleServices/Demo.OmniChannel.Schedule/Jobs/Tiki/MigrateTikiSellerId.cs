using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.ChannelClient.Impls;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.Utilities;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ChannelType = Demo.OmniChannelCore.Api.Shared.Enum.ChannelType;

namespace Demo.OmniChannel.Schedule.Jobs.Tiki
{
    public class MigrateTikiSellerId: BaseJob
    {
        private readonly IAppSettings _settings;

        public MigrateTikiSellerId(
            IConfiguration config, 
            ICacheClient cacheClient, 
            IDbConnectionFactory dbConnectionFactory,
            IAppSettings settings,
            IScheduleService scheduleService) : base(config, cacheClient, dbConnectionFactory, scheduleService)
        {
            _settings = settings;
        }

        public override async Task Execute(IJobExecutionContext context)
        {
            var logId = Guid.NewGuid();
            var logAction = $"{nameof(LicenseScheduleJob)}";
            var migrateTikiSellerIdLog = new LogObject(Logger, logId)
            {
                Action = logAction
            };
            try
            {
                using var db = DbConnectionFactory.OpenDbConnection();
                var omniChannelRepository = new OmniChannelRepository(db);
                var channels = await omniChannelRepository.GetTotalByChannelType((byte)ChannelType.Tiki, null, null);
                Dictionary<string, Domain.Model.OmniChannel> channelDictionary =
                    new Dictionary<string, Domain.Model.OmniChannel>();
                List<long> channelIds = new List<long>();
                foreach (var channel in channels)
                {
                    channelDictionary.Add(channel.Id.ToString(), channel);
                    channelIds.Add(channel.Id);
                }
                migrateTikiSellerIdLog.RequestObject = $"Total omni channel Tiki: {channelIds.Count}";
                migrateTikiSellerIdLog.LogInfo();

                var omniChannelAuthRepository = new OmniChannelAuthRepository(db);
                var query = omniChannelAuthRepository.BuildGenericQuery(
                    channel => channel.OmniChannelId != null && 
                               channelIds.Contains((long)channel.OmniChannelId) &&
                               channel.AccessToken != null &&
                               channel.AccessToken != "" &&
                               channel.ShopId == 0);
                var channelAuths = await omniChannelAuthRepository.GetAsync<OmniChannelAuth>(query);
                migrateTikiSellerIdLog.RequestObject = $"Total omni channel auth Tiki : {channelAuths.Count}";
                migrateTikiSellerIdLog.LogInfo();
                foreach (var channelAuth in channelAuths)
                {
                    try
                    {
                        var accessToken = CryptoHelper.RijndaelDecrypt(channelAuth.AccessToken);
                        var clientTiki = new TikiClientV2(_settings);
                        var sellerInfo = await clientTiki.GetShopStatus(accessToken);
                        if (sellerInfo == null)
                        {
                            migrateTikiSellerIdLog.LogWarning($@"Seller info is null with 
                                token: {accessToken} - Omni channel ID: {channelAuth.OmniChannelId}");
                            continue;
                        }
                        // Update omni channel
                        var omniChannel = channelDictionary[channelAuth.OmniChannelId.ToString()];
                        if (omniChannel != null)
                        {
                            omniChannel.IdentityKey = sellerInfo.IdentityKey;
                            await omniChannelRepository.UpdateAsync(omniChannel);
                        }

                        // Update omni channel auth
                        channelAuth.ShopId = long.Parse(sellerInfo.IdentityKey);
                        await omniChannelAuthRepository.UpdateAsync(channelAuth);

                        // Remove cache
                        CacheClient.Remove(string.Format(KvConstant.ChannelAuthKey, channelAuth.OmniChannelId));
                        if (omniChannel != null)
                            CacheClient.Remove(string.Format(KvConstant.ChannelsByRetailerKey, omniChannel.RetailerId));
                        CacheClient.Remove(string.Format(KvConstant.ChannelByIdKey, channelAuth.OmniChannelId));
                    } catch (Exception e)
                    {
                        migrateTikiSellerIdLog.LogError(e);
                    }
                }
            }
            catch (Exception e)
            {
                migrateTikiSellerIdLog.LogError(e);
                throw;
            }
        }
    }
}
﻿using Quartz;
using ServiceStack.Data;
using System;
using System.Threading.Tasks;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using ServiceStack.Messaging;
using ServiceStack.Caching;
using Demo.OmniChannel.Repository.Impls;
using ServiceStack.Logging;
using System.Linq;
using System.Collections.Generic;
using ServiceStack;

namespace Demo.OmniChannel.Schedule.Jobs
{
    public abstract class BaseDailyRefreshAccessTokenJob : IJob
    {
        private readonly IMessageFactory _messageFactory;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly ICacheClient _cacheClient;
        private ILog _logger => LogManager.GetLogger(typeof(BaseDailyRefreshAccessTokenJob));
        protected BaseDailyRefreshAccessTokenJob(
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IMessageFactory messageFactory)
        {
            _cacheClient = cacheClient;
            _dbConnectionFactory = dbConnectionFactory;
            _messageFactory = messageFactory;
        }
        protected byte ChannelType { get; set; }
        protected double RefreshBeforeExpiryMinutes { get; set; } //day
        protected List<int> IncludeRetailerIds { get; set; }
        protected List<int> ExcludeRetailerIds { get; set; }

        public async Task Execute(IJobExecutionContext context)
        {
            var logId = Guid.NewGuid();
            var logAction = $"{EnumHelper.ToDescription((ChannelType)ChannelType)}_{nameof(BaseDailyRefreshAccessTokenJob)}";
            var refreshTokenLog = new LogObject(_logger, logId)
            {
                Action = logAction
            };
            try
            {
                using (var db = _dbConnectionFactory.OpenDbConnection())
                {
                    var channelRepository = new OmniChannelRepository(db);
                    var channels = await channelRepository.GetChannelForRefreshToken(ChannelType, RefreshBeforeExpiryMinutes);

                    if (IncludeRetailerIds.Any())
                    {
                        channels = channels.Where(x => IncludeRetailerIds.Contains(x.RetailerId)).ToList();
                    }

                    if (ExcludeRetailerIds.Any())
                    {
                        channels = channels.Where(x => !ExcludeRetailerIds.Contains(x.RetailerId)).ToList();
                    }
                    channels = channels.GroupBy(x => x.Id).Select(x => x.First()).ToList();

                    if (channels.Any())
                    {
                        using (var mq = _messageFactory.CreateMessageProducer())
                        {
                            foreach (var channel in channels)
                            {
                                var message = new RefreshTokenMessage
                                {
                                    ChannelId = channel.Id,
                                    ChannelJson = channel.ToSafeJson(),
                                    LogId = logId
                                };
                                mq.Publish(message);
                            }
                        }
                    }

                    refreshTokenLog.TotalItem = channels.Count;
                    refreshTokenLog.LogInfo();
                }
            }
            catch (Exception e)
            {
                refreshTokenLog.LogError(e);
            }
        }
    }
}

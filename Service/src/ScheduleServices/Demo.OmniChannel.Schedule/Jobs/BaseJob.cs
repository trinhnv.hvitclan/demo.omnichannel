﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Schedule.Common;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.Utilities;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Schedule.Jobs
{
    public abstract class BaseJob : IJob
    {
        protected readonly List<KvEntity> KvEntities;
        protected readonly string KvMasterEntities;
        protected readonly IConfiguration Configuration;
        protected readonly string KvChannelEntities;
        protected ILog Logger => LogManager.GetLogger(typeof(BaseJob));
        protected readonly ICacheClient CacheClient;
        protected readonly IDbConnectionFactory DbConnectionFactory;
        protected const string ScheduleCacheKey = KvConstant.ChannelScheduleByIdKey;
        public IScheduleService ScheduleService { get; set; }

        protected BaseJob(IConfiguration config, ICacheClient cacheClient, IDbConnectionFactory dbConnectionFactory, IScheduleService scheduleService)
        {
            Configuration = config;
            var kvSql = new KvSqlConnectString();
            Configuration.GetSection("SqlConnectStrings").Bind(kvSql);
            KvEntities = kvSql.KvEntitiesDC1.ToList();
            KvMasterEntities = kvSql.KvMasterEntities;
            KvChannelEntities = kvSql.KvChannelEntities;
            CacheClient = cacheClient;
            DbConnectionFactory = dbConnectionFactory;
            ScheduleService = scheduleService;
        }

        public abstract Task Execute(IJobExecutionContext context);

        protected async Task<KvRetailer> GetRetailer(long retailerId)
        {
            var kvRetailer = CacheClient.Get<KvRetailer>(string.Format(KvConstant.RetailerIdCacheKey, retailerId));
            if (kvRetailer == null)
            {
                using (var dbMaster = DbConnectionFactory.Open(nameof(KvMasterEntities)))
                {
                    kvRetailer = await dbMaster.SingleAsync<KvRetailer>(x => x.Id == retailerId);
                    if (kvRetailer != null)
                    {
                        CacheClient.Set(string.Format(KvConstant.RetailerIdCacheKey, retailerId), kvRetailer, DateTime.Now.AddDays(7));
                    }
                }
            }
            return kvRetailer;
        }

        protected async Task<KvGroup> GetRetailerGroup(int groupId)
        {
            var kvGroup = CacheClient.Get<KvGroup>(string.Format(KvConstant.KvGroupCacheKey, groupId));
            if (kvGroup == null)
            {
                using (var dbMaster = DbConnectionFactory.Open(nameof(KvMasterEntities)))
                {
                    kvGroup = await dbMaster.SingleAsync<KvGroup>(x => x.Id == groupId);
                    if (kvGroup != null)
                    {
                        CacheClient.Set(string.Format(KvConstant.KvGroupCacheKey, groupId), kvGroup, DateTime.Now.AddDays(7));
                    }
                }
            }
            return kvGroup;
        }
        protected async Task<List<ScheduleDto>> GetScheduleFromCaching(byte channelType, byte scheduleType, List<long> scheduleIds)
        {
            var schedules = ScheduleService.GetByIdsCoverDeserializeError(scheduleIds);
            if ((schedules == null || schedules.Count == 0 || schedules.Count != scheduleIds.Count)
                && !string.IsNullOrEmpty(KvChannelEntities))
            {
                schedules = await GetSchedulesDb(scheduleIds);
            }
            if (schedules == null)
            {
                return new List<ScheduleDto>();
            }
            schedules = schedules.Where(t => !t.IsRunning && t.Type == scheduleType && t.ChannelType == channelType).ToList();
            return schedules;
        }

        protected void RunningSchedule(byte channelType, IEnumerable<ScheduleDto> dtos)
        {
            ScheduleService.AddOrUpdateBatch(channelType, (byte)ScheduleType.SyncOrder, dtos);
        }

        /// <summary>
        /// Trong thời gian maintance không thực hiện jobs liên quan database
        ///   (không hợp lệ có ghi log)
        /// </summary>
        protected bool IsValidMaintenanceTime(string jobName)
        {
            var section = Configuration.GetSection("MaintenanceTime");
            var mtSetting = new MaintenanceTimeConfig();
            section.Bind(mtSetting);

            if (!SystemHelper.IsValidMaintenanceTime(mtSetting))
            {
                Logger.Info($"Is maintenance time --> ignore job name: {jobName}");
                return false;
            }
            else
            {
                return true;
            }
        }
        protected async Task<Domain.Common.KvInternalContext> CreateContext(int retailerId)
        {
            var retailer = await GetRetailer(retailerId);
            if (retailer == null) return null;
            var kvGroup = await GetRetailerGroup(retailer.GroupId);
            if (kvGroup == null) return null;

            var kvInternalContext = new Domain.Common.KvInternalContext
            {
                RetailerCode = retailer.Code,
                RetailerId = retailer.Id,
                Group = new Domain.Common.KvGroup
                {
                    Id = kvGroup?.Id ?? 0,
                    ConnectionString = kvGroup?.ConnectionString
                }
            };
            return kvInternalContext;
        }

        protected JobChannel GetJobChannel(string name)
        {
            var kvQuartz = new KvQuartz();
            Configuration.GetSection("Quartz").Bind(kvQuartz);
            return (kvQuartz?.JobChannels).FirstOrDefault(t => t.JobName.Contains(name));
        }


        #region Private method
        private async Task<List<ScheduleDto>> GetSchedulesDb(List<long> scheduleIds)
        {
            var sheduleDb = new List<ScheduleDto>();

            using (var dbOmni = DbConnectionFactory.Open(nameof(KvChannelEntities)))
            {
                var omniChannelScheduleRepository = new OmniChannelScheduleRepository(dbOmni);
                var totalPages = (int)Math.Ceiling(scheduleIds.Count / (decimal)1000);
                for (int page = 1; page <= totalPages; page++)
                {
                    var schedulePaging = scheduleIds.Page(page, 1000).ToList();
                    var getSchedule = await omniChannelScheduleRepository.GetScheduleDtoByIds(schedulePaging);
                    sheduleDb.AddRange(getSchedule);
                }
            }
            return sheduleDb;
        }
        #endregion
    }
}
﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Helper;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Models;
using Demo.OmniChannel.Utilities;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Schedule.Jobs
{
    public class SyncRetryInvoiceJobImpls
    {
        [DisallowConcurrentExecution]
        public class ShopeeSyncSyncRetryInvoiceJob : BaseSyncRetryInvoiceJob
        {
            private readonly IChannelBusiness _channelBusiness;
            public ShopeeSyncSyncRetryInvoiceJob(IConfiguration config,
                ICacheClient cacheClient,
                IDbConnectionFactory dbConnectionFactory,
                IMessageFactory messageFactory,
                IRetryInvoiceMongoService retryInvoiceMongoService,
                 IChannelBusiness channelBusiness,
                  IAppSettings appSettings,
                IScheduleService scheduleService) : base(config,
                cacheClient,
                dbConnectionFactory,
                messageFactory,
                retryInvoiceMongoService,
                scheduleService)
            {
                ChannelType = (byte)ShareKernel.Common.ChannelType.Shopee;
                _ = new SelfAppConfig(appSettings);
                _channelBusiness = channelBusiness;
            }

            public async override Task PushRedisMessageQueue(RetryInvoice invoice, Guid logId)
            {
                var syncRetryOrderJobLog = new LogObject(Logger, logId)
                {
                    Action = $"{EnumHelper.ToDescription((ChannelType)ChannelType)}_{nameof(BaseSyncRetryOrderJob)}_SendWebhookShopee"
                };
                try
                {
                    if (invoice == null)
                    {
                        syncRetryOrderJobLog.Description = "Order retry is null";
                        return;
                    }
                    var channel = await _channelBusiness.GetChannel(invoice.ChannelId, invoice.RetailerId, logId);
                    if (channel == null)
                    {
                        syncRetryOrderJobLog.Description = "Channel is null !!";
                        return;
                    }
                    var req = new ShopeeWebhookModel
                    {
                        ShopId = ConvertHelper.ToInt64(channel.IdentityKey),
                        Code = 3,
                        Timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds(),
                        Data = new DataBody
                        {
                            Ordersn = invoice.OrderId,
                            Status = "READY_TO_SHIP",
                            UpdateTime = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds()
                        }
                    };
                    await ShopeeHelperV2.SendToWebHookAsync(req);
                    syncRetryOrderJobLog.Description = $"Push webhook retry success orderid {invoice.OrderId}";
                    syncRetryOrderJobLog.LogInfo();
                }
                catch (Exception ex)
                {
                    syncRetryOrderJobLog.LogError(ex);
                }
            }
        }
        [DisallowConcurrentExecution]
        public class LazadaSyncSyncRetryInvoiceJob : BaseSyncRetryInvoiceJob
        {
            public LazadaSyncSyncRetryInvoiceJob(IConfiguration config,
                ICacheClient cacheClient,
                IDbConnectionFactory dbConnectionFactory,
                IMessageFactory messageFactory,
                IRetryInvoiceMongoService retryInvoiceMongoService,
                IScheduleService scheduleService) : base(config,
                cacheClient,
                dbConnectionFactory,
                messageFactory,
                retryInvoiceMongoService,
                scheduleService)
            {
                ChannelType = (byte)ShareKernel.Common.ChannelType.Lazada;
            }
        }
        [DisallowConcurrentExecution]
        public class TikiSyncSyncRetryInvoiceJob : BaseSyncRetryInvoiceJob
        {
            public TikiSyncSyncRetryInvoiceJob(IConfiguration config,
                ICacheClient cacheClient,
                IDbConnectionFactory dbConnectionFactory,
                IMessageFactory messageFactory,
                IRetryInvoiceMongoService retryInvoiceMongoService,
                IScheduleService scheduleService) : base(config,
                cacheClient,
                dbConnectionFactory,
                messageFactory,
                retryInvoiceMongoService,
                scheduleService)
            {
                ChannelType = (byte)ShareKernel.Common.ChannelType.Tiki;
            }
        }
        [DisallowConcurrentExecution]
        public class SendoSyncSyncRetryInvoiceJob : BaseSyncRetryInvoiceJob
        {
            public SendoSyncSyncRetryInvoiceJob(IConfiguration config,
                ICacheClient cacheClient,
                IDbConnectionFactory dbConnectionFactory,
                IMessageFactory messageFactory,
                IRetryInvoiceMongoService retryInvoiceMongoService,
                IScheduleService scheduleService) : base(config,
                cacheClient,
                dbConnectionFactory,
                messageFactory,
                retryInvoiceMongoService,
                scheduleService)
            {
                ChannelType = (byte)ShareKernel.Common.ChannelType.Sendo;
            }
        }

        [DisallowConcurrentExecution]
        public class TiktokSyncSyncRetryInvoiceJob : BaseSyncRetryInvoiceJob
        {
            public TiktokSyncSyncRetryInvoiceJob(IConfiguration config,
                ICacheClient cacheClient,
                IDbConnectionFactory dbConnectionFactory,
                IMessageFactory messageFactory,
                IRetryInvoiceMongoService retryInvoiceMongoService,
                IScheduleService scheduleService) : base(config,
                cacheClient,
                dbConnectionFactory,
                messageFactory,
                retryInvoiceMongoService,
                scheduleService)
            {
                ChannelType = (byte)ShareKernel.Common.ChannelType.Tiktok;
            }
        }
    }
}
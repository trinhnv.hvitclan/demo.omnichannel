﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Schedule.Common;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Schedule.Jobs
{
    [DisallowConcurrentExecution]
    public class SyncErrorInvoiceJob : BaseJob
    {
        private readonly IMessageFactory _messageFactory;
        private ILog _logger => LogManager.GetLogger(typeof(SyncErrorInvoiceJob));
        private readonly IInvoiceMongoService _invoiceMongoService;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly IAppSettings _settings;

        public SyncErrorInvoiceJob(
            IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IScheduleService scheduleService,
            IMessageFactory messageFactory,
            IAppSettings settings,
            IInvoiceMongoService invoiceMongoService) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService)
        {
            _messageFactory = messageFactory;
            _dbConnectionFactory = dbConnectionFactory;
            _settings = settings;
            _invoiceMongoService = invoiceMongoService;
        }

        public override async Task Execute(IJobExecutionContext context)
        {
            var logId = Guid.NewGuid();
            var log = new LogObject(_logger, logId)
            {
                Action = $"{nameof(SyncErrorInvoiceJob)}"
            };
            try
            {
                var getErrorfilter = _settings.Get<SyncInvoiceError>("SyncInvoiceError");
                var kvQuartz = new KvQuartz();
                Configuration.GetSection("Quartz").Bind(kvQuartz);
                var jobItem = (kvQuartz?.JobChannels).FirstOrDefault(t => t.JobName.Contains(nameof(SyncErrorInvoiceJob)));
                var fromDate = getErrorfilter.FromDate != null ? getErrorfilter.FromDate.Value : DateTime.Now.AddDays(-1);
                var toDate = getErrorfilter.ToDate != null ? getErrorfilter.ToDate.Value : DateTime.Now;
                var getInvoice = await _invoiceMongoService.GetInvoiceByErrorMessage(getErrorfilter.ErrorScan, fromDate, toDate,
                    jobItem.IncludeRetail, jobItem.ExcludeRetail);
                var channels = await GetChannels(getInvoice.Select(x => x.ChannelId).Distinct().ToList());
                await HandleErrorInvoiceMessage(channels, getInvoice);
                log.RequestObject = $"From date:{fromDate}, To date: {toDate}";
                log.ResponseObject = $"Push order to invoice {string.Join(",", getInvoice.Select(x => x.Code))}";
                log.LogInfo();
            }
            catch (Exception ex)
            {
                log.LogError(ex);
            }
        }


        private async Task HandleErrorInvoiceMessage(List<Domain.Model.OmniChannel> channels,
           List<MongoDb.Invoice> invoices)
        {
            using var mq = _messageFactory.CreateMessageProducer();
            var retailerIdLst = invoices.Select(item => item.RetailerId).Distinct().ToList();
            if (retailerIdLst.Count > 0)
            {
                foreach (var retailerId in retailerIdLst)
                {
                    var invoiceByRetailerIdLst = invoices.Where(item => item.RetailerId == retailerId).ToList();
                    var kvContext = await CreateContext(retailerId);
                    if(kvContext == null)
                    {
                        continue;
                    }
                    foreach (var invoice in invoiceByRetailerIdLst)
                    {
                        var channel = channels.FirstOrDefault(x => x.Id == invoice.ChannelId);
                        if (channel != null)
                        {
                            PushMessageToChannel(mq, channel, invoice, kvContext);
                        }
                    }
                }
            }
        }

        private void PushMessageToChannel(IMessageProducer messageProducer, Domain.Model.OmniChannel channel,
            MongoDb.Invoice invoice, Domain.Common.KvInternalContext kvContext)
        {
            switch (channel.Type)
            {
                case (byte)ChannelType.Lazada:
                    {
                        var message = new LazadaSyncErrorInvoiceMessage
                        {
                            KvEntities = kvContext.Group?.ConnectionString,
                            Invoice = invoice.ToSafeJson(),
                            RetailerId = channel.RetailerId,
                            BranchId = invoice.BranchId,
                            ChannelId = invoice.ChannelId,
                            PlatformId = channel.PlatformId
                        };
                        messageProducer.Publish(message);
                        break;
                    }

                case (byte)ChannelType.Shopee:
                    {
                        var message = new ShopeeSyncErrorInvoiceMessage
                        {
                            KvEntities = kvContext.Group?.ConnectionString,
                            Invoice = invoice.ToSafeJson(),
                            RetailerId = channel.RetailerId,
                            BranchId = invoice.BranchId,
                            ChannelId = invoice.ChannelId,
                            PlatformId = channel.PlatformId
                        };
                        messageProducer.Publish(message);

                        break;
                    }

                case (byte)ChannelType.Tiktok:
                    {
                        var message = new TiktokSyncErrorInvoiceMessage
                        {
                            KvEntities = kvContext.Group?.ConnectionString,
                            Invoice = invoice.ToSafeJson(),
                            RetailerId = channel.RetailerId,
                            BranchId = invoice.BranchId,
                            ChannelId = invoice.ChannelId,
                            PlatformId = channel.PlatformId
                        };
                        messageProducer.Publish(message);

                        break;
                    }

                case (byte)ChannelType.Tiki:
                    {
                        var message = new TikiSyncErrorInvoiceMessage
                        {
                            KvEntities = kvContext.Group?.ConnectionString,
                            Invoice = invoice.ToSafeJson(),
                            RetailerId = channel.RetailerId,
                            BranchId = invoice.BranchId,
                            ChannelId = invoice.ChannelId,
                            PlatformId = channel.PlatformId
                        };
                        messageProducer.Publish(message);

                        break;
                    }

                case (byte)ChannelType.Sendo:
                    {
                        var message = new SendoSyncErrorInvoiceMessage
                        {
                            KvEntities = kvContext.Group?.ConnectionString,
                            Invoice = invoice.ToSafeJson(),
                            RetailerId = channel.RetailerId,
                            BranchId = invoice.BranchId,
                            ChannelId = invoice.ChannelId,
                            PlatformId = channel.PlatformId
                        };
                        messageProducer.Publish(message);

                        break;
                    }
            }
        }

        private async Task<List<Domain.Model.OmniChannel>> GetChannels(List<long> channelIds)
        {
            var channels = new List<Domain.Model.OmniChannel>();
            using (var db = _dbConnectionFactory.OpenDbConnection())
            {
                var omniChannelRespo = new OmniChannelRepository(db);
                var totalPages = (int)Math.Ceiling(channelIds.Count / (decimal)1000);
                for (int page = 1; page <= totalPages; page++)
                {
                    var channelPages = channelIds.Page(page, 1000).ToList();
                    var getChannel = await omniChannelRespo.GetByIdsAsync(channelPages);
                    channels.AddRange(getChannel);
                }
            }
            return channels;
        }
    }
}
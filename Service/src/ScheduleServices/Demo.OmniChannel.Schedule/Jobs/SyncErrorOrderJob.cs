﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Helper;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Schedule.Common;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.ShareKernel.Models;
using Demo.OmniChannel.Utilities;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Schedule.Jobs
{
    [DisallowConcurrentExecution]
    public class SyncErrorOrderJob : BaseJob
    {
        private readonly IMessageFactory _messageFactory;
        private ILog _logger => LogManager.GetLogger(typeof(SyncErrorInvoiceJob));
        private readonly IOrderMongoService _orderMongoService;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly IAppSettings _settings;

        public SyncErrorOrderJob(
            IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IScheduleService scheduleService,
            IMessageFactory messageFactory,
            IAppSettings settings,
            IOrderMongoService orderMongoService) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService)
        {
            _messageFactory = messageFactory;
            _dbConnectionFactory = dbConnectionFactory;
            _settings = settings;
            _orderMongoService = orderMongoService;
        }

        public override async Task Execute(IJobExecutionContext context)
        {
            var logId = Guid.NewGuid();
            var log = new LogObject(_logger, logId)
            {
                Action = $"{nameof(SyncErrorOrderJob)}"
            };
            try
            {
                var getErrorfilter = _settings.Get<SyncOrderError>("SyncOrderError");
                if (getErrorfilter == null)
                {
                    log.LogWarning("SyncOrderError config is null");
                    return;
                }
                var kvQuartz = new KvQuartz();
                Configuration.GetSection("Quartz").Bind(kvQuartz);
                var jobItem = (kvQuartz?.JobChannels).FirstOrDefault(t => t.JobName.Contains(nameof(SyncErrorOrderJob)));
                var fromDate = getErrorfilter.FromDate != null ? getErrorfilter.FromDate.Value : DateTime.Now.AddDays(-1);
                var toDate = getErrorfilter.ToDate != null ? getErrorfilter.ToDate.Value : DateTime.Now;
                var erorOrderLst = await _orderMongoService.GetOrderByErrorMessage(getErrorfilter.ErrorScan, fromDate, toDate,
                    jobItem.IncludeRetail, jobItem.ExcludeRetail);
                var channels = await GetChannels(erorOrderLst.Select(x => x.ChannelId).Distinct().ToList());
                await HandleErrorOrderMessage(channels, erorOrderLst, logId);
                log.RequestObject = $"From date:{fromDate}, To date: {toDate}";
                log.ResponseObject = $"Push order to order {string.Join(",", erorOrderLst.Select(x => x.Code))}";
                log.LogInfo();
            }
            catch (Exception ex)
            {
                log.LogError(ex);
            }
        }

        private async Task HandleErrorOrderMessage(List<Domain.Model.OmniChannel> channels,
           List<MongoDb.Order> orderLst, Guid logId)
        {
            using var mq = _messageFactory.CreateMessageProducer();
            var retailerIdLst = orderLst.Select(item => item.RetailerId).Distinct().ToList();
            if (retailerIdLst.Count > 0)
            {
                foreach (var retailerId in retailerIdLst)
                {
                    var orderByRetailerIdLst = orderLst.Where(item => item.RetailerId == retailerId).ToList();
                    var kvContext = await CreateContext(retailerId);
                    if (kvContext == null)
                    {
                        continue;
                    }
                    foreach (var order in orderByRetailerIdLst)
                    {
                        var channel = channels.FirstOrDefault(x => x.Id == order.ChannelId);
                        if (channel != null)
                        {
                            PushMessageToChannel(mq, channel, order, kvContext, logId);
                        }
                    }
                }
            }
        }

        private async void PushMessageToChannel(IMessageProducer messageProducer, Domain.Model.OmniChannel channel,
            MongoDb.Order order, Domain.Common.KvInternalContext kvContext, Guid logId)
        {
            switch (channel.Type)
            {
                case (byte)ChannelType.Lazada:
                    {
                        var message = new LazadaSyncErrorOrderMessage
                        {
                            KvEntities = kvContext.Group?.ConnectionString,
                            Order = order.ToSafeJson(),
                            RetailerId = channel.RetailerId,
                            BranchId = order.BranchId,
                            ChannelId = order.ChannelId,
                            PlatformId = channel.PlatformId
                        };
                        messageProducer.Publish(message);
                        break;
                    }

                case (byte)ChannelType.Shopee:
                    {
                        await PushShopeeMessageQueue(order, channel, logId);
                        break;
                    }

                case (byte)ChannelType.Tiktok:
                    {
                        await PushTiktokMessageQueue(order, channel, logId);
                        break;
                    }

                case (byte)ChannelType.Tiki:
                    {
                        var message = new TikiSyncErrorOrderMessage
                        {
                            KvEntities = kvContext.Group?.ConnectionString,
                            Order = order.ToSafeJson(),
                            RetailerId = channel.RetailerId,
                            BranchId = order.BranchId,
                            ChannelId = order.ChannelId,
                            PlatformId = channel.PlatformId
                        };
                        messageProducer.Publish(message);

                        break;
                    }

                case (byte)ChannelType.Sendo:
                    {
                        var message = new SendoSyncErrorOrderMessage
                        {
                            KvEntities = kvContext.Group?.ConnectionString,
                            Order = order.ToSafeJson(),
                            RetailerId = channel.RetailerId,
                            BranchId = order.BranchId,
                            ChannelId = order.ChannelId,
                            PlatformId = channel.PlatformId
                        };
                        messageProducer.Publish(message);

                        break;
                    }
            }
        }

        public async Task PushTiktokMessageQueue(MongoDb.Order order, Domain.Model.OmniChannel channel, Guid logId)
        {

            var syncRetryOrderJobLog = new LogObject(Logger, logId)
            {
                Action = $"{nameof(SyncErrorOrderJob)}_SendWebhookTiktok"
            };
            try
            {
                if (order == null)
                {
                    return;
                }
                var req = new TiktokWebhookModel
                {
                    ShopId = channel.IdentityKey,
                    Timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds(),
                    Data = new DataOrder
                    {
                        Ordersn = order.OrderId,
                        OrderStatus = "111",
                        UpdateTime = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds()
                    }
                };
                await TikTokHelper.SendToWebHookAsync(req);
                syncRetryOrderJobLog.Description = $"Push webhook error order success orderid {order.OrderId}";
                syncRetryOrderJobLog.LogInfo();
            }
            catch (Exception ex)
            {
                syncRetryOrderJobLog.LogError(ex);
            }
        }

        public async Task PushShopeeMessageQueue(MongoDb.Order order, Domain.Model.OmniChannel channel, Guid logId)
        {
            var syncRetryOrderJobLog = new LogObject(Logger, logId)
            {
                Action = $"{nameof(SyncErrorOrderJob)}_SendWebhookShope"
            };
            try
            {
                if (order == null)
                {
                    return;
                }
                var req = new ShopeeWebhookModel
                {
                    ShopId = ConvertHelper.ToInt64(channel.IdentityKey),
                    Code = 3,
                    Timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds(),
                    Data = new DataBody
                    {
                        Ordersn = order.OrderId,
                        Status = "READY_TO_SHIP",
                        UpdateTime = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds()
                    }
                };
                await ShopeeHelperV2.SendToWebHookAsync(req);
                syncRetryOrderJobLog.Description = $"Push webhook retry order success orderid {order.OrderId}";
                syncRetryOrderJobLog.LogInfo();
            }
            catch (Exception ex)
            {
                syncRetryOrderJobLog.LogError(ex);
            }
        }

        private async Task<List<Domain.Model.OmniChannel>> GetChannels(List<long> channelIds)
        {
            var channels = new List<Domain.Model.OmniChannel>();
            using (var db = _dbConnectionFactory.OpenDbConnection())
            {
                var omniChannelRespo = new OmniChannelRepository(db);
                var totalPages = (int)Math.Ceiling(channelIds.Count / (decimal)1000);
                for (int page = 1; page <= totalPages; page++)
                {
                    var channelPages = channelIds.Page(page, 1000).ToList();
                    var getChannel = await omniChannelRespo.GetByIdsAsync(channelPages);
                    channels.AddRange(getChannel);
                }
            }
            return channels;
        }
    }
}
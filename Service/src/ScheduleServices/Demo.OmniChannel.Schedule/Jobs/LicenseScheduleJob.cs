﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Schedule.Common;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Impls;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Schedule.Jobs
{
    public class LicenseScheduleJob : BaseJob
    {
        public IAuditTrailInternalClient AuditTrailInternalClient { get; set; }
        private readonly IAppSettings AppSettings;
        private const int MaxChannelTrialRetailer = 5;
        private const string Action_Pos_Parameter = "LicenseScheduleJobPosParameter";
        public LicenseScheduleJob(IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IAppSettings settings,
            IScheduleService scheduleService
            ) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService)
        {
            AppSettings = settings;
        }

        public async override Task Execute(IJobExecutionContext context)
        {
            var logId = Guid.NewGuid();
            var logObj = new LogObject(Logger, logId)
            {
                Action = $"{nameof(LicenseScheduleJob)}"
            };

            try
            {
                List<Domain.Model.OmniChannel> listDeactivateChannel = new List<Domain.Model.OmniChannel>();
                List<Domain.Model.OmniChannel> activeChannels = null;
                var configDeactivateChannelFeature = AppSettings?.Get<DeactivateChannelFeature>("DeactivateChannelFeature");
                List<KvRetailerExpiry> listRetailer = new List<KvRetailerExpiry>();
                List<string> listDeactivateRetailer = new List<string>();

                using (var db = DbConnectionFactory.OpenDbConnection())
                {
                    var omniChannelRepository = new OmniChannelRepository(db);
                    var kvQuartz = new KvQuartz();
                    Configuration.GetSection("Quartz").Bind(kvQuartz);
                    var jobItem = (kvQuartz?.JobChannels).FirstOrDefault(t => t.JobName.Contains(nameof(LicenseScheduleJob)));
                    activeChannels = await omniChannelRepository.GetAllChannels(configDeactivateChannelFeature?.IncludeRetail, configDeactivateChannelFeature?.ExcludeRetailer);
                }
                var groupChannelByRetailer = activeChannels.GroupBy(t => t.RetailerId);

                foreach (var group in groupChannelByRetailer)
                {
                    var kvRetailer = await GetRetailer(group.Key);
                    if (kvRetailer == null) continue;
                    var kvGroup = await GetRetailerGroup(kvRetailer?.GroupId ?? 0);

                    var omniPosParameters = await GetPosParametersByRetailersAsync(kvRetailer?.Id ?? 0, nameof(PosSetting.OmniChannel), kvGroup);

                    var isExpiredLicense = IsExpiredLicense(omniPosParameters, kvRetailer);
                    var visibleChannels = group.ToList();
                    var isExpiredLicenseTMDT = IsExpiredLicenseTMDT(omniPosParameters, isExpiredLicense, visibleChannels);
                    if (isExpiredLicenseTMDT)
                    {
                        listRetailer.Add(new KvRetailerExpiry()
                        {
                            RetailerId = kvRetailer?.Id ?? 0,
                            Name = kvRetailer.Code,
                            ExpiryDateRetailer = kvRetailer.ExpiryDate.Value.ToString("dd-MM-yyyy"),
                            ExpiryDateTMDT = omniPosParameters.ExpiredDate != null ? omniPosParameters.ExpiredDate.Value.ToString("dd-MM-yyyy") : string.Empty,
                            IsActiveOmniChannel = omniPosParameters.isActive,
                        });
                    }

                    if (configDeactivateChannelFeature != null && configDeactivateChannelFeature.Enable && isExpiredLicenseTMDT)
                    {
                        #region New Trial Retailer after KOL release
                        var isExpiredTrialRetailer = IsExpiredTrialRetailer(kvRetailer);
                        if (isExpiredTrialRetailer)
                        {
                            listDeactivateRetailer.Add(kvRetailer.Code);
                            listDeactivateChannel.AddRange(visibleChannels);
                            await DeactivateChannel(visibleChannels, kvRetailer, kvGroup);
                            continue;
                        }
                        #endregion

                        listDeactivateRetailer.Add(kvRetailer.Code);
                        listDeactivateChannel.AddRange(visibleChannels);
                        await DeactivateChannel(visibleChannels, kvRetailer, kvGroup);
                    }
                }
                logObj.ResponseObject = new { Result = $" Retailers: {listRetailer.Select(x => x.Name).Join(", ")} RetailerDeactivate: {listRetailer.ToSafeJson()} DeactivateRetailers: {listDeactivateRetailer.Join(", ")} DeactivateChannels: {listDeactivateChannel.Select(x => x.Id).ToJson()}" };
                logObj.LogInfo();
            }
            catch (Exception ex)
            {
                logObj.LogError(ex);
                throw;
            }

        }

        #region PRIVATE METHOD

        public async Task<List<Domain.Model.OmniChannel>> GetAllActiveChannels()
        {
            using (var dbMaster = DbConnectionFactory.Open(nameof(KvMasterEntities)))
            {
                var omniChannels = await dbMaster.SelectAsync<Domain.Model.OmniChannel>(x => x.IsDeleted == null || x.IsDeleted == false);
                if (omniChannels != null)
                {
                    return omniChannels;
                }
                return null;
            }
        }
        private async Task<PosParameter> GetPosParametersByRetailersAsync(int retailerId, string keyPosparameter, KvGroup kvGroup)
        {
            if (kvGroup == null) return null;
            var connectionString = kvGroup.ConnectionString.Substring(kvGroup.ConnectionString.IndexOf('=') + 1)?.ToLower();
            try
            {
                using (var dbMaster = DbConnectionFactory.Open(connectionString))
                {
                    var posParameters = await dbMaster
                        .SingleAsync<PosParameter>(t => t.RetailerId == retailerId && t.Key == keyPosparameter);
                    return posParameters;
                }
            }
            catch (Exception e)
            {
                var logId = Guid.NewGuid();
                var log = new LogObject(Logger, logId)
                {
                    Action = Action_Pos_Parameter,
                    RetailerId = retailerId,
                    Description = $"Connection Error To {connectionString}",
                };
                log.LogError(e);
                return null;
            }
        }
        private bool IsExpiredLicenseTMDT(PosParameter posParameter, bool IsExpiredLicense, List<Domain.Model.OmniChannel> channels)
        {
            if (channels.Count > 0 && posParameter != null && (IsExpiredLicense || !posParameter.isActive))
            {
                return true;
            }
            return false;
        }
        private bool IsExpiredLicense(PosParameter posParameter, KvRetailer retailer)
        {
            var today = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day);
            DateTime expiryDate;
            if (posParameter?.ExpiredDate == default(DateTime) || posParameter?.ExpiredDate == null)
                expiryDate = (DateTime)retailer.ExpiryDate;
            else
                expiryDate = (DateTime)posParameter.ExpiredDate;
            if (expiryDate >= today)
                return false;
            return true;
        }

        private bool IsExpiredTrialRetailer(KvRetailer kvRetailer)
        {
            var today = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day);
            if (kvRetailer.ContractType == 0 && kvRetailer.ExpiryDate <= today)
            {
                return true;
            }
            return false;
        }

        private async Task DeactivateChannel(List<Domain.Model.OmniChannel> channels, KvRetailer kvRetailer, KvGroup kvGroup)
        {
            if (channels == null || channels.Count == 0) return;
            foreach (var channel in channels)
            {
                var connectionString = kvGroup.ConnectionString.Substring(kvGroup.ConnectionString.IndexOf('=') + 1)?.ToLower();
                var auditContext = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, kvRetailer.Id, connectionString, channel.BranchId, null);
                var coreContext = auditContext.ConvertTo<KvInternalContext>();
                coreContext.UserId = auditContext.User?.Id ?? 0;
                var internalClient = new IntegrationInternalClient(AppSettings);
                await internalClient.DeactivateChannel(coreContext, channel.Id, EnumHelper.ToDescription((Sdk.Common.ChannelType)channel.Type));

            }
        }
        public class KvRetailerExpiry
        {
            public long RetailerId { get; set; }
            public string Name { get; set; }
            public string ExpiryDateTMDT { get; set; }
            public string ExpiryDateRetailer { get; set; }
            public bool IsActiveOmniChannel { get; set; }
        }

        #endregion

    }
}

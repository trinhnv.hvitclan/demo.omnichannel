﻿using System;
using System.Data;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Data;
using System.Threading.Tasks;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using ServiceStack.Caching;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Schedule.Jobs
{
    [DisallowConcurrentExecution]
    public class BaseResetScheduleJob : BaseJob
    {
        protected byte ChannelType { get; set; }
        public BaseResetScheduleJob(IConfiguration config,
                                      ICacheClient cacheClient,
                                      IDbConnectionFactory dbConnectionFactory,
                                      IScheduleService scheduleService) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService)
        {
        }

        public void ProcessResetSchedule()
        {
            var logObject = new LogObject(Logger, Guid.NewGuid())
            {
                Action = $"{EnumHelper.ToDescription((ChannelType) ChannelType)}_ResetSchedule"
            };
            var totalItem = 0;

            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                using (var scheduleRepo = new OmniChannelScheduleRepository(db))
                {
                    var orderTask = RunWithScheduleType(scheduleRepo, ScheduleType.SyncOrder);
                    orderTask.Wait();

                    var productTask = RunWithScheduleType(scheduleRepo, ScheduleType.SyncModifiedProduct);
                    productTask.Wait();
                }
            }

            logObject.TotalItem = totalItem;
            logObject.LogInfo();

            // Local function
            async Task RunWithScheduleType(OmniChannelScheduleRepository scheduleRepo, ScheduleType scheduleType)
            {
                var scheduleTotal = await scheduleRepo.GetTotal(ChannelType, (byte) scheduleType, null, null);
                var pageSize = 1000;
                var totalPage = Math.Ceiling((double) scheduleTotal / pageSize);

                for (var i = 0; i < totalPage; i++)
                {
                    var scheduleIds = await scheduleRepo.GetIds(ChannelType, (byte) scheduleType, null, null, i * pageSize, pageSize);
                    var jobInCache = ScheduleService.GetByIdsCoverDeserializeError(scheduleIds);

                    if (jobInCache?.Any() != true) continue;

                    var runningSchedules = jobInCache.Where(p => p.IsRunning).ToList();
                    foreach (var schedule in runningSchedules)
                    {
                        schedule.IsRunning = false;
                    }

                    if (runningSchedules?.Any() != true) continue;

                    totalItem += runningSchedules.Count;
                    ScheduleService.AddOrUpdateBatch(ChannelType, (byte)scheduleType, runningSchedules);
                }
            }
        }
        public override async Task Execute(IJobExecutionContext context)
        {
            if (!IsValidMaintenanceTime(this.GetType().Name)) return;

            await Task.Run(() => ProcessResetSchedule());
        }
    }
}

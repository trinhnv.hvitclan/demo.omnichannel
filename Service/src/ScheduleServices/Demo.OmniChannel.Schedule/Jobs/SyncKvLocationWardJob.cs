﻿using System;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Schedule.Jobs
{
    [DisallowConcurrentExecution]
    public class SyncKvLocationWardJob : BaseJob
    {
        public SyncKvLocationWardJob(IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IScheduleService scheduleService) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService)
        {
        }

        public override async Task Execute(IJobExecutionContext context)
        {
            if (!IsValidMaintenanceTime(this.GetType().Name)) return;
            
            var logObject = new LogObject(Logger, Guid.NewGuid())
            {
                Action = "SyncKvLocationAndWard"
            };
            using (var connection = DbConnectionFactory.OpenDbConnection())
            {
                var omniChannelRepo = new OmniChannelRepository(connection);
                var channel = await omniChannelRepo.SingleAsync(x => x.IsActive && (x.IsDeleted == null || x.IsDeleted == false));
                if (channel == null)
                {
                    return;
                }
                var kvRetailer = await GetRetailer(channel.RetailerId);
                if (kvRetailer == null)
                {
                    return;
                }
                var kvGroup = await GetRetailerGroup(kvRetailer.GroupId);
                if (kvGroup == null)
                {
                    return;
                }

                using (var db = DbConnectionFactory.Open(kvGroup.ConnectionString.Substring(kvGroup.ConnectionString.IndexOf('=') + 1)?.ToLower()))
                {
                    using (var trans = db.OpenTransaction(IsolationLevel.ReadUncommitted))
                    {
                        var location = await db.SelectAsync<Location>();
                        var values = location.Select(p => new { Key = Regex.Replace(p.NormalName, @"\s+", ""), Value = p });
                        var now = DateTime.Now;
                        var kvCacheClient = ((IKvCacheClient)CacheClient);
                        kvCacheClient.InsertAllByPipeLine(values.GroupBy(x => x.Key).Select(x => x.FirstOrDefault())
                            .ToDictionary(x => string.Format(KvConstant.LocationCacheKey, x.Key), y => y.Value), (TimeSpan)(now.AddYears(1) - now));
                        var wards = await db.SelectAsync<Wards>();
                        var wardsInsert = wards.Select(p => new { Key = Regex.Replace(p.NormalName, @"\s+", ""), ParentItemId = p.ParentId, Value = p });
                        kvCacheClient.InsertAllByPipeLine(wardsInsert.GroupBy(x => x.Key).Select(x => x.FirstOrDefault())
                            .ToDictionary(x => string.Format(KvConstant.WardCacheKey, x.ParentItemId, x.Key), y => y.Value), (TimeSpan)(now.AddYears(1) - now));
                    }
                }
            }
            logObject.LogInfo();
        }
    }
}
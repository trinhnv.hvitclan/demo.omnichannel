﻿using Demo.OmniChannel.ScheduleService.Interfaces;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.Schedule.Jobs
{
    [DisallowConcurrentExecution]
    public class ShopeeSyncPaymentTransactionJob : BaseSyncPaymentTransactionJob
    {
        public ShopeeSyncPaymentTransactionJob(IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IScheduleService scheduleService,
            IMessageFactory messageFactory) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService,
            messageFactory)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Shopee;
        }
    }
}

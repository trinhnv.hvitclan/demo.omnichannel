﻿using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.Utilities;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Schedule.Jobs
{
    [DisallowConcurrentExecution]
    public class SyncSellerStatusJob : BaseJob
    {
        private IAppSettings _settings;
        public SyncSellerStatusJob(IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IAppSettings settings,
            IScheduleService scheduleService) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService)
        {
            _settings = settings;
        }

        public override async Task Execute(IJobExecutionContext context)
        {
            if (!IsValidMaintenanceTime(this.GetType().Name)) return;

            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                var logId = Guid.NewGuid();
                var logObject = new LogObject(Logger, logId)
                {
                    Action = "SyncSellerStatus"
                };
                var pendingChannel =
                    await db.LoadSelectAsync<Domain.Model.OmniChannel>(x => x.Status == (byte) ChannelStatusType.Pending && x.Type == (byte)ChannelType.Tiki);
                var channelClient = new ChannelClient.Impls.ChannelClient();
                var tikiClient = channelClient.GetClient((byte) ChannelType.Tiki, _settings);
                foreach (var item in pendingChannel)
                {
                    var logErrorObj = new LogObject(Logger, logId)
                    {
                        Action = "EnableTikiChannel"
                    };
                    try
                    {
                        if (item.OmniChannelAuth == null || string.IsNullOrEmpty(item.OmniChannelAuth?.AccessToken))
                        {
                            continue;
                        }

                        logErrorObj.RequestObject = $"{CryptoHelper.PassPhrase} - {item.OmniChannelAuth.AccessToken}";

                        var accessToken = CryptoHelper.RijndaelDecrypt(item.OmniChannelAuth.AccessToken);
                        var shopInfo = await tikiClient.GetShopStatus(accessToken);
                        if (shopInfo.Status != (byte)ChannelStatusType.Approved)
                        {
                            continue;
                        }

                        if (shopInfo.Active == 1)
                        {
                            item.IsActive = true;
                            item.Status = (byte)ChannelStatusType.Approved;
                            await db.UpdateOnlyAsync(item, onlyFields: p => new[] { "IsActive", "Status" }, where: x => x.Id == item.Id);
                            await tikiClient.EnableUpdateProduct(accessToken);

                            logErrorObj.ResponseObject = item;
                            logErrorObj.Description = "Enable Tiki channel successful";
                            logErrorObj.LogInfo();
                        }
                    }
                    catch (Exception e)
                    {
                        logErrorObj.LogError(e);
                    }
                }

                logObject.TotalItem = pendingChannel?.Count;
                logObject.LogInfo();
            }
        }
    }
}
﻿using Demo.OmniChannel.ScheduleService.Interfaces;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Data;

namespace Demo.OmniChannel.Schedule.Jobs
{
    public class DailySyncJobFromDbImpls
    {
        [DisallowConcurrentExecution]
        public class LazadaDailySyncJobFromDb : BaseDailySyncJobFromDbJob
        {
            public LazadaDailySyncJobFromDb(IConfiguration config,
                ICacheClient cacheClient,
                IDbConnectionFactory dbConnectionFactory,
                IScheduleService scheduleService) : base(config,
                cacheClient, dbConnectionFactory, scheduleService)
            {
                ChannelType = (byte)ShareKernel.Common.ChannelType.Lazada;
            }
        }
        [DisallowConcurrentExecution]
        public class ShopeeDailySyncJobFromDb : BaseDailySyncJobFromDbJob
        {
            public ShopeeDailySyncJobFromDb(IConfiguration config,
                ICacheClient cacheClient,
                IDbConnectionFactory dbConnectionFactory,
                IScheduleService scheduleService) : base(config,
                cacheClient,
                dbConnectionFactory, scheduleService)
            {
                ChannelType = (byte)ShareKernel.Common.ChannelType.Shopee;
            }
        }
        [DisallowConcurrentExecution]
        public class TikiDailySyncJobFromDb : BaseDailySyncJobFromDbJob
        {
            public TikiDailySyncJobFromDb(IConfiguration config,
                ICacheClient cacheClient,
                IDbConnectionFactory dbConnectionFactory,
                IScheduleService scheduleService) : base(config,
                cacheClient,
                dbConnectionFactory, scheduleService)
            {
                ChannelType = (byte)ShareKernel.Common.ChannelType.Tiki;
            }
        }
        [DisallowConcurrentExecution]
        public class SendoDailySyncJobFromDb : BaseDailySyncJobFromDbJob
        {
            public SendoDailySyncJobFromDb(IConfiguration config,
                ICacheClient cacheClient,
                IDbConnectionFactory dbConnectionFactory,
                IScheduleService scheduleService) : base(config,
                cacheClient,
                dbConnectionFactory, scheduleService)
            {
                ChannelType = (byte)ShareKernel.Common.ChannelType.Sendo;
            }
        }
    }
}
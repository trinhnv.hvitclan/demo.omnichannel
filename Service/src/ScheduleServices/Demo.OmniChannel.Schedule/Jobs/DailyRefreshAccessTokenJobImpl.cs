﻿using Demo.OmniChannel.Schedule.Common;
using Demo.OmniChannel.Utilities;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System.Collections.Generic;
using System.Linq;

namespace Demo.OmniChannel.Schedule.Jobs
{
    [DisallowConcurrentExecution]
    public class TikiDailyRefreshAccessTokenJob : BaseDailyRefreshAccessTokenJob
    {
        public TikiDailyRefreshAccessTokenJob(IConfiguration config, ICacheClient cacheClient, IDbConnectionFactory dbConnectionFactory, IMessageFactory messageFactory) : base(cacheClient, dbConnectionFactory, messageFactory)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Tiki;
            var kvQuartz = new KvQuartz();
            config.GetSection("Quartz").Bind(kvQuartz);
            var jobItem = (kvQuartz?.JobChannels).FirstOrDefault(t => t.ChannelType == ChannelType && t.JobName.Contains(nameof(TikiDailyRefreshAccessTokenJob)));

            RefreshBeforeExpiryMinutes = NumberHelper.GetValueOrDefault(jobItem.RefreshTokenBeforeMinutes, 1440);

            IncludeRetailerIds = jobItem?.IncludeRetail?.ToList() ?? new List<int>();
            ExcludeRetailerIds = jobItem?.ExcludeRetail?.ToList() ?? new List<int>();
        }
    }


    [DisallowConcurrentExecution]
    public class LazadaDailyRefreshAccessTokenJob : BaseDailyRefreshAccessTokenJob
    {
        public LazadaDailyRefreshAccessTokenJob(IConfiguration config, ICacheClient cacheClient, IDbConnectionFactory dbConnectionFactory, IMessageFactory messageFactory) : base(cacheClient, dbConnectionFactory, messageFactory)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Lazada;

            var kvQuartz = new KvQuartz();
            config.GetSection("Quartz").Bind(kvQuartz);
            var jobItem = (kvQuartz?.JobChannels).FirstOrDefault(t => t.ChannelType == ChannelType && t.JobName.Contains(nameof(LazadaDailyRefreshAccessTokenJob)));

            RefreshBeforeExpiryMinutes = NumberHelper.GetValueOrDefault(jobItem.RefreshTokenBeforeMinutes, 1440);

            IncludeRetailerIds = jobItem?.IncludeRetail?.ToList() ?? new List<int>();
            ExcludeRetailerIds = jobItem?.ExcludeRetail?.ToList() ?? new List<int>();
        }
    }

    [DisallowConcurrentExecution]
    public class SendoDailyRefreshAccessTokenJob : BaseDailyRefreshAccessTokenJob
    {
        public SendoDailyRefreshAccessTokenJob(IConfiguration config, ICacheClient cacheClient, IDbConnectionFactory dbConnectionFactory, IMessageFactory messageFactory) : base(cacheClient, dbConnectionFactory, messageFactory)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Sendo;

            var kvQuartz = new KvQuartz();
            config.GetSection("Quartz").Bind(kvQuartz);
            var jobItem = (kvQuartz?.JobChannels).FirstOrDefault(t => t.ChannelType == ChannelType && t.JobName.Contains(nameof(SendoDailyRefreshAccessTokenJob)));

            RefreshBeforeExpiryMinutes = NumberHelper.GetValueOrDefault(jobItem.RefreshTokenBeforeMinutes, 1440);

            IncludeRetailerIds = jobItem?.IncludeRetail?.ToList() ?? new List<int>();
            ExcludeRetailerIds = jobItem?.ExcludeRetail?.ToList() ?? new List<int>();
        }
    }

    [DisallowConcurrentExecution]
    public class ShopeeDailyRefreshAccessTokenJob : BaseDailyRefreshAccessTokenJob
    {
        public ShopeeDailyRefreshAccessTokenJob(IConfiguration config, ICacheClient cacheClient, IDbConnectionFactory dbConnectionFactory, IMessageFactory messageFactory) : base(cacheClient, dbConnectionFactory, messageFactory)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Shopee;

            var kvQuartz = new KvQuartz();
            config.GetSection("Quartz").Bind(kvQuartz);
            var jobItem = (kvQuartz?.JobChannels).FirstOrDefault(t => t.ChannelType == ChannelType && t.JobName.Contains(nameof(ShopeeDailyRefreshAccessTokenJob)));

            RefreshBeforeExpiryMinutes = NumberHelper.GetValueOrDefault(jobItem.RefreshTokenBeforeMinutes, 1440);

            IncludeRetailerIds = jobItem?.IncludeRetail?.ToList() ?? new List<int>();
            ExcludeRetailerIds = jobItem?.ExcludeRetail?.ToList() ?? new List<int>();
        }
    }

    [DisallowConcurrentExecution]
    public class TiktokDailyRefreshAccessTokenJob : BaseDailyRefreshAccessTokenJob
    {
        public TiktokDailyRefreshAccessTokenJob(IConfiguration config, ICacheClient cacheClient, IDbConnectionFactory dbConnectionFactory, IMessageFactory messageFactory) : base(cacheClient, dbConnectionFactory, messageFactory)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Tiktok;

            var kvQuartz = new KvQuartz();
            config.GetSection("Quartz").Bind(kvQuartz);
            var jobItem = (kvQuartz?.JobChannels).FirstOrDefault(t => t.ChannelType == ChannelType && t.JobName.Contains(nameof(TiktokDailyRefreshAccessTokenJob)));

            RefreshBeforeExpiryMinutes = NumberHelper.GetValueOrDefault(jobItem.RefreshTokenBeforeMinutes, 1440);

            IncludeRetailerIds = jobItem?.IncludeRetail?.ToList() ?? new List<int>();
            ExcludeRetailerIds = jobItem?.ExcludeRetail?.ToList() ?? new List<int>();
        }
    }
}

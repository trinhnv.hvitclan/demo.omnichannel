﻿using Demo.OmniChannel.Infrastructure.Auditing;
using Quartz;
using ServiceStack.Logging;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Demo.OmniChannel.ScheduleService.Interfaces;
using ServiceStack.Caching;
using ServiceStack.Data;
using Demo.OmniChannel.Schedule.Common;
using System.Linq;
using Demo.OmniChannel.ShareKernel.Common;
using System.Collections.Generic;
using ServiceStack.Configuration;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.Infrastructure.EventBus.Event;
using Demo.OmniChannel.ShareKernel.Dto;

namespace Demo.OmniChannel.Schedule.Jobs.Tiktok
{
    [DisallowConcurrentExecution]
    public class TiktokSyncProductFromErrorOrderJob : BaseJob
    {
        private readonly IAppSettings _appSettings;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly IIntegrationEventService _integrationEventService;
        private ILog _logger => LogManager.GetLogger(
            typeof(TiktokSyncProductFromErrorOrderJob));

        public TiktokSyncProductFromErrorOrderJob(
            IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IScheduleService scheduleService,
            IAppSettings appSettings,
            IIntegrationEventService integrationEventService) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService)
        {
            _appSettings = appSettings;
            _dbConnectionFactory = dbConnectionFactory;
            _integrationEventService = integrationEventService;
        }

        public override async Task Execute(IJobExecutionContext context)
        {
            var logId = Guid.NewGuid();
            var log = new LogObject(_logger, logId)
            {
                Action = $"{nameof(TiktokSyncProductFromErrorOrderJob)}"
            };
            try
            {
                var kvQuartz = new KvQuartz();
                var listProduct = new List<MongoDb.Product>();
                Configuration.GetSection("Quartz").Bind(kvQuartz);
                var jobItem = (kvQuartz?.JobChannels).FirstOrDefault(t =>
                    t.JobName.Contains(Enum.GetName(typeof(ScheduleType), ScheduleType.SyncProductFromErrorOrder)));
                var channels = await GetChannels(jobItem.IncludeRetail);

                foreach (var item in channels)
                {
                    HandlePushEvent(item);
                }

                log.ResponseObject = $"Channels: {channels.Count()}";
                log.LogInfo();
            }
            catch (Exception ex)
            {
                log.LogError(ex);
            }
        }

        #region Private method
        private async Task<List<OmniChannelDto>> GetChannels(List<int> includeRetail)
        {
            List<OmniChannelDto> channels;
            using (var db = _dbConnectionFactory.OpenDbConnection())
            {
                var omniChannelRespo = new OmniChannelRepository(db);
                channels = await omniChannelRespo.GetChannelByChannelType(
                    (byte)ChannelType.Tiktok, includeRetail);
            }
            return channels;
        }

        private void HandlePushEvent(OmniChannelDto channel)
        {
            var key = string.Format(KvConstant.CacheTiktokSyncProductOrder, channel.Id);
            var getCache = CacheClient.Get<TiktokSyncProductFromErrorDto>(key);
            if (getCache != null && getCache.IsRunning) return;

            _integrationEventService.AddEventWithRoutingKeyAsync(
                       new TiktokSyncProductFromErrorOrder(channel.RetailerId, channel.Id),
                       RoutingKey.TiktokSyncProductFromOrder);
        }
        #endregion     
    }
}

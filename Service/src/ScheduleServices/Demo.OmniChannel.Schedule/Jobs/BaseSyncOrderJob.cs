﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Schedule.Common;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Schedule.Jobs
{
    public abstract class BaseSyncOrderJob : BaseJob
    {
        private readonly IMessageFactory _messageFactory;
        private readonly IChannelBusiness _channelBusiness;
        private readonly SelfAppConfig _config;
        protected BaseSyncOrderJob(IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IMessageFactory messageFactory,
            IScheduleService scheduleService,
            IChannelBusiness channelBusiness,
            IAppSettings appSettings) : base(config,
            cacheClient,
            dbConnectionFactory, 
            scheduleService)
        {
            _messageFactory = messageFactory;
            _channelBusiness = channelBusiness;
            _config = new SelfAppConfig(appSettings);
        }
        protected byte ChannelType { get; set; }
        public override async Task Execute(IJobExecutionContext context) 
        {
            if (!IsValidMaintenanceTime(this.GetType().Name)) return;
            var logId = Guid.NewGuid();
            var logAction = $"{EnumHelper.ToDescription((ChannelType)ChannelType)}_{nameof(BaseSyncOrderJob)}";
            var syncOrderJobLog = new LogObject(Logger, logId)
            {
                Action = logAction
            };

            try
            {
                using (var db = DbConnectionFactory.OpenDbConnection())
                {
                    using (var scheduleRepo = new OmniChannelScheduleRepository(db))
                    {
                        var retailerCodes = new List<string>();
                        var channelIds = new List<long>();
                        var kvQuartz = new KvQuartz();
                        Configuration.GetSection("Quartz").Bind(kvQuartz);
                        var jobItem = (kvQuartz?.JobChannels).FirstOrDefault(t => t.ChannelType == ChannelType && t.JobName.Contains(Enum.GetName(typeof(ScheduleType), ScheduleType.SyncOrder)));
                        var total = await scheduleRepo.GetTotal(ChannelType, (byte) ScheduleType.SyncOrder, jobItem?.IncludeRetail?.ToList(), jobItem?.ExcludeRetail?.ToList());
                        var pageSize = 1000;
                        var totalPage = Math.Ceiling((double) total / pageSize);

                        for (var i = 0; i < totalPage; i++)
                        {
                            var scheduleIds = await scheduleRepo.GetIds(ChannelType, (byte) ScheduleType.SyncOrder, jobItem?.IncludeRetail?.ToList(), jobItem?.ExcludeRetail?.ToList(), i * pageSize, pageSize);
                            
                            var lsCache = await GetScheduleFromCaching(ChannelType, (byte) ScheduleType.SyncOrder, scheduleIds);

                            foreach (var item in lsCache)
                            {
                                var channel = await _channelBusiness.GetChannel(item.ChannelId, item.RetailerId, logId);
                                if (channel == null)
                                {
                                    var log = new LogObject(Logger, logId)
                                    {
                                        Action = $"{logAction}_GetChannel",
                                        RequestObject = "ChannelId: " + item.ChannelId,
                                        Description = "Channel is null"
                                    };
                                    log.LogError();
                                    continue;
                                }

                                var kvRetailer = await GetRetailer(item.RetailerId);
                                if (kvRetailer == null)
                                {
                                    var log = new LogObject(Logger, logId)
                                    {
                                        Action = $"{logAction}_GetRetailer",
                                        RequestObject = "RetailerId: " + item.RetailerId,
                                        Description = "Retailer is null"
                                    };
                                    log.LogError();
                                    continue;
                                }

                                var kvGroup = await GetRetailerGroup(kvRetailer.GroupId);
                                if (kvGroup == null || string.IsNullOrEmpty(kvGroup.ConnectionString))
                                {
                                    var log = new LogObject(Logger, logId)
                                    {
                                        Action = $"{logAction}_GetGroup",
                                        RequestObject = "GroupId: " + kvRetailer?.GroupId,
                                        ResponseObject = kvGroup,
                                        Description = "kvGroup or kvGroup.ConnectionString is null"
                                    };
                                    log.LogError();
                                    continue;
                                }

                                item.IsRunning = true;

                                PushRedisMessageQueue(item, kvRetailer, kvGroup, channel);
                                RunningSchedule(ChannelType, new List<ScheduleDto> {item});

                                retailerCodes.Add(kvRetailer.Code);
                                channelIds.Add(item.ChannelId);
                            }
                        }

                        syncOrderJobLog.ResponseObject = $"ChannelId:{string.Join(",", channelIds.Distinct())} Retailer:{string.Join(",", retailerCodes.Distinct())}";
                        syncOrderJobLog.LogInfo();
                    }
                }
            }
            catch (Exception e)
            {
                syncOrderJobLog.LogError(e);
                throw;
            }
        }

        private void RunningSchedule(byte channelType, IEnumerable<ScheduleDto> dtos)
        {
            ScheduleService.AddOrUpdateBatch(channelType, (byte)ScheduleType.SyncOrder, dtos);
        }
        private void PushRedisMessageQueue(ScheduleDto item, KvRetailer kvRetailer, KvGroup kvGroup, Domain.Model.OmniChannel channel)
        {
            var logId = Guid.NewGuid();
            switch (item.ChannelType)
            {
                case (byte) ShareKernel.Common.ChannelType.Lazada:
                {
                    using (var mq = _messageFactory.CreateMessageProducer())
                    {
                        mq.Publish(new LazadaSyncOrderMessage
                        {
                            ChannelId = item.ChannelId,
                            ChannelType = item.ChannelType,
                            LastSync = item.LastSync,
                            RetailerId = channel.RetailerId,
                            BranchId = channel.BranchId,
                            RetailerCode = kvRetailer?.Code,
                            KvEntities = kvGroup.ConnectionString,
                            ScheduleDto = item,
                            GroupId = kvGroup.Id,
                            BasePriceBookId = channel.BasePriceBookId.GetValueOrDefault(),
                            SyncOrderFormula = channel.SyncOrderFormula ?? 30,
                            IsFirstSync = item.LastSync == null,
                            LogId = logId,
                            PlatformId = channel.PlatformId
                        });
                    }

                    break;
                }

                case (byte) ShareKernel.Common.ChannelType.Tiki:
                {
                    using (var mq = _messageFactory.CreateMessageProducer())
                    {
                        mq.Publish(new TikiSyncOrderMessage
                        {
                            ChannelId = item.ChannelId,
                            ChannelType = item.ChannelType,
                            LastSync = item.LastSync,
                            RetailerId = channel.RetailerId,
                            BranchId = channel.BranchId,
                            RetailerCode = kvRetailer?.Code,
                            KvEntities = kvGroup.ConnectionString,
                            ScheduleDto = item,
                            GroupId = kvGroup.Id,
                            BasePriceBookId = channel.BasePriceBookId.GetValueOrDefault(),
                            SyncOrderFormula = channel.SyncOrderFormula ?? 30,
                            IsFirstSync = item.LastSync == null,
                            LogId = logId,
                            PlatformId = channel.PlatformId
                        });
                    }
                    break;
                }
                case (byte)ShareKernel.Common.ChannelType.Sendo:
                {
                    using (var mq = _messageFactory.CreateMessageProducer())
                    {
                        mq.Publish(new SendoSyncOrderMessage
                        {
                            ChannelId = item.ChannelId,
                            ChannelType = item.ChannelType,
                            LastSync = item.LastSync,
                            RetailerId = channel.RetailerId,
                            BranchId = channel.BranchId,
                            RetailerCode = kvRetailer?.Code,
                            KvEntities = kvGroup.ConnectionString,
                            ScheduleDto = item,
                            GroupId = kvGroup.Id,
                            BasePriceBookId = channel.BasePriceBookId.GetValueOrDefault(),
                            SyncOrderFormula = channel.SyncOrderFormula ?? 30,
                            IsFirstSync = item.LastSync == null,
                            LogId = logId,
                            PlatformId = channel.PlatformId
                        });
                    }
                    break;
                }
            }
        }
     
    }
}

﻿using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Schedule.Common;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Schedule.Jobs.Lazada
{
    [DisallowConcurrentExecution]
    public class LazadaSyncFeeTransactionJob : BaseJob
    {
        private readonly IMessageFactory _messageFactory;

        public LazadaSyncFeeTransactionJob(IConfiguration config,
             ICacheClient cacheClient,
             IDbConnectionFactory dbConnectionFactory,
             IScheduleService scheduleService,
             IMessageFactory messageFactory)
            : base(config, cacheClient, dbConnectionFactory, scheduleService)
        {
            _messageFactory = messageFactory;
        }

        public async override Task Execute(IJobExecutionContext context)
        {
            if (!IsValidMaintenanceTime(this.GetType().Name)) return;
            var logId = Guid.NewGuid();
            var logAction = $"{nameof(LazadaSyncFeeTransactionJob)}";
            var syncFeeTransactionLog = new LogObject(Logger, logId)
            {
                Action = logAction
            };

            try
            {
                using (var db = DbConnectionFactory.OpenDbConnection())
                {
                    var omniChannelRespo = new OmniChannelRepository(db);

                    // Gọi xuống db để lấy tất cả những omniChannel là LZD
                    var kvQuartz = new KvQuartz();
                    Configuration.GetSection("Quartz").Bind(kvQuartz);
                    var jobItem = (kvQuartz?.JobChannels).FirstOrDefault(t => t.JobName.Contains($"{nameof(LazadaSyncFeeTransactionJob)}"));
                    var channels = await omniChannelRespo.GetTotalByChannelType((byte)ChannelType.Lazada, jobItem?.IncludeRetail?.ToList(), jobItem?.ExcludeRetail?.ToList());
                    // Bắn MQ để xử lý
                    foreach (var channelItem in channels)
                    {
                        var kvRetailer = await GetRetailer(channelItem.RetailerId);
                        if (kvRetailer == null)
                        {
                            syncFeeTransactionLog.RequestObject = "RetailerId: " + channelItem.RetailerId;
                            syncFeeTransactionLog.Description = "Retailer is null";
                            syncFeeTransactionLog.LogError();
                            continue;
                        }

                        var kvGroup = await GetRetailerGroup(kvRetailer.GroupId);
                        if (kvGroup == null || string.IsNullOrEmpty(kvGroup.ConnectionString))
                        {
                            syncFeeTransactionLog.RequestObject = "GroupId: " + kvRetailer?.GroupId;
                            syncFeeTransactionLog.Description = "kvGroup or kvGroup.ConnectionString is null";
                            syncFeeTransactionLog.LogError();
                            continue;
                        }
                        PushRedisMessageQueue(kvRetailer, kvGroup, channelItem, logId);

                    }

                    syncFeeTransactionLog.Description = $"PushRedisMessageQueue LazadaSyncFeeTransactionJob Count {channels?.Count.ToString()}";
                }
            }
            catch (Exception e)
            {
                syncFeeTransactionLog.LogError(e);
                throw;
            }

        }

        private void PushRedisMessageQueue(KvRetailer kvRetailer, KvGroup kvGroup, Domain.Model.OmniChannel channel, Guid logId)
        {
            if (ValidateRunning(channel))
            {
                using (var mq = _messageFactory.CreateMessageProducer())
                {
                    var message = new LazadaSyncFeeTransactionMessage
                    {
                        ChannelId = channel.Id,
                        ChannelType = channel.Type,
                        ChannelName = channel.Name,
                        RetailerId = channel.RetailerId,
                        RetailerCode = kvRetailer?.Code,
                        BranchId = channel.BranchId,
                        GroupId = kvGroup.Id,
                        ShopId = channel.IdentityKey,
                        SentTime = DateTime.Now,
                        LogId = logId,
                        KvEntities = kvGroup.ConnectionString
                    };
                    mq.Publish(message);
                }
                SetCacheRunning(channel);
            }
        }

        private bool ValidateRunning(Domain.Model.OmniChannel channel)
        {
            var isRunning = CacheClient.Get<bool>(string.Format(KvConstant.FeeLZDPlatformKey, channel.Id));
            if (!isRunning)
            {
                return true;
            }
            return false;
        }

        private void SetCacheRunning(Domain.Model.OmniChannel channel)
        {
            if (!ValidateRunning(channel))
            {
                CacheClient.Set(string.Format(KvConstant.FeeLZDPlatformKey, channel.Id),true, DateTime.Now.AddDays(1));
            }
        }
    }
}

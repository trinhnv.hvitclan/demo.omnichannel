﻿using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Microsoft.EntityFrameworkCore.Internal;
using ServiceStack.Messaging;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Schedule.Jobs
{
    [DisallowConcurrentExecution]
    public class CreateLoadTestStockMessageJob : BaseJob
    {
        public readonly IMessageFactory _messageFactory;


        public CreateLoadTestStockMessageJob(IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IMessageFactory messageFactory,
            IScheduleService scheduleService) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService)
        {
            _messageFactory = messageFactory;
        }

        public override async Task Execute(IJobExecutionContext context)
        {
            try
            {
                var listProductsIds = new List<ProductBranch>();
                if (EnumerableExtensions.Any(KvEntities))
                {
                    string connectionString = Enumerable.FirstOrDefault(KvEntities).Name;
                    //connectionString = connectionString.Substring(connectionString.IndexOf('=') + 1);
                    using (var db = DbConnectionFactory.OpenDbConnection(connectionString.ToLower()))
                    {

                        var productRepo = new ProductRepository(db);
                        var retailerId = Configuration.GetSection("LoadTest").GetSection("ProcessStock").GetValue<int>("RetailerId");
                        var branchId = Configuration.GetSection("LoadTest").GetSection("ProcessStock").GetValue<int>("BranchId");
                        var limitNumber = Configuration.GetSection("LoadTest").GetSection("ProcessStock").GetValue<int>("LimitNumber");
                        listProductsIds = await productRepo.GetProductByRetailerId(retailerId, branchId, limitNumber);
                        connectionString = "KVEntities";

                    }
                    foreach (var product in listProductsIds)
                    {
                        var productsubcribe = new Domain.Model.ProductSubscribe()
                        {
                            ProductBranch = new List<ProductBranch>() { product },
                            BranchId = product.BranchId,
                            Price = 100000,
                            RetailerId = product.RetailerId,
                            Id = product.ProductId
                        };
                        var stockUpdateMessage = new ProcessStockMessage<Domain.Model.ProductSubscribe>
                        {
                            RetailerId = product.RetailerId,
                            BranchId = product.BranchId,
                            SentTime = DateTime.Now,
                            Products = new List<ProductSubscribe>() { productsubcribe },
                            ConnectionString = connectionString,
                            LogId = Guid.NewGuid()
                        };
                        using (var mq = _messageFactory.CreateMessageProducer())
                        {
                            mq.Publish(stockUpdateMessage);
                        }
                    }
                } 
                
                
        }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
}
    }
}

﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Schedule.Common;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Data;

namespace Demo.OmniChannel.Schedule.Jobs
{
    /// <summary>
    /// Sync LastSync, NextRun from Caching to SQL
    /// </summary>
    [DisallowConcurrentExecution]
    public class BaseDailySyncFromCacheJob : BaseJob
    {
        public byte ChannelType { get; set; }
        public BaseDailySyncFromCacheJob(IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IScheduleService scheduleService) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService)
        {
        }

        public override async Task Execute(IJobExecutionContext context)
        {
            if (!IsValidMaintenanceTime(this.GetType().Name)) return;

            var logObject = new LogObject(Logger, Guid.NewGuid())
            {
                Action = $"{EnumHelper.ToDescription((ChannelType)ChannelType)}_{nameof(BaseDailySyncFromCacheJob)}"
            };

            long totalItemHandle = 0;
            long totalCompleted = 0;

            try
            {
                using (var db = DbConnectionFactory.OpenDbConnection())
                {
                    using (var scheduleRepo = new OmniChannelScheduleRepository(db))
                    {
                        await RunWithScheduleType(scheduleRepo, ScheduleType.SyncOrder);
                        await RunWithScheduleType(scheduleRepo, ScheduleType.SyncModifiedProduct);
                    }
                }

                logObject.TotalItem = totalCompleted;
                logObject.Description = $"Item completed: {totalCompleted}/{totalItemHandle}";
                logObject.LogInfo();
            }
            catch (Exception e)
            {
                logObject.Description = $"Item completed: {totalCompleted}/{totalItemHandle}";
                logObject.LogError(e);
                throw;
            }

            // Local function
            async Task RunWithScheduleType(OmniChannelScheduleRepository scheduleRepo, ScheduleType scheduleType)
            {
                var total = await scheduleRepo.GetTotal(ChannelType, (byte)scheduleType, null, null);
                if (total == 0) return;

                totalItemHandle += total;

                var pageSize = 100;
                var totalPage = (int)Math.Ceiling((double)total / pageSize);
                var kvQuartz = GetQuartz();

                for (var i = 0; i < totalPage; i++)
                {
                    var scIds = await scheduleRepo.GetIds(ChannelType, (byte)scheduleType, null, null, pageSize * i, pageSize);
                    var jobsInCache = ScheduleService.GetByIdsCoverDeserializeError(scIds);

                    if (jobsInCache?.Any() != true) continue;

                    foreach (var item in jobsInCache)
                    {
                        if (IsValid(kvQuartz, scheduleType, item.RetailerId))
                        {
                            await scheduleRepo.UpdateOnlyAsync(new OmniChannelSchedule { LastSync = item.LastSync, NextRun = item.NextRun }, new[] { "LastSync", "NextRun" }, x => x.Id == item.Id);
                            totalCompleted++;
                        }
                    }
                }
            }
        }

        private KvQuartz GetQuartz()
        {
            var kvQuartz = new KvQuartz();
            Configuration.GetSection("Quartz").Bind(kvQuartz);
            return kvQuartz;
        }

        private bool IsValid(KvQuartz kvQuartz, ScheduleType scheduleType, int retailerId)
        {
            var jobItem = (kvQuartz?.JobChannels).FirstOrDefault(t => t.ChannelType == ChannelType
                && t.JobName.Contains(Enum.GetName(typeof(ScheduleType), scheduleType)));

            if (jobItem == null) return true;

            if (!jobItem.IsActive) return false;

            if (jobItem.ExcludeRetail == null || jobItem.ExcludeRetail.Count == 0) return true;

            if (jobItem.ExcludeRetail.Contains(retailerId)) return false;

            return true;
        }
    }
}
﻿using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Schedule.Jobs
{
    public abstract class BaseSyncRetryInvoiceJob : BaseJob
    {
        private IMessageFactory _messageFactory;
        private IRetryInvoiceMongoService _retryInvoiceMongoService;
        public BaseSyncRetryInvoiceJob(IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IMessageFactory messageFactory,
            IRetryInvoiceMongoService retryInvoiceMongoService,
            IScheduleService scheduleService) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService)
        {
            _messageFactory = messageFactory;
            _retryInvoiceMongoService = retryInvoiceMongoService;
        }
        protected byte ChannelType { get; set; }
        public override async Task Execute(IJobExecutionContext context)
        {

            var totalRetry = Configuration.GetValue<int>("totalInvoiceRetry");
            var pageSize = Configuration.GetValue<int>("retryPageSize");
            var totalRecords = await _retryInvoiceMongoService.TotalByChannelType(ChannelType, totalRetry);
            var total = totalRecords / pageSize;
            total = totalRecords % pageSize == 0 ? total : total + 1;
            for (int i = 0; i < total; i++)
            {
                var syncRetryInvoiceLog = new LogObject(Logger, Guid.NewGuid())
                {
                    Action = $"{EnumHelper.ToDescription((ChannelType)ChannelType)}_{nameof(BaseSyncRetryInvoiceJob)}"
                };
                var currentItems =
                    await _retryInvoiceMongoService.GetByChannelType(ChannelType, totalRetry, pageSize, i * pageSize);
                foreach (var item in currentItems)
                {
                    await PushRedisMessageQueue(item, syncRetryInvoiceLog.Id);
                }
                syncRetryInvoiceLog.RequestObject = $"Push orderId: {string.Join(",", currentItems.Select(item => item.OrderId).ToList())}";
                syncRetryInvoiceLog.TotalItem = currentItems.Count;
                syncRetryInvoiceLog.LogInfo();
            }

            var totalInvoiceDeleted = await _retryInvoiceMongoService.RemoveOldErrorInvoiceByChannelType(ChannelType, totalRetry, DateTime.Now.AddDays(-90));
            if (totalInvoiceDeleted > 0)
            {
                var removeRetryInvoiceJobLog = new LogObject(Logger, Guid.NewGuid())
                {
                    Action = $"{EnumHelper.ToDescription((ChannelType)ChannelType)}_{nameof(BaseSyncRetryInvoiceJob)}_Remove",
                    TotalItem = totalInvoiceDeleted
                };
                removeRetryInvoiceJobLog.LogInfo();
            }
        }
        public virtual Task PushRedisMessageQueue(RetryInvoice invoice, Guid logId)
        {
            using (var mq = _messageFactory.CreateMessageProducer())
            {
                switch (invoice.ChannelType)
                {
                    case (byte)Sdk.Common.ChannelType.Lazada:
                        {
                            var createInvoice = new LazadaCreateInvoiceMessage
                            {
                                ChannelId = invoice.ChannelId,
                                ChannelType = invoice.ChannelType,
                                BranchId = invoice.BranchId,
                                KvEntities = invoice.KvEntities,
                                RetailerId = invoice.RetailerId,
                                OrderId = invoice.OrderId,
                                Order = invoice.Order,
                                KvOrder = invoice.KvOrder,
                                KvOrderId = invoice.KvOrderId,
                                RetryCount = invoice.RetryCount,
                                InvoiceCode = invoice.Code,
                                IsDbException = invoice.IsDbException
                            };
                            mq.Publish(createInvoice);
                            break;
                        }
                    
                    case (byte)Sdk.Common.ChannelType.Tiki:
                        {
                            var createInvoice = new TikiCreateInvoiceMessage
                            {
                                ChannelId = invoice.ChannelId,
                                ChannelType = invoice.ChannelType,
                                BranchId = invoice.BranchId,
                                KvEntities = invoice.KvEntities,
                                RetailerId = invoice.RetailerId,
                                OrderId = invoice.OrderId,
                                Order = invoice.Order,
                                KvOrder = invoice.KvOrder,
                                KvOrderId = invoice.KvOrderId,
                                RetryCount = invoice.RetryCount,
                                InvoiceCode = invoice.Code,
                                IsDbException = invoice.IsDbException
                            };
                            mq.Publish(createInvoice);
                            break;
                        }
                    case (byte)Sdk.Common.ChannelType.Sendo:
                        {
                            var createInvoice = new SendoCreateInvoiceMessage
                            {
                                ChannelId = invoice.ChannelId,
                                ChannelType = invoice.ChannelType,
                                BranchId = invoice.BranchId,
                                KvEntities = invoice.KvEntities,
                                RetailerId = invoice.RetailerId,
                                OrderId = invoice.OrderId,
                                Order = invoice.Order,
                                KvOrder = invoice.KvOrder,
                                KvOrderId = invoice.KvOrderId,
                                RetryCount = invoice.RetryCount,
                                InvoiceCode = invoice.Code,
                                IsDbException = invoice.IsDbException
                            };
                            mq.Publish(createInvoice);
                            break;
                        }
                    case (byte)Sdk.Common.ChannelType.Shopee:
                        {
                            var createInvoice = new ShopeeCreateInvoiceMessageV2
                            {
                                ChannelId = invoice.ChannelId,
                                ChannelType = invoice.ChannelType,
                                BranchId = invoice.BranchId,
                                KvEntities = invoice.KvEntities,
                                RetailerId = invoice.RetailerId,
                                OrderId = invoice.OrderId,
                                Order = invoice.Order,
                                KvOrder = invoice.KvOrder,
                                KvOrderId = invoice.KvOrderId,
                                RetryCount = invoice.RetryCount,
                                InvoiceCode = invoice.Code,
                                IsDbException = invoice.IsDbException
                            };
                            mq.Publish(createInvoice);
                            break;
                        }
                    case (byte)Sdk.Common.ChannelType.Tiktok:
                        {
                            var createInvoice = new TiktokCreateInvoiceMessageV2
                            {
                                ChannelId = invoice.ChannelId,
                                ChannelType = invoice.ChannelType,
                                BranchId = invoice.BranchId,
                                KvEntities = invoice.KvEntities,
                                RetailerId = invoice.RetailerId,
                                OrderId = invoice.OrderId,
                                Order = invoice.Order,
                                KvOrder = invoice.KvOrder,
                                KvOrderId = invoice.KvOrderId,
                                RetryCount = invoice.RetryCount,
                                InvoiceCode = invoice.Code,
                                IsDbException = invoice.IsDbException
                            };
                            mq.Publish(createInvoice);
                            break;
                        }
                }
                return Task.FromResult(0);
            }
        }
    }
}
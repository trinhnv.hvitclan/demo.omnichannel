﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Schedule.Common;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Schedule.Jobs
{
    public abstract class BaseSyncModifiedProductJob : BaseJob
    {
        private IChannelBusiness _channelBusiness;
        private IMessageFactory _messageFactory;
        protected BaseSyncModifiedProductJob(IConfiguration config,
                                      ICacheClient cacheClient,
                                      IDbConnectionFactory dbConnectionFactory,
                                      IScheduleService scheduleService,
                                      IChannelBusiness channelBusiness,
                                      IMessageFactory messageFactory) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService)
        {
            _channelBusiness = channelBusiness;
            _messageFactory = messageFactory;
        }

        protected byte ChannelType { get; set; }

        public override async Task Execute(IJobExecutionContext context)
        {
            if (!IsValidMaintenanceTime(this.GetType().Name)) return;

            var logId = Guid.NewGuid();
            var logAction = $"{EnumHelper.ToDescription((ChannelType)ChannelType)}_{nameof(BaseSyncModifiedProductJob)}";
            var syncModifiedProductJobLog = new LogObject(Logger, logId)
            {
                Action = logAction
            };

            try
            {
                using (var db = DbConnectionFactory.OpenDbConnection())
                {
                    using (var scheduleRepo = new OmniChannelScheduleRepository(db))
                    {
                        var retailerCodes = new List<string>();
                        var channelIds = new List<long>();
                        var kvQuartz = new KvQuartz();
                        Configuration.GetSection("Quartz").Bind(kvQuartz);
                        var jobItem = (kvQuartz?.JobChannels).FirstOrDefault(t => t.ChannelType == ChannelType && t.JobName.Contains(Enum.GetName(typeof(ScheduleType), ScheduleType.SyncModifiedProduct)));
                        var total = await scheduleRepo.GetTotal(ChannelType, (byte)ScheduleType.SyncModifiedProduct, jobItem?.IncludeRetail?.ToList(), jobItem?.ExcludeRetail?.ToList());
                        var pageSize = 1000;
                        var totalPage = Math.Ceiling((double)total / pageSize);

                        for (var i = 0; i < totalPage; i++)
                        {
                            var scheduleIds = await scheduleRepo.GetIds(ChannelType, (byte)ScheduleType.SyncModifiedProduct, jobItem?.IncludeRetail?.ToList(), jobItem?.ExcludeRetail?.ToList(), i * pageSize, pageSize);
                            var lsCache = await GetScheduleFromCaching(ChannelType, (byte)ScheduleType.SyncModifiedProduct, scheduleIds);

                            foreach (var item in lsCache)
                            {
                                var channel = await _channelBusiness.GetChannel(item.ChannelId, item.RetailerId, logId);
                                if (channel == null)
                                {
                                    var log = new LogObject(Logger, logId)
                                    {
                                        Action = $"{logAction}_GetChannel",
                                        RequestObject = "ChannelId: " + item.ChannelId,
                                        Description = "Channel is null"
                                    };
                                    log.LogError();
                                    continue;
                                }

                                var kvRetailer = await GetRetailer(item.RetailerId);
                                if (kvRetailer == null)
                                {
                                    var log = new LogObject(Logger, logId)
                                    {
                                        Action = $"{logAction}_GetRetailer",
                                        RequestObject = "RetailerId: " + item.RetailerId,
                                        Description = "Retailer is null"
                                    };
                                    log.LogError();
                                    continue;
                                }

                                var kvGroup = await GetRetailerGroup(kvRetailer.GroupId);
                                if (kvGroup == null || string.IsNullOrEmpty(kvGroup.ConnectionString))
                                {
                                    var log = new LogObject(Logger, logId)
                                    {
                                        Action = $"{logAction}_GetGroup",
                                        RequestObject = "GroupId: " + kvRetailer?.GroupId,
                                        ResponseObject = kvGroup,
                                        Description = "kvGroup or kvGroup.ConnectionString is null"
                                    };
                                    log.LogError();
                                    continue;
                                }
                                PushToMQ(item, channel, kvRetailer, kvGroup, logId);
                                // Update IsRunning
                                item.IsRunning = true;
                                ScheduleService.AddOrUpdateBatch(ChannelType, (byte)ScheduleType.SyncModifiedProduct, new List<ScheduleDto> { item });

                                channelIds.Add(item.ChannelId);
                                retailerCodes.Add(kvRetailer.Code);
                            }

                            syncModifiedProductJobLog.ResponseObject = $"ChannelId:{string.Join(", ", channelIds)} Retailer:{string.Join(", ", retailerCodes.Distinct())}";
                            syncModifiedProductJobLog.LogInfo();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                syncModifiedProductJobLog.LogError(e);
                throw;
            }
        }

        public virtual void PushToMQ(ScheduleDto item, Demo.OmniChannel.Domain.Model.OmniChannel channel, KvRetailer kvRetailer, KvGroup kvGroup, Guid logId)
        {
            var message = new SyncModifiedProductMessage
            {
                ChannelId = item.ChannelId,
                ChannelType = item.ChannelType,
                RetailerId = channel.RetailerId,
                BranchId = channel.BranchId,
                LastSyncDateTime = item.LastSync,
                ChannelScheduleId = item.Id,
                RetailerCode = kvRetailer?.Code,
                KvEntities = kvGroup.ConnectionString,
                ScheduleDto = item,
                LogId = Guid.NewGuid(),
                ParentLogIds = new List<Guid> { logId }
            };
            using (var mq = _messageFactory.CreateMessageProducer())
            {
                mq.Publish(message);
            }
        }
    }
}

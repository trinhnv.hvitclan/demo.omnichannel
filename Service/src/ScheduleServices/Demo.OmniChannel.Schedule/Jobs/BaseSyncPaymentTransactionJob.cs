﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Schedule.Common;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.Schedule.Jobs
{
    public abstract class BaseSyncPaymentTransactionJob : BaseJob
    {
        protected byte ChannelType { get; set; }
        private readonly IMessageFactory _messageFactory;

        protected BaseSyncPaymentTransactionJob(IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IScheduleService scheduleService,
            IMessageFactory messageFactory) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService)
        {
            _messageFactory = messageFactory;
        }


        public override async Task Execute(IJobExecutionContext context)
        {
            if (!IsValidMaintenanceTime(this.GetType().Name)) return;

            var logId = Guid.NewGuid();
            var logAction = $"{EnumHelper.ToDescription((ChannelType)ChannelType)}_{nameof(BaseSyncPaymentTransactionJob)}";
            var syncPaymentTransactionLog = new LogObject(Logger, logId)
            {
                Action = logAction
            };

            try
            {
                using (var db = DbConnectionFactory.OpenDbConnection())
                {
                    var omniChannelRespo = new OmniChannelRepository(db);
                    var scheduleRepo = new OmniChannelScheduleRepository(db);
                    var kvQuartz = new KvQuartz();
                    Configuration.GetSection("Quartz").Bind(kvQuartz);

                    var jobItem = (kvQuartz?.JobChannels).FirstOrDefault(t => t.ChannelType == ChannelType && t.JobName.Contains(Enum.GetName(typeof(ScheduleType), ScheduleType.SyncPaymentTransaction)));
                    var channels = await omniChannelRespo.GetTotalByChannelType(ChannelType, jobItem?.IncludeRetail?.ToList(), jobItem?.ExcludeRetail?.ToList());
                    var schedule = await scheduleRepo.GetScheduleForJob(ChannelType, (byte)ScheduleType.SyncPaymentTransaction, jobItem?.IncludeRetail?.ToList(), jobItem?.ExcludeRetail?.ToList());
                    var scheduleIds = schedule.Select(x => x.Id).ToList();

                    var scheduleCaches = await GetScheduleFromCaching(ChannelType, (byte)ScheduleType.SyncPaymentTransaction,
                        scheduleIds);

                    foreach (var item in channels)
                    {
                        var kvRetailer = await GetRetailer(item.RetailerId);
                        if (kvRetailer == null)
                        {
                            var log = new LogObject(Logger, logId)
                            {
                                Action = $"{logAction}_GetRetailer",
                                RequestObject = "RetailerId: " + item.RetailerId,
                                Description = "Retailer is null"
                            };
                            log.LogError();
                            continue;
                        }

                        var kvGroup = await GetRetailerGroup(kvRetailer.GroupId);
                        if (kvGroup == null || string.IsNullOrEmpty(kvGroup.ConnectionString))
                        {
                            var log = new LogObject(Logger, logId)
                            {
                                Action = $"{logAction}_GetGroup",
                                RequestObject = "GroupId: " + kvRetailer?.GroupId,
                                ResponseObject = kvGroup,
                                Description = "kvGroup or kvGroup.ConnectionString is null"
                            };
                            log.LogError();
                            continue;
                        }

                        var scheduleCache =
                            scheduleCaches.FirstOrDefault(t => t.ChannelId == item.Id && t.RetailerId == kvRetailer.Id);
                        if (scheduleCache == null)
                        {
                            var scheduleGetFromDb = schedule.FirstOrDefault(x =>
                                x.ChannelId == item.Id && x.Type == (byte)ScheduleType.SyncPaymentTransaction);
                            if (scheduleGetFromDb == null) continue;

                            var scheduleDto = new ScheduleDto
                            {
                                ChannelId = item.Id,
                                ChannelType = item.Type,
                                ChannelBranch = item.BranchId,
                                IsRunning = false,
                                Id = scheduleGetFromDb.Id,
                                RetailerId = item.RetailerId,
                                NextRun = scheduleGetFromDb.NextRun,
                                Type = scheduleGetFromDb.Type,
                            };
                            ScheduleService.AddOrUpdate(scheduleDto);
                            scheduleCache = scheduleDto;
                        }

                        PushRedisMessageQueue(scheduleCache, kvRetailer, kvGroup, item, logId);
                    }
                }
            }
            catch (Exception e)
            {
                syncPaymentTransactionLog.LogError(e);
                throw;
            }
        }

        private void PushRedisMessageQueue(ScheduleDto schedule, KvRetailer kvRetailer, KvGroup kvGroup, Domain.Model.OmniChannel channel, Guid logId)
        {
            switch (channel.Type)
            {
                case (byte)ShareKernel.Common.ChannelType.Shopee:
                    {
                        using (var mq = _messageFactory.CreateMessageProducer())
                        {
                            var message = new ShopeeSyncPaymentTransactionMessage
                            {
                                ChannelId = channel.Id,
                                ChannelType = channel.Type,
                                ChannelName = channel.Name,
                                RetailerId = channel.RetailerId,
                                RetailerCode = kvRetailer?.Code,
                                BranchId = channel.BranchId,
                                GroupId = kvGroup.Id,
                                ShopId = channel.IdentityKey,
                                SentTime = DateTime.Now,
                                LastSync = schedule.LastSync,
                                ScheduleId = schedule.Id,
                                KvEntities = kvGroup.ConnectionString,
                                LogId = Guid.NewGuid(),
                                PlatformId = channel.PlatformId,
                                ScheduleDto = schedule
                            };
                            mq.Publish(message);
                        }
                        break;
                    }
            }
        }
    }
}

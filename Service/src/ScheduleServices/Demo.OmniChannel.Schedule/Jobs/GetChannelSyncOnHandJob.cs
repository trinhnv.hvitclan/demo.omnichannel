﻿using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Schedule.Jobs
{
    [DisallowConcurrentExecution]
    public class GetChannelSyncOnHandJob : IJob
    {
        private IMessageFactory _messageFactory;
        private IDbConnectionFactory _dbConnectionFactory;
        private ILog _logger => LogManager.GetLogger(typeof(GetChannelSyncOnHandJob));
        public GetChannelSyncOnHandJob(IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IMessageFactory messageFactory) 
        {
            _messageFactory = messageFactory;
            _dbConnectionFactory = dbConnectionFactory;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var logId = Guid.NewGuid();
            var getChannelSyncOnHandJobLog = new LogObject(_logger, logId)
            {
                Action =
                    $"{nameof(GetChannelSyncOnHandJob)}"
            };
            using (var db = _dbConnectionFactory.OpenDbConnection())
            {
                var omniChannelRepository = new OmniChannelRepository(db);
                var channels = await omniChannelRepository.GetChannelHaveSyncOnHandAsync();
                if (channels == null || !channels.Any())
                {
                    getChannelSyncOnHandJobLog.Description = "Empty Channel";
                    getChannelSyncOnHandJobLog.LogInfo();
                    return;
                }

                foreach (var channel in channels)
                {
                    using (var mq = _messageFactory.CreateMessageProducer())
                    {
                        mq.Publish(new GetProductBranchByChannelMessage
                        {
                            RetailerId = channel.RetailerId,
                            BranchId = channel.BranchId,
                            ChannelId = channel.Id,
                            ChannelName = channel.Name,
                            ChannelType = channel.Type,
                            BasePriceBookId = channel.BasePriceBookId ?? 0,
                            PriceBookId = channel.PriceBookId ?? 0,
                            SyncOnHandFormula = channel.SyncOnHandFormula,
                            LogId = logId
                        });
                    }
                }
                getChannelSyncOnHandJobLog.TotalItem = channels.Count;
                getChannelSyncOnHandJobLog.LogInfo();
            }

        }
    }
}
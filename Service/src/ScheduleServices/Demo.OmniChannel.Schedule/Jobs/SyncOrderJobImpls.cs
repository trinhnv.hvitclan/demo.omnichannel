﻿using Demo.OmniChannel.Business.Interfaces;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Data;
using Demo.OmniChannel.ScheduleService.Interfaces;
using ServiceStack.Messaging;
using ServiceStack.Caching;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.Schedule.Jobs
{
    [DisallowConcurrentExecution]
    public class LazadaSyncOrderJob : BaseSyncOrderJob
    {
        public LazadaSyncOrderJob(
            IConfiguration config,
            ICacheClient cacheClient, 
            IDbConnectionFactory dbConnectionFactory, 
            IMessageFactory messageFactory,
            IScheduleService scheduleService,
            IChannelBusiness channelBusiness,
            IAppSettings appSettings) : base(config,
            cacheClient,
            dbConnectionFactory,
            messageFactory,
            scheduleService,
            channelBusiness,
            appSettings)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Lazada;
        }
    }

    [DisallowConcurrentExecution]
    public class ShopeeSyncOrderJob : BaseSyncOrderJob
    {
        public ShopeeSyncOrderJob(
            IConfiguration config,
            ICacheClient cacheClient, 
            IDbConnectionFactory dbConnectionFactory, 
            IMessageFactory messageFactory,
            IScheduleService scheduleService,
            IChannelBusiness channelBusiness,
            IAppSettings appSettings) : base(config,
            cacheClient,
            dbConnectionFactory,
            messageFactory,
            scheduleService,
            channelBusiness,
            appSettings)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Shopee;
        }
    }

    [DisallowConcurrentExecution]
    public class TikiSyncOrderJob : BaseSyncOrderJob
    {
        public TikiSyncOrderJob(
            IConfiguration config,
            ICacheClient cacheClient, 
            IDbConnectionFactory dbConnectionFactory,
            IMessageFactory messageFactory,
            IScheduleService scheduleService,
            IChannelBusiness channelBusiness,
            IAppSettings appSettings) : base(config,
            cacheClient,
            dbConnectionFactory,
            messageFactory,
            scheduleService,
            channelBusiness,
            appSettings)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Tiki;
        }
    }
    [DisallowConcurrentExecution]
    public class SendoSyncOrderJob : BaseSyncOrderJob
    {
        public SendoSyncOrderJob(
            IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IMessageFactory messageFactory,
            IScheduleService scheduleService,
            IChannelBusiness channelBusiness,
            IAppSettings appSettings) : base(config,
            cacheClient,
            dbConnectionFactory,
            messageFactory,
            scheduleService,
            channelBusiness,
            appSettings)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Sendo;
        }
    }
}

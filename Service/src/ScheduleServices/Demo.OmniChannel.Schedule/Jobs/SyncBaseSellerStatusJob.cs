﻿using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Schedule.Jobs
{
    [DisallowConcurrentExecution]
    public abstract class SyncBaseSellerStatusJob : BaseJob
    {
        private readonly IAppSettings _settings;
        private readonly IOmniChannelAuthService _omniChannelAuthService;
        private readonly IOmniChannelPlatformService _omniChannelPlatformService;

        protected byte ChannelType { get; set; }

        protected SyncBaseSellerStatusJob(IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IAppSettings settings,
            IScheduleService scheduleService,
            IOmniChannelAuthService omniChannelAuthService,
            IOmniChannelPlatformService omniChannelPlatformService) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService)
        {
            _settings = settings;
            _omniChannelAuthService = omniChannelAuthService;
            _omniChannelPlatformService = omniChannelPlatformService;
        }

        public abstract Task<List<Domain.Model.OmniChannel>> GetChannelLst(IDbConnection dbConnection);
        public abstract Task<bool> CheckChannelValid(IBaseClient client, ChannelClient.Models.TokenResponse authen, Demo.OmniChannel.ChannelClient.Models.Platform platform);

        public override async Task Execute(IJobExecutionContext context)
        {
            if (!IsValidMaintenanceTime(this.GetType().Name)) return;

            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                var logId = Guid.NewGuid();
                var logObject = new LogObject(Logger, logId)
                {
                    Action = "SyncSellerStatus"
                };
                var pendingChannel = await GetChannelLst(db);
                var channelClient = new ChannelClient.Impls.ChannelClient();
                var client = channelClient.GetClientV2(ChannelType, _settings);
                foreach (var item in pendingChannel)
                {
                    var logErrorObj = new LogObject(Logger, logId)
                    {
                        Action = "CheckStatusChannel"
                    };
                    try
                    {
                        if (item.OmniChannelAuth == null || string.IsNullOrEmpty(item.OmniChannelAuth?.AccessToken))
                        {
                            continue;
                        }
                        var auth = await _omniChannelAuthService.GetChannelAuth(item, logId);
                        var platform = await _omniChannelPlatformService.GetById(item.PlatformId);
                        var shopInfo = await CheckChannelValid(client, auth, platform);
                        if (shopInfo)
                        {
                            item.Status = (byte)ChannelStatusType.Approved;
                            await db.UpdateOnlyAsync(item, onlyFields: p => new[] { "Status" }, where: x => x.Id == item.Id);
                            logErrorObj.ResponseObject = item;
                            logErrorObj.Description = "Enable channel successful";
                            logErrorObj.LogInfo();
                        }
                    }
                    catch (Exception e)
                    {
                        logErrorObj.LogError(e);
                    }
                }

                logObject.TotalItem = pendingChannel?.Count;
                logObject.LogInfo();
            }
        }
    }
}
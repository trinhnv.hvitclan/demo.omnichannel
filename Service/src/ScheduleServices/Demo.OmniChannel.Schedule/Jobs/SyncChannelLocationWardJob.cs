﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.ChannelClient.Impls;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.Utilities;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Schedule.Jobs
{
    [DisallowConcurrentExecution]
    public class SyncChannelLocationWardJob : IJob
    {
        private readonly IAppSettings _settings;
        private readonly ISendoLocationMongoService _sendoLocationMongoService;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        protected ChannelType ChannelType { get; set; }

        private ILog _logger => LogManager.GetLogger(this.GetType());

        public SyncChannelLocationWardJob(ICacheClient cacheClient,
            IAppSettings settings,
            IDbConnectionFactory dbConnectionFactory,
            ISendoLocationMongoService sendoLocationMongoService)
        {
            _settings = settings;
            _dbConnectionFactory = dbConnectionFactory;
            _sendoLocationMongoService = sendoLocationMongoService;

            ChannelType = ChannelType.Sendo;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var logId = Guid.NewGuid();
            var logObject = new LogObject(_logger, logId)
            {
                Action = "SyncChannelLocationWard"
            };
            try
            {
                string accessToken = null;

                using (var connection = _dbConnectionFactory.Open())
                {
                    var omniChannelRepo = new OmniChannelRepository(connection);
                    var defaultChannel = await omniChannelRepo.GetDefaultActiveChannel((byte)ChannelType);
                    if (!string.IsNullOrEmpty(defaultChannel?.OmniChannelAuth?.AccessToken))
                    {
                        accessToken = CryptoHelper.RijndaelDecrypt(defaultChannel?.OmniChannelAuth?.AccessToken);
                    }
                }

                if (string.IsNullOrEmpty(accessToken))
                {
                    logObject.Description = "AccessToken is null";
                    logObject.LogError();
                    return;
                }

                var sendoClient = new SendoClient(_settings);

                var locationRes = await sendoClient.GetSendoLocation(accessToken);
                var locations = locationRes.Select(x => new SendoLocation
                {
                    DistrictId = x.DistrictId,
                    FullName = x.FullName
                }).ToList();

                var (totalAddNew, totalUpdated) = await _sendoLocationMongoService.AddOrUpdate(locations);

                logObject.ResponseObject = locationRes;
                logObject.Description = $"Total add new: {totalAddNew}, total updated: {totalUpdated}";
                logObject.LogInfo();
            }
            catch (Exception e)
            {
                logObject.LogError(e);
            }
        }
    }
}
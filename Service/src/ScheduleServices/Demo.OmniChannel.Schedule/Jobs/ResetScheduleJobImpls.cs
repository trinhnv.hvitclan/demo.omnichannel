﻿using Demo.OmniChannel.ScheduleService.Interfaces;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Data;

namespace Demo.OmniChannel.Schedule.Jobs
{

    [DisallowConcurrentExecution]
    public class LazadaResetScheduleJob : BaseResetScheduleJob
    {
        public LazadaResetScheduleJob(IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IScheduleService scheduleService) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Lazada;
        }
    }
    [DisallowConcurrentExecution]
    public class ShopeeResetScheduleJob : BaseResetScheduleJob
    {
        public ShopeeResetScheduleJob(IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IScheduleService scheduleService) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Shopee;
        }
    }
    [DisallowConcurrentExecution]
    public class TikiResetScheduleJob : BaseResetScheduleJob
    {
        public TikiResetScheduleJob(IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IScheduleService scheduleService) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Tiki;
        }
    }
    [DisallowConcurrentExecution]
    public class SendoResetScheduleJob : BaseResetScheduleJob
    {
        public SendoResetScheduleJob(IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IScheduleService scheduleService) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Sendo;
        }
    }

    [DisallowConcurrentExecution]
    public class TiktokResetScheduleJob : BaseResetScheduleJob
    {
        public TiktokResetScheduleJob(IConfiguration config,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IScheduleService scheduleService) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Tiktok;
        }
    }
}
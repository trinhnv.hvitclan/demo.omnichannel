﻿using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Schedule.Jobs
{
    public abstract class BaseSyncRetryOrderJob : BaseJob
    {
        private IRetryOrderMongoService _retryOrderMongoService;
        protected IMessageFactory _messageFactory;
        public BaseSyncRetryOrderJob(IConfiguration config,
            ICacheClient cacheClient,
            IRetryOrderMongoService retryOrderMongoService,
            IMessageFactory messageFactory,
            IDbConnectionFactory dbConnectionFactory,
            IScheduleService scheduleService) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService)
        {
            _retryOrderMongoService = retryOrderMongoService;
            _messageFactory = messageFactory;
        }
        protected byte ChannelType { get; set; }
        public override async Task Execute(IJobExecutionContext context)
        {
           
            var totalRetry = Configuration.GetValue<int>("totalOrderRetry");
            var pageSize = Configuration.GetValue<int>("retryPageSize");
            var totalRecords = await _retryOrderMongoService.TotalByChannelType(ChannelType, totalRetry);
            var total = totalRecords / pageSize;
            total = totalRecords % pageSize == 0 ? total : total + 1;
            for (int i = 0; i < total; i++)
            {
                var logId = Guid.NewGuid();
                var syncRetryOrderJobLog = new LogObject(Logger, logId)
                {
                    Action =
                        $"{EnumHelper.ToDescription((ChannelType) ChannelType)}_{nameof(BaseSyncRetryOrderJob)}"
                };
                var currentItems =
                    await _retryOrderMongoService.GetByChannelType(ChannelType, totalRetry, pageSize, i * pageSize);
                foreach (var item in currentItems)
                {
                    await PushMessageQueue(item, logId);
                }
                syncRetryOrderJobLog.TotalItem = currentItems.Count;
                syncRetryOrderJobLog.LogInfo(false);
            }

            var totalOrderDeleted = await _retryOrderMongoService.RemoveOldErrorOrderByChannelType(ChannelType, totalRetry, DateTime.Now.AddDays(-30));
            if (totalOrderDeleted > 0)
            {
                var removeRetryOrderJobLog = new LogObject(Logger, Guid.NewGuid())
                {
                    Action = $"{EnumHelper.ToDescription((ChannelType)ChannelType)}_{nameof(BaseSyncRetryOrderJob)}_Remove",
                    TotalItem = totalOrderDeleted
                };
                removeRetryOrderJobLog.LogInfo();
            }
        }


        public virtual Task PushMessageQueue(RetryOrder order, Guid logId)
        {
            switch ((byte)ChannelType)
            {
                case (byte)Sdk.Common.ChannelType.Lazada:
                    {
                        var createOrder = new LazadaCreateOrderMessage
                        {
                            ChannelId = order.ChannelId,
                            ChannelType = order.ChannelType,
                            BranchId = order.BranchId,
                            KvEntities = order.KvEntities,
                            RetailerId = order.RetailerId,
                            OrderId = order.OrderId,
                            Order = order.Order,
                            GroupId = order.GroupId,
                            PriceBookId = order.PriceBookId,
                            IsFirstSync = order.IsFirstSync,
                            OrderStatus = order.OrderStatus,
                            RetryCount = order.RetryCount,
                            IsDbException = order.IsDbException,
                            LogId = logId,
                            PlatformId = order.PlatformId
                        };
                        using (var mq = _messageFactory.CreateMessageProducer())
                        {
                            mq.Publish(createOrder);
                        }

                        break;
                    }

                case (byte)Sdk.Common.ChannelType.Shopee:
                    {
                        var createOrder = new ShopeeCreateOrderMessage
                        {
                            ChannelId = order.ChannelId,
                            ChannelType = order.ChannelType,
                            BranchId = order.BranchId,
                            KvEntities = order.KvEntities,
                            RetailerId = order.RetailerId,
                            OrderId = order.OrderId,
                            Order = order.Order,
                            GroupId = order.GroupId,
                            PriceBookId = order.PriceBookId,
                            IsFirstSync = order.IsFirstSync,
                            OrderStatus = order.OrderStatus,
                            RetryCount = order.RetryCount,
                            IsDbException = order.IsDbException,
                            LogId = logId,
                            SourceQueue = "Queue is created from BaseSyncRetryOrderJob",
                            PlatformId = order.PlatformId
                        };
                        using (var mq = _messageFactory.CreateMessageProducer())
                        {
                            mq.Publish(createOrder);
                        }

                        break;
                    }

                case (byte)Sdk.Common.ChannelType.Tiki:
                    {
                        var createOrder = new TikiCreateOrderMessage
                        {
                            ChannelId = order.ChannelId,
                            ChannelType = order.ChannelType,
                            BranchId = order.BranchId,
                            KvEntities = order.KvEntities,
                            RetailerId = order.RetailerId,
                            OrderId = order.OrderId,
                            Order = order.Order,
                            GroupId = order.GroupId,
                            PriceBookId = order.PriceBookId,
                            IsFirstSync = order.IsFirstSync,
                            OrderStatus = order.OrderStatus,
                            RetryCount = order.RetryCount,
                            IsDbException = order.IsDbException,
                            LogId = logId,
                            PlatformId = order.PlatformId
                        };
                        using (var mq = _messageFactory.CreateMessageProducer())
                        {
                            mq.Publish(createOrder);
                        }

                        break;
                    }
                case (byte)Sdk.Common.ChannelType.Sendo:
                    {
                        var createOrder = new SendoCreateOrderMessage
                        {
                            ChannelId = order.ChannelId,
                            ChannelType = order.ChannelType,
                            BranchId = order.BranchId,
                            KvEntities = order.KvEntities,
                            RetailerId = order.RetailerId,
                            OrderId = order.OrderId,
                            Order = order.Order,
                            GroupId = order.GroupId,
                            PriceBookId = order.PriceBookId,
                            IsFirstSync = order.IsFirstSync,
                            OrderStatus = order.OrderStatus,
                            RetryCount = order.RetryCount,
                            IsDbException = order.IsDbException,
                            LogId = logId,
                            PlatformId = order.PlatformId
                        };
                        using (var mq = _messageFactory.CreateMessageProducer())
                        {
                            mq.Publish(createOrder);
                        }

                        break;
                    }
            }
            return Task.CompletedTask;
        }
    }
}
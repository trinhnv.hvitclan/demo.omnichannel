﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Schedule.Jobs
{
    [DisallowConcurrentExecution]
    public class LazadaSyncModifiedProductJob : BaseSyncModifiedProductJob
    {
        public LazadaSyncModifiedProductJob(IConfiguration config,
                                      ICacheClient cacheClient,
                                      IDbConnectionFactory dbConnectionFactory,
                                      IScheduleService scheduleService,
                                      IChannelBusiness channelBusiness,
                                      IMessageFactory messageFactory) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService,
            channelBusiness,
            messageFactory)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Lazada;
        }
    }

    [DisallowConcurrentExecution]
    public class ShopeeSyncModifiedProductJob : BaseSyncModifiedProductJob
    {
        public ShopeeSyncModifiedProductJob(IConfiguration config,
                                      ICacheClient cacheClient,
                                      IDbConnectionFactory dbConnectionFactory,
                                      IScheduleService scheduleService,
                                      IChannelBusiness channelBusiness,
                                      IMessageFactory messageFactory) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService,
            channelBusiness,
            messageFactory)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Shopee;
        }
    }

    [DisallowConcurrentExecution]
    public class TikiSyncModifiedProductJob : BaseSyncModifiedProductJob
    {
        private readonly IMessageFactory _messageFactory;
        public TikiSyncModifiedProductJob(IConfiguration config,
                                      ICacheClient cacheClient,
                                      IDbConnectionFactory dbConnectionFactory,
                                      IScheduleService scheduleService,
                                      IChannelBusiness channelBusiness,
                                      IMessageFactory messageFactory) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService,
            channelBusiness,
            messageFactory)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Tiki;
            _messageFactory = messageFactory;
        }

        public override async void PushToMQ(ScheduleDto item, Domain.Model.OmniChannel channel, KvRetailer kvRetailer, KvGroup kvGroup, Guid logId)
        {
            var message = new SyncModifiedProductMessage
            {
                ChannelId = item.ChannelId,
                ChannelType = item.ChannelType,
                RetailerId = channel.RetailerId,
                BranchId = channel.BranchId,
                LastSyncDateTime = item.LastSync,
                ChannelScheduleId = item.Id,
                RetailerCode = kvRetailer?.Code,
                KvEntities = kvGroup.ConnectionString,
                ScheduleDto = item,
                LogId = Guid.NewGuid(),
                ParentLogIds = new List<Guid> { logId }
            };
            await Task.Delay(500);
            using (var mq = _messageFactory.CreateMessageProducer())
            {
                mq.Publish(message);
            }
        }
    }

    [DisallowConcurrentExecution]
    public class SendoSyncModifiedProductJob : BaseSyncModifiedProductJob
    {
        public SendoSyncModifiedProductJob(IConfiguration config,
                                      ICacheClient cacheClient,
                                      IDbConnectionFactory dbConnectionFactory,
                                      IScheduleService scheduleService,
                                      IChannelBusiness channelBusiness,
                                      IMessageFactory messageFactory) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService,
            channelBusiness,
            messageFactory)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Sendo;
        }
    }

    [DisallowConcurrentExecution]
    public class TiktokSyncModifiedProductJob : BaseSyncModifiedProductJob
    {
        public TiktokSyncModifiedProductJob(IConfiguration config,
                                      ICacheClient cacheClient,
                                      IDbConnectionFactory dbConnectionFactory,
                                      IScheduleService scheduleService,
                                      IChannelBusiness channelBusiness,
                                      IMessageFactory messageFactory) : base(config,
            cacheClient,
            dbConnectionFactory,
            scheduleService,
            channelBusiness,
            messageFactory)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Tiktok;
        }
    }
}

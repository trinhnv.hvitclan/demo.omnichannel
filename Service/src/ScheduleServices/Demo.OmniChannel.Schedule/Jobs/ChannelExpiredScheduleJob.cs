﻿using Demo.OmniChannel.ShareKernel.Dto;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.EventBus;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Infrastructure.EventBus.RabbitMQ;
using Microsoft.Extensions.Options;

namespace Demo.OmniChannel.Schedule.Jobs
{
    public class ChannelExpiredScheduleJob : BaseJob
    {
        private readonly IIntegrationEventService _integrationEventService;
        private readonly string _handlerChannelExpiredRoutingKey;
        private readonly RabbitMqEventBusConfiguration _rabbitMqEventBusConfiguration;
        public ChannelExpiredScheduleJob(IConfiguration config,
           ICacheClient cacheClient,
           IDbConnectionFactory dbConnectionFactory,
           IScheduleService scheduleService,
           IIntegrationEventService integrationEventService,
           IOptions<RabbitMqEventBusConfiguration> rabbitMqEventBusConfiguration
           ) : base(config,
           cacheClient,
           dbConnectionFactory,
           scheduleService)
        {
            _integrationEventService = integrationEventService;
            _handlerChannelExpiredRoutingKey = RoutingKey.OmniPushChannelExpired;
            _rabbitMqEventBusConfiguration = rabbitMqEventBusConfiguration.Value;
        }

        public async override Task Execute(IJobExecutionContext context)
        {

            var logger = new LogObject(Logger, Guid.NewGuid())
            {
                Action = $"{nameof(ChannelExpiredScheduleJob)}"
            };
            var allChannels = new List<OmniChannelDto>();

            try
            {
                using (var db = DbConnectionFactory.OpenDbConnection())
                {
                    var omniChannelRepository = new OmniChannelRepository(db);
                    var jobChannel = GetJobChannel(nameof(ChannelExpiredScheduleJob));
                    #region Step 1: Scan các shop chưa xóa, đã hết hạn và chưa từng được quét để gửi thông báo
                    var skip = 0;
                    while (true)
                    {
                        var deactiveChannels = await omniChannelRepository.GetAllDeactiveChannels(jobChannel?.IncludeRetail?.ToList(), jobChannel?.ExcludeRetail?.ToList(), skip);
                        var countChannel = deactiveChannels.Count;
                        if (countChannel <= 0)
                        {
                            break;
                        }

                        skip += deactiveChannels.Count;
                        await SendNotificationAsync(deactiveChannels);
                        allChannels.AddRange(deactiveChannels);
                    }
                }

                logger.ResponseObject = $"Channels: {allChannels.Count()}";
                logger.LogInfo();
                #endregion
            }
            catch (Exception ex)
            {
                logger.LogError(ex);
                throw;
            }
        }


        private async Task SendNotificationAsync(List<OmniChannelDto> deactiveChannels)
        {
            var channelByRetailers = deactiveChannels.GroupBy(t => t.RetailerId);

            foreach (var group in channelByRetailers)
            {
                var kvRetailer = await GetRetailer(group.Key);
                if (kvRetailer == null)
                {
                    continue;
                }

                var kvGroup = await GetRetailerGroup(kvRetailer.GroupId);
                if (kvGroup == null || string.IsNullOrEmpty(kvGroup.ConnectionString))
                {
                    continue;
                }

                var channels = group.ToList();
                _integrationEventService.AddEventWithRoutingKeyAsync(new ChannelExpiredByRetailer()
                {
                    RetailerId = kvRetailer.Id,
                    OmniChannelExpires = channels.Select(x => new OmniChannelExpiredPayload()
                    {
                        ChannelId = x.Id,
                        Name = x.Name,
                        ChannelType = x.Type
                    }).ToList(),

                }, _handlerChannelExpiredRoutingKey, _rabbitMqEventBusConfiguration.ExchangeSendNotification);

            }
        }
    }

    public class ChannelExpiredByRetailer : IntegrationEvent
    {
        public int RetailerId { get; set; }
        public List<OmniChannelExpiredPayload> OmniChannelExpires { get; set; }
    }

    public class OmniChannelExpiredPayload
    {
        public long ChannelId { get; set; }
        public string Name { get; set; }
        public byte ChannelType { get; set; }
        public string TypeName
        {
            get
            {
                return EnumHelper.EnumTypeTo<ChannelType>(ChannelType).ToString();
            }
        }
    }
}

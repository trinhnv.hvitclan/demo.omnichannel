﻿using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Microsoft.Extensions.Configuration;
using Quartz;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.Schedule.Jobs
{
    [DisallowConcurrentExecution]
    public class ShopeeSyncSyncRetryOrderJob : BaseSyncRetryOrderJob
    {
        public ShopeeSyncSyncRetryOrderJob(IConfiguration config,
            ICacheClient cacheClient,
            IRetryOrderMongoService retryOrderMongoService,
            IDbConnectionFactory dbConnectionFactory,
            IMessageFactory messageFactory,
            IScheduleService scheduleService) : base(config,
            cacheClient,
            retryOrderMongoService,
            messageFactory,
            dbConnectionFactory,
            scheduleService)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Shopee;
        }
    }



    [DisallowConcurrentExecution]
    public class LazadaSyncSyncRetryOrderJob : BaseSyncRetryOrderJob
    {
        public LazadaSyncSyncRetryOrderJob(IConfiguration config,
            ICacheClient cacheClient,
            IRetryOrderMongoService retryOrderMongoService,
            IMessageFactory messageFactory,
            IDbConnectionFactory dbConnectionFactory,
            IScheduleService scheduleService) : base(config,
            cacheClient,
            retryOrderMongoService,
            messageFactory,
            dbConnectionFactory,
            scheduleService)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Lazada;
        }
    }
    [DisallowConcurrentExecution]
    public class TikiSyncSyncRetryOrderJob : BaseSyncRetryOrderJob
    {
        public TikiSyncSyncRetryOrderJob(IConfiguration config,
            ICacheClient cacheClient,
            IRetryOrderMongoService retryOrderMongoService,
            IMessageFactory messageFactory,
            IDbConnectionFactory dbConnectionFactory,
            IScheduleService scheduleService) : base(config,
            cacheClient,
            retryOrderMongoService,
            messageFactory,
            dbConnectionFactory,
            scheduleService)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Tiki;
        }
    }

    [DisallowConcurrentExecution]
    public class SendoSyncSyncRetryOrderJob : BaseSyncRetryOrderJob
    {
        public SendoSyncSyncRetryOrderJob(IConfiguration config,
            ICacheClient cacheClient,
            IRetryOrderMongoService retryOrderMongoService,
            IMessageFactory messageFactory,
            IDbConnectionFactory dbConnectionFactory,
            IScheduleService scheduleService) : base(config,
            cacheClient,
            retryOrderMongoService,
            messageFactory,
            dbConnectionFactory,
            scheduleService)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Sendo;
        }
    }
}
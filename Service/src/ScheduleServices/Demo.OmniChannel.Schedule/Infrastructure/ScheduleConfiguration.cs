﻿using System;
using System.Linq;
using System.Reflection;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Schedule.Jobs;
using Demo.OmniChannel.Schedule.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using Serilog;

namespace Demo.OmniChannel.Schedule.Infrastructure
{
    public static class ScheduleConfiguration
    {
        public static void UseQuartz(this IServiceCollection services, params Type[] jobs)
        {
            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
            services.AddHostedService<QuartzHostedService>();
            services.AddSingleton<IJobFactory, QuartzJobFactory>();
            services.Add(jobs.Select(jobType => new ServiceDescriptor(jobType, jobType, ServiceLifetime.Singleton)));
            services.AddSingleton(provider =>
            {
                var schedulerFactory = new StdSchedulerFactory();
                var scheduler = schedulerFactory.GetScheduler().GetAwaiter().GetResult();
                scheduler.JobFactory = provider.GetService<IJobFactory>();
                scheduler.Start();
                return scheduler;
            });
        }

        public static void UseRedisMqHandler(this IServiceCollection services, KvRedisConfig redisMqConfig)
        {
            var lsName = redisMqConfig.EventMessages.Where(t => t.IsActive).Select(v => v.Name);
            var types = typeof(BaseService).GetTypeInfo().Assembly.DefinedTypes
                .Where(p => p.GetTypeInfo().IsAssignableFrom(p.AsType()) && p.IsClass && lsName.Contains(p.Name)).Select(p => p.AsType());
            try
            {
                foreach (var type in types)
                {
                    services.AddTransient(typeof(IHostedService), type);
                }
            }
            catch (Exception e)
            {
                Log.Logger.Error($"Register {nameof(IHostedService)} error: {e.Message}", e);
                throw;
            }
        }
        public static void RegisterStartJob(this IServiceCollection services,string nameService, string cronExpression)
        {
            var types = typeof(BaseJob).GetTypeInfo().Assembly.DefinedTypes
                .Where(p => p.GetTypeInfo().IsAssignableFrom(p.AsType()) && p.IsClass && p.Name== nameService).Select(p => p.AsType());
            foreach (var type in types)
            {
                services.AddSingleton(type);
                services.AddSingleton(new JobSchedule(jobType: type, cronExpression: cronExpression));
            }
           
        }
        public static void RegisterStartJob<TJob>(this IServiceCollection services, string cronExpression) where TJob: IJob
        {
            services.AddSingleton(typeof(TJob));
            services.AddSingleton(new JobSchedule(jobType: typeof(TJob), cronExpression: cronExpression));
        }

        public class JobSchedule
        {
            public JobSchedule(Type jobType, string cronExpression)
            {
                JobType = jobType;
                CronExpression = cronExpression;
            }

            public Type JobType { get; }
            public string CronExpression { get; }
        }
    }
}

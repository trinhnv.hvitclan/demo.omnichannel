﻿using Demo.OmniChannel.Business;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.DCControl.ConfigureService;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Infrastructure.EventBus.Extentions;
using Demo.OmniChannel.Infrastructure.IoC;
using Demo.OmniChannel.Infrastructure.Logging;
using Demo.OmniChannel.KafkaClient;
using Demo.OmniChannel.MongoService.Common;
using Demo.OmniChannel.MongoService.Impls;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Schedule.Common;
using Demo.OmniChannel.Schedule.Infrastructure;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.Services.Impls;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Impls;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.Messaging.Redis;
using ServiceStack.OrmLite;
using ServiceStack.Redis;
using System.Linq;

namespace Demo.OmniChannel.Schedule
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            var builder = new ConfigurationBuilder()
                .AddConfiguration(configuration)
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        //User net core
        public void ConfigureServices(IServiceCollection services)
        {
            #region Register ServiceStack License
            Licensing.RegisterLicense(Configuration.GetSection("servicestack:license").Value);
            #endregion
            services.AddSingleton(c => Configuration);
            services.AddSingleton<IAppSettings>(x => new NetCoreAppSettings(Configuration));
            #region Sql ConnectionString
            var kvConn = new KvSqlConnectString();
            Configuration.GetSection("SqlConnectStrings").Bind(kvConn);
            var connectFactory = new OrmLiteConnectionFactory(kvConn.KvChannelEntities, SqlServer2016Dialect.Provider);
            connectFactory.RegisterConnection(nameof(kvConn.KvMasterEntities), kvConn.KvMasterEntities, SqlServer2016Dialect.Provider);
            connectFactory.RegisterConnection(nameof(kvConn.KvChannelEntities), kvConn.KvChannelEntities, SqlServer2016Dialect.Provider);

            foreach (var item in kvConn.KvEntitiesDC1)
            {
                connectFactory.RegisterConnection(item.Name.ToLower(), item.Value, SqlServer2016Dialect.Provider);
            }
            SqlServerDialect.Provider.GetStringConverter().UseUnicode = true;
            services.AddSingleton<IDbConnectionFactory>(c => connectFactory);
            //services.AddScoped(c => connectFactory.OpenDbConnection());
            #endregion

            #region Redis
            var redisConfig = new KvRedisConfig();
            Configuration.GetSection("redis:cache").Bind(redisConfig);
            services.AddSingleton(sc => KvRedisPoolManager.GetClientsManager(redisConfig));
            services.AddSingleton<ICacheClient, KvCacheClient>();
            services.AddSingleton<IKvLockRedis, KvLockRedis>();

            var redisMqConfig = new KvRedisConfig();
            Configuration.GetSection("redis:message").Bind(redisMqConfig);
            var redisFactory = KvRedisPoolManager.GetClientsManager(redisMqConfig);
            var mqHost = new RedisMqServer(redisFactory, retryCount: 2);
            services.AddSingleton<IMessageService>(mqHost);
            services.AddSingleton<IMessageFactory>(c => new RedisMessageFactory(redisFactory));
            services.AddSingleton(redisMqConfig);
            #endregion

            #region Register kafka
            var kafka = new Kafka();
            Configuration.GetSection("Kafka").Bind(kafka);
            KafkaClient.KafkaClient.Instance.SetKafkaConsumerConfig(kafka);
            KafkaClient.KafkaClient.Instance.SetKafkaProducerConfig(kafka);
            KafkaClient.KafkaClient.Instance.InitProducer();
            var kafka2 = new Kafka();
            Configuration.GetSection("Kafka2").Bind(kafka2);
            KafkaClient.KafkaClient.Instance.SetKafkaProducerConfig(kafka2, true);
            KafkaClient.KafkaClient.Instance.InitProducer(true);
            MissingMessageSingleton.Instance.LoopDequeue();
            #endregion

            #region Register Mongo
            var mongoSetting = new MongoDbSettings();
            Configuration.GetSection("mongodb").Bind(mongoSetting);
            if (!string.IsNullOrEmpty(mongoSetting.ConnectionString))
            {
                services.Configure<MongoDbSettings>(Configuration.GetSection("mongodb"));
                services.UsingMongoDb(Configuration);
                services.AddTransient<IRetryOrderMongoService, RetryOrderMongoService>();
                services.AddTransient<IRetryInvoiceMongoService, RetryInvoiceMongoService>();
                services.AddTransient<IProductMongoService, ProductMongoService>();
                services.AddTransient<IProductMappingService, ProductMappingService>();
                services.AddTransient<ISendoLocationMongoService, SendoLocationMongoService>();
                services.AddTransient<IOrderMongoService, OrderMongoService>();
                services.AddTransient<IInvoiceMongoService, InvoiceMongoService>();
            }
            #endregion
            //Register OmmiChannel repositories
            services.RegisterOmmiChannelRepositories();

            services.AddScoped<IAuditTrailInternalClient>(c => new AuditTrailInternalClient(c.GetService<IAppSettings>()));

            //Register Job
            var quartzSetting = new KvQuartz();
            Configuration.GetSection("Quartz").Bind(quartzSetting);
            if (quartzSetting.JobChannels != null && quartzSetting.JobChannels.Any())
            {
                services.RegisterJobs(Configuration, quartzSetting);
            }

            if (redisMqConfig.EventMessages != null && redisMqConfig.EventMessages.Any())
            {
                services.AddScoped<IMappingBusiness, MappingBusiness>();
                services.AddScoped<IPriceBusiness, PriceBusiness>();
                services.UseRedisMqHandler(redisMqConfig);
            }
            
            #region Register Channel Third Party
            services.AddSingleton(c => new ChannelClient.Impls.ChannelClient());
            services.AddScoped<IOmniChannelPlatformService, OmniChannelPlatformService>();
            #endregion

            #region RabbitMQ Producer
            services.AddConfigEventBus(Configuration);
            services.AddEventBus();
            #endregion

            services.AddScoped<IScheduleService>(c =>
                new ScheduleService.Impls.ScheduleService(c.GetRequiredService<IRedisClientsManager>(),
                    c.GetService<IAppSettings>()));
            services.AddScoped<IChannelBusiness, ChannelBusiness>();
            services.AddScoped<IOmniChannelAuthService, OmniChannelAuthService>();
            services.AddScoped<IOmniChannelPlatformService, OmniChannelPlatformService>();
            services.UseQuartz();
            SetEncryptPassPhrase();

            services.RegisterApplicationDcControl(new ApplicationDcInputOptions
            {
                ServiceName = "ScheduleService",
                AppSettings = new NetCoreAppSettings(Configuration)
            });

            Configuration.ConfigLog(GetType(), "Demo.OmmiChannel.Schedule is started.");
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime lifetime, IMessageService messageService)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            lifetime.ApplicationStarted.Register(messageService.Start);
        }

        private void SetEncryptPassPhrase()
        {
            CryptoHelper.PassPhrase = Configuration.GetSection("EncryptPassPhrase").Value;

            if (string.IsNullOrWhiteSpace(CryptoHelper.PassPhrase))
            {
                throw new KvException("Setting EncryptPassPhrase invalid. Check setting file, please.");
            }
        }
    }

    internal static class AppConfigHelper
    {
        public static void RegisterJobs(this IServiceCollection services, IConfiguration configuration,
            KvQuartz quartzSetting)
        {
           
            foreach (var item in quartzSetting.JobChannels.Where(t => t != null && t.IsActive))
            {
                services.RegisterStartJob(item.JobName, item.CronExpression);
            }
        }
    }
}

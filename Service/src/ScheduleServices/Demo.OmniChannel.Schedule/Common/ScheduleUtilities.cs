﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Demo.OmniChannel.Repository.Interfaces;

namespace Demo.OmniChannel.Schedule.Common
{
    public class ScheduleUtilities
    {
        public static async Task ResetScheduleRunningStatus(IOmniChannelScheduleRepository omniChannelScheduleRepo)
        {
            var scheduleChannelRunning = (await omniChannelScheduleRepo.WhereAsync(x => x.IsRunning));
            scheduleChannelRunning.ForEach(x => x.IsRunning = false);
            await omniChannelScheduleRepo.UpdateAllAsync(scheduleChannelRunning);
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Demo.OmniChannel.Schedule.Common
{
    public class KvQuartz
    {
        public List<JobChannel> JobChannels { get; set; }
    }
    public class JobChannel
    {
        public byte ChannelType { get; set; }
        public string JobName { get; set; }
        public string CronExpression { get; set; }
        public string Description { get; set; }
        public List<int> IncludeRetail { get; set; }
        public List<int> ExcludeRetail { get; set; }
        public bool IsActive { get; set; }
        public List<long> ChannelIds { get; set; }

        #region Extent
        //for refresh token
        public double RefreshTokenBeforeMinutes { get; set; }
        #endregion
    }

    public class MaintenanceTime
    {
        public bool IsEnable { get; set; }
        public TimeSpan Start { get; set; }
        public TimeSpan End { get; set; }
    }
}

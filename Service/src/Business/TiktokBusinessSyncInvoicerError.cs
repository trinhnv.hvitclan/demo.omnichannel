﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.DomainService.Interfaces;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannelCore.Api.Sdk.Common;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Business
{
    public class TiktokBusinessSyncInvoicerError : BaseSyncInvoiceErrorBusiness, ITiktokBusinessSyncInvoicerError
    {
        public TiktokBusinessSyncInvoicerError(
        ILogger<TiktokBusinessSyncInvoicerError> logger,
        IAppSettings settings,
        ICacheClient cacheClient,
        IDbConnectionFactory dbConnectionFactory,
        IKvLockRedis kvLockRedis,
        IProductMappingService productMappingService,
        IInvoiceMongoService invoiceMongoService,
        IInvoiceInternalClient invoiceInternalClient,
        IAuditTrailInternalClient auditTrailInternalClient,
        IOmniChannelAuthService channelAuthService,
        IRetryInvoiceMongoService retryInvoiceMongoService,
        IOrderInternalClient orderInternalClient,
        IChannelBusiness channelBusiness,
        IDeliveryInfoInternalClient deliveryInfoInternalClient,
        IDeliveryPackageInternalClient deliveryPackageInternalClient,
        ICustomerInternalClient customerInternalClient,
        IOmniChannelSettingService omniChannelSettingService,
        ISurChargeInternalClient surChargeInternalClient,
        IOmniChannelPlatformService omniChannelPlatformService,
        ICreateInvoiceDomainService createInvoiceDomainService,
        IMessageService messageService,
        IChannelClient channelClient)
        : base(logger,
              settings,
              cacheClient,
              dbConnectionFactory,
              kvLockRedis,
              productMappingService,
              invoiceMongoService,
              invoiceInternalClient,
              auditTrailInternalClient,
              channelAuthService,
              retryInvoiceMongoService,
              orderInternalClient,
              channelBusiness,
              deliveryInfoInternalClient,
              deliveryPackageInternalClient,
              customerInternalClient,
              omniChannelSettingService,
              surChargeInternalClient,
              omniChannelPlatformService,
              createInvoiceDomainService,
              messageService,
              channelClient)
        {
        }
        protected override async Task UpdateFeeDeliveryInOrder(string connectStringName, KvInternalContext coreContext, byte channelType, long? deliveryInfoId, decimal? price, string feeJson)
        {
            if (price.HasValue && !string.IsNullOrEmpty(feeJson) && deliveryInfoId.HasValue)
            {
                using (var db = await _dbConnectionFactory.OpenAsync(connectStringName.ToLower()))
                {
                    var deliveryInfoRepository = new DeliveryInfoRepository(db);
                    var existDelivery = await deliveryInfoRepository.SingleAsync(x => x.Id == deliveryInfoId);
                    if (existDelivery != null && existDelivery.FeeJson != feeJson)
                    {
                        var deliveryInfo = existDelivery.ConvertTo<OmniChannelCore.Api.Sdk.Models.DeliveryInfo>();
                        deliveryInfo.UpdateFeeJson(price, feeJson);
                        await _deliveryInfoInternalClient.UpdateDeliveryPriceAndFeeJson(coreContext, deliveryInfo);
                    }
                }
            }
        }
    }
}

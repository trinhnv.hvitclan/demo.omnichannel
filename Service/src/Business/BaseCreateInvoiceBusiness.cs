﻿using Demo.Audit.Model.Message;
using Demo.OmniChannel.Business.Validators;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.RequestDTO;
using Demo.OmniChannel.DomainService.Helper;
using Demo.OmniChannel.DomainService.Interfaces;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.RedisStreamMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Demo.OmniChannelCore.Api.Sdk.Models;
using Newtonsoft.Json;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvoiceDetail = Demo.OmniChannelCore.Api.Sdk.Models.InvoiceDetail;
using InvoiceDelivery = Demo.OmniChannelCore.Api.Sdk.Models.InvoiceDelivery;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.Services.LogginConfiguration;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.Infrastructure.Common;
using ServiceStack.Messaging;
using Demo.OmniChannel.ShareKernel.EventLogMessage;
using Demo.OmniChannel.Redis;

namespace Demo.OmniChannel.Business
{
    public abstract class BaseCreateInvoiceBusiness : BaseBusiness, IBaseCreateInvoiceBusiness
    {
        protected readonly IDeliveryInfoInternalClient _deliveryInfoInternalClient;
        private readonly IInvoiceInternalClient _invoiceInternalClient;
        private readonly IAuditTrailInternalClient _auditTrailInternalClient;
        protected readonly IOrderInternalClient _orderInternalClient;
        protected readonly ICustomerInternalClient _customerInternalClient;
        private readonly ISurChargeInternalClient _surChargeInternalClient;
        private readonly IPriceBookInternalClient _priceBookInternalClient;
        private readonly IProductInternalClient _productInternalClient;
        private readonly IInvoiceMongoService _invoiceMongoService;
        private readonly IRetryInvoiceMongoService _retryInvoiceMongoService;
        private readonly ICreateInvoiceDomainService _createInvoiceDomainService;
        private readonly ICreateOrderDomainService _createOrderDomainService;
        private readonly IIntegrationEventService _integrationEventService;
        private readonly IOmniChannelSettingService _omniChannelSettingService;
        private readonly ChannelClient.Interfaces.IChannelClient _channelClient;
        protected readonly ICacheClient _cacheClient;
        protected readonly IKvLockRedis _kvLockRedis;

        public BaseCreateInvoiceBusiness(
            IOmniChannelSettingService omniChannelSettingService,
            IInvoiceMongoService invoiceMongoService,
            IDeliveryInfoInternalClient deliveryInfoInternalClient,
            IRetryInvoiceMongoService retryInvoiceMongoService,
            IInvoiceInternalClient invoiceInternalClient,
            IAuditTrailInternalClient auditTrailInternalClient,
            IOrderInternalClient orderInternalClient,
            ChannelClient.Interfaces.IChannelClient channelClient,
            IAppSettings settings,
            ICustomerInternalClient customerInternalClient,
            ICacheClient cacheClient,
            ICreateOrderDomainService createOrderDomainService,
            ISurChargeInternalClient surChargeInternalClient,
            IIntegrationEventService integrationEventService,
            ICreateInvoiceDomainService createInvoiceDomainService,
            IPriceBookInternalClient priceBookInternalClient,
            IProductInternalClient productInternalClient,
            ServiceStack.Data.IDbConnectionFactory dbConnectionFactory,
            IMessageService messageService,
            IKvLockRedis kvLockRedis)
            : base(settings, dbConnectionFactory, messageService)
        {
            _omniChannelSettingService = omniChannelSettingService;
            _invoiceMongoService = invoiceMongoService;
            _deliveryInfoInternalClient = deliveryInfoInternalClient;
            _retryInvoiceMongoService = retryInvoiceMongoService;
            _invoiceInternalClient = invoiceInternalClient;
            _auditTrailInternalClient = auditTrailInternalClient;
            _orderInternalClient = orderInternalClient;
            _customerInternalClient = customerInternalClient;
            _createOrderDomainService = createOrderDomainService;
            _surChargeInternalClient = surChargeInternalClient;
            _integrationEventService = integrationEventService;
            _createInvoiceDomainService = createInvoiceDomainService;
            _priceBookInternalClient = priceBookInternalClient;
            _productInternalClient = productInternalClient;
            _channelClient = channelClient;
            _cacheClient = cacheClient;
            _kvLockRedis = kvLockRedis;
        }

        #region Public method

        /// <summary>
        /// Handle message create invoice
        /// </summary>
        /// <param name="message"> message invoice</param>
        /// <returns></returns>
        public async Task<bool> HandleCreateInvoice(CreateInvoiceMessage message)
        {
            try
            {
                PushLishMQToCreateLog((int)LogType.Info, message.LogId,
                    "ReceiveInvoiceInfoFromGRPC", message.ToJson(), null, 0, 0, null, null);
                using (var lockByOrderId = _kvLockRedis.GetLockFactory())
                {
                    using (lockByOrderId.AcquireLock(string.Format(KvConstant.LockProcessInvoiceV2,
                        message.RetailerId, message.OrderId), TimeSpan.FromSeconds(60)))
                    {
                        await ProgressMessageInvoice(message);
                    }
                }
            }
            catch (Exception ex)
            {
                await HandleException(ex, message);
            }
            finally
            {
                PublishUpdatePaymentTransaction(message);
            }
            return true;
        }


        /// <summary>
        /// Progress message create invoice
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task<bool> ProgressMessageInvoice(CreateInvoiceMessage message)
        {
            var log = new SeriLogObject(message.LogId)
            {
                Action = ExceptionType.CreateInvoice,
                RetailerId = message.RetailerId,
                OmniChannelId = message.ChannelId,
                RequestObject = message
            };
            var connectStringName = message.KvEntities.Substring(message.KvEntities.IndexOf('=') + 1);
            var context = await ContextHelper.GetExecutionContext(_cacheClient, DbConnectionFactory, message.RetailerId,
                connectStringName, message.BranchId, Settings?.Get<int>("ExecutionContext"), message.LogId);
            var coreContext = context.ConvertTo<OmniChannelCore.Api.Sdk.Common.KvInternalContext>();
            var code = ChannelTypeHelper.GetOrderPrefix(message.ChannelType);
            var existOrder = await _orderInternalClient.GetOrderByCodeAsync(coreContext, $"{code}_{message.OrderId}");
            var validateResult = await new CreateInvoiceValidators(
                _invoiceInternalClient, message.ChannelType, coreContext).ValidateAsync(existOrder);

            if (!validateResult.IsValid)
            {
                await RemoveErrorInvoiceInMongo(message.OrderId, message.BranchId, message.ChannelId);
                PushMessageToInvoiceTrackingEvent(message, validateResult.Errors.FirstOrDefault()?.ErrorMessage);
                log.ResponseObject = validateResult.Errors.FirstOrDefault();
                log.LogWarning();
                return false;
            }

            var settings = await _omniChannelSettingService.GetChannelSettings(message.ChannelId, message.RetailerId);
            var deliveryInfo = await _deliveryInfoInternalClient.GetLastByOrderId(coreContext, message.KvOrderId);
            if (deliveryInfo == null)
            {
                log.RequestObject = "DeliveryInfo is null!";
                log.LogError();
                PushMessageToInvoiceTrackingEvent(message, "DeliveryInfo is null!");
                return false;
            }

            var kvOrder = existOrder.ConvertTo<KvOrder>();

            await UpdateDeliveryInfo(coreContext, message, kvOrder, deliveryInfo);

            //Khởi tạo hóa đơn
            var invoices = await GetKvInvoice(
                message, kvOrder,
                deliveryInfo, settings.IsConfirmReturning,
                coreContext);

            if (invoices == null || !invoices.Any())
            {
                log.Description = "No created invoice!";
                log.LogWarning();
                PushMessageToInvoiceTrackingEvent(message, "No created invoice");
                return false;
            }

            //Lấy thông tin khách hàng 
            var customer = kvOrder.CustomerId.HasValue
                ? (await _customerInternalClient.GetCustomerByIdAsync(coreContext, kvOrder.CustomerId.Value)
                )?.ConvertTo<Customer>()
                : null;

            //Lấy thông tin bảng giá
            var extra = await GetExtraAsync(coreContext, message.PriceBookId);

            invoices.ForEach(invoice =>
            {
                invoice.UpdatePriceBookId(extra.PriceBookId);
                invoice.UpdateSaleChannelId(existOrder.SaleChannelId);
            });

            var invoicesFilter = invoices.Where(x => x.NewStatus != null).ToList();

            //Tạo hóa đơn
            foreach (var kvInvoice in invoicesFilter)
            {
                await ProgressCreateUpdateInvoice(message, kvInvoice, coreContext,
                    deliveryInfo, invoicesFilter, kvOrder, customer, settings);
            }

            await HandleVoidInvoice(invoicesFilter, message, coreContext, existOrder);

            log.LogInfo();
            return true;
        }


        /// <summary>
        /// Progress create or update invoice
        /// </summary>
        /// <param name="message"></param>
        /// <param name="invoiceModel"></param>
        /// <param name="coreContext"></param>
        /// <param name="deliveryInfo"></param>
        /// <param name="kvInvoices"></param>
        /// <param name="existOrder"></param>
        /// <param name="customer"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        public async Task ProgressCreateUpdateInvoice(
           CreateInvoiceMessage message, KvInvoice invoiceModel,
           OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext,
           DeliveryInfoForOrder deliveryInfo,
           List<KvInvoice> kvInvoices,
           KvOrder existOrder,
           Customer customer,
           OmniChannelSettingObject settings)
        {
            try
            {
                await CreateUpdateInvoice(message, invoiceModel, coreContext,
                   deliveryInfo, kvInvoices, existOrder, customer, settings);
            }
            catch (Exception e)
            {
                await HandleException(e, message);

                await SaveSyncInvoiceError(coreContext, invoiceModel, existOrder, message.OrderId, customer,
                                    message.ChannelId, message.ChannelType, e.Message, e.Message);
            }
        }

        /// <summary>
        /// Handle create update invoice
        /// </summary>
        /// <param name="message"></param>
        /// <param name="invoiceModel"></param>
        /// <param name="coreContext"></param>
        /// <param name="deliveryInfo"></param>
        /// <param name="kvInvoices"></param>
        /// <param name="existOrder"></param>
        /// <param name="customer"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        public async Task CreateUpdateInvoice(
           CreateInvoiceMessage message, KvInvoice invoiceModel,
           OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext,
           DeliveryInfoForOrder deliveryInfo,
           List<KvInvoice> kvInvoices,
           KvOrder existOrder,
           Customer customer,
           OmniChannelSettingObject settings)
        {
            await UpdateFeeDeliveryInOrder(coreContext, deliveryInfo?.DeliveryInfoId,
                invoiceModel.DeliveryDetail.Price, invoiceModel.DeliveryDetail.FeeJson);

            var mappings = await GetMappings(invoiceModel, message.RetailerId, message.ChannelId,
                message.ChannelType);

            UpdateOrderDetail(invoiceModel, mappings);

            var invoiceKvProductIds = invoiceModel.InvoiceDetails.Where(p => p.ProductId > 0)
                .Select(p => p.ProductId).ToList();

            var kvProducts = await _productInternalClient.GetProductByIds(coreContext,
                message.BranchId, invoiceKvProductIds);

            var invoiceKvProducts = kvProducts.Where(x => invoiceKvProductIds.Contains(x.Id)).ToList();

            // Tạo HD trong lúc hàng hóa đang bị lock gây ra trường hợp không đồng bộ đúng stock
            await DelayByProductLock(message.ChannelId, invoiceKvProductIds);

            await RemoveMongoRetryInvoice(message.OrderId, message.BranchId, message.ChannelId, message.RetailerId);

            var existInvoice = await GetExistInvoice(invoiceModel, kvInvoices, coreContext);

            var lstInvoiceSurcharge = await _createOrderDomainService.GetListInvoiceSurchargeAsync(
                invoiceModel.SurCharges, coreContext, message);

            //Nếu hóa đơn đã tồn tại
            if (existInvoice != null)
            {
                await UpdateInvoice(invoiceModel, existInvoice, message,
                    lstInvoiceSurcharge, coreContext);
                PushMessageToInvoiceTrackingEvent(message, "Update invoice!");
                return;
            }

            if (invoiceModel.NewStatus == (byte)InvoiceState.Void)
            {
                await RemoveErrorInvoiceInMongo(message.OrderId, message.BranchId, message.ChannelId, invoiceModel.Code);
                await VoidKvOrder(message, coreContext, message.ChannelType, existOrder);
                PushMessageToInvoiceTrackingEvent(message, "Invoice cannel!");
                return;
            }

            var isValidateProduct = await ValidateProduct(invoiceKvProducts, invoiceModel, message.ChannelId,
                message.OrderId, message.ChannelType, existOrder, customer, coreContext, mappings, settings);

            if (!isValidateProduct)
            {
                PushLishMQToCreateLog((int)LogType.Warning, message.LogId, "CreateUpdateInvoice", null,
                null, 0, 0, "Invoice mapping invalid", null);
                PushMessageToInvoiceTrackingEvent(message, "Invoice mapping invalid!");
                return;
            }

            await CreateInvoice(invoiceModel, message, existOrder, settings, kvProducts, customer,
                coreContext, lstInvoiceSurcharge);
        }

        /// <summary>
        /// Update invoice
        /// </summary>
        /// <param name="invoiceRequest"></param>
        /// <param name="existInvoice"></param>
        /// <param name="message"></param>
        /// <param name="invoiceSurCharges"></param>
        /// <param name="coreContext"></param>
        /// <returns></returns>
        public async Task UpdateInvoice(KvInvoice invoiceRequest,
            Invoice existInvoice,
            CreateInvoiceMessage message,
            List<InvoiceSurCharges> invoiceSurCharges,
            OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext)
        {
            await RemoveErrorInvoiceInMongo(message.OrderId, message.BranchId, message.ChannelId, existInvoice.Code);

            //Nếu hóa đơn kv hoàn thành, hủy không thực hiện update
            if (existInvoice.Status == (byte)InvoiceState.Issued
                || existInvoice.Status == (byte)InvoiceState.Void) return;

            await _surChargeInternalClient.UpdateInvoiceSurcharge(coreContext, invoiceSurCharges, existInvoice.Id);

            var existDelivery = await _deliveryInfoInternalClient.GetLastByInvoiceId(coreContext, existInvoice.Id);

            if (existDelivery == null) return;

            var isChangeDeliveryPriceOrFee = existDelivery.FeeJson != invoiceRequest.DeliveryDetail.FeeJson
                && invoiceRequest.DeliveryDetail.Price.HasValue;
            if (existDelivery.Status != (byte)DeliveryStatus.Returned && isChangeDeliveryPriceOrFee)
            {
                existDelivery.UpdateFeeJson(invoiceRequest.DeliveryDetail.Price, invoiceRequest.DeliveryDetail.FeeJson);
                await _deliveryInfoInternalClient.UpdateDeliveryPriceAndFeeJson(coreContext, existDelivery);
            }

            if (invoiceRequest.DeliveryStatus == null) return;

            if (existDelivery.Status == invoiceRequest.DeliveryStatus.Value) return;

            if (invoiceRequest.DeliveryDetail.Price.HasValue)
            {
                existDelivery.UpdateFeeJson(invoiceRequest.DeliveryDetail.Price, invoiceRequest.DeliveryDetail.FeeJson);
            }
            await _invoiceInternalClient.UpdateStatus(coreContext,
                existDelivery, invoiceRequest.DeliveryStatus.Value);

            PushLishMQToCreateLog((int)LogType.Info, message.LogId, "UpdateInvoice", null,
                null, 0, 0, $"Update status success old delivery status {existDelivery.Status}; " +
                $"new status {invoiceRequest.DeliveryStatus.Value}", null);

            if (message.ChannelType == (byte)ChannelType.Tiktok && invoiceRequest.DeliveryStatus.Value == (byte)DeliveryStatus.Void)
            {
                await _invoiceInternalClient.VoidInvoice(coreContext, existInvoice.Id, message.RetailerId);
                await WriteLogForUpdateInvoice(message, invoiceRequest, existDelivery.Status, coreContext, true);
            }

            #region Logs
            await WriteLogForUpdateInvoice(message, invoiceRequest, existDelivery.Status, coreContext);
            #endregion
        }

        /// <summary>
        /// Create invoice
        /// </summary>
        /// <param name="invoiceRequest"></param>
        /// <param name="message"></param>
        /// <param name="existOrder"></param>
        /// <param name="settings"></param>
        /// <param name="kvProducts"></param>
        /// <param name="customer"></param>
        /// <param name="coreContext"></param>
        /// <param name="invoiceSurCharges"></param>
        /// <returns></returns>
        public async Task CreateInvoice(KvInvoice invoiceRequest,
            CreateInvoiceMessage message,
            KvOrder existOrder,
            OmniChannelSettingObject settings,
            List<ProductBrachDto> kvProducts,
            Customer customer,
            OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext,
            List<InvoiceSurCharges> invoiceSurCharges)
        {
            List<ProductBatchExpireDto> productBatchExPires = null;
            if (settings.IsAutoSyncBatchExpire)
            {
                var invoiceKvProductIds = invoiceRequest.InvoiceDetails.Where(p => p.ProductId > 0)
                    .Select(p => p.ProductId).ToList();

                var invoiceDetailsCopy = ConvertHelper.DeepCopy(invoiceRequest.InvoiceDetails);
                productBatchExPires =
                    await _productInternalClient.GetProductBatchExpireByBranch(
                        coreContext,
                        invoiceRequest.BranchId,
                        invoiceKvProductIds);

                invoiceRequest.InvoiceDetails = DetechHasExpireHelper.DetectInvoiceDetailHasBatchExpire(
                    kvProducts,
                    invoiceRequest.InvoiceDetails,
                    productBatchExPires);

                var (isValidAutoSyncBatchExpire, errorMessage, productLog) = _createInvoiceDomainService.ValidateAutoSyncBatchExpire(invoiceDetailsCopy, invoiceRequest.InvoiceDetails);
                if (!isValidAutoSyncBatchExpire)
                {
                    invoiceRequest.InvoiceDetails = invoiceDetailsCopy;
                    await SaveSyncInvoiceError(coreContext, invoiceRequest, existOrder,
                        message.OrderId, customer, message.ChannelId, message.ChannelType,
                        errorMessage, productLog);
                    PushMessageToInvoiceTrackingEvent(message, "Invalid batchExpire!");
                    return;
                }
            }

            var invoiceInsert = invoiceRequest.ConvertTo<Invoice>();

            invoiceInsert.UpdatePuseChaseDate(invoiceInsert.PurchaseDate, existOrder.PurchaseDate, false);
            invoiceInsert.UpdateSurCharges(invoiceSurCharges);

            var invoiceWarranties = await _createInvoiceDomainService.GetInvoiceWarranties(coreContext, kvProducts,
                invoiceRequest.InvoiceDetails.Select(x => new MongoDb.InvoiceDetail { ProductId = x.ProductId, Uuid = x.Uuid }).ToList(),
                invoiceInsert.PurchaseDate);
            invoiceInsert.UpdateInvoiceWarranties(invoiceWarranties);

            var invoiceDetailsInsert = invoiceRequest.InvoiceDetails.ConvertAll(x => x.ConvertTo<InvoiceDetail>());
            invoiceDetailsInsert.ForEach(x => x.UpdatePropetiesUseWarranty(invoiceInsert.InvoiceWarranties));

            var invoiceId = await _invoiceInternalClient.CreateInvoice(coreContext, invoiceInsert, invoiceDetailsInsert,
                invoiceRequest.DeliveryDetail.ConvertTo<InvoiceDelivery>());

            if (invoiceId == 0)
            {
                var ex = new OmniException("Tạo hóa đơn không thành công");
                throw ex;
            }

            PushLishMQToCreateLog((int)LogType.Info, message.LogId, "CreateInvoiceSuccess", invoiceInsert.ToJson(),
                $"InvoiceId: {invoiceId}", 0, 0, null, null);

            PushMessageToInvoiceTrackingEvent(message, "CreateInvoiceSuccess!");

            await RemoveMongoRetryInvoice(message.OrderId, message.BranchId, message.ChannelId, message.RetailerId);

            await RemoveErrorInvoiceInMongo(message.OrderId, message.BranchId, message.ChannelId,
                invoiceRequest.Code);

            #region Logs
            await WriteLogForCreateInvoice(message, invoiceRequest, existOrder, coreContext,
                productBatchExPires, invoiceInsert.InvoiceWarranties);
            #endregion

            if (invoiceRequest.DeliveryStatus != null && invoiceRequest.DeliveryStatus != (byte)DeliveryStatus.Pending)
            {
                var existDelivery = await _deliveryInfoInternalClient.GetLastByInvoiceId(coreContext, invoiceId);
                await _invoiceInternalClient.UpdateStatus(coreContext,
                    existDelivery, invoiceRequest.DeliveryStatus.Value);

                await WriteLogForUpdateInvoice(message, invoiceRequest, existDelivery.Status, coreContext);
            }
        }
        #endregion

        #region Private method
        private async Task<int> DelayByProductLock(long channelId, List<long> productIds)
        {
            var countDelay = 0;
            var millisecondsDelay = 1000;
            while (true)
            {
                if (countDelay >= 10 || !HasProductLock()) break;

                countDelay++;
                await Task.Delay(millisecondsDelay);

                bool HasProductLock()
                {
                    foreach (var productId in productIds)
                    {
                        var productLock = _cacheClient.Get<bool>(
                            string.Format(KvConstant.LockProductStockCache, channelId, productId));

                        if (productLock) return true;
                    }

                    return false;
                }
            }

            return countDelay * millisecondsDelay;
        }

        private async Task<Invoice> GetExistInvoice(
            KvInvoice invoice,
            List<KvInvoice> kvInvoices,
            OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext)
        {
            var originalCode = invoice.Code.Split('.')[0];
            var existedInvoices = await _invoiceInternalClient.GetByOriginalCode(coreContext, originalCode);
            Invoice existInvoice;
            if (kvInvoices.Count > 1)
            {
                var otherInvoiceCodes = kvInvoices.Where(x => x.Code != invoice.Code).Select(x => x.Code);
                var existDelivery = await _deliveryInfoInternalClient.GetLastByDeliveryCode(coreContext, invoice.DeliveryCode);
                existInvoice = existedInvoices?.FirstOrDefault(x => x.Id == (existDelivery?.InvoiceId ?? 0) && !otherInvoiceCodes.Contains(x.Code));
            }
            else
            {
                existInvoice = existedInvoices?.FirstOrDefault(x => !x.Code.ToUpper().Contains("DELETE")
                && !x.Code.ToUpper().Contains("OLD"));
            }

            return existInvoice;
        }

        private async Task RemoveErrorInvoiceInMongo(string orderId, int branchId,
           long channelId, string invoiceCode = null)
        {
            var invoiceInMongo = await _invoiceMongoService.GetByOrderId(orderId, branchId, channelId, invoiceCode);
            if (invoiceInMongo != null)
            {
                foreach (var invoice in invoiceInMongo)
                {
                    await _invoiceMongoService.RemoveAsync(invoice.Id);
                }
            }
        }

        private async Task RemoveMongoRetryInvoice(string orderId, int branchId,
            long channelId, int retailerId)
        {
            var existRetryInvoice = await _retryInvoiceMongoService.GetByOrderId(orderId, channelId,
                branchId, retailerId);
            if (existRetryInvoice != null && !existRetryInvoice.IsDbException)
            {
                await _retryInvoiceMongoService.RemoveAsync(existRetryInvoice.Id);
            }
        }

        private async Task VoidKvOrder(CreateInvoiceMessage data,
            OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext,
            byte channelType, KvOrder order)
        {
            if (order == null || (order.Status == (byte)OrderState.Void))
            {
                return;
            }

            var isVoidInvoiceSuccess = false;
            var existInvoices = await _invoiceInternalClient.GetByOrderId(coreContext, order.Id);
            if (existInvoices != null && existInvoices.Any())
            {
                existInvoices = existInvoices
                    .GroupBy(x => x.Code)
                    .Select(x => x.First())
                    .ToList();
                foreach (var invoice in existInvoices)
                {
                    try
                    {
                        if (invoice.Status == (byte)InvoiceState.Void)
                        {
                            isVoidInvoiceSuccess = true;
                            continue;
                        }
                        await _invoiceInternalClient.VoidInvoice(coreContext, invoice.Id);

                        var invoiceLog = new AuditTrailLog
                        {
                            FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channelType),
                            Action = (int)AuditTrailAction.OrderIntergate,
                            CreatedDate = DateTime.Now,
                            BranchId = data.BranchId
                        };
                        //TMDT-22
                        if (channelType == (byte)ChannelType.Shopee || channelType == (byte)ChannelType.Tiktok)
                        {
                            var priceBookName = "Bảng giá chung";

                            if (invoice.PriceBookId > 0)
                            {
                                var priceBook = await _priceBookInternalClient.GetById(coreContext,
                                    new OmniChannelCore.Api.Sdk.RequestDtos.PriceBookGetByIdRequest { PriceBookId = invoice.PriceBookId });
                                priceBookName = priceBook?.Name ?? priceBookName;
                            }

                            var deliveryInfo = await _deliveryInfoInternalClient.GetLastByInvoiceId(coreContext, invoice.Id);
                            var deliveryCode = deliveryInfo?.DeliveryCode;

                            invoiceLog.Content = $"Hủy hóa đơn: [InvoiceCode]{invoice.Code}[/InvoiceCode] " +
                                $"(cho đơn đặt hàng: [OrderCode]{order.Code}[/OrderCode]), mã vận đơn {deliveryCode},  " +
                                $"Bảng giá: {priceBookName}, giá trị: {StringHelper.NormallizeWfp((double)invoice.Total)}, thời gian: {invoice.PurchaseDate:dd/MM/yyyy HH:mm:ss} bao gồm: <br/>";

                            var kvProducts = await _productInternalClient.GetProductByIds(coreContext, data.BranchId, invoice.InvoiceDetails.Select(y => y.ProductId).ToList());
                            foreach (var item in invoice.InvoiceDetails)
                            {
                                var p = kvProducts.FirstOrDefault(x => x.Id == item.ProductId);
                                invoiceLog.Content += $"- [ProductCode]{p?.Code}[/ProductCode]: {StringHelper.Normallize(item.Quantity)} * {StringHelper.NormallizeWfp((double)(item.Price))} <br/>";
                            }
                        }
                        else
                        {
                            invoiceLog.Content = $"Hủy hóa đơn: [InvoiceCode]{invoice.Code}[/InvoiceCode]";
                        }

                        await _auditTrailInternalClient.AddLogAsync(coreContext, invoiceLog);
                        isVoidInvoiceSuccess = true;
                    }
                    catch (Exception ex)
                    {
                        var invoiceLog = new AuditTrailLog
                        {
                            FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channelType),
                            Action = (int)AuditTrailAction.OrderIntergate,
                            CreatedDate = DateTime.Now,
                            BranchId = data.BranchId,
                            Content = $"Cập nhật thông tin đơn đặt hàng KHÔNG thành công: [OrderCode]{order.Code}[/OrderCode], lý do: <br/> Không hủy được hóa đơn liên quan"
                        };
                        await _auditTrailInternalClient.AddLogAsync(coreContext, invoiceLog);
                        return;
                    }
                }
            }

            //TMDT-22 không hủy đơn hàng shopee sau khi hủy hóa đơn
            if (channelType != (byte)Sdk.Common.ChannelType.Shopee ||
                !isVoidInvoiceSuccess ||
                existInvoices == null ||
                (channelType == (byte)Sdk.Common.ChannelType.Shopee && order.Status != (byte)OrderState.Finalized)
            )
            {
                var logOrder = new AuditTrailLog
                {
                    FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channelType),
                    Action = (int)AuditTrailAction.OrderIntergate,
                    CreatedDate = DateTime.Now,
                    BranchId = data.BranchId,
                    Content = $"Hủy đơn đặt hàng: [OrderCode]{order.Code}[/OrderCode]"
                };
                await _orderInternalClient.VoidOrder(coreContext, order.Id);
                await _auditTrailInternalClient.AddLogAsync(coreContext, logOrder);
            }
        }

        private async Task<List<KvInvoice>> GetKvInvoice(
            CreateInvoiceMessage data,
            KvOrder kvOrder,
            DeliveryInfoForOrder deliveryInfo,
            bool isReturningInvoice,
            OmniChannelCore.Api.Sdk.Common.KvInternalContext kvInternalContext)
        {
            //Nếu đơn hàng trên sàn hủy thực hiện hủy đơn KV
            var channelOrder = JsonConvert.DeserializeObject<KvOrder>(data.Order);
            if (channelOrder.ChannelIsCancelOrder)
            {
                await RemoveErrorInvoiceInMongo(data.OrderId, data.BranchId, data.ChannelId);
                await VoidKvOrder(data, kvInternalContext, data.ChannelType, kvOrder);
                await RemoveMongoRetryInvoice(data.OrderId, data.BranchId, data.ChannelId, data.RetailerId);
                return null;
            }

            var client = _channelClient.GetClient(data.ChannelType, Settings);

            return client.GenKvInvoices(
                new CreateInvoiceRequest
                {
                    BranchId = data.BranchId,
                    OrderId = data.OrderId,
                    Order = data.Order,
                    PriceBookId = 0,
                    KvOrder = kvOrder,
                    KvDelivery = deliveryInfo.ConvertTo<DeliveryInfoDTO>(),
                    RetryCount = data.RetryCount,
                    IsConfirmReturning = isReturningInvoice
                });
        }

        private async Task<Extra> GetExtraAsync(
            OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext,
            long priceBookId)
        {
            var extra = new Extra
            {
                PriceBookId = new ChannelClient.Models.PriceBook
                {
                    Id = 0,
                    Name = "Bảng giá chung"
                }
            };
            if (priceBookId > 0)
            {
                var priceBook = await _priceBookInternalClient.GetById(coreContext,
                    new OmniChannelCore.Api.Sdk.RequestDtos.PriceBookGetByIdRequest { PriceBookId = priceBookId });

                if (priceBook != null && priceBook.IsActive && priceBook.isDeleted != true &&
                    priceBook.StartDate < DateTime.Now && priceBook.EndDate > DateTime.Now)
                {
                    extra = new ChannelClient.Models.Extra
                    {
                        PriceBookId = new ChannelClient.Models.PriceBook
                        {
                            Id = priceBook.Id,
                            Name = priceBook.Name
                        }
                    };
                }
            }
            return extra;
        }


        private async Task WriteLogForCreateInvoice(CreateInvoiceMessage message,
           KvInvoice invoice,
           KvOrder kvOrder,
           OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext,
           List<ProductBatchExpireDto> productBatchExPires,
           List<OmniChannelCore.Api.Sdk.Models.InvoiceWarranties> invoiceWarranties)
        {
            try
            {
                var productDetail = new StringBuilder();
                string batchExPire = string.Empty;
                double total = 0;
                if (invoice.InvoiceDetails != null && invoice.InvoiceDetails.Any())
                {
                    productDetail.Append(", bao gồm:<div>");
                    foreach (var inv in invoice.InvoiceDetails)
                    {
                        if (productBatchExPires != null || productBatchExPires.Any())
                        {
                            var productBatchExPire = productBatchExPires.FirstOrDefault(x => x.ProductId == inv.ProductId);
                            if (productBatchExPire != null) batchExPire = $" , {productBatchExPire.BatchName} - Hạn sử dụng: {productBatchExPire.ExpireDate.ToString("dd/MM/yyyy")}";

                        }
                        total += ((double)inv.Price - (double)(inv.Discount ?? 0)) * inv.Quantity;
                        productDetail.Append(
                            $"- [ProductCode]{inv.ProductCode}[/ProductCode] : {StringHelper.Normallize(inv.Quantity)}*{StringHelper.NormallizeWfp((double)inv.Price - (double)(inv.Discount ?? 0))}{batchExPire}<br>");
                        var productWarrantys = invoiceWarranties?.Where(x => x.InvoiceDetailUuid == inv.Uuid).ToList();
                        var getWarrantys = GetWarrantysLog(inv, productWarrantys);
                        productDetail.Append(string.IsNullOrEmpty(getWarrantys.ToString()) ? string.Empty : $"{getWarrantys}<br>");
                    }

                    productDetail.Append("</div>");
                }

                var log = new AuditTrailLog
                {
                    FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(message.ChannelType),
                    Action = (int)AuditTrailAction.InvoiceIntergate,
                    CreatedDate = DateTime.Now,
                    BranchId = message.BranchId,
                    Content =
                        $"Tạo hóa đơn [InvoiceCode]{invoice.Code}[/InvoiceCode] (cho đơn đặt hàng [OrderCode]{kvOrder.Code}[/OrderCode])," +
                        $"Bảng giá: {invoice.PriceBookName}, giá trị: {StringHelper.NormallizeWfp(total)}, thời gian: {invoice.PurchaseDate:dd/MM/yyyy HH:mm:ss}, " +
                        $"trạng thái: {EnumHelper.ToDescription((InvoiceState)invoice.Status)}, trạng thái giao: {EnumHelper.ToDescription((DeliveryStatus)invoice.DeliveryDetail.Status)}{productDetail}"
                };

                await _auditTrailInternalClient.AddLogAsync(coreContext, log);
            }
            catch
            {
                // ignored
            }
        }

        private async Task WriteLogForCreateInvoiceFail(OmniChannelCore.Api.Sdk.Common.KvInternalContext context,
            KvInvoice invoice, KvOrder order, byte channelType, string auditTrailMessage)
        {
            var log = new AuditTrailLog
            {
                FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channelType),
                Action = (int)AuditTrailAction.InvoiceIntergate,
                CreatedDate = DateTime.Now,
                BranchId = context.BranchId,
                Content =
                    $"Tạo hóa đơn KHÔNG thành công: [InvoiceCode]{invoice.Code}[/InvoiceCode] (cho đơn đặt hàng [OrderCode]{order.Code}[/OrderCode]) lý do: <br/>{auditTrailMessage}"
            };
            await _auditTrailInternalClient.AddLogAsync(context, log);
        }

        private async Task<bool> ValidateProduct(
            List<ProductBrachDto> kvProducts,
            KvInvoice invoice, long channelId, string orderId, byte channelType,
            KvOrder order, Customer customer,
            OmniChannelCore.Api.Sdk.Common.KvInternalContext context,
            List<MongoDb.ProductMapping> mappings,
            OmniChannelSettingObject settings)
        {
            var result = true;
            var productLog = new StringBuilder();
            var errorMessage = new StringBuilder();

            foreach (var detail in invoice.InvoiceDetails)
            {
                var product = kvProducts.FirstOrDefault(x => x.Id == detail.ProductId);
                detail.ProductName = product?.FullName ?? detail.ProductChannelName;
                var channelTrackKey = !string.IsNullOrEmpty(detail.ProductChannelId) ? detail.ProductChannelId : detail.ProductChannelSku;
                if (product == null)
                {
                    detail.UseProductBatchExpire = false;
                    detail.UseProductSerial = false;
                    errorMessage.Append($"Hàng hóa {detail.ProductChannelName} ({channelTrackKey}) chưa liên kết với hàng hóa nào trên Demo; ");
                    productLog.AppendFormat($"- {detail.ProductChannelSku ?? detail.ProductChannelId} ({channelTrackKey}): chưa xác định trên Demo <br/>");
                    result = false;
                }
                else
                {
                    detail.UseProductBatchExpire = product.IsBatchExpireControl.HasValue ? product.IsBatchExpireControl.Value : false;
                    detail.UseProductSerial = product.IsLotSerialControl.HasValue ? product.IsLotSerialControl.Value : false;
                    var mapping = mappings.FirstOrDefault(x => x.ChannelId == channelId && ((!string.IsNullOrEmpty(detail.ProductChannelId) && x.CommonProductChannelId == detail.ProductChannelId) || x.ProductChannelSku == detail.ProductChannelSku));
                    if (mapping == null)
                    {
                        errorMessage.Append($"Hàng hóa {detail.ProductChannelName} ({channelTrackKey}) chưa liên kết với hàng hóa nào trên Demo; ");
                        productLog.AppendFormat($"- [ProductCode]{detail.ProductCode}[/ProductCode]: liên kết hàng hóa không tồn tại <br/>");
                        result = false;
                    }
                    else if (product.isDeleted == true)
                    {
                        errorMessage.Append($"Hàng hóa {detail.ProductChannelName} ({channelTrackKey}) chưa liên kết với hàng hóa nào trên Demo; ");
                        productLog.AppendFormat($"- [ProductCode]{detail.ProductCode}[/ProductCode]: đã bị xóa <br/>");
                        result = false;
                    }
                    else if (product.IsLotSerialControl == true)
                    {
                        productLog.AppendFormat($"- [ProductCode]{detail.ProductCode}[/ProductCode]: là sản phẩm serial/imei <br/>");
                        errorMessage.Append($"{detail.ProductCode}: chưa được chọn Serial/IMEI để tạo hóa đơn; ");
                        result = false;
                    }
                    else if (!settings.IsAutoSyncBatchExpire && product.IsBatchExpireControl == true)
                    {
                        productLog.AppendFormat($"- [ProductCode]{detail.ProductCode}[/ProductCode]: là sản phẩm lô/hạn <br/>");
                        errorMessage.Append($"{detail.ProductCode}: chưa xác định Lô/date để tạo hóa đơn; ");
                        result = false;
                    }
                }
            }

            if (!result)
            {
                if ((byte)InvoiceState.Void == invoice.Status || (byte)InvoiceState.Void == invoice.NewStatus)
                {
                    await RemoveSyncInvoiceError(context, invoice, orderId, channelId);
                }
                else
                {
                    await SaveSyncInvoiceError(context, invoice, order, orderId, customer, channelId, channelType,
                        errorMessage.ToString(), productLog.ToString());
                }

            }
            return result;
        }

        private async Task RemoveSyncInvoiceError(
            OmniChannelCore.Api.Sdk.Common.KvInternalContext context,
            KvInvoice invoice,
            string channelOrderId,
            long channelId)
        {
            try
            {
                var existInvoice = await _invoiceMongoService.GetByInvoiceId(channelOrderId,
                    context.BranchId, channelId, invoice.Code);

                if (existInvoice != null)
                {
                    await _invoiceMongoService.RemoveAsync(existInvoice.Id);
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        private async Task SaveSyncInvoiceError(
            OmniChannelCore.Api.Sdk.Common.KvInternalContext context,
            KvInvoice invoice, KvOrder order,
            string channelOrderId, Customer customer,
            long channelId, byte channelType, string errorMessage,
            string auditTrailMessage)
        {
            try
            {
                var existInvoice = await _invoiceMongoService.GetByInvoiceId(channelOrderId, context.BranchId, channelId, invoice.Code);
                var retryInvoice = new MongoDb.Invoice
                {
                    Code = invoice.Code,
                    BranchId = invoice.BranchId,
                    SoldById = invoice.SoldById,
                    SaleChannelId = invoice.SaleChannelId ?? 0,
                    PurchaseDate = invoice.PurchaseDate,
                    Description = invoice.Description,
                    IsSyncSuccess = false,
                    UsingCod = true,
                    OrderId = invoice.OrderId,
                    DeliveryDetail = invoice.DeliveryDetail?.ConvertTo<MongoDb.InvoiceDelivery>(),
                    CustomerId = invoice.CustomerId,
                    CustomerName = customer?.Name,
                    CustomerCode = customer?.Code,
                    CustomerPhone = customer?.ContactNumber,
                    NewStatus = invoice.NewStatus,
                    Status = invoice.Status,
                    ChannelStatus = invoice.ChannelStatus,
                    ChannelId = channelId,
                    ErrorMessage = errorMessage,
                    ChannelOrderId = channelOrderId,
                    RetailerId = context.RetailerId,
                    CreatedDate = DateTime.Now,
                    Discount = invoice.Discount,
                    InvoiceDetails = new List<MongoDb.InvoiceDetail>(),
                    InvoiceSurCharges = invoice.SurCharges?.Select(p => new MongoDb.InvoiceSurCharges
                    {
                        Name = p.Name,
                        SurValue = p.Value,
                        Price = p.Value ?? 0
                    }).ToList() ?? new List<MongoDb.InvoiceSurCharges>()
                };
                foreach (var item in invoice.InvoiceDetails)
                {
                    var detail = new MongoDb.InvoiceDetail
                    {
                        CommonProductChannelId = item.ProductChannelId,
                        CommonParentChannelProductId = item.ParentChannelProductId,
                        Price = item.Price,
                        Discount = item.Discount,
                        DiscountPrice = item.DiscountPrice,
                        DiscountRatio = item.DiscountRatio,
                        ProductBatchExpireId = item.ProductBatchExpireId,
                        ProductChannelName = item.ProductChannelName,
                        ProductCode = item.ProductCode,
                        ProductId = item.ProductId,
                        ProductChannelSku = item.ProductChannelSku,
                        ProductName = item.ProductName,
                        Quantity = item.Quantity,
                        SerialNumbers = item.SerialNumbers,
                        SubTotal = item.SubTotal,
                        UsePoint = item.UsePoint,
                        UseProductBatchExpire = item.UseProductBatchExpire,
                        UseProductSerial = item.UseProductSerial,
                        Note = item.Note,
                        ReturnQuantity = item.ReturnQuantity,
                        Uuid = item.Uuid
                    };
                    retryInvoice.InvoiceDetails.Add(detail);
                }

                if (existInvoice == null)
                {
                    await _invoiceMongoService.AddSync(retryInvoice);
                }
                else
                {
                    retryInvoice.Id = existInvoice.Id;
                    await _invoiceMongoService.UpdateAsync(existInvoice.Id, retryInvoice);
                }
                PushLishMessageErrorNotification(order, channelOrderId, channelId, invoice.Code, errorMessage, channelType);
                #region AuditTrail
                await WriteLogForCreateInvoiceFail(context, invoice, order, channelType, auditTrailMessage);

                #endregion
            }
            catch (Exception e)
            {
                throw;
            }
        }

        private void PushLishMessageErrorNotification(KvOrder kvOrder,
            string orderChannelId,
            long channelId,
            string invoiceCode,
            string errorMessage,
            byte channelType)
        {
            var obj = new Infrastructure.EventBus.Event.ErrorOrderInvoiceNotificationMesage(Guid.NewGuid())
            {
                RetailerId = kvOrder.RetailerId,
                BranchId = kvOrder.BranchId,
                ChannelId = channelId,
                OrderKv = invoiceCode,
                OrderId = orderChannelId,
                PurchaseDate = kvOrder.PurchaseDate,
                ErrorType = (int)Demo.OmniChannel.Sdk.Common.SyncTabError.Invoice,
                ErrorMessage = errorMessage,
                Type = channelType
            };
            _integrationEventService.AddEventWithRoutingKeyAsync(obj, RoutingKey.MessageErrorNotification);
        }

        private StringBuilder GetWarrantysLog(ChannelClient.Models.InvoiceDetail invoiceDetail,
          List<OmniChannelCore.Api.Sdk.Models.InvoiceWarranties> invoiceWarrantys)
        {
            var warrantysLog = new StringBuilder();

            var checkProductIsCombo = !invoiceWarrantys.Where(x => x.ProductId == invoiceDetail.ProductId).Any();
            var productWarrantysGroup = invoiceWarrantys.GroupBy(x => new { x.ProductId, x.ProductCode }).ToList();
            foreach (var group in productWarrantysGroup)
            {
                var guarrantee = new StringBuilder();
                var mainGuarrantee = string.Empty;
                var productWarrantys = invoiceWarrantys.Where(x => x.ProductId == group.Key.ProductId).ToList();

                productWarrantys?.ForEach(w =>
                {
                    if (w.WarrantyType == (int)WarrantyType.Maintenance)
                    {
                        mainGuarrantee = string.Format("Định kỳ bảo trì: {0} {1} ({2}) đến {3}",
                            w.NumberTime, SystemHelper.GetWarrantyTimeTypeText(w.TimeType).ToLower(),
                            w.Description, ((DateTime)w.ExpireDate).ToString("dd/MM/yyyy"));
                    }
                    else
                    {
                        var guarranteelog = string.Format("{0} {1} ({2}) đến {3}",
                           w.NumberTime, SystemHelper.GetWarrantyTimeTypeText(w.TimeType).ToLower(),
                           w.Description, ((DateTime)w.ExpireDate).ToString("dd/MM/yyyy"));

                        guarrantee.Append(string.IsNullOrEmpty(guarrantee.ToString())
                            ? $"Bảo hành: {guarranteelog}"
                            : $", {guarranteelog}");
                    }
                });
                if (checkProductIsCombo)
                {
                    warrantysLog.Append($"Thành phần bảo hành: [ProductCode]{group.Key.ProductCode}[/ProductCode]<br />");
                }
                warrantysLog.Append(string.IsNullOrEmpty(guarrantee.ToString()) ? string.Empty : $"&nbsp;&nbsp;{guarrantee}<br>");
                warrantysLog.Append(string.IsNullOrEmpty(mainGuarrantee) ? string.Empty : $"&nbsp;&nbsp;{mainGuarrantee}<br>");
            }

            return warrantysLog;
        }

        private async Task AddOrUpdateRetryInvoice(CreateInvoiceMessage message, bool isDbException = false,
            string invoiceCode = null)
        {
            var existRetryInvoice =
                await _retryInvoiceMongoService.GetByOrderId(message.OrderId, message.ChannelId, message.BranchId,
                    message.RetailerId);
            if (existRetryInvoice != null)
            {
                existRetryInvoice.ModifiedDate = DateTime.Now;
                existRetryInvoice.RetryCount = message.RetryCount + 1;
                await _retryInvoiceMongoService.UpdateAsync(existRetryInvoice.Id, existRetryInvoice);
            }
            else
            {
                await _retryInvoiceMongoService.AddSync(new MongoDb.RetryInvoice
                {
                    ChannelId = message.ChannelId,
                    ChannelType = message.ChannelType,
                    BranchId = message.BranchId,
                    KvEntities = message.KvEntities,
                    RetailerId = message.RetailerId,
                    OrderId = message.OrderId,
                    Order = message.Order,
                    KvOrder = message.KvOrder,
                    KvOrderId = message.KvOrderId,
                    CreatedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    IsDbException = isDbException,
                    Code = invoiceCode,
                    RetryCount = 1
                });
            }
        }

        private async Task HandleException(Exception e, CreateInvoiceMessage message)
        {
            var connectStringName = message.KvEntities.Substring(message.KvEntities.IndexOf('=') + 1);
            var context = await ContextHelper.GetExecutionContext(_cacheClient, DbConnectionFactory, message.RetailerId,
                connectStringName, message.BranchId, Settings?.Get<int>("ExecutionContext"));

            if (e is OmniChannelCore.Api.Sdk.Common.KvRetailerExpireException)
            {
                await DeActiveChannel(message.ChannelId, message.ChannelType,
                    context.ConvertTo<Sdk.Common.KvInternalContext>());
                return;
            }

            if (e is OmniChannelCore.Api.Sdk.Common.KvDbException
                                || (e.Message != null && (e.Message.ToLower().Contains(DbExceptionMessages.ConnectionTimeout)
                                || e.Message.ToLower().Contains(DbExceptionMessages.TransactionDeadlocked)
                                || e.Message.ToLower().Contains(DbExceptionMessages.DatabaseException))))
            {
                await AddOrUpdateRetryInvoice(message, e is OmniChannelCore.Api.Sdk.Common.KvDbException,
                    null);
            }

            PushLishMQToCreateLog((int)LogType.Error, message.LogId, "ExceptionCreateInvoice",
                    message.ToJson(), e.Message + e.StackTrace, message.RetailerId, message.ChannelId, null, null);

            PushMessageToInvoiceTrackingEvent(message, "ExceptionCreateInvoice!");
        }

        private void PushMessageToInvoiceTrackingEvent(CreateInvoiceMessage message, string error = "")
        {
            if (message.Source != null && message.Source.ToLower().Equals(ProcessOrderSource.WebHook))
            {
                string exChangeTrackProcessExChange = Settings.Get<string>("OrderTrackingEventExChange");
                var eventModel = new TrackModelEvent
                {
                    Code = message.CodeSource,
                    OrderSn = message.OrderSn,
                    ErrorMessage = error,
                    Status = ProcessOrderStatus.Processed,
                    OrderTimestamp = message.Timestamps,
                    ShopId = message.ShopId,
                    Source = message.Source,
                    ProcessType = ProcessFlowType.Invoice
                };
                _integrationEventService.AddEventWithRoutingKeyAsync(new OrderTrackingEvent
                {
                    TrackModelEvent = eventModel,
                }, RoutingKey.OrderTrackingEvent, exChangeTrackProcessExChange);
            }
        }
        #endregion

        #region AbstractMethod
        protected abstract Task UpdateFeeDeliveryInOrder(
            OmniChannelCore.Api.Sdk.Common.KvInternalContext kvInternalContext,
            long? deliveryInfoId, decimal? price, string feeJson);

        protected abstract void UpdateOrderDetail(KvInvoice invoice,
            List<MongoDb.ProductMapping> mappings);

        protected abstract Task<List<MongoDb.ProductMapping>> GetMappings(KvInvoice invoice,
            int retailerId, long channelId, byte channelType);

        protected abstract Task HandleVoidInvoice(List<KvInvoice> invoices, CreateInvoiceMessage data,
            OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext,
            OmniChannelCore.Api.Sdk.Models.Order existOrder);
        #endregion

        #region Protected method
        protected async Task WriteLogForUpdateInvoice(CreateInvoiceMessage message,
           KvInvoice invoice, byte oldDeliveryStatus,
            OmniChannelCore.Api.Sdk.Common.KvInternalContext kvInternalContext,
            bool logVoidInvoice = false)
        {
            if (oldDeliveryStatus != invoice.DeliveryStatus)
            {
                var log = new AuditTrailLog
                {
                    FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(message.ChannelType),
                    Action = (int)AuditTrailAction.InvoiceIntergate,
                    CreatedDate = DateTime.Now,
                    BranchId = message.BranchId
                };

                if (invoice.NewStatus == (byte)InvoiceState.Void && logVoidInvoice)
                {
                    log.Content = $"Hủy hóa đơn: [InvoiceCode]{invoice.Code}[/InvoiceCode]";
                }
                else
                {
                    var contentLog =
                        new StringBuilder(
                            $"Đồng bộ trạng thái hóa đơn ([InvoiceCode]{invoice.Code}[/InvoiceCode]), vận đơn thành công: <br/>");
                    contentLog.AppendFormat(
                        $"- Vận đơn: Trạng thái: {EnumHelper.ToDescription((DeliveryStatus)oldDeliveryStatus)} -> {EnumHelper.ToDescription((DeliveryStatus)(invoice.DeliveryStatus ?? 0))} <br/>");

                    log.Content = contentLog.ToString();
                }

                await _auditTrailInternalClient.AddLogAsync(kvInternalContext, log);
            }
        }
        #endregion

        #region Virtual method
        protected virtual void PublishUpdatePaymentTransaction(CreateInvoiceMessage message) { }

        protected virtual Task UpdateDeliveryInfo(OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext,
            CreateInvoiceMessage data,
            KvOrder kvOrder,
            DeliveryInfoForOrder deliveryInfo)
        {

            return Task.CompletedTask;
        }

        #endregion
    }
}

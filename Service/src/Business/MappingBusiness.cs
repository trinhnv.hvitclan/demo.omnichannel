﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Newtonsoft.Json;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Business
{
    public class MappingBusiness : IMappingBusiness
    {
        private const int WaitingDeleteMappingExpireIn = 60000;    // ms
        private const int WaitingDeleteMappingDelay = 1000;        // ms
        private readonly IAppSettings _settings;

        private readonly IProductMongoService _producChannelService;
        private readonly IProductMappingService _productMappingService;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly ILog _logger = LogManager.GetLogger(typeof(OnHandBusiness));
        private IMessageFactory _messageFactory;
        private readonly ICacheClient _cacheClient;
        public MappingBusiness(IDbConnectionFactory dbConnectionFactory, IProductMappingService productMappingService, IProductMongoService producChannelService, IMessageFactory messageFactory, ICacheClient cacheClient, IAppSettings settings)
        {
            _dbConnectionFactory = dbConnectionFactory;
            _productMappingService = productMappingService;
            _producChannelService = producChannelService;
            _messageFactory = messageFactory;
            _cacheClient = cacheClient;
            _settings = settings;
            _ = new SelfAppConfig(settings);

        }
        public async Task CreateMappingAsync(string connectStringName, int retailerId, int branchId, List<Domain.Model.OmniChannel> channels, List<KvProductMapping> kvProductMappings, Guid logId, string serviceName, int totalPageSizeSync = 50)
        {
            var productIds = kvProductMappings.Where(x => x.Id > 0).Select(x => x.Id).Distinct().ToList();
            var mappingExisted = await _productMappingService.GetByKvProductIds(retailerId, productIds);
            var kvProductDic = kvProductMappings.Where(x => !string.IsNullOrEmpty(x.Code)).GroupBy(x => x.Id).Select(x => x.First()).ToDictionary(x => x.Id, y => y.Code);


            #region add new mappings

            var productNoConnected = new List<MongoDb.Product>();
            var productHasConnected = new List<MongoDb.Product>();
            foreach (var channel in channels)
            {
                // #TMDT-23 Đề xuất ngắt kết nối khi đổi mã SKU; tự động đẩy lại đồng bộ trùng mã SKU lại cho hàng hóa
                if (mappingExisted.Any() && channel.IsAutoDeleteMapping == true)
                {
                    foreach (var kvProductMapping in kvProductMappings)
                    {
                        if (string.IsNullOrEmpty(kvProductMapping.Code)) continue;

                        // Điều kiện delete: Mã hàng KV thay đổi, Mã hàng KV mới != SKU sàn
                        var mappingDelete = mappingExisted.FirstOrDefault(x => x.ChannelId == channel.Id && x.ProductKvId == kvProductMapping.Id && x.ProductKvSku != kvProductMapping.Code && x.ProductChannelSku != kvProductMapping.Code);
                        if (mappingDelete != null)
                        {
                            await RemoveMappings(connectStringName, retailerId, branchId, new List<long> { channel.Id }, new List<long> { kvProductMapping.Id }, null, logId);
                            mappingExisted = mappingExisted.Where(x => x.Id != mappingDelete.Id).ToList();
                        }
                    }
                }

                if (channel.IsAutoMappingProduct)
                {
                    var mappedKvProductIds = mappingExisted.Where(x => x.ChannelId == channel.Id).Select(x => x.ProductKvId).ToList();
                    var activeFeatureMultiSku = SelfAppConfig.CheckActiveFeatureByPlatform(SelfAppConfig.UseMultiMappingSku, retailerId, channel.Type);
                    if (!activeFeatureMultiSku || 
                        (channel.Type != (byte)ChannelType.Shopee &&
                        channel.Type != (byte)ChannelType.Tiktok &&
                        channel.Type != (byte)ChannelType.Lazada))
                    {
                        var codes = kvProductDic.Where(x => !mappedKvProductIds.Contains(x.Key)).Select(x => x.Value).ToList();
                        var channelProductNonMapping = await _producChannelService.GetProductSameKvSku(channel.Id, codes, false);
                        channelProductNonMapping = channelProductNonMapping.GroupBy(x => x.ItemSku).Select(x => x.First()).ToList();
                        productNoConnected.AddRange(channelProductNonMapping);
                    }
                    //Mapping 1-n
                    else
                    {
                        var codes = kvProductDic.Select(x => x.Value).ToList();
                        var channelProductNonMapping = await _producChannelService.GetProductSameKvSku(channel.Id, codes, false);
                        channelProductNonMapping = channelProductNonMapping.Where(x => !mappingExisted.Select(m => m.ProductChannelId).Contains(x.ItemId)).Distinct().ToList();
                        productNoConnected.AddRange(channelProductNonMapping);
                    }
                }

                if (mappingExisted.Any())
                {
                    var productConnected = await _producChannelService.GetByItemIds(channel.RetailerId, channel.Id, mappingExisted.Select(x => x.CommonProductChannelId).ToList(), ConvertHelper.CheckUseStringItemId(channel.Type));
                    productHasConnected.AddRange(productConnected);
                }
            }



            var newMappingProduct = productNoConnected
                       .Join(kvProductMappings,
                          channelProduct => channelProduct.ItemSku,
                          kvProduct => kvProduct.Code,
                          (channelProduct, kvProduct) => new ProductMappingPre
                          {
                              RetailerId = channelProduct.RetailerId,
                              ProductChannelSku = channelProduct.ItemSku,
                              ProductChannelId = channelProduct.CommonItemId,
                              ProductChannelName = channelProduct.ItemName,
                              ParentProductChannelId = channelProduct.CommonParentItemId,
                              ChannelProductKey = channelProduct.Id,
                              ProductKvId = kvProduct.Id,
                              ProductKvSku = kvProduct.Code,
                              ProductChannelType = channelProduct.Type,
                              ProductKvFullName = kvProduct.Fullname,
                              ChannelId = channelProduct.ChannelId,
                              BranchId = channelProduct.BranchId,
                              InActiveBranchIds = kvProduct.InActiveBranchIds
                          }).ToList();

            #endregion

            #region update existed mappings
            var updateMappingProduct = new List<ProductMappingPre>();
            foreach (var item in mappingExisted)
            {
                var kvProduct = kvProductMappings.FirstOrDefault(x => x.Id == item.ProductKvId);
                if (kvProduct == null) continue;
                var mapBranchId = channels.FirstOrDefault(x => x.Id == item.ChannelId)?.BranchId ?? 0;

                if (kvProduct.Fullname != item.ProductKvFullName || kvProduct.Code != item.ProductKvSku)
                {
                    var productConnected = productHasConnected.FirstOrDefault(x => x.CommonItemId == item.CommonProductChannelId);
                    var mappingUpdate = new ProductMappingPre
                    {
                        RetailerId = item.RetailerId,
                        ChannelId = item.ChannelId,
                        ProductChannelSku = item.ProductChannelSku,
                        ProductChannelId = item.CommonProductChannelId,
                        ProductChannelName = item.ProductChannelName,
                        ParentProductChannelId = item.CommonParentProductChannelId,
                        ProductChannelType = item.ProductChannelType,
                        ProductKvId = kvProduct.Id,
                        ProductKvSku = kvProduct.Code,
                        ProductKvFullName = kvProduct.Fullname,
                        BranchId = mapBranchId,
                        InActiveBranchIds = kvProduct.InActiveBranchIds,
                        ChannelProductKey = productConnected?.Id
                    };
                    updateMappingProduct.Add(mappingUpdate);
                }
            }
            #endregion

            newMappingProduct = newMappingProduct.Where(x => x.InActiveBranchIds == null || !x.InActiveBranchIds.Contains(x.BranchId))
                .GroupBy(x => new { x.ChannelId, x.ProductChannelId, x.ProductKvId }).Select(x => x.FirstOrDefault()).ToList();
            updateMappingProduct = updateMappingProduct.Where(x => x.InActiveBranchIds == null || !x.InActiveBranchIds.Contains(x.BranchId))
                .GroupBy(x => new { x.ChannelId, x.ProductChannelId, x.ProductKvId }).Select(x => x.FirstOrDefault()).ToList();

            using (var mq = _messageFactory.CreateMessageProducer())
            {
                var channelAddNewMappings = newMappingProduct.GroupBy(x => x.ChannelId).Select(x => x.Key);
                foreach (var channelId in channelAddNewMappings)
                {
                    var mappings = newMappingProduct.Where(x => x.ChannelId == channelId);
                    for (int i = 0; ; i++)
                    {
                        var addPageMappings = mappings.Skip(i * totalPageSizeSync).Take(totalPageSizeSync).ToList();
                        if (!addPageMappings.Any()) break;
                        var addMappingMessage = new CreateProductMappingMessage
                        {
                            ConnectStringName = connectStringName,
                            RetailerId = retailerId,
                            BranchId = branchId,
                            ChannelId = channelId,
                            ProductMappings = addPageMappings,
                            IsAddNew = true,
                            LogId = logId,
                            MessageFrom = $"MappingBusiness-{serviceName}"
                        };
                        mq.Publish(addMappingMessage);
                    }
                }

                var channelUpdateMappings = updateMappingProduct.GroupBy(x => x.ChannelId).Select(x => x.Key);
                foreach (var channelId in channelUpdateMappings)
                {
                    for (int i = 0; ; i++)
                    {
                        var updatePageMappings = updateMappingProduct.Skip(i * totalPageSizeSync).Take(totalPageSizeSync).ToList();
                        if (!updatePageMappings.Any()) break;
                        var updateMappingMessage = new CreateProductMappingMessage
                        {
                            ConnectStringName = connectStringName,
                            RetailerId = retailerId,
                            BranchId = branchId,
                            ChannelId = channelId,
                            ProductMappings = updatePageMappings,
                            IsAddNew = false,
                            LogId = logId,
                            MessageFrom = $"MappingBusiness-{serviceName}"
                        };
                        mq.Publish(updateMappingMessage);
                    }
                }
            }
        }

        public async Task CreateMappingAsyncV2(string connectStringName, int retailerId, int branchId, List<Domain.Model.OmniChannel> channels, List<KvProductMapping> kvProductMappings, Guid logId, string serviceName, int totalPageSizeSync = 50)
        {
            var mappingExisted = await _productMappingService.Get(retailerId, channels.Select(t => t.Id).ToList(), new List<long> { });
            var kvProductDic = kvProductMappings.Where(x => !string.IsNullOrEmpty(x.Code)).GroupBy(x => x.Id).Select(x => x.First()).ToDictionary(x => x.Id, y => y.Code);


            #region add new mappings

            _logger.Info($"MappingBusiness: Start mapping update");
            var productNoConnected = new List<MongoDb.Product>();
            var productHasConnected = new List<MongoDb.Product>();
            foreach (var channel in channels)
            {
                // #TMDT-23 Đề xuất ngắt kết nối khi đổi mã SKU; tự động đẩy lại đồng bộ trùng mã SKU lại cho hàng hóa
                if (mappingExisted.Any() && channel.IsAutoDeleteMapping == true)
                {
                    foreach (var kvProductMapping in kvProductMappings)
                    {
                        if (string.IsNullOrEmpty(kvProductMapping.Code)) continue;

                        // Điều kiện delete: Mã hàng KV thay đổi, Mã hàng KV mới != SKU sàn
                        var mappingDelete = mappingExisted.Where(x => x.ChannelId == channel.Id && x.ProductKvId == kvProductMapping.Id && x.ProductKvSku != kvProductMapping.Code && x.ProductChannelSku != kvProductMapping.Code).ToList();
                        if (mappingDelete.Count > 0)
                        {
                            await RemoveMappings(connectStringName, retailerId, branchId, new List<long> { channel.Id }, new List<long> { kvProductMapping.Id }, null, logId);
                            mappingExisted = mappingExisted.Where(x => !mappingDelete.Select(t => t.Id).ToList().Contains(x.Id)).ToList();
                        }
                    }
                }

                if (channel.IsAutoMappingProduct)
                {
                    var mappedKvProductIds = mappingExisted.Where(x => x.ChannelId == channel.Id).Select(x => x.ProductKvId).ToList();
                    var activeFeatureMultiSku = SelfAppConfig.CheckActiveFeatureByPlatform(SelfAppConfig.UseMultiMappingSku, retailerId, channel.Type);
                    if (!activeFeatureMultiSku || 
                        (channel.Type != (byte)ChannelType.Shopee &&
                        channel.Type != (byte)ChannelType.Tiktok &&
                        channel.Type != (byte)ChannelType.Lazada))
                    {
                        var codes = kvProductDic.Where(x => !mappedKvProductIds.Contains(x.Key)).Select(x => x.Value).ToList();
                        var channelProduct = await _producChannelService.GetProductSameKvSku(channel.Id, codes);
                        var channelProductNonMapping = channelProduct.Where(product => !mappingExisted.Any(productMapping => product.StrItemId == productMapping.StrProductChannelId));
                        channelProductNonMapping = channelProductNonMapping.GroupBy(x => x.ItemSku).Select(x => x.First()).ToList();
                        productNoConnected.AddRange(channelProductNonMapping);
                    }
                    //Mapping 1-n
                    else
                    {
                        var codes = kvProductDic.Select(x => x.Value).ToList();
                        var channelProduct = await _producChannelService.GetProductSameKvSku(channel.Id, codes);
                        var channelProductNonMapping = channelProduct.Where(x => !mappingExisted.Select(m => m.ProductChannelId).Contains(x.ItemId)).ToList();
                        productNoConnected.AddRange(channelProductNonMapping);
                    }
                }

                if (mappingExisted.Any())
                {
                    var productConnected = await _producChannelService.GetByItemIds(channel.RetailerId, channel.Id, mappingExisted.Select(x => x.CommonProductChannelId).ToList(), ConvertHelper.CheckUseStringItemId(channel.Type));
                    productHasConnected.AddRange(productConnected);
                }
            }



            var newMappingProduct = productNoConnected
                       .Join(kvProductMappings,
                          channelProduct => channelProduct.ItemSku,
                          kvProduct => kvProduct.Code,
                          (channelProduct, kvProduct) => new ProductMappingPre
                          {
                              ProductChannelObjectIdKey = channelProduct.Id,
                              RetailerId = channelProduct.RetailerId,
                              ProductChannelSku = channelProduct.ItemSku,
                              ProductChannelId = channelProduct.CommonItemId,
                              ProductChannelName = channelProduct.ItemName,
                              ParentProductChannelId = channelProduct.CommonParentItemId,
                              ChannelProductKey = channelProduct.Id,
                              ProductKvId = kvProduct.Id,
                              ProductKvSku = kvProduct.Code,
                              ProductChannelType = channelProduct.Type,
                              ProductKvFullName = kvProduct.Fullname,
                              ChannelId = channelProduct.ChannelId,
                              BranchId = channelProduct.BranchId,
                              InActiveBranchIds = kvProduct.InActiveBranchIds
                          }).ToList();

            #endregion

            #region update existed mappings
            var updateMappingProduct = new List<ProductMappingPre>();
            foreach (var item in mappingExisted)
            {
                var kvProduct = kvProductMappings.FirstOrDefault(x => x.Id == item.ProductKvId);
                if (kvProduct == null) continue;
                var mapBranchId = channels.FirstOrDefault(x => x.Id == item.ChannelId)?.BranchId ?? 0;

                if ((!string.IsNullOrEmpty(kvProduct.Fullname) && kvProduct.Fullname != item.ProductKvFullName) || 
                    (!string.IsNullOrEmpty(kvProduct.Code) && kvProduct.Code != item.ProductKvSku))
                {
                    var productConnected = productHasConnected.FirstOrDefault(x => x.CommonItemId == item.CommonProductChannelId);
                    var mappingUpdate = new ProductMappingPre
                    {
                        ProductChannelObjectIdKey = item.Id,
                        RetailerId = item.RetailerId,
                        ChannelId = item.ChannelId,
                        ProductChannelSku = item.ProductChannelSku,
                        ProductChannelId = item.CommonProductChannelId,
                        ProductChannelName = item.ProductChannelName,
                        ParentProductChannelId = item.CommonParentProductChannelId,
                        ProductChannelType = item.ProductChannelType,
                        ProductKvId = kvProduct.Id,
                        ProductKvSku = kvProduct.Code,
                        ProductKvFullName = kvProduct.Fullname,
                        BranchId = mapBranchId,
                        InActiveBranchIds = kvProduct.InActiveBranchIds,
                        ChannelProductKey = productConnected?.Id,
                    };
                    updateMappingProduct.Add(mappingUpdate);
                }
            }
            #endregion

            newMappingProduct = newMappingProduct.Where(x => x.InActiveBranchIds == null || !x.InActiveBranchIds.Contains(x.BranchId))
                .GroupBy(x => new { x.ChannelId, x.ProductChannelId, x.ProductKvId }).Select(x => x.FirstOrDefault()).ToList();
            updateMappingProduct = updateMappingProduct.Where(x => x.InActiveBranchIds == null || !x.InActiveBranchIds.Contains(x.BranchId))
                .GroupBy(x => new { x.ChannelId, x.ProductChannelId, x.ProductKvId }).Select(x => x.FirstOrDefault()).ToList();

            _logger.Info($"MappingBusiness: newMappingProduct: {newMappingProduct.ToSafeJson()} - updateMappingProduct: {updateMappingProduct.ToSafeJson()}");

            using (var mq = _messageFactory.CreateMessageProducer())
            {
                var channelAddNewMappings = newMappingProduct.GroupBy(x => x.ChannelId).Select(x => x.Key);
                foreach (var channelId in channelAddNewMappings)
                {
                    var mappings = newMappingProduct.Where(x => x.ChannelId == channelId).ToList();
                    for (var i = 0; ; i++)
                    {
                        var addPageMappings = mappings.Skip(i * totalPageSizeSync).Take(totalPageSizeSync).ToList();
                        if (!addPageMappings.Any()) break;
                        var configMappingV2Feature = _settings?.Get<MappingV2Feature>("MappingV2Feature");
                        var coreContext = await ContextHelper.GetExecutionContext(_cacheClient, _dbConnectionFactory, retailerId, connectStringName, branchId, _settings?.Get<int>("ExecutionContext"));
                        if (configMappingV2Feature != null && configMappingV2Feature.IsValid(coreContext.Group?.Id ?? 0, retailerId))
                        {
                            _logger.Info($"MappingBusiness: Will send message to Kafka V2.");
                            var mappingRequest = new MappingRequest
                            {
                                ConnectStringName = connectStringName,
                                RetailerId = retailerId,
                                BranchId = branchId,
                                ChannelId = channelId,
                                ProductMappings = addPageMappings,
                                IsAddNew = true,
                                LogId = logId,
                                MessageFrom = $"MappingBusiness-{serviceName}"
                            };
                            var kafkaMessage = new KafkaMessage(configMappingV2Feature.TopicMappingRequestName,
                                $"{mappingRequest.RetailerId}_{mappingRequest.BranchId}_{mappingRequest.ChannelId}", mappingRequest);
                            KafkaClient.KafkaClient.Instance.PublishMessage(kafkaMessage.TopicName, kafkaMessage.Key, JsonConvert.SerializeObject(kafkaMessage), isV2: true);
                        }
                        else
                        {
                            var addMappingMessage = new CreateProductMappingMessage
                            {
                                ConnectStringName = connectStringName,
                                RetailerId = retailerId,
                                BranchId = branchId,
                                ChannelId = channelId,
                                ProductMappings = addPageMappings,
                                IsAddNew = true,
                                LogId = logId,
                                MessageFrom = $"MappingBusiness-{serviceName}"
                            };
                            mq.Publish(addMappingMessage);
                        }

                    }
                }

                var channelUpdateMappings = updateMappingProduct.GroupBy(x => x.ChannelId).Select(x => x.Key);
                foreach (var channelId in channelUpdateMappings)
                {
                    for (int i = 0; ; i++)
                    {
                        var updatePageMappings = updateMappingProduct.Where(x => x.ChannelId == channelId).Skip(i * totalPageSizeSync).Take(totalPageSizeSync).ToList();
                        if (!updatePageMappings.Any()) break;
                        var configMappingV2Feature = _settings?.Get<MappingV2Feature>("MappingV2Feature");
                        var coreContext = await ContextHelper.GetExecutionContext(_cacheClient, _dbConnectionFactory, retailerId, connectStringName, branchId, _settings?.Get<int>("ExecutionContext"));
                        if (configMappingV2Feature != null && configMappingV2Feature.IsValid(coreContext.Group?.Id ?? 0, retailerId))
                        {
                            var mappingRequest = new MappingRequest
                            {
                                ConnectStringName = connectStringName,
                                RetailerId = retailerId,
                                BranchId = branchId,
                                ChannelId = channelId,
                                ProductMappings = updatePageMappings,
                                IsAddNew = false,
                                LogId = logId,
                                MessageFrom = $"MappingBusiness-{serviceName}"
                            };
                            var kafkaMessage = new KafkaMessage(configMappingV2Feature.TopicMappingRequestName,
                                $"{mappingRequest.RetailerId}_{mappingRequest.BranchId}_{mappingRequest.ChannelId}", mappingRequest);
                            KafkaClient.KafkaClient.Instance.PublishMessage(kafkaMessage.TopicName, kafkaMessage.Key, JsonConvert.SerializeObject(kafkaMessage), isV2: true);
                        }
                        else
                        {
                            var updateMappingMessage = new CreateProductMappingMessage
                            {
                                ConnectStringName = connectStringName,
                                RetailerId = retailerId,
                                BranchId = branchId,
                                ChannelId = channelId,
                                ProductMappings = updatePageMappings,
                                IsAddNew = false,
                                LogId = logId,
                                MessageFrom = $"MappingBusiness-{serviceName}"
                            };
                            mq.Publish(updateMappingMessage);
                        }
                    }
                }
            }
        }

        public async Task RemoveMappings(string connectStringName, int retailerId, int branchId, List<long> channelIds, List<long> kvProductIds, List<string> channelProductIds, Guid logId, bool isDeleteKvProduct = true)
        {
            var configMappingV2Feature = _settings?.Get<MappingV2Feature>("MappingV2Feature");
            var coreContext = await ContextHelper.GetExecutionContext(_cacheClient, _dbConnectionFactory, retailerId, connectStringName, branchId, _settings?.Get<int>("ExecutionContext"));
            if (configMappingV2Feature != null && configMappingV2Feature.IsValid(coreContext.Group?.Id ?? 0, retailerId))
            {
                var channelIdsStr = string.Empty;
                if (channelIds != null && channelIds.Count > 0)
                {
                    var channelIdsString = channelIds.ConvertAll(x => x.ToString());
                    channelIdsStr = channelIdsString.Join("-");
                }
                var removeMsg = new RemoveProductMappingMessage
                {
                    RetailerId = retailerId,
                    BranchId = branchId,
                    KvProductIds = kvProductIds,
                    ChannelIds = channelIds,
                    ChannelProductIds = channelProductIds,
                    ConnectStringName = connectStringName,
                    IsDeleteKvProduct = isDeleteKvProduct,
                    LogId = logId,
                    MessageFrom = "MappingBusiness"
                };
                var kafkaMessage = new KafkaMessage(configMappingV2Feature.TopicRemoveMappingRequestName,
                    $"{removeMsg.RetailerId}_{removeMsg.BranchId}_{channelIdsStr}", removeMsg);
                KafkaClient.KafkaClient.Instance.PublishMessage(kafkaMessage.TopicName, kafkaMessage.Key, kafkaMessage.ToSafeJson(), isV2: true);
            }
            else
            {
                using (var mq = _messageFactory.CreateMessageProducer())
                {
                    var removeMsg = new RemoveProductMappingMessage
                    {
                        RetailerId = retailerId,
                        BranchId = branchId,
                        KvProductIds = kvProductIds,
                        ChannelIds = channelIds,
                        ChannelProductIds = channelProductIds,
                        ConnectStringName = connectStringName,
                        IsDeleteKvProduct = isDeleteKvProduct,
                        LogId = logId
                    };
                    mq.Publish(removeMsg);
                }
            }
            // Xóa mapping trước khi thực hiện đồng bộ (chưa xóa xong sẽ chờ)
            if (channelIds != null)
                foreach (var channelId in channelIds)
                {
                    SetCacheWaitingDeleteMapping(channelId, kvProductIds);
                }
        }

        public async Task<List<ProductMapping>> GetMappingsByProductIds(BaseRetryMessage data, List<long> kvProductIds)
        {
         
            var redisMapProducts = kvProductIds.Select(p =>
                KvConstant.ListProductMappingCacheKey.FormatWith(data.RetailerId, data.ChannelId, p)).ToList();
            var cacheProductMaps = _cacheClient.GetAll<List<List<ProductMapping>>>(redisMapProducts);
            var productMaps = cacheProductMaps.Where(x => x.Value != null).SelectMany(x => x.Value).SelectMany(x => x)
                .ToList();

            var cacheMapProductIds = productMaps.Select(p => p.ProductKvId).ToList();
            if (cacheMapProductIds.Any())
            {
                var productMissingInCache = kvProductIds.Except(cacheMapProductIds).ToList();
                if (!productMissingInCache.Any())
                {
                    return productMaps;
                }
                var productMapMongos =
                    await _productMappingService.GetByChannelProductIds(data.RetailerId, data.ChannelId, null,
                        productMissingInCache, data.ChannelType == (byte)ChannelType.Sendo, isStringItemId: ConvertHelper.CheckUseStringItemId(data.ChannelType));
                if (productMapMongos.Any())
                {
                    productMaps.AddRange(productMapMongos);
                }
            }
            else
            {
                var productMapMongos =
                    await _productMappingService.GetByChannelProductIds(data.RetailerId, data.ChannelId, null,
                        kvProductIds, data.ChannelType == (byte)ChannelType.Sendo, isStringItemId: ConvertHelper.CheckUseStringItemId(data.ChannelType));
                if (productMapMongos.Any())
                {
                    productMaps.AddRange(productMapMongos);
                }
            }
            return productMaps;
        }

        public Task<List<ProductMapping>> GetMappingByItemIds(long channelId, int retailerId, List<string> itemIds)
        {
            return _productMappingService.GetByChannelProductIds(retailerId, channelId, itemIds);
        }

        /// <summary>
        /// Xóa mapping trước khi thực hiện đồng bộ (chưa xóa xong sẽ chờ)
        /// </summary>
        public void SetCacheWaitingDeleteMapping(long channelId, List<long> kvProductIds)
        {
            if (channelId <= 0 || kvProductIds?.Any() != true) return;

            foreach (var kvProductId in kvProductIds)
            {
                _logger.Info($"SetCacheWaitingDeleteMapping channelId: {channelId}, kvProductId: {kvProductId}");
                _cacheClient.Set(KvConstant.LockWaitingDeleteMappingCache.Fmt(channelId, kvProductId), true, TimeSpan.FromMilliseconds(WaitingDeleteMappingExpireIn));
            }
        }

        public void RemoveCacheWaitingDeleteMapping(long channelId, List<long> kvProductIds)
        {
            if (channelId <= 0 || kvProductIds?.Any() != true) return;

            foreach (var kvProductId in kvProductIds)
            {
                _cacheClient.Remove(KvConstant.LockWaitingDeleteMappingCache.Fmt(channelId, kvProductId));
            }
        }

        /// <summary>
        /// Chờ xóa mapping trước khi thực hiện đồng bộ
        /// </summary>
        public async Task WaitingDeleteMapping(long channelId, List<long> kvProductIds)
        {
            var retryCount = 0;
            var maximumRetry = Math.Ceiling((double)WaitingDeleteMappingExpireIn / WaitingDeleteMappingDelay);
            while (IsWaiting() && retryCount <= maximumRetry)
            {
                _logger.Info($"WaitingDeleteMapping channelId: {channelId}");
                await Task.Delay(WaitingDeleteMappingDelay);
                retryCount++;
            }

            bool IsWaiting()
            {
                foreach (var kvProductId in kvProductIds)
                {
                    var isWaiting = _cacheClient.Get<bool>(KvConstant.LockWaitingDeleteMappingCache.Fmt(channelId, kvProductId));

                    if (isWaiting) return true;
                }

                return false;
            }
        }
    }
}

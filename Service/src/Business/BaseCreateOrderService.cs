﻿using Amazon.Auth.AccessControlPolicy;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.Domain.Common;
using Demo.OmniChannel.DomainService.Helper;
using Demo.OmniChannel.DomainService.Interfaces;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Sdk.Interfaces;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.EventLogMessage;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore.Infrastructure;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using KVCoreContext = Demo.OmniChannelCore.Api.Sdk.Common;
using SDKCCoreInterface = Demo.OmniChannelCore.Api.Sdk.Interfaces;
using SDKCommon = Demo.OmniChannelCore.Api.Sdk.Common;
using SDKCoreModel = Demo.OmniChannelCore.Api.Sdk.Models;

namespace Demo.OmniChannel.Business.Implements
{
    public abstract class BaseCreateOrderService<T> : IBaseCreateOrderBusiness where T : ShareKernel.RedisStreamMessage.CreateOrderMessage
    {
        private readonly IIntegrationEventService _integrationEventService;
        public readonly IDbConnectionFactory _dbConnectionFactory;
        public readonly IRedisClientsManager _redisClientsManager;
        private readonly ICacheClient _cacheClient;
        private readonly IOrderMongoService _orderMongoService;
        private readonly IInvoiceMongoService _invoiceMongoService;
        private readonly IRetryOrderMongoService _retryOrderMongoService;
        private readonly IRetryInvoiceMongoService _retryInvoiceMongoService;
        private readonly IMessageService _messageService;
        protected IAppSettings _appSettings;
        private readonly IAuditTrailInternalClient _auditTrailInternalClient;
        private readonly IDeliveryInfoInternalClient _deliveryInfoInternalClient;
        private readonly ISurChargeInternalClient _surChargeInternalClient;
        private readonly IOmniChannelSettingService _omniChannelSettingService;
        private readonly IProductMappingService _productMappingService;
        private readonly ISendoLocationMongoService _sendoLocationMongoService;
        private readonly ISurChargeBranchInternalClient _surChargeBranchInternalClient;
        private readonly IIntegrationInternalClient _integrationInternalClient;
        private readonly ICustomerInternalClient _customerInternalClient;
        private readonly IPartnerDeliveryInternalClient _partnerInternalClient;
        private readonly IKvLockRedis _kvLockRedis;
        private readonly SDKCCoreInterface.IOrderInternalClient _orderInternalClient;
        private readonly ICreateOrderDomainService _createOrderDomainService;
        protected int MAX_RETRY = 5;

        protected byte OmniChannelType;
        protected T Data;


        public BaseCreateOrderService(
            IIntegrationEventService integrationEventService,
            IDbConnectionFactory dbConnectionFactory,
            IRedisClientsManager redisClientsManager,
            ICacheClient cacheClient,
            IOrderMongoService orderMongoService,
            IInvoiceMongoService invoiceMongoService,
            IRetryOrderMongoService retryOrderMongoService,
            IRetryInvoiceMongoService retryInvoiceMongoService,
            IAuditTrailInternalClient auditTrailInternalClient,
            ISurChargeInternalClient surChargeInternalClient,
            IOmniChannelSettingService omniChannelSettingService,
            IProductMappingService productMappingService,
            ISendoLocationMongoService sendoLocationMongoService,
            ISurChargeBranchInternalClient surChargeBranchInternalClient,
            IIntegrationInternalClient integrationInternalClient,
            ICustomerInternalClient customerInternalClient,
            SDKCCoreInterface.IOrderInternalClient orderInternalClient,
            IPartnerDeliveryInternalClient partnerInternalClient,
            IKvLockRedis kvLockRedis,
            IMessageService messageService,
            IAppSettings appSettings,
            IDeliveryInfoInternalClient deliveryInfoInternalClient,
            ICreateOrderDomainService createOrderDomainService)
        {
            _dbConnectionFactory = dbConnectionFactory;
            _redisClientsManager = redisClientsManager;
            _cacheClient = cacheClient;
            _orderMongoService = orderMongoService;
            _invoiceMongoService = invoiceMongoService;
            _retryOrderMongoService = retryOrderMongoService;
            _retryInvoiceMongoService = retryInvoiceMongoService;
            _auditTrailInternalClient = auditTrailInternalClient;
            _surChargeInternalClient = surChargeInternalClient;
            _omniChannelSettingService = omniChannelSettingService;
            _productMappingService = productMappingService;
            _sendoLocationMongoService = sendoLocationMongoService;
            _surChargeBranchInternalClient = surChargeBranchInternalClient;
            _integrationInternalClient = integrationInternalClient;
            _customerInternalClient = customerInternalClient;
            _orderInternalClient = orderInternalClient;
            _partnerInternalClient = partnerInternalClient;
            _kvLockRedis = kvLockRedis;
            _messageService = messageService;
            _appSettings = appSettings;
            _integrationEventService = integrationEventService;
            _deliveryInfoInternalClient = deliveryInfoInternalClient;
            _createOrderDomainService = createOrderDomainService;
        }

        #region abstractMethod
        protected abstract T DeserializeRequest(string request);
        protected abstract KvOrder GenKvOrder(KvInternalContext context, OmniChannelSettingObject setting, T message);
        protected abstract string GetOrderId(T message);
        protected abstract Task<bool> SkipCreateCustomer(KvInternalContext context);
        protected abstract bool ValidateMessage(T message);

        #endregion

        #region impliments method
        public async Task<bool> CreateOrderServiceV2(string request)
        {
            if (ValidateRequest(request))
            {
                try
                {
                    var message = DeserializeRequest(request);
                    Data = message;
                    if (message.RetryProcessService)
                    {
                        PushMessageToOrderTrackingEvent(message, "RetryProcess order, ignore!");
                        return false;
                    }
                    message.LogId = Guid.NewGuid();
                    PushLishMQToCreateLog((int)LogType.Info, message.LogId, "ReceiveOrderInfoFromGRPC", request.ToJson(), null, 0, 0, null, null);
                    var channels = await GetByIdentityKey(OmniChannelType, message.ShopId.ToString());
                    channels = await FilterChannelWithConfig(channels);
                    if (channels == null || !channels.Any())
                    {
                        PushLishMQToCreateLog((int)LogType.Warning, message.LogId, null, null, null, 0, 0, "Omni channel is null", null);
                        PushMessageToOrderTrackingEvent(message, "Omni channel is null");
                        return true;
                    }
                    foreach (var channel in channels)
                    {
                        await ProgressOrder(channel, message);
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    PushLishMQToCreateLog((int)LogType.Error, Guid.NewGuid(), "CreateOrderServiceV2", request, null, 0, 0, ex.StackTrace, ex);
                }
            }
            return false;

        }

        public async Task<bool> RetryOrderServiceV2(Guid logId, int retailerId, int branchId, int omniChannelId, string kvOrder)
        {
            try
            {
                PushLishMQToCreateLog((int)LogType.Info, logId, "ReceiveOrderFromRetryOrderError", kvOrder, null, retailerId, omniChannelId, null, null);
                var channel = await GetByOmniChannelId(omniChannelId);
                if (channel == null)
                {
                    PushLishMQToCreateLog((int)LogType.Warning, logId, null, null, null, retailerId, omniChannelId, "Omni channel is null", null);
                    return true;
                }
                PushLishMQToCreateLog((int)LogType.Info, logId, "BeginProressRetryOrder", kvOrder, null, retailerId, omniChannelId, null, null);
                await ProgressRetryOrder(channel, kvOrder, logId);
            }
            catch (Exception ex)
            {
                PushLishMQToCreateLog((int)LogType.Error, logId, kvOrder, null, null, retailerId, omniChannelId, "", ex);
            }
            return true;
        }

        public async Task<bool> SyncErrorOrderServiceV2(Guid logId, int retailerId, int branchId, int omniChannelId, string orderSn)
        {
            try
            {
                PushLishMQToCreateLog((int)LogType.Info, logId, "ReceiveOrderFromOrderError", orderSn, null, retailerId, omniChannelId, null, null);
                var channel = await GetByOmniChannelId(omniChannelId);
                if (channel == null)
                {
                    PushLishMQToCreateLog((int)LogType.Warning, logId, null, null, null, retailerId, omniChannelId, "Omni channel is null", null);
                    return true;
                }
                var existOrder = await _orderMongoService.GetByOrderId(orderSn, branchId, channel.Id);
                if (existOrder != null)
                {
                    if (!string.IsNullOrEmpty(existOrder.KvOrder))
                    {
                        PushLishMQToCreateLog((int)LogType.Info, logId, "BeginProressErrorOrder", channel.ToSafeJson(), null, channel.RetailerId, channel.Id, null, null);
                        await ProgressRetryOrder(channel, existOrder.KvOrder, logId);
                    }
                    else
                    {
                        PushLishMQToCreateLog((int)LogType.Info, logId, "BeginPushMesageErrorOrderRedis", channel.ToSafeJson(), null, channel.RetailerId, channel.Id, null, null);
                        await PushRedisOmni(channel, existOrder, logId);
                    }
                }
            }
            catch (Exception ex)
            {
                PushLishMQToCreateLog((int)LogType.Error, logId, orderSn, null, null, retailerId, omniChannelId, "", ex);
            }
            return true;
        }

        #endregion

        #region virtual method
        protected virtual void PushRetryMessage(string message, byte channelType)
        {

        }
        protected virtual bool FilterStatus(KvOrder kvOrder, Guid id, Domain.Model.OmniChannel channel)
        {
            return true;
        }
        protected virtual Task<List<Domain.Model.OmniChannel>> FilterChannelWithConfig(List<Domain.Model.OmniChannel> channels)
        {
            return Task.FromResult(channels);
        }

        protected virtual void PushMessageToOrderTrackingEvent(T message, string error = "")
        {
            if (message?.Source != null && message.Source.ToLower().Equals(ProcessOrderSource.WebHook))
            {
                string exChangeTrackProcessExChange = _appSettings.Get<string>("OrderTrackingEventExChange");
                var eventModel = new TrackModelEvent
                {
                    Code = message.Code,
                    OrderSn = message.OrderSn,
                    ErrorMessage = error,
                    Status = ProcessOrderStatus.Processed,
                    OrderTimestamp = message.Timestamps ?? 0,
                    ShopId = message.ShopId,
                    Source = message.Source,
                    ProcessType = ProcessFlowType.Order
                };
                _integrationEventService.AddEventWithRoutingKeyAsync(new OrderTrackingEvent
                {
                    TrackModelEvent = eventModel,
                }, RoutingKey.OrderTrackingEvent, exChangeTrackProcessExChange);
            }
        }

        protected virtual void PushLishKafkaAutoCreateProduct(KvOrder kvOrder, Domain.Model.OmniChannel channel, KvInternalContext context, OmniChannelSettingObject settings)
        { }
        #endregion

        #region protected method
        protected async Task<List<Domain.Model.OmniChannel>> GetByIdentityKey(byte channelType, string identityKey)
        {
            List<Domain.Model.OmniChannel> omniChannels = null;
            var identityCacheSetKey = string.Format(KvConstant.IdentityCacheSetKey, channelType, identityKey);

            using (var cli = _redisClientsManager.GetClient())
            {
                if (!cli.ContainsKey(identityCacheSetKey))
                {
                    using (var db = _dbConnectionFactory.OpenDbConnection())
                    {
                        var channelRepository = new OmniChannelRepository(db);
                        omniChannels = await channelRepository.GetChannelByIdentityKey(channelType, identityKey);
                        if (omniChannels != null && omniChannels.Any())
                        {
                            var channelIds = omniChannels.Select(x => x.Id.ToString()).ToList();
                            var cacheClient = cli.As<Domain.Model.OmniChannel>();
                            cacheClient.StoreAll(omniChannels);
                            cli.AddRangeToSet(identityCacheSetKey, channelIds);
                        }
                    }
                }
                else
                {
                    var channelIds = cli.GetAllItemsFromSet(identityCacheSetKey);
                    var cacheClient = cli.As<Domain.Model.OmniChannel>();
                    omniChannels = cacheClient.GetByIds(channelIds).ToList();
                }
            }

            return omniChannels;
        }

        protected async Task<KvInternalContext> CreateContext(int retailerId)
        {
            var retailer = await GetRetailer(retailerId);
            if (retailer == null) return null;
            var kvGroup = await GetRetailerGroup(retailer.GroupId);
            if (kvGroup == null) return null;

            var kvInternalContext = new KvInternalContext
            {
                RetailerCode = retailer.Code,
                RetailerId = retailer.Id,
                Group = new KvGroup
                {
                    Id = kvGroup?.Id ?? 0,
                    ConnectionString = kvGroup?.ConnectionString,
                    ConnectionStringName = kvGroup?.ConnectionString.Substring(kvGroup.ConnectionString.IndexOf('=') + 1)
                }
            };
            return kvInternalContext;
        }
        protected bool IsValidateFormula(OmniChannelSettingObject setting, KvOrder kvOrder)
        {
            if (setting.SyncOrderFormulaType == (byte)SyncOrderFormulaType.Create &&
                setting.SyncOrderFormulaDateTime != null &&
                kvOrder.ChannelCreatedDate <= setting.SyncOrderFormulaDateTime)
            {
                return false;
            }
            return true;
        }
        protected async Task<(bool, Domain.Model.Order)> CheckExistedOrder(KvInternalContext context, string orderCode, byte type)
        {
            using (var db = await _dbConnectionFactory.OpenAsync(context.Group.ConnectionStringName.ToLower()))
            {
                var kvOrderCode = $"{ChannelTypeHelper.GetOrderPrefix(type)}_{orderCode}";
                var orderRepository = new OrderRepository(db);
                var existed = await orderRepository.GetOrderByCodeWithUncommited(context.RetailerId, kvOrderCode);
                if (existed == null)
                {
                    return (false, new Domain.Model.Order());
                }
                return (true, existed);
            }
        }
        protected async Task RemoveErrorOrderInMongo(string orderId, int branchId, long channelId, bool isRemoveInvoiceError)
        {
            var orderInMongo = await _orderMongoService.GetByOrderId(orderId, branchId, channelId);
            if (orderInMongo != null)
            {
                await _orderMongoService.RemoveAsync(orderInMongo.Id);
            }

            if (isRemoveInvoiceError)
            {
                var invoiceInMongo = await _invoiceMongoService.GetByOrderId(orderId, branchId, channelId);
                if (invoiceInMongo != null)
                {
                    foreach (var invoice in invoiceInMongo)
                    {
                        await _invoiceMongoService.RemoveAsync(invoice.Id);
                    }
                }
            }
        }
        protected async Task RemoveMongoRetryOrder(string orderId, int branchId, long channelId, int retailerId)
        {
            var existRetryOrder = await _retryOrderMongoService.GetByOrderId(orderId, channelId, branchId, retailerId);
            if (existRetryOrder != null && !existRetryOrder.IsDbException)
            {
                await _retryOrderMongoService.RemoveAsync(existRetryOrder.Id);
            }

            var existRetryInvoice =
                await _retryInvoiceMongoService.GetByOrderId(orderId, channelId, branchId, retailerId);
            if (existRetryInvoice != null && !existRetryInvoice.IsDbException)
            {
                await _retryInvoiceMongoService.RemoveAsync(existRetryInvoice.Id);
            }
        }
        protected void PushLishMQToCreateLog(int typeOfLog, Guid id, string action, string requestObject, string responseObject, int? retailerId, long? omniChannelId, string message, Exception exception)
        {
            SeriLogObject log = new SeriLogObject(id)
            {
                Action = action,
                RequestObject = requestObject,
                ResponseObject = responseObject,
                RetailerId = retailerId,
                OmniChannelId = omniChannelId
            };
            switch (typeOfLog)
            {
                case (int)LogType.Warning:
                    log.Description = message;
                    log.LogWarning();
                    break;
                case (int)LogType.Error:
                    log.LogError(exception);
                    break;
                default:
                    log.Description = message;
                    log.LogInfo();
                    break;
            }
        }
        protected async Task PublishCreateInvoiceQueue(KvInternalContext context, Domain.Model.OmniChannel channel, KvOrder kvOrder, Domain.Model.Order existOrder, bool hasCreateOrder)
        {
            if (hasCreateOrder)
            {
                var millisecondsDelay = NumberHelper.GetValueOrDefault(_appSettings?.Get<int>("ChannelProductLock"), 5) * 1000;
                await Task.Delay(millisecondsDelay);
            }
            var configCreateInvoiceFeature = _appSettings.Get<CreateInvoiceV2Feature>("Kafka:CreateInvoiceV2Feature");

            using (var mq = _messageService.MessageFactory.CreateMessageProducer())
            {
                switch (channel.Type)
                {
                    case (byte)ChannelTypeEnum.Shopee:
                        var createInvoice = new ShopeeCreateInvoiceMessageV2
                        {
                            ChannelId = channel.Id,
                            ChannelType = (byte)ChannelType.Shopee,
                            BranchId = channel.BranchId,
                            KvEntities = context.Group.ConnectionString,
                            RetailerId = context.RetailerId,
                            OrderId = kvOrder.Ordersn,
                            Order = kvOrder.ToSafeJson(),
                            KvOrder = existOrder.ToSafeJson(),
                            KvOrderId = existOrder.Id,
                            LogId = context.LogId,
                        };
                        if (Data != null)
                        {
                            createInvoice.OrderSn = Data.OrderSn;
                            createInvoice.OrderSn = Data.OrderSn;
                            createInvoice.ShopId = Data.ShopId;
                            createInvoice.Source = Data.Source;
                            createInvoice.Timestamps = Data.Timestamps ?? 0;
                            createInvoice.CodeSource = Data.Code;
                        }
                        if (configCreateInvoiceFeature.IsValid(context.Group?.Id ?? 0, context.RetailerId))
                        {
                            KafkaClient.KafkaClient.Instance.PublishMessage($"{configCreateInvoiceFeature.TopicCreateInvoiceShopee}",
                            string.Empty, JsonConvert.SerializeObject(createInvoice));
                        }
                        else
                        {
                            mq.Publish(createInvoice);
                        }
                        break;

                    case (byte)ChannelType.Tiktok:
                        var createTiktokOrder = new TiktokCreateInvoiceMessageV2
                        {
                            ChannelId = channel.Id,
                            ChannelType = (byte)ChannelType.Tiktok,
                            BranchId = channel.BranchId,
                            KvEntities = context.Group.ConnectionString,
                            RetailerId = context.RetailerId,
                            OrderId = kvOrder.Ordersn,
                            Order = kvOrder.ToSafeJson(),
                            KvOrder = existOrder.ToSafeJson(),
                            KvOrderId = existOrder.Id,
                            LogId = context.LogId,

                        };
                        if (Data != null)
                        {
                            createTiktokOrder.OrderSn = Data.OrderSn;
                            createTiktokOrder.OrderSn = Data.OrderSn;
                            createTiktokOrder.ShopId = Data.ShopId;
                            createTiktokOrder.Source = Data.Source;
                            createTiktokOrder.Timestamps = Data.Timestamps ?? 0;
                            createTiktokOrder.CodeSource = Data.Code;
                        }
                        if (configCreateInvoiceFeature.IsValid(context.Group?.Id ?? 0, context.RetailerId))
                        {
                            KafkaClient.KafkaClient.Instance.PublishMessage($"{configCreateInvoiceFeature.TopicCreateInvoiceTiktok}",
                            string.Empty, JsonConvert.SerializeObject(createTiktokOrder));
                        }
                        else
                        {
                            mq.Publish(createTiktokOrder);
                        }
                        break;

                    default:
                        break;
                }
            }
        }
        protected async Task<Domain.Model.SaleChannel> GetSaleChannel(IDbConnection db, KvInternalContext context, long channelId)
        {
            var saleChannelRepository = new SaleChannelRepository(db);
            return await saleChannelRepository.GetByOmmiChannelId(context.RetailerId, channelId);
        }
        protected async Task<Repository.Common.PosSetting> CheckPosSetting(IDbConnection db, KvInternalContext context)
        {
            var posSettingRepository = new PosSettingRepository(db);
            var postSettingDic = await posSettingRepository.GetSettingAsync(context.RetailerId);
            var posSetting = new Repository.Common.PosSetting(postSettingDic, context.RetailerId, _dbConnectionFactory, _cacheClient, _appSettings);
            return posSetting;
        }
        protected async Task TurnOffSalerHandle(KVCoreContext.KvInternalContext coreContext, KvInternalContext context, byte type)
        {
            var log = new Audit.Model.Message.AuditTrailLog
            {
                FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(type),
                Action = (int)AuditTrailAction.OrderIntergate,
                CreatedDate = DateTime.Now,
                BranchId = context.BranchId,
                Content = "Không đồng bộ đơn đặt hàng do chưa bật tính năng đặt hàng"
            };
            await _auditTrailInternalClient.AddLogAsync(coreContext, log);
        }
        protected async Task<KVCoreContext.KvInternalContext> CreateKVCoreContext(KvInternalContext context)
        {
            var coreContext = await ContextHelper.GetExecutionContext(_cacheClient, _dbConnectionFactory,
                    context.RetailerId, context.Group.ConnectionStringName, context.BranchId, _appSettings?.Get<int>("ExecutionContext"));
            var coreSDKContext = coreContext.ConvertTo<KVCoreContext.KvInternalContext>();
            coreSDKContext.UserId = coreContext.User.Id;
            return coreSDKContext;
        }
        protected async Task ProcessLocation(KvOrder kvOrder)
        {
            kvOrder.CustomerLocation = await GetSendoLocation(kvOrder.OrderDelivery.ReceiverDistrictId);

            if (kvOrder?.OrderDelivery != null)
            {
                if (string.IsNullOrEmpty(kvOrder.OrderDelivery.LocationName) &&
                    !string.IsNullOrEmpty(kvOrder.CustomerLocation))
                {
                    kvOrder.OrderDelivery.LocationName = kvOrder.CustomerLocation;
                }

                if (string.IsNullOrEmpty(kvOrder.OrderDelivery.WardName) &&
                    !string.IsNullOrEmpty(kvOrder.CustomerWard))
                {
                    kvOrder.OrderDelivery.WardName = kvOrder.CustomerWard;
                }

                GetKvLocationWard(kvOrder);
            }
        }
        protected async Task<List<SDKCoreModel.InvoiceSurCharges>> GetListInvoiceSurchargeAsync(KVCoreContext.KvInternalContext coreContext, KvInternalContext context, KvOrder kvOrder, Domain.Model.OmniChannel channel)
        {
            var surCharges = kvOrder.SurCharges?.ToList();
            var lstInvoiceSurcharge = new List<SDKCoreModel.InvoiceSurCharges>();
            if (surCharges != null && surCharges.Count > 0)
            {
                foreach (var itemSurCharge in surCharges)
                {
                    var codeByGen = ChannelTypeHelper.GenerateSurchargeCode(channel.Type, context.RetailerId,
                        itemSurCharge.Type ?? 0);
                    var surcharge =
                        await _surChargeInternalClient.GetSurchargeByCode(coreContext, codeByGen);

                    if (surcharge != null && !surcharge.isActive)
                    {
                        continue;
                    }

                    if (surcharge == null)
                    {
                        var surchargeTmp = new Demo.OmniChannelCore.Api.Sdk.Models.SurCharge
                        {
                            Code = codeByGen,
                            Name = itemSurCharge.Name,
                            Value = itemSurCharge.Value,
                            RetailerId = context.RetailerId,
                            isActive = true,
                            isAuto = false,
                            ForAllBranch = true,
                            isReturnAuto = true
                        };
                        surcharge = await _surChargeInternalClient.CreateSurCharge(coreContext, surchargeTmp);
                    }

                    var isApproveSurcharge = await CheckSurchargeActiveWithBranch(coreContext, surcharge, context.BranchId);

                    if (isApproveSurcharge)
                    {
                        var invoiceSurcharge = new SDKCoreModel.InvoiceSurCharges
                        {
                            SurchargeId = surcharge?.Id ?? 0,
                            Price = itemSurCharge.Value ?? 0,
                            RetailerId = context.RetailerId,
                            Name = surcharge?.Name,
                            SurValue = itemSurCharge.Value
                        };

                        lstInvoiceSurcharge.Add(invoiceSurcharge);
                    }
                }
            }
            return lstInvoiceSurcharge;
        }
        protected async Task<(bool, string, string)> MappingHandle(IDbConnection db, KvInternalContext context, KvOrder kvOrder, Domain.Model.OmniChannel channel)
        {
            PushLishMQToCreateLog((int)LogType.Info, context.LogId, "MappingHandle", $"{kvOrder.Ordersn} with data : {string.Join(",", kvOrder.OrderDetails.Select(item => item.ProductChannelId))}", null, channel.RetailerId, channel.Id, null, null);
            var channelSettings = await _omniChannelSettingService.GetChannelSettings(channel.Id, context.RetailerId);
            var channelProductIds = kvOrder.OrderDetails.Where(x => !string.IsNullOrEmpty(x.ProductChannelId))
            .Select(x => x.ProductChannelId).ToList();
            List<Demo.OmniChannel.MongoDb.ProductMapping> mappings = null;
            if (channelProductIds.Any())
            {
                mappings = await _productMappingService.GetByChannelProductIds(context.RetailerId, channel.Id, channelProductIds, isStringItemId: ConvertHelper.CheckUseStringItemId(channel.Type));
            }

            foreach (var detail in kvOrder.OrderDetails)
            {
                var mapping = mappings?.FirstOrDefault(x =>
                    !string.IsNullOrEmpty(detail.ProductChannelId) &&
                    x.CommonProductChannelId == detail.ProductChannelId);

                detail.ProductId = mapping?.ProductKvId ?? 0;
                detail.ProductCode = mapping?.ProductKvSku ?? string.Empty;
            }

            var kvProductIds = kvOrder.OrderDetails.Where(x => x.ProductId > 0).Select(x => x.ProductId)
                          .ToList();
            var productRepository = new ProductRepository(db);
            var kvProducts = await productRepository.GetProductByIds(context.RetailerId, context.BranchId, kvProductIds, isLogMonitor: false);
            if (channelSettings.IsAutoSyncBatchExpire)
            {
                kvOrder.OrderDetails = DetechHasExpireHelper.DetectOrderDetailHasBatchExpire(kvProducts, kvOrder.OrderDetails);
            }
            return ValidateProductHandle(channel, kvOrder, kvProducts, mappings);
        }
        protected async Task SaveOrderSyncError(KVCoreContext.KvInternalContext coreContext, KvInternalContext context, KvOrder kvOrder, string errorMessage, string productLog, Domain.Model.OmniChannel channel)
        {
            var existOrder = await _orderMongoService.GetByOrderId(kvOrder.Ordersn, context.BranchId, channel.Id);
            var retryOrder = new MongoDb.Order
            {
                KvOrder = kvOrder.ToSafeJson(),
                Code = kvOrder.Code,
                BranchId = kvOrder.BranchId,
                SoldById = kvOrder.SoldById ?? 0,
                SaleChannelId = kvOrder.SaleChannelId ?? 0,
                PurchaseDate = kvOrder.PurchaseDate,
                Description = kvOrder.Description,
                IsSyncSuccess = false,
                UsingCod = true,
                OrderDelivery = kvOrder.OrderDelivery?.ConvertTo<MongoDb.InvoiceDelivery>(),
                CustomerId = kvOrder.CustomerId ?? 0,
                CustomerPhone = kvOrder.CustomerPhone,
                CustomerCode = kvOrder.CustomerCode,
                CustomerName = kvOrder.CustomerName,
                CustomerAddress = kvOrder.CustomerAddress,
                NewStatus = kvOrder.NewStatus,
                Status = kvOrder.Status,
                ChannelStatus = kvOrder.ChannelStatus,
                ChannelId = channel.Id,
                ErrorMessage = errorMessage,
                OrderId = kvOrder.Ordersn,
                RetailerId = context.RetailerId,
                CreatedDate = DateTime.Now,
                Discount = kvOrder.Discount ?? 0,
                OrderSurCharges = kvOrder.SurCharges?.Select(p => new MongoDb.InvoiceSurCharges
                {
                    Name = p.Name,
                    SurValue = p.Value,
                    Price = p.Value ?? 0
                }).ToList() ?? new List<MongoDb.InvoiceSurCharges>(),
                OrderDetails = kvOrder.OrderDetails?.Select(p => new MongoDb.OrderDetail
                {
                    ProductId = p.ProductId,
                    ProductCode = p.ProductCode,
                    ProductName = p.ProductName,
                    Price = p.Price,
                    Discount = p.Discount.GetValueOrDefault(),
                    DiscountPrice = p.DiscountPrice,
                    CommonProductChannelId = p.ProductChannelId,
                    DiscountRatio = p.DiscountRatio.GetValueOrDefault(),
                    Note = p.Note,
                    CommonParentChannelProductId = p.ParentChannelProductId,
                    ProductChannelName = p.ProductChannelName,
                    ProductChannelSku = p.ProductChannelSku,
                    Quantity = p.Quantity,
                    UseProductBatchExpire = p.UseProductBatchExpire,
                    UseProductSerial = p.UseProductSerial,
                    Uuid = p.Uuid
                }).ToList()
            };
            if (existOrder == null)
            {
                await _orderMongoService.AddSync(retryOrder);
                PushLishMQToCreateLog((int)LogType.Warning, context.LogId, "AfterSaveMongo", $"Id:{retryOrder.Id}", null, context.RetailerId, channel.Id, null, null);
            }
            else
            {
                retryOrder.Id = existOrder.Id;
                await _orderMongoService.UpdateAsync(existOrder.Id, retryOrder);
            }
            #region Audit Trail
            PushLishMessageErrorNotification(kvOrder, channel, context.LogId, errorMessage);
            await WriteLogCreateOrderFail(coreContext, context, kvOrder, channel, productLog);
            #endregion
        }
        protected async Task CustomerHandle(IDbConnection db, KVCoreContext.KvInternalContext coreContext, KvInternalContext context, KvOrder kvOrder, Repository.Common.PosSetting posSetting, Domain.Model.OmniChannel channel)
        {
            if (await SkipCreateCustomer(context))
            {
                PushLishMQToCreateLog((int)LogType.Info, context.LogId, "CustomerHandle", null, kvOrder.Ordersn, channel.RetailerId, channel.Id, "Settings of syncCustomer", null);
                return;
            }

            var productRepository = new ProductRepository(db);
            var kvProductIds = kvOrder.OrderDetails.Where(x => x.ProductId > 0).Select(x => x.ProductId)
                       .ToList();
            var kvProducts = await productRepository.GetProductByIds(context.RetailerId, context.BranchId, kvProductIds, isLogMonitor: false);
            var customer = await CreateCustomerAsync(db, coreContext, context, kvOrder, posSetting?.ManagerCustomerByBranch, channel);
            PushLishMQToCreateLog((int)LogType.Info, context.LogId, "CustomerHandle", null, customer.newCustomer?.Code, channel.RetailerId, channel.Id, null, null);
            if (customer.isError)
            {
                await WriteLogErrNotCreateCustomer(coreContext, kvOrder, kvProducts, context, channel);
                throw new OmniException("Có lỗi tạo khách hàng");
            }
            if (customer.isRetailerExpireException)
            {
                PushLishMQToCreateLog((int)LogType.Warning, context.LogId, "CustomerHandle", null, null, 0, 0, "Retailer expire exception", null);
                await DeActiveChannel(channel.Id, coreContext, channel.Type);
                throw new ShopeeException("Retailer expire exception");
            }
            kvOrder.UpdateCustomerCodeAndName(customer.newCustomer.Id, customer.newCustomer.Code, customer.newCustomer.Name, customer.newCustomer.ContactNumber);
        }
        protected async Task<(Domain.Model.Customer newCustomer, bool isError, bool isRetailerExpireException)> CreateCustomerAsync(IDbConnection db, KVCoreContext.KvInternalContext coreContext, KvInternalContext context, KvOrder kvOrder, bool? managerCustomerByBranch, Domain.Model.OmniChannel channel)
        {
            var isManagerCustomerByBranch = managerCustomerByBranch ?? false;
            var newCustomer = new Domain.Model.Customer();
            var customerRepository = new CustomerRepository(db);
            var phone = PhoneNumberHelper.GetPerfectContactNumber(kvOrder.CustomerPhone);

            if (channel.Type != (byte)ChannelType.Tiktok
                && !PhoneNumberHelper.IsValidContactNumber(phone, RegionCode.VN))
            {
                return (newCustomer, false, false);
            }

            if (channel.Type == (byte)ChannelType.Tiktok && isManagerCustomerByBranch)
            {
                kvOrder.CustomerCode = $"{kvOrder.CustomerCode}_{channel.BranchId}";
            }
            using (var cli = _kvLockRedis.GetLockFactory())
            {
                var lockAddCustomerKey = phone;
                if (string.IsNullOrEmpty(phone))
                {
                    lockAddCustomerKey = kvOrder.Code;
                }

                using (cli.AcquireLock(string.Format(KvConstant.LockAddCustomer, lockAddCustomerKey),
                    TimeSpan.FromSeconds(60)))
                {
                    Domain.Model.Customer existsCustomer = await _GetCustomerAsync(customerRepository, context.RetailerId,
                        channel.BranchId, phone, kvOrder.CustomerCode, isManagerCustomerByBranch);

                    newCustomer.ContactNumber = phone;

                    if (existsCustomer == null)
                    {
                        try
                        {
                            var customer = new Demo.OmniChannelCore.Api.Sdk.Models.Customer
                            {
                                Code = kvOrder.CustomerCode,
                                ContactNumber = phone,
                                Name = kvOrder.CustomerName,
                                Address = kvOrder.CustomerAddress,
                                BranchId = channel.BranchId,
                                RetailerId = context.RetailerId
                            };
                            if (!string.IsNullOrEmpty(kvOrder.CustomerLocation))
                            {
                                customer.LocationName = kvOrder.CustomerLocation;
                            }

                            if (!string.IsNullOrEmpty(kvOrder.CustomerWard))
                            {
                                customer.WardName = kvOrder.CustomerWard;
                            }
                            var customerId = await _customerInternalClient.CreateCustomerAsync(coreContext, customer);

                            if (customerId < 1)
                            {
                                return (newCustomer, true, false);
                            }

                            newCustomer.Id = customerId;
                            newCustomer.Code = kvOrder.CustomerCode;
                            newCustomer.Name = kvOrder.CustomerName;
                        }
                        catch (Exception ex)
                        {
                            if (ex is SDKCommon.KvRetailerExpireException)
                            {
                                return (newCustomer, false, true);
                            }

                            existsCustomer = await _GetCustomerAsync(customerRepository, context.RetailerId, channel.BranchId, phone, kvOrder.CustomerCode, isManagerCustomerByBranch);

                            if (existsCustomer != null)
                            {
                                newCustomer.Id = existsCustomer.Id;
                                newCustomer.Code = existsCustomer.Code;
                                newCustomer.Name = existsCustomer.Name;
                            }
                        }
                    }
                    else
                    {
                        newCustomer.Id = existsCustomer.Id;
                        newCustomer.Code = existsCustomer.Code;
                        newCustomer.Name = existsCustomer.Name;
                    }
                }
            }
            return (newCustomer, false, false);
        }
        protected async Task PartnerDeliveryHandle(IDbConnection db, KVCoreContext.KvInternalContext coreContext, KvInternalContext context, KvOrder kvOrder, Domain.Model.OmniChannel channel)
        {
            PushLishMQToCreateLog((int)LogType.Info, context.LogId, "PartnerDeliveryHandle", null, null, channel.RetailerId, channel.Id, null, null);
            var (deliveryBy, isRetailerExpireException) = await CreatePartnerAsync(db, coreContext, context, kvOrder, channel);
            if (isRetailerExpireException)
            {
                PushLishMQToCreateLog((int)LogType.Warning, context.LogId, "PartnerDeliveryHandle", null, null, 0, 0, "Retailer expire exception", null);
                await DeActiveChannel(channel.Id, coreContext, channel.Type);
            }
            if (deliveryBy > 0)
            {
                kvOrder.OrderDelivery.DeliveryBy = deliveryBy;
            }
        }
        protected async Task PriceBookHandle(IDbConnection db, KvInternalContext context, KvOrder kvOrder, Domain.Model.OmniChannel channel)
        {

            var priceBookRepository = new PriceBookRepository(db);

            var extra = new ChannelClient.Models.Extra
            {
                PriceBookId = new ChannelClient.Models.PriceBook
                {
                    Id = 0,
                    Name = "Bảng giá chung"
                }
            };
            if (channel.BasePriceBookId > 0)
            {
                var priceBook = await priceBookRepository.SingleAsync(p => p.RetailerId == context.RetailerId && p.Id == channel.BasePriceBookId);

                if (priceBook != null && priceBook.IsActive && priceBook.isDeleted != true &&
                    priceBook.StartDate < DateTime.Now && priceBook.EndDate > DateTime.Now)
                {
                    extra = new ChannelClient.Models.Extra
                    {
                        PriceBookId = new ChannelClient.Models.PriceBook
                        {
                            Id = priceBook.Id,
                            Name = priceBook.Name
                        }
                    };
                }
            }
            kvOrder.Extra = extra.ToSafeJson();
        }
        protected async Task BeforeCreateOrderSuccessHandle(KvInternalContext context, KvOrder kvOrder, Domain.Model.OmniChannel channel)
        {
            await RemoveMongoRetryOrder(kvOrder.Ordersn, context.BranchId, channel.Id, context.RetailerId);
        }
        protected async Task<SDKCoreModel.Order> CreateOrderHandle(KVCoreContext.KvInternalContext coreContext, KvInternalContext context, KvOrder kvOrder, Domain.Model.OmniChannel channel)
        {
            var coreSDKKVOrder = kvOrder.ConvertTo<Demo.OmniChannelCore.Api.Sdk.Models.Order>();
            coreSDKKVOrder.OrderDetails = kvOrder.OrderDetails.Map(x => x.ConvertTo<Demo.OmniChannelCore.Api.Sdk.Models.OrderDetail>());
            coreSDKKVOrder.OrderDelivery = kvOrder.OrderDelivery.ConvertTo<Demo.OmniChannelCore.Api.Sdk.Models.InvoiceDelivery>();
            coreSDKKVOrder.InvoiceWarranties = kvOrder.InvoiceWarranties.Map(x => x.ConvertTo<Demo.OmniChannelCore.Api.Sdk.Models.InvoiceWarranties>()); ;

            var orderDelivery = kvOrder.OrderDelivery.ConvertTo<Demo.OmniChannelCore.Api.Sdk.Models.InvoiceDelivery>();
            await SurchargeHandle(coreContext, context, coreSDKKVOrder, kvOrder, channel);
            PushLishMQToCreateLog((int)LogType.Info, context.LogId, "BeforeCallSDKCreateOrder", null, null, context.RetailerId, channel.Id, null, null);
            var result = await _orderInternalClient.CreateOrder(
                               coreContext,
                               coreSDKKVOrder,
                               coreSDKKVOrder.OrderDetails.ToList(),
                               orderDelivery);
            PushLishMQToCreateLog((int)LogType.Info, context.LogId, "CreateOrderSuccess", result.ToSafeJson(), null, context.RetailerId, channel.Id, null, null);
            return result;
        }
        protected async Task AfterCreateOrderSuccessHandle(KVCoreContext.KvInternalContext coreContext, KvInternalContext context, KvOrder kvOrder, Domain.Model.OmniChannel channel, SDKCoreModel.Order coreSDKKVOrder)
        {
            PushMessageToOrderTrackingEvent(Data);
            await RemoveErrorOrderInMongo(kvOrder.Ordersn, context.BranchId, channel.Id, kvOrder.NewStatus == (byte)OrderState.Void);
            await HandleAuditLogAfterSuccess(coreContext, context, kvOrder, channel.Type);
            if (kvOrder.NewStatus == (byte)OrderState.Void)
            {
                PushLishMQToCreateLog((int)LogType.Info, context.LogId, "AfterCreateOrderSuccessHandle", null, null, context.RetailerId, channel.Id, "void kvOrder", null);
                await VoidKvOrder(coreContext, context, kvOrder, coreSDKKVOrder.Id, channel.Type);
            }
            if (kvOrder.NewStatus == (byte)OrderState.Finalized)
            {
                var existOrder = coreSDKKVOrder.ConvertTo<Domain.Model.Order>();
                await PublishCreateInvoiceQueue(context, channel, kvOrder, existOrder, true);
                PushLishMQToCreateLog((int)LogType.Info, context.LogId, "PublishCreateInvoiceQueue", existOrder.ToSafeJson(), null, context.RetailerId, channel.Id, null, null);
            }
        }
        protected async Task ExceptionHandle(KVCoreContext.KvInternalContext coreContext, KvInternalContext context, Domain.Model.OmniChannel channel, KvOrder kvOrder, Exception ex)
        {
            if (ex is SDKCommon.KvRetailerExpireException)
            {
                await DeActiveChannel(channel.Id, coreContext, (byte)ChannelType.Shopee);
                await RemoveMongoRetryOrder(kvOrder.Ordersn, channel.BranchId, channel.Id, channel.RetailerId);
                return;
            }

            if (ex is SDKCommon.KvDbException || ex is SqlException || (ex.Message != null && (ex.Message.ToLower().Contains(DbExceptionMessages.ConnectionTimeout) || ex.Message.ToLower().Contains(DbExceptionMessages.TransactionDeadlocked))))
            {
                await AddOrUpdateRetryOrder(context, channel, kvOrder);
            }
            await BusinessChannelError(coreContext, context, kvOrder, channel, ex);
        }
        protected async Task<Domain.Model.OmniChannel> GetByOmniChannelId(int omniChannelId)
        {
            var omniCacheSetKey = string.Format(KvConstant.ChannelByIdKey, omniChannelId);

            var omniChannel = _cacheClient.Get<Domain.Model.OmniChannel>(omniCacheSetKey);
            if (omniChannel != null)
            {
                return omniChannel;
            }
            else
            {
                using (var db = _dbConnectionFactory.OpenDbConnection())
                {
                    var channelRepository = new OmniChannelRepository(db);
                    omniChannel = await channelRepository.GetById(omniChannelId);
                    if (omniChannel != null)
                    {
                        _cacheClient.Set(omniCacheSetKey, omniChannel);
                    }
                }
            }
            return omniChannel;
        }
        protected async Task InvoiceWarrantiesHandle(KvInternalContext context, KvOrder kvOrder)
        {
            kvOrder.InvoiceWarranties = await _createOrderDomainService.GetInvoiceWarranties(context,
               kvOrder.OrderDetails.Select(x => new MongoDb.OrderDetail { ProductId = x.ProductId, Uuid = x.Uuid }).ToList(),
               kvOrder.PurchaseDate);
            kvOrder.OrderDetails.ToList().ForEach(x => x.UpdatePropetiesUseWarranty(kvOrder.InvoiceWarranties));
        }
        #endregion

        #region private method

        private void PushLishMessageErrorNotification(KvOrder kvOrder, Domain.Model.OmniChannel channel, Guid logId, string errorMessage)
        {
            var obj = new Infrastructure.EventBus.Event.ErrorOrderInvoiceNotificationMesage(logId)
            {
                RetailerId = channel.RetailerId,
                BranchId = channel.BranchId,
                ChannelId = channel.Id,
                OrderKv = kvOrder.Code,
                OrderId = kvOrder.Ordersn,
                PurchaseDate = kvOrder.PurchaseDate,
                ErrorType = (int)Sdk.Common.SyncTabError.Order,
                ErrorMessage = errorMessage,
                Type = OmniChannelType
            };
            _integrationEventService.AddEventWithRoutingKeyAsync(obj, RoutingKey.MessageErrorNotification);
        }

        private bool ValidateRequest(string request)
        {
            if (string.IsNullOrWhiteSpace(request))
            {
                throw new ArgumentNullException(nameof(request));
            }
            return true;
        }
        private async Task ProgressRetryOrder(Domain.Model.OmniChannel channel, string kvOrderString, Guid logId)
        {
            var kvOrder = kvOrderString.FromJson<KvOrder>();
            var context = await CreateContext(channel.RetailerId);
            var settings = await _omniChannelSettingService.GetChannelSettings(channel.Id, context.RetailerId);
            context.LogId = logId;
            context.BranchId = channel.BranchId;
            var (isExisted, existedOrder) = await CheckExistedOrder(context, kvOrder.Ordersn, channel.Type);
            if (isExisted)
            {
                PushLishMQToCreateLog((int)LogType.Info, logId, "ProgressOrderIfExisted", null, null, channel.RetailerId, channel.Id, null, null);
                await ProgressOrderIfExisted(context, channel, kvOrder, existedOrder);
            }
            else
            {
                PushLishMQToCreateLog((int)LogType.Info, logId, "ProgressOrderIfNonExisted", null, null, channel.RetailerId, channel.Id, null, null);
                await ProgressOrderIfNonExisted(context, channel, kvOrder, settings);
            }
        }

        private async Task PushRedisOmni(Domain.Model.OmniChannel channel, Demo.OmniChannel.MongoDb.Order order, Guid logId)
        {
            var context = await CreateContext(channel.RetailerId);
            var message = new ShopeeSyncErrorOrderMessage
            {
                RetailerCode = context.RetailerCode,
                KvEntities = context.Group?.ConnectionString,
                Order = order.ToSafeJson(),
                RetailerId = channel.RetailerId,
                ChannelId = order.ChannelId,
                BranchId = channel.BranchId,
                LogId = logId,
                PlatformId = channel.PlatformId
            };
            using (var mq = _messageService.MessageFactory.CreateMessageProducer())
            {
                mq.Publish(message);
            }
        }

        private async Task ProgressOrder(Domain.Model.OmniChannel channel, T message)
        {
            var context = await CreateContext(channel.RetailerId);
            context.LogId = message.LogId;
            context.BranchId = channel.BranchId;
            try
            {
                if (channel.OmniChannelSchedules == null || channel.OmniChannelSchedules.All(x => x.Type != (byte)ScheduleType.SyncOrder))
                {
                    PushLishMQToCreateLog((int)LogType.Warning, message.LogId, "ProgressOrder", message.ToJson(), null, channel.RetailerId, channel.Id, "Turn off syncOrder", null);
                    PushMessageToOrderTrackingEvent(message, "Turn off syncOrder");
                    return;
                }
                if (!ValidateMessage(message))
                {
                    return;
                }
                var settings = await _omniChannelSettingService.GetChannelSettings(channel.Id, context.RetailerId);
                var kvOrder = GenKvOrder(context, settings, message);
                if (kvOrder == null)
                {
                    PushLishMQToCreateLog((int)LogType.Warning, message.LogId, "ProgressOrder", null, null, channel.RetailerId, channel.Id, "KvOrder is null", null);
                    PushMessageToOrderTrackingEvent(message, "KvOrder is null");
                    return;
                }

              
                var checkValidOrderWithFomula = IsValidateFormula(settings, kvOrder);
                if (!checkValidOrderWithFomula)
                {
                    PushMessageToOrderTrackingEvent(message, "Đơn hàng lấy từ ngày không hợp lệ");
                    PushLishMQToCreateLog((int)LogType.Warning, message.LogId, "ProgressOrder", kvOrder.Code, null, channel.RetailerId, channel.Id, "Đơn hàng lấy từ ngày không hợp lệ", null);
                    return;
                }
                var orderId = GetOrderId(message);
                var (isExisted, existedOrder) = await CheckExistedOrder(context, orderId, channel.Type);
                if (isExisted)
                {
                    PushLishMQToCreateLog((int)LogType.Info, message.LogId, "ProgressOrderIfExisted", orderId, null, channel.RetailerId, channel.Id, null, null);
                    await ProgressOrderIfExisted(context, channel, kvOrder, existedOrder);
                }
                else
                {
                    PushLishMQToCreateLog((int)LogType.Info, message.LogId, "ProgressOrderIfNonExisted", orderId, null, channel.RetailerId, channel.Id, null, null);
                    await ProgressOrderIfNonExisted(context, channel, kvOrder, settings);
                }
            }
            catch (Exception ex)
            {
                PushMessageToOrderTrackingEvent(message, ex.Message);
                var orderSn = GetOrderId(message);
                PushLishMQToCreateLog((int)LogType.Warning, message.LogId, "AddOrUpdateRetryOrder", orderSn, null, channel.RetailerId, channel.Id, ex.Message, null);
                await AddOrUpdateRetryOrder(context, channel, new KvOrder
                {
                    Ordersn = orderSn
                });
            }

        }


        private async Task ProgressOrderIfNonExisted(KvInternalContext context, Domain.Model.OmniChannel channel, KvOrder kvOrder, OmniChannelSettingObject settings)
        {
            var coreContext = await CreateKVCoreContext(context);
            try
            {
                using (var db = await _dbConnectionFactory.OpenAsync(context.Group.ConnectionStringName.ToLower()))
                {
                    PushLishMQToCreateLog((int)LogType.Info, context.LogId, "CheckPosSetting", null, null, channel.RetailerId, channel.Id, null, null);
                    var posSetting = await CheckPosSetting(db, context);
                    if (!posSetting.SellAllowOrder)
                    {
                        await RemoveMongoRetryOrder(kvOrder.Ordersn, channel.BranchId, channel.Id, channel.RetailerId);
                        PushLishMQToCreateLog((int)LogType.Warning, context.LogId, "CheckPosSettingSellAllowOrder", kvOrder.ToSafeJson(), null, channel.RetailerId, channel.Id, "Turn off seller allow", null);
                        await TurnOffSalerHandle(coreContext, context, channel.Type);
                        PushMessageToOrderTrackingEvent(Data, "Turn off seller allow");
                        return;
                    }

                    if (kvOrder == null)
                    {
                        PushLishMQToCreateLog((int)LogType.Warning, context.LogId, "ProgressOrderIfNonExisted", null, null, channel.RetailerId, channel.Id, "KVOrderIsNull", null);
                        PushMessageToOrderTrackingEvent(Data, "KVOrderIsNull");
                        return;
                    }
                    if (!FilterStatus(kvOrder, context.LogId, channel))
                    {
                        PushMessageToOrderTrackingEvent(Data, "Order status in ignore list");
                        return;
                    }
                    var saleChannel = await GetSaleChannel(db, context, channel.Id);
                    if (saleChannel == null)
                    {
                        PushMessageToOrderTrackingEvent(Data, "Sale channel is null");
                        PushLishMQToCreateLog((int)LogType.Warning, context.LogId, "ProgressOrderIfNonExisted", null, null, channel.RetailerId, channel.Id, "Sale channel is null !!", null);
                        return;
                    }
                    kvOrder.SaleChannelId = saleChannel.Id;
                    await ProcessLocation(kvOrder);
                    await GetListInvoiceSurchargeAsync(coreContext, context, kvOrder, channel);
                    await CustomerHandle(db, coreContext, context, kvOrder, posSetting, channel);
                    var (isValid, errorMessage, productLog) = await MappingHandle(db, context, kvOrder, channel);
                    if (!isValid)
                    {
                        PushLishKafkaAutoCreateProduct(kvOrder, channel, context, settings);
                        if ((byte)OrderState.Void == kvOrder.Status || (byte)OrderState.Void == kvOrder.NewStatus)
                        {
                            await RemoveOrderSyncError(context, kvOrder, channel);
                            PushLishMQToCreateLog((int)LogType.Warning, context.LogId, "ProgressOrderIfNonExisted", kvOrder.ToSafeJson(), null, channel.RetailerId, channel.Id, "Remove retry order if order cancelled", null);
                        }
                        else
                        {
                            await SaveOrderSyncError(coreContext, context, kvOrder, errorMessage, productLog, channel);
                            PushLishMQToCreateLog((int)LogType.Warning, context.LogId, "ProgressOrderIfNonExisted", kvOrder.ToSafeJson(), null, channel.RetailerId, channel.Id, "Mapping is not valid", null);
                        }
                        await RemoveMongoRetryOrder(kvOrder.Ordersn, channel.BranchId, channel.Id, channel.RetailerId);
                        PushMessageToOrderTrackingEvent(Data, "Mapping is not valid");
                        return;
                    }
                    await PartnerDeliveryHandle(db, coreContext, context, kvOrder, channel);
                    await PriceBookHandle(db, context, kvOrder, channel);
                    await InvoiceWarrantiesHandle(context, kvOrder);
                    await BeforeCreateOrderSuccessHandle(context, kvOrder, channel);

                    var result = await CreateOrderHandle(coreContext, context, kvOrder, channel);
                    if (result == null || result.Id <= 0)
                    {
                        PushLishMQToCreateLog((int)LogType.Warning, context.LogId, "AfterCallSDKCreateOrderFailed", kvOrder.ToSafeJson(), null, context.RetailerId, channel.Id, null, null);
                        PushMessageToOrderTrackingEvent(Data, "CreateOrder failed");
                    }
                    else
                    {
                        await AfterCreateOrderSuccessHandle(coreContext, context, kvOrder, channel, result);
                    }
                }
            }
            catch (Exception ex)
            {
                PushMessageToOrderTrackingEvent(Data, ex.Message);
                PushLishMQToCreateLog((int)LogType.Error, context.LogId, "ProgressOrderIfNonExisted", kvOrder.ToSafeJson(), null, context.RetailerId, channel.Id, null, ex);
                await ExceptionHandle(coreContext, context, channel, kvOrder, ex);
            }
        }
        protected virtual async Task ProgressOrderIfExisted(KvInternalContext context, Domain.Model.OmniChannel channel, KvOrder kvOrder, Domain.Model.Order existedOrder)
        {
            await RemoveErrorOrderInMongo(kvOrder.Ordersn, context.BranchId, channel.Id, kvOrder.Status == (byte)OrderState.Void);
            await RemoveMongoRetryOrder(kvOrder.Ordersn, context.BranchId, channel.Id, context.RetailerId);
            if (kvOrder.Status != (byte)OrderState.Void)
            {
                await CreateDeliveryForOrderIfNotExisted(context, existedOrder, kvOrder);
                await PublishCreateInvoiceQueue(context, channel, kvOrder, existedOrder, false);
                PushLishMQToCreateLog((int)LogType.Info, context.LogId, "ProgressOrderIfExisted", kvOrder.ToSafeJson(), null, context.RetailerId, channel.Id, "PublishCreateInvoiceQueue", null);
            }
            PushMessageToOrderTrackingEvent(Data);
        }

        protected async Task CreateDeliveryForOrderIfNotExisted(KvInternalContext context, Domain.Model.Order existedOrder, KvOrder kvOrder)
        {
            using (var db = await _dbConnectionFactory.OpenAsync(context.Group.ConnectionStringName.ToLower()))
            {
                var partnerRepository = new PartnerDeliveryRepository(db);
                var deliveryInfoRepository = new DeliveryInfoRepository(db);
                var deliveryInfoForOrder = await deliveryInfoRepository.GetLastByOrderIdAsync(context.RetailerId, existedOrder.Id);
                if (deliveryInfoForOrder == null)
                {
                    var coreContext = await CreateKVCoreContext(context);
                    var partnerDelivery = await partnerRepository.GetPartnerDeliveryByCodeAsync(coreContext.RetailerId, kvOrder.OrderDelivery.PartnerDelivery.Code.ToLower());
                    if (partnerDelivery == null)
                    {
                        return;
                    }
                    var deliveryInfo = new DeliveryInfo
                    {
                        OrderId = existedOrder.Id,
                        DeliveryBy = partnerDelivery.Id,
                        UseDefaultPartner = false,
                        ServiceType = Delivery.Normal.ToString(),
                        ServiceTypeText = EnumHelper.ToDescription(Delivery.Normal),
                        Price = kvOrder?.OrderDelivery?.Price,
                        Status = kvOrder?.OrderDelivery?.Status ?? (byte)DeliveryStatus.Pending,
                        CreatedBy = existedOrder.CreatedBy,
                        CreatedDate = existedOrder.CreatedDate,
                        ModifiedBy = existedOrder.ModifiedBy,
                        ModifiedDate = existedOrder.ModifiedDate,
                        RetailerId = existedOrder.RetailerId,
                        FeeJson = kvOrder?.OrderDelivery.FeeJson
                    };

                    // DeliveryPackage
                    var deliveryPackage = new DeliveryPackage
                    {
                        OrderId = existedOrder.Id,
                        Weight = kvOrder.OrderDelivery.Weight,
                        Length = kvOrder.OrderDelivery.Length,
                        Width = kvOrder.OrderDelivery.Width,
                        Height = kvOrder.OrderDelivery.Height,
                        Receiver = kvOrder.OrderDelivery.Receiver,
                        ContactNumber = kvOrder.OrderDelivery.ContactNumber,
                        Address = kvOrder.OrderDelivery.Address,
                        LocationId = kvOrder.OrderDelivery.LocationId,
                        LocationName = kvOrder.OrderDelivery.LocationName,
                        WardName = kvOrder.OrderDelivery.WardName,
                        UsingCod = true,
                    };
                    var deliveryInfoCore = deliveryInfo.ConvertTo<Demo.OmniChannelCore.Api.Sdk.Models.DeliveryInfo>();
                    var deliveryPackageCore = deliveryPackage.ConvertTo<Demo.OmniChannelCore.Api.Sdk.Models.DeliveryPackage>();
                    var result = await _deliveryInfoInternalClient.CreateDeliveryForOrderAsync(coreContext, (int)existedOrder.Id, deliveryInfoCore, deliveryPackageCore);
                    PushLishMQToCreateLog((int)LogType.Info, context.LogId, "ProgressOrderIfNonExisted", kvOrder.ToSafeJson(), null, context.RetailerId, null, $"create delivery for orderid :{existedOrder.Id} and code :{kvOrder.Code} result : {result} ", null);
                }
            }
        }

        private async Task SurchargeHandle(KVCoreContext.KvInternalContext coreContext, KvInternalContext context, Demo.OmniChannelCore.Api.Sdk.Models.Order kvSDKOrder, KvOrder kvOrder, Domain.Model.OmniChannel channel)
        {
            var lstInvoiceSurcharge = await GetListInvoiceSurchargeAsync(coreContext, context, kvOrder, channel);
            if (lstInvoiceSurcharge != null && lstInvoiceSurcharge.Count > 0)
            {
                kvSDKOrder.OrderSurCharges = lstInvoiceSurcharge;
                kvSDKOrder.Surcharge = lstInvoiceSurcharge.Sum(x => x.Price);
            }
        }
        private async Task<(long, bool)> CreatePartnerAsync(IDbConnection db, KVCoreContext.KvInternalContext coreContext, KvInternalContext context, KvOrder kvOrder, Domain.Model.OmniChannel channel)
        {
            var partnerRepository = new PartnerDeliveryRepository(db);
            long deliveryBy = 0;
            bool isRetailerExpireException = false;
            var partnerDelivery = await partnerRepository.SingleAsync(p =>
                           p.RetailerId == context.RetailerId &&
                           p.Code.ToLower() == kvOrder.OrderDelivery.PartnerDelivery.Code.ToLower());

            if (partnerDelivery == null && kvOrder.OrderDelivery?.PartnerDelivery != null)
            {
                var newPartner = kvOrder.OrderDelivery.PartnerDelivery;
                newPartner.RetailerId = context.RetailerId;
                newPartner.IsOmniChannel = true;
                try
                {
                    var partnerId = await _partnerInternalClient.CreatePartner(coreContext, newPartner.ConvertTo<Demo.OmniChannelCore.Api.Sdk.Models.PartnerDelivery>());
                    deliveryBy = partnerId;
                }
                catch (Exception e)
                {
                    if (e is SDKCommon.KvRetailerExpireException)
                    {
                        isRetailerExpireException = true;
                        await DeActiveChannel(channel.Id, coreContext, channel.Type);

                    }
                    else
                    {
                        partnerDelivery = await partnerRepository.SingleAsync(p => p.RetailerId == context.RetailerId && p.Code == kvOrder.OrderDelivery.PartnerDelivery.Code.ToLower());
                        if (partnerDelivery != null)
                        {
                            deliveryBy = partnerDelivery.Id;
                        }
                    }
                }
            }
            else if (partnerDelivery != null)
            {
                if (partnerDelivery.IsOmniChannel == null || partnerDelivery.IsOmniChannel == false)
                {
                    partnerDelivery.IsOmniChannel = true;
                    await _partnerInternalClient.UpdatePartnerOmniChannel(coreContext, partnerDelivery.ConvertTo<Demo.OmniChannelCore.Api.Sdk.Models.PartnerDelivery>());
                }
                deliveryBy = partnerDelivery.Id;
            }

            return (deliveryBy, isRetailerExpireException);
        }
        private async Task<Domain.Model.Customer> _GetCustomerAsync(CustomerRepository customerRepository, int retailerId, int branchId, string phone, string customerCode, bool isManagerCustomerByBranch)
        {
            return await customerRepository.GetAsync(retailerId, branchId, phone, customerCode, isManagerCustomerByBranch);
        }
        private async Task DeActiveChannel(long channelId, KVCoreContext.KvInternalContext kvInternalContext, byte channelType)
        {
            await _integrationInternalClient.DeactivateChannel(
                kvInternalContext.ConvertTo<Sdk.Common.KvInternalContext>(), channelId, EnumHelper.ToDescription((ChannelType)channelType));
        }
        private async Task HandleAuditLogAfterSuccess(KVCoreContext.KvInternalContext coreContext, KvInternalContext context, KvOrder kvOrder, byte type)
        {
            var productDetail = new StringBuilder();
            double total = 0;
            if (kvOrder.OrderDetails != null && kvOrder.OrderDetails.Any())
            {
                productDetail.Append(", bao gồm:<div>");
                foreach (var item in kvOrder.OrderDetails)
                {
                    total += ((double)item.Price - (double)item.Discount.GetValueOrDefault()) * item.Quantity;
                    productDetail.Append(
                        $"- [ProductCode]{item.ProductCode}[/ProductCode] :" +
                        $" {StringHelper.Normallize(item.Quantity)}*" +
                        $"{StringHelper.NormallizeWfp((double)item.Price - (double)item.Discount.GetValueOrDefault())}<br>");

                    var productWarrantys = kvOrder.InvoiceWarranties?.Where(x => x.InvoiceDetailUuid == item.Uuid).ToList();
                    var getWarrantys = GetWarrantysLog(item, productWarrantys);
                    productDetail.Append(string.IsNullOrEmpty(getWarrantys.ToString()) ? string.Empty : $"{getWarrantys}<br>");
                }
                productDetail.Append("</div>");
            }

            var channelName = EnumHelper.GetNameByType<ChannelType>(type);
            var log = new Audit.Model.Message.AuditTrailLog
            {
                FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(type),
                Action = (int)AuditTrailAction.OrderIntergate,
                BranchId = context.BranchId,
                CreatedDate = DateTime.Now,
                Content =
                    $"Tạo đơn đặt hàng: [OrderCode]{kvOrder.Code}[/OrderCode] từ đơn đặt hàng {kvOrder.Ordersn} trên {channelName}, giá trị: {StringHelper.NormallizeWfp(total)}, thời gian: {kvOrder.PurchaseDate:dd/MM/yyyy HH:mm:ss}{productDetail}"
            };
            await _auditTrailInternalClient.AddLogAsync(coreContext, log);
        }
        public async Task VoidKvOrder(KVCoreContext.KvInternalContext coreContext, KvInternalContext context, KvOrder kvOrder, long orderId, byte type)
        {
            await _orderInternalClient.VoidOrder(coreContext, orderId);
            #region Logs

            var log = new Audit.Model.Message.AuditTrailLog
            {
                FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(type),
                Action = (int)AuditTrailAction.OrderIntergate,
                CreatedDate = DateTime.Now,
                BranchId = context.BranchId,
                Content = $"Hủy đơn đặt hàng: [OrderCode]{kvOrder.Code}[/OrderCode]"
            };
            await _auditTrailInternalClient.AddLogAsync(coreContext, log);
            #endregion
        }
        private async Task AddOrUpdateRetryOrder(KvInternalContext context, Domain.Model.OmniChannel channel, KvOrder kvOrder)
        {
            var existRetryOrder = await _retryOrderMongoService.GetByOrderId(kvOrder.Ordersn, channel.Id, context.BranchId, context.RetailerId);
            if (existRetryOrder != null)
            {
                existRetryOrder.ModifiedDate = DateTime.Now;
                existRetryOrder.RetryCount += 1;
                await _retryOrderMongoService.UpdateAsync(existRetryOrder.Id, existRetryOrder);
            }
            else
            {
                await _retryOrderMongoService.AddSync(new MongoDb.RetryOrder
                {
                    ChannelId = channel.Id,
                    ChannelType = channel.Type,
                    BranchId = context.BranchId,
                    KvEntities = context.Group.ConnectionString,
                    RetailerId = context.RetailerId,
                    OrderId = kvOrder.Ordersn,
                    Order = kvOrder.ToSafeJson(),
                    GroupId = context.Group.Id,
                    CreatedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    RetryCount = 1,
                    PlatformId = channel.PlatformId
                });
            }
        }
        private async Task BusinessChannelError(KVCoreContext.KvInternalContext coreContext, KvInternalContext context, KvOrder kvOrder, Domain.Model.OmniChannel channel, Exception e)
        {
            if (string.IsNullOrEmpty(e.Message) ||
                               (!e.Message.Trim()
                                    .Equals(
                                        string.Format(KvOrderValidateMessage.OrderExistedVi, $"{kvOrder.Code}")
                                            .Trim(), StringComparison.OrdinalIgnoreCase)
                                && !e.Message.Trim()
                                    .Equals(
                                        string.Format(KvOrderValidateMessage.OrderExistedEn, $"{kvOrder.Code}")
                                            .Trim(), StringComparison.OrdinalIgnoreCase)))
            {
                await RemoveMongoRetryOrder(kvOrder.Ordersn, channel.BranchId, channel.Id, channel.RetailerId);
                await SaveOrderSyncError(coreContext, context, kvOrder, e.Message, e.Message, channel);
            }
        }
        private async Task WriteLogCreateOrderFail(KVCoreContext.KvInternalContext coreContext, KvInternalContext context, KvOrder kvOrder, Domain.Model.OmniChannel channel, string productLog)
        {
            var log = new Audit.Model.Message.AuditTrailLog
            {
                FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channel.Type),
                Action = (int)AuditTrailAction.OrderIntergate,
                CreatedDate = DateTime.Now,
                BranchId = context.BranchId,
                Content = $"Tạo đơn đặt hàng KHÔNG thành công: [OrderCode]{kvOrder.Code}[/OrderCode] (cho đơn đặt hàng: {kvOrder.Ordersn}), lý do: <br/>{productLog}"
            };
            try
            {
                await _auditTrailInternalClient.AddLogAsync(coreContext, log);
            }
            catch (Exception e)
            {
                if (e is SDKCommon.KvRetailerExpireException)
                {
                    await DeActiveChannel(channel.Id, coreContext, channel.Type);
                }
            }
        }
        private async Task<bool> CheckSurchargeActiveWithBranch(OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext, Demo.OmniChannelCore.Api.Sdk.Models.SurCharge surcharge, int branchId)
        {
            if (surcharge.ForAllBranch) return true;
            var surchargeByBranch =
                await _surChargeBranchInternalClient.GetSurChargeBranchById(coreContext, surcharge.Id);
            var isApproveSurcharge = surchargeByBranch?.Any(b => b.BranchId == branchId) ?? false;
            return isApproveSurcharge;
        }
        private (bool, string, string) ValidateProductHandle(Domain.Model.OmniChannel channel, KvOrder kvOrder, List<Domain.Model.ProductBranchDTO> kvProducts, List<Demo.OmniChannel.MongoDb.ProductMapping> mappings)
        {
            var productLog = new StringBuilder();
            var errorMessage = new StringBuilder();
            var isValid = true;
            foreach (var detail in kvOrder.OrderDetails)
            {
                var product = kvProducts.FirstOrDefault(x => x.Id == detail.ProductId);
                detail.ProductName = product?.FullName ?? detail.ProductChannelName;
                var getTrackKey = ConvertHelper.GetUseParrentItemId(channel.Type, detail.ParentChannelProductId, detail.ProductChannelId);
                var channelTrackKey = !string.IsNullOrEmpty(getTrackKey)
                    ? getTrackKey
                    : detail.ProductChannelSku;
                if (product == null || mappings == null)
                {
                    errorMessage.Append(
                        $"Hàng hóa {detail.ProductChannelName} ({channelTrackKey}) chưa liên kết với hàng hóa nào trên Demo; ");
                    productLog.AppendFormat(
                        $"- {detail.ProductChannelSku ?? getTrackKey} " +
                        $"({channelTrackKey}): chưa xác định trên Demo <br/>");
                    isValid = false;
                }
                else
                {
                    var mapping = mappings.FirstOrDefault(x =>
                        x.ChannelId == channel.Id &&
                        ((!string.IsNullOrEmpty(detail.ProductChannelId) &&
                          x.CommonProductChannelId == detail.ProductChannelId) ||
                         x.ProductChannelSku == detail.ProductChannelSku));
                    if (mapping == null)
                    {
                        errorMessage.Append(
                            $"Hàng hóa {detail.ProductChannelName} ({channelTrackKey}) chưa liên kết với hàng hóa nào trên Demo; ");
                        productLog.AppendFormat(
                            $"- [ProductCode]{detail.ProductCode}[/ProductCode]: liên kết hàng hóa không tồn tại <br/>");
                        isValid = false;
                    }
                    else if (product.isDeleted == true)
                    {
                        errorMessage.Append(
                            $"Hàng hóa {detail.ProductChannelName} ({channelTrackKey}) chưa liên kết với hàng hóa nào trên Demo; ");
                        productLog.AppendFormat($"- [ProductCode]{detail.ProductCode}[/ProductCode]: đã bị xóa <br/>");
                        isValid = false;
                    }
                }
            }

            if (!isValid)
            {
                return (false, errorMessage.ToString(), productLog.ToString());
            }
            return (true, "", "");
        }

        private async Task<Domain.Model.KvRetailer> GetRetailer(long retailerId)
        {
            var kvRetailer = _cacheClient.Get<Domain.Model.KvRetailer>(string.Format(KvConstant.RetailerIdCacheKey, retailerId));
            if (kvRetailer == null)
            {
                using (var dbMaster = await _dbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities))
                {
                    kvRetailer = await dbMaster.SingleAsync<Domain.Model.KvRetailer>(x => x.Id == retailerId);
                    if (kvRetailer != null)
                    {
                        _cacheClient.Set(string.Format(KvConstant.RetailerIdCacheKey, retailerId), kvRetailer, DateTime.Now.AddDays(7));
                    }
                }
            }
            return kvRetailer;
        }
        private async Task<KvGroup> GetRetailerGroup(int groupId)
        {
            var kvGroup = _cacheClient.Get<KvGroup>(string.Format(KvConstant.KvGroupCacheKey, groupId));
            if (kvGroup == null)
            {
                using (var dbMaster = await _dbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities))
                {
                    kvGroup = await dbMaster.SingleAsync<Domain.Common.KvGroup>(x => x.Id == groupId);
                    if (kvGroup != null)
                    {
                        _cacheClient.Set(string.Format(KvConstant.KvGroupCacheKey, groupId), kvGroup, DateTime.Now.AddDays(7));
                    }
                }
            }
            return kvGroup;
        }
        private async Task<string> GetSendoLocation(int districtId)
        {
            if (districtId > 0)
            {
                var location = _cacheClient.Get<MongoDb.SendoLocation>(KvConstant.CacheSendoLocation.Fmt(districtId));
                if (location == null)
                {
                    location = (await _sendoLocationMongoService.GetByDistrictId(districtId));
                    if (location != null)
                    {
                        _cacheClient.Set(KvConstant.CacheSendoLocation.Fmt(districtId), location, TimeSpan.FromDays(30));
                    }
                }
                return location?.FullName;
            }
            return null;
        }
        private void GetKvLocationWard(KvOrder order)
        {
            if (ValidateKvOrder(order))
            {
                Domain.Model.Location kvLocation = null;
                if (!string.IsNullOrEmpty(order.OrderDelivery.ChannelState) &&
                    !string.IsNullOrEmpty(order.OrderDelivery.ChannelCity))
                {
                    var state = Regex.Replace(StringHelper.ConvertToUnsign(order.OrderDelivery.ChannelState), @"\s+", "");
                    state = !string.IsNullOrEmpty(state) && state.StartsWith("TP.")
                        ? Regex.Replace(state, "TP.", "")
                        : state;
                    var city = Regex.Replace(StringHelper.ConvertToUnsign(order.OrderDelivery.ChannelCity), @"\s+", "");
                    kvLocation = _cacheClient.Get<Domain.Model.Location>(string.Format(KvConstant.LocationCacheKey, $"{state}-{city}"));
                    order.OrderDelivery.LocationId = kvLocation?.Id;
                    if (kvLocation != null)
                    {
                        order.OrderDelivery.LocationName = kvLocation.Name;
                    }
                }
                else if (!string.IsNullOrEmpty(order.OrderDelivery.LocationName))
                {
                    var locationKey = Regex.Replace(StringHelper.ConvertToUnsign(order.OrderDelivery.LocationName), @"\s+",
                        "");
                    kvLocation = _cacheClient.Get<Domain.Model.Location>(string.Format(KvConstant.LocationCacheKey, locationKey));
                    order.OrderDelivery.LocationId = kvLocation?.Id;
                }

                if (kvLocation != null && (!string.IsNullOrEmpty(order.OrderDelivery?.ChannelWard) && kvLocation.Id > 0))
                {
                    var ward = Regex.Replace(StringHelper.ConvertToUnsign(order?.OrderDelivery.ChannelWard), @"\s+", "");
                    var kvWard = _cacheClient.Get<Domain.Model.Wards>(string.Format(KvConstant.WardCacheKey, kvLocation?.Id, ward));
                    order.OrderDelivery.WardName = kvWard != null ? kvWard.Name : order.OrderDelivery.ChannelWard;
                }
                else
                {
                    order.OrderDelivery.WardName = order.OrderDelivery.ChannelWard;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="kvOrder"></param>
        /// <param name="errorMessage"></param>
        /// <param name="productLog"></param>
        /// <param name="channel"></param>
        /// <returns></returns>
        protected async Task RemoveOrderSyncError(KvInternalContext context, KvOrder kvOrder, Domain.Model.OmniChannel channel)
        {
            var existOrder = await _orderMongoService.GetByOrderId(kvOrder.Ordersn, context.BranchId, channel.Id);
            if (existOrder != null)
            {
                await _orderMongoService.RemoveAsync(existOrder.Id);
            }
        }

        private bool ValidateKvOrder(KvOrder order)
        {
            if (order == null)
            {
                return false;
            }
            return true;
        }

        private async Task WriteLogErrNotCreateCustomer(KVCoreContext.KvInternalContext coreContext, KvOrder order, List<Domain.Model.ProductBranchDTO> kvProducts,
            KvInternalContext context, Domain.Model.OmniChannel channel)
        {
            foreach (var detail in order.OrderDetails)
            {
                detail.ProductName = kvProducts.FirstOrDefault(x => x.Id == detail.ProductId)?.FullName;
            }

            var errorMessage = "Đồng bộ thông tin khách hàng thất bại.";
            var customerLog = $"- [CustomerCode]: Đồng bộ thông tin khách hàng thất bại <br/>";
            await SaveOrderSyncError(coreContext, context, order, errorMessage,
                customerLog, channel);
        }

        private StringBuilder GetWarrantysLog(KvOrderDetail orderDetail,
            List<InvoiceWarranties> invoiceWarrantys)
        {
            var warrantysLog = new StringBuilder();

            var checkProductIsCombo = !invoiceWarrantys.Where(x => x.ProductId == orderDetail.ProductId).Any();
            var productWarrantysGroup = invoiceWarrantys.GroupBy(x => new { x.ProductId, x.ProductCode }).ToList();
            foreach (var group in productWarrantysGroup)
            {
                var guarrantee = new StringBuilder();
                var mainGuarrantee = string.Empty;
                var productWarrantys = invoiceWarrantys.Where(x => x.ProductId == group.Key.ProductId).ToList();

                productWarrantys?.ForEach(w =>
                {
                    if (w.WarrantyType == (int)WarrantyType.Maintenance)
                    {
                        mainGuarrantee = string.Format("Định kỳ bảo trì: {0} {1} ({2}) đến {3}",
                            w.NumberTime, SystemHelper.GetWarrantyTimeTypeText(w.TimeType).ToLower(),
                            w.Description, ((DateTime)w.ExpireDate).ToString("dd/MM/yyyy"));
                    }
                    else
                    {
                        var guarranteelog = string.Format("{0} {1} ({2}) đến {3}",
                           w.NumberTime, SystemHelper.GetWarrantyTimeTypeText(w.TimeType).ToLower(),
                           w.Description, ((DateTime)w.ExpireDate).ToString("dd/MM/yyyy"));

                        guarrantee.Append(string.IsNullOrEmpty(guarrantee.ToString())
                            ? $"Bảo hành: {guarranteelog}"
                            : $", {guarranteelog}");
                    }
                });
                if (checkProductIsCombo)
                {
                    warrantysLog.Append($"Thành phần bảo hành: [ProductCode]{group.Key.ProductCode}[/ProductCode]<br />");
                }
                warrantysLog.Append(string.IsNullOrEmpty(guarrantee.ToString()) ? string.Empty : $"&nbsp;&nbsp;{guarrantee}<br>");
                warrantysLog.Append(string.IsNullOrEmpty(mainGuarrantee) ? string.Empty : $"&nbsp;&nbsp;{mainGuarrantee}<br>");
            }

            return warrantysLog;
        }
        #endregion
    }
}

﻿using Demo.Audit.Model.Message;
using Demo.OmniChannel.Business.Dto;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.ShareKernel.Models;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Common;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Demo.OmniChannelCore.Api.Sdk.Models;
using Demo.OmniChannelCore.Api.Sdk.RequestDtos;
using Newtonsoft.Json;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProductRequest = Demo.OmniChannelCore.Api.Sdk.RequestDtos.ProductRequest;

namespace Demo.OmniChannel.Business.Implements
{
    public class AutoCreateProductBusinessService : IAutoCreateProductBusiness
    {
        private readonly IProductInternalClient _productInternalClient;
        private readonly ICategoryInternalClient _categoryInternalClient;
        private readonly IProductMappingService _productMappingService;
        private readonly IProductMongoService _productMongoService;
        private readonly IOmniChannelPlatformService _platformService;
        private readonly IOmniChannelAuthService _channelAuthService;
        private readonly IAttributeInternalClient _attributeInternalClient;
        private readonly IProductImageInternalClient _productImageInternalClient;
        private readonly IProductAttributeInternalClient _productAttributeInternalClient;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly ICacheClient _cacheClient;
        private readonly IChannelBusiness _channelBusiness;
        private readonly IAppSettings _appSettings;
        private const string prefixProductCode = "SPO";
        private readonly IAuditTrailInternalClient _auditTrailInternalClient;

        public AutoCreateProductBusinessService(
            IProductInternalClient productInternalClient,
            ICategoryInternalClient categoryInternalClient,
            IProductMappingService productMappingService,
            IProductMongoService productMongoService,
            IChannelBusiness channelBusiness,
            IOmniChannelPlatformService platformService,
            IOmniChannelAuthService channelAuthService,
            IAttributeInternalClient attributeInternalClient,
            IProductAttributeInternalClient productAttributeInternalClient,
            IProductImageInternalClient productImageInternalClient,
            IAuditTrailInternalClient auditTrailInternalClient,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IAppSettings appSettings)
        {
            _productInternalClient = productInternalClient;
            _categoryInternalClient = categoryInternalClient;
            _productMappingService = productMappingService;
            _productMongoService = productMongoService;
            _channelBusiness = channelBusiness;
            _platformService = platformService;
            _channelAuthService = channelAuthService;
            _attributeInternalClient = attributeInternalClient;
            _productAttributeInternalClient = productAttributeInternalClient;
            _productImageInternalClient = productImageInternalClient;
            _cacheClient = cacheClient;
            _dbConnectionFactory = dbConnectionFactory;
            _appSettings = appSettings;
            _auditTrailInternalClient = auditTrailInternalClient;
        }

        /// <summary>
        /// Tạo sản phẩm ở KV
        /// </summary>
        /// <param name="data"></param> 
        /// <returns></returns>
        public async Task<AutoCreateProductResponse> CreateProductToKv(SyncOrderWithAutoCreateProduct param)
        {
            var kvOrderObject = JsonConvert.DeserializeObject<KvOrder>(param.KvOrder);

            var connectStringName = param.KvEntities.Substring(param.KvEntities.IndexOf('=') + 1);
            var context = await ContextHelper.GetExecutionContext(_cacheClient, _dbConnectionFactory, param.RetailerId,
                connectStringName, param.BranchId, _appSettings?.Get<int>("ExecutionContext"));

            var kvContext = new KvInternalContext(param.LogId, null, context.RetailerCode, context.BranchId,
                context.RetailerId, context.User?.Id ?? 0, context.GroupId);
            var categoryId = await GetCategoryId(kvContext, CategoryName.Orther);

            var channel = await _channelBusiness.GetChannel(param.ChannelId, param.RetailerId, param.LogId);
            if (channel == null) throw new OmniException(ErrorCode.ErrorBussiness, $"Channel {param.ChannelId} is null");

            var productKv = await GetProductKv(connectStringName, param.RetailerId, param.BranchId,
                new HashSet<string>(kvOrderObject.OrderDetails.Select(x => x.ProductChannelSku).ToList()));
            var posSetting = await PosSettings(connectStringName, param.RetailerId);

            return await CreateProductToKv(kvOrderObject.OrderDetails, kvContext, productKv, channel, categoryId, posSetting?.RewardPoint);
        }

        #region Private method

        private async Task<AutoCreateProductResponse> CreateProductToKv(List<KvOrderDetail> orderDetails, KvInternalContext context,
            List<Domain.Model.Product> productKv, Domain.Model.OmniChannel channel, long categoryId, bool? isRewardPoint)
        {
            var productsNoMapping = new List<MongoDb.Product>();
            var productsNoMappingVariation = new List<MongoDb.Product>();
            var listModels = new List<ShopeeModelListReponseV2>();
            var listItemBaseInfo = new List<ItemListBase>();
            var response = new AutoCreateProductResponse();

            //get hàng hóa normal và mapping
            var orderDetailNormals = orderDetails.Where(x => x.VariationId == 0).ToList();
            if (orderDetailNormals.Any())
            {
                productsNoMapping = await GetProductsNoMapping(context.RetailerId, new List<long> { channel.Id },
                    orderDetailNormals.Select(x => x.ItemId.ToString()).ToList());
                listItemBaseInfo = await GetItemListBases(productsNoMapping.Select(x => x.ItemId ?? 0).ToList(), channel, context);
            }

            //get hàng hóa variation và mapping
            var orderDetailVariations = orderDetails.Where(x => x.VariationId != 0).ToList();
            if (orderDetailVariations.Any())
            {
                productsNoMappingVariation = await GetProductsNoMappingVariation(context.RetailerId, new List<long> { channel.Id },
                    orderDetailVariations.Select(x => x.ParentChannelProductId.ToString()).ToList(),
                    orderDetailVariations.Select(x => x.VariationId.ToString()).ToList());

                var parentItemIds = productsNoMappingVariation.Where(x => x.ParentItemId != null).Select(x => x.ParentItemId ?? 0)
                .Distinct().ToList();
                listModels = await GetModelList(parentItemIds, channel, context);
            }

            if (productsNoMapping.Any())
            {
                var responseCreateProduct = await CreateProductMappingNormal(productsNoMapping, orderDetailNormals, context, productKv,
                    channel, listItemBaseInfo, categoryId, isRewardPoint);

                response.KvProductIds.AddRange(responseCreateProduct.KvProductIds);
                response.IsSuccess = responseCreateProduct.IsSuccess;
            }

            if (productsNoMappingVariation.Any())
            {
                var createProductVariations = await CreateProductMappingVariations(productsNoMappingVariation, orderDetailVariations, context,
                    productKv, channel, categoryId, listModels, isRewardPoint);

                response.KvProductIds.AddRange(createProductVariations.KvProductIds);
                if (createProductVariations.IsSuccess)
                {
                    response.IsSuccess = createProductVariations.IsSuccess;
                }
            }

            return response;
        }

        private async Task<List<MongoDb.Product>> GetProductsNoMapping(long retailerId, List<long> channelIds, List<string> itemIds)
        {
            var products = await _productMongoService.Get(retailerId, channelIds, itemIds, isConnected: false,
                ConvertHelper.CheckUseStringItemId((byte)ChannelType.Shopee));
            if (!products.Any())
                throw new OmniException(ErrorCode.ErrorBussiness, $"Channel {channelIds}: Không tìm thấy sản phẩm cần mapping");

            var productsNoName = products.Where(x => string.IsNullOrEmpty(x.ItemName)).ToList();
            if (productsNoName.Any())
            {
                throw new OmniException(ErrorCode.ErrorBussiness, $"item: {productsNoName.Select(x => x.ItemId).Join("-")} không có tên sản phẩm");
            }
            return products;
        }

        private async Task<List<MongoDb.Product>> GetProductsNoMappingVariation(long retailerId, List<long> channelIds,
            List<string> parentIds, List<string> itemIds)
        {
            var products = await _productMongoService.GetProductByParentIds(retailerId, channelIds,
                parentIds, false);
            if (!products.Any()) throw new OmniException(ErrorCode.ErrorBussiness, $"Channel {channelIds}: Không tìm thấy sản phẩm cần mapping");
            var productsNoName = products.Where(x => string.IsNullOrEmpty(x.ItemName)).ToList();
            if (productsNoName.Any())
            {
                throw new OmniException(ErrorCode.ErrorBussiness, $"item: {productsNoName.Select(x => x.ItemId).Join("-")} không có tên sản phẩm");
            }
            var productSkuNull = products.Where(x => string.IsNullOrEmpty(x.ItemSku)).ToList() ?? new List<MongoDb.Product>();

            var productGroups = products.Where(p => !string.IsNullOrEmpty(p.ItemSku)).GroupBy(p => new { p.ParentItemId, p.ItemSku }).Select(p => new MongoDb.Product
            {
                ParentItemId = p.Key.ParentItemId,
                ItemSku = p.Key.ItemSku,
                ItemId = p.Where(x => itemIds.Contains(x.CommonItemId))?.FirstOrDefault()?.ItemId ?? p.First().ItemId,
                ItemName = p.Where(x => itemIds.Contains(x.CommonItemId))?.FirstOrDefault()?.ItemName ?? p.First().ItemName,
                ItemImages = p.Where(x => itemIds.Contains(x.CommonItemId))?.FirstOrDefault()?.ItemImages ?? p.First().ItemImages,
            }).ToList();
            return productGroups.Concat(productSkuNull).ToList();
        }

        private async Task<CreateProductNormalReponse> CreateProductMappingNormal(
            List<MongoDb.Product> productsNoMapping, List<KvOrderDetail> orderDetails,
            KvInternalContext context, List<Domain.Model.Product> productKv,
            Domain.Model.OmniChannel channel, List<ItemListBase> productBaseInfos, long categoryId, bool? isRewardPoint)
        {
            var newProductMapping = new List<MongoDb.ProductMapping>();
            var productImages = new List<ProductImage>();
            var response = new CreateProductNormalReponse();

            foreach (var product in productsNoMapping)
            {
                var productOnShopee = productBaseInfos?.FirstOrDefault(x => x.ItemId == product.ItemId);
                var productKvModel = new OmniChannelCore.Api.Sdk.RequestDtos.ProductRequest
                {
                    Code = string.IsNullOrEmpty(productOnShopee.ItemSku) ? StringHelper.SubStringWithLength($"{prefixProductCode}{product.ItemId}", 40) : productOnShopee.ItemSku,
                    MasterCode = $"{prefixProductCode}{product.ParentItemId}",
                    Name = product.ItemName,
                    CategoryId = categoryId,
                    ProductType = (byte)ProductType.Purchased,
                    FullName = product.ItemName,
                    AllowsSale = true,
                    IsRelateToOmniChannel = true,
                    isActive = true,
                    BasePrice = (decimal)(productOnShopee?.PriceInfo?.FirstOrDefault()?.CurrentPrice ?? 0),
                    OnHand = (productOnShopee?.StockInfoV2?.SellerStocks?.FirstOrDefault()?.Stock ?? 0) +
                    orderDetails.Where(x => x.ItemId == product.ItemId)?.Sum(x => x.Quantity) ?? 0,
                    BranchId = channel.BranchId,
                    IsRewardPoint = isRewardPoint,
                };
                var productImage = product.ItemImages?.Select(im => new ProductCreateImageRequest { Url = im }).Take(5).ToList()
                   ?? new List<ProductCreateImageRequest>();

                //Kiểm tra nếu tự động mapping sẽ lấy sản phẩm ở KV
                var productExistCodeKv = productKv.FirstOrDefault(x => x.Code.ToLower() == product.ItemSku.ToLower());
                var productKvResp = channel.IsAutoMappingProduct && productExistCodeKv != null ?
                    productExistCodeKv.ConvertTo<Product>() : await _productInternalClient.CreatedProduct(context, productKvModel, productImage);
                var map = new MongoDb.ProductMapping(context.RetailerId, channel.Id, productKvResp.Id, productKvResp.Code,
                 productKvResp.FullName, product.ItemSku, product.ItemName, (byte)ChannelProductType.Normal, product.ItemId.ToString(), product.ParentItemId.ToString());
                newProductMapping.Add(map);

                await WriteLogAutoCreateProduct(context, productKvModel);
            }

            if (newProductMapping.Any())
            {
                await _productMappingService.AddOrUpdate(context.RetailerId, newProductMapping);
                await _productMongoService.UpdateProductsConnect(productsNoMapping.Select(x => x.Id).ToList(), true);
                await _productInternalClient.UpdateIsRelateToOmniChannel(context, newProductMapping?.Select(pm => pm.ProductKvId).ToList(), true);
                var mappingKeyList = newProductMapping.Select(x => string.Format(KvConstant.ListProductMappingCacheKey,
                    x.RetailerId, x.ChannelId, x.ProductKvId)).Distinct().ToList();
                _cacheClient.RemoveAll(mappingKeyList);

                response.KvProductIds = newProductMapping.Select(x => x.ProductKvId).ToList();
                response.IsSuccess = true;

                foreach (var productMapping in newProductMapping)
                {
                    await WriteLogAutoMappingProduct(context, productMapping, channel.Name);
                }
            }
            return response;
        }

        private async Task<CreateProductVariationsReponse> CreateProductMappingVariations(
            List<MongoDb.Product> productsNoMapping, List<KvOrderDetail> orderDetails,
            KvInternalContext context, List<Domain.Model.Product> productKv,
            Domain.Model.OmniChannel channel, long categoryId,
            List<ShopeeModelListReponseV2> listModels, bool? isRewardPoint)
        {
            var newProductMapping = new List<MongoDb.ProductMapping>();
            var productKvResult = new List<Product>();
            var response = new CreateProductVariationsReponse();

            //Nhóm các sản phẩm cùng một loại với nhau
            var parentItemIds = productsNoMapping.Where(x => x.ParentItemId != null).Select(x => x.ParentItemId ?? 0)
                .Distinct().ToList();

            //Tạo sản phẩm thuộc tính
            foreach (var parentItemId in parentItemIds)
            {
                //Lấy danh sách thuộc tính
                var productFilterWithGroup = productsNoMapping.Where(x => x.ParentItemId == parentItemId).ToList();
                //Tạo mặc định variation đầu tiên làm cha
                var firstParrent = productFilterWithGroup.FirstOrDefault();
                //Lấy thông tin sản phẩm trên sàn
                var firstParrentOnShopee = listModels?.FirstOrDefault(x => x.ItemId == parentItemId)
                    .Response?.Model?.FirstOrDefault(x => x.ModelId == firstParrent.ItemId);

                var firstParentModel = new ProductRequest
                {
                    Code = string.IsNullOrEmpty(firstParrentOnShopee.ModelSku) ? StringHelper.SubStringWithLength($"{prefixProductCode}{firstParrent.ItemId}", 40) : firstParrentOnShopee.ModelSku,
                    MasterCode = $"{prefixProductCode}{firstParrent.ParentItemId}",
                    Name = firstParrent.ItemName.Split('-')[0].TrimEnd(),
                    CategoryId = categoryId,
                    ProductType = (byte)ProductType.Purchased,
                    FullName = firstParrent.ItemName,
                    HasVariants = true,
                    AllowsSale = true,
                    IsRelateToOmniChannel = true,
                    isActive = true,
                    BasePrice = (decimal)(firstParrentOnShopee?.PriceInfo?.FirstOrDefault()?.CurrentPrice ?? 0),
                    OnHand = (firstParrentOnShopee?.StockInfoV2?.SellerStocks?.FirstOrDefault()?.Stock ?? 0)
                    + orderDetails.Where(x => x.VariationId == firstParrent.ItemId)?.Sum(x => x.Quantity) ?? 0,
                    BranchId = channel.BranchId,
                    IsRewardPoint = isRewardPoint
                };
                //Kiểm tra nếu tự động mapping sẽ lấy sản phẩm ở KV
                var firstProductExistCodeKv = productKv.FirstOrDefault(x => x.Code.ToLower().Equals(firstParrent.ItemSku.ToLower()));
                var firstProduct = channel.IsAutoMappingProduct && firstProductExistCodeKv != null ? firstProductExistCodeKv.ConvertTo<Product>() :
                    await _productInternalClient.CreatedProduct(context, firstParentModel, null);
                if (!channel.IsAutoMappingProduct || firstProductExistCodeKv == null)
                {
                    await WriteLogAutoCreateProduct(context, firstParentModel);
                }
                var mappingFirstParrent = new MongoDb.ProductMapping(context.RetailerId, channel.Id, firstProduct.Id, firstProduct.Code,
                   firstProduct.FullName, firstParrent.ItemSku, firstParrent.ItemName, (byte)ChannelProductType.Variation,
                   firstParrent.ItemId.ToString(), firstParrent.ParentItemId.ToString());
                newProductMapping.Add(mappingFirstParrent);
                productKvResult.Add(firstProduct);

                //Tạo tiếp các variation tiếp theo
                var parentChild = productFilterWithGroup.Where(x => x.ItemId != firstParrent.ItemId).ToList();
                foreach (var productChild in parentChild)
                {
                    //Lấy thông tin sản phẩm trên sàn
                    var productChildOnShopee = listModels.FirstOrDefault(x => x.ItemId == parentItemId)
                        .Response.Model.FirstOrDefault(x => x.ModelId == productChild.ItemId);

                    var productKvModel = new ProductRequest
                    {
                        Code = string.IsNullOrEmpty(productChildOnShopee.ModelSku) ? StringHelper.SubStringWithLength($"{prefixProductCode}{productChild.ItemId}", 40) : productChildOnShopee.ModelSku,
                        MasterCode = $"{prefixProductCode}{productChild.ParentItemId}",
                        Name = productChild.ItemName.Split('-')[0].TrimEnd(),
                        CategoryId = categoryId,
                        ProductType = (byte)ProductType.Purchased,
                        FullName = productChild.ItemName,
                        MasterProductId = firstProduct.Id,
                        AllowsSale = true,
                        HasVariants = true,
                        IsRelateToOmniChannel = true,
                        isActive = true,
                        BasePrice = (decimal)(productChildOnShopee?.PriceInfo?.FirstOrDefault()?.CurrentPrice ?? 0),
                        OnHand = (productChildOnShopee?.StockInfoV2?.SellerStocks?.FirstOrDefault()?.Stock ?? 0)
                        + orderDetails.Where(x => x.VariationId == productChild.ItemId)?.Sum(x => x.Quantity) ?? 0,
                        BranchId = channel.BranchId
                    };
                    var productChildExistCodeKv = productKv.FirstOrDefault(x => x.Code.ToLower() == productChild.ItemSku.ToLower());
                    var productKvResp = channel.IsAutoMappingProduct && productChildExistCodeKv != null ? productChildExistCodeKv.ConvertTo<Product>() :
                        await _productInternalClient.CreatedProduct(context, productKvModel, null);
                    var mapParrentChild = new MongoDb.ProductMapping(context.RetailerId, channel.Id, productKvResp.Id,
                        productKvResp.Code, productKvResp.FullName, productChild.ItemSku, productChild.ItemName,
                        (byte)ChannelProductType.Variation, productChild.ItemId.ToString(), productChild.ParentItemId.ToString());
                    newProductMapping.Add(mapParrentChild);
                    productKvResult.Add(productKvResp);

                    await WriteLogAutoCreateProduct(context, productKvModel);
                }
            }

            //Không tạo thuộc tính liên quan đến các sản phẩm check trùng ở Kv
            var newProductMappingFilter = newProductMapping.Where(x => productKv.All(y => y.Id != x.ProductKvId)).ToList();

            //Lấy danh sách liên quan đến sản phẩm variation
            var (productAndAttributes, productImages) = await GetProductRelateVariaton(context, productKvResult, newProductMappingFilter, listModels);

            if (newProductMapping.Any())
            {
                await _productMappingService.AddOrUpdate(context.RetailerId, newProductMapping);
                await _productMongoService.UpdateProductsConnect(productsNoMapping.Select(x => x.Id).ToList(), true);
                await _productInternalClient.UpdateIsRelateToOmniChannel(context, newProductMapping.Select(pm => pm.ProductKvId).ToList(), true);
                var mappingKeyList = newProductMapping.Select(x => string.Format(KvConstant.ListProductMappingCacheKey,
                    x.RetailerId, x.ChannelId, x.ProductKvId)).Distinct().ToList();

                if (productAndAttributes.Any())
                {
                    await _productAttributeInternalClient.BatchAddAsync(context, productAndAttributes);
                }

                if (productImages.Any())
                {
                    await _productImageInternalClient.BatchAddAsync(context, productImages);
                }
                _cacheClient.RemoveAll(mappingKeyList);
                response.KvProductIds = newProductMapping.Select(x => x.ProductKvId).ToList();
                response.IsSuccess = true;

                foreach (var productMapping in newProductMapping)
                {
                    await WriteLogAutoMappingProduct(context, productMapping, channel.Name);
                }
            }

            return response;
        }

        private async Task WriteLogAutoCreateProduct(KvInternalContext context, ProductRequest productKvModel)
        {
            var createProductAuditTrail = productKvModel.ToObject<CreateProductAuditTrail>();
            var logAddProduct = new AuditTrailLog
            {
                FunctionId = (int)FunctionType.ShopeeInteration,
                Action = (int)AuditTrailAction.ProductIntergate,
                BranchId = productKvModel.BranchId,
                CreatedDate = DateTime.Now,
                Content = AuditTrailHelper.GetCreateProductContent(createProductAuditTrail).ToString(),
            };

            await _auditTrailInternalClient.AddLogAsync(context, logAddProduct);
        }

        private async Task WriteLogAutoMappingProduct(KvInternalContext context,
            MongoDb.ProductMapping productKvModel, string channelName)
        {
            var productAutoMappingAuditTrail = productKvModel.ToObject<ProductAutoMappingAuditTrail>();

            var logMappingProduct = new AuditTrailLog
            {
                FunctionId = (int)FunctionType.ShopeeInteration,
                Action = (int)AuditTrailAction.ProductIntergate,
                BranchId = context.BranchId,
                CreatedDate = DateTime.Now,
                Content = AuditTrailHelper.GetAutoMappingProductContent(productAutoMappingAuditTrail, channelName).ToString(),
            };
            await _auditTrailInternalClient.AddLogAsync(context, logMappingProduct);
        }

        private async Task<List<ItemListBase>> GetItemListBases(List<long> itemIds, Domain.Model.OmniChannel channel, KvInternalContext context)
        {
            var platform = await _platformService.GetById(channel.PlatformId);
            var client = new ChannelClient.Impls.ShopeeClientV2(_appSettings);
            var auth = await _channelAuthService.GetChannelAuth(channel, context.LogId);
            return await client.GetItemListBaseWithPageSize(new ChannelAuth() { AccessToken = auth.AccessToken, ShopId = auth.ShopId },
                itemIds, context.LogId, platform);
        }

        private async Task<List<ShopeeModelListReponseV2>> GetModelList(List<long> itemIds, Domain.Model.OmniChannel channel, KvInternalContext context)
        {
            var platform = await _platformService.GetById(channel.PlatformId);
            var client = new ChannelClient.Impls.ShopeeClientV2(_appSettings);
            var auth = await _channelAuthService.GetChannelAuth(channel, context.LogId);

            var result = new List<ShopeeModelListReponseV2>();

            foreach (var itemId in itemIds)
            {
                var model = await client.GetModelList(itemId, channel.Id, context.LogId,
                    new ChannelAuth() { AccessToken = auth.AccessToken, ShopId = auth.ShopId }, platform);
                model.ItemId = itemId;

                var numberRetry = _appSettings.Get("NumberRetryGetModelList", 2);
                var millisecondDelay = _appSettings.Get("MillisecondDelayGetModelList", 1000);
                var count = 0;

                while (count < numberRetry && model?.Response?.TierVariations == null)
                {
                    model = await client.GetModelList(itemId, channel.Id, context.LogId,
                        new ChannelAuth() { AccessToken = auth.AccessToken, ShopId = auth.ShopId }, platform);
                    count++;
                    await Task.Delay(millisecondDelay);
                }

                if (model?.Response?.TierVariations == null)
                    throw new OmniException($"TierVariations is null, itemId = {itemId}");

                result.Add(model);
            }

            return result;
        }

        private async Task<(List<ProductAndProductAttribute> productAndAttributes, List<ProductImageRequest> productImages)> GetProductRelateVariaton(
            KvInternalContext context, List<Product> productsKv, List<MongoDb.ProductMapping> productMappings, List<ShopeeModelListReponseV2> modelList)
        {
            var productImages = new List<ProductImageRequest>();
            var productAndAttributes = new List<ProductAndProductAttribute>();

            if (productMappings == null || !productMappings.Any()) return (productAndAttributes, productImages);

            var attributes = modelList.SelectMany(x => x.Response.TierVariations.Select(y => y.Name)).ToList();

            var getAttributeKv = await _attributeInternalClient.GetAttributeByNamesAsync(context,
                attributes.Distinct().ToList());
            var getAttributeNew = attributes.Where(x => !getAttributeKv.Select(y => y.Name.ToUpper()).Contains(x.ToUpper())).ToList();
            var createAttributeKv = getAttributeNew.Any() ? await _attributeInternalClient.BatchAddAsync(context, getAttributeNew) :
                new List<OmniChannelCore.Api.Sdk.Models.Attribute>();

            //Lấy danh sách thuộc tính ở KV
            var kvAttributes = getAttributeKv.Concat(createAttributeKv).ToList();

            //Tạo danh sách thuộc tính và ảnh gắn sản phẩm
            foreach (var productMapping in productMappings)
            {
                var getProduct = productsKv.FirstOrDefault(p => p.Id == productMapping.ProductKvId);
                var getModelList = modelList.FirstOrDefault(x => x.ItemId == productMapping.ParentProductChannelId);
                var getModel = getModelList?.Response?.Model?.FirstOrDefault(x => x.ModelId == productMapping.ProductChannelId);
                if (getModel == null) continue;
                var productAttribute = getModel.TierIndex.Select((value, index) =>
                {
                    var tierVariations = getModelList.Response.TierVariations[index];
                    var optionListImage = tierVariations.OptionList[value].Image;
                    if (optionListImage != null)
                    {
                        var productImage = new ProductImageRequest
                        {
                            ProductId = productMapping.ProductKvId,
                            Image = tierVariations.OptionList[value].Image.ImageUrl
                        };
                        productImages.Add(productImage);
                    }

                    var kvAttribute = kvAttributes.FirstOrDefault(a => a.Name.ToUpper() == tierVariations.Name.ToUpper());
                    return new ProductAttribute
                    {
                        AttributeId = kvAttribute?.Id ?? 0,
                        Value = tierVariations.OptionList[value].Option,
                        ProductId = productMapping.ProductKvId
                    };
                }).ToList();

                productAndAttributes.Add(new ProductAndProductAttribute { Product = getProduct, ProductAttributes = productAttribute });
            }
            return (productAndAttributes, productImages);
        }

        private async Task<long> GetCategoryId(KvInternalContext context, string name)
        {
            var getCategoryResp = await _categoryInternalClient.GetCategoryByNameAsync(context, name);
            if (getCategoryResp != null && getCategoryResp.Id != 0) return getCategoryResp.Id;
            var categoryCreate = new Category { Name = name };
            var category = await _categoryInternalClient.CreateCategoryAsync(context, categoryCreate);
            return category.Id;
        }

        private async Task<List<Domain.Model.Product>> GetProductKv(string connectStringName, int retailerId, int branchId, HashSet<string> codes)
        {
            List<Domain.Model.Product> productList;
            using (var db = await _dbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var productRepo = new ProductRepository(db);
                productList = await productRepo.GetProductByCode(retailerId, branchId, codes);
            }
            return productList;
        }
        private async Task<PosSetting> PosSettings(string connectStringName, int retailerId)
        {
            using (var db = await _dbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var posSettingRepository = new PosSettingRepository(db);
                var postSettingDic = await posSettingRepository.GetSettingAsync(retailerId);
                var posSetting = new PosSetting(postSettingDic, retailerId, _dbConnectionFactory, _cacheClient, _appSettings);
                return posSetting;
            }
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Services.LogginConfiguration;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.FeatureToggle;
using Serilog;

namespace Demo.OmniChannel.Business
{
    public class OnHandBusiness : IOnHandBusiness
    {
        private readonly IProductMappingService _productMappingService;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly ILog _logger = LogManager.GetLogger(typeof(OnHandBusiness));
        private readonly IMessageFactory _messageFactory;
        private readonly IAppSettings _settings;

        public OnHandBusiness(
            IDbConnectionFactory dbConnectionFactory,
            IProductMappingService productMappingService,
            IMessageFactory messageFactory,
            IAppSettings settings)
        {
            _dbConnectionFactory = dbConnectionFactory;
            _productMappingService = productMappingService;
            _messageFactory = messageFactory;
            _settings = settings;
        }

        public async Task SyncMultiOnHand(string connectStringName,
            Domain.Model.OmniChannel channel,
            List<string> channelProductIds,
            List<long> kvProductIdsReq,
            bool isIgnoreAuditTrail,
            Guid logId,
            DateTime? messageSentTime,
            bool isWriteLog = false,
            int totalPageSizeSync = 50,
            List<ProductMapping> mappings = null,
            bool isGetMapping = true,
            LogObjectMicrosoftExtension logMicrosoftExtension = null)
        {
            var logMessage = new LogObject(_logger, logId)
            {
                Action = ExceptionType.SyncMultiOnHandGetProduct,
                RetailerId = channel.RetailerId,
                BranchId = channel.BranchId,
                OmniChannelId = channel.Id,
                RequestObject = $"KvProductIds: {kvProductIdsReq?.Join(", ")} - ChannelProductIds: {channelProductIds?.Join(", ")}"
            };

            try
            {
                if (!Enum.IsDefined(typeof(SyncOnHandFormula), (int)channel.SyncOnHandFormula))
                {
                    logMessage.LogWarning($"SyncOnHandFormula = {channel.SyncOnHandFormula} is not defined.");
                    return;
                }

                if (isGetMapping)
                {
                    mappings = await _productMappingService.GetByChannelProductIds(channel.RetailerId, channel.Id,
                        channelProductIds, kvProductIdsReq, channel.Type == (byte)ChannelType.Sendo,
                        isStringItemId: ConvertHelper.CheckUseStringItemId(channel?.Type));
                }

                if (mappings == null || !mappings.Any())
                {
                    logMessage.LogWarning("Mapping is null");
                    return;
                }

                var productOnHand = new Dictionary<long, ProductBranchDto>();

                using (var db = await _dbConnectionFactory.OpenAsync(connectStringName.ToLower()))
                {
                    var productBranchRepository = new ProductBranchRepository(db);
                    var productRepository = new ProductRepository(db);
                    var productMappingIds = mappings.GroupBy(x => x.ProductKvId)
                        .Select(x => x.FirstOrDefault()?.ProductKvId ?? 0).ToList();
                    var totalPages = (int)Math.Ceiling(productMappingIds.Count / (decimal)1000);
                    LogConnectString(db, logId);

                    for (int page = 1; page <= totalPages; page++)
                    {
                        var productPaging = new HashSet<long>(productMappingIds.Page(page, 1000).ToList());
                        var kvProducts = await productRepository.WhereAsync(p =>
                                p.RetailerId == channel.RetailerId && p.IsActive
                                && (p.isDeleted == null || p.isDeleted == false) &&
                                p.ProductType == (byte)ProductType.Purchased && productPaging.Contains(p.Id));
                        var productIds = kvProducts.Select(p => p.Id).ToList();
                        var productBranches = await productBranchRepository.GetProduct(channel.RetailerId, channel.BranchId, productIds);

                        var getProductOnHand = kvProducts.ToDictionary(p => p.Id, p => new ProductBranchDto
                        {
                            OnHand = SyncHelper.CalculateOnHandFormula(channel.SyncOnHandFormula,
                            productBranches.FirstOrDefault(pb => pb.ProductId == p.Id)?.OnHand ?? 0,
                            productBranches.FirstOrDefault(pb => pb.ProductId == p.Id)?.Reserved ?? 0,
                            productBranches.FirstOrDefault(pb => pb.ProductId == p.Id)?.OnOrder ?? 0),
                            Revision = productBranches.FirstOrDefault(pb => pb.ProductId == p.Id)?.Revision ?? p.Revision
                        });
                        productOnHand = productOnHand.Union(getProductOnHand).ToDictionary(x => x.Key, y => y.Value);
                    }
                }

                if (!productOnHand.Any())
                {
                    logMessage.LogWarning("Product onHand null");
                    return;
                }

                //Loại bỏ các mapping không tìm thấy ở KV (Bị xóa, Hàng service, combo)
                var mappingsFilter = mappings.Where(x => productOnHand.Any(y => y.Key == x.ProductKvId)).ToList();
                if (mappingsFilter == null || !mappingsFilter.Any())
                {
                    logMessage.LogWarning("Mapping is null because product is delete or product is service");
                    return;
                }

                var totalMessage = PushMessageQueueToChannel(mappingsFilter, channel, connectStringName,
                    productOnHand, isIgnoreAuditTrail);

                logMessage.ResponseObject = $"Product onHand productOnHand = {productOnHand.Count}, list message: {string.Join(" , ", totalMessage)}";
                logMessage.LogInfo();
            }
            catch (Exception ex)
            {
                logMessage.LogError(ex);
                throw;
            }
        }


        public async Task<List<ProductBranch>> GetProductBranchesWithRetry(
            ProductBranchRepository productBranchRepository,
            int retailerId,
            long branchId,
            List<long> productIds,
            DateTime? messageSentTime)
        {
            var productBranches = await GetProductBranch(productBranchRepository, retailerId, branchId, productIds);

            if (productBranches?.Any() != true || messageSentTime == null) return productBranches;

            var retryTimes = _settings.Get<int?>("RetryGetData:OnHand:Times") ?? 0;
            if (retryTimes == 0) return productBranches;

            // Handle retry get data after update database
            var retried = 0;
            var delayUpdated = _settings.Get<int>("RetryGetData:OnHand:DelayUpdated");
            var retryInterval = _settings.Get<int>("RetryGetData:OnHand:RetryInterval");
            productBranches.ForEach(p =>
            {
                if (p.ModifiedDate == null) p.ModifiedDate = p.CreatedDate;
            });
            while (true)
            {
                if (retried < retryTimes && productBranches.Any(x => x.ModifiedDate <= messageSentTime.Value.AddMilliseconds(-delayUpdated)))
                {
                    productBranches = await GetProductBranch(productBranchRepository, retailerId, branchId, productIds);
                    retried++;
                    _logger.Info($"Handle retry get data after update database. Retry count {retried}");
                    await Task.Delay(retryInterval);
                }
                else
                {
                    break;
                }
            }

            return productBranches;
        }



        #region Private method
        private async Task<List<ProductBranch>> GetProductBranch(
            ProductBranchRepository productBranchRepository,
            int retailerId,
            long branchId,
            List<long> productIds)
        {
            return await productBranchRepository.WhereAsync(x =>
                x.RetailerId == retailerId &&
                x.BranchId == branchId &&
                productIds.Contains(x.ProductId) &&
                (x.IsActive == null || x.IsActive == true));
        }

        private List<string> PushMessageQueueToChannel(
          List<ProductMapping> mappings,
          Domain.Model.OmniChannel channel,
          string connectStringName,
          Dictionary<long, ProductBranchDto> productOnHand,
          bool isIgnoreAuditTrail)
        {
            var totalQueue = new List<string>();

            switch (channel.Type)
            {
                case (byte)ChannelType.Lazada:
                case (byte)ChannelType.Sendo:
                case (byte)ChannelType.Tiki:
                    var totalPages = (int)Math.Ceiling(mappings.Count / (decimal)50);
                    Parallel.For(1, totalPages + 1, page =>
                    {
                        var productPageMappings = mappings.Page(page, 50).ToList();

                        var productMappingsNormal = productPageMappings.Where(p => p.ProductChannelType == (byte)ShareKernel.Common.ChannelProductType.Normal).ToList();
                        if (productMappingsNormal != null && productMappingsNormal.Any())
                        {
                            var messageOnHand = CreateMessageQueue(productMappingsNormal, channel, connectStringName,
                                productOnHand, isIgnoreAuditTrail);
                            PushOnHandRedisMessageQueue(channel, messageOnHand);
                            totalQueue.Add(messageOnHand.LogId.ToString());
                        }

                        var productMappingsvariation = productPageMappings.Where(p => p.ProductChannelType == (byte)ShareKernel.Common.ChannelProductType.Variation).ToList();
                        if (productMappingsvariation != null && productMappingsvariation.Any())
                        {
                            var messageOnHand = CreateMessageQueue(productMappingsvariation, channel, connectStringName,
                                productOnHand, isIgnoreAuditTrail);
                            PushOnHandRedisMessageQueue(channel, messageOnHand);
                            totalQueue.Add(messageOnHand.LogId.ToString());
                        }
                    });
                    break;
                case (byte)ChannelType.Shopee:
                case (byte)ChannelType.Tiktok:
                    //Nếu có nhiều sản phẩm mapping KvProductId nhóm lại lấy sản phẩm đầu tiên
                    var groupParrent = mappings.GroupBy(p => p.ProductKvId).Select(x => x.FirstOrDefault())
                    .GroupBy(p => p.CommonParentProductChannelId).Select(x => new
                    {
                        Parrent = x.Key,
                        Child = x.ToList()
                    }).ToList();

                    Parallel.ForEach(groupParrent, mapping =>
                    {
                        var messageOnHand = CreateMessageQueue(mapping.Child, channel, connectStringName,
                          productOnHand, isIgnoreAuditTrail);
                        PushOnHandRedisMessageQueue(channel, messageOnHand);
                        totalQueue.Add(messageOnHand.LogId.ToString());
                    });
                    break;
                default:
                    break;
            }
            return totalQueue;
        }

        private void PushOnHandRedisMessageQueue(
            Domain.Model.OmniChannel channel,
           SyncOnHandMessage messageOnHand)
        {
            switch (channel.Type)
            {
                case (byte)ChannelType.Lazada:
                    {
                        if (new LazadaSyncOnHandMessageV2Toggle(_settings).Enable(channel.RetailerId, 0))
                        {
                            using (var mq = _messageFactory.CreateMessageQueueClient())
                            {
                                var message = messageOnHand.ConvertTo<LazadaSyncOnHandMessage>();
                                var mess = new Message { CreatedDate = DateTime.Now, Id = Guid.NewGuid() };
                                mess.Body = message;
                                mq.Publish($"mq:{_settings.Get("LazadaSyncOnHandMessageV2_Topic", "LazadaSyncOnHandMessageV2_Topic")}.inq", mess);
                            }
                        }
                        else
                        {
                            Log.Information($"OnHand LZD send LazadaSyncOnHandMessageV1");
                            using (var mq = _messageFactory.CreateMessageProducer())
                            {
                                mq.Publish(messageOnHand.ConvertTo<LazadaSyncOnHandMessage>());
                            }
                        }
                        break;
                    }

                case (byte)ChannelType.Shopee:
                    {
                        if (new ShopeeSyncOnHandMessageV2Toggle(_settings).Enable(channel.RetailerId, 0))
                        {
                            using (var mq = _messageFactory.CreateMessageQueueClient())
                            {
                                var message = messageOnHand.ConvertTo<ShopeeSyncOnHandMessageV2>();
                                var mess = new Message { CreatedDate = DateTime.Now, Id = Guid.NewGuid() };
                                mess.Body = message;
                                mq.Publish($"mq:{_settings.Get("ShopeeSyncOnHandMessageV2_Topic", "ShopeeSyncOnHandMessageV2_Topic")}.inq", mess);
                            }
                        }
                        else
                        {
                            using (var mq = _messageFactory.CreateMessageProducer())
                            {
                                mq.Publish(messageOnHand.ConvertTo<ShopeeSyncOnHandMessageV2>());
                            }
                        }
                        break;
                    }

                case (byte)ChannelType.Tiki:
                    {
                        if (new TikiSyncOnHandMessageV2Toggle(_settings).Enable(channel.RetailerId, 0))
                        {
                            using (var mq = _messageFactory.CreateMessageQueueClient())
                            {
                                var message = messageOnHand.ConvertTo<TikiSyncOnHandMessage>();
                                var mess = new Message { CreatedDate = DateTime.Now, Id = Guid.NewGuid() };
                                mess.Body = message;
                                mq.Publish($"mq:{_settings.Get("TikiSyncOnHandMessageV2_Topic", "TikiSyncOnHandMessageV2_Topic")}.inq", mess);
                            }
                        }
                        else
                        {
                            using (var mq = _messageFactory.CreateMessageProducer())
                            {
                                mq.Publish(messageOnHand.ConvertTo<TikiSyncOnHandMessage>());
                            }
                        }
                       
                        break;
                    }

                case (byte)ChannelType.Sendo:
                    {
                        if (new SendoSyncOnHandMessageV2Toggle(_settings).Enable(channel.RetailerId, 0))
                        {
                            using (var mq = _messageFactory.CreateMessageQueueClient())
                            {
                                var message = messageOnHand.ConvertTo<SendoSyncOnHandMessage>();
                                var mess = new Message { CreatedDate = DateTime.Now, Id = Guid.NewGuid() };
                                mess.Body = message;
                                mq.Publish($"mq:{_settings.Get("SendoSyncOnHandMessageV2_Topic", "SendoSyncOnHandMessageV2_Topic")}.inq", mess);
                            }
                        }
                        else
                        {
                            using (var mq = _messageFactory.CreateMessageProducer())
                            {
                                mq.Publish(messageOnHand.ConvertTo<SendoSyncOnHandMessage>());
                            }
                        }

                        break;
                    }
                case (byte)ChannelType.Tiktok:
                    {
                        if (new TiktokSyncOnHandMessageV2Toggle(_settings).Enable(channel.RetailerId, 0))
                        {
                            using (var mq = _messageFactory.CreateMessageQueueClient())
                            {
                                var message = messageOnHand.ConvertTo<TiktokSyncOnHandMessage>();
                                var mess = new Message { CreatedDate = DateTime.Now, Id = Guid.NewGuid() };
                                mess.Body = message;
                                mq.Publish($"mq:{_settings.Get("TiktokSyncOnHandMessageV2_Topic", "TiktokSyncOnHandMessageV2_Topic")}.inq", mess);
                            }
                        }
                        else
                        {
                            using (var mq = _messageFactory.CreateMessageProducer())
                            {
                                mq.Publish(messageOnHand.ConvertTo<TiktokSyncOnHandMessage>());
                            }
                        }

                        break;
                    }
            }
        }

        private SyncOnHandMessage CreateMessageQueue(
            List<ProductMapping> mappings,
            Domain.Model.OmniChannel channel,
            string connectStringName,
            Dictionary<long, ProductBranchDto> productOnHand,
            bool isIgnoreAuditTrail)
        {
            var message = new SyncOnHandMessage
            {
                ChannelId = channel.Id,
                ChannelName = channel.Name,
                ChannelType = channel.Type,
                BranchId = channel.BranchId,
                RetailerId = channel.RetailerId,
                KvEntities = connectStringName,
                MultiProducts = mappings.Select(p => new MultiProductItem
                {
                    ItemId = p.CommonProductChannelId,
                    ItemSku = p.ProductChannelSku,
                    KvProductSku = p.ProductKvSku,
                    KvProductId = p.ProductKvId,
                    ParentItemId = p.CommonParentProductChannelId,
                    OnHand = productOnHand.ContainsKey(p.ProductKvId)
                        ? Math.Floor(productOnHand[p.ProductKvId].OnHand ?? 0)
                        : 0,
                    Revision = productOnHand.ContainsKey(p.ProductKvId) ? productOnHand[p.ProductKvId].Revision : null
                }).ToList(),
                IsIgnoreAuditTrail = isIgnoreAuditTrail,
                LogId = Guid.NewGuid(),
            };
            return message;
        }

        private void LogConnectString(System.Data.IDbConnection db, Guid logId)
        {
            var logMessage = new LogObject(_logger, logId)
            {
                Action = "LogConnectString",
                ResponseObject = db.ConnectionString
            };
            logMessage.LogInfo();
        }
        #endregion
    }
}

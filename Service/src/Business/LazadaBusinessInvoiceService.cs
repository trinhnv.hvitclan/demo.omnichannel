﻿using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.DomainService.Interfaces;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.RedisStreamMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Messaging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Business
{
    public class LazadaBusinessInvoiceService : BaseCreateInvoiceBusiness
    {
        private readonly IDeliveryInfoRepository _deliveryInfoRepository;
        private readonly IProductMappingService _productMappingService;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IInvoiceInternalClient _invoiceInternalClient;

        public LazadaBusinessInvoiceService
            (IOmniChannelSettingService omniChannelSettingService,
            IInvoiceRepository invoiceRepository,
            IInvoiceMongoService invoiceMongoServices,
            IDeliveryInfoInternalClient deliveryInfoInternalClient,
            IRetryInvoiceMongoService retryInvoiceMongoService,
            IInvoiceInternalClient invoiceInternalClient,
            IAuditTrailInternalClient auditTrailInternalClient,
            IOrderInternalClient orderInternalClient,
            ChannelClient.Impls.ChannelClient channelClient,
            IAppSettings appSettings,
            ICustomerInternalClient customerInternalClient,
            IDeliveryInfoRepository deliveryInfoRepository,
            IProductMappingService productMappingService,
            ICacheClient cacheClient,
            ICreateOrderDomainService createOrderDomainService,
            ISurChargeInternalClient surChargeInternalClient,
            IIntegrationEventService integrationEventService,
            ICreateInvoiceDomainService createInvoiceDomainService,
            IPriceBookInternalClient priceBookInternalClient,
            IProductInternalClient productInternalClient,
            ServiceStack.Data.IDbConnectionFactory dbConnectionFactory,
            IMessageService messageService,
            IKvLockRedis kvLockRedis
            ) : base(omniChannelSettingService,
                invoiceMongoServices,
                deliveryInfoInternalClient,
                retryInvoiceMongoService,
                invoiceInternalClient,
                auditTrailInternalClient,
                orderInternalClient,
                channelClient,
                appSettings,
                customerInternalClient,
                cacheClient,
                createOrderDomainService,
                surChargeInternalClient,
                integrationEventService,
                createInvoiceDomainService,
                priceBookInternalClient,
                productInternalClient,
                dbConnectionFactory,
                messageService,
                kvLockRedis)
        {
            _deliveryInfoRepository = deliveryInfoRepository;
            _productMappingService = productMappingService;
            _invoiceRepository = invoiceRepository;
            _invoiceInternalClient = invoiceInternalClient;
        }

        protected override Task UpdateFeeDeliveryInOrder(
            OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext,
            long? deliveryInfoId,
            decimal? price,
            string feeJson)
        {
            return Task.CompletedTask;
        }

        protected override void UpdateOrderDetail(ChannelClient.Models.KvInvoice invoice,
            List<MongoDb.ProductMapping> mappings)
        {
            foreach (var detail in invoice.InvoiceDetails)
            {
                var mapping = mappings?.FirstOrDefault(x => (!string.IsNullOrEmpty(detail.ProductChannelId)
                && x.CommonProductChannelId == detail.ProductChannelId)
                || x.ProductChannelSku.Equals(detail.ProductChannelSku));

                detail.ProductId = mapping?.ProductKvId ?? 0;
                detail.ProductCode = mapping?.ProductKvSku ?? string.Empty;
            }
        }

        protected override async Task<List<ProductMapping>> GetMappings(ChannelClient.Models.KvInvoice invoice,
            int retailerId, long channelId, byte channelType)
        {
            var productIds = invoice.InvoiceDetails.Where(p => !string.IsNullOrEmpty(p.ProductChannelId))
               .Select(p => p.ProductChannelId.ToString()).ToList();

            List<ProductMapping> mappings;

            if (productIds.Count > 0)
            {
                mappings = await _productMappingService.GetByChannelProductIds(retailerId, channelId, productIds, isStringItemId: ConvertHelper.CheckUseStringItemId(channelType));
            }
            else
            {
                var productSkus = invoice.InvoiceDetails.Where(p => !string.IsNullOrEmpty(p.ProductChannelSku)).Select(p => p.ProductChannelSku).ToList();
                mappings = await _productMappingService.GetByChannelProductSkus(retailerId, channelId, productSkus);
            }

            return mappings;
        }

        protected override async Task HandleVoidInvoice(List<KvInvoice> invoices, CreateInvoiceMessage data,
            OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext,
            OmniChannelCore.Api.Sdk.Models.Order existOrder)
        {
            var originalCode = invoices.FirstOrDefault(x => !string.IsNullOrEmpty(x.Code))?.Code.Split('.')[0];
            var existedInvoices = (await _invoiceRepository.WhereAsync(x => x.RetailerId == data.RetailerId && x.Code.StartsWith(originalCode))).ToList();

            var existIds = existedInvoices.Select(x => x.Id).ToList();
            var existDeliveries = await _deliveryInfoRepository.WhereAsync(x => x.RetailerId == data.RetailerId && existIds.Contains(x.InvoiceId ?? 0));
            var lzdDeliveryCodes = invoices.Select(x => x.DeliveryCode).ToList();
            foreach (var item in existedInvoices)
            {
                var deliveryCodeExist = existDeliveries.FirstOrDefault(x => x.InvoiceId == item.Id)?.DeliveryCode;
                if (string.IsNullOrEmpty(deliveryCodeExist)) continue;

                if (item.Status != (byte)ChannelClient.Common.InvoiceState.Issued &&
                    item.Status != (byte)ChannelClient.Common.InvoiceState.Void &&
                    item.OrderId == existOrder.Id &&
                    !lzdDeliveryCodes.Contains(deliveryCodeExist))
                {
                    await _invoiceInternalClient.VoidInvoice(coreContext, item.Id, data.RetailerId);
                    await WriteLogForUpdateInvoice(data, new KvInvoice
                    {
                        Code = item.Code,
                        NewStatus = (byte)ChannelClient.Common.InvoiceState.Void
                    }, 0, coreContext, true);
                }
            }
        }
    }
}

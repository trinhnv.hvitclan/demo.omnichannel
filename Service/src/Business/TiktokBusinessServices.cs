﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Interfaces.NewBase;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Tiktok;
using Demo.OmniChannel.Domain.Common;
using Demo.OmniChannel.DomainService.Interfaces;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Sdk.Interfaces;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.EventLogMessage;
using Demo.OmniChannel.ShareKernel.RedisStreamMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Newtonsoft.Json;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using ServiceStack.Redis;
using System;
using System.Linq;
using System.Threading.Tasks;
using SDKCCoreInterface = Demo.OmniChannelCore.Api.Sdk.Interfaces;

namespace Demo.OmniChannel.Business.Implements
{
    public class TiktokBusinessServices : BaseCreateOrderService<TiktokCreateOrderStreamMessage>, ITiktokBusinessInterface
    {
        private readonly ITiktokBusinessClientV2 _tiktokBusinessClientV2;
        private readonly IIntegrationEventService _integrationEventService;

        public TiktokBusinessServices(
            IIntegrationEventService integrationEventService,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IDeliveryInfoInternalClient deliveryInfoInternalClient,
            IKvLockRedis kvLockRedis,
            IRedisClientsManager redisClientsManager,
            IOrderMongoService orderMongoService,
            IInvoiceMongoService invoiceMongoService,
            IOmniChannelSettingService omniChannelSettingService,
            IRetryOrderMongoService retryOrderMongoService,
            IRetryInvoiceMongoService retryInvoiceMongoService,
            IAuditTrailInternalClient auditTrailInternalClient,
            ISurChargeInternalClient surChargeInternalClient,
            IProductMappingService productMappingService,
            ISendoLocationMongoService sendoLocationMongoService,
            ISurChargeBranchInternalClient surChargeBranchInternalClient,
            IIntegrationInternalClient integrationInternalClient,
            ICustomerInternalClient customerInternalClient,
            SDKCCoreInterface.IOrderInternalClient orderInternalClient,
            IPartnerDeliveryInternalClient partnerInternalClient,
            IMessageService messageService,
            IAppSettings settings,
            ITiktokBusinessClientV2 tiktokBusinessClientV2,
            ICreateOrderDomainService createOrderDomainService
            ) :
            base(integrationEventService, dbConnectionFactory, redisClientsManager,
                 cacheClient, orderMongoService, invoiceMongoService,
                 retryOrderMongoService, retryInvoiceMongoService,
                 auditTrailInternalClient, surChargeInternalClient,
                 omniChannelSettingService, productMappingService,
                 sendoLocationMongoService, surChargeBranchInternalClient,
                 integrationInternalClient, customerInternalClient,
                 orderInternalClient, partnerInternalClient, kvLockRedis,
                 messageService, settings, deliveryInfoInternalClient,
                 createOrderDomainService)
        {
            _tiktokBusinessClientV2 = tiktokBusinessClientV2;
            OmniChannelType = (byte)ChannelType.Tiktok;
            _ = new SelfAppConfig(settings);
            _integrationEventService = integrationEventService;
        }


        protected override void PushMessageToOrderTrackingEvent(TiktokCreateOrderStreamMessage message, string error ="")
        {
            if (message?.Source != null && message.Source.ToLower().Equals(ProcessOrderSource.WebHook))
            {
                string exChangeTrackProcessExChange = _appSettings.Get<string>("OrderTrackingEventExChange");
                var eventModel = new TrackModelEvent
                {
                    Code = message.Code,
                    OrderSn = message.OrderSn,
                    ErrorMessage = error,
                    Status = ProcessOrderStatus.Processed,
                    OrderTimestamp = message.Timestamps ?? 0,
                    ShopId = message.ShopId,
                    Source = message.Source,
                    ProcessType = ProcessFlowType.Order
                };
                _integrationEventService.AddEventWithRoutingKeyAsync(new OrderTrackingEvent
                {
                    TrackModelEvent = eventModel,
                }, RoutingKey.OrderTrackingEvent, exChangeTrackProcessExChange);
            }
        }

        protected override TiktokCreateOrderStreamMessage DeserializeRequest(string request)
        {
            return JsonConvert.DeserializeObject<TiktokCreateOrderStreamMessage>(request);
        }

        protected override KvOrder GenKvOrder(KvInternalContext context, OmniChannelSettingObject setting, TiktokCreateOrderStreamMessage message)
        {
            return _tiktokBusinessClientV2.GenListKvOrder(context.BranchId, context.UserId, context.RetailerId, context.Group?.Id ?? 0, setting, JsonConvert.SerializeObject(message));
        }

        protected override string GetOrderId(TiktokCreateOrderStreamMessage message)
        {
            return JsonConvert.DeserializeObject<TiktokOrderDetail>(message.Order.ToString())?.OrderId;
        }

        protected async override Task ProgressOrderIfExisted(KvInternalContext context, Domain.Model.OmniChannel channel, KvOrder kvOrder, Domain.Model.Order existedOrder)
        {
            await RemoveErrorOrderInMongo(kvOrder.Ordersn, context.BranchId, channel.Id, kvOrder.NewStatus == (byte)OrderState.Void);
            await RemoveMongoRetryOrder(kvOrder.Ordersn, context.BranchId, channel.Id, context.RetailerId);
            if (kvOrder.NewStatus == (byte)OrderState.Void)
            {
                var coreContext = await CreateKVCoreContext(context);
                PushLishMQToCreateLog((int)LogType.Info, context.LogId, "VoidKvOrderIfNoExistedInvoice", kvOrder.Ordersn, null, context.RetailerId, channel.Id, "void kvOrder", null);
                using (var db = await _dbConnectionFactory.OpenAsync(context.Group.ConnectionStringName.ToLower()))
                {
                    var invoiceRepository = new InvoiceRepository(db);
                    var existInvoices = (await invoiceRepository.GetByOrderId(existedOrder.Id, channel.RetailerId));
                    if (existInvoices == null || existInvoices.Count == 0)
                    {
                        await VoidKvOrder(coreContext, context, kvOrder, existedOrder.Id, channel.Type);
                    }
                }

            }
            if (kvOrder.Status != (byte)OrderState.Void)
            {
                await CreateDeliveryForOrderIfNotExisted(context, existedOrder, kvOrder);
                await PublishCreateInvoiceQueue(context, channel, kvOrder, existedOrder, false);
                PushLishMQToCreateLog((int)LogType.Info, context.LogId, "ProgressOrderIfExisted", kvOrder.ToSafeJson(), null, context.RetailerId, channel.Id, "PublishCreateInvoiceQueue", null);
            }
        }

        protected override bool FilterStatus(KvOrder kvOrder, Guid logId, Domain.Model.OmniChannel channel)
        {
            if (kvOrder.ChannelStatus == TiktokOrderStatus.Unpaid.ToString() || kvOrder.ChannelStatus == TiktokOrderStatus.Cancelled.ToString())
            {
                string channelStatus = kvOrder.ChannelStatus == TiktokOrderStatus.Unpaid.ToString() ? "Unpaid" : "Unpaid -> Cancel";
                PushLishMQToCreateLog((int)LogType.Warning, logId, "ProgressOrderIfNonExisted", null, null, channel.RetailerId,
                    channel.Id, $"Đơn hàng ở trạng thái {channelStatus} không đồng bộ", null);
                return false;
            }
            return true;
        }

        protected override void PushRetryMessage(string message, byte channelType)
        {
            string orderString = string.Empty;
            string orderId = string.Empty;
            var orderTiktokDeadLetter = JsonConvert.DeserializeObject<TiktokDeadLetterQueueCreateOrder>(message);
            if (orderTiktokDeadLetter.RetryCount > MAX_RETRY) return;
            orderTiktokDeadLetter.RetryCount++;
            var orderTiktok = JsonConvert.DeserializeObject<TiktokOrderDetail>(orderTiktokDeadLetter.Order.ToString());
            orderId = orderTiktok?.OrderId;
            orderString = JsonConvert.SerializeObject(orderTiktokDeadLetter);
            var kafkaConfig = _appSettings.Get<Kafka>("Kafka");
            var topicName = kafkaConfig.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.DLQCreateOrder &&
            x.ChannelType == channelType)?.Name;
            KafkaClient.KafkaClient.Instance.PublishMessage($"{topicName}", orderId.ToMD5Hash(), orderString);
        }

        protected override async Task<bool> SkipCreateCustomer(KvInternalContext context)
        {
            ShareKernel.Dto.OmniPlatformSettingDto settings;

            using (var db = await _dbConnectionFactory.OpenAsync())
            {
                var omniPlatformSettingRepository = new OmniPlatformSettingRepository(db);

                settings =
                    await omniPlatformSettingRepository.GetByRetailerAsync(context.RetailerId);
            }

            if (!settings.IsTiktokShopSyncCustomer) return true;
            return false;
        }

        protected override bool ValidateMessage(TiktokCreateOrderStreamMessage message)
        {
            return true;
        }
    }
}

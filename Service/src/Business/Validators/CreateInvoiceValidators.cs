﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using ServiceStack.FluentValidation;
using ServiceStack.FluentValidation.Validators;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Business.Validators
{
    public class CreateInvoiceValidators : AbstractValidator<OmniChannelCore.Api.Sdk.Models.Order>
    {
        private readonly IInvoiceInternalClient _invoiceInternalClient;
        public CreateInvoiceValidators(
            IInvoiceInternalClient invoiceInternalClient,
            byte channelType,
            OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext)
        {
            _invoiceInternalClient = invoiceInternalClient;
            ValidateOrder(channelType, coreContext);
        }
        protected void ValidateOrder(byte channelType,
            OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext)
        {
            RuleFor(s => s)
                .CustomAsync(async (obj, context, token) =>
                {
                    var isValidNotExitOrder = ValidateOrderNotExistOrCannel(obj, context);
                    if (!isValidNotExitOrder) return;

                    var isValiedOrderFinalized = await ValidateOrderFinalized(obj, context, 
                        channelType, coreContext);
                    if (!isValiedOrderFinalized) return;
                });
        }

        #region Private method
        private bool ValidateOrderNotExistOrCannel(OmniChannelCore.Api.Sdk.Models.Order order,
            CustomContext context)
        {
            if (order == null || order.Status == (byte)OrderState.Void)
            {
                context.AddFailure("Order status invalid");
                return false;
            }
            return true;
        }

        private async Task<bool> ValidateOrderFinalized(OmniChannelCore.Api.Sdk.Models.Order order,
            CustomContext context, byte channelType,
            OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext)
        {
            if (order?.Status == (byte)OrderState.Finalized)
            {
                var invoicePrefix = ChannelTypeHelper.GetInvoicePrefix(channelType);
                var existInvoices = await _invoiceInternalClient.GetByOrderId(coreContext, order.Id);

                var existInvoiceInprogress = existInvoices?.FirstOrDefault(x =>
                    x.Status != (byte)InvoiceState.Void && x.Status != (byte)InvoiceState.Issued);

                if (existInvoiceInprogress != null
                    && !string.IsNullOrEmpty(existInvoiceInprogress.Code)
                    && !existInvoiceInprogress.Code.StartsWith(invoicePrefix))
                {
                    context.AddFailure("Invoice existed - Issue");
                    return false;
                }

                var existInvoiceFinal = existInvoices?.FirstOrDefault(x =>
                    x.Status == (byte)InvoiceState.Void || x.Status == (byte)InvoiceState.Issued);
                if (existInvoiceInprogress == null && existInvoiceFinal != null
                    && !string.IsNullOrEmpty(existInvoiceFinal.Code) &&
                    !existInvoiceFinal.Code.StartsWith(invoicePrefix))
                {
                    context.AddFailure("Invoice existed - Void");
                    return false;
                }
            }
            return true;
        }
        #endregion
    }
}

﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ShareKernel.Common;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using System;
using System.Data;
using System.Threading.Tasks;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.Utilities;
using ServiceStack;
using ServiceStack.Logging;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Business
{
    public class ChannelBusiness : IChannelBusiness
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly ICacheClient _cacheClient;
        private readonly IAppSettings _settings;
        private readonly ILog _logger = LogManager.GetLogger(typeof(ChannelBusiness));

        public ChannelBusiness(IDbConnectionFactory dbConnectionFactory, ICacheClient cacheClient, IAppSettings settings)
        {
            _dbConnectionFactory = dbConnectionFactory;
            _cacheClient = cacheClient;
            _settings = settings;
        }

        public async Task<Domain.Model.OmniChannel> GetChannel(long channelId, int retailerId, Guid logId)
        {
            var lockSetCacheKey = KvConstant.LockSetCacheChannelKey.Fmt(retailerId, channelId);
            var delayCount = 0;
            while (_cacheClient.Get<bool>(lockSetCacheKey) && delayCount < 10)
            {
                _logger.Info($"lockSetCacheKey: {lockSetCacheKey}");
                await Task.Delay(500);
                delayCount++;
            }

            var cacheKey = string.Format(KvConstant.ChannelByIdKey, channelId);
            var channel = _cacheClient.Get<Domain.Model.OmniChannel>(cacheKey);

            if (channel != null && channel.Id == channelId && channel.RetailerId == retailerId) return channel;

            // Log omni channel in cache conflict data
            if (channel != null &&
                (channel.Id != channelId || channel.RetailerId != retailerId))
            {
                var exMessage = $"Data in cache OmniChannelId {channelId} conflict (key: {cacheKey}. data: {channel.ToJson()}).";
                _logger.Error($"{nameof(KvOmniChannelDataConflictException)} error: {exMessage} - Context: {new ExceptionContextModel {LogId = logId, RetailerId = retailerId, ChannelId = channelId}.ToJson()}", new KvOmniChannelDataConflictException(exMessage));
            }
            
            _cacheClient.Set(lockSetCacheKey, true, DateTime.Now.AddMilliseconds(5000));

            using (var db = _dbConnectionFactory.OpenDbConnection())
            {
                using (_ = db.OpenTransaction(IsolationLevel.ReadUncommitted))
                {
                    var channelRepository = new OmniChannelRepository(db);
                    channel = await channelRepository.SingleAsync(x => x.Id == channelId && x.RetailerId == retailerId);
                }
            }

            if (channel == null)
            {
                _cacheClient.Remove(lockSetCacheKey);
                return null;
            }

            var expiredIn = TimeSpan.FromDays(NumberHelper.GetValueOrDefault(_settings?.Get<int?>("ChannelCache"), 30));
            _cacheClient.Set(cacheKey, channel, expiredIn);
            _cacheClient.Remove(lockSetCacheKey);

            return channel;
        }
    }
}

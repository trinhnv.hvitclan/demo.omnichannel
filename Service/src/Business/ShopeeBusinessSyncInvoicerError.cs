﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.DomainService.Interfaces;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.Business
{
    public class ShopeeBusinessSyncInvoicerError : BaseSyncInvoiceErrorBusiness, IShopeeBusinessSyncInvoicerError
    {
        public ShopeeBusinessSyncInvoicerError(
            ILogger<ShopeeBusinessSyncInvoicerError> logger,
            IAppSettings settings, ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IKvLockRedis kvLockRedis,
            IProductMappingService productMappingService,
            IInvoiceMongoService invoiceMongoService,
            IInvoiceInternalClient invoiceInternalClient, 
            IAuditTrailInternalClient auditTrailInternalClient,
            IOmniChannelAuthService channelAuthService, 
            IRetryInvoiceMongoService retryInvoiceMongoService,
            IOrderInternalClient orderInternalClient, 
            IChannelBusiness channelBusiness,
            IDeliveryInfoInternalClient deliveryInfoInternalClient,
            IDeliveryPackageInternalClient deliveryPackageInternalClient,
            ICustomerInternalClient customerInternalClient,
            IOmniChannelSettingService omniChannelSettingService,
            ISurChargeInternalClient surChargeInternalClient,
            IOmniChannelPlatformService omniChannelPlatformService,
            ICreateInvoiceDomainService createInvoiceDomainService,
            IMessageService messageService,
            IChannelClient channelClient)
            : base(logger,
                  settings,
                  cacheClient,
                  dbConnectionFactory,
                  kvLockRedis,
                  productMappingService,
                  invoiceMongoService,
                  invoiceInternalClient,
                  auditTrailInternalClient,
                  channelAuthService,
                  retryInvoiceMongoService,
                  orderInternalClient,
                  channelBusiness,
                  deliveryInfoInternalClient,
                  deliveryPackageInternalClient,
                  customerInternalClient,
                  omniChannelSettingService,
                  surChargeInternalClient,
                  omniChannelPlatformService,
                  createInvoiceDomainService,
                  messageService,
                  channelClient)
        {
        }
    }
}

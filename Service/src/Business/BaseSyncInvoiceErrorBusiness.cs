﻿using Demo.Audit.Model.Message;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.RequestDTO;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.DomainService.Interfaces;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.Services.LogginConfiguration;
using Demo.OmniChannel.ShareKernel.Auth;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Common;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Demo.OmniChannelCore.Api.Sdk.Models;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeliveryInfo = Demo.OmniChannelCore.Api.Sdk.Models.DeliveryInfo;
using Invoice = Demo.OmniChannel.MongoDb.Invoice;
using InvoiceDelivery = Demo.OmniChannelCore.Api.Sdk.Models.InvoiceDelivery;
using InvoiceDetail = Demo.OmniChannelCore.Api.Sdk.Models.InvoiceDetail;
using Platform = Demo.OmniChannel.ChannelClient.Models.Platform;
using ProductMapping = Demo.OmniChannel.MongoDb.ProductMapping;


namespace Demo.OmniChannel.Business
{
    public abstract class BaseSyncInvoiceErrorBusiness : BaseBusiness, IBaseSyncInvoiceErrorBusiness
    {
        private readonly IAppSettings _settings;
        private readonly ICacheClient _cacheClient;
        private readonly IProductMappingService _productMappingService;
        private readonly IInvoiceMongoService _invoiceMongoService;
        private readonly IInvoiceInternalClient _invoiceInternalClient;
        private readonly IAuditTrailInternalClient _auditTrailInternalClient;
        private readonly IRetryInvoiceMongoService _retryInvoiceMongoService;
        private readonly IOmniChannelAuthService _channelAuthService;
        private readonly IOrderInternalClient _orderInternalClient;
        private readonly IDeliveryPackageInternalClient _deliveryPackageInternalClient;
        private readonly IChannelBusiness _channelBusiness;
        private readonly IKvLockRedis _kvLockRedis;
        private readonly ICustomerInternalClient _customerInternalClient;
        private readonly IOmniChannelSettingService _omniChannelSettingService;
        private readonly ISurChargeInternalClient _surChargeInternalClient;
        private readonly IOmniChannelPlatformService _omniChannelPlatformService;
        private readonly ICreateInvoiceDomainService _createInvoiceDomainService;
        private readonly ILogger _logger;
        protected readonly IDeliveryInfoInternalClient _deliveryInfoInternalClient;
        protected readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly IChannelClient _channelClient;

        public BaseSyncInvoiceErrorBusiness(
            ILogger logger,
            IAppSettings settings,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IKvLockRedis kvLockRedis,
            IProductMappingService productMappingService,
            IInvoiceMongoService invoiceMongoService,
            IInvoiceInternalClient invoiceInternalClient,
            IAuditTrailInternalClient auditTrailInternalClient,
            IOmniChannelAuthService channelAuthService,
            IRetryInvoiceMongoService retryInvoiceMongoService,
            IOrderInternalClient orderInternalClient,
            IChannelBusiness channelBusiness,
            IDeliveryInfoInternalClient deliveryInfoInternalClient,
            IDeliveryPackageInternalClient deliveryPackageInternalClient,
            ICustomerInternalClient customerInternalClient,
            IOmniChannelSettingService omniChannelSettingService,
            ISurChargeInternalClient surChargeInternalClient,
            IOmniChannelPlatformService omniChannelPlatformService,
            ICreateInvoiceDomainService createInvoiceDomainService,
            IMessageService messageService,
            IChannelClient channelClient)
            : base(settings, dbConnectionFactory, messageService)
        {
            _logger = logger;
            _settings = settings;
            _cacheClient = cacheClient;
            _dbConnectionFactory = dbConnectionFactory;
            _kvLockRedis = kvLockRedis;
            _productMappingService = productMappingService;
            _invoiceMongoService = invoiceMongoService;
            _invoiceInternalClient = invoiceInternalClient;
            _auditTrailInternalClient = auditTrailInternalClient;
            _channelAuthService = channelAuthService;
            _retryInvoiceMongoService = retryInvoiceMongoService;
            _orderInternalClient = orderInternalClient;
            _channelBusiness = channelBusiness;
            _deliveryInfoInternalClient = deliveryInfoInternalClient;
            _deliveryPackageInternalClient = deliveryPackageInternalClient;
            _customerInternalClient = customerInternalClient;
            _omniChannelSettingService = omniChannelSettingService;
            _surChargeInternalClient = surChargeInternalClient;
            _omniChannelPlatformService = omniChannelPlatformService;
            _createInvoiceDomainService = createInvoiceDomainService;
            _channelClient = channelClient;
        }

        public async Task<bool> ProcessSyncInvoiceError(BaseSyncErrorInvoiceMessage data)
        {
            await RetryInvoices(data);
            return true;
        }

        private async Task RetryInvoices(BaseSyncErrorInvoiceMessage data)
        {
            var loggerExtension = new LogObjectMicrosoftExtension(_logger, data.LogId)
            {
                Action = "SyncErrorInvoice",
                RetailerId = data.RetailerId,
                BranchId = data.BranchId,
                OmniChannelId = data.ChannelId
            };

            Invoice invoice = data.Invoice.FromJson<Invoice>();
            if (invoice == null) return;
            KvInternalContext coreContext = new KvInternalContext();
            string connectStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);
            var context = await ContextHelper.GetExecutionContext(_cacheClient, _dbConnectionFactory, data.RetailerId,
                connectStringName, invoice.BranchId, _settings?.Get<int>("ExecutionContext"));

            try
            {
                using (var lockByOrderId = _kvLockRedis.GetLockFactory())
                {
                    using (lockByOrderId.AcquireLock(string.Format(KvConstant.LockProcessInvoice, data.RetailerId, invoice.ChannelOrderId), TimeSpan.FromSeconds(_settings.Get("LockProcessInvoiceSecond", 60))))
                    {
                        var channel = await _channelBusiness.GetChannel(data.ChannelId, data.RetailerId, data.LogId);
                        if (channel == null) return;

                        byte channelType = channel.Type;
                        data.ChannelType = channelType;
                        coreContext = context.ConvertTo<KvInternalContext>();
                        coreContext.UserId = context.User?.Id ?? 0;
                        coreContext.LogId = data.LogId;
                        loggerExtension.Update(invoice.ChannelOrderId, channel.Type, ((ChannelTypeEnum)channelType).ToString(), context.GroupId);
                        await ProcessInvoice(data, invoice, channel, loggerExtension, context, coreContext, channelType, connectStringName);
                        loggerExtension.LogInfo();
                    }
                }
            }
            catch (Exception e)
            {
                await HandleException(data, loggerExtension, invoice, coreContext, connectStringName, e);
            }
        }
        private async Task HandleException(BaseSyncErrorInvoiceMessage data, LogObjectMicrosoftExtension loggerExtension, Invoice invoice, KvInternalContext coreContext, string connectStringName, Exception e)
        {
            loggerExtension.Description = $"{e.Message} - {e.StackTrace}";
            loggerExtension.ResponseObject = null;
            loggerExtension.LogError(e, false, e is OmniChannelCore.Api.Sdk.Common.KvValidateException);

            if (string.IsNullOrEmpty(e.Message) ||
                (!e.Message.Trim().Equals(string.Format(KvOrderValidateMessage.OrderExistedVi, invoice.Code).Trim(), StringComparison.OrdinalIgnoreCase)
                 && !e.Message.Trim().Equals(string.Format(KvOrderValidateMessage.OrderExistedEn, invoice.Code).Trim(), StringComparison.OrdinalIgnoreCase)))
            {
                Domain.Model.Order existOrder = await GetOrderByCode(invoice, data.ChannelType, connectStringName);
                await SaveSyncInvoiceError(coreContext, invoice, existOrder, loggerExtension, invoice.ChannelOrderId,
                    invoice.ChannelId,
                    data.ChannelType,
                    e.Message, e.Message);

                if (e is KvDbException
                    || e.Message.ToLower().Contains(DbExceptionMessages.ConnectionTimeout)
                    || e.Message.ToLower().Contains(DbExceptionMessages.TransactionDeadlocked)
                    || e.Message.ToLower().Contains(DbExceptionMessages.DatabaseException))
                {
                    await AddOrUpdateRetryInvoice(data, invoice, existOrder, e is KvDbException, invoice.Code);
                }
            }
        }
        /// <summary>
        /// Xử lý hóa đơn
        /// </summary>
        /// <param name="data"></param>
        /// <param name="invoice"></param>
        /// <param name="channel"></param>
        /// <param name="loggerExtension"></param>
        /// <param name="context"></param>
        /// <param name="coreContext"></param>
        /// <param name="channelType"></param>
        /// <param name="connectStringName"></param>
        /// <returns></returns>
        private async Task ProcessInvoice(BaseSyncErrorInvoiceMessage data,
            Invoice invoice,
            Domain.Model.OmniChannel channel,
            LogObjectMicrosoftExtension loggerExtension,
            ExecutionContext context,
            KvInternalContext coreContext,
            byte channelType,
            string connectStringName)
        {
            var invoiceInMongo = await _invoiceMongoService.GetByInvoiceId(invoice.ChannelOrderId, invoice.BranchId, invoice.ChannelId, invoice.Code);
            Domain.Model.Order existOrder = await GetOrderByCode(invoice, channelType, connectStringName);

            await RemoveMongoRetryInvoice(invoice.ChannelOrderId, invoice.BranchId, invoice.ChannelId,
                invoice.RetailerId);

            if (existOrder == null || existOrder.Status == (byte)OrderState.Void)
            {
                await RemoveErrorInvoice(invoiceInMongo);
                loggerExtension.ResponseObject = existOrder;
                loggerExtension.Description = "Order is null";
                loggerExtension.LogInfo();
                return;
            }

            #region Status Finalized (Hoàn thành) & Ongoing (Đang giao hàng)

            byte? oldInvoiceStatus = null;
            long invoiceId = 0;
            if (existOrder?.Status == (byte)OrderState.Finalized || existOrder?.Status == (byte)OrderState.Ongoing)
            {
                var invoicePrefix = ChannelTypeHelper.GetInvoicePrefix(channelType);

                List<Domain.Model.Invoice> existInvoices = new List<Domain.Model.Invoice>();
                using (var db = await _dbConnectionFactory.OpenAsync(connectStringName.ToLower()))
                {
                    var invoiceRepository = new InvoiceRepository(db);
                    existInvoices = await invoiceRepository.GetByOrderId(existOrder.Id, data.RetailerId);
                }

                var existInvoiceInprogress = existInvoices?.FirstOrDefault(x => x.Status != (byte)InvoiceState.Void && x.Status != (byte)InvoiceState.Issued && x.Code == invoice.Code);
                if (existInvoiceInprogress != null && !string.IsNullOrEmpty(existInvoiceInprogress.Code) && !existInvoiceInprogress.Code.StartsWith(invoicePrefix))
                {
                    loggerExtension.Description = "Invoice existed - Issue";
                    loggerExtension.LogInfo();
                    await RemoveErrorInvoice(invoiceInMongo);
                    return;
                }

                var existInvoiceFinal = existInvoices?.FirstOrDefault(x => x.Status == (byte)InvoiceState.Void || x.Status == (byte)InvoiceState.Issued);
                if (existInvoiceInprogress == null && existInvoiceFinal != null && !string.IsNullOrEmpty(existInvoiceFinal.Code) && !existInvoiceFinal.Code.StartsWith(invoicePrefix))
                {
                    loggerExtension.Description = "Invoice existed - Void";
                    loggerExtension.LogInfo();
                    await RemoveErrorInvoice(invoiceInMongo);
                    return;
                }

                if (existInvoiceInprogress != null)
                {
                    invoiceId = existInvoiceInprogress.Id;
                    oldInvoiceStatus = existInvoiceInprogress.Status;
                }
                await RemoveErrorInvoice(invoiceInMongo);
            }
            #endregion

            await UpdateInsertInvoice(data, existOrder, invoice, invoiceId, channel, loggerExtension, context, coreContext, channelType, connectStringName, oldInvoiceStatus);
            await RemoveErrorInvoice(invoiceInMongo);
        }
        /// <summary>
        /// Thêm sửa Hóa đơn
        /// </summary>
        /// <param name="data"></param>
        /// <param name="existOrder"></param>
        /// <param name="invoice"></param>
        /// <param name="invoiceId"></param>
        /// <param name="channel"></param>
        /// <param name="loggerExtension"></param>
        /// <param name="context"></param>
        /// <param name="coreContext"></param>
        /// <param name="channelType"></param>
        /// <param name="connectStringName"></param>
        /// <param name="oldInvoiceStatus"></param>
        /// <returns></returns>
        private async Task UpdateInsertInvoice(BaseSyncErrorInvoiceMessage data,
            Domain.Model.Order existOrder,
            Invoice invoice,
            long invoiceId,
            Domain.Model.OmniChannel channel,
            LogObjectMicrosoftExtension loggerExtension,
            ExecutionContext context,
            KvInternalContext coreContext,
            byte channelType,
            string connectStringName,
            byte? oldInvoiceStatus = null)
        {
            var kvInternalContext = new KvInternalContext
            {
                LogId = data.LogId,
                BranchId = invoice.BranchId,
                RetailerId = context.RetailerId,
                RetailerCode = context.RetailerCode,
                UserId = context.User?.Id ?? 0,
                GroupId = context.GroupId
            };

            var platform = await _omniChannelPlatformService.GetById(channel.PlatformId);
            var omniChannelSettings = await _omniChannelSettingService.GetChannelSettings(data.ChannelId, data.RetailerId);

            #region Get lastest order status

            var client = _channelClient.CreateClient(channelType, context.GroupId, context.RetailerId);
            var authKey = await GetAuthByChannel(channelType, channel, data.LogId, context);

            var (orderStatus, invoiceStatus, deliveryStatus) = await client.GetOrderStatus(context.RetailerId,
                invoice.ChannelId, data.LogId, invoice.ChannelOrderId, authKey, loggerExtension, true, platform, omniChannelSettings);
            #endregion

            #region Void order KV (Hủy đơn)
            if (orderStatus == (byte)OrderState.Void && oldInvoiceStatus == null &&
                ((channelType != (byte)Sdk.Common.ChannelType.Shopee && channelType != (byte)Sdk.Common.ChannelType.Tiktok && channelType != (byte)Sdk.Common.ChannelType.Tiki) || deliveryStatus != (byte)DeliveryStatus.Returned))
            {
                await VoidOrderHandle(data, invoice, coreContext, channelType, context, existOrder, kvInternalContext);
            }
            #endregion

            #region Create invoice KV
            DeliveryInfo existDelivery = null;
            if (invoiceId == 0)
            {
                // Tạo hóa đơn
                await HandleCreateInvoice(data, existOrder, invoice, loggerExtension, channelType, platform, omniChannelSettings, client, authKey, coreContext, kvInternalContext, connectStringName, invoiceStatus, deliveryStatus);
            }
            else
            {
                existDelivery = await _deliveryInfoInternalClient.GetLastByInvoiceId(coreContext, invoiceId);
            }
            #endregion

            #region Update delivery status

            var oldDeliveryStatus = existDelivery?.Status;
            if (existDelivery != null && deliveryStatus.HasValue && oldDeliveryStatus != deliveryStatus.Value)
            {
                await _invoiceInternalClient.UpdateStatus(kvInternalContext,
                    existDelivery.ConvertTo<DeliveryInfo>(), deliveryStatus.Value);
            }
            #endregion

            #region AuditTrail
            await WriteLogForUpdateInvoice(kvInternalContext, channelType, invoice.Code, oldInvoiceStatus, invoiceStatus ?? invoice.Status, oldDeliveryStatus, deliveryStatus);
            #endregion
        }
        /// <summary>
        /// Tạo hóa đơn
        /// </summary>
        /// <param name="data"></param>
        /// <param name="existOrder"></param>
        /// <param name="invoice"></param>
        /// <param name="loggerExtension"></param>
        /// <param name="channelType"></param>
        /// <param name="platform"></param>
        /// <param name="omniChannelSettings"></param>
        /// <param name="client"></param>
        /// <param name="authKey"></param>
        /// <param name="coreContext"></param>
        /// <param name="kvInternalContext"></param>
        /// <param name="connectStringName"></param>
        /// <param name="invoiceStatus"></param>
        /// <param name="deliveryStatus"></param>
        /// <returns></returns>
        private async Task HandleCreateInvoice(BaseSyncErrorInvoiceMessage data,
            Domain.Model.Order existOrder,
            Invoice invoice,
            LogObjectMicrosoftExtension loggerExtension,
            byte channelType,
            Platform platform,
            OmniChannelSettingObject omniChannelSettings,
            IBaseClient client,
            ChannelAuth authKey,
            KvInternalContext coreContext,
            KvInternalContext kvInternalContext,
            string connectStringName,
            byte? invoiceStatus,
            byte? deliveryStatus)
        {
            #region PriceBook (Bảng giá) & SaleChannel (Kênh bán)
            Domain.Model.PriceBook priceBook = await GetPriceBookById(invoice, connectStringName);

            // Xét bảng giá
            SetExtraInvoice(invoice, existOrder, priceBook);

            // Xét kênh bán
            var invoiceInsert = invoice.ConvertTo<OmniChannelCore.Api.Sdk.Models.Invoice>();
            invoiceInsert.SaleChannelId = existOrder.SaleChannelId;
            #endregion

            #region Get order Detail
            var deliveryInfo = await _deliveryInfoInternalClient.GetLastByOrderId(coreContext, existOrder.Id);
            var orderInfoLatest = await GetOrderDetailLatest(data, invoice, client, authKey, loggerExtension, platform, omniChannelSettings, kvInternalContext);

            // Update fee Tiktok vao Order
            await UpdateFeeDeliveryInOrder(connectStringName, coreContext, channelType, deliveryInfo?.DeliveryInfoId, orderInfoLatest.OrderDelivery.Price, orderInfoLatest.OrderDelivery.FeeJson);

            // Xử lý phụ phí hóa đơn
            await InvoiceSurChargesHandle(data, invoice, coreContext, invoiceInsert);
            #endregion

            #region Customer
            // Xử lý khách hàng
            await CustomerHandle(data, invoice, loggerExtension, connectStringName, coreContext, existOrder, deliveryInfo, orderInfoLatest);
            #endregion

            // Set product kv (Chỉ Lazada cho phép mapping SKU)
            List<ProductMapping> mappings = await HandleProductMapping(data, loggerExtension, invoice, channelType, platform, omniChannelSettings, client, authKey, deliveryInfo, orderInfoLatest);

            var invoiceProductIds = invoice.InvoiceDetails.Where(p => p.ProductId > 0).Select(p => p.ProductId).ToList();
            List<ProductBranchDTO> kvProducts = await GetProductBranchByIds(connectStringName, data.RetailerId, data.BranchId, invoiceProductIds);

            //kiểm tra active tính năng tự động đồng bộ hàng lô date
            //trả về danh sách chi tiết hóa đơn
            List<Domain.Model.ProductBatchExpireDto> productBatchExPires = await GetProductBatchExpire(data, connectStringName, invoiceProductIds);
            if (omniChannelSettings.IsAutoSyncBatchExpire)
            {
                var invoiceDetails = invoice.InvoiceDetails.Map(x => x.ConvertTo<ChannelClient.Models.InvoiceDetail>());
                var invoiceDetailsCopy = ConvertHelper.DeepCopy(invoiceDetails);
                invoiceDetails = DetectInvoiceDetailHasBatchExpire(kvProducts, invoiceDetails, productBatchExPires);
                var (isValidAutoSyncBatchExpire, errorMessage, productLog) = _createInvoiceDomainService.ValidateAutoSyncBatchExpire(invoiceDetailsCopy, invoiceDetails);
                if (!isValidAutoSyncBatchExpire)
                {
                    return;
                }
                invoice.InvoiceDetails = invoiceDetails.Map(o => o.ConvertTo<MongoDb.InvoiceDetail>());
            }

            // Validate Tạo đơn
            var isValidateProduct = await ValidateInvoiceProduct(connectStringName,
                invoice, loggerExtension, data.RetailerId, invoice.BranchId, invoice.ChannelId,
                invoice.ChannelOrderId, channelType, existOrder, coreContext, mappings);
            if (!isValidateProduct) return;

            invoice.InvoiceDetails = _createInvoiceDomainService.SplitOrderDetails(kvProducts, invoice.InvoiceDetails);

            var products = kvProducts.Select(x => x.ConvertTo<OmniChannelCore.Api.Sdk.Models.ProductBrachDto>()).ToList();
            invoiceInsert.InvoiceWarranties = await _createInvoiceDomainService.GetInvoiceWarranties(
                kvInternalContext, products, invoice.InvoiceDetails, invoice.PurchaseDate);

            var invoiceDetailsInsert = invoice.InvoiceDetails.Select(x => x.ConvertTo<InvoiceDetail>()).ToList();
            invoiceDetailsInsert.ForEach(x => x.UpdatePropetiesUseWarranty(invoiceInsert.InvoiceWarranties));

            invoiceInsert.UpdatePuseChaseDate(invoiceInsert.PurchaseDate, existOrder.PurchaseDate, data.CurrentSaleTime);

            if (invoiceStatus.HasValue) invoiceInsert.Status = invoiceStatus.Value;

            if (deliveryStatus.HasValue && invoiceInsert.DeliveryDetail != null)
            {
                invoiceInsert.DeliveryDetail.Status = deliveryStatus.Value;
                invoiceInsert.DeliveryDetail.LatestStatus = deliveryStatus.Value;
            }

            // CreateInvoice KV
            long invoiceId = await _invoiceInternalClient.CreateInvoice(kvInternalContext, invoiceInsert, invoiceDetailsInsert,
                invoice.DeliveryDetail.ConvertTo<InvoiceDelivery>());
            if (invoiceId == 0)
            {
                var ex = new OmniException("Tạo hóa đơn không thành công");
                loggerExtension.LogError(ex);
                throw ex;
            }
            var existDelivery = await _deliveryInfoInternalClient.GetLastByInvoiceId(coreContext, invoiceId);

            #region Logs create invoice
            await WriteLogCreateNewInvoiceSuccess(
                invoice, channelType, existOrder, invoiceInsert,
                existDelivery, deliveryStatus, coreContext, priceBook,
                productBatchExPires, invoiceInsert.InvoiceWarranties);

            #endregion
        }
        /// <summary>
        /// Mapping hàng hóa
        /// </summary>
        /// <param name="data"></param>
        /// <param name="loggerExtension"></param>
        /// <param name="invoice"></param>
        /// <param name="channelType"></param>
        /// <param name="platform"></param>
        /// <param name="omniChannelSettings"></param>
        /// <param name="client"></param>
        /// <param name="authKey"></param>
        /// <param name="deliveryInfo"></param>
        /// <param name="orderInfoLatest"></param>
        /// <returns></returns>
        private async Task<List<ProductMapping>> HandleProductMapping(BaseSyncErrorInvoiceMessage data,
            LogObjectMicrosoftExtension loggerExtension,
            Invoice invoice,
            byte channelType,
            Platform platform,
            OmniChannelSettingObject omniChannelSettings,
            IBaseClient client,
            ChannelAuth authKey,
            DeliveryInfoForOrder deliveryInfo,
            KvOrder orderInfoLatest)
        {
            invoice.InvoiceDetails = await GetAndSetMongoInvoiceDetailAgain(data,
                channelType, invoice, deliveryInfo, authKey, loggerExtension, orderInfoLatest, client,
                omniChannelSettings, platform);

            var channelProductIds = invoice.InvoiceDetails.Where(p => !string.IsNullOrEmpty(p.CommonProductChannelId)).Select(x => x.CommonProductChannelId).ToList();

            List<ProductMapping> mappings;

            // Set product kv (Chỉ Lazada cho phép mapping SKU)
            if (channelType == (byte)Sdk.Common.ChannelType.Lazada)
            {
                if (channelProductIds.Count > 0)
                {
                    mappings = await _productMappingService.GetByChannelProductIds(data.RetailerId, data.ChannelId, channelProductIds, isStringItemId: ConvertHelper.CheckUseStringItemId(channelType));
                }
                else
                {
                    var productSkus = invoice.InvoiceDetails.Where(p => !string.IsNullOrEmpty(p.ProductChannelSku)).Select(p => p.ProductChannelSku).ToList();
                    mappings = await _productMappingService.GetByChannelProductSkus(data.RetailerId, data.ChannelId, productSkus);
                }

                foreach (var detail in invoice.InvoiceDetails)
                {
                    var mapping = mappings?.FirstOrDefault(x => (!string.IsNullOrEmpty(detail.CommonProductChannelId) && x.CommonProductChannelId == detail.CommonProductChannelId) || x.ProductChannelSku.Equals(detail.ProductChannelSku));

                    detail.ProductId = mapping?.ProductKvId ?? 0;
                    detail.ProductCode = mapping?.ProductKvSku ?? string.Empty;
                }
            }
            else
            {
                mappings = await _productMappingService.GetByChannelProductIds(data.RetailerId, data.ChannelId, channelProductIds, isStringItemId: ConvertHelper.CheckUseStringItemId(channelType));

                foreach (var detail in invoice.InvoiceDetails)
                {
                    var mapping = mappings?.FirstOrDefault(x => !string.IsNullOrEmpty(detail.CommonProductChannelId) && x.CommonProductChannelId == detail.CommonProductChannelId);

                    detail.ProductId = mapping?.ProductKvId ?? 0;
                    detail.ProductCode = mapping?.ProductKvSku ?? string.Empty;
                }
            }

            return mappings;
        }
        /// <summary>
        /// Phụ phí hóa đơn
        /// </summary>
        /// <param name="data"></param>
        /// <param name="invoice"></param>
        /// <param name="coreContext"></param>
        /// <param name="invoiceInsert"></param>
        /// <returns></returns>
        private async Task InvoiceSurChargesHandle(BaseSyncErrorInvoiceMessage data,
            Invoice invoice,
            KvInternalContext coreContext,
            OmniChannelCore.Api.Sdk.Models.Invoice invoiceInsert)
        {
            var lstInvoiceSurcharge = new List<OmniChannelCore.Api.Sdk.Models.InvoiceSurCharges>();
            if (invoice.InvoiceSurCharges != null && invoice.InvoiceSurCharges.Any())
            {
                foreach (var itemSurCharge in invoice.InvoiceSurCharges)
                {
                    var invoiceSurcharge = new OmniChannelCore.Api.Sdk.Models.InvoiceSurCharges();
                    var surchange =
                        await _surChargeInternalClient.GetSurChargeByName(coreContext, itemSurCharge.Name);
                    if (surchange != null && !string.IsNullOrEmpty(surchange.Code))
                    {
                        //create surcharge for Order
                        invoiceSurcharge = new OmniChannelCore.Api.Sdk.Models.InvoiceSurCharges()
                        {
                            SurchargeId = surchange.Id,
                            Price = (Decimal)itemSurCharge.Price,
                            RetailerId = data.RetailerId,
                            Name = surchange.Name,
                            SurValue = (Decimal)itemSurCharge.Price
                        };
                        lstInvoiceSurcharge.Add(invoiceSurcharge);
                    }
                }

                invoiceInsert.InvoiceSurCharges = lstInvoiceSurcharge;
                invoiceInsert.Surcharge = lstInvoiceSurcharge.Sum(x => x.Price);
            }
        }
        /// <summary>
        /// Hủy đơn hàng
        /// </summary>
        /// <param name="data"></param>
        /// <param name="invoice"></param>
        /// <param name="coreContext"></param>
        /// <param name="channelType"></param>
        /// <param name="context"></param>
        /// <param name="existOrder"></param>
        /// <param name="kvInternalContext"></param>
        /// <returns></returns>
        private async Task VoidOrderHandle(BaseSyncErrorInvoiceMessage data,
            Invoice invoice,
            KvInternalContext coreContext,
            byte channelType,
            ExecutionContext context,
            Domain.Model.Order existOrder,
            KvInternalContext kvInternalContext)
        {
            await _orderInternalClient.VoidOrder(kvInternalContext, existOrder.Id);
            var logOrder = new AuditTrailLog
            {
                FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channelType),
                Action = (int)AuditTrailAction.OrderIntergate,
                CreatedDate = DateTime.Now,
                BranchId = data.BranchId,
                Content = $"Hủy đơn đặt hàng: [OrderCode]{existOrder.Code}[/OrderCode]"
            };
            await _auditTrailInternalClient.AddLogAsync(kvInternalContext, logOrder);
            var logVoidOrder = new AuditTrailLog
            {
                FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channelType),
                Action = (int)AuditTrailAction.InvoiceIntergate,
                CreatedDate = DateTime.Now,
                BranchId = context.BranchId
            };
            logVoidOrder.Content = $"Tạo hóa đơn KHÔNG thành công: [InvoiceCode]{invoice.Code}[/InvoiceCode] (cho đơn đặt hàng: [OrderCode]{existOrder.Code}[/OrderCode]), " +
                                   $"lý do: đơn đặt hàng đã hủy trên {Enum.GetName(typeof(ChannelType), channelType)}";
            await _auditTrailInternalClient.AddLogAsync(coreContext, logVoidOrder);
            return;
        }
        /// <summary>
        /// Xử lý tạo khách hàng
        /// </summary>
        /// <param name="data"></param>
        /// <param name="invoice"></param>
        /// <param name="loggerExtension"></param>
        /// <param name="connectStringName"></param>
        /// <param name="coreContext"></param>
        /// <param name="existOrder"></param>
        /// <param name="deliveryInfo"></param>
        /// <param name="orderInfoLatest"></param>
        /// <returns></returns>
        private async Task CustomerHandle(BaseSyncErrorInvoiceMessage data,
            Invoice invoice, LogObjectMicrosoftExtension loggerExtension,
            string connectStringName, KvInternalContext coreContext,
            Domain.Model.Order existOrder,
            DeliveryInfoForOrder deliveryInfo,
            KvOrder orderInfoLatest)
        {
            if (ValidateCreateCustomer(data.ChannelType) && existOrder?.Status == (byte)OrderState.Draft)
            {
                try
                {
                    if (deliveryInfo != null
                        && orderInfoLatest?.OrderDelivery != null)
                    {
                        if (deliveryInfo.Receiver != orderInfoLatest.OrderDelivery.Receiver ||
                            deliveryInfo.ContactNumber != orderInfoLatest.OrderDelivery.ContactNumber ||
                            deliveryInfo.Address != orderInfoLatest.OrderDelivery.Address ||
                            deliveryInfo.WardName != orderInfoLatest.OrderDelivery.WardName ||
                            deliveryInfo.LocationName != orderInfoLatest.OrderDelivery.LocationName)
                        {
                            var isChangedPhone = PhoneNumberHelper.GetPerfectContactNumber(deliveryInfo.ContactNumber) != PhoneNumberHelper.GetPerfectContactNumber(orderInfoLatest.OrderDelivery.ContactNumber);

                            var dpDataChanged = new OmniChannelCore.Api.Sdk.Models.DeliveryPackage
                            {
                                RetailerId = existOrder.RetailerId,
                                OrderId = existOrder.Id,
                                Receiver = orderInfoLatest.OrderDelivery.Receiver,
                                ContactNumber = orderInfoLatest.OrderDelivery.ContactNumber,
                                Address = orderInfoLatest.OrderDelivery.Address,
                                WardName = orderInfoLatest.OrderDelivery.WardName,
                                LocationName = orderInfoLatest.OrderDelivery.LocationName,
                            };
                            var dpConditionChanged = new DeliveryPackageConditionUpdate { OrderId = existOrder.Id };
                            var fieldChangeds = new[] { "Receiver", "ContactNumber", "Address", "WardName", "LocationName" };

                            var dpUpdatedRow = await _deliveryPackageInternalClient.Update(coreContext, dpDataChanged, dpConditionChanged, fieldChangeds);

                            if (invoice.DeliveryDetail != null)
                            {
                                invoice.DeliveryDetail.Receiver = orderInfoLatest.OrderDelivery.Receiver;
                                invoice.DeliveryDetail.ContactNumber = orderInfoLatest.OrderDelivery.ContactNumber;
                                invoice.DeliveryDetail.Address = orderInfoLatest.OrderDelivery.Address;
                                invoice.DeliveryDetail.WardName = orderInfoLatest.OrderDelivery.WardName;
                                invoice.DeliveryDetail.LocationName = orderInfoLatest.OrderDelivery.LocationName;
                            }

                            long? newOrderCustomerId;
                            int orderUpdatedCusRow = 0;

                            //Kiểm tra số điện thoại có hợp lệ. 
                            var phone = PhoneNumberHelper.GetPerfectContactNumber(orderInfoLatest.CustomerPhone);
                            var isValidPhone = PhoneNumberHelper.IsValidContactNumber(phone, RegionCode.VN) || data.ChannelType != (byte)ChannelTypeEnum.Shopee;
                            // Update OrderCustomer
                            if (dpUpdatedRow > 0 && isChangedPhone && isValidPhone)
                            {
                                using (var cli = _kvLockRedis.GetLockFactory())
                                {
                                    var lockAddCustomerKey = phone;
                                    if (string.IsNullOrEmpty(phone))
                                    {
                                        lockAddCustomerKey = orderInfoLatest.Code;
                                    }
                                    using (cli.AcquireLock(string.Format(KvConstant.LockAddCustomer, lockAddCustomerKey), TimeSpan.FromSeconds(60)))
                                    {
                                        PosSetting posSetting = null;
                                        using (var db = await _dbConnectionFactory.OpenAsync(connectStringName.ToLower()))
                                        {
                                            var posSettingRepository = new PosSettingRepository(db);
                                            posSetting = new PosSetting(await posSettingRepository.GetSettingAsync(data.RetailerId), data.RetailerId, _dbConnectionFactory, _cacheClient, _settings);
                                        }

                                        Domain.Model.Customer existsCustomer = await GetCustomer(data, connectStringName, phone, posSetting);
                                        if (existsCustomer == null)
                                        {
                                            try
                                            {
                                                var customerInsert = new Demo.OmniChannelCore.Api.Sdk.Models.Customer
                                                {
                                                    ContactNumber = phone,
                                                    Name = orderInfoLatest.CustomerName,
                                                    Address = orderInfoLatest.CustomerAddress,
                                                    BranchId = data.BranchId,
                                                    RetailerId = data.RetailerId
                                                };
                                                if (!string.IsNullOrEmpty(orderInfoLatest.CustomerLocation))
                                                {
                                                    customerInsert.LocationName = orderInfoLatest.CustomerLocation;
                                                }

                                                if (!string.IsNullOrEmpty(orderInfoLatest.CustomerWard))
                                                {
                                                    customerInsert.WardName = orderInfoLatest.CustomerWard;
                                                }

                                                newOrderCustomerId = await _customerInternalClient.CreateCustomerAsync(coreContext, customerInsert);
                                            }
                                            catch (Exception ex)
                                            {
                                                newOrderCustomerId = existsCustomer?.Id;
                                                loggerExtension.Description = "Fail update customer --> ignore";
                                                loggerExtension.LogError(ex);
                                            }
                                        }
                                        else
                                        {
                                            newOrderCustomerId = existsCustomer?.Id;
                                        }
                                    }

                                    if (newOrderCustomerId != null && newOrderCustomerId > 0)
                                    {
                                        orderUpdatedCusRow = await _orderInternalClient.UpdateCustomer(coreContext, existOrder.Id, newOrderCustomerId.Value);
                                        invoice.CustomerId = newOrderCustomerId.Value;
                                    }
                                }
                            }

                            loggerExtension.Description = $"Success";
                            loggerExtension.RequestObject = $"{new { OrderId = existOrder.Id, DpDataChanged = dpDataChanged, DpConditionChanged = dpConditionChanged, FieldChangeds = fieldChangeds }.ToSafeJson()}";
                            loggerExtension.ResponseObject = $"{new { DeliveryPackageUpdatedRow = dpUpdatedRow, IsChangedPhone = isChangedPhone, OrderUpdatedCusRow = orderUpdatedCusRow }.ToSafeJson()}";
                            loggerExtension.LogInfo();
                        }
                    }
                }
                catch (Exception e)
                {
                    loggerExtension.Description = "Fail update delivery --> ignore";
                    loggerExtension.LogError(e);
                }
            }
        }

        private async Task<Domain.Model.PriceBook> GetPriceBookById(Invoice invoice, string connectStringName)
        {
            Domain.Model.PriceBook priceBook = null;
            using (var db = await _dbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var priceBookRepository = new PriceBookRepository(db);
                priceBook = await priceBookRepository.GetById(invoice.RetailerId, invoice.PriceBookId);
            }

            return priceBook;
        }

        private async Task<List<Domain.Model.ProductBatchExpireDto>> GetProductBatchExpire(BaseSyncErrorInvoiceMessage data,
            string connectStringName,
            List<long> invoiceProductIds)
        {
            List<Domain.Model.ProductBatchExpireDto> productBatchExPires = null;
            using (var db = await _dbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var productRepository = new ProductRepository(db);
                productBatchExPires = await productRepository.GetProductBatchExpireByBranch(data.RetailerId, data.BranchId, invoiceProductIds);
            }

            return productBatchExPires;
        }

        private async Task<List<ProductBranchDTO>> GetProductBranchByIds(string connectStringName,
            int retailerId,
            int branchId,
            List<long> invoiceProductIds)
        {
            List<ProductBranchDTO> kvProducts = new List<ProductBranchDTO>();
            using (var db = await _dbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var productRepository = new ProductRepository(db);
                kvProducts = await productRepository.GetProductByIds(retailerId, branchId, invoiceProductIds, isLogMonitor: false);
            }

            return kvProducts;
        }

        private List<ChannelClient.Models.InvoiceDetail> DetectInvoiceDetailHasBatchExpire(
             List<ProductBranchDTO> productBranchDtos,
             List<ChannelClient.Models.InvoiceDetail> kvInvoiceDetails,
             List<Domain.Model.ProductBatchExpireDto> productBatchExpireDtos)
        {
            if (productBatchExpireDtos == null || !productBatchExpireDtos.Any()) return kvInvoiceDetails;

            var invoiceSplitedDetails = new List<ChannelClient.Models.InvoiceDetail>();
            foreach (var invoiceDetail in kvInvoiceDetails)
            {
                var productId = invoiceDetail.ProductId;
                var productItem = productBranchDtos?.FirstOrDefault(p => p.Id == productId);
                if (productItem == null) continue;
                if (!productItem.IsBatchExpireControl.GetValueOrDefault()) { invoiceSplitedDetails.Add(invoiceDetail); continue; }
                if (productItem.MasterUnitId > 0 && !productItem.IsHasVariantsAndSingleUnit())
                {
                    productId = productItem.MasterUnitId ?? 0;
                }

                invoiceDetail.IsBatchExpireControl = true;
                var invoiceDetails = SplitProductBatchExpireInvoiceDetail(
                    invoiceDetail,
                    productBatchExpireDtos,
                    productItem.ConversionValue,
                    productId);
                invoiceSplitedDetails.AddRange(invoiceDetails);
            }

            return invoiceSplitedDetails;
        }
        private static List<ChannelClient.Models.InvoiceDetail> SplitProductBatchExpireInvoiceDetail(
        ChannelClient.Models.InvoiceDetail invoiceDetail,
        List<Domain.Model.ProductBatchExpireDto> productBatchExpireDtos,
        double conversionValue,
        long productId)
        {
            var invoiceDetails = new List<ChannelClient.Models.InvoiceDetail>();
            var remainedQuantity = invoiceDetail.Quantity;
            foreach (var productBatchExpire in productBatchExpireDtos)
            {
                if (productBatchExpire.ProductId != productId)
                    continue;
                var onhand = Math.Round(productBatchExpire.OnHand / conversionValue, 3);
                // KOL-9082
                if (onhand >= 0.001)
                {
                    invoiceDetail.ProductBatchExpireId = productBatchExpire.Id;
                    invoiceDetail.Quantity = remainedQuantity;
                    invoiceDetails.Add(invoiceDetail);

                    remainedQuantity = remainedQuantity - onhand;
                    if (remainedQuantity <= 0)      // onhand > remainedQuantity -> stop
                    {
                        break;
                    }
                    invoiceDetail.Quantity = onhand;    // onhand < remainedQuantity -> take all onhand then add new batch
                                                        // add new batch
                    invoiceDetail = invoiceDetail.CreateCopy();
                }
            }

            return invoiceDetails;
        }
        /// <summary>
        /// Dữ liệu khách hàng
        /// </summary>
        /// <param name="data"></param>
        /// <param name="connectStringName"></param>
        /// <param name="phone"></param>
        /// <param name="posSetting"></param>
        /// <returns></returns>
        private async Task<Domain.Model.Customer> GetCustomer(BaseSyncErrorInvoiceMessage data, string connectStringName, string phone, PosSetting posSetting)
        {
            Domain.Model.Customer existsCustomer = null;
            using (var db = await _dbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var customerRepository = new CustomerRepository(db);
                existsCustomer = await customerRepository.GetByFilterAsync(data.RetailerId, data.BranchId, phone, null, posSetting.ManagerCustomerByBranch);
            }

            return existsCustomer;
        }
        /// <summary>
        /// Lấy đơn hàng
        /// </summary>
        /// <param name="invoice"></param>
        /// <param name="channelType"></param>
        /// <param name="connectStringName"></param>
        /// <returns></returns>
        private async Task<Domain.Model.Order> GetOrderByCode(Invoice invoice, byte channelType, string connectStringName)
        {
            Domain.Model.Order existOrder = null;
            using (var db = await _dbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var orderRepository = new OrderRepository(db);
                var orderPrefix = ChannelTypeHelper.GetOrderPrefix(channelType);
                existOrder = await orderRepository.GetOrderByCodeAsync($"{orderPrefix}_{invoice.ChannelOrderId}", invoice.RetailerId);
            }

            return existOrder;
        }
        /// <summary>
        /// Xét Bảng giá
        /// </summary>
        /// <param name="invoice"></param>
        /// <param name="existOrder"></param>
        /// <param name="priceBook"></param>
        private void SetExtraInvoice(Invoice invoice, Domain.Model.Order existOrder, Domain.Model.PriceBook priceBook)
        {
            invoice.PriceBookId = existOrder.Extra?.FromJson<ChannelClient.Models.Extra>().PriceBookId?.Id ?? 0;

            if (invoice.PriceBookId > 0)
            {
                if (priceBook == null || !priceBook.IsActive || priceBook.isDeleted == true ||
                    priceBook.EndDate < DateTime.Now)
                    invoice.PriceBookId = 0;
            }
        }
        private async Task<ChannelAuth> GetAuthByChannel(byte? channelType, Domain.Model.OmniChannel channel, Guid logId, ShareKernel.Auth.ExecutionContext context)
        {

            var channelAuth = await _channelAuthService.GetChannelAuth(channel, logId);

            if (channelAuth == null || (channelType == (byte)Sdk.Common.ChannelType.Lazada && string.IsNullOrEmpty(channelAuth.AccessToken)))
            {
                return null;
            }
            return new ChannelAuth
            {
                Id = channelAuth.Id,
                AccessToken = channelAuth.AccessToken,
                ShopId = channelAuth.ShopId
            };

        }

        /// <summary>
        /// Write log create new invoice success
        /// </summary>
        /// <param name="invoice"></param>
        /// <param name="db"></param>
        /// <param name="channelType"></param>
        /// <param name="existOrder"></param>
        /// <param name="invoiceInsert"></param>
        /// <param name="existDelivery"></param>
        /// <param name="deliveryStatus"></param>
        /// <param name="coreContext"></param>
        /// <param name="priceBook"></param>
        /// <returns></returns>
        private async Task WriteLogCreateNewInvoiceSuccess(
            Invoice invoice,
            byte channelType,
            Domain.Model.Order existOrder,
            OmniChannelCore.Api.Sdk.Models.Invoice invoiceInsert,
            DeliveryInfo existDelivery,
            byte? deliveryStatus,
            KvInternalContext coreContext,
            Domain.Model.PriceBook priceBook,
            List<Domain.Model.ProductBatchExpireDto> productBatchExPires,
            List<OmniChannelCore.Api.Sdk.Models.InvoiceWarranties> invoiceWarranties)
        {
            var productDetail = new StringBuilder();
            string batchExPire = string.Empty;
            double total = 0;
            if (invoice.InvoiceDetails != null && invoice.InvoiceDetails.Any())
            {
                productDetail.Append(", bao gồm:<div>");
                foreach (var inv in invoice.InvoiceDetails)
                {
                    if (productBatchExPires != null && productBatchExPires.Any())
                    {
                        var productBatchExPire = productBatchExPires.FirstOrDefault(x => x.ProductId == inv.ProductId);
                        if (productBatchExPire != null) batchExPire = $" , {productBatchExPire.BatchName} - Hạn sử dụng: {productBatchExPire.ExpireDate.ToString("dd/MM/yyyy")}";

                    }
                    total += ((double)inv.Price - (double)(inv.Discount ?? 0)) * inv.Quantity;
                    productDetail.Append(
                        $"- [ProductCode]{inv.ProductCode}[/ProductCode] : {StringHelper.Normallize(inv.Quantity)}*{StringHelper.NormallizeWfp((double)inv.Price - (double)(inv.Discount ?? 0))}{batchExPire}<br>");
                    var productWarrantys = invoiceWarranties?.Where(x => x.InvoiceDetailUuid == inv.Uuid).ToList();
                    var getWarrantys = GetWarrantysLog(inv, productWarrantys);
                    productDetail.Append(string.IsNullOrEmpty(getWarrantys.ToString()) ? string.Empty : $"{getWarrantys}<br>");
                }

                productDetail.Append("</div>");
            }

            var priceBookName = "Bảng giá chung";

            priceBookName = priceBook?.Name ?? priceBookName;
            var log = new AuditTrailLog
            {
                FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channelType),
                Action = (int)AuditTrailAction.InvoiceIntergate,
                CreatedDate = DateTime.Now,
                BranchId = invoice.BranchId,
                Content =
                    $"Tạo hóa đơn V2 [InvoiceCode]{invoice.Code}[/InvoiceCode] (cho đơn đặt hàng [OrderCode]{existOrder.Code}[/OrderCode])," +
                    $"Bảng giá: {priceBookName}, giá trị: {StringHelper.NormallizeWfp(total)}, thời gian: {invoice.PurchaseDate:dd/MM/yyyy HH:mm:ss}, " +
                    $"trạng thái: {EnumHelper.ToDescription((InvoiceState)invoiceInsert.Status)}, trạng thái giao: {EnumHelper.ToDescription((DeliveryStatus)(existDelivery?.Status ?? deliveryStatus ?? 0))}{productDetail}"
            };
            await _auditTrailInternalClient.AddLogAsync(coreContext, log);
        }

        private async Task WriteLogForUpdateInvoice(KvInternalContext context, byte? channelType, string invoiceCode, byte? oldInvoiceStatus, byte newInvoiceStatus, byte? oldDeliveryStatus, byte? newDeliveryStatus)
        {
            var contentLog = new StringBuilder($"Đồng bộ trạng thái hóa đơn ([InvoiceCode]{invoiceCode}[/InvoiceCode]), vận đơn thành công: <br/>");
            bool isAddLog = false;
            if (oldInvoiceStatus.HasValue && oldInvoiceStatus != newInvoiceStatus && newInvoiceStatus != (byte)InvoiceState.Issued)
            {
                contentLog.AppendFormat($"- Hóa đơn: Trạng thái: {EnumHelper.ToDescription((InvoiceState)oldInvoiceStatus)} -> {EnumHelper.ToDescription((InvoiceState)newInvoiceStatus)} <br/>");
                isAddLog = true;
            }
            if (oldDeliveryStatus.HasValue && newDeliveryStatus.HasValue && oldDeliveryStatus != newDeliveryStatus)
            {
                contentLog.AppendFormat($"- Vận đơn: Trạng thái: {EnumHelper.ToDescription((DeliveryStatus)oldDeliveryStatus)} -> {EnumHelper.ToDescription((DeliveryStatus)newDeliveryStatus)} <br/>");
                isAddLog = true;
            }

            if (isAddLog)
            {
                var log = new AuditTrailLog
                {
                    FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channelType),
                    Action = (int)AuditTrailAction.InvoiceIntergate,
                    CreatedDate = DateTime.Now,
                    BranchId = context.BranchId
                };
                log.Content = contentLog.ToString();
                await _auditTrailInternalClient.AddLogAsync(context, log);
            }
        }

        private async Task<bool> ValidateInvoiceProduct(string connectStringName, Invoice invoice, LogObjectMicrosoftExtension loggerExtension, int retailerId, int branchId, long channelId, string orderId, byte channelType, Domain.Model.Order order, KvInternalContext context, List<ProductMapping> mappings)
        {
            List<ProductBranchDTO> kvProducts = await GetProductBranchByIds(connectStringName, retailerId, branchId, invoice.InvoiceDetails.Select(p => p.ProductId).ToList());
            var productLog = new StringBuilder();
            var errorMessage = new StringBuilder();
            var isValid = true;
            foreach (var detail in invoice.InvoiceDetails)
            {
                var product = kvProducts.FirstOrDefault(x => x.Id == detail.ProductId);
                detail.ProductName = product?.FullName ?? detail.ProductChannelName;
                var channelTrackKey = !string.IsNullOrEmpty(detail.CommonProductChannelId) ? detail.CommonProductChannelId : detail.ProductChannelSku;
                if (product == null)
                {
                    detail.UseProductBatchExpire = false;
                    detail.UseProductSerial = false;
                    errorMessage.Append($"Hàng hóa {detail.ProductChannelName} ({channelTrackKey}) chưa liên kết với hàng hóa nào trên Demo; ");
                    productLog.AppendFormat($"- {detail.ProductChannelSku ?? detail.CommonProductChannelId} ({channelTrackKey}): chưa xác định trên Demo <br/>");
                    isValid = false;
                }
                else
                {
                    detail.UseProductBatchExpire = product.IsBatchExpireControl.HasValue ? product.IsBatchExpireControl.Value : false;
                    detail.UseProductSerial = product.IsLotSerialControl.HasValue ? product.IsLotSerialControl.Value : false;
                    var mapping = mappings.FirstOrDefault(x => x.ChannelId == channelId && ((!string.IsNullOrEmpty(detail.CommonProductChannelId) && x.CommonProductChannelId == detail.CommonProductChannelId) || x.ProductChannelSku == detail.ProductChannelSku));
                    if (mapping == null)
                    {
                        errorMessage.Append($"Hàng hóa {detail.ProductChannelName} ({channelTrackKey}) chưa liên kết với hàng hóa nào trên Demo; ");
                        productLog.AppendFormat($"- [ProductCode]{detail.ProductCode}[/ProductCode]: liên kết hàng hóa không tồn tại <br/>");
                        isValid = false;
                    }
                    else if (product.isDeleted == true || mapping == null)
                    {
                        errorMessage.Append($"Hàng hóa {detail.ProductChannelName} ({channelTrackKey}) chưa liên kết với hàng hóa nào trên Demo; ");
                        productLog.AppendFormat($"- [ProductCode]{detail.ProductCode}[/ProductCode]: đã bị xóa <br/>");
                        isValid = false;
                    }
                    else if (product.IsLotSerialControl == true && (string.IsNullOrEmpty(detail.SerialNumbers) || detail.SerialNumbers.Split(',').Count() < detail.Quantity))
                    {
                        productLog.AppendFormat($"- [ProductCode]{detail.ProductCode}[/ProductCode]: chưa nhập đủ số lượng cần giao <br/>");
                        errorMessage.Append($"{detail.ProductCode}: Bạn chưa nhập đủ số lượng cần giao; ");
                        isValid = false;
                    }
                    else if (product.IsBatchExpireControl == true && !detail.ProductBatchExpireId.HasValue)
                    {
                        productLog.AppendFormat($"- [ProductCode]{detail.ProductCode}[/ProductCode]: chưa nhập đủ số lượng cần giao <br/>");
                        errorMessage.Append($"{detail.ProductCode}: Bạn chưa nhập đủ số lượng cần giao; ");
                        isValid = false;
                    }
                }

                if (!detail.UseProductBatchExpire)
                {
                    detail.ProductBatchExpireId = null;
                }
                if (!detail.UseProductSerial)
                {
                    detail.SerialNumbers = null;
                }
            }

            if (!isValid)
            {
                await SaveSyncInvoiceError(context, invoice, order, loggerExtension, orderId, channelId, channelType, errorMessage.ToString(), productLog.ToString());
            }
            loggerExtension.LogInfo($"Retailer: {retailerId} - Channel: {channelId} - OrderId: {orderId} - Action: CreateInvoice error: {errorMessage}");
            string requestLog = $"Retailer: {retailerId} - Channel: {channelId} - OrderId: {orderId} - Action: CreateInvoice error: {errorMessage}";
            return isValid;
        }

        private async Task SaveSyncInvoiceError(KvInternalContext context, Invoice invoice, Domain.Model.Order order, LogObjectMicrosoftExtension loggerExtension, string channelOrderId, long channelId, byte channelType, string errorMessage, string auditTrailMessage)
        {
            loggerExtension.Action = ExceptionType.SaveSyncInvoiceError;
            loggerExtension.ResponseObject = $"Get Invoice Invoice Error";

            try
            {
                var existInvoice = await _invoiceMongoService.GetByInvoiceId(channelOrderId, context.BranchId, channelId, invoice.Code);
                if (existInvoice != null)
                {
                    existInvoice.ErrorMessage = errorMessage;
                    existInvoice.InvoiceDetails = invoice.InvoiceDetails;
                    await _invoiceMongoService.UpdateAsync(existInvoice.Id, existInvoice);
                }

                #region AuditTrail
                var log = new AuditTrailLog
                {
                    FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channelType),
                    Action = (int)AuditTrailAction.InvoiceIntergate,
                    CreatedDate = DateTime.Now,
                    BranchId = context.BranchId,
                    Content =
                        $"Tạo hóa đơn KHÔNG thành công: [InvoiceCode]{invoice.Code}[/InvoiceCode] (cho đơn đặt hàng [OrderCode]{order.Code}[/OrderCode]) lý do: <br/>{auditTrailMessage}"

                };
                await _auditTrailInternalClient.AddLogAsync(context, log);
            }
            catch (Exception e)
            {
                loggerExtension.LogError(e);
                throw;
            }


            #endregion
        }
        private async Task RemoveMongoRetryInvoice(string orderId, int branchId, long channelId, int retailerId)
        {
            var existRetryInvoice = await _retryInvoiceMongoService.GetByOrderId(orderId, channelId, branchId, retailerId);
            if (existRetryInvoice != null)
            {
                await _retryInvoiceMongoService.RemoveAsync(existRetryInvoice.Id);
            }
        }
        private async Task RemoveErrorInvoice(Invoice invoice)
        {
            if (invoice != null)
            {
                await _invoiceMongoService.RemoveAsync(invoice.Id);
            }
        }

        private async Task AddOrUpdateRetryInvoice(BaseSyncErrorInvoiceMessage data, Invoice invoice, Domain.Model.Order kvOrder, bool isDbException = false, string invoiceCode = null)
        {
            var existRetryInvoice =
                await _retryInvoiceMongoService.GetByOrderId(invoice.ChannelOrderId, data.ChannelId, data.BranchId,
                    data.RetailerId);
            if (existRetryInvoice != null)
            {
                existRetryInvoice.ModifiedDate = DateTime.Now;
                existRetryInvoice.RetryCount = 1;
                await _retryInvoiceMongoService.UpdateAsync(existRetryInvoice.Id, existRetryInvoice);
            }
            else
            {
                await _retryInvoiceMongoService.AddSync(new RetryInvoice
                {
                    ChannelId = data.ChannelId,
                    ChannelType = data.ChannelType,
                    BranchId = data.BranchId,
                    KvEntities = data.KvEntities,
                    RetailerId = data.RetailerId,
                    OrderId = invoice.ChannelOrderId,
                    Order = null,
                    KvOrder = kvOrder.ToSafeJson(),
                    KvOrderId = kvOrder.Id,
                    CreatedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    IsDbException = isDbException,
                    Code = invoiceCode,
                    RetryCount = 1
                });
            }
        }

        /// <summary>
        /// if products channel have exsist productId equal 0, we get data from client and convert to model mongo InvoiceDetail
        /// </summary>
        /// <param name="data"></param>
        /// <param name="channelType"></param>
        /// <param name="invoice"></param>
        /// <param name="deliveryInfo"></param>
        /// <param name="authKey"></param>
        /// <param name="loggerExtension"></param>
        /// <param name="orderInfoLatest"></param>
        /// <param name="client"></param>
        /// <returns></returns>
        private async Task<List<MongoDb.InvoiceDetail>> GetAndSetMongoInvoiceDetailAgain(
            BaseSyncErrorInvoiceMessage data, byte channelType,
            Invoice invoice, DeliveryInfoForOrder deliveryInfo,
            ChannelAuth authKey, LogObjectMicrosoftExtension loggerExtension,
            KvOrder orderInfoLatest, IBaseClient client, OmniChannelSettingObject setttings, Platform platform)
        {
            var channelProductIds = invoice.InvoiceDetails.Select(x => x.ProductChannelId).ToList();

            var isExistProductEqualZeroOrNull = channelProductIds.Any(x => x == null || x.Value == 0);

            if (isExistProductEqualZeroOrNull && channelType == (byte)Sdk.Common.ChannelType.Sendo)
            {
                var invoicesLatest = await GetListKvInvoiceByData(data, invoice, deliveryInfo, authKey, loggerExtension,
                    orderInfoLatest, client, setttings, platform);
                if (!invoicesLatest.Any()) return invoice.InvoiceDetails;
                var invoiceDetailFromClient = invoicesLatest.FirstOrDefault();

                var itemInvoiceDetail =
                    (from item in invoiceDetailFromClient?.InvoiceDetails ?? new List<ChannelClient.Models.InvoiceDetail>()
                     group item
                         by new
                         {
                             item.ProductChannelId,
                             item.ParentChannelProductId,
                             item.ProductChannelName,
                             item.ProductChannelSku,
                         }
                        into gr
                     select new
                     {
                         gr.Key.ProductChannelId,
                         gr.Key.ParentChannelProductId,
                         gr.Key.ProductChannelName,
                         gr.Key.ProductChannelSku
                     }).ToList();

                var invoiceDetails =
                    (from item in itemInvoiceDetail
                     from itemInvoiceOld in invoice.InvoiceDetails ?? new List<MongoDb.InvoiceDetail>()
                     where item.ProductChannelId == itemInvoiceOld.StrProductChannelId
                     select new MongoDb.InvoiceDetail
                     {
                         CommonProductChannelId = item.ProductChannelId,
                         CommonParentChannelProductId = item.ParentChannelProductId,
                         Price = itemInvoiceOld.Price,
                         Discount = itemInvoiceOld.Discount,
                         DiscountPrice = itemInvoiceOld.DiscountPrice,
                         DiscountRatio = itemInvoiceOld.DiscountRatio,
                         ProductBatchExpireId = itemInvoiceOld.ProductBatchExpireId,
                         ProductChannelName = item.ProductChannelName,
                         ProductCode = itemInvoiceOld.ProductCode,
                         ProductId = itemInvoiceOld.ProductId,
                         ProductChannelSku = item.ProductChannelSku,
                         ProductName = itemInvoiceOld.ProductName,
                         Quantity = itemInvoiceOld.Quantity,
                         SerialNumbers = itemInvoiceOld.SerialNumbers,
                         SubTotal = itemInvoiceOld.SubTotal,
                         UsePoint = itemInvoiceOld.UsePoint,
                         UseProductBatchExpire = itemInvoiceOld.UseProductBatchExpire,
                         UseProductSerial = itemInvoiceOld.UseProductSerial,
                         Note = itemInvoiceOld.Note,
                         ReturnQuantity = itemInvoiceOld.ReturnQuantity,
                         Uuid = StringHelper.GenerateUUID()
                     }).ToList();
                return invoiceDetails;
            }

            return invoice.InvoiceDetails;
        }

        protected virtual Task UpdateFeeDeliveryInOrder(string connectStringName, KvInternalContext coreContext, byte channelType, long? deliveryInfoId, decimal? price, string feeJson)
        {
            return Task.CompletedTask;
        }

        private async Task<KvOrder> GetOrderDetailLatest(BaseSyncErrorInvoiceMessage data, Invoice invoice, IBaseClient client,
            ChannelAuth authKey, LogObjectMicrosoftExtension loggerExtension, Platform platform,
            OmniChannelSettingObject settings,
            KvInternalContext kvInternalContext)
        {
            var orderDetailGetResponse = await client.GetOrderDetail(data.RetailerId, data.ChannelId, authKey,
                new CreateOrderRequest
                {
                    BranchId = data.BranchId,
                    OrderId = invoice.ChannelOrderId,
                    Order = string.Empty,
                    RetryCount = 0,
                    PriceBookId = invoice.PriceBookId,
                    GroupId = kvInternalContext.GroupId
                }, loggerExtension, platform, settings);

            return orderDetailGetResponse.Order;
        }

        private bool ValidateCreateCustomer(byte channelType)
        {
            if (channelType == (byte)ShareKernel.Common.ChannelType.Shopee || channelType == (byte)ShareKernel.Common.ChannelType.Tiktok)
            {
                return true;
            }
            return false;
        }

        private async Task<List<KvInvoice>> GetListKvInvoiceByData(
            BaseSyncErrorInvoiceMessage data, Invoice invoice, DeliveryInfoForOrder deliveryInfo,
            ChannelAuth authKey, LogObjectMicrosoftExtension loggerExtension,
            KvOrder orderInfoLatest,
            IBaseClient client,
            OmniChannelSettingObject settings,
            Platform platform)
        {
            (var isSuccess, var isVoidOrder, var isRetryInvoice, List<KvInvoice> invoicesLatest) = await client.GetInvoiceDetail(data.RetailerId, data.ChannelId, data.LogId, authKey,
                new CreateInvoiceRequest
                {
                    BranchId = data.BranchId,
                    OrderId = invoice.ChannelOrderId,
                    Order = invoice.ChannelOrder,
                    PriceBookId = 0,
                    KvOrder = orderInfoLatest,
                    KvDelivery = deliveryInfo.ConvertTo<DeliveryInfoDTO>(),
                    RetryCount = 3,
                    IsConfirmReturning = settings.IsConfirmReturning
                }, loggerExtension, platform, settings);

            if (isRetryInvoice)
            {
                await AddOrUpdateRetryInvoice(data, invoice, new Domain.Model.Order());

                loggerExtension.Description = "Is retry invoice";
                loggerExtension.LogInfo(true);
                return null;
            }

            if (!isSuccess) return null;

            return invoicesLatest ?? new List<KvInvoice>();
        }


        private StringBuilder GetWarrantysLog(OmniChannel.MongoDb.InvoiceDetail invoiceDetail,
            List<OmniChannelCore.Api.Sdk.Models.InvoiceWarranties> invoiceWarrantys)
        {
            var warrantysLog = new StringBuilder();

            var checkProductIsCombo = !invoiceWarrantys.Where(x => x.ProductId == invoiceDetail.ProductId).Any();
            var productWarrantysGroup = invoiceWarrantys.GroupBy(x => new { x.ProductId, x.ProductCode }).ToList();
            foreach (var group in productWarrantysGroup)
            {
                var guarrantee = new StringBuilder();
                var mainGuarrantee = string.Empty;
                var productWarrantys = invoiceWarrantys.Where(x => x.ProductId == group.Key.ProductId).ToList();

                productWarrantys?.ForEach(w =>
                {
                    if (w.WarrantyType == (int)WarrantyType.Maintenance)
                    {
                        mainGuarrantee = string.Format("Định kỳ bảo trì: {0} {1} ({2}) đến {3}",
                            w.NumberTime, SystemHelper.GetWarrantyTimeTypeText(w.TimeType).ToLower(),
                            w.Description, ((DateTime)w.ExpireDate).ToString("dd/MM/yyyy"));
                    }
                    else
                    {
                        var guarranteelog = string.Format("{0} {1} ({2}) đến {3}",
                           w.NumberTime, SystemHelper.GetWarrantyTimeTypeText(w.TimeType).ToLower(),
                           w.Description, ((DateTime)w.ExpireDate).ToString("dd/MM/yyyy"));

                        guarrantee.Append(string.IsNullOrEmpty(guarrantee.ToString())
                            ? $"Bảo hành: {guarranteelog}"
                            : $", {guarranteelog}");
                    }
                });
                if (checkProductIsCombo)
                {
                    warrantysLog.Append($"Thành phần bảo hành: [ProductCode]{group.Key.ProductCode}[/ProductCode]<br />");
                }
                warrantysLog.Append(string.IsNullOrEmpty(guarrantee.ToString()) ? string.Empty : $"&nbsp;&nbsp;{guarrantee}<br>");
                warrantysLog.Append(string.IsNullOrEmpty(mainGuarrantee) ? string.Empty : $"&nbsp;&nbsp;{mainGuarrantee}<br>");
            }

            return warrantysLog;
        }
    }
}
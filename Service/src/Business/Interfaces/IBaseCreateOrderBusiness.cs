﻿using System;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Business.Interfaces
{
    public interface IBaseCreateOrderBusiness
    {
        Task<bool> CreateOrderServiceV2(string request);
        Task<bool> RetryOrderServiceV2(Guid logId, int retailerId, int branchId, int omniChannelId, string kvOrder);
        Task<bool> SyncErrorOrderServiceV2(Guid logId, int retailerId, int branchId, int omniChannelId, string orderSn);
    }
}

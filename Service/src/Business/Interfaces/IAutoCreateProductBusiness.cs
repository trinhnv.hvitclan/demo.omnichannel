﻿using System.Threading.Tasks;
using Demo.OmniChannel.Business.Dto;
using Demo.OmniChannel.ShareKernel.KafkaMessage;

namespace Demo.OmniChannel.Business.Interfaces
{
    public interface IAutoCreateProductBusiness
    {
        /// <summary>
        /// Tạo sản phẩm ở KV
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        Task<AutoCreateProductResponse> CreateProductToKv(SyncOrderWithAutoCreateProduct param);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.Repository.Impls;

namespace Demo.OmniChannel.Business.Interfaces
{
    public interface IOnHandBusiness
    {
        Task SyncMultiOnHand(string connectStringName,
            Domain.Model.OmniChannel channel,
            List<string> channelProductIds,
            List<long> kvProductIdsReq,
            bool isIgnoreAuditTrail,
            Guid logId,
            DateTime? messageSentTime,
            bool isWriteLog = false,
            int totalPageSizeSync = 50,
            List<ProductMapping> mappings = null,
            bool isGetMapping = true,
            LogObjectMicrosoftExtension logMicrosoftExtension = null);

        Task<List<ProductBranch>> GetProductBranchesWithRetry(
            ProductBranchRepository productBranchRepository,
            int retailerId,
            long branchId,
            List<long> productIds,
            DateTime? timeUpdated);
    }
}

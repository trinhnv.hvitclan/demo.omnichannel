﻿using Demo.OmniChannel.Domain.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.ShareKernel.KafkaMessage;

namespace Demo.OmniChannel.Business.Interfaces
{
    public interface IMappingBusiness
    {
        Task CreateMappingAsync(string connectStringName, int retailerId, int branchId, List<Domain.Model.OmniChannel> channels, List<KvProductMapping> kvProductMappings, Guid logId, string serviceName, int totalPageSizeSync = 50);
        Task CreateMappingAsyncV2(string connectStringName, int retailerId, int branchId, List<Domain.Model.OmniChannel> channels, List<KvProductMapping> kvProductMappings, Guid logId, string serviceName, int totalPageSizeSync = 50);
        Task RemoveMappings(string connectStringName, int retailerId, int branchId, List<long> channelIds, List<long> kvProductIds, List<string> channelProductIds, Guid logId, bool isDeleteKvProduct = true);
        Task<List<ProductMapping>> GetMappingsByProductIds(BaseRetryMessage data, List<long> kvProductIds);
        Task<List<ProductMapping>> GetMappingByItemIds(long channelId, int retailerId, List<string> itemIds);
        void SetCacheWaitingDeleteMapping(long channelId, List<long> kvProductIds);
        void RemoveCacheWaitingDeleteMapping(long channelId, List<long> kvProductIds);
        Task WaitingDeleteMapping(long channelId, List<long> kvProductIds);
    }
}

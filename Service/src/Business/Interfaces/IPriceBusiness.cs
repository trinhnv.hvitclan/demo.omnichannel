﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.Infrastructure.EventBus.Event;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.ShareKernel.KafkaMessage;

namespace Demo.OmniChannel.Business.Interfaces
{
    public interface IPriceBusiness
    {
        Task SyncMultiPrice(string connectStringName,
            Domain.Model.OmniChannel channel,
            List<string> channelProductIds,
            List<long> kvProductIdsReq,
            bool isIgnoreAuditTrail,
            Guid logId,
            bool isWriteLog = false,
            int totalPageSizeSync = 50,
            List<ProductMapping> mappings = null,
            bool isGetMapping = true,
            string source = null);

        Task<List<MultiProductItem>> GetProductPricesAsync(
            string connectStringName,
          Domain.Model.OmniChannel channel,
          List<ProductPrice> products);
    }
}

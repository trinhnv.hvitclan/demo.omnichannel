﻿using Demo.OmniChannel.ShareKernel.KafkaMessage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Business.Interfaces
{
    public  interface IBaseSyncInvoiceErrorBusiness
    {
        Task<bool> ProcessSyncInvoiceError(BaseSyncErrorInvoiceMessage message);
    }
}

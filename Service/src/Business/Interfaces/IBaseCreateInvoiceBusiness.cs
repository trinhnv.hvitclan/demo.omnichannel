﻿using Demo.OmniChannel.ShareKernel.RedisStreamMessage;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Business.Interfaces
{
    public interface IBaseCreateInvoiceBusiness
    {
        Task<bool> HandleCreateInvoice(CreateInvoiceMessage message);
    }
}

﻿using System;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Business.Interfaces
{
    public interface IChannelBusiness
    {
        Task<Domain.Model.OmniChannel> GetChannel(long channelId, int retailerId, Guid logId);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Demo.Audit.Model.Message;
using Demo.OmniChannel.ShareKernel.Dto;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.FeatureToggle;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Infrastructure.EventBus.Event;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using ChannelProductType = Demo.OmniChannel.ShareKernel.Common.ChannelProductType;

namespace Demo.OmniChannel.Business
{
    public class PriceBusiness : IPriceBusiness
    {
        private readonly IProductMongoService _productMongoService;
        private readonly IProductMappingService _productMappingService;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly ILog _logger = LogManager.GetLogger(typeof(PriceBusiness));
        private readonly IMessageFactory _messageFactory;
        private readonly IAuditTrailInternalClient _auditTrailInternalClient;
        private readonly IAppSettings _settings;
        private readonly ICacheClient _cacheClient;

        public PriceBusiness(IDbConnectionFactory dbConnectionFactory,
            IProductMappingService productMappingService,
            IProductMongoService productMongoService,
            IMessageFactory messageFactory,
            IAuditTrailInternalClient auditTrailInternalClient,
            IAppSettings settings,
            ICacheClient cacheClient)
        {
            _dbConnectionFactory = dbConnectionFactory;
            _productMappingService = productMappingService;
            _productMongoService = productMongoService;
            _messageFactory = messageFactory;
            _auditTrailInternalClient = auditTrailInternalClient;
            _settings = settings;
            _cacheClient = cacheClient;
        }

        public async Task SyncMultiPrice(string connectStringName,
            Domain.Model.OmniChannel channel,
            List<string> channelProductIds,
            List<long> kvProductIdsReq,
            bool isIgnoreAuditTrail,
            Guid logId,
            bool isWriteLog = false,
            int totalPageSizeSync = 50,
            List<ProductMapping> mappings = null,
            bool isGetMapping = true,
            string source = null)
        {
            var logMessage = new LogObject(_logger, logId)
            {
                Action = "SyncMultiPrice",
                RetailerId = channel?.RetailerId,
                BranchId = channel?.BranchId,
                OmniChannelId = channel?.Id,
                RequestObject = $"KvProductIds: {kvProductIdsReq?.Join(", ")} - ChannelProductIds: {channelProductIds?.Join(", ")}"
            };
            try
            {
                await SyncPrice(
                        connectStringName,
                        channel,
                        channelProductIds,
                        kvProductIdsReq,
                        isIgnoreAuditTrail,
                        logId,
                        source
                    );
                logMessage.LogInfo();
            }
            catch (Exception ex)
            {
                logMessage.LogError(ex);
                throw;
            }
        }

        #region Private method
        private async Task OnHandlerPricebookFailed(
          Domain.Model.OmniChannel channel,
          string connectStringName,
          bool isIgnoreAuditTrail,
          string detailMessage,
          Guid logId)
        {
            if (isIgnoreAuditTrail) return;

            // Bảng giá bán không hợp lệ thì báo lỗi, không đồng bộ cả bảng giá khuyến mại KOL-175
            // Core context
            var coreContext = await GetCoreContext(channel, channel.RetailerId, connectStringName, channel.BranchId, logId);
            // Audit log
            var log = new AuditTrailLog
            {
                FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channel.Type),
                Action = (int)AuditTrailAction.ProductIntergate,
                CreatedDate = DateTime.Now,
                BranchId = channel.BranchId
            };
            var typeName = Enum.GetName(typeof(ChannelType), channel.Type);
            var contentLog = new StringBuilder($"Đồng bộ thông tin giá bán hàng hóa KHÔNG thành công: {typeName} shop: {channel.Email ?? channel.Name}, Bảng giá bán không hợp lệ : {detailMessage}");
            log.Content = contentLog.ToString();
            await _auditTrailInternalClient.AddLogAsync(coreContext, log);
        }

        private async Task OnAddErrorProductsPriceBookFailed(
            Domain.Model.OmniChannel channel,
            List<long> productMappingIds,
            string detailMessage)
        {
            foreach (var productKvId in productMappingIds)
            {
                var channelProducts = await GetChannelProducts(channel.RetailerId, channel.Id, productKvId);
                var errorProducts = await _productMongoService.GetByProductChannelIds(channel.RetailerId, channel.Id, channelProducts?.Select(t => t.ProductChannelId.ToString()).ToList(), isStringItemId: ConvertHelper.CheckUseStringItemId(channel.Type));
                await _productMongoService.UpdateFailedMessageAndStatusCode(errorProducts?.Select(t => t.Id).ToList(), 0, detailMessage, SyncErrorType.Price);
            }

            #region Private method
            async Task<List<ProductMapping>> GetChannelProducts(long retailerId, long channelId, long productId)
            {
                List<ProductMapping> dataInCache = null;
                if (_cacheClient != null)
                {
                    dataInCache = _cacheClient.Get<List<ProductMapping>>(string.Format(
                        KvConstant.ListProductMappingCacheKey,
                        retailerId, channelId, productId));
                }

                if (dataInCache != null && dataInCache.Any())
                {
                    return dataInCache;
                }

                var retVal =
                    await _productMappingService.GetListByKvSingleProductId(retailerId, channelId, productId);

                if (retVal == null)
                {
                    return null;
                }

                _cacheClient?.Set(string.Format(KvConstant.ListProductMappingCacheKey, retailerId, channelId, productId), retVal,
                    DateTime.Now.AddDays(30));
                return retVal;
            }
            #endregion Private method
        }

        private async Task<OmniChannelCore.Api.Sdk.Common.KvInternalContext> GetCoreContext(
            Domain.Model.OmniChannel omniChannel,
            int retailerId,
            string connectStringName,
            int branchId,
            Guid logId)
        {
            var context = await ContextHelper.GetExecutionContext(_cacheClient, _dbConnectionFactory, retailerId, connectStringName, branchId, _settings?.Get<int>("ExecutionContext"));
            context.ShopId = omniChannel.IdentityKey;
            context.ChannelId = omniChannel.Id;
            var coreContext = context.ConvertTo<OmniChannelCore.Api.Sdk.Common.KvInternalContext>();
            coreContext.UserId = context.User?.Id ?? 0;
            coreContext.LogId = logId;
            return coreContext;
        }


        private async Task SyncPrice(string connectStringName,
          Domain.Model.OmniChannel channel,
          List<string> channelProductIds,
          List<long> kvProductIdsReq,
          bool isIgnoreAuditTrail,
          Guid logId,
          string source)
        {
            var logMessage = new LogObject(_logger, logId)
            {
                Action = "SyncMultiPriceGetProduct",
                RetailerId = channel.RetailerId,
                BranchId = channel.BranchId,
                OmniChannelId = channel.Id,
                RequestObject = $"KvProductIds: {kvProductIdsReq?.Join(", ")} - ChannelProductIds: {channelProductIds?.Join(", ")}"
            };

            var mappings = await _productMappingService.GetByChannelProductIds(channel.RetailerId, channel.Id,
                 channelProductIds, kvProductIdsReq, channel.Type == (byte)ChannelType.Sendo,
                 isStringItemId: ConvertHelper.CheckUseStringItemId(channel.Type));

            if (mappings?.Any() != true)
            {
                logMessage.LogWarning("Mapping is null");
                return;
            }

            var basePriceProducts = new Dictionary<long, decimal>();
            var salePriceProducts = new Dictionary<long, decimal>();
            var kvProducts = new List<ProductDto>();
            PriceBook salePriceBook = null;

            using (var db = await _dbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var priceBookRepository = new PriceBookRepository(db);
                var productRepo = new ProductRepository(db);
                var productMappingIds = mappings.GroupBy(x => x.ProductKvId)
                    .Select(x => x.FirstOrDefault()?.ProductKvId ?? 0).ToList();
                var totalPages = (int)Math.Ceiling(productMappingIds.Count / (decimal)1000);
                for (int page = 1; page <= totalPages; page++)
                {
                    var productPaging = new HashSet<long>(productMappingIds.Page(page, 1000).ToList());
                    kvProducts.AddRange(await productRepo.GetByIds(channel.RetailerId, productPaging));

                    var getProductInBasePrice = await GetProductInBasePricebook(channel, priceBookRepository,
                        productRepo, connectStringName, isIgnoreAuditTrail, productPaging);
                    var getProductInSalePrice = await GetProductInPriceBook(channel, priceBookRepository,
                        productRepo, productPaging);
                    basePriceProducts = basePriceProducts.Union(getProductInBasePrice).ToDictionary(x => x.Key, y => y.Value);
                    salePriceProducts = salePriceProducts.Union(getProductInSalePrice.salePriceProducts).ToDictionary(x => x.Key, y => y.Value);
                    salePriceBook = getProductInSalePrice.salePriceBook;
                }
            }

            if (basePriceProducts.Count > 0 || salePriceProducts.Count > 0)
            {
                PushMessageQueueToChannel(mappings, channel, connectStringName, basePriceProducts,
                    salePriceBook, salePriceProducts, kvProducts, isIgnoreAuditTrail, source);
            }

            logMessage.ResponseObject = $"Total basePriceProducts = {basePriceProducts.Count}, salePriceProducts = {salePriceProducts.Count}";
            logMessage.LogInfo();
        }

        /// <summary>
        /// Lấy danh sách sản phẩm bảng giá bán
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="priceBookRepository"></param>
        /// <param name="productRepo"></param>
        /// <param name="connectStringName"></param>
        /// <param name="isIgnoreAuditTrail"></param>
        /// <param name="kvProductIds"></param>
        /// <returns></returns>
        private async Task<Dictionary<long, decimal>> GetProductInBasePricebook(
            Domain.Model.OmniChannel channel,
            PriceBookRepository priceBookRepository,
            ProductRepository productRepo,
            string connectStringName, bool isIgnoreAuditTrail,
            HashSet<long> kvProductIds)
        {
            var basePriceProducts = new Dictionary<long, decimal>();
            if (channel.BasePriceBookId.GetValueOrDefault() > 0)
            {
                var basePriceBook = await priceBookRepository.GetByIdAsync(channel.BasePriceBookId, true);
                if (basePriceBook == null)
                {
                    string errorMessage = "Bảng giá bán không hợp lệ";
                    await OnHandlerPricebookFailed(channel, connectStringName, isIgnoreAuditTrail, errorMessage, Guid.NewGuid());
                    await OnAddErrorProductsPriceBookFailed(channel, kvProductIds.ToList(), errorMessage);
                    return basePriceProducts;
                }
                var (isValidBasePriceBook, detailMessage) = basePriceBook.IsValidPriceBook(channel.BranchId);
                if (isValidBasePriceBook)
                {
                    var productInPriceBook =
                        await priceBookRepository.GetPriceByProductIds(basePriceBook.Id, kvProductIds);
                    var productNotInPriceBookIds =
                        new HashSet<long>(kvProductIds.Except(productInPriceBook.Keys));
                    var productNotInPriceBook = (await productRepo.GetByIds(channel.RetailerId, productNotInPriceBookIds))
                        .ToDictionary(x => x.Id, y => y.BasePrice);
                    basePriceProducts = productInPriceBook.Union(productNotInPriceBook)
                     .ToDictionary(x => x.Key, y => y.Value);
                }
                else
                {
                    await OnHandlerPricebookFailed(channel, connectStringName, isIgnoreAuditTrail, detailMessage, Guid.NewGuid());
                    await OnAddErrorProductsPriceBookFailed(channel, kvProductIds.ToList(), detailMessage);
                    return basePriceProducts;
                }
            }
            else
            {
                basePriceProducts = (await productRepo.GetByIds(channel.RetailerId, kvProductIds))
                    .ToDictionary(x => x.Id, y => y.BasePrice);
            }
            return basePriceProducts;
        }

        /// <summary>
        /// Lấy danh sách sản phẩm ở bảng giá khuyến mại
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="priceBookRepository"></param>
        /// <param name="productRepo"></param>
        /// <param name="kvProductIds"></param>
        /// <returns></returns>
        private async Task<(Dictionary<long, decimal> salePriceProducts, PriceBook salePriceBook)> GetProductInPriceBook(
            Domain.Model.OmniChannel channel,
            PriceBookRepository priceBookRepository,
             ProductRepository productRepo,
             HashSet<long> kvProductIds)
        {
            PriceBook salePriceBook = null;
            var salePriceProducts = new Dictionary<long, decimal>();
            if (channel.PriceBookId.GetValueOrDefault() > 0)
            {
                salePriceBook = await priceBookRepository.GetByIdAsync(channel.PriceBookId, true);

                if (salePriceBook != null &&
                   (salePriceBook.PriceBookBranches != null &&
                    salePriceBook.PriceBookBranches.Any(x => x.BranchId == channel.BranchId) ||
                    salePriceBook.IsGlobal) && salePriceBook.IsActive &&
                   !salePriceBook.isDeleted.GetValueOrDefault() && salePriceBook.StartDate < DateTime.Now && salePriceBook.EndDate > DateTime.Now)
                {
                    var productInPriceBook = await priceBookRepository.GetPriceByProductIds(salePriceBook.Id, kvProductIds);
                    var productNotInPriceBookIds = new HashSet<long>(kvProductIds.Except(productInPriceBook.Keys));
                    var productNotInPriceBook = (await productRepo.GetByIds(channel.RetailerId, productNotInPriceBookIds)).ToDictionary(x => x.Id, y => y.BasePrice);
                    salePriceProducts = productInPriceBook.Union(productNotInPriceBook)
                        .ToDictionary(x => x.Key, y => y.Value);
                }
                else
                {
                    // Nếu bảng giá khuyến mại không hợp lệ thì không đồng bộ bảng giá khuyến mại
                    salePriceProducts = new Dictionary<long, decimal>();
                }
            }
            else if (channel.PriceBookId != null)
            {
                salePriceProducts = (await productRepo.GetByIds(channel.RetailerId, kvProductIds)).ToDictionary(x => x.Id, y => y.BasePrice);
            }
            return (salePriceProducts, salePriceBook);
        }

        /// <summary>
        /// Push message
        /// </summary>
        /// <param name="mappings"></param>
        /// <param name="channel"></param>
        /// <param name="connectStringName"></param>
        /// <param name="basePriceProducts"></param>
        /// <param name="salePriceBook"></param>
        /// <param name="salePriceProducts"></param>
        /// <param name="kvProducts"></param>
        /// <param name="isIgnoreAuditTrail"></param>
        /// <param name="source"></param>
        private void PushMessageQueueToChannel(
            List<ProductMapping> mappings,
            Domain.Model.OmniChannel channel,
            string connectStringName,
            Dictionary<long, decimal> basePriceProducts,
            PriceBook salePriceBook,
            Dictionary<long, decimal> salePriceProducts,
            List<ProductDto> kvProducts,
            bool isIgnoreAuditTrail,
            string source)
        {
            switch (channel.Type)
            {
                case (byte)ChannelType.Lazada:
                case (byte)ChannelType.Tiki:
                case (byte)ChannelType.Sendo:
                    var totalPages = (int)Math.Ceiling(mappings.Count / (decimal)50);
                    Parallel.For(1, totalPages + 1, page =>
                    {
                        var productPageMappings = mappings.Page(page, 50).ToList();

                        var productMappingsNormal = productPageMappings.Where(p => p.ProductChannelType == (byte)ChannelProductType.Normal).ToList();
                        if (productMappingsNormal != null && productMappingsNormal.Any())
                        {
                            var messagePrice = CreateMessageQueue(productMappingsNormal, channel, connectStringName,
                                basePriceProducts, salePriceBook, salePriceProducts, kvProducts, isIgnoreAuditTrail, source);
                            PushPriceRedisMessageQueue(channel, messagePrice);
                        }

                        var productMappingsvariation = productPageMappings.Where(p => p.ProductChannelType == (byte)ChannelProductType.Variation).ToList();
                        if (productMappingsvariation != null && productMappingsvariation.Any())
                        {
                            var messagePrice = CreateMessageQueue(productMappingsvariation, channel, connectStringName,
                                basePriceProducts, salePriceBook, salePriceProducts, kvProducts, isIgnoreAuditTrail, source);
                            PushPriceRedisMessageQueue(channel, messagePrice);
                        }
                    });
                    break;
                case (byte)ChannelType.Shopee:
                case (byte)ChannelType.Tiktok:
                    //Nếu có nhiều sản phẩm mapping KvProductId nhóm lại lấy sản phẩm đầu tiên
                    var groupParrent = mappings.GroupBy(p => p.ProductKvId).Select(x => x.FirstOrDefault())
                    .GroupBy(p => p.CommonParentProductChannelId).Select(x => new
                    {
                        Parrent = x.Key,
                        Child = x.ToList()
                    }).ToList();

                    Parallel.ForEach(groupParrent, mapping =>
                    {
                        var messagePrice = CreateMessageQueue(mapping.Child, channel, connectStringName,
                            basePriceProducts, salePriceBook, salePriceProducts, kvProducts, isIgnoreAuditTrail, source);
                        PushPriceRedisMessageQueue(channel, messagePrice);
                    });

                    break;
                default:
                    break;
            }

        }

        /// <summary>
        /// Khởi tạo message
        /// </summary>
        /// <param name="mappings"></param>
        /// <param name="channel"></param>
        /// <param name="connectStringName"></param>
        /// <param name="basePriceProducts"></param>
        /// <param name="salePriceBook"></param>
        /// <param name="salePriceProducts"></param>
        /// <param name="kvProducts"></param>
        /// <param name="isIgnoreAuditTrail"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        private SyncPriceMessage CreateMessageQueue(
            List<ProductMapping> mappings,
            Domain.Model.OmniChannel channel,
            string connectStringName,
            Dictionary<long, decimal> basePriceProducts,
            PriceBook salePriceBook,
            Dictionary<long, decimal> salePriceProducts,
            List<ProductDto> kvProducts,
            bool isIgnoreAuditTrail,
            string source)
        {
            var message = new SyncPriceMessage
            {
                ChannelId = channel.Id,
                ChannelType = channel.Type,
                ChannelName = channel.Name,
                BranchId = channel.BranchId,
                RetailerId = channel.RetailerId,
                ItemType = (byte)ChannelProductType.Normal,
                KvEntities = connectStringName,
                MultiProducts = mappings.Where(p => basePriceProducts.Keys.Contains(p.ProductKvId)).Select(p => new MultiProductItem
                {
                    ItemId = p.CommonProductChannelId,
                    ItemSku = p.ProductChannelSku,
                    KvProductId = p.ProductKvId,
                    KvProductSku = p.ProductKvSku,
                    ParentItemId = p.CommonParentProductChannelId,
                    Price = basePriceProducts.ContainsKey(p.ProductKvId) ? NumberHelper.RoundProductPrice(basePriceProducts[p.ProductKvId]) : null,
                    SalePrice = salePriceProducts.ContainsKey(p.ProductKvId) ? NumberHelper.RoundProductPrice(salePriceProducts[p.ProductKvId]) : null,
                    StartSaleDate = salePriceBook != null && salePriceProducts.ContainsKey(p.ProductKvId) ? salePriceBook.StartDate : DateTime.Now,
                    EndSaleDate = salePriceBook != null && salePriceProducts.ContainsKey(p.ProductKvId) ? salePriceBook.EndDate : DateTime.Now,
                    Revision = kvProducts.FirstOrDefault(x => x.Id == p.ProductKvId)?.Revision
                }).ToList(),
                IsIgnoreAuditTrail = isIgnoreAuditTrail,
                LogId = Guid.NewGuid(),
                Source = source
            };
            return message;
        }

        private void PushPriceRedisMessageQueue(Domain.Model.OmniChannel channel,
            SyncPriceMessage messagePrice)
        {
            switch (channel.Type)
            {
                case (byte)ChannelType.Lazada:
                    {
                        if (new LazadaSyncPriceMessageV2Toggle(_settings).Enable(channel.RetailerId, 0))
                        {
                            using (var mq = _messageFactory.CreateMessageQueueClient())
                            {
                                var message = messagePrice.ConvertTo<LazadaSyncPriceMessage>();
                                var mess = new Message { CreatedDate = DateTime.Now, Id = Guid.NewGuid() };
                                mess.Body = message;
                                mq.Publish($"mq:{_settings.Get("LazadaSyncPriceMessageV2_Topic", "LazadaSyncPriceMessageV2_Topic")}.inq", mess);
                            }
                        }
                        else
                        {
                            using (var mq = _messageFactory.CreateMessageProducer())
                            {
                                mq.Publish(messagePrice.ConvertTo<LazadaSyncPriceMessage>());
                            }
                        }
                        break;
                    }

                case (byte)ChannelType.Shopee:
                    {
                        if (new ShopeeSyncPriceMessageV2Toggle(_settings).Enable(channel.RetailerId, 0))
                        {
                            using (var mq = _messageFactory.CreateMessageQueueClient())
                            {
                                var message = messagePrice.ConvertTo<ShopeeSyncPriceMessageV2>();
                                var mess = new Message { CreatedDate = DateTime.Now, Id = Guid.NewGuid() };
                                mess.Body = message;
                                mq.Publish($"mq:{_settings.Get("ShopeeSyncPriceMessageV2_Topic", "ShopeeSyncPriceMessageV2_Topic")}.inq", mess);
                            }
                        }
                        else
                        {
                            using (var mq = _messageFactory.CreateMessageProducer())
                            {
                                mq.Publish(messagePrice.ConvertTo<ShopeeSyncPriceMessageV2>());
                            }
                        }
                        break;
                    }

                case (byte)ChannelType.Tiki:
                    {
                        if (new TikiSyncPriceMessageV2Toggle(_settings).Enable(channel.RetailerId, 0))
                        {
                            using (var mq = _messageFactory.CreateMessageQueueClient())
                            {
                                var message = messagePrice.ConvertTo<TikiSyncPriceMessage>();
                                var mess = new Message { CreatedDate = DateTime.Now, Id = Guid.NewGuid() };
                                mess.Body = message;
                                mq.Publish($"mq:{_settings.Get("TikiSyncPriceMessageV2_Topic", "TikiSyncPriceMessageV2_Topic")}.inq", mess);
                            }
                        }
                        else
                        {
                            using (var mq = _messageFactory.CreateMessageProducer())
                            {
                                mq.Publish(messagePrice.ConvertTo<TikiSyncPriceMessage>());
                            }
                        }
                        break;
                    }

                case (byte)ChannelType.Sendo:
                    {
                        if (new SendoSyncPriceMessageV2Toggle(_settings).Enable(channel.RetailerId, 0))
                        {
                            using (var mq = _messageFactory.CreateMessageQueueClient())
                            {
                                var message = messagePrice.ConvertTo<SendoSyncPriceMessage>();
                                var mess = new Message { CreatedDate = DateTime.Now, Id = Guid.NewGuid() };
                                mess.Body = message;
                                mq.Publish($"mq:{_settings.Get("SendoSyncPriceMessageV2_Topic", "SendoSyncPriceMessageV2_Topic")}.inq", mess);
                            }
                        }
                        else
                        {
                            using (var mq = _messageFactory.CreateMessageProducer())
                            {
                                mq.Publish(messagePrice.ConvertTo<SendoSyncPriceMessage>());
                            }
                        }
                        break;
                    }
                case (byte)ChannelType.Tiktok:
                    {
                        if (new TiktokSyncPriceMessageV2Toggle(_settings).Enable(channel.RetailerId, 0))
                        {
                            using (var mq = _messageFactory.CreateMessageQueueClient())
                            {
                                var message = messagePrice.ConvertTo<TiktokSyncPriceMessage>();
                                var mess = new Message { CreatedDate = DateTime.Now, Id = Guid.NewGuid() };
                                mess.Body = message;
                                mq.Publish($"mq:{_settings.Get("TiktokSyncPriceMessageV2_Topic", "TiktokSyncPriceMessageV2_Topic")}.inq", mess);
                            }
                        }
                        else
                        {
                            using (var mq = _messageFactory.CreateMessageProducer())
                            {
                                mq.Publish(messagePrice.ConvertTo<TiktokSyncPriceMessage>());
                            }
                        }
                        break;
                    }
            }
        }
        #endregion

        /// <summary>
        /// Lấy danh sách giá kv
        /// </summary>
        /// <param name="connectStringName"></param>
        /// <param name="channel"></param>
        /// <param name="products"></param>
        /// <returns></returns>
        public async Task<List<MultiProductItem>> GetProductPricesAsync(
            string connectStringName,
          Domain.Model.OmniChannel channel,
          List<ProductPrice> products)
        {
            //Lấy danh sách mappings
            var mappings = await _productMappingService.GetByProductChannelIds(
                channel.RetailerId, channel.Id, products.Select(x => x.ItemId).ToList(),
                ConvertHelper.CheckUseStringItemId(channel.Type));



            using (var db = await _dbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var priceBookRepository = new PriceBookRepository(db);
                var productRepo = new ProductRepository(db);

                var productMappingIds = mappings.GroupBy(x => x.ProductKvId)
                    .Select(x => x.FirstOrDefault()?.ProductKvId ?? 0).ToList();

                var kvProducts = await productRepo.GetByIds(channel.RetailerId,
                    new HashSet<long>(productMappingIds));

                var basePriceProducts = await GetProductInBasePricebook(channel, priceBookRepository,
                        productRepo, connectStringName, true, new HashSet<long>(productMappingIds));

                var (salePriceProducts, salePriceBook) = await GetProductInPriceBook(channel, priceBookRepository,
                    productRepo, new HashSet<long>(productMappingIds));

                return mappings.Select(p => new MultiProductItem
                {
                    ItemId = p.CommonProductChannelId,
                    ItemSku = p.ProductChannelSku,
                    KvProductId = p.ProductKvId,
                    KvProductSku = p.ProductKvSku,
                    ItemType = p.ProductChannelType,
                    ParentItemId = p.CommonParentProductChannelId,
                    Price = basePriceProducts.ContainsKey(p.ProductKvId) ? NumberHelper.RoundProductPrice(basePriceProducts[p.ProductKvId]) : null,
                    SalePrice = salePriceProducts.ContainsKey(p.ProductKvId) ? NumberHelper.RoundProductPrice(salePriceProducts[p.ProductKvId]) : null,
                    StartSaleDate = salePriceBook != null && salePriceProducts.ContainsKey(p.ProductKvId) ? salePriceBook.StartDate : DateTime.Now,
                    EndSaleDate = salePriceBook != null && salePriceProducts.ContainsKey(p.ProductKvId) ? salePriceBook.EndDate : DateTime.Now,
                    Revision = kvProducts.FirstOrDefault(x => x.Id == p.ProductKvId)?.Revision
                }).ToList();
            }
        }
    }
}

﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.DomainService.Interfaces;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.RedisStreamMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Demo.OmniChannelCore.Api.Sdk.Models;
using Newtonsoft.Json;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Business
{
    public class ShopeeBusinessInvoiceService : BaseCreateInvoiceBusiness, IShopeeBusinessInvoiceInterface
    {
        private readonly IProductMappingService _productMappingService;
        private readonly IDeliveryPackageInternalClient _deliveryPackageInternalClient;

        public ShopeeBusinessInvoiceService
            (IOmniChannelSettingService omniChannelSettingService,
            IInvoiceMongoService invoiceMongoServices,
            IDeliveryInfoInternalClient deliveryInfoInternalClient,
            IRetryInvoiceMongoService retryInvoiceMongoService,
            IInvoiceInternalClient invoiceInternalClient,
            IAuditTrailInternalClient auditTrailInternalClient,
            IOrderInternalClient orderInternalClient,
            ChannelClient.Interfaces.IChannelClient channelClient,
            IAppSettings appSettings,
            ICustomerInternalClient customerInternalClient,
            IProductMappingService productMappingService,
            ICacheClient cacheClient,
            ICreateOrderDomainService createOrderDomainService,
            ISurChargeInternalClient surChargeInternalClient,
            IIntegrationEventService integrationEventService,
            ICreateInvoiceDomainService createInvoiceDomainService,
            IPriceBookInternalClient priceBookInternalClient,
            IProductInternalClient productInternalClient,
            ServiceStack.Data.IDbConnectionFactory dbConnectionFactory,
            IMessageService messageService,
            IDeliveryPackageInternalClient deliveryPackageInternalClient,
            IKvLockRedis kvLockRedis
            ) : base(omniChannelSettingService,
                invoiceMongoServices,
                deliveryInfoInternalClient,
                retryInvoiceMongoService,
                invoiceInternalClient,
                auditTrailInternalClient,
                orderInternalClient,
                channelClient,
                appSettings,
                customerInternalClient,
                cacheClient,
                createOrderDomainService,
                surChargeInternalClient,
                integrationEventService,
                createInvoiceDomainService,
                priceBookInternalClient,
                productInternalClient,
                dbConnectionFactory,
                messageService,
                kvLockRedis)
        {
            _productMappingService = productMappingService;
            _deliveryPackageInternalClient = deliveryPackageInternalClient;
        }

        protected override Task UpdateFeeDeliveryInOrder(
            OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext,
            long? deliveryInfoId,
            decimal? price,
            string feeJson)
        {
            return Task.CompletedTask;
        }

        protected override void UpdateOrderDetail(KvInvoice invoice,
            List<MongoDb.ProductMapping> mappings)
        {
            foreach (var detail in invoice.InvoiceDetails)
            {
                var mapping = mappings?.FirstOrDefault(x => !string.IsNullOrEmpty(detail.ProductChannelId)
                && x.CommonProductChannelId == detail.ProductChannelId);

                detail.ProductId = mapping?.ProductKvId ?? 0;
                detail.ProductCode = mapping?.ProductKvSku ?? string.Empty;
            }
        }

        protected override async Task<List<MongoDb.ProductMapping>> GetMappings(ChannelClient.Models.KvInvoice invoice,
            int retailerId, long channelId, byte channelType)
        {
            var productIds = invoice.InvoiceDetails.Where(p => !string.IsNullOrEmpty(p.ProductChannelId))
               .Select(p => p.ProductChannelId.ToString()).ToList();

            var mappings = await _productMappingService.GetByChannelProductIds(retailerId, channelId,
                productIds, isStringItemId: ConvertHelper.CheckUseStringItemId(channelType));

            return mappings;
        }

        protected override Task HandleVoidInvoice(List<KvInvoice> invoices, CreateInvoiceMessage data,
           OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext,
           OmniChannelCore.Api.Sdk.Models.Order existOrder)
        {
            return Task.CompletedTask;
        }

        protected override async Task UpdateDeliveryInfo(
           OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext,
           CreateInvoiceMessage data,
           KvOrder kvOrder,
           DeliveryInfoForOrder deliveryInfo)
        {
            if (kvOrder?.Status != (byte)OrderState.Draft) return;

            var order = JsonConvert.DeserializeObject<KvOrder>(data.Order);
            var connectStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);

            if (
                                   order?.OrderDelivery != null
                                   && (
                                       deliveryInfo.Receiver != order.OrderDelivery.Receiver ||
                                       deliveryInfo.ContactNumber != order.OrderDelivery.ContactNumber ||
                                       deliveryInfo.Address != order.OrderDelivery.Address ||
                                       deliveryInfo.WardName != order.OrderDelivery.WardName ||
                                       deliveryInfo.LocationName != order.OrderDelivery.LocationName)
                               )
            {
                var isChangedPhone = PhoneNumberHelper.GetPerfectContactNumber(deliveryInfo.ContactNumber) != PhoneNumberHelper.GetPerfectContactNumber(order.OrderDelivery.ContactNumber);

                deliveryInfo.Receiver = order.OrderDelivery.Receiver;
                deliveryInfo.ContactNumber = order.OrderDelivery.ContactNumber;
                deliveryInfo.Address = order.OrderDelivery.Address;
                deliveryInfo.WardName = order.OrderDelivery.WardName;
                deliveryInfo.LocationName = order.OrderDelivery.LocationName;

                var dpDataChanged = new OmniChannelCore.Api.Sdk.Models.DeliveryPackage
                {
                    RetailerId = kvOrder.RetailerId,
                    OrderId = kvOrder.Id,
                    Receiver = order.OrderDelivery.Receiver,
                    ContactNumber = order.OrderDelivery.ContactNumber,
                    Address = order.OrderDelivery.Address,
                    WardName = order.OrderDelivery.WardName,
                    LocationName = order.OrderDelivery.LocationName,
                };
                var dpConditionChanged = new DeliveryPackageConditionUpdate { OrderId = kvOrder.Id };
                var fieldChangeds = new[] { "Receiver", "ContactNumber", "Address", "WardName", "LocationName" };
                var dpUpdatedRow = await _deliveryPackageInternalClient.Update(coreContext, dpDataChanged, dpConditionChanged, fieldChangeds);

                long orderUpdatedCusRow = 0;
                var phone = PhoneNumberHelper.GetPerfectContactNumber(order.CustomerPhone);

                var isValidPhone = PhoneNumberHelper.IsValidContactNumber(phone, RegionCode.VN) || data.ChannelType != (byte)ChannelTypeEnum.Shopee;
                // Update OrderCustomer
                if (dpUpdatedRow > 0 && isChangedPhone && isValidPhone)
                {
                    using (var cli = _kvLockRedis.GetLockFactory())
                    {
                        var lockAddCustomerKey = phone;
                        if (string.IsNullOrEmpty(phone))
                        {
                            lockAddCustomerKey = order.Code;
                        }

                        long? newOrderCustomerId = 0;
                        using (cli.AcquireLock(string.Format(KvConstant.LockAddCustomer, lockAddCustomerKey), TimeSpan.FromSeconds(60)))
                        {
                            var posSetting = await GetPosSettingAsync(connectStringName, data);

                            var existsCustomer = await GetCustomer(connectStringName, phone, posSetting.ManagerCustomerByBranch, data);
                            if (existsCustomer == null)
                            {
                                try
                                {
                                    var customerInsert = new OmniChannelCore.Api.Sdk.Models.Customer
                                    {
                                        ContactNumber = phone,
                                        Name = order.CustomerName,
                                        Address = order.CustomerAddress,
                                        BranchId = data.BranchId,
                                        RetailerId = data.RetailerId
                                    };
                                    if (!string.IsNullOrEmpty(order.CustomerLocation))
                                    {
                                        customerInsert.LocationName = order.CustomerLocation;
                                    }

                                    if (!string.IsNullOrEmpty(order.CustomerWard))
                                    {
                                        customerInsert.WardName = order.CustomerWard;
                                    }

                                    newOrderCustomerId = await _customerInternalClient.CreateCustomerAsync(coreContext, customerInsert);
                                }
                                catch (Exception ex)
                                {
                                    existsCustomer = await GetCustomer(connectStringName, phone, posSetting.ManagerCustomerByBranch, data);

                                    newOrderCustomerId = existsCustomer?.Id;
                                }
                            }
                            else
                            {
                                newOrderCustomerId = existsCustomer?.Id;
                            }
                        }

                        if (newOrderCustomerId != null && newOrderCustomerId > 0)
                        {
                            orderUpdatedCusRow = await _orderInternalClient.UpdateCustomer(coreContext, kvOrder.Id, newOrderCustomerId.Value);
                            kvOrder.CustomerId = newOrderCustomerId.Value;
                        }
                    }
                }
            }
        }

        #region Private method
        private async Task<PosSetting> GetPosSettingAsync(string connectStringName, CreateInvoiceMessage data)
        {
            PosSetting posSetting = null;

            using (var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var posSettingRepository = new PosSettingRepository(db);
                posSetting = new PosSetting(await posSettingRepository.GetSettingAsync(data.RetailerId), data.RetailerId, DbConnectionFactory, _cacheClient, Settings);
            }
            return posSetting;
        }

        private async Task<Domain.Model.Customer> GetCustomer(string connectStringName, string phone,
            bool isManagerCustomerByBranch, CreateInvoiceMessage data)
        {
            Domain.Model.Customer customer = null;

            using (var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var customerRepository = new CustomerRepository(db);
                customer = await customerRepository.GetByFilterAsync(data.RetailerId, data.BranchId, phone, null, isManagerCustomerByBranch);
            }
            return customer;
        }

        #endregion
    }
}

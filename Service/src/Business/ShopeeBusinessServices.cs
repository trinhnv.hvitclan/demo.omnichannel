﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Interfaces.NewBase;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.Domain.Common;
using Demo.OmniChannel.DomainService.Interfaces;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Sdk.Interfaces;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.EventLogMessage;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.ShareKernel.RedisStreamMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Newtonsoft.Json;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using ServiceStack.Redis;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SDKCCoreInterface = Demo.OmniChannelCore.Api.Sdk.Interfaces;


namespace Demo.OmniChannel.Business.Implements
{
    public class ShopeeBusinessServices : BaseCreateOrderService<ShopeeCreateOrderKafkaMessageV2>, IShopeeBusinessInterface
    {
        public List<string> ShopeeNeedLogisticStatusLst = new List<string>() { ChannelClient.Common.ShopeeStatus.Processed, ChannelClient.Common.ShopeeStatus.Shipped };
        private readonly IShopeeBusinessClient _shopeeBusinessClient;
        private readonly IIntegrationEventService _integrationEventService;
        public ShopeeBusinessServices(
            IIntegrationEventService integrationEventService,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IDeliveryInfoInternalClient deliveryInfoInternalClient,
            IKvLockRedis kvLockRedis,
             IAppSettings appSettings,
             IMessageService messageService,
             IIntegrationInternalClient integrationInternalClient,
             IRetryOrderMongoService retryOrderMongoService,
             IAuditTrailInternalClient auditTrailInternalClient,
             ISendoLocationMongoService sendoLocationMongoService,
             ISurChargeInternalClient surChargeInternalClient,
             ISurChargeBranchInternalClient surChargeBranchInternalClient,
             IProductMappingService productMappingService,
             IOmniChannelSettingService omniChannelSettingService,
             ICustomerInternalClient customerInternalClient,
             IOrderMongoService orderMongoService,
             IPartnerDeliveryInternalClient partnerInternalClient,
             SDKCCoreInterface.IOrderInternalClient orderInternalClient,
             IRetryInvoiceMongoService retryInvoiceMongoService,
             IInvoiceMongoService invoiceMongoService,
             IRedisClientsManager redisClientsManager,
             IShopeeBusinessClient shopeeBusinessClient,
             ICreateOrderDomainService createOrderDomainService) : base(
                 integrationEventService, dbConnectionFactory, redisClientsManager,
                 cacheClient, orderMongoService, invoiceMongoService,
                 retryOrderMongoService, retryInvoiceMongoService,
                 auditTrailInternalClient, surChargeInternalClient,
                 omniChannelSettingService, productMappingService,
                 sendoLocationMongoService, surChargeBranchInternalClient,
                 integrationInternalClient, customerInternalClient,
                 orderInternalClient, partnerInternalClient, kvLockRedis,
                 messageService, appSettings, deliveryInfoInternalClient,
                 createOrderDomainService)
        {
            _shopeeBusinessClient = shopeeBusinessClient;
            OmniChannelType = (byte)ChannelType.Shopee;
            _integrationEventService = integrationEventService;
        }


        protected override void PushMessageToOrderTrackingEvent(ShopeeCreateOrderKafkaMessageV2 message, string error = "")
        {
            if (message?.Source != null && message.Source.ToLower().Equals(ProcessOrderSource.WebHook))
            {
                string exChangeTrackProcessExChange = _appSettings.Get<string>("OrderTrackingEventExChange");
                var eventModel = new TrackModelEvent
                {
                    Code = message.Code,
                    OrderSn = message.OrderSn,
                    ErrorMessage = error,
                    Status = ProcessOrderStatus.Processed,
                    OrderTimestamp = message.Timestamps ?? 0,
                    ShopId = message.ShopId,
                    Source = message.Source,
                    ProcessType = ProcessFlowType.Order
                };
                _integrationEventService.AddEventWithRoutingKeyAsync(new OrderTrackingEvent
                {
                    TrackModelEvent = eventModel,
                }, RoutingKey.OrderTrackingEvent, exChangeTrackProcessExChange);
            }
        }

        protected override ShopeeCreateOrderKafkaMessageV2 DeserializeRequest(string request)
        {
            return JsonConvert.DeserializeObject<ShopeeCreateOrderKafkaMessageV2>(request);
        }

        protected override KvOrder GenKvOrder(KvInternalContext context, OmniChannelSettingObject setting, ShopeeCreateOrderKafkaMessageV2 message)
        {
            return _shopeeBusinessClient.GenKvOrderV2WithLogistics(message.Order, message.PaymentEscrowDetail, message.Tracking, setting, context.BranchId, context.UserId, context.RetailerId, context.Group?.Id ?? 0);
        }

        protected override string GetOrderId(ShopeeCreateOrderKafkaMessageV2 message)
        {
            return message?.Order?.OrderSn;
        }

        protected override void PushRetryMessage(string message, byte channelType)
        {
            string orderString = string.Empty;
            string orderId = string.Empty;
            var orderShopee = JsonConvert.DeserializeObject<ShopeeDeadLetterQueueCreateOrderV2>(message);
            if (orderShopee.RetryCount > MAX_RETRY) return;
            orderShopee.RetryCount++;
            orderId = orderShopee?.Order.OrderSn;
            orderString = JsonConvert.SerializeObject(orderShopee);
            var kafkaConfig = _appSettings.Get<Kafka>("Kafka");
            var topicName = kafkaConfig.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.DLQCreateOrder &&
            x.ChannelType == channelType)?.Name;
            KafkaClient.KafkaClient.Instance.PublishMessage($"{topicName}", orderId.ToMD5Hash(), orderString);
        }

        protected override Task<List<Domain.Model.OmniChannel>> FilterChannelWithConfig(List<Domain.Model.OmniChannel> channels)
        {
            var configshopeeOptimizeV2Feature = _appSettings.Get<UseShopeeOptimizeV2Feature>("UseShopeeOptimizeV2Feature");
            if (configshopeeOptimizeV2Feature != null && configshopeeOptimizeV2Feature.Enable && (configshopeeOptimizeV2Feature.IncludeRetail.Count != 0))
            {
                channels = channels.Where(item => configshopeeOptimizeV2Feature.IncludeRetail.Contains(item.RetailerId)).ToList();
            }
            return Task.FromResult(channels);
        }

        protected override void PushLishKafkaAutoCreateProduct(KvOrder kvOrder, Domain.Model.OmniChannel channel, KvInternalContext context, OmniChannelSettingObject settings)
        {
            if (settings.IsAutoCreatingProduct)
            {
                var syncOrderWithAutoCreateProduct = new SyncOrderWithAutoCreateProduct
                {
                    KvOrder = JsonConvert.SerializeObject(kvOrder),
                    KvEntities = context.Group.ConnectionString,
                    RetailerId = context.RetailerId,
                    BranchId = context.BranchId,
                    ChannelId = channel.Id,
                    LogId = context.LogId
                };
                var kafkaConfig = _appSettings.Get<Kafka>("Kafka");
                var topicName = kafkaConfig.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.CreateOrderWithAutoCreateProduct && x.ChannelType == channel.Type)?.Name;
                KafkaClient.KafkaClient.Instance.PublishMessage($"{topicName}",
                    string.Empty, JsonConvert.SerializeObject(syncOrderWithAutoCreateProduct));
                PushLishMQToCreateLog((int)LogType.Info, context.LogId, "PushLishKafkaAutoCreateProduct", kvOrder.Ordersn, null, context.RetailerId, channel.Id, null, null);
            }
        }
        protected override async Task<bool> SkipCreateCustomer(KvInternalContext context)
        {
            ShareKernel.Dto.OmniPlatformSettingDto settings;

            using (var db = await _dbConnectionFactory.OpenAsync())
            {
                var omniPlatformSettingRepository = new OmniPlatformSettingRepository(db);

                settings =
                    await omniPlatformSettingRepository.GetByRetailerAsync(context.RetailerId);
            }

            if (!settings.IsShopeeSyncCustomer) return true;
            return false;
        }

        protected override bool ValidateMessage(ShopeeCreateOrderKafkaMessageV2 message)
        {
            var orderDetail = message.Order;
            var trackingInfo = message.Tracking;
            var escrowDetails = message.PaymentEscrowDetail;
            if (orderDetail == null || string.IsNullOrEmpty(orderDetail.OrderStatus))
            {
                throw new OmniValidateOrderException("order detail or status order is null, add to retry order");
            }
            if (escrowDetails == null || escrowDetails.OrderIncome == null)
            {
                throw new OmniValidateOrderException("escrowDetails is null, add to retry order");
            }
            var orderStatus = orderDetail.OrderStatus;
            if (orderDetail.ShippingCarrier == ShopeeShipingCarrier.SellerShipping)
            {
                return true;
            }
            var lowerLstStatusValidateWithTracking = ShopeeNeedLogisticStatusLst.Select(item => item.ToLower()).ToList();
            if (lowerLstStatusValidateWithTracking.Contains(orderStatus.ToLower()) && (trackingInfo == null || string.IsNullOrEmpty(trackingInfo.TrackingNumber)))
            {
                throw new OmniValidateOrderException("tracking number is null, need tracking number to process invoice, add to retry order");
            }
            return true;
        }
    }
}

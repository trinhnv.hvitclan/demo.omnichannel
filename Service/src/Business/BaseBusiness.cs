﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Impls;
using Demo.OmniChannel.ShareKernel.Common;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System;
using System.Threading.Tasks;
using ChannelType = Demo.OmniChannel.Sdk.Common.ChannelType;

namespace Demo.OmniChannel.Business
{
    public abstract class BaseBusiness
    {
        protected IAppSettings Settings;

        protected IDbConnectionFactory DbConnectionFactory;

        protected IMessageService MessageService;

        protected BaseBusiness(
            IAppSettings settings,
            IDbConnectionFactory dbConnectionFactory,
            IMessageService messageService)
        {
            Settings = settings;
            DbConnectionFactory = dbConnectionFactory;
            MessageService = messageService;
        }
        protected async Task DeActiveChannel(long channelId, byte channelType, KvInternalContext kvInternalContext)
        {
            var integrationInternalClient = new IntegrationInternalClient(Settings);
            await integrationInternalClient.DeactivateChannel(
                kvInternalContext.ConvertTo<KvInternalContext>(), channelId, EnumHelper.ToDescription((ChannelType)channelType));
        }

        protected void PushLishMQToCreateLog(int typeOfLog, Guid id, string action, string requestObject,
            string responseObject, int? retailerId, long? omniChannelId, string message, Exception exception)
        {
            SeriLogObject log = new SeriLogObject(id)
            {
                Action = action,
                RequestObject = requestObject,
                ResponseObject = responseObject,
                RetailerId = retailerId,
                OmniChannelId = omniChannelId
            };
            switch (typeOfLog)
            {
                case (int)LogType.Warning:
                    log.Description = message;
                    log.LogWarning();
                    break;
                case (int)LogType.Error:
                    log.LogError(exception);
                    break;
                default:
                    log.Description = message;
                    log.LogInfo();
                    break;
            }
        }
    }
}

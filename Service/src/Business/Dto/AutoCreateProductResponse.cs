﻿using System.Collections.Generic;

namespace Demo.OmniChannel.Business.Dto
{
    public class AutoCreateProductResponse
    {
        public List<long> KvProductIds { get; set; } = new List<long> { };

        public bool IsSuccess { get; set; }
    }

    public class CreateProductNormalReponse
    {
        public List<long> KvProductIds { get; set; } = new List<long> { };
        public bool IsSuccess { get; set; }
    }

    public class CreateProductVariationsReponse
    {
        public List<long> KvProductIds { get; set; } = new List<long> { };
        public bool IsSuccess { get; set; }
    }
}

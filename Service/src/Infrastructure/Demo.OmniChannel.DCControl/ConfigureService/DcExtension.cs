﻿using Demo.OmniChannel.MongoService.Impls;
using Demo.OmniChannel.MongoService.Interface;
using Microsoft.Extensions.DependencyInjection;
using ServiceStack;

namespace Demo.OmniChannel.DCControl.ConfigureService
{
    public static class DcExtension
    {
        /// <summary>
        /// Register DC Control for OmniChannel API - Leave this register in the end of ConfigureServices function.
        /// </summary>
        /// <param name="appHost"></param>
        /// <param name="options"></param>
        public static void RegisterDcControl(this IAppHost appHost, ApiDcInputOptions options)
        {
            options.Container.AddScoped<IDcInfosService, DcInfosService>();
            ApiDcControl.Register(options);
        }

        /// <summary>
        /// Register DC Control for OmniChannel Application - Leave this register in the end of ConfigureServices function.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="options"></param>
        public static void RegisterApplicationDcControl(this IServiceCollection services, ApplicationDcInputOptions options)
        {
            services.AddScoped<IDcInfosService, DcInfosService>();
            var provider = services.BuildServiceProvider();
            ApplicationDcControl.Register(options, provider);
        }
    }
}

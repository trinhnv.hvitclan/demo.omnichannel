﻿using ServiceStack.Configuration;

namespace Demo.OmniChannel.DCControl.ConfigureService
{
    public class BaseDcInputOptions
    {
        public string ServiceName { get; set; }
        public IAppSettings AppSettings { get; set; }
    }
}

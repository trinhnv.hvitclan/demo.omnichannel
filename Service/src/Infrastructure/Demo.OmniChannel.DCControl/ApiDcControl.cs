﻿using Demo.OmniChannel.DCControl.Adapters;
using Demo.OmniChannel.DCControl.Common;
using Demo.OmniChannel.DCControl.ConfigureService;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Serilog;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Data;

namespace Demo.OmniChannel.DCControl
{
    public class ApiDcControl
    {
        public static ApiDcControl Instance => Nested.instance;
        public IAppSettings AppSettings { get; set; }
        private IAdapter _adapter;
        public bool IsRunning { get; set; }
        private static class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested()
            {
            }
            // ReSharper disable once InconsistentNaming
            internal static readonly ApiDcControl instance = new ApiDcControl();
        }

        private ApiDcControl()
        {
            _adapter = new NoneConfigAdapter();
        }

        public static bool Register(ApiDcInputOptions options)
        {
            try
            {
                var dcControlConfig = options.AppSettings.Get<DCControlConfig>("DCControl");
                var redisConfig = options.AppSettings.Get<KvRedisConfig>("redis:cache");

                Instance.AppSettings = options.AppSettings;

                if (!dcControlConfig.IsActive) return false;
                var redisClientManager = KvRedisPoolManager.GetClientsManager(redisConfig);
                if (redisClientManager == null) {
                    Log.Information($"ApiDcControl not started because: redisClientManager null");
                    return false;
                }

                var dcInfosService = HostContext.AppHost.Container.TryResolve<IDcInfosService>();
                if (dcInfosService == null) { 
                    Log.Information($"ApiDcControl not started because: dcInfosService null");
                    return false; 
                }

                var dbConnectionFactory = HostContext.AppHost.Container.TryResolve<IDbConnectionFactory>();
                if (dbConnectionFactory == null) { 
                    Log.Information($"ApiDcControl not started because: dbConnectionFactory null");
                    return false;
                }

                if (options.AppSettings == null) {
                    Log.Information($"ApiDcControl not started because: AppSettings null");
                    return false; 
                }

                IAdapter dcAdapter = new DcAdapter(redisClientManager, dcInfosService, options, dbConnectionFactory);
                Instance.UseAdapter(dcAdapter, options);
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
           
        }

        public void UseAdapter(IAdapter adapter, ApiDcInputOptions options)
        {
            _adapter = adapter;
            _adapter.StartSubcription();
            IsRunning = true;
        }

        public string GetDcStringName(int groupId)
        {
            return _adapter.GetDcStringName(groupId);
        }
    }
}

﻿using Demo.OmniChannel.DCControl.Common;
using Demo.OmniChannel.DCControl.ConfigureService;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.ShareKernel.Common;
using Polly;
using Serilog;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.DCControl.Adapters
{
    public class DcAdapter : IAdapter
    {
        private readonly IRedisClientsManager _redisClientManager;
        private readonly IDcInfosService _dcInfosService;
        private readonly IAppSettings _appSettings;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly BaseDcInputOptions _dcInputOptions;

        private Dictionary<string, DcInfos> _dcInfos;

        public Dictionary<string, DcInfos> DcInfos
        {
            get
            {
                if (_dcInfos == null)
                {
                    _dcInfos = new Dictionary<string, DcInfos>();
                    return _dcInfos;
                }
                return _dcInfos;
            }
            set { _dcInfos = value; }
        }

        public DcAdapter(IRedisClientsManager redisClientManager, IDcInfosService dcInfosService, BaseDcInputOptions dcInputOptions, IDbConnectionFactory dbConnectionFactory)
        {
            _redisClientManager = redisClientManager;
            _dcInfosService = dcInfosService;
            _dcInputOptions = dcInputOptions;
            _appSettings = dcInputOptions.AppSettings;
            _dbConnectionFactory = dbConnectionFactory;
        }

        public void StartSubcription()
        {
            var dcControlConfig = _appSettings.Get<DCControlConfig>("DCControl");
            ReRegisterOrmConnectionString();
            Task.Run(() =>
            {
                PollyReSubSubcription(dcControlConfig);
            });
        }

        private void PollyReSubSubcription(DCControlConfig dcControlConfig)
        {
            try
            {
                var polly = Policy
                    .Handle<DCControlUnSubscribedException>()
                    .WaitAndRetry(10, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));

                polly.Execute(() =>
                {
                    StartPollySubcription(dcControlConfig);
                });
            }
            catch (Exception ex)
            {
                Log.Warning($"DCCONTROL - {_dcInputOptions.ServiceName} Redis Channel {ex.Message}");
            }

        }

        private void StartPollySubcription(DCControlConfig dcControlConfig)
        {
            try
            {
                using (var redisClient = _redisClientManager.GetClient())
                {
                    using (var subscription = redisClient.CreateSubscription())
                    {
                        subscription.OnSubscribe = channel =>
                        {
                            Log.Information($"DCCONTROL - {_dcInputOptions.ServiceName} Subscribed to Redis Channel: '{channel}'");
                        };
                        subscription.OnUnSubscribe = channel =>
                        {
                            Log.Warning($"DCCONTROL - {_dcInputOptions.ServiceName} UnSubscribed to Redis Channel '{channel}'");
                            var exception = new DCControlUnSubscribedException($"UnSubscribed {_dcInputOptions.ServiceName} UnSubscribed to Redis Channel");
                            throw exception;
                        };
                        subscription.OnMessage = (channel, msg) =>
                        {
                            Log.Information($"DCCONTROL - {_dcInputOptions.ServiceName} Received '{msg}' from channel '{channel}'");
                            ReRegisterOrmConnectionString();
                        };
                        while (true)
                        {
                            subscription.SubscribeToChannels(dcControlConfig.RedisSubcribeChannelName);
                        }
                    }
                }
            }
            catch (DCControlUnSubscribedException ex)
            {
                Log.Warning($"DCControlUnSubscribedException - {ex.Message}");
                throw;
            }
        }


        private void ReRegisterOrmConnectionString()
        {
            //Step 1: Get newest DC information from MongoDb
            GetDcInformationFromMongo();
            //Step 2: Re-assign connection string DbConnectionString Factory
            ReRegisterConnectionString();
        }


        private void ReRegisterConnectionString()
        {
            var kvSql = _appSettings.Get<KvSqlConnectString>("SqlConnectStrings");
            foreach (var dcInfos in DcInfos)
            {
                //Xác định groupId này ở DC nào dựa vào Key của dictionary
                List<KvEntity> connectionStrings;
                switch (dcInfos.Key?.ToLower())
                {
                    case "dc1":
                        connectionStrings = kvSql.KvEntitiesDC1;
                        break;
                    case "dc1p":
                        connectionStrings = kvSql.KvEntitiesDC1p;
                        break;
                    case "dc2":
                        connectionStrings = kvSql.KvEntitiesDC2;
                        break;
                    default:
                        continue;
                }

                foreach (var groupId in dcInfos.Value.GroupIds)
                {
                    //Get Connection String 
                    var connectionString = connectionStrings.FirstOrDefault(c => c.Name.Equals($"KVEntities{groupId}"));
                    if (connectionString == null) continue;
                    //Đăng kí lại connectionString cho dbConnectionFactory
                    _dbConnectionFactory.RegisterConnection($"KVEntities{groupId}".ToLower(), connectionString.Value, SqlServer2016Dialect.Provider);
                    Log.Information($"DCCONTROL - {_dcInputOptions.ServiceName} Update GroupId: {groupId} {dcInfos.Key} with ConnectionString: {connectionString.Value}");
                }
            }
        }

        private void GetDcInformationFromMongo()
        {
            var dcInfos = _dcInfosService.GetAll().GetResult().ToList();
            Log.Information($"DCCONTROL - {_dcInputOptions.ServiceName} GET Data from MongoDb: {dcInfos.ToList().ToSafeJson()}");
            foreach (var element in dcInfos)
            {
                DcInfos[element.Name] = element;
            }
        }

        public string GetDcStringName(int groupId)
        {
            foreach (var dcInfos in DcInfos)
            {
                if (dcInfos.Value.GroupIds.Contains(groupId))
                    return dcInfos.Key.ToLower();
            }
            return null;
        }
    }
}

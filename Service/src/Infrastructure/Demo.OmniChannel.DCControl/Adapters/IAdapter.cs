﻿

namespace Demo.OmniChannel.DCControl.Adapters
{
    public interface IAdapter
    {
        void StartSubcription();
        string GetDcStringName(int groupId);
    }
}

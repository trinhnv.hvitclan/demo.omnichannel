﻿

namespace Demo.OmniChannel.DCControl.Adapters
{
    public class NoneConfigAdapter : IAdapter
    {
        public string GetDcStringName(int groupId)
        {
            return "";
        }

        public void StartSubcription()
        {
            //do nothing
        }
    }
}

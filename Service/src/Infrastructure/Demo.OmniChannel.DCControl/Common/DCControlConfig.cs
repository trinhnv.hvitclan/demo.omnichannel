﻿

namespace Demo.OmniChannel.DCControl.Common
{
    public class DCControlConfig
    {
        public bool IsActive { get; set; }
        public string RedisSubcribeChannelName { get; set; }
    }
}

﻿using System;

namespace Demo.OmniChannel.DCControl.Common
{
    public class DCControlUnSubscribedException : Exception
    {
        public DCControlUnSubscribedException(string msg) : base(msg)
        {
        }
    }
}

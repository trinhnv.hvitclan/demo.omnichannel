﻿using Demo.OmniChannel.DCControl.Adapters;
using Demo.OmniChannel.DCControl.Common;
using Demo.OmniChannel.DCControl.ConfigureService;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Microsoft.Extensions.DependencyInjection;
using ServiceStack.Configuration;
using ServiceStack.Data;
using System;

namespace Demo.OmniChannel.DCControl
{
    public class ApplicationDcControl
    {
        public static ApplicationDcControl Instance => Nested.instance;
        public IAppSettings AppSettings { get; set; }
        private IAdapter _adapter;
        public bool IsRunning{ get; set; }
        private static class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested()
            {
            }
            // ReSharper disable once InconsistentNaming
            internal static readonly ApplicationDcControl instance = new ApplicationDcControl();
        }

        private ApplicationDcControl()
        {
            _adapter = new NoneConfigAdapter();
        }

        public static bool Register(ApplicationDcInputOptions options, ServiceProvider serviceProvider)
        {
            try
            {
                var dcControlConfig = options.AppSettings.Get<DCControlConfig>("DCControl");
                var redisConfig = options.AppSettings.Get<KvRedisConfig>("redis:cache");

                Instance.AppSettings = options.AppSettings;

                if (dcControlConfig.IsActive)
                {
                    var redisClientManager = KvRedisPoolManager.GetClientsManager(redisConfig);
                    if (redisClientManager == null) return false;

                    var dcInfosService = serviceProvider.GetService<IDcInfosService>();
                    if (dcInfosService == null) return false;

                    var dbConnectionFactory = serviceProvider.GetService<IDbConnectionFactory>();
                    if (dbConnectionFactory == null) return false;

                    if (options.AppSettings == null) return false;

                    IAdapter dcAdapter = new DcAdapter(redisClientManager, dcInfosService, options, dbConnectionFactory);
                    Instance.UseAdapter(dcAdapter, options);
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void UseAdapter(IAdapter adapter, ApplicationDcInputOptions options)
        {
            _adapter = adapter;
            _adapter.StartSubcription();
            IsRunning = true;
        }

        public string GetDcStringName(int groupId)
        {
            return _adapter.GetDcStringName(groupId);
        }
    }
}

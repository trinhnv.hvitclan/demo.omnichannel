﻿USE KVOmniChannel;

ALTER TABLE [dbo].[OmniChannelAuth]
    ADD [ExpiryDate] DATETIME2 (7) NULL;


UPDATE [dbo].[OmniChannelAuth] SET [ExpiryDate] = DATEADD(ss,[ExpiresIn],[CreatedDate]) WHERE [ExpiresIn] > 0 AND [ModifiedDate] IS NULL
UPDATE [dbo].[OmniChannelAuth] SET [ExpiryDate] = DATEADD(ss,[ExpiresIn],[ModifiedDate]) WHERE [ExpiresIn] > 0 AND [ModifiedDate] IS NOT NULL

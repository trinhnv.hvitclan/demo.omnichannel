﻿CREATE TABLE [dbo].[OmniChannelAuth] (
    [Id]                BIGINT         IDENTITY (1, 1) NOT NULL,
    [ShopId]            BIGINT         NOT NULL,
    [AccessToken]       NVARCHAR (1000) NULL,
    [RefreshToken]      NVARCHAR (1000) NULL,
    [ExpiresIn]         INT            NOT NULL,
    [RefreshExpiresIn]  INT            NOT NULL,
    [CreatedDate]       DATETIME2 (7)  NOT NULL,
    [ModifiedDate]      DATETIME2 (7)  NULL,
    [CreateBy]          BIGINT         NOT NULL,
    [ModifiedBy]        BIGINT         NULL,
    [OmniChannelId]     BIGINT         NULL,
    [ExpiryDate]		DATETIME2 NULL, 
    CONSTRAINT [PK_OmniChannelAuth] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_OmniChannelAuth_OmniChannel_OmniChannelId] FOREIGN KEY ([OmniChannelId]) REFERENCES [dbo].[OmniChannel] ([Id])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_OmniChannelAuth_OmniChannelId]
    ON [dbo].[OmniChannelAuth]([OmniChannelId] ASC) WHERE ([OmniChannelId] IS NOT NULL);


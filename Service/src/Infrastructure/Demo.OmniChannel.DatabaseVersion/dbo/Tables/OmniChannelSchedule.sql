﻿CREATE TABLE [dbo].[OmniChannelSchedule] (
    [Id]            BIGINT        IDENTITY (1, 1) NOT NULL,
    [NextRun]       DATETIME2 (7) NULL,
    [LastSync]      DATETIME2 (7) NULL,
    [IsRunning]     BIT           NOT NULL,
    [Type]          INT           NOT NULL,
    [OmniChannelId] BIGINT        NOT NULL,
    CONSTRAINT [PK_OmniChannelSchedule] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_OmniChannelSchedule_OmniChannel_OmniChannelId] FOREIGN KEY ([OmniChannelId]) REFERENCES [dbo].[OmniChannel] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_OmniChannelSchedule_OmniChannelId]
    ON [dbo].[OmniChannelSchedule]([OmniChannelId] ASC);


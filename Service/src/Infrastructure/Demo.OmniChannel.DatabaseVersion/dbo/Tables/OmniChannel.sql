﻿CREATE TABLE [dbo].[OmniChannel] (
    [Id]                   BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]                 NVARCHAR (255) NULL,
    [Email]                NVARCHAR (255) NULL,
    [IsActive]             BIT            NOT NULL,
    [Status]               TINYINT        NULL,  
    [CreatedDate]          DATETIME2 (7)  NOT NULL,
    [ModifiedDate]         DATETIME2 (7)  NULL,
    [CreateBy]             BIGINT         NOT NULL,
    [ModifiedBy]           BIGINT         NULL,
    [Type]                 TINYINT        NOT NULL,
    [BranchId]             INT            NOT NULL,
    [RetailerId]           INT            NOT NULL,
    [PriceBookId]          BIGINT         NULL,
    [BasePriceBookId]      BIGINT         NULL,
    [SyncOnHandFormula]    TINYINT        NOT NULL,
    [SyncOrderFormula]     INT            NULL,
    [IsApplyAllFormula]    BIT            NOT NULL,
    [IsAutoMappingProduct] BIT            NOT NULL,
    [IsDeleted]            BIT            NULL,
    [IdentityKey]          NVARCHAR (50) NULL,
    [RegisterDate]         DATETIME2 (7)  NULL,
    [ExtraKey]          NVARCHAR (50) NULL,
    [IsAutoDeleteMapping] BIT NULL, 
    CONSTRAINT [PK_OmniChannel] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex_OmniChannelApp]
    ON [dbo].[OmniChannel]([RetailerId] ASC)
    INCLUDE([BranchId], [PriceBookId]);


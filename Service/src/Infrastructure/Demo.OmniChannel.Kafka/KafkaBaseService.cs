﻿using Confluent.Kafka;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Polly;
using Polly.Retry;
using Polly.Timeout;
using Serilog;
using ServiceStack;
using ServiceStack.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Kafka
{
    public abstract class KafkaBaseService<T> : IHostedService where T : BaseKafkaModel
    {

        protected string TopicName { get; set; }
        protected int ConsumerSize { get; set; }
        protected IAppSettings Settings;
        protected Demo.OmniChannel.Utilities.Kafka Kafka;
        private readonly List<Task> _listTask = new List<Task>();
        private readonly CancellationTokenSource _stoppingCts = new CancellationTokenSource();

        protected KafkaBaseService(IAppSettings settings)
        {
            Kafka = KafkaClient.KafkaClient.Instance.GetKafka();
            Settings = settings;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Log.Information($"----------------------------------------- start Subscribe {typeof(T).Name} -----------------------------------------------");
            for (var i = 0; i < ConsumerSize; i++)
            {
                _listTask.Add(Task.Factory.StartNew(Execute, _stoppingCts.Token, TaskCreationOptions.LongRunning, TaskScheduler.Current));
                Log.Information("consumer created");
            }
            return Task.CompletedTask;
        }

        private async Task Execute()
        {
            var policy = GetAsyncRetryPolicy();
            await policy.ExecuteAsync(async () => await SubExcute()); // async execute overload

        }

        private AsyncRetryPolicy GetAsyncRetryPolicy()
        {
            var retries = 1;
            return Policy.Handle<Exception>()
                .WaitAndRetryForeverAsync(
                    attempt => TimeSpan.FromSeconds(0.1 * Math.Pow(2, attempt)), // Back off!  200, 400, 800, 1600, 3200, 6400, 12800 (ms)
                    (exception, calculatedWaitDuration) =>  // Capture some info for logging!
                    {
                        retries++;
                        if (retries > 5)
                        {
                            Log.Warning($"URGENT consumer {TopicName} may have retry more than 5 times + {exception.Message} + {exception.StackTrace}");
                        }
                    });
        }

        protected abstract Task<bool> ProcessMessage(T data, bool isWriteLog = false);

        protected async virtual Task<bool> ProcessMessageWithoutDeserialize(string data, bool isWriteLog = false)
        {
            var message = JsonConvert.DeserializeObject<T>(data);
            return await ProcessMessage(message);
        }


        private async Task SubExcute()
        {
            var consumerBuilder = new ConsumerBuilder<Ignore, string>(KafkaClient.KafkaClient.Instance.GetKafkaConsumerConfig());
            consumerBuilder.SetLogHandler((_, e) =>
            {
                if (Kafka.IsLogHandler)
                {
                    Log.Information($"Kafka consumer log handler - {e.Name}, {e.Facility}, {e.Level.ToString()}, message: {e.Message}");
                }

            }).SetErrorHandler((_, e) =>
            {
                Log.Error($"Kafka consumer error - {e}");
            });
            using (var consumer = consumerBuilder.Build())
            {
                consumer.Subscribe(TopicName);
                try
                {
                    while (true)
                    {
                        try
                        {
                            var consumeResult = consumer.Consume();
                            try
                            {
                                var timeoutPolicy = Policy.TimeoutAsync(Settings.Get("kafka.MaxPollIntervalMs", 300000) / 1000 - 10, TimeoutStrategy.Pessimistic);
                                await timeoutPolicy.ExecuteAsync(async () =>
                                {
                                    await ProcessMessageWithoutDeserialize(consumeResult.Message.Value);
                                });

                            }
                            catch (Exception e)
                            {
                                Log.Error($"Kafka exception: {e.Message}", e);
                            }

                            var isAutoCommit = Kafka?.IsAutoCommit ?? false;
                            if (!isAutoCommit)
                            {
                                if (consumeResult.Offset % Settings.Get("kafka.CommitPeriod", 5) != 0) continue;
                                try
                                {
                                    consumer.Commit(consumeResult);
                                }
                                catch (KafkaException e)
                                {
                                    Log.Error($"Kafka Consume Commit error: {e} , Message length: {consumeResult.Message?.Value.Length}", e);
                                }
                            }
                        }
                        catch (ConsumeException ce)
                        {
                            Log.Error($"ConsumeException: {ce.Message} | Topic: {TopicName}", ce);
                            consumer.Close();
                            throw;
                        }
                    }
                }
                catch (OperationCanceledException e)
                {
                    Log.Error($"Consumer is Down: {e.ToJson()}", e);
                    consumer.Close();
                }
            }
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            Log.Information("Consummer is going down");
            if (!_listTask.Any())
            {
                return;
            }
            try
            {
                _stoppingCts.Cancel();
            }
            finally
            {
                await Task.WhenAll(_listTask);
            }
        }
    }
}

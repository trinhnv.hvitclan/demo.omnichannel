﻿using Demo.OmniChannel.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Demo.OmniChannel.Infrastructure.Persistence.Ef.EntityConfigurations
{
    internal class OmniChannelPlatformEntityConfig : IEntityTypeConfiguration<OmniChannelPlatform>
    {
        public void Configure(EntityTypeBuilder<OmniChannelPlatform> builder)
        {
            builder.ToTable("OmniChannelPlatform").HasKey(x => x.Id);
        }
    }
}
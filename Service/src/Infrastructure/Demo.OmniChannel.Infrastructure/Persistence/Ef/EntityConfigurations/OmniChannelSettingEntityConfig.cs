﻿using Demo.OmniChannel.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Demo.OmniChannel.Infrastructure.Persistence.Ef.EntityConfigurations
{
    internal class OmniChannelSettingEntityConfig : IEntityTypeConfiguration<OmniChannelSetting>
    {
        public void Configure(EntityTypeBuilder<OmniChannelSetting> builder)
        {
            builder.ToTable("OmniChannelSetting").HasKey(x => x.Id);

            builder.ToTable("OmniChannelSetting")
                .HasIndex(p => new { p.OmniChannelId, p.RetailerId })
                .HasName("NonClusteredIndex_OmniChannelSetting_OmniChannelId");

            builder.ToTable("OmniChannelSetting")
                .HasIndex(p => new { p.OmniChannelId, p.Name })
                .HasName("NonClusteredIndex_OmniChannelSetting-OmniChannelId-Name")
                .IsUnique();
        }
    }
}
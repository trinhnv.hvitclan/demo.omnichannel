﻿using Demo.OmniChannel.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Demo.OmniChannel.Infrastructure.Persistence.Ef.EntityConfigurations
{
    internal class OmniChannelAuthEntityConfig : IEntityTypeConfiguration<OmniChannelAuth>
    {
        public void Configure(EntityTypeBuilder<OmniChannelAuth> builder)
        {
            builder.ToTable("OmniChannelAuth").HasKey(x => x.Id);

            builder
                .HasOne(v => v.OmniChannel)
                .WithOne(f => f.OmniChannelAuth)
                .HasForeignKey<OmniChannelAuth>(c => c.OmniChannelId);

            builder.HasIndex(p => new { p.OmniChannelId })
                      .IsUnique()
                      .HasFilter("[OmniChannelId] IS NOT NULL");

            builder.ToTable("OmniChannelAuth")
              .HasIndex(p => new { p.ShopId })
              .HasName("NonClusteredIndex_OmniChannelAuth_ShopId");
        }
    }
}
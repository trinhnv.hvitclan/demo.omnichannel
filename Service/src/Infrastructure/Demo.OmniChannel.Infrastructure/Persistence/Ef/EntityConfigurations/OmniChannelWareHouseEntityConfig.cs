﻿using Demo.OmniChannel.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Demo.OmniChannel.Infrastructure.Persistence.Ef.EntityConfigurations
{
    internal class OmniChannelWareHouseEntityConfig : IEntityTypeConfiguration<OmniChannelWareHouse>
    {
        public void Configure(EntityTypeBuilder<OmniChannelWareHouse> builder)
        {
            builder.ToTable("OmniChannelWareHouse").HasKey(x => x.Id);

            builder.ToTable("OmniChannelWareHouse")
                .HasIndex(p => new { p.RetailerId , p.OmniChannelId })
                .HasName("NonClusteredIndex_OmniChannelWareHouse_OmniChannelId");
        }
    }
}

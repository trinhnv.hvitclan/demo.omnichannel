﻿using Demo.OmniChannel.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Demo.OmniChannel.Infrastructure.Persistence.Ef.EntityConfigurations
{
    internal class OmniChannelEntityConfig : IEntityTypeConfiguration<Domain.Model.OmniChannel>
    {
        public void Configure(EntityTypeBuilder<Domain.Model.OmniChannel> builder)
        {
            builder.ToTable("OmniChannel")
                .HasKey(x => x.Id);
            builder.Ignore(x => x.OmniChannelAuth);
            builder.Ignore(x => x.OmniChannelSchedules);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.RetailerId).IsRequired();
            builder.Property(x => x.BranchId).IsRequired();
            builder.Property(x => x.Type).IsRequired();

            builder.ToTable("OmniChannel")
                .HasIndex(c => c.RetailerId)
                .Include<Domain.Model.OmniChannel>(c => new
                {
                    c.BranchId,
                    c.PriceBookId
                })
                .HasName("NonClusteredIndex_OmniChannelApp");

            builder.ToTable("OmniChannel")
                .HasIndex(p => new { p.IdentityKey, p.PlatformId })
                .HasName("NonClusteredIndex_OmniChannel_PlatformId");

            builder.ToTable("OmniChannel")
                .HasIndex(o =>  new {o.IsActive, o.IsDeleted })
                .Include<Domain.Model.OmniChannel>(o => new
                {
                    o.Id,
                    o.Name,
                    o.Type,
                    o.RetailerId
                })
                .HasName("NonClusteredIndex_IsActive_IsDeleted");
        }
    }
}
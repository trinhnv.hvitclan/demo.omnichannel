﻿using Demo.OmniChannel.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Demo.OmniChannel.Infrastructure.Persistence.Ef.EntityConfigurations
{
    public class OmniChannelScheduleEntityConfig : IEntityTypeConfiguration<OmniChannelSchedule>
    {
        public void Configure(EntityTypeBuilder<OmniChannelSchedule> builder)
        {
            builder.ToTable("OmniChannelSchedule").HasKey(x => x.Id);

            builder.ToTable("OmniChannelSchedule")
                .HasIndex(p => new { p.OmniChannelId, p.Type })
                .HasName("NonClusteredIndex_OmniChannelSchedule-OmniChannelId-Type")
                .IsUnique();
        }
    }
}
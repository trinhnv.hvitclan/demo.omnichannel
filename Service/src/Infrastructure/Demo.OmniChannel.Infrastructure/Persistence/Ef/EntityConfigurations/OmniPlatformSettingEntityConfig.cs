﻿using Demo.OmniChannel.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Demo.OmniChannel.Infrastructure.Persistence.Ef.EntityConfigurations
{
    internal class OmniPlatformSettingEntityConfig : IEntityTypeConfiguration<OmniPlatformSetting>
    {
        public void Configure(EntityTypeBuilder<OmniPlatformSetting> builder)
        {
            builder.ToTable("OmniPlatformSetting").HasKey(x => x.Id);

            builder.ToTable("OmniPlatformSetting")
                .HasIndex(p => new { p.RetailerId })
                .HasName("NonClusteredIndex_RetailerId");
        }
    }
}

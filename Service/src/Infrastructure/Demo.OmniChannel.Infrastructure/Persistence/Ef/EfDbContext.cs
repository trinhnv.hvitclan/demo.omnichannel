﻿using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Persistence.Ef.EntityConfigurations;
using Demo.OmniChannel.Infrastructure.Persistence.Ef.ExtendedSqlServerMigrations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Demo.OmniChannel.Infrastructure.Persistence.Ef
{
    public class EfDbContext : DbContext
    {
        public EfDbContext(DbContextOptions<EfDbContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.ReplaceService<IMigrationsAnnotationProvider, ExtendedSqlServerMigrationsAnnotationProvider>();
            optionsBuilder.ReplaceService<IMigrationsSqlGenerator, ExtendedSqlServerMigrationsSqlGenerator>();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region Configuration
            modelBuilder.ApplyConfiguration(new OmniChannelAuthEntityConfig());
            modelBuilder.ApplyConfiguration(new OmniChannelEntityConfig());
            modelBuilder.ApplyConfiguration(new OmniChannelScheduleEntityConfig());
            modelBuilder.ApplyConfiguration(new OmniChannelSettingEntityConfig());
            modelBuilder.ApplyConfiguration(new OmniChannelPlatformEntityConfig());
            modelBuilder.ApplyConfiguration(new OmniChannelWareHouseEntityConfig());
            modelBuilder.ApplyConfiguration(new OmniPlatformSettingEntityConfig());
            #endregion

            #region Build RelationShip
            modelBuilder.Entity<Domain.Model.OmniChannel>()
                .HasOne(c => c.OmniChannelAuth).WithOne(c => c.OmniChannel);
            modelBuilder.Entity<OmniChannelAuth>().HasOne(ka => ka.OmniChannel).WithOne(ka => ka.OmniChannelAuth);
            modelBuilder.Entity<OmniChannelSchedule>().HasOne(ks => ks.OmniChannel).WithMany(ks => ks.OmniChannelSchedules).HasForeignKey(x => x.OmniChannelId);
            modelBuilder.Entity<OmniChannelWareHouse>().HasOne(ks => ks.OmniChannel).WithMany(ks => ks.OmniChannelWareHouses).HasForeignKey(x => x.OmniChannelId);
            #endregion
        }
    }
}
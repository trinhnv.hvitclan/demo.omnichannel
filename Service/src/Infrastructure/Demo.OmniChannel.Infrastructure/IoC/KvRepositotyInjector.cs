﻿using Funq;
using Microsoft.Extensions.DependencyInjection;
using ServiceStack;
using System.Linq;
using System.Reflection;
using Demo.OmniChannel.Repository.Interfaces;

namespace Demo.OmniChannel.Infrastructure.IoC
{
    public static class KvRepositotyInjector
    {
        public static void RegisterOmmiChannelRepositories(this IServiceCollection services)
        {
            var type = typeof(IBaseRepository<>);
            var assembly = Assembly.GetAssembly(type);
            var types = assembly.GetTypes()
                .Where(m => m.IsClass && m.IsPublic && m.IsOrHasGenericInterfaceTypeOf(type) && !m.ContainsGenericParameters).ToList();
            
            foreach (var theType in types)
            {
                services.AddScoped(theType.GetInterface($"I{theType.Name}"), theType);
            }
        }

        public static void RegisterOmmiChannelRepositories(this Container container)
        {
            var type = typeof(IBaseRepository<>);
            //Get the Assembly Where the injectable classes are located.
            var assembly = Assembly.GetAssembly(type);
            //Get the injectable classes 
            var types = assembly.GetTypes()
                .Where(m => m.IsClass && m.IsPublic && m.IsOrHasGenericInterfaceTypeOf(type)).ToList();

            //loop through the injectable classes
            foreach (var theType in types)
            {
                //create the interface based on the naming convention
                var theInterface = theType.GetInterface($"I{theType.Name}");
                container.RegisterAutoWiredType(theType, theInterface, ReuseScope.Request);
            }
        }
    }
}

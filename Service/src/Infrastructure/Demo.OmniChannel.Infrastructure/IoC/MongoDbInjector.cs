﻿using Demo.OmniChannel.MongoService.Common;
using Demo.OmniChannel.MongoService.Impls;
using Demo.OmniChannel.MongoService.Interface;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;

namespace Demo.OmniChannel.Infrastructure.IoC
{
    public static class MongoDbExtension
    {
        public static void UsingMongoDb(this IServiceCollection services, IConfiguration configuration)
        {
            var mongo = new MongoDbSettings();
            configuration.GetSection("mongodb").Bind(mongo);
            var mongoClient = new MongoClient(mongo.ConnectionString);
            services.AddSingleton<IMongoClient>(z => mongoClient);
            services.AddSingleton(z => mongoClient.GetDatabase(mongo.DatabaseName));
            services.AddTransient<IProductMongoService, ProductMongoService>();
            services.AddTransient<IProductMappingService, ProductMappingService>();
            services.AddTransient<IRetryRefreshTokenMongoService, RetryRefreshTokenMongoService>();
        }
    }
}
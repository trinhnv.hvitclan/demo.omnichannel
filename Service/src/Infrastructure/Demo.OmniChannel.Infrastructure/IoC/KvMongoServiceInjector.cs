﻿using System.Linq;
using System.Reflection;
using Funq;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.MongoService.Interface;
using ServiceStack;

namespace Demo.OmniChannel.Infrastructure.IoC
{
    public static class KvMongoServiceInjector
    {
        public static void RegisterService(this Container container)
        {
            var type = typeof(IProductMongoService);
            //Get the Assembly Where the injectable classes are located.
            var assembly = Assembly.GetAssembly(type);
            //Get the injectable classes 
            var types = assembly.GetTypes()
                .Where(m => m.Namespace != null && m.Namespace.EndsWith("MongoService.Impls") && m.IsClass && m.IsPublic).ToList();

            //loop through the injectable classes
            foreach (var theType in types)
            {
                //create the interface based on the naming convention
                var theInterface = theType.GetInterface($"I{theType.Name}");
                container.RegisterAutoWiredType(theType, theInterface, ReuseScope.Request);
            }
        }
    }
}
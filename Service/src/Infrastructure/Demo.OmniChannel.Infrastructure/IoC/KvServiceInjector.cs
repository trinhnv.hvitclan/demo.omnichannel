﻿using Funq;
using Microsoft.Extensions.DependencyInjection;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Demo.OmniChannel.Services.Interfaces;

namespace Demo.OmniChannel.Infrastructure.IoC
{
    public static class KvServiceInjector
    {
        //For .NET Core
        public static void RegisterOmmiChannelServices(this IServiceCollection services)
        {
            var assembly = Assembly.GetAssembly(typeof(IOmniChannelService));
            var types = assembly.GetTypes()
                .Where(m => m.Namespace.EndsWith("Services.Impls") && m.IsClass && m.IsPublic).ToList();
            foreach (var theType in types)
            {
                services.AddScoped(theType.GetInterface($"I{theType.Name}"), theType);
            }
        }

        //For ServiceStack
        public static void RegisterOmmiChannelServices(this Container container)
        {
            var assembly = Assembly.GetAssembly(typeof(IOmniChannelService));
            var types = assembly.GetTypes()
                .Where(m => m.Namespace.EndsWith("Services.Impls") && m.IsClass && m.IsPublic).ToList();

            foreach (var theType in types)
            {
                var theInterface = theType.GetInterface($"I{theType.Name}");
                container.RegisterAutoWiredType(theType, theInterface, ReuseScope.Request);
            }
        }
    }
}

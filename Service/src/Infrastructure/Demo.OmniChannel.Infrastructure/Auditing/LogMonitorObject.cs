﻿using System;
using ServiceStack.Logging;

namespace Demo.OmniChannel.Infrastructure.Auditing
{
    public class LogMonitorObject : LogObject
    {
        public LogMonitorObject(ILog logger, Guid id) : base(logger, id)
        {
        }

        public int TotalMessagesFailed { get; set; }
        public int TotalNormalMessagesReceived { get; set; }
        public int TotalMessagesProcessed { get; set; }
        public string Status { get; set; }
    }
}
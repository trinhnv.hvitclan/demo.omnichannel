﻿using ServiceStack;
using ServiceStack.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Demo.OmniChannel.Infrastructure.Auditing
{
    public class LogObject
    {
        public LogObject(ILog logger, Guid id)
        {
            Id = id;
            Timer = Stopwatch.StartNew();
            StartTime = DateTime.Now.ToString("o");
            Logger = logger;
            LogStages = new List<LogStage>();
        }

        public List<Guid> ParentIds { get; set; }
        public Guid Id { get; set; }
        public string Action { get; set; }

        private Stopwatch Timer { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public long? TotalTime { get; set; }
        public long Duration { get; set; }
        public int? RetailerId { get; set; }
        public int? BranchId { get; set; }
        public string RetailerCode { get; set; }
        public string Description { get; set; }
        public long? OmniChannelId { get; set; }
        public object RequestObject { get; set; }
        public object ResponseObject { get; set; }
        public long? TotalItem { get; set; }
        private ILog Logger { get; set; }
        public List<LogStage> LogStages { get; set; }
        public string CustomerCentricType { get; set; }

        public void LogInfo(bool isLogTotalTime = false)
        {
            Timer.Stop();
            EndTime = DateTime.Now.ToString("o");
            if (isLogTotalTime == true)
            {
                TotalTime = Timer.ElapsedMilliseconds;
            }
            else
            {
                Duration = Timer.ElapsedMilliseconds;
            }

            if (RequestObject != null && !(RequestObject is string))
            {
                RequestObject = RequestObject.ToSafeJson();
            }

            if (ResponseObject != null && !(ResponseObject is string))
            {
                ResponseObject = ResponseObject.ToSafeJson();
            }

            Logger.Info(this.ToSafeJson());
        }

        public void LogError(Exception ex = null, bool isLogTotalTime = false, bool isValidate = false)
        {
            Timer.Stop();
            EndTime = DateTime.Now.ToString("o");
            if (isLogTotalTime == true)
            {
                TotalTime = Timer.ElapsedMilliseconds;
            }
            else
            {
                Duration = Timer.ElapsedMilliseconds;
            }

            if (RequestObject != null && !(RequestObject is string))
            {
                RequestObject = RequestObject.ToSafeJson();
            }

            if (ResponseObject != null && !(ResponseObject is string))
            {
                ResponseObject = ResponseObject.ToSafeJson();
            }

            if (ex != null)
            {
                Description = string.IsNullOrEmpty(Description) ? ex.Message : Description;

                if (isValidate)
                {
                    Logger.Info(this.ToSafeJson(), ex);
                }
                else
                {
                    Logger.Error(this.ToSafeJson(), ex);
                }
            }
            else
            {
                if (isValidate)
                {
                    Logger.Info(this.ToSafeJson());
                }
                else
                {
                    Logger.Error(this.ToSafeJson());
                }
            }
        }

        public void LogWarning(string message)
        {
            Timer.Stop();
            EndTime = DateTime.Now.ToString("o");
            Duration = Timer.ElapsedMilliseconds;

            TotalTime = Timer.ElapsedMilliseconds;

            if (RequestObject != null && !(RequestObject is string))
            {
                RequestObject = RequestObject.ToSafeJson();
            }

            if (ResponseObject != null && !(ResponseObject is string))
            {
                ResponseObject = ResponseObject.ToSafeJson();
            }
            Description = message;

            Logger.Warn(this.ToSafeJson());
        }

        public void AddToParentIds(List<Guid> itemIds)
        {
            if (itemIds?.Any() != true) return;

            if (ParentIds == null) ParentIds = new List<Guid>();

            ParentIds.AddRange(itemIds);
        }

        public LogObject Clone(string action)
        {
            var logger = new LogObject(Logger, Id)
            {
                Action = action
            };
            return logger;
        }

    }

    public class LogStage
    {
        public string Name { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public long Duration { get; set; }
        private Stopwatch Timer { get; set; }
        public LogStage(string name)
        {
            Name = name;
            Timer = Stopwatch.StartNew();
            StartTime = DateTime.Now.ToString("o");
        }

        public void EndStage()
        {
            EndTime = DateTime.Now.ToString("o");
            Timer.Stop();
            Duration = Timer.ElapsedMilliseconds;
        }
    }

    public class CustomerCentricType
    {
        public const string Auto = "Auto";
        public const string AutoRetry = "AutoRetry";
        public const string ManualRetry = "ManualRetry";
    }
}
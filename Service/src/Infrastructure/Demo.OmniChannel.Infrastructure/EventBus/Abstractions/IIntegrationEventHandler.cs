﻿using System.Threading.Tasks;
using Demo.OmniChannel.ShareKernel.EventBus;

namespace Demo.OmniChannel.Infrastructure.EventBus.Abstractions
{
    public interface IIntegrationEventHandler<in TIntegrationEvent> where TIntegrationEvent : IntegrationEvent
    {
        Task Handle(TIntegrationEvent @event);
    }
}

﻿
using Demo.OmniChannel.ShareKernel.Auth;

namespace Demo.OmniChannel.Infrastructure.EventBus.Abstractions
{
    class EventContext
    {
        public int TenantId { get; set; }
        public int BranchId { get; set; }
        public int GroupId { get; set; }
        public long UserId { get; set; }
        public string BearToken { get; set; }
        public string RetailerCode { get; set; }
        public SessionUser User { get; set; }
    }
}

﻿using Demo.OmniChannel.ShareKernel.EventBus;

namespace Demo.OmniChannel.Infrastructure.EventBus.Abstractions
{
    public interface IEventBus
    {
        void Publish(IntegrationEvent @event,
            string routingKey = null,
            string exchange = null);

        void Subscribe<T, TH>()
            where T : IntegrationEvent
            where TH : IIntegrationEventHandler<T>;

        void Unsubscribe<T, TH>()
            where T : IntegrationEvent
            where TH : IIntegrationEventHandler<T>;

    }
}

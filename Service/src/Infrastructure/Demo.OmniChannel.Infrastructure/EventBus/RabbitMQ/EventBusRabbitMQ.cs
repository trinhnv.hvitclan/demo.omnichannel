﻿using Demo.OmniChannel.Infrastructure.EventBus.Abstractions;
using Demo.OmniChannel.ShareKernel.EventBus;
using Demo.OmniChannel.Utilities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Polly;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Infrastructure.EventBus.RabbitMQ
{
    public sealed class EventBusRabbitMq : IEventBus, IDisposable
    {
        #region Properties
        private const string BrokerName = "omni-channel-event";
        private readonly IRabbitMqPersistentConnection _persistentConnection;
        private readonly ILogger<EventBusRabbitMq> _logger;
        private readonly IEventBusSubscriptionsManager _subsManager;
        private readonly IServiceProvider _serviceProvider;
        private readonly int _retryCount;
        private readonly string _exchange;
        private readonly string _exchangeType;

        private IModel _consumerChannel = null;
        private string _queueName;
        #endregion

        #region Constructor_persistentConnection
        public EventBusRabbitMq(
            IRabbitMqPersistentConnection persistentConnection,
            ILogger<EventBusRabbitMq> logger,
            IEventBusSubscriptionsManager subsManager,
            IServiceProvider serviceProvider,
            string queueName = null,
            int retryCount = 5,
            string exchange = null,
            string exchangeType = null
        )
        {
            _persistentConnection = persistentConnection ?? throw new ArgumentNullException(nameof(persistentConnection));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _subsManager = subsManager ?? new InMemoryEventBusSubscriptionsManager();
            _queueName = queueName;
            _retryCount = retryCount;
            _subsManager.OnEventRemoved += SubsManager_OnEventRemoved;
            _exchange = exchange;
            _exchangeType = exchangeType;
            _serviceProvider = serviceProvider;
            _consumerChannel = CreateConsumerChannel();
        }
        #endregion

        #region Public methods

        public void Publish(IntegrationEvent @event,
            string routingKey = null, string exchange = null)
        {
            if (!_persistentConnection.IsConnected)
            {
                _persistentConnection.TryConnect();
            }

            var policy = Policy.Handle<BrokerUnreachableException>()
                .Or<SocketException>()
                .WaitAndRetry(_retryCount, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)), (ex, time) =>
                {
                    _logger.LogWarning($@"Could not publish event: {@event.Id} after {time.TotalSeconds:N1}s ({ex.Message})");
                });

            policy.Execute(() =>
            {
                using (var channel = _persistentConnection.CreateModel())
                {
                    var eventName = string.IsNullOrEmpty(routingKey) ? @event.GetType()
                        .Name : routingKey;

                    var message = JsonConvert.SerializeObject(@event);
                    var body = Encoding.UTF8.GetBytes(message);

                    var properties = channel.CreateBasicProperties();
                    if (!string.IsNullOrEmpty(exchange))
                    {
                        channel.BasicPublish(exchange, eventName, true, properties, body);
                    }
                    else if (!string.IsNullOrEmpty(_exchange) && !string.IsNullOrEmpty(_exchangeType))
                    {
                        channel.BasicPublish(_exchange, eventName, true, properties, body);
                    }
                    else
                    {
                        channel.BasicPublish(BrokerName, eventName, true, properties, body);
                    }
                }
            });
        }

        public void Subscribe<T, TH>()
            where T : IntegrationEvent
            where TH : IIntegrationEventHandler<T>
        {
            var eventName = _subsManager.GetEventKey<T>();
            DoInternalSubscription(eventName);
            _subsManager.AddSubscription<T, TH>();
        }

        public void Unsubscribe<T, TH>()
            where T : IntegrationEvent
            where TH : IIntegrationEventHandler<T>
        {
            var eventName = _subsManager.GetEventKey<T>();
            _logger.LogInformation($@"Unsubscribing from event {eventName}");
            _subsManager.RemoveSubscription<T, TH>();
        }

        public void Dispose()
        {
            _consumerChannel?.Dispose();
            _subsManager.Clear();
        }

        #endregion

        #region Private methods
        private void SubsManager_OnEventRemoved(object sender, string eventName)
        {
            if (!_persistentConnection.IsConnected)
            {
                _persistentConnection.TryConnect();
            }

            using (var channel = _persistentConnection.CreateModel())
            {
                channel.QueueUnbind(queue: _queueName,
                    exchange: BrokerName,
                    routingKey: eventName);

                if (!_subsManager.IsEmpty) return;
                _queueName = string.Empty;
                _consumerChannel.Close();
            }
        }

        private void DoInternalSubscription(string eventName)
        {
            var containsKey = _subsManager.HasSubscriptionsForEvent(eventName);
            if (containsKey) return;

            if (!_persistentConnection.IsConnected)
            {
                _persistentConnection.TryConnect();
            }

            using (var channel = _persistentConnection.CreateModel())
            {
                channel.QueueBind(queue: _queueName,
                    exchange: !string.IsNullOrEmpty(_exchange) ? _exchange : BrokerName,
                    routingKey: eventName
                    );
            }
        }

        private IModel CreateConsumerChannel()
        {
            if (string.IsNullOrEmpty(_queueName)) return null;


            if (!_persistentConnection.IsConnected)
            {
                _persistentConnection.TryConnect();
            }

            var channel = _persistentConnection.CreateModel();

            channel.ExchangeDeclare(exchange: !string.IsNullOrEmpty(_exchange) ? _exchange : BrokerName,
                type: "topic", true);

            channel.QueueDeclare(queue: _queueName,
                durable: true,
                exclusive: false,
                autoDelete: false,
                new Dictionary<string, object>() { { "x-queue-type", "quorum" } });


            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += async (model, ea) =>
            {
                await ReceivedEvent(channel, ea);
            };

            channel.BasicConsume(queue: _queueName,
                autoAck: false,
                consumer: consumer);

            channel.CallbackException += (sender, ea) =>
            {
                _consumerChannel.Dispose();
                _consumerChannel = CreateConsumerChannel();
            };

            return channel;
        }

        private async Task ReceivedEvent(IModel channel, BasicDeliverEventArgs ea)
        {
            var eventName = ea.RoutingKey;
            var message = Encoding.UTF8.GetString(ea.Body);
            var retryCount = 0;
            var retryAllow = 3;
            var logId = Guid.NewGuid().ToString("N").ToLower();

            while (retryCount <= retryAllow)//NOSONAR
            {
                try
                {
                    await ProcessEvent(_serviceProvider, eventName, message);

                    channel.BasicAck(ea.DeliveryTag, multiple: false);
                    break;
                }
                catch (Exception ex)
                {
                    // Retry too times --> Ignore event
                    if (retryCount == retryAllow)
                    {
                        _logger.LogError(ex, $"[LogId: {logId}] Error when consum event --> Ignore event. MessageData: {message}");
                        channel.BasicAck(ea.DeliveryTag, multiple: false);
                        break;
                    }

                    retryCount++;
                    _logger.LogError(ex, $"[LogId: {logId}] Retry consum event ({retryCount}/{retryAllow}). MessageData: {message}");
                    await Task.Delay(2000);
                }
            }
        }

        private async Task ProcessEvent(IServiceProvider provider, string eventName, string message)
        {
            if (_subsManager.HasSubscriptionsForEvent(eventName))
            {
                var subscriptions = _subsManager.GetHandlersForEvent(eventName);
                foreach (var subscription in subscriptions)
                {
                    _logger.LogDebug("event: " + eventName);
                    var handler = provider.GetRequiredService(subscription.HandlerType);
                    if (handler == null) continue;
                    var eventType = _subsManager.GetEventTypeByName(eventName);
                    var integrationEvent = JsonConvert.DeserializeObject(message, eventType, new JsonSerializerSettings()
                    {
                        ContractResolver = new PrivateResolver(),
                        ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
                        DateTimeZoneHandling = DateTimeZoneHandling.Local
                    });
                    var concreteType = typeof(IIntegrationEventHandler<>).MakeGenericType(eventType);
                    var method = concreteType.GetMethod("Handle")?.Invoke(handler, new[] { integrationEvent });
                    if (method == null) continue;
                    await (Task)method;
                }
            }
        }

        #endregion
    }
}

﻿using Demo.OmniChannel.Infrastructure.EventBus.Abstractions;
using Demo.OmniChannel.Infrastructure.EventBus.RabbitMQ;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;

namespace Demo.OmniChannel.Infrastructure.EventBus.Extentions
{
    public static class EventBusNativeInjectorExtension
    {
        public static void AddEventBus(this IServiceCollection services)
        {
            services.AddSingleton<IRabbitMqPersistentConnection>(sp =>
            {
                var rabbitMqConfiguration = sp.GetService<IOptions<RabbitMqEventBusConfiguration>>().Value;
                var logger = sp.GetRequiredService<ILogger<RabbitMqPersistentConnection>>();
                var factory = new ConnectionFactory
                {
                    HostName = rabbitMqConfiguration.Connection,
                    Port = rabbitMqConfiguration.Port,
                    //RequestedHeartbeat = rabbitMqConfiguration.RequestedHeartbeat ?? 10
                };

                if (!string.IsNullOrEmpty(rabbitMqConfiguration.VirtualHost))
                {
                    factory.VirtualHost = rabbitMqConfiguration.VirtualHost;
                }

                if (!string.IsNullOrEmpty(rabbitMqConfiguration.UserName))
                {
                    factory.UserName = rabbitMqConfiguration.UserName;
                }

                if (!string.IsNullOrEmpty(rabbitMqConfiguration.Password))
                {
                    factory.Password = rabbitMqConfiguration.Password;
                }

                var retryCount = 5;
                if (rabbitMqConfiguration.RetryCount.HasValue)
                {
                    retryCount = rabbitMqConfiguration.RetryCount.Value;
                }
                return new RabbitMqPersistentConnection(factory, logger, retryCount);

            });
            services.AddTransient<IIntegrationEventService, IntegrationEventService>();
            services.AddSingleton<IEventBusSubscriptionsManager, InMemoryEventBusSubscriptionsManager>();
            services.AddSingleton<IEventBus, EventBusRabbitMq>(sp =>
            {
                var rabbitMqPersistentConnection = sp.GetRequiredService<IRabbitMqPersistentConnection>();
                var logger = sp.GetRequiredService<ILogger<EventBusRabbitMq>>();
                var eventBusSubcriptionsManager = sp.GetRequiredService<IEventBusSubscriptionsManager>();
                var rabbitMqConfiguration = sp.GetService<IOptions<RabbitMqEventBusConfiguration>>().Value;
                var retryCount = 5;
                if (rabbitMqConfiguration.RetryCount.HasValue)
                {
                    retryCount = rabbitMqConfiguration.RetryCount.Value;
                }

                if (!string.IsNullOrEmpty(rabbitMqConfiguration.Exchange)
                    && !string.IsNullOrEmpty(rabbitMqConfiguration.ExchangeType))
                {
                    return new EventBusRabbitMq(
                        rabbitMqPersistentConnection,
                        logger,
                        eventBusSubcriptionsManager,
                        sp,
                        rabbitMqConfiguration.QueueName,
                        retryCount,
                        rabbitMqConfiguration.Exchange,
                        rabbitMqConfiguration.ExchangeType);
                }

                return new EventBusRabbitMq(
                        rabbitMqPersistentConnection,
                        logger,
                        eventBusSubcriptionsManager,
                        sp,
                        rabbitMqConfiguration.QueueName,
                        retryCount);
            });
        }
    }
}

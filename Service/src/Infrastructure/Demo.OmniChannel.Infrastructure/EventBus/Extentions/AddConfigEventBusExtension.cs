﻿using Demo.OmniChannel.Infrastructure.EventBus.RabbitMQ;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Demo.OmniChannel.Infrastructure.EventBus.Extentions
{
    public static class AddConfigEventBusExtension
    {
        public static void AddConfigEventBus(this IServiceCollection services, IConfiguration config)
        {
            services.Configure<RabbitMqEventBusConfiguration>(config.GetSection("EventBus:RabbitMq"));
        }
    }
}

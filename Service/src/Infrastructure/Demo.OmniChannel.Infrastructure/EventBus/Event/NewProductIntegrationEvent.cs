﻿using Demo.OmniChannel.ShareKernel.EventBus;
using System.Collections.Generic;

namespace Demo.OmniChannel.Infrastructure.EventBus.Event
{
    public class NewProductIntegrationEvent : IntegrationEvent
    {
        public List<MongoDb.Product> NewProducts { get; set; }
        public long? UserId { get; set; }
        public string RetailerCode { get; set; }
        public long ChannelId { get; set; }
    }
}

﻿using Demo.OmniChannel.ShareKernel.EventBus;
using System;


namespace Demo.OmniChannel.Infrastructure.EventBus.Event
{
    public class ErrorProductNotificationMesage : IntegrationEvent
    {
        public ErrorProductNotificationMesage(Guid logId) : base(logId)
        {
        }
        public int BranchId { get; set; }
        public long ChannelId { get; set; }
        public int Type { get; set; }
        public int ErrorType { get; set; } //SyncTabError-enum
        public string ItemSku { get; set; }
        public string ItemName { get; set; }
        public string Image { get; set; }
        public string ErrorMessage { get; set; }
    }
}

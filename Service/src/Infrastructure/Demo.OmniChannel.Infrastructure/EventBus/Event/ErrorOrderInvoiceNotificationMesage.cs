﻿using Demo.OmniChannel.ShareKernel.EventBus;
using System;

namespace Demo.OmniChannel.Infrastructure.EventBus.Event
{
    public class ErrorOrderInvoiceNotificationMesage : IntegrationEvent
    {
        public ErrorOrderInvoiceNotificationMesage(Guid logId) : base(logId)
        {
        }
        public int BranchId { get; set; }
        public long ChannelId { get; set; }
        public string OrderKv { get; set; }
        public string OrderId { get; set; }
        public DateTime PurchaseDate { get; set; }
        public int ErrorType { get; set; } //SyncTabError-enum
        public string ErrorMessage { get; set; }
        public int Type { get; set; }
        public Guid LogId { get; set; }
    }
}

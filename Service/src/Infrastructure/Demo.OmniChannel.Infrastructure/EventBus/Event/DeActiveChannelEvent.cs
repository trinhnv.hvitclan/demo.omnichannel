﻿
using Demo.OmniChannel.ShareKernel.EventBus;

namespace Demo.OmniChannel.Infrastructure.EventBus.Event
{
    public class DeActiveChannelEvent : IntegrationEvent
    {
        public long RetailerId { get; set; }

        public string ShopId { get; set; }
    }
}

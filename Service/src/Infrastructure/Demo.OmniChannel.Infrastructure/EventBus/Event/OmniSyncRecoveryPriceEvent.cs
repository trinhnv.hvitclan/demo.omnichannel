﻿using Demo.OmniChannel.ShareKernel.EventBus;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Demo.OmniChannel.Infrastructure.EventBus.Event
{
    public class OmniSyncRecoveryPriceEvent : IntegrationEvent
    {
        [JsonProperty(PropertyName = "products")]
        public List<ProductPrice> Products { get; set; }

        [JsonProperty(PropertyName = "channel_id")]
        public long ChannelId { get; set; }

        [JsonProperty(PropertyName = "retailer_id")]
        public override int RetailerId { get; set; }
    }

    public class ProductPrice
    {
        [JsonProperty(PropertyName = "item_id")]
        public string ItemId { get; set; }

        [JsonProperty(PropertyName = "parent_id")]
        public string ParrentId { get; set; }

        [JsonProperty(PropertyName = "price")]
        public decimal? Price { get; set; }

        [JsonProperty(PropertyName = "special_price")]
        public decimal? SpecialPrice { get; set; }

        [JsonProperty(PropertyName = "special_from_time")]
        public DateTime? SpecialFromTime { get; set; }

        [JsonProperty(PropertyName = "special_to_time")]
        public DateTime? SpecialToTime { get; set; }
    }
}
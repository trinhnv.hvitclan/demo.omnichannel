﻿using Demo.OmniChannel.ShareKernel.EventBus;
using System.Collections.Generic;

namespace Demo.OmniChannel.Infrastructure.EventBus.Event
{
    public class OmniSyncPriceEvent : IntegrationEvent
    {
        public List<string> ChannelProductIds { get; set; }
        public List<long> KvProductIds { get; set; }
        public long ChannelId { get; set; }
        public override int RetailerId { get; set; }
    }
}

﻿using Demo.OmniChannel.ShareKernel.EventBus;
using System;
using System.Collections.Generic;

namespace Demo.OmniChannel.Infrastructure.EventBus.Event
{
    public class SyncPriceEvent : IntegrationEvent
    {
        public SyncPriceBody Body { get; set; }       
    }

    public class SyncPriceBody
    {
        public long ChannelId { get; set; }

        public byte ChannelType { get; set; }

        public string KvEntities { get; set; }

        public int BranchId { get; set; }

        public long RetailerId { get; set; }

        public string Source { get; set; }

        public bool IsIgnoreAuditTrail { get; set; } = true;

        public List<SyncItemPrice> MultiProducts { get; set; }
    }

    public class SyncItemPrice
    {
        public string ItemSku { get; set; }
        public string ItemId { get; set; }
        public string ParentItemId { get; set; }
        public byte ItemType { get; set; }
        public double OnHand { get; set; }
        public decimal? Price { get; set; }
        public decimal? SalePrice { get; set; }
        public DateTime? StartSaleDate { get; set; }
        public DateTime? EndSaleDate { get; set; }
        public string KvProductSku { get; set; }
        public byte[] Revision { get; set; }
        public long KvProductId { get; set; }
    }
}

﻿using Demo.OmniChannel.ShareKernel.EventBus;
using System;

namespace Demo.OmniChannel.Infrastructure.EventBus.Event
{
    public class TiktokSyncProductFromErrorOrder : IntegrationEvent
    {
        public long ChannelId { get; set; }

        public TiktokSyncProductFromErrorOrder(
            int retailerId,
            long channelId)
        {
            RetailerId = retailerId;
            ChannelId = channelId;
        }
    }

    public class TiktokSyncProductFromErrorDto
    {
        public DateTime LastSync { get; set; }
        public bool IsRunning { get; set; }
        
        public TiktokSyncProductFromErrorDto(
            DateTime dateTime,
            bool isRunning)
        {
            LastSync = dateTime;
            IsRunning = isRunning;
        }
    }
}

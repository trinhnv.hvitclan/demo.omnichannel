﻿using Demo.OmniChannel.ShareKernel.EventBus;

namespace Demo.OmniChannel.Infrastructure.EventBus.Service
{
    public interface IIntegrationEventService
    {
       void AddEventAsync(IntegrationEvent @event);
       void AddEventWithRoutingKeyAsync(IntegrationEvent @event,
           string routingKey,
           string exchange = null);
    }
}

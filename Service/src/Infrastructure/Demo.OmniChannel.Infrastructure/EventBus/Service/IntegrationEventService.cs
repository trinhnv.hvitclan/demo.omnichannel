﻿using Demo.OmniChannel.Infrastructure.EventBus.Abstractions;
using Demo.OmniChannel.ShareKernel.EventBus;
using System;

namespace Demo.OmniChannel.Infrastructure.EventBus.Service
{
    public class IntegrationEventService : IIntegrationEventService
    {
        private readonly IEventBus _eventBus;

        public IntegrationEventService(
            IEventBus eventBus
        )
        {
            _eventBus = eventBus;
        }


        public void AddEventAsync(IntegrationEvent @event)
        {
            PublishEventToEventBusAsync(@event);
        }

        public void AddEventWithRoutingKeyAsync(IntegrationEvent @event,
            string routingKey,
            string exchange = null)
        {
            PublishEventToEventBusAsync(@event, routingKey, exchange);
        }

        #region  private method
        private void PublishEventToEventBusAsync(IntegrationEvent eventLogs,
            string routingKey = null,
            string exchange = null)
        {
            try
            {
                _eventBus.Publish(eventLogs, routingKey, exchange);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        #endregion
    }
}

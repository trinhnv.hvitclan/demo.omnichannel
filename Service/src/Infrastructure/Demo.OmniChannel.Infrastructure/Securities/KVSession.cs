﻿using Demo.OmniChannel.ShareKernel.Auth;
using ServiceStack;

namespace Demo.OmniChannel.Infrastructure.Securities
{
    public class KVSession : AuthUserSession
    {
        public SessionUser CurrentUser { get; set; }
        public string KvSessionId { get; set; }
        public int CurrentRetailerId { get; set; }
        public int CurrentIndustryId { get; set; }
        public int GroupId { get; set; }
        public string CurrentRetailerCode { get; set; }
        public string CurrentLang { get; set; }
        public int CurrentBranchId { get; set; }
        public string HttpMethod { get; set; }
    }
}

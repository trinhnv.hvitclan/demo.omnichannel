﻿using Demo.OmniChannel.Infrastructure.CrmClient.Dtos;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Infrastructure.CrmClient
{
    public interface ICrmClient
    {
        Task<object> SendCrm(CrmRequest request);
    }
}

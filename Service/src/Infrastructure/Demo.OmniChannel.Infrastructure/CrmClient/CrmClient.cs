﻿using Demo.OmniChannel.Infrastructure.CrmClient.Dtos;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;


namespace Demo.OmniChannel.Infrastructure.CrmClient
{
    public class CrmClient : ICrmClient
    {
        private readonly ServiceStack.JsonHttpClient _jsonHttpClient;
        private const string OmniCrmServiceName = "Bán hàng online";
        private const string CrmDateFormat = "yyyy-MM-dd";
        private const string CrmActiveFeatureActionType = "dich-vu-gia-tang";
        private const string Note = "Web";
        private readonly CrmClientConfiguration _crmClientConfiguration;

        public CrmClient(ServiceStack.JsonHttpClient jsonHttpClient,
            IOptions<CrmClientConfiguration> crmClientConfiguration)
        {
            _jsonHttpClient = jsonHttpClient;
            _crmClientConfiguration = crmClientConfiguration.Value;
        }
        public async Task<object> SendCrm(CrmRequest request)
        {
            return await _jsonHttpClient.PostAsync<object>(_crmClientConfiguration.EndPoint, new
            {
                act = CrmActiveFeatureActionType,
                retailer_id = request.RetailerId.ToString(),
                ten_dich_vu = OmniCrmServiceName,
                ngay_dang_ky_dung_thu = request.StartDate?.ToString(CrmDateFormat) ?? string.Empty,
                ngay_het_han = request.EndDate?.ToString(CrmDateFormat) ?? string.Empty,
                ten_nguoi_lien_he = request.ContactName,
                so_dien_thoai_lien_he = request.Phone,
                ghi_chu = Note,
                app_token = _crmClientConfiguration.AppToken,
                nhan_tu_van = request.IsAdvise
            });
        }
    }
}

﻿
namespace Demo.OmniChannel.Infrastructure.CrmClient
{
    public class CrmClientConfiguration
    {
        public string EndPoint { get; set; }
        public string AppToken { get; set; }
    }
}

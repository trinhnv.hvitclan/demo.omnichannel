﻿using System;

namespace Demo.OmniChannel.Infrastructure.CrmClient.Dtos
{
    public class CrmRequest
    {
        public int RetailerId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string ContactName { get; set; }

        public string Phone { get; set; }
        public bool IsAdvise { get; set; }
    }
}

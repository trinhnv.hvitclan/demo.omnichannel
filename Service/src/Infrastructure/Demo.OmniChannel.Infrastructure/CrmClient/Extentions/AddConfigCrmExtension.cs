﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Demo.OmniChannel.Infrastructure.CrmClient.Extentions
{
    public static class AddConfigCrmExtension
    {
        public static void AddConfigCrmClient(this IServiceCollection services, IConfiguration config)
        {
            services.Configure<CrmClientConfiguration>(config.GetSection("CrmClient"));
        }
    }
}

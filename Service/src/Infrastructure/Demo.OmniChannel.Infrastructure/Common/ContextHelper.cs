﻿using System;
using System.Data;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ShareKernel.Auth;
using Demo.OmniChannel.ShareKernel.Common;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using KvGroup = Demo.OmniChannel.ShareKernel.Auth.KvGroup;

namespace Demo.OmniChannel.Infrastructure.Common
{
    public class ContextHelper
    {
        public static async Task<ExecutionContext> GetExecutionContext(ICacheClient cacheClient, IDbConnectionFactory dbConnectionFactory,
            int retailerId, string connectionString, int branchId, int? executionContext, Guid logId = default)
        {
            var context = cacheClient.Get<ExecutionContext>(string.Format(KvConstant.ExecutionContextCacheKey, retailerId));
            if (context != null)
            {
                if (branchId != 0)
                {
                    context.BranchId = branchId;
                }
                context.LogId = logId != default ? logId : Guid.NewGuid();
                return context;
            }
            context = new ExecutionContext();
            Domain.Model.KvGroup group = new Domain.Model.KvGroup();
            using (var dbMaster = dbConnectionFactory.Open("KvMasterEntities"))
            {
                var retailer = await dbMaster.SingleAsync<KvRetailer>(x => x.Id == retailerId);
                context.RetailerCode = retailer.Code;
                group.Id = retailer?.GroupId ?? 0;
            }
            using (var db = dbConnectionFactory.OpenDbConnection(connectionString.ToLower()))
            {
                using (var trans = db.OpenTransaction(IsolationLevel.ReadUncommitted))
                {
                    var userRepository = new UserRepository(db);
                    var user = await userRepository.SingleAsync(
                        u => u.RetailerId == retailerId && u.IsAdmin && u.IsActive);
                    if (branchId == 0)
                    {
                        var branchRepository = new BranchRepository(db);
                        var branch = await branchRepository.SingleAsync(x => x.RetailerId == retailerId && x.IsActive && x.LimitAccess == false);
                        if (branch != null)
                        {
                            branchId = branch.Id;
                        }
                    }
                    context.User = user.ConvertTo<SessionUser>();
                    context.AuthorizedBranchIds = new[] { branchId };
                    context.RetailerId = retailerId;
                    context.BranchId = branchId;
                    context.LogId = logId != default ? logId : Guid.NewGuid();
                    context.GroupId = group.Id;
                    context.Group = group.ConvertTo<KvGroup>();
                    cacheClient.Set(string.Format(KvConstant.ExecutionContextCacheKey, retailerId), context,
                        TimeSpan.FromDays(executionContext ?? 7));
                    return context;
                }
            }
        }
    }
}
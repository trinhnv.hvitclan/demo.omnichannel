﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Amazon;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using Demo.OmniChannel.Infrastructure.Common;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.ShareKernel.Common
{
    public class EmailHelper: IEmailHelper
    {
        private readonly AmazonSimpleEmailServiceClient _client;
        public string _from { get; set; }

        public EmailHelper(IAppSettings settings)
        {
            AmazonSimpleEmailServiceConfig amConfig = new AmazonSimpleEmailServiceConfig()
            {
                RegionEndpoint = RegionEndpoint.USWest2
            };
            _client = new AmazonSimpleEmailServiceClient(settings.GetString("HealthCheck:EmailAwsAccessKeyId"),
                settings.GetString("HealthCheck:EmailAwsSecretAccessKey"), amConfig);
            _from = settings.GetString("HealthCheck:EmailAwsFrom");
        }
        public async Task<bool> SendAsync(List<string> to, List<string> cc, string subject, string contenthtml, string color)
        {

            try
            {
                var text = HtmlRemoval.StripTagsRegex(contenthtml);
                //await SendSlack(subject, text, color);
                var request = new SendEmailRequest
                {
                    Source = _from,
                    Message = new Message
                    {
                        Subject = new Content(subject),
                        Body = new Body { Html = new Content(contenthtml), Text = new Content(text) }
                    }
                };
                var des = new Destination { ToAddresses = to };
                if (cc != null && cc.Any())
                {
                    des.CcAddresses = cc;
                }

                request.Destination = des;
                var ret = await _client.SendEmailAsync(request);
                return ret.HttpStatusCode == HttpStatusCode.OK;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public void Dispose()
        {
            _client?.Dispose();
        }
    }

    public interface IEmailHelper : IDisposable
    {
        Task<bool> SendAsync(List<string> to, List<string> cc, string subject, string content, string color);
    }
}
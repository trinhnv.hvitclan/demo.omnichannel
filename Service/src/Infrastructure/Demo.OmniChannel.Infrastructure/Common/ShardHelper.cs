﻿using System;
using System.Collections.Generic;
using Demo.OmniChannel.ShareKernel.Common;
using ServiceStack;

namespace Demo.OmniChannel.Infrastructure.Common
{
    public class ShardHelper
    {
        public static List<ShardConnect> GetShardMap()
        {
            try
            {
                var path = "~/shardconfig.json".MapServerPath();
                var content = path.ReadAllText();
                var ls = content.FromJson<List<ShardConnect>>();
                var retval = new List<ShardConnect>();
                foreach (var item in ls)
                {
                    retval.Add(new ShardConnect
                    {
                        Shard = item.Shard,
                        Prefix = item.Prefix
                    });
                }
                return retval;
            }
            catch (Exception e)
            {
                return new List<ShardConnect>();
            }

        }
    }
}
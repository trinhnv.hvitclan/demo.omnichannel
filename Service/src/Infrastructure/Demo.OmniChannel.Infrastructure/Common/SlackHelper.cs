﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using RestSharp;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Text;

namespace Demo.OmniChannel.Infrastructure.Common
{
    public class SlackHelper
    {
        private IAppSettings _settings;
        public SlackHelper(IAppSettings settings)
        {
            _settings = settings;
        }
        public async Task SendMessage(string subject, string content, string monitorLink, string color)
        {
            var client = new RestClient(_settings.GetString("HealthCheck:SlackHook"));
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Content-Type", "application/json");
            var body = new
            {
                //color,
                attachments = new List<object>
                {
                    new
                    {
                        mrkdwn_in = new []{ "text"},
                        title = subject,
                        text = content.ReplaceAll("<br/>", " \n "),
                        color,
                        actions = new []
                        {
                            new
                            {
                                name="kibanalink",
                                text = "Kibana Monitor Link",
                                type = "button",
                                url = $"{monitorLink}",
                                action_id = 1
                            }
                        },
                        footer = "by omni-channel team",
                        footer_icon= "https://platform.slack-edge.com/img/default_application_icon.png",
                        ts = DateTime.UtcNow.ToUnixTime()
                    },
                }
            };
            request.AddJsonBody(body.ToSafeJson());
            await client.ExecuteTaskAsync(request);
        }
    }
}
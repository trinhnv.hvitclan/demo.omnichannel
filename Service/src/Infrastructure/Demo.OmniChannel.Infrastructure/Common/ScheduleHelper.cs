﻿using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;

namespace Demo.OmniChannel.Infrastructure.Common
{
    public class ScheduleHelper
    {
        private readonly IScheduleService _scheduleService;
        public ScheduleHelper(IScheduleService scheduleService)
        {
            _scheduleService = scheduleService;
        }
        public void UnlockSchedule(ScheduleDto dto)
        {
            _scheduleService.AddOrUpdate(dto);

        }
        public void ReleaseSchedule(ScheduleDto dto)
        {
            _scheduleService.AddOrUpdate(dto, true);
        }
    }
}
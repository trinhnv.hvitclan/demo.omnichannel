﻿using Demo.OmniChannel.Infrastructure.CrmClient;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Demo.OmniChannel.Infrastructure.Extensions
{
    public static class CrmClientInjectorExtensions
    {
        public static void RegisterCrmClientService(this IServiceCollection services)
        {
            services.AddScoped<ICrmClient, CrmClient.CrmClient>(c =>
            {
                var client = GetHttpClient();
                var configs = c.GetRequiredService<IOptions<CrmClientConfiguration>>();
                return new CrmClient.CrmClient(client, configs);
            });
        }

        private static ServiceStack.JsonHttpClient GetHttpClient()
        {
            return new ServiceStack.JsonHttpClient();
        }
    }
}

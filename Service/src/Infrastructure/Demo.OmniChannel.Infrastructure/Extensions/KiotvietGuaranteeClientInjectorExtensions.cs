﻿using Demo.OmniChannel.Infrastructure.DemoGuaranteeClient;
using Demo.OmniChannel.Infrastructure.DemoGuaranteeClient.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using ServiceStack.Caching;

namespace Demo.OmniChannel.Infrastructure.Extensions
{
    public static class DemoGuaranteeClientInjectorExtensions
    {
        public static void RegisterDemoGuaranteeClientService(this IServiceCollection services)
        {
            services.AddTransient<IGuaranteeClient, GuaranteeClient>(c =>
            {
                var configs = c.GetRequiredService<IOptions<GuaranteeClientConfiguration>>();
                var cache = c.GetRequiredService<ICacheClient>();
                return new GuaranteeClient(configs, cache);
            });
        }
    }
}

﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Demo.OmniChannel.Infrastructure.DemoGuaranteeClient.Extentions
{
    public static class AddConfigGuaranteeClient
    {
        public static void AddConfigConfigGuaranteeClient(this IServiceCollection services, IConfiguration config)
        {
            services.Configure<GuaranteeClientConfiguration>(config.GetSection("GuaranteeClient"));
        }
    }
}

﻿

using Demo.OmniChannel.DCControl;
using Microsoft.Extensions.Options;

namespace Demo.OmniChannel.Infrastructure.DemoGuaranteeClient
{
    public abstract class BaseGuaranteeClient
    {
        protected GuaranteeClientConfiguration _guaranteeClientConfiguration;

        public BaseGuaranteeClient(
            IOptions<GuaranteeClientConfiguration> crmClientConfiguration)
        {
            _guaranteeClientConfiguration = crmClientConfiguration.Value;
        }

        protected string GetEndPointGuaranteeMultiDC(int? groupId)
        {
            if (groupId == null)
                return $"{_guaranteeClientConfiguration.EndPointDC1P}";

            #region DCControl

            string dcName = null;
            if (ApiDcControl.Instance.IsRunning)
            {
                dcName = ApiDcControl.Instance.GetDcStringName(groupId ?? 0);
            }
            if (ApplicationDcControl.Instance.IsRunning)
            {
                dcName = ApplicationDcControl.Instance.GetDcStringName(groupId ?? 0);
            }

            //Retailer không control bản free => Bắn về DC1
            if (dcName == null)
                return _guaranteeClientConfiguration.EndPointDC1P;

            switch (dcName)
            {
                case "dc1p":
                    return _guaranteeClientConfiguration.EndPointDC1P;
                case "dc2":
                    return _guaranteeClientConfiguration.EndPointDC2;
                default:
                    return _guaranteeClientConfiguration.EndPointDC1P;
            }
            #endregion DCControl
        }

        protected string GetEndPointKVAAMultiDC(int? groupId)
        {
            if (groupId == null)
                return $"{_guaranteeClientConfiguration.EndPointKvAuthDC1P}";

            #region DCControl

            string dcName = null;
            if (ApiDcControl.Instance.IsRunning)
            {
                dcName = ApiDcControl.Instance.GetDcStringName(groupId ?? 0);
            }
            if (ApplicationDcControl.Instance.IsRunning)
            {
                dcName = ApplicationDcControl.Instance.GetDcStringName(groupId ?? 0);
            }

            //Retailer không control bản free => Bắn về DC1
            if (dcName == null)
                return _guaranteeClientConfiguration.EndPointKvAuthDC1P;

            switch (dcName)
            {
                case "dc1p":
                    return _guaranteeClientConfiguration.EndPointKvAuthDC1P;
                case "dc2":
                    return _guaranteeClientConfiguration.EndPointKvAuthDC2;
                default:
                    return _guaranteeClientConfiguration.EndPointKvAuthDC1P;
            }
            #endregion DCControl
        }
    }
}

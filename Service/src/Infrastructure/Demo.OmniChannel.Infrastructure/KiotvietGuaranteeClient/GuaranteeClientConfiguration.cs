﻿
using System.Collections.Generic;

namespace Demo.OmniChannel.Infrastructure.DemoGuaranteeClient
{
    public class GuaranteeClientConfiguration
    {
        public string EndPointDC2 { get; set; }

        public string EndPointDC1P { get; set; }

        public string EndPointKvAuthDC1P { get; set; }

        public string EndPointKvAuthDC2 { get; set; }

        public string SecretKeyKvAuth { get; set; }

        public List<int> IncludeRetailerIds { get; set; } = new List<int>();
    }
}

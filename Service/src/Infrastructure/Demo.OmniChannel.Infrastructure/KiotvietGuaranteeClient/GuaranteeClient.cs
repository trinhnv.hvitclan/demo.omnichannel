﻿using Demo.OmniChannel.Infrastructure.DemoGuaranteeClient.Dtos;
using Demo.OmniChannel.Infrastructure.DemoGuaranteeClient.Interfaces;
using Demo.OmniChannel.ShareKernel.Auth;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.EndPoints;
using Demo.OmniChannel.Utilities;
using Microsoft.Extensions.Options;
using ServiceStack.Caching;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Infrastructure.DemoGuaranteeClient
{
    public class GuaranteeClient : BaseGuaranteeClient, IGuaranteeClient
    {
        private readonly ICacheClient _cacheClient;
        public GuaranteeClient(
            IOptions<GuaranteeClientConfiguration> crmClientConfiguration,
            ICacheClient cacheClient) : base(crmClientConfiguration)
        {
            _cacheClient = cacheClient;
        }
        public async Task<List<WarrantyDto>> GetWarrantyListByProductIds(
            ExecutionContext context, List<long> productIds)
        {
            var token = await GetKvAuToken(context);
            if (token == null) return null;
            return await GetWarrantyList(context, token, productIds);
        }

        #region Private method
        private async Task<KvAuthDto> GetKvAuToken(ExecutionContext context)
        {
            try
            {
                var _jsonHttpClient = new ServiceStack.JsonHttpClient();
                var cacheKey = string.Format(KvConstant.CacheKvAuToken, context.RetailerId);
                var getTokenCache = _cacheClient.Get<KvAuthDto>(cacheKey);
                if (getTokenCache != null) return getTokenCache;
                SetHeaderHttpClient(_jsonHttpClient, context);
                var unixTimestamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
                var url = $"{GetEndPointKVAAMultiDC(context.GroupId)}{KvAuthEndpoint.GetToken}";
                var token = await _jsonHttpClient.PostAsync<KvAuthDto>(url, new
                {
                    UnixTimestamp = unixTimestamp,
                    SecureHash = SystemHelper.GenerateSecureHash(context.RetailerId,
                         _guaranteeClientConfiguration.SecretKeyKvAuth, unixTimestamp)
                });
                if (token == null) return null;
                _cacheClient.Set(cacheKey, token, TimeSpan.FromSeconds(token.ExpiresIn - 86400));
                return token;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private async Task<List<WarrantyDto>> GetWarrantyList(
            ExecutionContext context,
            KvAuthDto kvAuth,
            List<long> productIds)
        {
            try
            {
                var _jsonHttpClient = new ServiceStack.JsonHttpClient();
                SetHeaderHttpClient(_jsonHttpClient, context, kvAuth);
                var url = $"{GetEndPointGuaranteeMultiDC(context.GroupId)}{KvAuthEndpoint.GetWarranty}";
                var warrantyList = await _jsonHttpClient.PostAsync<List<WarrantyDto>>(url, new
                {
                    RetailerId = context.RetailerId,
                    ProductIds = productIds
                });
                return warrantyList;
            }
            catch
            {
                return new List<WarrantyDto>();
            }
        }

        private void SetHeaderHttpClient(ServiceStack.JsonHttpClient jsonHttpClient,
            ExecutionContext context, KvAuthDto kvAuth = null)
        {
            jsonHttpClient.AddHeader("Retailer", context.RetailerCode);
            jsonHttpClient.BearerToken = kvAuth?.AccessToken;
            var httpClient = jsonHttpClient.GetHttpClient();
            httpClient.Timeout = TimeSpan.FromSeconds(30);
            jsonHttpClient.HttpClient = httpClient;
        }
        #endregion
    }
}

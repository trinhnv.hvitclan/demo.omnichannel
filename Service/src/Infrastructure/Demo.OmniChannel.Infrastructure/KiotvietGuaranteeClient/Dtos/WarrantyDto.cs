﻿
namespace Demo.OmniChannel.Infrastructure.DemoGuaranteeClient.Dtos
{
    public class WarrantyDto
    {
        public long Id { get; set; }
        public string Description { get; set; }

        public int NumberTime { get; set; }

        public int TimeType { get; set; }

        public int WarrantyType { get; set; }

        public long ProductId { get; set; }
    }
}

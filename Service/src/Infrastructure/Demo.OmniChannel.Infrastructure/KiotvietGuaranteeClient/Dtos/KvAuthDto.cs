﻿

namespace Demo.OmniChannel.Infrastructure.DemoGuaranteeClient.Dtos
{
    public class KvAuthDto
    {
        public string RetailerCode { get; set; }

        public string AccessToken { get; set; }

        public double ExpiresIn { get; set; } //Sencond
    }
}

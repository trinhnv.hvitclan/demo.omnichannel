﻿using Demo.OmniChannel.Infrastructure.DemoGuaranteeClient.Dtos;
using Demo.OmniChannel.ShareKernel.Auth;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Infrastructure.DemoGuaranteeClient.Interfaces
{
    public interface IGuaranteeClient
    {
        Task<List<WarrantyDto>> GetWarrantyListByProductIds(ExecutionContext context,
            List<long> productIds);
    }
}

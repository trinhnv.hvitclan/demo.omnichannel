﻿using Funq;
using Demo.OmniChannel.CoreDemoContext.Implements;
using Demo.OmniChannel.CoreDemoContext.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using ServiceStack;

namespace Demo.OmniChannel.CoreDemoContext.ConfigExtentions
{
    public static class DICoreDemoContextConfiguration
    {
        public static void AddDICoreDemoContextConfiguration(this Container container)
        {
            container.AddTransient<IDemoOmniChannelCoreContext, DemoOmniChannelCoreContext>();
        }

        public static void AddDICoreDemoContextConfiguration(this IServiceCollection services)
        {
            services.AddTransient<IDemoOmniChannelCoreContext, DemoOmniChannelCoreContext>();
        }
    }
}

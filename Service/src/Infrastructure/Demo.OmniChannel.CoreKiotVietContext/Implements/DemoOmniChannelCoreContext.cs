﻿using Demo.OmniChannel.CoreDemoContext.Interfaces;
using Demo.OmniChannel.Domain.Common;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.ShareKernel.Common;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;
using System.Threading.Tasks;

namespace Demo.OmniChannel.CoreDemoContext.Implements
{
    public class DemoOmniChannelCoreContext : IDemoOmniChannelCoreContext
    {
        private readonly ICacheClient _cacheClient;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly IAppSettings _appSettings;

        public DemoOmniChannelCoreContext()
        {
        }

        public DemoOmniChannelCoreContext(ICacheClient cacheClient, IDbConnectionFactory dbConnectionFactory, IAppSettings appSettings)
        {
            _cacheClient = cacheClient;
            _dbConnectionFactory = dbConnectionFactory;
            _appSettings = appSettings;
        }

        public async Task<Domain.Common.KvInternalContext> CreateContext(int retailerId)
        {
            var retailer = await GetRetailer(retailerId);
            if (retailer == null) return null;
            var kvGroup = await GetRetailerGroup(retailer.GroupId);
            if (kvGroup == null) return null;

            var kvInternalContext = new Domain.Common.KvInternalContext
            {
                RetailerCode = retailer.Code,
                RetailerId = retailer.Id,
                Group = new KvGroup
                {
                    Id = kvGroup?.Id ?? 0,
                    ConnectionString = kvGroup?.ConnectionString,
                    ConnectionStringName = kvGroup?.ConnectionString.Substring(kvGroup.ConnectionString.IndexOf('=') + 1)?.ToLower()
        }
            };
            return kvInternalContext;

        }


        public async Task<Demo.OmniChannelCore.Api.Sdk.Common.KvInternalContext> CreateOmniChannelCoreContext(int retailerId, int branchId, string connectionString)
        {
            var connectStringName = connectionString.Substring(connectionString.IndexOf('=') + 1);
            var context = await ContextHelper.GetExecutionContext(_cacheClient, _dbConnectionFactory, retailerId, connectStringName, branchId, _appSettings.Get<int>("ExecutionContext"));
            var coreContext = context.ConvertTo<Demo.OmniChannelCore.Api.Sdk.Common.KvInternalContext>();
            coreContext.UserId = context.User?.Id ?? 0;
            return coreContext;
        }

        public async Task<Domain.Model.KvRetailer> GetRetailer(int retailerId)
        {
            var kvRetailer = _cacheClient.Get<Domain.Model.KvRetailer>(string.Format(KvConstant.RetailerIdCacheKey, retailerId));
            if (kvRetailer == null)
            {
                using (var dbMaster = await _dbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities))
                {
                    kvRetailer = await dbMaster.SingleAsync<Domain.Model.KvRetailer>(x => x.Id == retailerId);
                    if (kvRetailer != null)
                    {
                        _cacheClient.Set(string.Format(KvConstant.RetailerIdCacheKey, retailerId), kvRetailer, DateTime.Now.AddDays(7));
                    }
                }
            }
            return kvRetailer;
        }

        private async Task<KvGroup> GetRetailerGroup(int groupId)
        {
            var kvGroup = _cacheClient.Get<KvGroup>(string.Format(KvConstant.KvGroupCacheKey, groupId));
            if (kvGroup == null)
            {
                using (var dbMaster = await _dbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities))
                {
                    kvGroup = await dbMaster.SingleAsync<Domain.Common.KvGroup>(x => x.Id == groupId);
                    if (kvGroup != null)
                    {
                        _cacheClient.Set(string.Format(KvConstant.KvGroupCacheKey, groupId), kvGroup, DateTime.Now.AddDays(7));
                    }
                }
            }
            return kvGroup;
        }
    }
}

﻿using System.Threading.Tasks;
using KvInternalContext = Demo.OmniChannelCore.Api.Sdk.Common.KvInternalContext;

namespace Demo.OmniChannel.CoreDemoContext.Interfaces
{
    public interface IDemoOmniChannelCoreContext
    {
        Task<KvInternalContext> CreateOmniChannelCoreContext(int retailerId, int branchId, string connectionString);
        Task<Demo.OmniChannel.Domain.Common.KvInternalContext> CreateContext(int retailerId);
        Task<Domain.Model.KvRetailer> GetRetailer(int retailerId);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.Utilities;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.DataAnnotations;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Repository.Common
{
    public class PosSetting
    {
        private Dictionary<string, string> _defaultActive;
        private Dictionary<string, bool> _industrySetting;
        private Dictionary<string, string> _buffer;
        private int _retailerId;
        private ICacheClient CacheClient { get; set; }
        private IDbConnectionFactory DbConnectionFactory { get; set; }
        private IAppSettings _appSettings { get; set; }

        public PosSetting(Dictionary<string, string> settingMap,
            int retailerId,
            IDbConnectionFactory dbConnectionFactory,
            ICacheClient cacheClient,
            IAppSettings appSettings)
        {
            _buffer = settingMap;
            _retailerId = retailerId;
            DbConnectionFactory = dbConnectionFactory;
            CacheClient = cacheClient;
            _appSettings = appSettings;
        }

        protected Dictionary<string, string> Data
        {
            get
            {
                _buffer = _buffer ?? new Dictionary<string, string>();
                return _buffer;
            }
        }

        public KvRetailer GetRetailer(long retailerId)
        {
            var kvRetailer = CacheClient.Get<KvRetailer>(string.Format(KvConstant.RetailerIdCacheKey, retailerId));
            if (kvRetailer == null)
            {
                using (var dbMaster = DbConnectionFactory.Open("KvMasterEntities"))
                {
                    kvRetailer = dbMaster.Single<KvRetailer>(x => x.Id == retailerId);
                    if (kvRetailer != null)
                    {
                        CacheClient.Set(string.Format(KvConstant.RetailerIdCacheKey, retailerId), kvRetailer, DateTime.Now.AddDays(7));
                    }
                }
            }
            return kvRetailer;
        }

        protected IList<KvPosSettings> GetKVPosSetting()
        {
            var kvRetailer = GetRetailer(_retailerId);
            var posSettings = CacheClient.Get<IList<KvPosSettings>>(string.Format(KvConstant.KvPosSettingKey, kvRetailer?.IndustryId));
            if (posSettings == null && kvRetailer != null)
            {
                using (var dbMaster = DbConnectionFactory.Open("KvMasterEntities"))
                {
                    posSettings = dbMaster.Select<KvPosSettings>(x => x.IndustryId == kvRetailer.IndustryId);
                    if (posSettings != null)
                    {
                        CacheClient.Set(string.Format(KvConstant.KvPosSettingKey, kvRetailer?.IndustryId), posSettings, DateTime.Now.AddDays(7));
                    }

                }
            }

            return posSettings;
        }

        public Dictionary<string, string> GetDefaultActiveSetting()
        {
            var result = new Dictionary<string, string>();
            var settings = GetKVPosSetting();
            //just get default if not existed in Db
            var savedSetting = Data;
            foreach (var st in settings)
            {
                if (savedSetting.ContainsKey(st.Key)) continue;
                if (st.IsDisplay)
                {
                    result.Add(st.Key, st.IsDefault.ToString());
                }
            }
            return result;
        }

        protected Dictionary<string, string> DefaultActive
        {
            get
            {
                if (_defaultActive == null)
                {
                    _defaultActive = GetDefaultActiveSetting();
                }
                return _defaultActive ?? new Dictionary<string, string>();
            }
        }

        protected Dictionary<string, bool> IndustrySetting
        {
            get
            {
                if (_industrySetting == null)
                {
                    var settings = GetKVPosSetting();
                    if (settings.Any())
                    {
                        _industrySetting = settings.Where(x => !x.IsFeature).ToDictionary(x => x.Key, y => y.IsDisplay || y.IsDefault);
                    }
                }
                return _industrySetting ?? new Dictionary<string, bool>();
            }
        }

        [Description("attributemeta_BookClosingKey")]
        [HelpMessage("attributemeta_BookClosing")]
        [NumberOrder(11)]
        [TypeSetting(SettingTypes.Settings)]
        [GroupSetting(GroupSettings.TransactionsManagement)]
        public bool ManagerCustomerByBranch
        {
            get => ConvertHelper.ToBoolean(GetPropValue(MethodBase.GetCurrentMethod().Name, false));
            set => SetPropValue(MethodBase.GetCurrentMethod().Name, value);
        }
        [Description("attributemeta_SellAllowOrderKey")]
        [NumberOrder(1)]
        [TypeSetting(SettingTypes.Settings)]
        [GroupSetting(GroupSettings.TransactionsManagement)]
        public bool SellAllowOrder
        {
            get => ConvertHelper.ToBoolean((object)this.GetPropValue(MethodBase.GetCurrentMethod().Name, (object)false));
            set => this.SetPropValue(MethodBase.GetCurrentMethod().Name, (object)value);
        }

        [Description("attributemeta_UseOrderSupplierKey")]
        [NumberOrder(12)]
        [TypeSetting(SettingTypes.Settings)]
        [GroupSetting(GroupSettings.TransactionsManagement)]
        public bool UseOrderSupplier
        {
            get { return ConvertHelper.ToBoolean(GetPropValue(MethodBase.GetCurrentMethod().Name, false)); }
            set { SetPropValue(MethodBase.GetCurrentMethod().Name, value); }
        }

        [Description("Quản lý sale channels")]
        [HelpMessage("Giải pháp quản lý bán hàng đa kênh TMĐT")]
        [NumberOrder(53)]
        [TypeSetting(SettingTypes.Feature)]
        public bool OmniChannel
        {
            get { return ConvertHelper.ToBoolean(GetPropValue(MethodBase.GetCurrentMethod().Name, false)); }
            set { SetPropValue(MethodBase.GetCurrentMethod().Name, value); }
        }

        [Description("attributemeta_UseLazadaIntergateKey")]
        [HelpMessage("attributemeta_UseLazadaIntergate")]
        [NumberOrder(28)]
        [TypeSetting(SettingTypes.Settings)]
        public bool UseLazadaIntergate
        {
            get { return ConvertHelper.ToBoolean(GetPropValue(MethodBase.GetCurrentMethod().Name, false)); }
            set { SetPropValue(MethodBase.GetCurrentMethod().Name, value); }
        }

        [Description("attributemeta_UseShopeeIntergateKey")]
        [HelpMessage("attributemeta_UseShopeeIntergate")]
        [NumberOrder(29)]
        [TypeSetting(SettingTypes.Settings)]
        public bool UseShopeeIntergate
        {
            get { return ConvertHelper.ToBoolean(GetPropValue(MethodBase.GetCurrentMethod().Name, false)); }
            set { SetPropValue(MethodBase.GetCurrentMethod().Name, value); }
        }

        [Description("attributemeta_UseTikiIntergateKey")]
        [HelpMessage("attributemeta_UseTikiIntergate")]
        [NumberOrder(39)]
        [TypeSetting(SettingTypes.Settings)]
        public bool UseTikiIntergate
        {
            get { return ConvertHelper.ToBoolean(GetPropValue(MethodBase.GetCurrentMethod().Name, false)); }
            set { SetPropValue(MethodBase.GetCurrentMethod().Name, value); }
        }

        [Description("attributemeta_UseSendoIntergateKey")]
        [HelpMessage("attributemeta_UseSendoIntergate")]
        [NumberOrder(39)]
        [TypeSetting(SettingTypes.Settings)]
        public bool UseSendoIntergate
        {
            get { return ConvertHelper.ToBoolean(GetPropValue(MethodBase.GetCurrentMethod().Name, false)); }
            set { SetPropValue(MethodBase.GetCurrentMethod().Name, value); }
        }

        [Description("attributemeta_RewardPointKey")]
        [HelpMessage("attributemeta_RewardPoint")]
        [NumberOrder(23)]
        [TypeSetting(SettingTypes.Settings)]
        [GroupSetting(GroupSettings.PartnerManagement)]
        public bool RewardPoint
        {
            get { return ConvertHelper.ToBoolean(GetPropValue(MethodBase.GetCurrentMethod().Name, false)); }
            set { SetPropValue(MethodBase.GetCurrentMethod().Name, value); }
        }

        public bool UseOmniChannelIntergate
        {
            get => this.UseLazadaIntergate || this.UseShopeeIntergate || this.UseTikiIntergate || this.UseSendoIntergate;
            set => this.SetPropValue(MethodBase.GetCurrentMethod().Name, (object)value);
        }

        public string GetPropValue(string propName, object def)
        {
            propName = propName.Replace("get_", "").Replace("set_", "");
            var val = def?.ToString() ?? string.Empty;
            if (Data != null && Data.ContainsKey(propName))
            {
                val = FilterPropValue(propName, Data[propName], val);
            }
            else if (DefaultActive.Any() && DefaultActive.ContainsKey(propName))
            {
                val = DefaultActive[propName];
            }
            return val;
        }

        public string FilterPropValue(string propName, string savedValue, object defaultValue)
        {
            try
            {
                var kvRetailer = GetRetailer(_retailerId);
                var configGroup = _appSettings.Get<List<int>>("UseFreePreimum:Group");

                if (configGroup.Any() && configGroup.Contains(kvRetailer.GroupId))
                    return savedValue;

                var type = typeof(PosSetting).GetProperty(propName, BindingFlags.Public | BindingFlags.Instance)?.PropertyType;
                if (type != null && type == typeof(bool) && IndustrySetting.Any() && IndustrySetting.ContainsKey(propName) && !IndustrySetting[propName]) // in settings
                {
                    // not display or feature in current industry will return default by code
                    return defaultValue?.ToString() ?? false.ToString();
                }
                return savedValue;
            }
            catch (Exception e)
            {
                return defaultValue?.ToString() ?? string.Empty;
            }
        }

        public void SetPropValue(string propName, object value)
        {
            propName = propName.Replace("get_", "").Replace("set_", "");
            if (Data.ContainsKey(propName))
            {
                Data[propName] = value.ToString();
            }
            else
            {
                Data.Add(propName, value.ToString());
            }

        }
    }

    public class KvPosSettings
    {
        public long Id { get; set; }
        public Nullable<int> IndustryId { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public bool IsDisplay { get; set; }
        public bool IsDefault { get; set; }
        public string Type { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<int> Order { get; set; }
        public bool IsFeature { get; set; }
    }

    public class HelpMessageAttribute : System.Attribute
    {
        public string Message { get; set; }
        public HelpMessageAttribute(string msg)
        {
            Message = msg;
        }
    }
    public class NumberOrderAttribute : System.Attribute
    {
        public int Order { get; set; }
        public NumberOrderAttribute(int order)
        {
            Order = order;
        }
    }

    public class TypeSettingAttribute : System.Attribute
    {
        public SettingTypes TypeSetting { get; set; }

        public TypeSettingAttribute(SettingTypes typeSetting)
        {
            TypeSetting = typeSetting;
        }
    }

    public class GroupSettingAttribute : System.Attribute
    {
        public GroupSettings GroupSetting { get; set; }

        public GroupSettingAttribute(GroupSettings groupSetting)
        {
            GroupSetting = groupSetting;
        }
    }




}
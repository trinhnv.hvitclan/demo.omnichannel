﻿using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Repository.Common
{
    public static class SqlExpressionHelper
    {
        public static SqlExpression<T> WithReadUnCommited<T>(this SqlExpression<T> source)
        {
            source.FromExpression += " WITH (READUNCOMMITTED) ";
             return source;
        }

        public static SqlExpression<T> WithNoLock<T>(this SqlExpression<T> source)
        {
            source.FromExpression += " WITH (NOLOCK) ";
            return source;
        }
    }
}

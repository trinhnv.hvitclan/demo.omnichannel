﻿using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IOrderRepository : IBaseRepository<Order>
    {
        Task<Order> GetOrderByCodeAsync(string code, int retailerId);
    }
}
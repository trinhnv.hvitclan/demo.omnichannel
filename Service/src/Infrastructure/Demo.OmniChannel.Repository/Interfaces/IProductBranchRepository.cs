﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IProductBranchRepository : IBaseRepository<ProductBranch>
    {
        Task<List<ProductBranch>> GetProduct(int retailerId,
            long branchId, List<long> productIds);
    }
}
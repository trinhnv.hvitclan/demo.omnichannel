﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IProductAttributeRepository : IBaseRepository<ProductAttribute>
    {
        Task<List<ProductAttribute>> GetByProductIds(List<long> ids, int retailerId);
    }
}

﻿using Demo.OmniChannel.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IBranchRepository : IBaseRepository<Branch>
    {
        Task<List<Branch>> GetListBranchByIds(int retailerId, List<int> branchIds);
    }
}
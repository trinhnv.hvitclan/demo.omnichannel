﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.ShareKernel.Dto;
using Demo.OmniChannel.Domain.Model;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IProductRepository : IBaseRepository<Product>
    {
        Task<List<ProductBranchDTO>> GetProductByIds(int retailerId, int branchId, List<long> productIds, bool isLogMonitor = false);
        Task<List<ProductBranchDTO>> GetProductByIdsIncludeDeleted(int retailerId, int branchId, List<long> productIds,bool isIncludeDeleted ,bool isLogMonitor = false);
        Task<List<Product>> GetProductByCode(int retailerId, int branchId, HashSet<string> productCodes, bool includeInActive = false);
        Task<List<Product>> GetProductChildUnitByIds(int retailerId, int branchId, List<long> productIds, bool isLogMonitor = false);
        Task<List<ProductBranch>> GetProductBranchByRange(int retailerId, int branchId, int minute);
        Task<List<ProductDto>> GetProductByRange(int retailerId, int minute);
        Task<List<ProductDto>> GetByIds(int retailerId, HashSet<long> productIds);
        Task<List<ProductBatchExpireDto>> GetProductBatchExpireByBranch(int retailerId, int branchId, List<long> productIds);
        Task<List<ProductBranch>> GetProductByRetailerId(int retailerId, int branchId, int range);
        Task<List<Product>> GetProductByMasterIds(int retailerId, List<long> kvProductIds);
    }
}
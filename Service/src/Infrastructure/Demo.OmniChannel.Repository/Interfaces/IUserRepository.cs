﻿using Demo.OmniChannel.Domain.Model;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IUserRepository : IBaseRepository<User>
    {
        
    }
}
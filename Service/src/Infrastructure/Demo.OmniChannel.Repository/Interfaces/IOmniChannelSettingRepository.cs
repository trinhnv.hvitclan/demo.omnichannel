﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Api.ServiceModel.Request;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IOmniChannelSettingRepository : IBaseRepository<OmniChannelSetting>
    {
        Task CreateOrUpdateSettingChannel(OmniChannelSettingRequest settingDto, Domain.Model.OmniChannel obj,
            List<Domain.Model.OmniChannel> allChannelOfCurrentRetailer);
        Task<List<OmniChannelSetting>> GetChannelSettings(long channelId, int retailerId);
        Task<List<OmniChannelSetting>> GetChannelSettings(List<long> channelIds, int retailerId);
        Task MarkChannelSentNotificationExpired(bool isMark, int retailerId, List<Domain.Model.OmniChannel> channels);
    }
}

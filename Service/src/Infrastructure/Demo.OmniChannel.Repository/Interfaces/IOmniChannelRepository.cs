﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.ShareKernel.Dto;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IOmniChannelRepository : IBaseDeleteRepository<Domain.Model.OmniChannel>
    {
        Task<long> CountChannelConnectedAsync(int retailerId, long shopId, long channelId, byte channelType, bool isIncludeDeleted = false);
        Task<List<Domain.Model.OmniChannel>> GetByRetailerAsync(int retailerId, byte? type, bool isIncludeDeleted = false, bool isIncludeInactive = true);
        Task<List<Domain.Model.OmniChannel>> GetByRetailerAsync(int retailerId, int branchId, byte? type, bool isIncludeDeleted = false, bool isIncludeInactive = true);
        Task<List<Domain.Model.OmniChannel>> GetByRetailerAsync(int retailerId, int branchId, List<int> types, bool isIncludeDeleted = false, bool isIncludeInactive = true);
        Task<List<OmniChannelDto>> GetChannelHaveSyncOnHandAsync();
        Task<List<OmniChannelDto>> GetChannelHaveSyncPriceAsync();
        Task<List<Domain.Model.OmniChannel>> GetChannelForRefreshToken(byte channelType, double refreshBeforeMinutes);
        Task<List<Domain.Model.OmniChannel>> GetChannelByIdentityKey(byte channelType, string identityKey);
        Task<List<Domain.Model.OmniChannel>> GetVisibleChannels(int retailerId);
        Task<Domain.Model.OmniChannel> GetDefaultActiveChannel(byte channelType);
        Task<List<Domain.Model.OmniChannel>> GetChannelByShopIds(int retailerId, List<string> shopIds, byte channelType, bool isIncludeDeleted = false);
        Task<List<Domain.Model.OmniChannel>> GetAllActiveChannels(List<int> includeRetailerIds, List<int> excludeRetailerIds);
        Task<List<Domain.Model.OmniChannel>> GetActiveChannelByRetailer(int retailerId);
        Task<List<Domain.Model.OmniChannel>> GetDuplicateChannels(List<int> includeRetailerIds);
        Task<List<Domain.Model.OmniChannel>> GetByIdsAsync(List<long> channelIds);
        Task<List<OmniChannelDto>> GetAllDeactiveChannels(List<int> includeRetailerIds, List<int> excludeRetailerIds, int skip = 0, int top = 1000);
        Task<List<Domain.Model.OmniChannel>> GetChannelWithNoLock(int retailerId);
        Task<List<Domain.Model.OmniChannel>> GetAllChannels(List<int> includeRetailerIds, List<int> excludeRetailerIds);
        Task<List<OmniChannelDto>> GetChannelByChannelType(byte channelType, List<int> includeRetailerIds);
    }
}
﻿using Demo.OmniChannel.Domain.Model;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IPartnerDeliveryRepository : IBaseRepository<PartnerDelivery>
    {
        Task<PartnerDelivery> GetPartnerDeliveryByCodeAsync(int retailerId, string code);
    }
}
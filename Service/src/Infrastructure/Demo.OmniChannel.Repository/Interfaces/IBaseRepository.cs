﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Common;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IBaseRepository<T> where T : class
    {
        #region Sync Methods
        long Add(T obj);
        T Update(T obj);
        bool Exists(Expression<Func<T, bool>> filter);

        #endregion

        #region Async Methods   
        Task<IEnumerable<T>> GetAllAsync(int? offset = 0, int? limit = int.MaxValue, Expression<Func<T, bool>> filter = null);
        Task<List<TResult>> GetAsync<TResult>(Expression<Func<T, bool>> filter, int? skip = null, int? take = null, string[] orderBy = null, string[] orderByDesc = null);
        Task<List<TResult>> GetAsync<TResult>(SqlExpression<T> query, int? skip = null, int? take = null, string[] orderBy = null, string[] orderByDesc = null);
        Task<List<T>> GetHasReferenceAsync(Expression<Func<T, bool>> filter);
        Task<List<T>> WhereAsync(Expression<Func<T, bool>> filter);
        Task<List<T>> WhereAsync(SqlExpression<T> filter);
        Task<T> SingleAsync(Expression<Func<T, bool>> filter);
        Task<T> SingleAsync(SqlExpression<T> filter);
        List<T> Where(Expression<Func<T, bool>> filter);
        List<T> GetAll();
        Task<bool> ExistsAsync(Expression<Func<T, bool>> filter);
        Task<T> GetByIdAsync(object id, bool isLoadReference = false);
        Task<long> AddAsync(T obj);
        Task<T> UpdateAsync(T obj);
        Task UpdateOnlyAsync(T obj, IList<string> fieldChangeds, Expression<Func<T, bool>> where);
        Task DeleteAsync(Expression<Func<T, bool>> where, bool isSoftDelete = false);
        Task<long> CountAsync(Expression<Func<T, bool>> filter);
        Task<long> CountAsync(SqlExpression<T> filter);
        Task<bool> SaveAsync(T obj, bool references = false);
        Task<int> UpdateAllAsync(List<T> lst);
        Task InsertAllAsync(List<T> lst);
        Task<PagingDataSource<TInto>> Execute<TInto>(ISqlExpression query);

        #endregion

        SqlExpression<T> BuildGenericQuery(Expression<Func<T, bool>> filter);
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface ISaleChannelRepository : IBaseRepository<SaleChannel>
    {
        Task<SaleChannel> GetByOmmiChannelId(long retailerId, long ommiChannelId);
        Task<List<SaleChannel>> GetSaleChannelListByOmmiChannelIds(int retailerId, List<long?> omniChannelIds);
        Task<SaleChannel> GetSaleChannelFB(long retailerId);
    }
}
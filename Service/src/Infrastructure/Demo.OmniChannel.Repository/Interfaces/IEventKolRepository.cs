﻿using Demo.OmniChannel.Domain.Model;
using System;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IEventKolRepository : IBaseRepository<EventKol>
    {
        Task<object> GetEventFail(DateTime fromDate, DateTime toDate, int page, int numberOfPage);
    }
}


using Demo.OmniChannel.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IProductSerialRepository : IBaseRepository<ProductSerial>
    {
        Task<List<ProductSerial>> GetListByProductIds(int retailerId, List<long> productIds);
    }

}

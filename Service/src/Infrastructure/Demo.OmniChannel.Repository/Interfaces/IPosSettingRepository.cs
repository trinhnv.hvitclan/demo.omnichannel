﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IPosSettingRepository : IBaseRepository<PosParameter>
    {
        Task<Dictionary<string, string>> GetSettingAsync(int retailerId);
        Task<PosParameter> GetSettingOmniChannel(int retailerId, string Key);
    }
}
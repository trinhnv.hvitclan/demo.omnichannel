﻿using Demo.OmniChannel.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface ICustomerRepository : IBaseRepository<Customer>
    {
        Task<Customer> GetByFilterAsync(int retailerId, int branchId, string phone, string customerCode, bool isManagerCustomerByBranch);
        Task<List<Customer>> GetListByIds(int retailerId, List<long> customerIds);
        Task<Customer> GetAsync(int retailerId, int branchId, string phone, string customerCode, bool isManagerCustomerByBranch);
    }

}
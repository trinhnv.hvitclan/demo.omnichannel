﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IProductBatchExpireBranchRepository : IBaseRepository<ProductBatchExpireBranch>
    {
        Task<List<ProductBatchExpireBranch>> GetListByBatchExpireIdsAndOnhand(int retailerId, int branchId,
            List<long> batchExpireIds);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Demo.OmniChannel.ShareKernel.Abstractions;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IBaseDeleteRepository<T> : IBaseRepository<T> where T : class, IDeleted, new()
    {
        Task<IEnumerable<T>> GetAllAsync(int? offset = 0, int? limit = int.MaxValue, Expression<Func<T, bool>> filter = null, bool isIncludeDeleted = false);

        Task<List<TResult>> GetAsync<TResult>(Expression<Func<T, bool>> filter, int? skip = null, int? take = null, string[] orderBy = null, string[] orderByDesc = null, bool isIncludeDeleted = false);

        Task<List<TResult>> GetAsync<TResult>(SqlExpression<T> query, int? skip = null, int? take = null, string[] orderBy = null, string[] orderByDesc = null, bool isIncludeDeleted = false);

        Task<List<T>> GetHasReferenceAsync(Expression<Func<T, bool>> filter, bool isIncludeDeleted = false);

        Task<List<T>> WhereAsync(Expression<Func<T, bool>> filter, bool isIncludeDeleted = false);

        Task<List<T>> WhereAsync(SqlExpression<T> filter, bool isIncludeDeleted = false);

        Task<T> SingleAsync(Expression<Func<T, bool>> filter, bool isIncludeDeleted = false);

        Task<T> SingleAsync(SqlExpression<T> filter, bool isIncludeDeleted = false);

        Task<bool> ExistsAsync(Expression<Func<T, bool>> filter, bool isIncludeDeleted = false);

        Task<T> GetByIdAsync(object id, bool isLoadReference = false, bool isIncludeDeleted = false);

        Task<long> CountAsync(Expression<Func<T, bool>> filter, bool isIncludeDeleted = false);

        Task<long> CountAsync(SqlExpression<T> filter, bool isIncludeDeleted = false);
    }
}
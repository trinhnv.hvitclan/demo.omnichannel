﻿namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IOmniChannelWareHouseRepository : IBaseDeleteRepository<Domain.Model.OmniChannelWareHouse>
    {
    }
}

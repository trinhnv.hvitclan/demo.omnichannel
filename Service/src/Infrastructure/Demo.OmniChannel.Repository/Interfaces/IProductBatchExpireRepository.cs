﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IProductBatchExpireRepository : IBaseRepository<ProductBatchExpire>
    {
        Task<List<ProductBatchExpire>> GetListByProductIds(int retailerId, List<long> productIds);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IPriceBookRepository
    {
        Task<(decimal?, DateTime?, DateTime?,string)> GetPriceByPriceBook(int retailerId, int branchId, long productId, long priceBookId);
        Task<Dictionary<long, decimal>> GetPriceByProductIds(long priceBookId, HashSet<long> productIds);
        Task<PriceBookDetail> GetPriceByProductId(long priceBookId, long productId);
        Task<List<PriceBook>> GetByFilterAsync(int retailerId, List<long> idLst, bool isAcvite);
        Task<PriceBook> GetById(int retailerId, long id);
        Task<List<PriceBookDetail>> GetDetailById(int retailerId, long id, List<long> productIds = null);
        Task<List<PriceBook>> GetByIds(int retailerId, List<long> ids);
        Task<List<PriceBook>> GetByRetailer(int retailerId);
    }
}
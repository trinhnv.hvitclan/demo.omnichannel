﻿using Demo.OmniChannel.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IProductImageRepository
    {
        Task<List<ProductImage>> GetProductImagesByProductIds(List<long> productIds, int retailerId);
    }
}

﻿using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IOmniChannelAuthRepository : IBaseRepository<OmniChannelAuth>
    {
        Task<OmniChannelAuth> GetByChannelIdAsync(long channelId);
        Task<OmniChannelAuth> UpdateAuthWithExpire(OmniChannelAuth auth);
    }
}
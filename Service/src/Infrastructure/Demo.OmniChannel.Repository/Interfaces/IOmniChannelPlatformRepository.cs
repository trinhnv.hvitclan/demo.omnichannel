﻿using System.Collections.Generic;
using Demo.OmniChannel.Domain.Model;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IOmniChannelPlatformRepository : IBaseRepository<OmniChannelPlatform>
    {

        Task<OmniChannelPlatform> GetById(long id);

        Task<List<OmniChannelPlatform>> GetByIds(List<long> ids);

        Task<OmniChannelPlatform> GetCurrent(int platformType, int? appSupport = null);
    }
}

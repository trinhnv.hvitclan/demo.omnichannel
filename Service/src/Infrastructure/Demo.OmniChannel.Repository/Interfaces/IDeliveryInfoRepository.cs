﻿using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.ShareKernel.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IDeliveryInfoRepository : IBaseRepository<DeliveryInfo>
    {
        Task<DeliveryInfoDto> GetLastByOrderIdAsync(int retailerId, long orderId);
        Task<DeliveryInfo> GetLastByInvoiceIdAsync(long invoiceId);
        Task<List<DeliveryInfo>> GetDeliveryInfoByListOrderId(List<long?> orderIdLst, int retailerId, int pageSize = 50);
        Task<List<DeliveryInfo>> GetDeliveryInfoByListInvoices(List<long?> invoiceIdLst, int retailerId, int pageSize =50);
    }
}
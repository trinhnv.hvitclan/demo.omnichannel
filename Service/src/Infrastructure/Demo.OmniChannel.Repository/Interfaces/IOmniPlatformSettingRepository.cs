﻿using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.ShareKernel.Dto;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IOmniPlatformSettingRepository : IBaseRepository<OmniPlatformSetting>
    {
        Task<CreateUpdateSettingCustomer> CreateOrUpdateAsync(CreateUpdateSettingCustomer settingDto, int retailerId);

        Task<OmniPlatformSettingDto> GetByRetailerAsync(int retailerId);
    }
}

﻿using Demo.OmniChannel.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IProductFormulaRepository : IBaseRepository<ProductFormula>
    {
        Task<List<ProductFormula>> GetByProductIds(int retailerId, List<long> productIds);
    }
}
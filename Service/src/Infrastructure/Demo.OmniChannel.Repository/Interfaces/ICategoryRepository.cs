﻿using Demo.OmniChannel.Domain.Model;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface ICategoryRepository : IBaseRepository<Category>
    {
        Task<Category> GetProductByNameAsync(int retailerId, string name, bool isParent = true);
    }
}
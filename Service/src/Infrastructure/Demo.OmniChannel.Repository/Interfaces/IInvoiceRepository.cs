﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IInvoiceRepository : IBaseRepository<Invoice>
    {
        Task<long?> CheckExistInvoice(long orderId, int retailerId, List<long> ids);
        Task<List<Invoice>> GetByOrderId(long orderId, int retailerId);

        Task<List<Invoice>> GetByLstOrderId(List<long?> orderIdLst, int retailerId, int pageSize);

        Task<List<Invoice>> GetLastExistedInvoiceLst(string originalCode, int retailerId);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.ShareKernel.Common;

namespace Demo.OmniChannel.Repository.Interfaces
{
    public interface IOmniChannelScheduleRepository : IBaseRepository<OmniChannelSchedule>
    {
        Task<List<long>> GetIds(byte channelType, byte scheduleType, List<int> includeRetailerIds, List<int> excludeRetailerIds, int skip, int take);
        Task<long> GetTotal(byte channelType, byte scheduleType, List<int> includeRetailerIds, List<int> excludeRetailerIds);
        Task<OmniChannelSchedule> UpdateExecutePlan(long scheduleId, DateTime? lastSync, DateTime? nextRun, bool isUnlock = false);
        Task<OmniChannelSchedule> UpdateRunningStatus(long scheduleId, bool isRunning);
        Task ReleaseAsync(long id, DateTime lastSync);
        Task UnlockAsync(long id);
        Task<List<ScheduleDto>> GetScheduleForJob(byte channelType, int scheduleType, List<int> includeRetail = null, List<int> excludeRetail = null, List<long> notScheduleIds = null);
        Task<ScheduleDto> GetScheduleForJobById(long id);
        Task<OmniChannelSchedule> ResetExecutePlans(long channelId, byte scheduleType, DateTime? lastSync = null);
        Task<ScheduleDto> GetSchedule(long channelId, int scheduleType);
    }
}
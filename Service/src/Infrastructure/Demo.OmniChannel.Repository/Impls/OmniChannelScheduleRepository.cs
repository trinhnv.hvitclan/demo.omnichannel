﻿using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.SqlServer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Impls
{
    public class OmniChannelScheduleRepository : BaseRepository<OmniChannelSchedule>, IOmniChannelScheduleRepository
    {
        public OmniChannelScheduleRepository(IDbConnection dbConnection) : base(dbConnection)
        {

        }

        public async Task<List<long>> GetIds(byte channelType, byte scheduleType, List<int> includeRetailerIds, List<int> excludeRetailerIds, int skip, int take)
        {
            var query = GetQueryChannelValid(channelType, scheduleType, includeRetailerIds, excludeRetailerIds);

            query = query.OrderBy(oc => oc.Id).Skip(skip).Take(take)
                .Select<Domain.Model.OmniChannel, OmniChannelSchedule>((oc, sc) => sc.Id);

            var r = await Db.SelectAsync<long>(query);

            return r;
        }

        public async Task<long> GetTotal(byte channelType, byte scheduleType, List<int> includeRetailerIds, List<int> excludeRetailerIds)
        {
            var query = GetQueryChannelValid(channelType, scheduleType, includeRetailerIds, excludeRetailerIds);

            var r = await Db.CountAsync(query);

            return r;
        }

        private SqlExpression<Domain.Model.OmniChannel> GetQueryChannelValid(byte channelType, byte scheduleType, List<int> includeRetailerIds, List<int> excludeRetailerIds)
        {
            var query = Db.From<Domain.Model.OmniChannel>().WithReadUnCommited()
                .Join<OmniChannelSchedule>((oc, sc) => sc.OmniChannelId == oc.Id, SqlServerTableHint.ReadUncommitted)
                .Where<Domain.Model.OmniChannel, OmniChannelSchedule>((oc, sc) => (oc.IsDeleted == null || oc.IsDeleted == false) &&
                                                                                  oc.IsActive == true &&
                                                                                  oc.Status != (byte)ChannelStatusType.NeedRegister &&
                                                                                  oc.Type == channelType &&
                                                                                  sc.Type == scheduleType);

            if (includeRetailerIds?.Any() == true) query = query.Where(oc => includeRetailerIds.Contains(oc.RetailerId));
            if (excludeRetailerIds?.Any() == true) query = query.Where(oc => !excludeRetailerIds.Contains(oc.RetailerId));

            return query;
        }

        public async Task<OmniChannelSchedule> UpdateExecutePlan(long scheduleId, DateTime? lastSync, DateTime? nextRun, bool isUnlock = false)
        {
            var itemUpdated = await GetByIdAsync(scheduleId);
            if (itemUpdated != null)
            {
                if (!isUnlock)
                {
                    itemUpdated.LastSync = lastSync;
                    itemUpdated.NextRun = nextRun ?? DateTime.Now;
                }
                itemUpdated.IsRunning = false;
                return await UpdateAsync(itemUpdated);
            }
            return null;
        }

        public async Task<OmniChannelSchedule> UpdateRunningStatus(long scheduleId, bool isRunning)
        {
            var itemUpdated = await GetByIdAsync(scheduleId);
            if (itemUpdated != null)
            {
                itemUpdated.IsRunning = isRunning;
                return await UpdateAsync(itemUpdated);
            }
            return null;
        }

        public async Task ReleaseAsync(long id, DateTime lastSync)
        {
            //2018-05-08 07:52:04.210
            await Db.ExecuteSqlAsync(
                "Update OmniChannelSchedule set IsRunning=0,  NextRun=DATEADD(minute," + 1 + $",NextRun),LastSync='{lastSync.AddMinutes(1):yyyy-MM-dd HH:mm:ss.fff}' where Id={id}");
        }

        public async Task UnlockAsync(long id)
        {
            //await Db.ExecuteSqlAsync(
            //    "Update KvChannelSchedule set IsRunning=0, NextRun=DATEADD(minute," + 1 + $",NextRun) where Id={id}");
            var itemUpdated = await GetByIdAsync(id);
            if (itemUpdated != null)
            {
                itemUpdated.NextRun = DateTime.Now.AddMinutes(-5);
                itemUpdated.IsRunning = false;
                await UpdateAsync(itemUpdated);
            }
        }

        public async Task<List<ScheduleDto>> GetScheduleForJob(byte channelType, int scheduleType, List<int> includeRetail = null, List<int> excludeRetail = null, List<long> notScheduleIds = null)
        {
            var query = Db.From<Domain.Model.OmniChannel>().Join<OmniChannelSchedule>((channel, schedule) => channel.Id == schedule.OmniChannelId)
                .Where<Domain.Model.OmniChannel>(x => x.IsActive && (x.IsDeleted == null || x.IsDeleted == false))
                .Where<OmniChannelSchedule>(x => x.Type == scheduleType)
                .Select<Domain.Model.OmniChannel, OmniChannelSchedule>((channel, schedule) => new
                {
                    ChannelId = channel.Id,
                    channel.RetailerId,
                    channel.BasePriceBookId,
                    ChannelType = channel.Type,
                    ChannelBranch = channel.BranchId,
                    schedule
                });

            query = query.Where(x => x.Type == channelType);
            if (notScheduleIds != null && notScheduleIds.Any())
            {
                query = query.Where<OmniChannelSchedule>(x => !notScheduleIds.Contains(x.Id));
            }
            if (includeRetail != null && includeRetail.Any())
            {
                query = query.Where(x => includeRetail.Contains(x.RetailerId));
            }
            if (excludeRetail != null && excludeRetail.Any())
            {
                query = query.Where(x => !excludeRetail.Contains(x.RetailerId));
            }
            return await Db.SelectAsync<ScheduleDto>(query);
        }

        public async Task<List<ScheduleDto>> GetScheduleDtoByIds(List<long> ids)
        {
            var query = Db.From<Domain.Model.OmniChannel>().Join<OmniChannelSchedule>((channel, schedule) => channel.Id == schedule.OmniChannelId)
                .Where<OmniChannelSchedule>(x => ids.Contains(x.Id))
                .Select<Domain.Model.OmniChannel, OmniChannelSchedule>((channel, schedule) => new
                {
                    ChannelId = channel.Id,
                    channel.RetailerId,
                    channel.BasePriceBookId,
                    ChannelType = channel.Type,
                    ChannelBranch = channel.BranchId,
                    schedule
                });
            return await Db.SelectAsync<ScheduleDto>(query);
        }

        public async Task<ScheduleDto> GetScheduleForJobById(long id)
        {
            var query = Db.From<Domain.Model.OmniChannel>()
                .Join<OmniChannelSchedule>((channel, schedule) => channel.Id == schedule.OmniChannelId)
                .Where<OmniChannelSchedule>(x => x.Id == id)
                .Select<Domain.Model.OmniChannel, OmniChannelSchedule>((channel, schedule) => new
                {
                    ChannelId = channel.Id,
                    channel.RetailerId,
                    channel.BasePriceBookId,
                    ChannelType = channel.Type,
                    ChannelBranch = channel.BranchId,
                    schedule
                });

            var result = await Db.SingleAsync<ScheduleDto>(query);

            return result;
        }

        public async Task<ScheduleDto> GetSchedule(long channelId, int scheduleType)
        {
            var query = Db.From<Domain.Model.OmniChannel>().Join<OmniChannelSchedule>((channel, schedule) => channel.Id == schedule.OmniChannelId)
                .Where<Domain.Model.OmniChannel>(x => x.Id == channelId && x.IsActive && (x.IsDeleted == null || x.IsDeleted == false))
                .Where<OmniChannelSchedule>(x => x.Type == scheduleType)
                .Select<Domain.Model.OmniChannel, OmniChannelSchedule>((channel, schedule) => new
                {
                    ChannelId = channel.Id,
                    channel.RetailerId,
                    channel.BasePriceBookId,
                    ChannelType = channel.Type,
                    ChannelBranch = channel.BranchId,
                    channel.SyncOrderFormula,
                    schedule
                });

            return (await Db.SelectAsync<ScheduleDto>(query)).FirstOrDefault();
        }

        public async Task<OmniChannelSchedule> ResetExecutePlans(long channelId, byte scheduleType, DateTime? lastSync = null)
        {
            var query = Db.From<OmniChannelSchedule>().Where(x => x.OmniChannelId == channelId && x.Type == scheduleType);
            var schedule = (await Db.SelectAsync(query)).FirstOrDefault();
            if (schedule != null)
            {
                schedule.NextRun = DateTime.Now;
                schedule.IsRunning = false;
                schedule.LastSync = lastSync;
                return await UpdateAsync(schedule);
            }
            return null;
        }
    }
}
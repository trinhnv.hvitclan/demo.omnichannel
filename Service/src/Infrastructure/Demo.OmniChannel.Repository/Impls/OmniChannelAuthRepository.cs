﻿using System;
using System.Data;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.Utilities;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Repository.Impls
{
    public class OmniChannelAuthRepository : BaseRepository<OmniChannelAuth>, IOmniChannelAuthRepository
    {
        public OmniChannelAuthRepository(IDbConnection dbConnection) : base(dbConnection)
        {

        }
        public async Task<OmniChannelAuth> GetByChannelIdAsync(long channelId)
        {
            var auth = await Db.SingleAsync<OmniChannelAuth>(x => x.OmniChannelId == channelId);
            if (auth != null && !string.IsNullOrEmpty(auth.AccessToken))
            {
                auth.AccessTokenDecode = CryptoHelper.RijndaelDecrypt(auth.AccessToken);
            }
            return auth;
        }

        public async Task<OmniChannelAuth> UpdateAuthWithExpire(OmniChannelAuth auth)
        {
            var oldModifiedDate = auth.ModifiedDate ?? auth.CreatedDate;
            var time = DateTime.Now - oldModifiedDate;
            auth.ExpiresIn = auth.ExpiresIn > time.TotalSeconds ? auth.ExpiresIn - (int)time.TotalSeconds : 0;
            auth.RefreshExpiresIn = auth.RefreshExpiresIn > time.TotalSeconds ? auth.RefreshExpiresIn - (int)time.TotalSeconds : 0;
            return await UpdateAsync(auth);
        }
    }
}
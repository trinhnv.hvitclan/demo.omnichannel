﻿using System.Data;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Interfaces;

namespace Demo.OmniChannel.Repository.Impls
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(IDbConnection dbConnection) : base(dbConnection)
        {

        }
    }
}
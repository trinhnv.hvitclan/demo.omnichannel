﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.Sdk.Common;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Repository.Impls
{
    public class OmniChannelPlatformRepository : BaseRepository<OmniChannelPlatform>, IOmniChannelPlatformRepository
    {
        public OmniChannelPlatformRepository(IDbConnection dbConnection) : base(dbConnection)
        {
        }

        public async Task<OmniChannelPlatform> GetById(long id)
        {
            var query =
                Db.From<OmniChannelPlatform>().Where(x => x.Id == id);

            return (await Db.SelectAsync(query)).FirstOrDefault();
        }

        public async Task<List<OmniChannelPlatform>> GetByIds(List<long> ids)
        {
            var query =
                Db.From<OmniChannelPlatform>().Where(x => ids.Contains(x.Id));

            return (await Db.SelectAsync(query)).ToList();
        }

        public async Task<OmniChannelPlatform> GetCurrent(int platformType, int? appSupport = null)
        {
            var query =
                Db.From<OmniChannelPlatform>().Where(x => x.IsCurrent && x.PlatformType == platformType &&
                x.AppSupport == (appSupport ?? (int) AppSupports.KiotOnlinePro));
            return (await Db.SelectAsync(query)).FirstOrDefault();
        }
    }
}

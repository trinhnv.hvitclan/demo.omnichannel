using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Interfaces;
using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Impls
{
    public class ProductAttributeRepository : BaseRepository<ProductAttribute>, IProductAttributeRepository
    {
        public ProductAttributeRepository(IDbConnection dbConnection) : base(dbConnection)
        {
        }

        public async Task<List<ProductAttribute>> GetByProductIds(List<long> ids, int retailerId)
        {
            var query = Db.From<ProductAttribute>().WithReadUnCommited().Where(x => Sql.In(x.ProductId, ids) && x.RetailerId == retailerId);
            var result = await Db.LoadSelectAsync(query);
            return result;
        }
    }
}

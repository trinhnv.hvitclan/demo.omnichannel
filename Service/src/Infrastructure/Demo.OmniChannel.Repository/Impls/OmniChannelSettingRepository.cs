﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Demo.OmniChannel.Api.ServiceModel.Request;
using Demo.OmniChannel.ShareKernel.Dto;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Repository.Impls
{
    public class OmniChannelSettingRepository : BaseRepository<OmniChannelSetting>, IOmniChannelSettingRepository
    {
        private readonly List<OmniChannelSetting> _settingNeedAdd = new List<OmniChannelSetting>();
        private readonly List<OmniChannelSetting> _settingNeedUpdate = new List<OmniChannelSetting>();
        public OmniChannelSettingRepository(IDbConnection dbConnection) : base(dbConnection)
        {
        }

        public async Task CreateOrUpdateSettingChannel(OmniChannelSettingRequest settingDto,
            Domain.Model.OmniChannel channel,
            List<Domain.Model.OmniChannel> allChannelOfCurrentRetailer)
        {
            if (settingDto == null) return;

            if(settingDto.SyncOrderFormulaType == (byte)SyncOrderFormulaType.Create && channel.SyncOrderFormula.HasValue && string.IsNullOrEmpty(settingDto.SyncOrderFormulaDateTime))
            {
                settingDto.SyncOrderFormulaDateTime = GetSyncOrderFormulaDateTime((SyncOrderFormula)channel.SyncOrderFormula);
            }

            var settingItemListDto = settingDto.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .ToDictionary(prop => prop.Name, prop => prop.GetValue(settingDto, null)?.ToString())
                .Where(s => s.Key != nameof(settingDto.IsApplyAllBatchExpire)).ToList();

            var oldSettingQuery =
                Db.From<OmniChannelSetting>().Where(x => x.OmniChannelId == channel.Id
                                                         && x.RetailerId == channel.RetailerId);
            var lsSetting = (await Db.SelectAsync(oldSettingQuery)).ToList();

            AddUpdateSetting(settingItemListDto, lsSetting, channel);

            if (settingDto.IsApplyAllBatchExpire)
            {
                var channelOfCurrentRetail =
                    allChannelOfCurrentRetailer.Where(x => x.IsActive && (x.IsDeleted == null || !x.IsDeleted.Value))
                        .ToList();
                var channelOfCurrentRetailIds = channelOfCurrentRetail.Select(c => c.Id).ToList();

                if (channelOfCurrentRetail.Any())
                {
                    var settingOfCurrentRetailQuery =
                        Db.From<OmniChannelSetting>().Where(x => x.RetailerId == channel.RetailerId
                                                                 && channelOfCurrentRetailIds.Contains(x.OmniChannelId));
                    var settingOfCurrentRetail = (await Db.SelectAsync(settingOfCurrentRetailQuery)).ToList();

                    foreach (var channelOther in channelOfCurrentRetail)
                    {
                        var settingForChannel = settingOfCurrentRetail.Where(s => s.OmniChannelId == channelOther.Id).ToList();
                        var settingDtoForChannel = settingItemListDto.Where(s => s.Key == nameof(settingDto.IsAutoSyncBatchExpire)).ToList();
                        AddUpdateSetting(settingDtoForChannel, settingForChannel, channelOther);
                    }
                }
            }

            if (_settingNeedUpdate.Any())
            {
                await UpdateAllAsync(_settingNeedUpdate);
            }

            if (_settingNeedAdd.Any())
            {
                await InsertAllAsync(_settingNeedAdd);
            }
        }
        private string GetSyncOrderFormulaDateTime(SyncOrderFormula OrderFormula)
        {
            DateTime dateTime = DateTime.Now;
            switch (OrderFormula)
            {
                case SyncOrderFormula.Today:
                    dateTime = dateTime.Date.AddSeconds(-1);
                    break;
                case SyncOrderFormula.Last7Day:
                    dateTime = dateTime.AddDays(-7);
                    break;
                case SyncOrderFormula.Last15Day:
                    dateTime = dateTime.AddDays(-15);
                    break;
                case SyncOrderFormula.Last30Day:
                    dateTime = dateTime.AddDays(-30);
                    break;
            }
            return dateTime.ToString("yyyy-MM-dd HH:mm:ss.fff");
        }
        public async Task<List<OmniChannelSetting>> GetChannelSettings(long channelId, int retailerId)
        {
            var settingsQuery =
                Db.From<OmniChannelSetting>().WithReadUnCommited().Where(x => x.OmniChannelId == channelId
                                                                            && x.RetailerId == retailerId);
            var setting = (await Db.SelectAsync(settingsQuery)).ToList();

            return setting;
        }

        public async Task<List<OmniChannelSetting>> GetChannelSettings(List<long> channelIds, int retailerId)
        {
            var settingsQuery = Db.From<OmniChannelSetting>().WithReadUnCommited().Where(x => channelIds.Contains(x.OmniChannelId) && x.RetailerId == retailerId);
            return (await Db.SelectAsync(settingsQuery)).ToList();
        }

        public async Task MarkChannelSentNotificationExpired(bool isMark, int retailerId, List<Domain.Model.OmniChannel> channels)
        {
            var channelIds = channels.Select(x => x.Id);
            var oldSettingQuery = Db.From<OmniChannelSetting>().Where(x => channelIds.Contains(x.OmniChannelId) && x.RetailerId == retailerId);
            var oldSettings = (await Db.SelectAsync(oldSettingQuery)).ToList();
            foreach (var channel in channels)
            {
                var existSetting = oldSettings.FirstOrDefault(x => nameof(OmniChannelSettingDto.IsSentExpirationNotification).Equals(x.Name));
                if (existSetting != null)
                {
                    existSetting.Update($"{isMark}");
                    _settingNeedUpdate.Add(existSetting);
                }
                else
                {
                    var newSetting = new OmniChannelSetting(nameof(OmniChannelSettingDto.IsSentExpirationNotification), $"{isMark}", true, channel.Type, channel.Id,
                        channel.BranchId, channel.RetailerId, channel.IdentityKey);
                    _settingNeedAdd.Add(newSetting);
                }
            }

            if (_settingNeedUpdate.Any())
            {
                await UpdateAllAsync(_settingNeedUpdate);
            }

            if (_settingNeedAdd.Any())
            {
                await InsertAllAsync(_settingNeedAdd);
            }
        }

        #region Private method
        private void AddUpdateSetting(List<KeyValuePair<string, string>> settingItemListDto, List<OmniChannelSetting> lsSetting, Domain.Model.OmniChannel channel)
        {
            foreach (var item in settingItemListDto)
            {
                if (string.IsNullOrEmpty(item.Key)) continue;
                var setting = lsSetting.FirstOrDefault(x => x.Name == item.Key);
                if (setting != null)
                {
                    setting.Update(item.Value);
                    _settingNeedUpdate.Add(setting);
                }
                else
                {
                    var newSetting = new OmniChannelSetting(item.Key, item.Value, true, channel.Type, channel.Id,
                        channel.BranchId, channel.RetailerId, channel.IdentityKey);
                    _settingNeedAdd.Add(newSetting);
                }
            }
        }

        #endregion
    }
}

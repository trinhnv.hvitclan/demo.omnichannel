﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Interfaces;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Repository.Impls
{
    public class ProductBranchRepository : BaseRepository<ProductBranch>, IProductBranchRepository
    {
        public ProductBranchRepository(IDbConnection dbConnection) : base(dbConnection)
        {

        }

        public async Task<List<ProductBranch>> GetProduct(int retailerId,
            long branchId, List<long> productIds)
        {
            var query = Db.From<ProductBranch>().WithReadUnCommited().Where(x =>
               x.RetailerId == retailerId && x.BranchId == branchId && productIds.Contains(x.ProductId) &&
             (x.IsActive == null || x.IsActive == true));

            return await Db.SelectAsync(query);
        }

        public async Task<List<ProductBranch>> GetMissingDataEs(int retailerId, int branchId, List<long> productIds)
        {
            var query = Db.From<ProductBranch>().WithReadUnCommited().Where(x => x.RetailerId == retailerId && x.BranchId == branchId
            && productIds.Contains(x.ProductId) && (x.IsActive == null || x.IsActive == true)).Select(x => new
            {
                ProductId = x.ProductId,
                OnHand = x.OnHand,
                Reserved = x.Reserved,
                OnOrder = x.OnOrder,
                LatestPurchasePrice = x.LatestPurchasePrice,
                Cost = x.Cost,
            });

            var result = await Db.SelectAsync(query);
            return result;
        }
    }
}
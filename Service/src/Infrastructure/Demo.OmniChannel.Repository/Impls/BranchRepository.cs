﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Interfaces;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Repository.Impls
{
    public class BranchRepository : BaseRepository<Branch>, IBranchRepository
    {
        public BranchRepository(IDbConnection dbConnection) : base(dbConnection)
        {

        }

        public async Task<List<Branch>> GetListBranchByIds(int retailerId, List<int> branchIds)
        {
            var query = Db.From<Branch>().WithReadUnCommited()
                .Where(x => x.RetailerId == retailerId && branchIds.Contains(x.Id));
            var result = await Db.LoadSelectAsync(query);
            return result;
        }

    }
}
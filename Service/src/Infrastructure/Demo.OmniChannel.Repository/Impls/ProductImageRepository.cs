﻿using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.Utilities;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Impls
{
    public class ProductImageRepository : BaseRepository<ProductImage>, IProductImageRepository
    {
        public ProductImageRepository(IDbConnection dbConnection) : base(dbConnection)
        {
        }

        public async Task<List<ProductImage>> GetProductImagesByProductIds(List<long> productIds, int retailerId)
        {
            
            int totalCount = productIds.Count;
            var totalPages = (int)Math.Ceiling(totalCount / (decimal)200);
            var productImageLst = new List<ProductImage>();
            for (int page = 1; page <= totalPages; page++)
            {
                var productImagePaging = productIds.Page(page, 200).ToList();
                var productImageResult = await Db.LoadSelectAsync<ProductImage>(
                    c => c.RetailerId == retailerId && productImagePaging.Contains(c.ProductId));
                productImageLst.AddRange(productImageResult);
            }
            return productImageLst;
        }
    }
}

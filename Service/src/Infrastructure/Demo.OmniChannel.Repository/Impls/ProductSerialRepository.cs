﻿using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Interfaces;
using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Impls
{
    public class ProductSerialRepository : BaseRepository<ProductSerial>, IProductSerialRepository
    {
        public ProductSerialRepository(IDbConnection dbConnection) : base(dbConnection)
        {
        }

        public async Task<List<ProductSerial>> GetListByProductIds(int retailerId, List<long> productIds)
        {
            var query = Db.From<ProductSerial>().WithReadUnCommited()
                .Where(x => x.RetailerId == retailerId && productIds.Contains(x.ProductId));
            var result = await Db.SqlListAsync<ProductSerial>(query);
            return result;
        }
    }
}

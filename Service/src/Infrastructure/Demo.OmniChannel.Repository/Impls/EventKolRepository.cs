﻿using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Interfaces;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Impls
{
    public class EventKolRepository : BaseRepository<EventKol>, IEventKolRepository
    {
        public EventKolRepository(IDbConnection dbConnection) : base(dbConnection)
        {
        }

        public async Task<object> GetEventFail(DateTime fromDate, DateTime toDate, int page, int numberOfPage)
        {
            page = numberOfPage * (page == 0 ? 0 : page - 1);
            var eventKolList = await Db.SelectAsync<EventKol>(@"select * from (select rs2.Id, rs2.DocumentId, rs2.DocumentType, rs2.DocumentCode from 
                    (select rs.Id from (select sum(Status) as SumStatus, Max(Id) as Id from EventKol with(NOLOCK) where CreatedDate >  N'" +
                                                              fromDate.ToString("yyyy-MM-dd HH:mm:ss.fff") + @"' and CreatedDate < N'" + toDate.ToString("yyyy-MM-dd HH:mm:ss.fff") + @"' group by RetailerId, DocumentId) 
                    rs where SumStatus < 0) rs1
                    INNER JOIN EventKol as rs2  with(NOLOCK) on rs1.Id = rs2.Id
                    LEFT JOIN EventKol as rs3 with(NOLOCK) on rs3.EventId = rs1.Id
                    where rs3.Id IS NULL AND rs2.CreatedDate > N'" + fromDate.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' and rs2.CreatedDate < N'" + toDate.ToString("yyyy-MM-dd HH:mm:ss.fff") +
                                                              @"') rs5 order by rs5.Id desc OFFSET " + page + " ROWS FETCH NEXT " + numberOfPage +
                                                              " ROWS ONLY");

            var eventKolCount = await Db.ScalarAsync<int>(@"select count(*) from (select rs2.Id, rs2.DocumentId, rs2.DocumentType, rs2.DocumentCode from 
                    (select rs.Id from (select sum(Status) as SumStatus, Max(Id) as Id from EventKol with(NOLOCK) where CreatedDate >  N'" +
                                                          fromDate.ToString("yyyy-MM-dd HH:mm:ss.fff") + @"' and CreatedDate < N'" + toDate.ToString("yyyy-MM-dd HH:mm:ss.fff") + @"' group by RetailerId, DocumentId) 
                    rs where SumStatus < 0) rs1
                    INNER JOIN EventKol as rs2  with(NOLOCK) on rs1.Id = rs2.Id
                    LEFT JOIN EventKol as rs3 with(NOLOCK) on rs3.EventId = rs1.Id
                    where rs3.Id IS NULL AND rs2.CreatedDate > N'" + fromDate.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' and rs2.CreatedDate < N'" + toDate.ToString("yyyy-MM-dd HH:mm:ss.fff") +
                                                          @"') rs5");


            var result = await GetResult(eventKolList);
            var more = eventKolCount > numberOfPage * (page + 1);
            return new {Result = result, More = more};
        }

        private async Task<List<string>> GetResult(List<EventKol> eventKolList)
        {
            var result1 = await GetEventInvoiceFailByCreateOrUpdate(eventKolList);
            var result2 = await GetEventInvoiceFailByDelete(eventKolList);
            var result3 = await GetEventOrderFailByCreateOrUpdate(eventKolList);
            var result4 = await GetEventOrderFailByDelete(eventKolList);
            var result5 = await GetEventCustomerByUpdate(eventKolList);
            var result6 = await GetEventCustomerByDelete(eventKolList);
            result1.AddRange(result2);
            result1.AddRange(result3);
            result1.AddRange(result4);
            result1.AddRange(result5);
            result1.AddRange(result6);
            return result1;
        }

        private async Task<List<string>> GetEventCustomerByUpdate(List<EventKol> eventKolList)
        {
            var customerIds = eventKolList.Where(x => x.DocumentType == 22 && x.Action != 2).Select(x => x.DocumentId)
                .ToList();


            var customerQuery = Db.From<Customer>().WithReadUnCommited().Where(x => customerIds.Contains(x.Id));
            var customers = await Db.LoadSelectAsync(customerQuery, new[] {"Retailer"});
            var result = customers.Select(item =>
            {
                var eventKol =
                    eventKolList.FirstOrDefault(e => e.DocumentType == 22 && e.DocumentId == item.Id && e.Action != 2);
                var action = eventKol?.Action ?? 0;
                var kolMapping = new KolCustomerMapping();
                kolMapping.CopyFrom(item, action == 0 ? "CREATE" : "UPDATE", eventKol?.Id ?? 0, item.Retailer?.Code);
                return kolMapping.ToSafeJson();
            }).ToList();
            return result;
        }

        private async Task<List<string>> GetEventCustomerByDelete(List<EventKol> eventKolList)
        {
            var customerIds = eventKolList.Where(x => x.DocumentType == 22 && x.Action == 2).Select(x => x.DocumentId)
                .ToList();

            var customerQuery = Db.From<Customer>().WithReadUnCommited().Where(x => customerIds.Contains(x.Id));
            var customers = await Db.LoadSelectAsync(customerQuery, new[] { "Retailer" });
            var result = customers.Select(item =>
            {
                var eventKol =
                    eventKolList.FirstOrDefault(e => e.DocumentType == 22 && e.DocumentId == item.Id && e.Action == 2);
                var kolMapping = new KolCustomerMapping();
                kolMapping.CopyFrom(item, "DELETE", eventKol?.Id ?? 0, item.Retailer?.Code);
                return kolMapping.ToSafeJson();
            }).ToList();
            return result;
        }

        private async Task<List<string>> GetEventOrderFailByCreateOrUpdate(List<EventKol> eventKolList)
        {
            var orderIds = eventKolList.Where(x => x.DocumentType == 2 && x.Action != 2).Select(x => x.DocumentId).ToList();
            var ordersQuery = Db.From<Order>().WithReadUnCommited().Where(x => orderIds.Contains(x.Id));
            var orders = await Db.LoadSelectAsync(ordersQuery, new[] { "Customer", "OrderDetails", "DeliveryInfos", "Payments", "Retailer" });

            var productIds = orders.Where(x => x.OrderDetails != null).SelectMany(x => x.OrderDetails)
                .Select(x => x.ProductId).Distinct().ToList();
            var partnerDeliveryIds = orders.Where(x => x.DeliveryInfos != null).SelectMany(x => x.DeliveryInfos)
                .Select(x => x.DeliveryBy ?? 0).Distinct().Where(x => x != 0).ToList();

            var products = await GetProducts(productIds);
            var productAttributes = await GetProductAttributes(productIds);
            var partnerDeliveries = await GetPartnerDeliveries(partnerDeliveryIds);

            var productIdsOfImage =
                products.Where(x => x.MasterUnitId.HasValue).Select(x => x.MasterUnitId ?? 0).ToList();
            productIdsOfImage.AddRange(productIds);
            productIdsOfImage = productIdsOfImage.Distinct().ToList();

            var productImages = await GetProductImages(productIdsOfImage);

            foreach (var order in orders)
            {
                foreach (var ordDetail in order.OrderDetails)
                {
                    ordDetail.Product = products.FirstOrDefault(x => x.Id == ordDetail.ProductId);
                    if (ordDetail.Product != null)
                    {
                        ordDetail.Product.ProductImages =
                            productImages.Where(x => x.ProductId == ordDetail.ProductId).ToList();
                        ordDetail.Product.ProductAttributes =
                            productAttributes.Where(x => x.ProductId == ordDetail.ProductId).ToList();
                    }
                }

                if (order.DeliveryInfos != null)
                {
                    foreach (var deliveryInfo in order.DeliveryInfos)
                    {
                        deliveryInfo.PartnerDelivery =
                            partnerDeliveries.FirstOrDefault(x => x.Id == deliveryInfo.DeliveryBy);
                    }
                }
            }

            var result = orders.Select(x =>
            {
                var eventKol =
                    eventKolList.FirstOrDefault(e => e.DocumentType == 2 && e.DocumentId == x.Id && e.Action != 2);
                var action = eventKol?.Action ?? 0;
                var kolMapping = new KolOrderMapping();
                kolMapping.CopyFrom(x, action == 0 ? "CREATE" : "UPDATE", eventKol?.Id ?? 0, x.Retailer?.Code,
                    x.CreatedBy);
                return kolMapping.ToSafeJson();
            }).ToList();
            return result;
        }

        private async Task<List<string>> GetEventOrderFailByDelete(List<EventKol> eventKolList)
        {
            var orderIds = eventKolList.Where(x => x.DocumentType == 2 && x.Action == 2).Select(x => x.DocumentId).ToList();
            var ordersQuery = Db.From<Order>().WithReadUnCommited().Where(x => orderIds.Contains(x.Id));
            var orders = await Db.LoadSelectAsync(ordersQuery, new []{ "Retailer" });

            var result = orders.Select(x =>
            {
                var eventKol =
                    eventKolList.FirstOrDefault(e => e.DocumentType == 2 && e.DocumentId == x.Id && e.Action == 2);
                var kolMapping = new KolOrderMappingDelete();
                kolMapping.CopyFrom(x.Code, x.Id, x.RetailerId, eventKol?.Id ?? 0, x.Retailer?.Code ?? "", x.CreatedBy,
                    x.BranchId);
                return kolMapping.ToSafeJson();
            }).ToList();
            return result;
        }

        private async Task<List<string>> GetEventInvoiceFailByCreateOrUpdate(List<EventKol> eventKolList)
        {
            var invoiceIds = eventKolList.Where(x => x.DocumentType == 1 && x.Action != 2).Select(x => x.DocumentId).ToList();
            if (!invoiceIds.Any()) return new List<string>();
            var invoicesQuery = Db.From<Invoice>().WithReadUnCommited().Where(x => invoiceIds.Contains(x.Id));
            var invoices = await Db.LoadSelectAsync(invoicesQuery, new [] {"Customer", "InvoiceDetails", "DeliveryInfos", "Payments", "Retailer", "Order" });

            var productIds = invoices.Where(x => x.InvoiceDetails != null).SelectMany(x => x.InvoiceDetails)
                .Select(x => x.ProductId).Distinct().ToList();
            var partnerDeliveryIds = invoices.Where(x => x.DeliveryInfos != null).SelectMany(x => x.DeliveryInfos)
                .Select(x => x.DeliveryBy ?? 0).Distinct().Where(x => x != 0).ToList();

            var products = await GetProducts(productIds);
            var productImages = await GetProductImages(productIds);
            var productAttributes = await GetProductAttributes(productIds);
            var partnerDeliveries = await GetPartnerDeliveries(partnerDeliveryIds);

            foreach (var invoice in invoices)
            {
                foreach (var invDetail in invoice.InvoiceDetails)
                {
                    invDetail.Product = products.FirstOrDefault(x => x.Id == invDetail.ProductId);
                    if (invDetail.Product != null)
                    {
                        invDetail.Product.ProductImages =
                            productImages.Where(x => x.ProductId == invDetail.ProductId).ToList();
                        invDetail.Product.ProductAttributes =
                            productAttributes.Where(x => x.ProductId == invDetail.ProductId).ToList();
                    }
                }

                if (invoice.DeliveryInfos != null)
                {
                    foreach (var deliveryInfo in invoice.DeliveryInfos)
                    {
                        deliveryInfo.PartnerDelivery =
                            partnerDeliveries.FirstOrDefault(x => x.Id == deliveryInfo.DeliveryBy);
                    }
                }
            }

            var result = invoices.Select(x =>
            {
                var eventKol =
                    eventKolList.FirstOrDefault(e => e.DocumentType == 1 && e.DocumentId == x.Id && e.Action != 2);
                var action = eventKol?.Action ?? 0;
                var kolMapping = new KolInvoiceMapping();
                kolMapping.CopyFrom(x, action == 0 ? "CREATE" : "UPDATE", eventKol?.Id ?? 0, x.Retailer?.Code,
                    x.CreatedBy);
                return kolMapping.ToSafeJson();
            }).ToList();
            return result;
        }

        private async Task<List<string>> GetEventInvoiceFailByDelete(List<EventKol> eventKolList)
        {
            var invoiceIds = eventKolList.Where(x => x.DocumentType == 1 && x.Action == 2).Select(x => x.DocumentId).ToList();
            var invoicesQuery = Db.From<Invoice>().WithReadUnCommited().Where(x => invoiceIds.Contains(x.Id));
            var invoices = await Db.LoadSelectAsync(invoicesQuery, new []{ "Retailer" });

            var result = invoices.Select(x =>
            {
                var eventKol =
                    eventKolList.FirstOrDefault(e => e.DocumentType == 1 && e.DocumentId == x.Id && e.Action == 2);
                var kolMapping = new KolInvoiceMappingDelete();
                kolMapping.CopyFrom(x.Code, x.Id, x.RetailerId, eventKol?.Id ?? 0, x.Retailer?.Code ?? "", x.CreatedBy,
                    x.BranchId);
                return kolMapping.ToSafeJson();
            }).ToList();
            return result;
        }

        private async Task<List<Product>> GetProducts(List<long> productIds)
        {
            var productsQuery = Db.From<Product>().WithReadUnCommited()
                .Where(x => productIds.Contains(x.Id));
            return await Db.LoadSelectAsync(productsQuery);
        }

        private async Task<List<ProductImage>> GetProductImages(List<long> productIds)
        {
            var productImagesQuery = Db.From<ProductImage>().WithReadUnCommited()
                .Where(x => productIds.Contains(x.ProductId));
            return await Db.LoadSelectAsync(productImagesQuery);
        }

        private async Task<List<ProductAttribute>> GetProductAttributes(List<long> productIds)
        {
            var productAttributesQuery = Db.From<ProductAttribute>().WithReadUnCommited()
                .Where(x => productIds.Contains(x.ProductId));
            return await Db.LoadSelectAsync(productAttributesQuery);
        }

        private async Task<List<PartnerDelivery>> GetPartnerDeliveries(List<long> partnerDeliveryIds)
        {
            var partnerDeliveriesQuery = Db.From<PartnerDelivery>().WithReadUnCommited()
                .Where(x => partnerDeliveryIds.Contains(x.Id));
            return await Db.LoadSelectAsync(partnerDeliveriesQuery);
        }
    }
}

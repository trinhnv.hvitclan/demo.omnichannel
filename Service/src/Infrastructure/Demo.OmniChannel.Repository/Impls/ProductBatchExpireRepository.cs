﻿using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.Utilities;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Impls
{
    public class ProductBatchExpireRepository : BaseRepository<ProductBatchExpire>, IProductBatchExpireRepository
    {
        public ProductBatchExpireRepository(IDbConnection dbConnection) : base(dbConnection)
        {

        }

        public async Task<List<ProductBatchExpire>> GetListByProductIds(int retailerId, List<long> productIds)
        {

            int totalCount = productIds.Count;
            var totalPages = (int)Math.Ceiling(totalCount / (decimal)200);
            var productBatchExpireLst = new List<ProductBatchExpire>();
            for (int page = 1; page <= totalPages; page++)
            {
                var productPaging = productIds.Page(page, 200).ToList();
                var ProductBatchExpireResult = await Db.LoadSelectAsync<ProductBatchExpire>(
                    c => c.RetailerId == retailerId && productPaging.Contains(c.ProductId));
                productBatchExpireLst.AddRange(ProductBatchExpireResult);
            }
            return productBatchExpireLst;
        }
    }
}
﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Interfaces;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.Legacy;

namespace Demo.OmniChannel.Repository.Impls
{
    public class SaleChannelRepository :BaseRepository<SaleChannel>, ISaleChannelRepository
    {
        public SaleChannelRepository(IDbConnection dbConnection) : base(dbConnection)
        {

        }
        public async Task<SaleChannel> GetByOmmiChannelId(long retailerId, long ommiChannelId)
        {
            using (Db.OpenTransaction(IsolationLevel.ReadUncommitted))
            {
                return await Db.SingleAsync<SaleChannel>(x => x.RetailerId == retailerId && x.OmniChannelId == ommiChannelId);
            }
        }

        public async Task<List<SaleChannel>> GetSaleChannelListByOmmiChannelIds(int retailerId, List<long?> omniChannelIds)
        {
            var query = Db.From<SaleChannel>().Where(x => x.RetailerId == retailerId && omniChannelIds.Contains(x.OmniChannelId));
            var saleChannelList = await Db.SelectAsync(query);
            return saleChannelList;
        }
        public async Task<SaleChannel> GetSaleChannelFB(long retailerId)
        {
            var query = Db.From<SaleChannel>().WithNoLock().Where(x => x.RetailerId == retailerId && x.Name == "Facebook");
            return await Db.SingleAsync(query);
        }
    }
}
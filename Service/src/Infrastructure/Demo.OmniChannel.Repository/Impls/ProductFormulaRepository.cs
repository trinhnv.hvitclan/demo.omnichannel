﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Interfaces;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Repository.Impls
{
    public class ProductFormulaRepository :BaseRepository<ProductFormula>, IProductFormulaRepository
    {
        public ProductFormulaRepository(IDbConnection dbConnection) : base(dbConnection)
        {

        }

        public async Task<List<ProductFormula>> GetByProductIds(int retailerId, List<long> productIds)
        {
            var query = Db.From<ProductFormula>().WithReadUnCommited()
             .CustomJoin($"INNER JOIN Product with (nolock) ON (Product.Id = ProductFormula.MaterialId and " +
             $"Product.RetailerId = {retailerId})")
             .Where(x => x.RetailerId == retailerId && productIds.Contains(x.ProductId))
             .Select<ProductFormula, Product>((pf, p) => new
             {
                 pf.ProductId,
                 pf.MaterialId,
                 pf.RetailerId,
                 pf.Quantity,
                 IsWarranty = p.IsWarranty,
                 MaterialCode = p.Code,
             });
            var productFormula = await Db.SelectAsync(query);
            return productFormula;
        }
    }
}
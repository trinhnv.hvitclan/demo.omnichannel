﻿using Demo.OmniChannel.Repository.Interfaces;
using System.Data;

namespace Demo.OmniChannel.Repository.Impls
{
    public class OmniChannelWareHouseRepository : BaseDeleteRepository<Domain.Model.OmniChannelWareHouse>, IOmniChannelWareHouseRepository 
    {
        public OmniChannelWareHouseRepository(IDbConnection dbConnection) : base(dbConnection) { }
    }
}

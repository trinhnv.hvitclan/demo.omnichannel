﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Interfaces;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Repository.Impls
{
    public class PosSettingRepository : BaseRepository<PosParameter>,IPosSettingRepository
    {
        public PosSettingRepository(IDbConnection dbConnection) : base(dbConnection)
        {
        }
        public async Task<Dictionary<string, string>> GetSettingAsync(int retailerId)
        {
            using (Db.OpenTransaction(IsolationLevel.ReadUncommitted))
            {
                var poss = await WhereAsync(x => x.RetailerId == retailerId && x.isActive);
                if (poss == null || poss.Count <= 0) return new Dictionary<string, string>();
                var possDisList = poss.GroupBy(x => x.Key).Select(x => x.First()).ToList();
                return possDisList.ToDictionary(x => x.Key, u => u.Value);
            }
        }

        public async Task<PosParameter> GetSettingOmniChannel(int retailerId, string Key)
        {
            using (Db.OpenTransaction(IsolationLevel.ReadUncommitted))
            {
                var poss = await SingleAsync(x => x.RetailerId == retailerId && x.isActive && x.Key.Equals(Key));
                return poss;
            }
        }
    }
}
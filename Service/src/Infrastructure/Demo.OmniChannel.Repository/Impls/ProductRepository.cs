﻿using Demo.OmniChannel.ShareKernel.Dto;
using Demo.OmniChannel.Domain.Common;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.Utilities;
using ServiceStack;
using ServiceStack.DataAnnotations;
using ServiceStack.Logging;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ServiceStack.Data;

namespace Demo.OmniChannel.Repository.Impls
{
    public class ProductRepository : BaseRepository<Product>, IProductRepository
    {
        private readonly ILog _logger = LogManager.GetLogger(typeof(ProductRepository));

        private const string QueryFieldProduct = @"p.Id,
				p.Code,
				p.Barcode,
				p.Name,
				p.FullName,        
                pb.Cost AS Cost,
				ISNULL(pb.LatestPurchasePrice, 0) as LatestPurchasePrice,
				p.BasePrice,
				ISNULL(pb.OnHand, 0) AS OnHand,
				ISNULL(pb.OnOrder, 0) AS OnOrder,
				ISNULL(pb.Reserved, 0) AS Reserved,
				p.Unit,
				p.MasterUnitId,
				p.ProductType,
				ISNULL(p.IsLotSerialControl, 0) AS IsLotSerialControl,
				ISNULL(p.IsBatchExpireControl, 0) AS IsBatchExpireControl,
				ISNULL(p.IsWarranty, 0) AS IsWarranty,
				p.ConversionValue,
				ISNULL(p.IsRewardPoint, 0) AS IsRewardPoint,
				ISNULL(p.AllowsSale, 0) AS AllowsSale,
				p.IsMedicineProduct,
                pi.[Image],
				ISNULL(pb.ActualReserved, 0) AS ActualReserved ";
        public ProductRepository(IDbConnection dbConnection) : base(dbConnection)
        {
        }

        public async Task<List<ProductBranchDTO>> GetProductByIds(int retailerId, int branchId, List<long> productIds, bool isLogMonitor = false)
        {
            var query = Db.From<Product>().WithReadUnCommited()
                .CustomJoin($"LEFT JOIN ProductBranch with (nolock) ON (Product.Id = ProductBranch.ProductId and " +
                $"ProductBranch.RetailerId = {retailerId} and " +
                $"ProductBranch.BranchId = {branchId})")
                .Where(x => x.RetailerId == retailerId && productIds.Contains(x.Id) && (x.isDeleted == null || x.isDeleted == false))
                .Select<Product, ProductBranch>((p, pb) => new
                {
                    p,
                    IsActiveBranch = (pb != null && pb.IsActive.HasValue) ? pb.IsActive : p.IsActive,
                    BranchId = branchId
                });
            return await Db.SelectAsync<ProductBranchDTO>(query);
        }

        public async Task<List<ProductBranchDTO>> GetProductByIdsIncludeDeleted(int retailerId, int branchId, List<long> productIds, bool isIncludeDeleted, bool isLogMonitor = false)
        {
            var query = Db.From<Product>().WithReadUnCommited()
                .LeftJoin<ProductBranch>((p, pb) => p.Id == pb.ProductId && pb.RetailerId == retailerId && pb.BranchId == branchId && pb.IsActive != null && pb.IsActive == true)
                .Where(x => x.RetailerId == retailerId && productIds.Contains(x.Id));
            if (!isIncludeDeleted)
            {
                query = query.Where(x => (x.isDeleted == null || x.isDeleted == false));
            }
            query = query.Select<Product, ProductBranch>((p, pb) => new
            {
                p,
                IsActiveBranch = (pb != null && pb.IsActive.HasValue) ? pb.IsActive : p.IsActive,
                BranchId = branchId
            });

            return await Db.SelectAsync<ProductBranchDTO>(query);
        }


        public async Task<List<Product>> GetProductByCode(int retailerId, int branchId, HashSet<string> productCodes, bool includeInActive = false)
        {
            if (productCodes == null || productCodes.Count <= 0) return new List<Product>();
            var codes = productCodes.Join(",");
            string query = "DECLARE @temp TABLE ( Code NVARCHAR(500) ); " +
                                 "INSERT @temp (Code) SELECT REPLACE(REPLACE(REPLACE(splitdata,'  ', ''), CHAR(13), ''), CHAR(10), '') FROM dbo.fnSplitStringMultiDelimiter(@productCodes, ',') WHERE LEN(splitdata) > 0 " +
                                 "SELECT p.Id, p.Code, p.MasterCode, p.FullName, p.Name, p.isActive, p.RetailerId, p.ModifiedDate, p.IsRelateToOmniChannel " +
                                 "FROM dbo.Product AS p WITH (NOLOCK) INNER JOIN @temp AS t ON t.Code = p.Code LEFT JOIN dbo.ProductBranch AS pb WITH (NOLOCK) ON p.RetailerId = pb.RetailerId AND pb.BranchId = @branchId AND pb.ProductId = p.Id AND (pb.isActive IS NULL OR pb.isActive = 1) " +
                                 "WHERE p.RetailerId = @retailerId AND (isDeleted = 0 OR isDeleted IS NULL) ";
            if (!includeInActive) query += "And p.isActive = 1";
            var result = await Db.SqlListAsync<Product>(query, new { retailerId, productCodes = codes, branchId });
            _logger.Info($"GetProductByCode retailerId: {retailerId} branchId: {branchId} productCodes: {codes} count: {result.Count}");
            return result;
        }

        /// <summary>
        /// Update field IsRelateToOmniChannel in Product table, remember the size of listIds must lte 5000, cause lock table risk
        /// </summary>
        /// <param name="productRelations"></param>
        /// <param name="value"></param>
        /// <param name="isRevert"></param>
        /// <returns></returns>
        public async Task<List<long>> UpdateKvProductRelation(List<ProductRelation> productRelations, bool value, bool isRevert = false)
        {
            if (productRelations == null || productRelations.Count <= 0) return new List<long>();
            var productIds = productRelations.Select(x => x.KvProductId).ToList();
            if (isRevert)
            {
                var productRelationsJson = productRelations.ToJson();
                var sqlParams = new List<SqlParameter>
                {
                    new SqlParameter("json", SqlDbType.NVarChar, -1)
                    {
                        Value = productRelationsJson
                    }
                };
                await Db.ScalarAsync<int>(@"
                ;WITH cte AS (
                SELECT p.IsRelateToOmniChannel, i.OldValueIsRelate, p.ModifiedDate                
                FROM OPENJSON(@json) WITH (KvProductId BIGINT '$.KvProductId', OldValueIsRelate BIT '$.OldValueIsRelate') AS i
                	INNER JOIN dbo.Product AS p WITH(UPDLOCK) ON p.Id = i.KvProductId
                WHERE p.IsRelateToOmniChannel IS NULL OR (p.IsRelateToOmniChannel IS NOT NULL AND p.IsRelateToOmniChannel <> i.OldValueIsRelate)
                )
				UPDATE cte
                SET cte.IsRelateToOmniChannel = cte.OldValueIsRelate, cte.ModifiedDate = GETDATE()
                ", sqlParams);
            }
            else
            {
                var listIds = productIds.Join(",");
                var sqlParams = new List<SqlParameter>
                {
                    new SqlParameter("listProductId", SqlDbType.NVarChar, -1)
                    {
                        Value = listIds
                    },
                    new SqlParameter("val", SqlDbType.Bit, -1)
                    {
                        Value = value
                    }
                };
                await Db.ScalarAsync<int>(@"
                ;WITH cte AS (
				SELECT p.IsRelateToOmniChannel, p.ModifiedDate
				FROM [dbo].[SplitBigInts](@listProductId, ',') AS t
                	INNER JOIN dbo.Product AS p WITH(UPDLOCK) ON p.Id = t.Item
				WHERE p.IsRelateToOmniChannel IS NULL OR (p.IsRelateToOmniChannel IS NOT NULL AND p.IsRelateToOmniChannel <> @val)
				)
				UPDATE cte
                SET cte.IsRelateToOmniChannel = @val, cte.ModifiedDate = GETDATE()
                ", sqlParams);
            }
            return productIds;
        }

        public async Task<bool> CheckProductBranchIsActive(int retailerId, int branchId, long productId)
        {
            var query = Db.From<ProductBranch>().WithReadUnCommited()
                .Where(x => x.RetailerId == retailerId && x.ProductId == productId && x.BranchId == branchId);
            var productBranch = await Db.SingleAsync(query);
            if (productBranch == null) return true;
            return productBranch.IsActive ?? true;
        }

        public async Task<List<Product>> GetProductChildUnitByIds(int retailerId, int branchId, List<long> productIds, bool isLogMonitor = false)
        {
            var query = Db.From<Product>().WithReadUnCommited()
                .Where(x => x.RetailerId == retailerId && (productIds.Contains(x.MasterProductId ?? 0) || productIds.Contains(x.MasterUnitId ?? 0))
                && x.IsActive && (x.isDeleted == null || x.isDeleted == false))
                .WithSqlFilter(sql => sql + $" AND (NOT EXISTS (SELECT * FROM ProductBranch " +
                $"WHERE ProductBranch.RetailerId = {retailerId} " +
                $"AND ProductBranch.BranchId = {branchId} " +
                $"AND Product.Id = ProductBranch.ProductId " +
                $"AND ProductBranch.IsActive IS NOT NULL AND ProductBranch.IsActive = 0))");

            List<Product> result;
            if (isLogMonitor)
            {
                var watch = new Stopwatch();
                watch.Start();
                result = await Db.LoadSelectAsync(query);
                watch.Stop();
                var logObj = new LoggigMonitorSql()
                {
                    Action = "GetProductByIds",
                    Count = productIds.Count,
                    Duration = watch.ElapsedMilliseconds,
                };
                _logger.Info(logObj.ToSafeJson());
            }
            else
            {
                result = await Db.LoadSelectAsync(query);
            }

            return result;
        }

        public async Task<List<ProductBranch>> GetProductBranchByRange(int retailerId, int branchId, int minute)
        {
            var toDate = DateTime.Now;
            var fromDate = DateTime.Now.AddMinutes(-minute);
            var query = Db.From<ProductBranch>().WithNoLock().Where(x => x.RetailerId == retailerId && x.BranchId == branchId &&
                                                            ((x.ModifiedDate != null && x.ModifiedDate >= fromDate &&
                                                             x.ModifiedDate <= toDate) ||
                                                            (x.ModifiedDate == null && x.CreatedDate >= fromDate && x.CreatedDate <= toDate)));
            return await Db.SelectAsync(query);
        }

        public async Task<List<ProductDto>> GetProductByRange(int retailerId, int minute)
        {
            var toDate = DateTime.Now;
            var fromDate = DateTime.Now.AddMinutes(-minute);
            var query = Db.From<Product>().WithNoLock().Where(x => x.RetailerId == retailerId &&
                                                                   ((x.ModifiedDate != null &&
                                                                     x.ModifiedDate >= fromDate &&
                                                                     x.ModifiedDate <= toDate) ||
                                                                    (x.ModifiedDate == null && x.CreatedDate >= fromDate &&
                                                                     x.CreatedDate <= toDate)));
            return await Db.SelectAsync<ProductDto>(query);
        }

        public async Task<List<ProductDto>> GetByIds(int retailerId, HashSet<long> productIds)
        {
            var productQuery = Db.From<Product>().WithReadUnCommited().Where(x =>
                x.RetailerId == retailerId && x.IsActive &&
                (x.isDeleted == null || x.isDeleted == false) && productIds.Contains(x.Id));
            return await Db.SelectAsync<ProductDto>(productQuery);
        }

        public async Task<List<Product>> GetListByIds(int retailerId, List<long> productIds)
        {
            var query = Db.From<Product>().WithReadUnCommited()
                .Where(x => x.RetailerId == retailerId && x.IsActive &&
                            (x.isDeleted == null || x.isDeleted == false) && productIds.Contains(x.Id));
            var result = await Db.SqlListAsync<Product>(query);
            return result;
        }

        public async Task<List<ProductBatchExpireDto>> GetProductBatchExpireByBranch(int retailerId, int branchId, List<long> productIds)
        {
            productIds = await GetMasterProductIds(retailerId, productIds);

            var query = Db.From<ProductBatchExpire>()
                .Join<ProductBatchExpireBranch>((p, pb) => p.Id == pb.ProductBatchExpireId)
                .Where<ProductBatchExpireBranch>(pb => pb.RetailerId == retailerId && pb.BranchId == branchId && pb.OnHand > 0)
                .Where<ProductBatchExpire>(p => p.RetailerId == retailerId && productIds.Contains(p.ProductId))
                .Select<ProductBatchExpire, ProductBatchExpireBranch>((p, pb) => new
                {
                    p.Id,
                    p.ProductId,
                    p.ExpireDate,
                    pb.OnHand,
                    pb.Status,
                    p.RetailerId,
                    pb.BranchId,
                    p.BatchName
                })
                .OrderBy(pb => pb.ExpireDate)
                .ThenBy(pb => pb.CreatedDate);

            var productBatchExpireDtos = await Db.SelectAsync<ProductBatchExpireDto>(query);
            productBatchExpireDtos = productBatchExpireDtos.Where(item => Math.Round(item.OnHand, 3) > 0).ToList();
            return productBatchExpireDtos;
        }

        private async Task<List<long>> GetMasterProductIds(int retailerId, List<long> productIds)
        {
            var productQuery = Db.From<Product>()
                .Where(p => p.RetailerId == retailerId)
                .Where(p => p.IsActive)
                .Where(p => p.isDeleted == null || p.isDeleted == false)
                .Where(p => productIds.Contains(p.Id));
            var products = await Db.SelectAsync(productQuery);
            foreach (var productItem in products)
            {
                var isBatchExpireControl = productItem.IsBatchExpireControl ?? false;
                if (!isBatchExpireControl || productItem.IsHasVariantsAndSingleUnit())
                {
                    continue;
                }
                var masterId = productItem.MasterUnitId ?? 0;
                if (masterId <= 0 || productIds.Contains(masterId))
                {
                    continue;
                }
                productIds.Remove(productItem.Id);
                productIds.Add(masterId);
            }

            return productIds;
        }

        public async Task<List<ProductBranch>> GetProductByRetailerId(int retailerId, int branchId, int range)
        {
            var productBranchQuery = Db.From<ProductBranch>().WithReadUnCommited()
                .Where(x => x.RetailerId == retailerId && x.BranchId == branchId && x.IsActive == true);

            return await Db.SelectAsync(productBranchQuery);
        }

        public async Task<List<ProductByBranch>> QueryGetProducts(int retailerId, int branchId, string keyword,
            int limit = 5, int[] productTypes = null)
        {
            if (productTypes == null)
            {
                productTypes = new[] { 2 };
            }
            var productTypeStr = string.Join(",", productTypes);
            var result = await Db.SqlListAsync<ProductByBranch>("pr_search_getProduct", cmd =>
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.AddParam("retailerId", retailerId);
                cmd.AddParam("branchId", branchId);
                cmd.AddParam("keyword", keyword);
                cmd.AddParam("listProductType", productTypeStr);
                cmd.AddParam("top", limit);
            });

            foreach (var item in result)
            {
                item.Cost = NumberHelper.RoundProductPrice(item.Cost) ?? 0;
                item.LatestPurchasePrice = NumberHelper.RoundProductPrice(item.LatestPurchasePrice) ?? 0;
                item.ProductAttributes = item.ProductAttribute.FromJson<List<ProductAttribute>>() ??
                                         new List<ProductAttribute>();
                item.ProductSerials = item.ProductSerial.FromJson<List<ProductSerial>>() ?? new List<ProductSerial>();
                item.ListProductUnit = item.ProductUnit.FromJson<List<ProductUnit>>() ?? new List<ProductUnit>();
                item.ProductBatchExpires = item.ProductBatchExpires ?? new List<ProductBatchExpireDto>();
                item.ProductShelves = item.ProductShelves ?? new List<ProductShelves>();
            }
            return result;
        }

        public async Task<List<ProductByBranch>> QueryGetProductsV2(int retailerId, int branchId, string keyword,
            int limit = 500, List<string> ignoreCodes = null, bool optimizeByIds = true)
        {
            var keyTemp = $"%{keyword.Trim()}%";
            var productCodes = "";
            var productIdStr = "";
            var tempCode = "";
            if (ignoreCodes != null && ignoreCodes.Any())
            {
                productCodes = ignoreCodes.Join(",");
                tempCode = @"DECLARE @tempCode TABLE(Code NVARCHAR(500) );
                             INSERT @tempCode (Code) SELECT splitdata FROM dbo.fnSplitStringMultiDelimiter(@productCodes, ',') WHERE LEN(splitdata) > 0; ";
            }
            var conditionCode = string.IsNullOrEmpty(tempCode) ? "" : "AND NOT EXISTS(SELECT Code FROM @tempCode WHERE Code = p.Code)";

            var queryField = QueryFieldProduct;
            var queryCondition = QueryConditionProductV2();

            string query;
            if (optimizeByIds)
            {
                var queryIds = $"{tempCode} SELECT TOP {limit} p.Id {queryCondition} {conditionCode}";
                var productIds = await Db.SqlListAsync<long>(queryIds, new { retailerId, branchId, keyTemp, productCodes });
                if (!productIds.Any()) return new List<ProductByBranch>();

                productIdStr = productIds.Join(",");
                var tempIds = @"DECLARE @tempIds TABLE( Id BIGINT );
                             INSERT @tempIds (Id) SELECT splitdata FROM dbo.fnSplitStringMultiDelimiter(@productIds, ',') WHERE LEN(splitdata) > 0; ";

                var conditionIds = "AND EXISTS(SELECT Id FROM @tempIds WHERE Id = p.Id)";
                query = $"{tempIds} SELECT {queryField} {queryCondition} {conditionIds}";
            }
            else
            {
                query = $"{tempCode} SELECT TOP {limit} {queryField} {queryCondition} {conditionCode}";
            }

            var result = await Db.SqlListAsync<ProductByBranch>(query, new { retailerId, branchId, keyTemp, productCodes, productIds = productIdStr });

            foreach (var item in result)
            {
                item.Cost = NumberHelper.RoundProductPrice(item.Cost) ?? 0;
                item.LatestPurchasePrice = NumberHelper.RoundProductPrice(item.LatestPurchasePrice) ?? 0;
                item.ProductAttributes = new List<ProductAttribute>();
                item.ProductSerials = new List<ProductSerial>();
                item.ListProductUnit = new List<ProductUnit>();
                item.ProductBatchExpires = new List<ProductBatchExpireDto>();
                item.ProductShelves = new List<ProductShelves>();
            }
            return result;
        }

        public async Task<PagingDataSource<ProductForChannelMapping>> GetProductForMapping(int retailerId, int branchId, List<long> productIds, int skip, int take)
        {
            var isGetAll = !productIds.Any();
            var productQuery = Db.From<Product>().WithReadUnCommited()
                .LeftJoin<ProductBranch>((p, pb) => p.Id == pb.ProductId && pb.RetailerId == retailerId && pb.BranchId == branchId)
                .Where<Product, ProductBranch>((p, pb) =>
                    p.RetailerId == retailerId &&
                     (isGetAll || Sql.In(p.Id, productIds)))
                .Select<Product, ProductBranch>((p, pb) => new
                {
                    Id = p.Id,
                    Code = p.Code,
                    Name = p.Name,
                    FullName = p.FullName,
                    Unit = p.Unit,
                    BasePrice = p.BasePrice,
                    IsActive = pb.IsActive ?? true,
                    CreatedDate = p.CreatedDate,
                    Description = p.Description,
                    OnHand = pb == null ? 0 : pb.OnHand
                });

            var totalItem = await Db.CountAsync(productQuery);
            var productsRes =
                await Db.SelectAsync<ProductForChannelMapping>(productQuery.OrderByDescending(x => x.CreatedDate)
                    .Skip(skip).Take(take));

            var productQueryIds = productsRes.Select(x => x.Id);
            var attributeQuery = Db.From<ProductAttribute>().WithReadUnCommited()
                .Where(x => productQueryIds.Contains(x.ProductId));
            var attributes = await Db.SelectAsync(attributeQuery);
            var dicAttributes = attributes.GroupBy(x => x.ProductId)
                .ToDictionary(x => x.Key, y => y.Select(pa => pa.Value).Join(" - "));

            if (dicAttributes.Any())
            {
                foreach (var item in productsRes)
                {
                    item.AttributeValue = dicAttributes.ContainsKey(item.Id) ? dicAttributes[item.Id] : string.Empty;
                }
            }

            return new PagingDataSource<ProductForChannelMapping>()
            {
                Data = productsRes,
                Total = totalItem,
            };
        }

        public async Task<List<ProductForChannelMapping>> GetProductForMappingByBranchLst(int retailerId, List<int> branchIdLst, List<long> productIds)
        {
            var productQuery = Db.From<Product>().WithReadUnCommited()
                .LeftJoin<ProductBranch>((p, pb) => p.Id == pb.ProductId && pb.RetailerId == retailerId && Sql.In(pb.BranchId, branchIdLst))
                .Where<Product, ProductBranch>((p, pb) =>  p.RetailerId == retailerId
                    && Sql.In(p.Id, productIds))
                .Select<Product, ProductBranch>((p, pb) => new
                {
                    p.Id,
                    p.Code,
                    p.Name,
                    p.FullName,
                    p.Unit,
                    p.BasePrice,
                    p.IsActive,
                    OnHand = pb != null ? pb.OnHand : 0,
                    OnOrder = pb != null ? pb.OnOrder : 0,
                    Reserved = pb != null ? pb.Reserved : 0,
                    p.CreatedDate,
                    p.ProductType,
                    BranchId = pb != null ? pb.BranchId : 0,
                });

            var products = await Db.SelectAsync<ProductForChannelMapping>(productQuery);

            var attributeQuery = Db.From<ProductAttribute>().WithReadUnCommited()
                .Where(x => productIds.Contains(x.ProductId));
            var attributes = await Db.SelectAsync(attributeQuery);
            var dicAttributes = attributes.GroupBy(x => x.ProductId)
                .ToDictionary(x => x.Key, y => y.Select(pa => pa.Value).Join(" - "));

            if (dicAttributes.Any())
            {
                foreach (var item in products)
                {
                    item.AttributeValue = dicAttributes.ContainsKey(item.Id) ? dicAttributes[item.Id] : string.Empty;
                }
            }

            return products;
        }

        public async Task<List<ProductForChannelMapping>> GetProductForMapping(int retailerId, int branchId, List<long> productIds)
        {
            var productQuery = Db.From<Product>().WithNoLock()
                .LeftJoin<ProductBranch>((p, pb) => p.Id == pb.ProductId && pb.RetailerId == retailerId && pb.BranchId == branchId)
                .Where<Product, ProductBranch>((p, pb) =>
                    p.RetailerId == retailerId && Sql.In(p.Id, productIds))
                .Select<Product, ProductBranch>((p, pb) => new
                {
                    Id = p.Id,
                    Code = p.Code,
                    Name = p.Name,
                    FullName = p.FullName,
                    Unit = p.Unit,
                    BasePrice = p.BasePrice,
                    IsActive = p.IsActive,
                    OnHand = pb != null ? pb.OnHand : 0,
                    OnOrder = pb != null ? pb.OnOrder : 0,
                    Reserved = pb != null ? pb.Reserved : 0,
                    CreatedDate = p.CreatedDate,
                    ProductType = p.ProductType
                });

            var products = await Db.SelectAsync<ProductForChannelMapping>(productQuery);

            var attributeQuery = Db.From<ProductAttribute>().WithReadUnCommited()
                .Where(x => productIds.Contains(x.ProductId));
            var attributes = await Db.SelectAsync(attributeQuery);
            var dicAttributes = attributes.GroupBy(x => x.ProductId)
                .ToDictionary(x => x.Key, y => y.Select(pa => pa.Value).Join(" - "));

            if (dicAttributes.Any())
            {
                foreach (var item in products)
                {
                    item.AttributeValue = dicAttributes.ContainsKey(item.Id) ? dicAttributes[item.Id] : string.Empty;
                }
            }

            return products;
        }

        public async Task<List<ProductAttribute>> GetProductAttributesByProductIds(int retailerId, List<long> productIds)
        {
            var query = Db.From<ProductAttribute>().WithReadUnCommited()
                .Where(x => x.RetailerId == retailerId && Sql.In(x.ProductId, productIds));
            var result = await Db.SelectAsync(query);
            return result;
        }

        private string QueryConditionProductV2()
        {
            return $"FROM Product AS p WITH ( NOLOCK ) " +
                $"LEFT JOIN dbo.ProductBranch AS pb WITH ( NOLOCK ) " +
                $"ON pb.ProductId = p.Id AND pb.BranchId = @branchId AND pb.RetailerId = @retailerId " +
                $"OUTER APPLY ( SELECT TOP (1)  E.[Image] FROM dbo.ProductImage E WITH ( NOLOCK ) WHERE E.ProductId = p.Id ORDER BY E.CreatedDate, E.Id ) pi " +
                $"WHERE p.RetailerId = @retailerId AND(p.isDeleted = 0 OR p.isDeleted IS NULL) " +
                $"AND p.isActive = 1 AND ISNULL(pb.isActive, p.isActive) = 1 " +
                $"AND (p.Code LIKE @keyTemp  OR p.FullName LIKE @keyTemp OR p.Name LIKE @keyTemp)";
        }
        public async Task<List<Product>> GetProductByMasterIds(int retailerId, List<long> kvProductIds)
        {
            var query = Db.From<Product>().WithNoLock().Where(x => x.RetailerId == retailerId && x.IsActive &&
                            (x.isDeleted == null || x.isDeleted == false) && kvProductIds.Contains(x.MasterProductId ?? 0));
            var result = await Db.SqlListAsync<Product>(query);
            return result;
        }

    }

    public class ProductRelation
    {
        public long KvProductId { get; set; }
        public bool OldValueIsRelate { get; set; }
    }

    public class Test
    {
        [StringLength(50)]
        public string Code { get; set; }
    }


    public class LoggigMonitorSql
    {
        public string Action { get; set; }
        public long Duration { get; set; }
        public int Count { get; set; }
    }
}
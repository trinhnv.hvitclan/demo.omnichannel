﻿using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Interfaces;
using ServiceStack.OrmLite;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Impls
{
    public class CategoryRepository : BaseRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(IDbConnection dbConnection) : base(dbConnection)
        {
        }

        public async Task<Category> GetProductByNameAsync(int retailerId, string name, bool isParent = true)
        {
            var query = Db.From<Category>().WithReadUnCommited()
                .Where(item => item.RetailerId == retailerId
                && item.Name == name
                && (item.isDeleted == null || item.isDeleted == false));
            if (isParent)
            {
                query.Where(item => item.ParentId == null);
            }
            else
            {
                query.Where(item => item.ParentId != null);
            }
            return (await Db.LoadSelectAsync(query)).FirstOrDefault();
        }
    }
}
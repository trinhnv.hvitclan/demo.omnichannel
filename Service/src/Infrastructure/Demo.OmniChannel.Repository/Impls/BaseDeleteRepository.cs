﻿using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.ShareKernel.Abstractions;

namespace Demo.OmniChannel.Repository.Impls
{
    public abstract class BaseDeleteRepository<T> : BaseRepository<T>, IBaseDeleteRepository<T> where T : class, IEntityId, IDeleted, new()
    {
        protected BaseDeleteRepository(IDbConnection dbConnection) : base(dbConnection)
        {
        }

        #region Async Methods

        public async Task<IEnumerable<T>> GetAllAsync(int? offset = 0, int? limit = int.MaxValue, Expression<Func<T, bool>> filter = null, bool isIncludeDeleted = false)
        {
            filter = SetIsDeleted(filter, isIncludeDeleted);

            return await base.GetAllAsync(offset, limit, filter);
        }

        public async Task<List<TResult>> GetAsync<TResult>(Expression<Func<T, bool>> filter, int? skip = null, int? take = null, string[] orderBy = null, string[] orderByDesc = null, bool isIncludeDeleted = false)
        {
            filter = SetIsDeleted(filter, isIncludeDeleted);

            return await base.GetAsync<TResult>(filter, skip, take, orderBy, orderByDesc);
        }

        public async Task<List<TResult>> GetAsync<TResult>(SqlExpression<T> query, int? skip = null, int? take = null, string[] orderBy = null, string[] orderByDesc = null, bool isIncludeDeleted = false)
        {
            query = SetIsDeleted(query, isIncludeDeleted);

            return await base.GetAsync<TResult>(query, skip, take, orderBy, orderByDesc);
        }

        public async Task<List<T>> GetHasReferenceAsync(Expression<Func<T, bool>> filter, bool isIncludeDeleted = false)
        {
            filter = SetIsDeleted(filter, isIncludeDeleted);

            return await base.GetHasReferenceAsync(filter);
        }

        public async Task<List<T>> WhereAsync(Expression<Func<T, bool>> filter, bool isIncludeDeleted = false)
        {
            filter = SetIsDeleted(filter, isIncludeDeleted);

            return await base.WhereAsync(filter);
        }

        public async Task<List<T>> WhereAsync(SqlExpression<T> filter, bool isIncludeDeleted = false)
        {
            filter = SetIsDeleted(filter, isIncludeDeleted);

            return await base.WhereAsync(filter);
        }

        public async Task<T> SingleAsync(Expression<Func<T, bool>> filter, bool isIncludeDeleted = false)
        {
            filter = SetIsDeleted(filter, isIncludeDeleted);

            return await base.SingleAsync(filter);
        }

        public async Task<T> SingleAsync(SqlExpression<T> filter, bool isIncludeDeleted = false)
        {
            filter = SetIsDeleted(filter, isIncludeDeleted);

            return await base.SingleAsync(filter);
        }

        public async Task<bool> ExistsAsync(Expression<Func<T, bool>> filter, bool isIncludeDeleted = false)
        {
            filter = SetIsDeleted(filter, isIncludeDeleted);

            return await base.ExistsAsync(filter);
        }

        public async Task<T> GetByIdAsync(object id, bool isLoadReference = false, bool isIncludeDeleted = false)
        {
            Expression<Func<T, bool>> filter = x => x.Id == long.Parse(id.ToString());

            filter = SetIsDeleted(filter, isIncludeDeleted);

            return isLoadReference ? (await GetHasReferenceAsync(filter)).FirstOrDefault() : (await GetAsync<T>(filter)).FirstOrDefault();
        }

        public async Task<long> CountAsync(Expression<Func<T, bool>> filter, bool isIncludeDeleted = false)
        {
            filter = SetIsDeleted(filter, isIncludeDeleted);

            return await base.CountAsync(filter);
        }

        public async Task<long> CountAsync(SqlExpression<T> filter, bool isIncludeDeleted = false)
        {
            filter = SetIsDeleted(filter, isIncludeDeleted);

            return await base.CountAsync(filter);
        }

        #endregion

        private Expression<Func<T, bool>> SetIsDeleted(Expression<Func<T, bool>> filter, bool isIncludeDeleted = false)
        {
            if (!isIncludeDeleted) filter = filter.And(x => x.IsDeleted == null || x.IsDeleted == false);

            return filter;
        }

        private SqlExpression<T> SetIsDeleted(SqlExpression<T> filter, bool isIncludeDeleted = false)
        {
            if (!isIncludeDeleted) filter = filter.And(x => x.IsDeleted == null || x.IsDeleted == false);

            return filter;
        }
    }
}
﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Interfaces;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Repository.Impls
{
    public class ProductBatchExpireBranchRepository : BaseRepository<ProductBatchExpireBranch>, IProductBatchExpireBranchRepository
    {
        public ProductBatchExpireBranchRepository(IDbConnection dbConnection) : base(dbConnection)
        {

        }

        public async Task<List<ProductBatchExpireBranch>> GetListByBatchExpireIdsAndOnhand(int retailerId, int branchId,
            List<long> batchExpireIds)
        {
            var query = Db.From<ProductBatchExpireBranch>().WithReadUnCommited()
                .Where(x => x.RetailerId == retailerId && x.BranchId == branchId &&
                            batchExpireIds.Contains(x.ProductBatchExpireId) && x.OnHand > 0);
            var result = await Db.LoadSelectAsync(query);
            return result;
        }
    }
}
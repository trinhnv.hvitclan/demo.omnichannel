﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.SqlServer;
using Demo.OmniChannel.ShareKernel.Dto;

namespace Demo.OmniChannel.Repository.Impls
{
    public class OmniChannelRepository : BaseDeleteRepository<Domain.Model.OmniChannel>, IOmniChannelRepository
    {
        //public KvChannelRepository() : base()
        //{

        //}
        public OmniChannelRepository(IDbConnection dbConnection) : base(dbConnection)
        {

        }
        public async Task<long> CountChannelConnectedAsync(int retailerId, long shopId, long channelId, byte channelType, bool isIncludeDeleted = false)
        {
            var filter = BuildGenericQuery(null)
                .Join<OmniChannelAuth>()
                .Where<Domain.Model.OmniChannel>(channel => channel.RetailerId == retailerId &&
                                             channel.Id != channelId &&
                                             channel.Type == channelType)
                .Where<OmniChannelAuth>(auth => auth.ShopId == shopId);

            var count = await CountAsync(filter, isIncludeDeleted);

            return count;

            //var sql = @"SELECT c.Id FROM dbo.KvChannel c
            //    JOIN dbo.KvChannelAuth a
            //    ON c.Id = a.KvChannelId
            //    WHERE c.RetailerId = @retailerId
            //    AND c.Id != @id
            //    AND a.ShopId = @shopId
            //    AND c.Type = @channelType";
            //var result = await Db.RowCountAsync(sql, new { retailerId, shopId, id = channelId, channelType });
            //return result;
        }

        public async Task<List<Domain.Model.OmniChannel>> GetByRetailerAsync(int retailerId, byte? type, bool isIncludeDeleted = false, bool isIncludeInactive = true)
        {
            if (type.HasValue && type > 0)
            {
                return await GetHasReferenceAsync(x => x.RetailerId == retailerId && x.Type == type && (isIncludeInactive || x.IsActive), isIncludeDeleted);
            }
            return await GetHasReferenceAsync(x => x.RetailerId == retailerId && (isIncludeInactive || x.IsActive), isIncludeDeleted);
        }

        public async Task<List<Domain.Model.OmniChannel>> GetAllActiveChannels(List<int> includeRetailerIds, List<int> excludeRetailerIds)
        {
            var query = Db.From<Domain.Model.OmniChannel>().WithNoLock().Where(x => x.IsActive && (x.IsDeleted == null || x.IsDeleted == false));

            if (includeRetailerIds?.Any() == true) query = query.Where(t => includeRetailerIds.Contains(t.RetailerId));
            if (excludeRetailerIds?.Any() == true) query = query.Where(t => !excludeRetailerIds.Contains(t.RetailerId));

            return await Db.LoadSelectAsync(query);
        }

        public async Task<List<OmniChannelDto>> GetChannelHaveSyncOnHandAsync()
        {
            var query = Db.From<Domain.Model.OmniChannel>()
                .Join<OmniChannelSchedule>((channel, schedule) => channel.Id == schedule.OmniChannelId, SqlServerTableHint.ReadUncommitted)
                .Where<Domain.Model.OmniChannel>(x => x.IsActive && (x.IsDeleted == null || x.IsDeleted == false))
                .Where<OmniChannelSchedule>(x => x.Type == (byte)ScheduleType.SyncOnhand)
                .Select<Domain.Model.OmniChannel, OmniChannelSchedule>((channel, schedule) => new
                {
                    channel.Id,
                    channel.Name,
                    channel.RetailerId,
                    channel.BranchId,
                    channel.Type,
                    channel.SyncOnHandFormula
                });
            return await Db.SelectAsync<OmniChannelDto>(query);
        }

        public async Task<List<OmniChannelDto>> GetChannelHaveSyncPriceAsync()
        {
            var query = Db.From<Domain.Model.OmniChannel>()
                .Join<OmniChannelSchedule>((channel, schedule) => channel.Id == schedule.OmniChannelId, SqlServerTableHint.ReadUncommitted)
                .Where<Domain.Model.OmniChannel>(x => x.IsActive && (x.IsDeleted == null || x.IsDeleted == false))
                .Where<OmniChannelSchedule>(x => x.Type == (byte)ScheduleType.SyncPrice)
                .Select<Domain.Model.OmniChannel, OmniChannelSchedule>((channel, schedule) => new
                {
                    ChannelId = channel.Id,
                    ChannelName = channel.Name,
                    channel.RetailerId,
                    channel.BranchId,
                    channel.Type,
                    channel.SyncOnHandFormula,
                    channel.BasePriceBookId,
                    channel.PriceBookId
                });
            return await Db.SelectAsync<OmniChannelDto>(query);
        }

        /// <summary>
        /// Get danh sách channel cần refesh access token.
        /// </summary>
        /// <param name="channelType">Loại channel</param>
        /// <param name="refreshBeforeMinutes">Số phút trước khi token hết hạn</param>
        /// <returns></returns>
        public async Task<List<Domain.Model.OmniChannel>> GetChannelForRefreshToken(byte channelType, double refreshBeforeMinutes)
        {
            var expireDate = DateTime.Now.AddMinutes(refreshBeforeMinutes);
            var query = Db.From<Domain.Model.OmniChannel>()
                .Join<OmniChannelAuth>((channel, auth) => channel.Id == auth.OmniChannelId, SqlServerTableHint.ReadUncommitted)
                .Where<Domain.Model.OmniChannel>(x => x.Type == channelType && x.IsActive && (x.IsDeleted == null || x.IsDeleted == false))
                .Where<OmniChannelAuth>(x => x.ExpiryDate != null && x.ExpiryDate < expireDate);

            return await Db.LoadSelectAsync(query);
        }

        public async Task<List<Domain.Model.OmniChannel>> GetVisibleChannels(int retailerId)
        {
            var query = Db.From<Domain.Model.OmniChannel>().WithReadUnCommited().Where(x => x.RetailerId == retailerId && (x.IsDeleted == null || x.IsDeleted == false));
            return await Db.LoadSelectAsync(query);
        }

        public async Task<List<Domain.Model.OmniChannel>> GetChannelByIdentityKey(byte channelType, string identityKey)
        {
            var query = Db.From<Domain.Model.OmniChannel>().WithReadUnCommited().Where(x => x.IdentityKey == identityKey && x.IsActive == true && (x.IsDeleted == null || x.IsDeleted == false) && x.Type == channelType);

            return await Db.LoadSelectAsync(query);
        }

        public async Task<Domain.Model.OmniChannel> GetDefaultActiveChannel(byte channelType)
        {
            var query = Db.From<Domain.Model.OmniChannel>()
                .Join<OmniChannelAuth>((channel, auth) => channel.Id == auth.OmniChannelId, SqlServerTableHint.ReadUncommitted)
                .Where<Domain.Model.OmniChannel>(x => x.Type == channelType && x.IsActive && (x.IsDeleted == null || x.IsDeleted == false)).Limit(1)
                .Where<OmniChannelAuth>(x => x.ExpiryDate != null && x.ExpiryDate > DateTime.Now.AddHours(1));

            return (await Db.LoadSelectAsync(query)).FirstOrDefault();
        }

        public async Task<List<Domain.Model.OmniChannel>> GetByRetailerAsync(int retailerId, int branchId, byte? type, bool isIncludeDeleted = false, bool isIncludeInactive = true)
        {
            if (type.HasValue && type > 0)
            {
                return await GetHasReferenceAsync(x => x.RetailerId == retailerId && x.BranchId == branchId && x.Type == type && (isIncludeInactive || x.IsActive), isIncludeDeleted);
            }
            return await GetHasReferenceAsync(x => x.RetailerId == retailerId && x.BranchId == branchId && (isIncludeInactive || x.IsActive), isIncludeDeleted);
        }

        public async Task<List<Domain.Model.OmniChannel>> GetByRetailerAsync(int retailerId, int branchId, List<int> types, bool isIncludeDeleted = false, bool isIncludeInactive = true)
        {
            if (branchId > 0)
            {
                if (types != null && types.Count > 0)
                {
                    return await GetHasReferenceAsync(x => x.RetailerId == retailerId && x.BranchId == branchId && types.Contains(x.Type) && (isIncludeInactive || x.IsActive), isIncludeDeleted);
                }
                return await GetHasReferenceAsync(x => x.RetailerId == retailerId && x.BranchId == branchId && (isIncludeInactive || x.IsActive), isIncludeDeleted);
            }

            if (types != null && types.Count > 0)
            {
                return await GetHasReferenceAsync(x => x.RetailerId == retailerId && types.Contains(x.Type) && (isIncludeInactive || x.IsActive), isIncludeDeleted);
            }
            return await GetHasReferenceAsync(x => x.RetailerId == retailerId && (isIncludeInactive || x.IsActive), isIncludeDeleted);
        }

        public async Task<List<Domain.Model.OmniChannel>> GetChannelByShopIds(int retailerId, List<string> shopIds, byte channelType, bool isIncludeDeleted = false)
        {
            var query = Db.From<Domain.Model.OmniChannel>().WithReadUnCommited()
                .Where(channel => channel.RetailerId == retailerId)
                .Where(channel => shopIds.Contains(channel.IdentityKey))
                .Where(channel => channel.Type == channelType)
                .Where(channel => channel.IsDeleted == null || channel.IsDeleted == false);

            return await Db.LoadSelectAsync(query);
        }


        public async Task<List<Domain.Model.OmniChannel>> GetTotalByChannelType(byte channelType, List<int> includeRetailerIds = null,
            List<int> excludeRetailerIds = null)
        {
            var query = Db.From<Domain.Model.OmniChannel>()
                .Where<Domain.Model.OmniChannel>(t => t.Type == channelType && (t.IsDeleted == null || t.IsDeleted == false) && t.IsActive);
            if (includeRetailerIds?.Any() == true) query = query.Where(t => includeRetailerIds.Contains(t.RetailerId));
            if (excludeRetailerIds?.Any() == true) query = query.Where(t => !excludeRetailerIds.Contains(t.RetailerId));
            var result = await Db.SelectAsync(query);
            return result;
        }

        public async Task<List<Domain.Model.OmniChannel>> GetActiveChannelByRetailer(int retailerId)
        {
            var query = Db.From<Domain.Model.OmniChannel>().WithReadUnCommited().Where(x => x.IsActive && x.RetailerId == retailerId && (x.IsDeleted == null || x.IsDeleted == false));
            return await Db.LoadSelectAsync(query);
        }

        public async Task<Domain.Model.OmniChannel> GetById(int omniChannelId)
        {
            var query = Db.From<Domain.Model.OmniChannel>().WithReadUnCommited().Where(x => x.Id == omniChannelId);
            return (await Db.LoadSelectAsync(query)).FirstOrDefault();
        }

        public async Task<List<Domain.Model.OmniChannel>> GetDuplicateChannels(List<int> includeRetailerIds)
        {

            var sql = @"SELECT o.*
                        FROM dbo.OmniChannel o WITH ( NOLOCK )
                        INNER JOIN 
                        (SELECT RetailerId, IdentityKey FROM dbo.OmniChannel WITH ( NOLOCK ) GROUP BY RetailerId, IdentityKey HAVING COUNT(*) > 1) dup
                        ON o.RetailerId = dup.RetailerId AND o.IdentityKey = dup.IdentityKey
                        WHERE o.RetailerId IN " + "(" + String.Join(",", includeRetailerIds) + ")";

            return await Db.SqlListAsync<Domain.Model.OmniChannel>(sql);
        }

        public async Task<List<Domain.Model.OmniChannel>> GetByIdsAsync(List<long> channelIds)
        {
            var query = Db.From<Domain.Model.OmniChannel>().WithReadUnCommited().Where(x => channelIds.Contains(x.Id));
            return await Db.LoadSelectAsync(query);
        }

        public async Task<List<OmniChannelDto>> GetAllDeactiveChannels(List<int> includeRetailerIds, List<int> excludeRetailerIds, int skip = 0, int top = 1000)
        {
            var query = Db.From<Domain.Model.OmniChannel>().WithReadUnCommited().
                Where(x => !x.IsActive && (x.IsDeleted == null || x.IsDeleted == false))
                .Select<Domain.Model.OmniChannel>((channel) => new
                {
                    channel.Id,
                    channel.Name,
                    channel.Type,
                    channel.RetailerId
                })
                .Skip(skip)
                .Take(top);


            if (includeRetailerIds?.Any() == true) query = query.Where(t => includeRetailerIds.Contains(t.RetailerId));
            if (excludeRetailerIds?.Any() == true) query = query.Where(t => !excludeRetailerIds.Contains(t.RetailerId));

            return await Db.SelectAsync<OmniChannelDto>(query);
        }
        //public async Task<List<Domain.Model.OmniChannel>> GetAllChannelByTpe(byte channelType, int retailerId)
        //{
        //    var query = Db.From<Domain.Model.OmniChannel>()
        //                  .Select<Domain.Model.OmniChannel>((channel) => new
        //                  {
        //                      channel.Id,
        //                      channel.RetailerId
        //                  })
        //                    .GroupBy(x => new { x.RetailerId, x.Id })
        //                   .Where<Domain.Model.OmniChannel>(x => x.Type == channelType && x.IsActive && (x.IsDeleted == null || x.IsDeleted == false));
        //    if (retailerId > 0) query = query.Where(t => t.RetailerId == retailerId);
        //    return await Db.LoadSelectAsync(query);
        //}

        public async Task<List<Domain.Model.OmniChannel>> GetChannelWithNoLock(int retailerId)
        {
            var query = Db.From<Domain.Model.OmniChannel>().WithNoLock().Where(x => x.RetailerId == retailerId && (x.IsDeleted == null || x.IsDeleted == false));
            return await Db.LoadSelectAsync(query);
        }

        public async Task<List<Domain.Model.OmniChannel>> GetAllChannels(List<int> includeRetailerIds, List<int> excludeRetailerIds)
        {
            var sql = @"SELECT o.Id, o.RetailerId, o.Type, o.BranchId
                        FROM dbo.OmniChannel o WITH ( NOLOCK )
                        WHERE o.IsActive = 1  and (o.IsDeleted IS NULL OR o.IsDeleted = 0) ";
            if (includeRetailerIds?.Any() == true) sql = sql + " AND o.RetailerId IN " + "(" + String.Join(", ", includeRetailerIds) + ")";
            if (excludeRetailerIds?.Any() == true) sql = sql + " AND o.RetailerId NOT IN " + "(" + String.Join(", ", excludeRetailerIds) + ")";
            return await Db.SqlListAsync<Domain.Model.OmniChannel>(sql);
        }
        public async Task<List<OmniChannelDto>> GetChannelByChannelType(byte channelType, List<int> includeRetailerIds)
        {
            var query = Db.From<Domain.Model.OmniChannel>().WithReadUnCommited().
                Where(x => x.IsActive && (x.IsDeleted == null || x.IsDeleted == false) && x.Type == channelType)
                .Select<Domain.Model.OmniChannel>((channel) => new
                {
                    channel.Id,
                    channel.RetailerId
                });

            if (includeRetailerIds?.Any() == true) query = query.Where(t => includeRetailerIds.Contains(t.RetailerId));
            return await Db.SelectAsync<OmniChannelDto>(query);
        }
    }
}

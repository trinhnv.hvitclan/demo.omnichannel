﻿using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Interfaces;
using ServiceStack.OrmLite;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Impls
{
    public class PartnerDeliveryRepository : BaseRepository<PartnerDelivery>, IPartnerDeliveryRepository
    {
        public PartnerDeliveryRepository(IDbConnection dbConnection) : base(dbConnection)
        {

        }

        public async Task<PartnerDelivery> GetPartnerDeliveryByCodeAsync(int retailerId, string code)
        {
            var partnerQuery = Db.From<PartnerDelivery>().WithReadUnCommited().Where(c => c.RetailerId == retailerId && c.Code == code);
            return (await Db.LoadSelectAsync(partnerQuery)).FirstOrDefault();
        }
    }
}
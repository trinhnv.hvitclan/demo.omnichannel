﻿using Demo.OmniChannel.Domain.Common;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.ShareKernel.Abstractions;
using Demo.OmniChannel.ShareKernel.Exceptions;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Impls
{
    public abstract class BaseRepository<T> : IBaseRepository<T>, IDisposable where T : class, new()
    {
        #region Properties
        public IDbConnectionFactory DbFactory { get; set; }
        protected IDbConnection Db => _db ?? (_db = DbFactory.OpenDbConnection());

        private IDbConnection _db;
        public KvInternalContext Context { get; set; }
        #endregion

        #region Constructor
        protected BaseRepository(IDbConnection dbConnection = null)
        {
            _db = dbConnection;

            OrmLiteConfig.InsertFilter = (dbCmd, row) =>
            {
                //if (!row.GetType().HasInterface(typeof(ICreated))) return;
                if (row is ICreateDate auditRow)
                    auditRow.CreatedDate = DateTime.Now;
            };

            OrmLiteConfig.UpdateFilter = (dbCmd, row) =>
            {
                //if (!row.GetType().HasInterface(typeof(IModify))) return;
                if (row is IModifiedDate auditRow)
                    auditRow.ModifiedDate = DateTime.Now;
            };
        }

        protected BaseRepository(IDbConnectionFactory dbConnectionFactory)
        {
            DbFactory = dbConnectionFactory;
        }
        public void SetDbConnection(IDbConnection dbConnection)
        {
            _db = dbConnection;
        }
        #endregion
        #region Sync Methods
        public long Add(T obj)
        {
            SetAuditCreate(obj);
            return Db.Insert(obj);

        }
        public T Update(T obj)
        {
            SetModified(obj);
            Db.Update(obj);

            return obj;
        }

        public virtual bool Exists(Expression<Func<T, bool>> filter)
        {
            return Db.Exists(filter);
        }
        public virtual List<T> Where(Expression<Func<T, bool>> filter)
        {
            return Db.Select<T>(filter);
        }
        public virtual List<T> GetAll()
        {
            return Db.Select<T>();
        }
        #endregion

        #region Async Methods

        public virtual async Task<IEnumerable<T>> GetAllAsync(int? offset = 0, int? limit = int.MaxValue, Expression<Func<T, bool>> filter = null)
        {
            var query = BuildGenericQuery(filter).Limit(offset, limit);
            return await Db.SelectAsync(query);
        }

        public virtual async Task<List<TResult>> GetAsync<TResult>(Expression<Func<T, bool>> filter, int? skip = null, int? take = null, string[] orderBy = null, string[] orderByDesc = null)
        {
            var exp = BuildGenericQuery(filter);

            return await GetAsync<TResult>(exp, skip, take, orderBy, orderByDesc);
        }

        public virtual async Task<List<TResult>> GetAsync<TResult>(SqlExpression<T> query, int? skip = null, int? take = null, string[] orderBy = null, string[] orderByDesc = null)
        {
            if (skip != null) query = query.Skip(skip);

            if (take != null) query = query.Take(take);

            if (orderBy?.Any() == true) query = query.OrderByFields(orderBy);

            if (orderByDesc?.Any() == true) query = query.OrderByFieldsDescending(orderByDesc);

            return await Db.SelectAsync<TResult>(query);
        }

        public virtual async Task<List<T>> GetHasReferenceAsync(Expression<Func<T, bool>> filter)
        {
            return await Db.LoadSelectAsync(filter);
        }

        public virtual async Task<List<T>> WhereAsync(Expression<Func<T, bool>> filter)
        {
            return await Db.SelectAsync<T>(filter);
        }

        public virtual async Task<List<T>> WhereAsync(SqlExpression<T> filter)
        {
            return await Db.SelectAsync<T>(filter);
        }

        public virtual async Task<T> SingleAsync(Expression<Func<T, bool>> filter)
        {
            return await Db.SingleAsync<T>(filter);
        }

        public virtual async Task<T> SingleAsync(SqlExpression<T> filter)
        {
            return await Db.SingleAsync<T>(filter);
        }

        public virtual async Task<bool> ExistsAsync(Expression<Func<T, bool>> filter)
        {
            return await Db.ExistsAsync(filter);
        }
        public async Task<T> GetByIdAsync(object id, bool isLoadReference = false)
        {
            if (isLoadReference)
            {
                return await Db.LoadSingleByIdAsync<T>(id);
            }
            return await Db.SingleByIdAsync<T>(id);
        }
        public async Task<long> AddAsync(T obj)
        {
            SetAuditCreate(obj);
            SetModified(obj);
            return await Db.InsertAsync(obj, true);
        }
        public async Task<T> UpdateAsync(T obj)
        {
            SetModified(obj);
            await Db.UpdateAsync(obj);
            return obj;
        }
        public async Task UpdateOnlyAsync(T obj, IList<string> fieldChangeds, Expression<Func<T, bool>> where)
        {
            SetModified(obj);

            fieldChangeds = fieldChangeds ?? new List<string>();

            if (obj is IModifiedDate) fieldChangeds.Add(nameof(IModifiedDate.ModifiedDate));

            if (obj is IModifiedBy) fieldChangeds.Add(nameof(IModifiedBy.ModifiedBy));

            await Db.UpdateOnlyAsync(obj, fieldChangeds.ToArray(), where);
        }
        public async Task DeleteAsync(Expression<Func<T, bool>> where, bool isSoftDelete = false)
        {
            var obj = new T();

            if (isSoftDelete)
            {
                var fieldChangeds = SetDeleted(obj);

                if (fieldChangeds?.Any() != true) throw new KvException("Object not contains delete field.");

                SetModified(obj);

                if (obj is IModifiedDate) fieldChangeds.Add(nameof(IModifiedDate.ModifiedDate));

                if (obj is IModifiedBy) fieldChangeds.Add(nameof(IModifiedBy.ModifiedBy));

                await Db.UpdateOnlyAsync(obj, fieldChangeds.ToArray(), where);
            }
            else
            {
                await Db.DeleteAsync(where);
            }
        }

        public virtual async Task<long> CountAsync(Expression<Func<T, bool>> filter)
        {
            if (filter == null) return await Db.CountAsync<T>();

            return await Db.CountAsync(filter);
        }

        public virtual async Task<long> CountAsync(SqlExpression<T> filter)
        {
            if (filter == null) return await Db.CountAsync<T>();

            return await Db.CountAsync(filter);
        }

        /// <summary>
        /// Insert a new row or update existing row. Returns true if a new row was inserted.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public async Task<bool> SaveAsync(T obj, bool references = false)
        {
            return await Db.SaveAsync(obj, references: references);
        }

        public async Task<int> UpdateAllAsync(List<T> lst)
        {
            foreach (var item in lst)
            {
                SetModified(item);
            }
            return await Db.UpdateAllAsync(lst);
        }

        public async Task InsertAllAsync(List<T> lst)
        {
            foreach (var item in lst)
            {
                SetAuditCreate(item);
            }
            await Db.InsertAllAsync(lst);
        }

        public async Task<PagingDataSource<TInto>> Execute<TInto>(ISqlExpression query)
        {
            var q = (SqlExpression<T>)query;
            return new PagingDataSource<TInto>
            {
                Total = (int)await Db.CountAsync(q),
                Data = await Db.LoadSelectAsync<TInto, T>(q),
            };
        }
        #endregion

        #region Set audit object
        private void SetModified(T obj)
        {
            if (obj is IModifiedDate modify)
            {

                modify.ModifiedDate = DateTime.Now;
            }

            if (obj is IModifiedBy by && Context != null)
            {
                by.ModifiedBy = Context.UserId;
            }
        }
        private void SetAuditCreate(T obj)
        {
            if (obj is ICreateDate date)
            {
                date.CreatedDate = DateTime.Now;
            }

            if (obj is ICreateBy by && Context != null)
            {
                by.CreateBy = Context.UserId;
            }
        }

        private List<string> SetDeleted(T obj)
        {
            var fieldChangeds = new List<string>();

            if (obj is IDeleted deleted)
            {
                deleted.IsDeleted = true;
                fieldChangeds.Add(nameof(deleted.IsDeleted));
            }

            return fieldChangeds;
        }
        #endregion

        #region Helper
        public SqlExpression<T> BuildGenericQuery(Expression<Func<T, bool>> filter)
        {
            var query = Db.From<T>();
            return filter != null ? query.Where(filter) : query;
        }
        #endregion

        private bool _disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (_disposedValue) return;

            if (disposing)
            {
                // TODOs: dispose managed state (managed objects).
                _db?.Close();
                _db?.Dispose();
            }

            // TODOs: free unmanaged resources (unmanaged objects) and override a finalizer below.
            // TODOs: set large fields to null.

            _disposedValue = true;
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODOs: uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }
    }
}
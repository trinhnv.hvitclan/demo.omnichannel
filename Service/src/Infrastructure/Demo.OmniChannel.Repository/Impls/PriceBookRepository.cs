﻿using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Impls
{
    public class PriceBookRepository : BaseRepository<PriceBook>, IPriceBookRepository
    {
        public PriceBookRepository(IDbConnection dbConnection) : base(dbConnection)
        {

        }

        public async Task<(decimal?, DateTime?, DateTime?, string)> GetPriceByPriceBook(int retailerId, int branchId, long productId, long priceBookId)
        {
            if (priceBookId <= 0)
            {
                return (null, default(DateTime), default(DateTime), "Bảng giá bán không hợp lệ");
            }
            var priceBook = (await Db.LoadSelectAsync<PriceBook>(x => x.RetailerId == retailerId && x.Id == priceBookId, new[] { "PriceBookBranches" })).FirstOrDefault();
            if (priceBook == null)
            {
                return (null, default(DateTime), default(DateTime), "Bảng giá bán không hợp lệ");
            }
            var (isValidBasePriceBook, detailMessage) = priceBook.IsValidPriceBook(branchId);
            if (!isValidBasePriceBook)
            {
                return (null, default(DateTime), default(DateTime), detailMessage);
            }

            var detail =
                (await Db.SelectAsync<PriceBookDetail>(x => x.PriceBookId == priceBookId && x.ProductId == productId)).FirstOrDefault();
            return (detail?.Price, priceBook?.StartDate, priceBook?.EndDate, "");
        }

        public async Task<Dictionary<long, decimal>> GetPriceByProductIds(long priceBookId, HashSet<long> productIds)
        {
            if (productIds == null || !productIds.Any())
            {
                return new Dictionary<long, decimal>();
            }

            if (priceBookId <= 0)
            {
                return new Dictionary<long, decimal>();
            }
            var query = Db.From<PriceBookDetail>().WithReadUnCommited().Where(x =>
                    x.PriceBookId == priceBookId && productIds.Contains(x.ProductId));
            var result = await Db.SelectAsync(query);
            if (result == null || !result.Any())
            {
                return new Dictionary<long, decimal>();
            }
            return result.ToDictionary(x => x.ProductId, y => y.Price);
        }

        public async Task<PriceBookDetail> GetPriceByProductId(long priceBookId, long productId)
        {
            if (productId <= 0)
            {
                return null;
            }

            return (await Db.SelectAsync<PriceBookDetail>(x => x.PriceBookId == priceBookId && x.ProductId == productId))
                .FirstOrDefault();
        }

        public async Task<(string, string)> GetChannelPriceBookName(Domain.Model.OmniChannel channel)
        {
            string basePriceBookName = null;
            string salePriceBookName = null;
            if (channel.BasePriceBookId.HasValue)
            {
                basePriceBookName = channel.BasePriceBookId != 0 ? (await GetByIdAsync(channel.BasePriceBookId))?.Name : "Bảng giá chung";
            }
            switch (channel.Type)
            {
                case (byte)ChannelType.Lazada:
                case (byte)ChannelType.Sendo:
                    {
                        if (channel.PriceBookId.HasValue)
                        {
                            salePriceBookName = channel.PriceBookId != 0 ? (await GetByIdAsync(channel.PriceBookId))?.Name : "Bảng giá chung";
                        }
                        else
                        {
                            salePriceBookName = "Không đồng bộ";
                        }

                        break;
                    }
            }
            return (basePriceBookName, salePriceBookName);
        }

        public async Task<List<PriceBook>> GetByFilterAsync(int retailerId, List<long> idLst, bool isAcvite)
        {
            var priceBookQuery = Db.From<PriceBook>().WithReadUnCommited()
               .Where(item => item.RetailerId == retailerId && idLst.Contains(item.Id)
               && item.IsActive && item.StartDate < DateTime.Now && item.EndDate > DateTime.Now);
            return await Db.LoadSelectAsync(priceBookQuery);
        }

        public async Task<PriceBook> GetById(int retailerId, long id)
        {
            var query = Db.From<PriceBook>().WithNoLock()
                .Where(x => x.RetailerId == retailerId && x.Id == id);
            var result = await Db.SingleAsync(query);
            return result;
        }

        public async Task<List<PriceBook>> GetByIds(int retailerId, List<long> ids)
        {
            var query = Db.From<PriceBook>().WithReadUnCommited()
                .Where(x => x.RetailerId == retailerId && ids.Contains(x.Id));
            return await Db.LoadSelectAsync(query);
        }

        public async Task<List<PriceBook>> GetByIdLstAsync(int retailerId, List<long> idLst)
        {
            var priceBookQuery = Db.From<PriceBook>().WithReadUnCommited()
                .Where(item => item.RetailerId == retailerId && idLst.Contains(item.Id) 
                && item.IsActive && item.StartDate < DateTime.Now && item.EndDate > DateTime.Now);
            return await Db.LoadSelectAsync(priceBookQuery);
        }

        public async Task<List<PriceBookDetail>> GetDetailById(int retailerId, long id, List<long> productIds = null)
        {
            var query = Db.From<PriceBookDetail>().WithNoLock()
                .Where(x => x.RetailerId == retailerId && x.PriceBookId == id);
            if (productIds != null && productIds.Any())
            {
                query = query.Where(x => productIds.Contains(x.ProductId));
            }
            var result = await Db.SelectAsync(query);
            return result;
        }

        public async Task<List<PriceBookDetail>> GetDetailByIdLst(int retailerId, List<long> idLst, List<long> productIdLst)
        {
            var query = Db.From<PriceBookDetail>().WithReadUnCommited()
                .Where(item => item.RetailerId == retailerId && idLst.Contains(item.PriceBookId) && productIdLst.Contains(item.ProductId));
            return await Db.LoadSelectAsync(query);
        }

        public async Task<List<PriceBookDetail>> GetDetailByPriceBookIdsAndProductIds(int retailerId, List<long> priceBookIds, List<long> productIds)
        {
            var query = Db.From<PriceBookDetail>().WithReadUnCommited()
                .Where(x => x.RetailerId == retailerId && priceBookIds.Contains(x.PriceBookId) && productIds.Contains(x.ProductId));
            var result = await Db.SelectAsync(query);
            return result;
        }
        public async Task<List<PriceBook>> GetByRetailer(int retailerId)
        {
            var query = Db.From<PriceBook>().WithReadUnCommited()
                .Where(p => p.RetailerId == retailerId);
            return await Db.LoadSelectAsync(query);
        }
    }
}
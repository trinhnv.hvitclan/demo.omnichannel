﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.Utilities;
using ServiceStack;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Impls
{
    public class OrderRepository : BaseRepository<Order>, IOrderRepository
    {
        public OrderRepository(IDbConnection dbConnection) : base(dbConnection)
        {

        }

        public async Task<Order> GetOrderByCodeAsync(string code, int retailerId)
        {
            const string query = "SELECT [Id],[CustomerId],[CashierId],[PurchaseDate],[EndPurchaseDate],[Code],[BookingTitle],[PaymentType],[BranchId],[Description],[Status] " +
                                   ",[ModifiedDate],[RetailerId],[Discount],[SoldById],[TableId],[CreatedDate],[CreatedBy],[ModifiedBy],[DiscountRatio],[Extra],[Total],[TotalPayment] " +
                                   ",[UsingCod],[Surcharge],[Point],[Uuid],[SaleChannelId],[IsFavourite], " +
                                   "(SELECT detail.ProductId, detail.Quantity, detail.Price, detail.Discount, detail.DiscountRatio, detail.Note, detail.OrderId FROM  [OrderDetail] AS detail with (nolock) " +
                                   "JOIN [Order] ord with (nolock) on detail.OrderId = ord.Id  " +
                                   " WHERE ord.Id = o.Id  " +
                                   "FOR JSON PATH) AS OrderDetail  " +
                                   "FROM [Order] o WITH (NOLOCK) WHERE RetailerId = @retailerId AND Code = @code " +
                                   "OPTION(OPTIMIZE FOR UNKNOWN) ";
            var result = (await Db.SqlListAsync<OrderOptimize>(query, new { retailerId, code })).FirstOrDefault();
            if (result != null)
            {
                result.OrderDetails = result.OrderDetail.FromJson<List<OrderDetail>>() ?? new List<OrderDetail>();
            }
            return result;
        }

        public async Task<Order> GetOrderByCodeWithUncommited(int retailerId, string code)
        {
            var orderQuery = Db.From<Order>().WithReadUnCommited().Where(c => c.RetailerId == retailerId && c.Code == code);
            return (await Db.LoadSelectAsync(orderQuery)).FirstOrDefault();
        }

        public async Task<List<Order>> GetOrdersByCodeListAsync(List<string> codeList, int retailerId)
        {
            int totalCount = codeList.Count;
            var totalPages = (int)Math.Ceiling(totalCount / (decimal)2000);
            var listOrder = new List<Order>();
            for (int page = 1; page <= totalPages; page++)
            {
                var orderPaging = codeList.Page(page, 2000).ToList();
                var orders = await Db.LoadSelectAsync<Order>(
                    c => c.RetailerId == retailerId && orderPaging.Contains(c.Code),
                    new[] { "OrderDetails" });
                listOrder.AddRange(orders);
            }
            return listOrder;
        }

        public async Task<List<Order>> GetOrdersByCodeListAsync(List<string> codeList, int retailerId, int pageSize)
        {
            int totalCount = codeList.Count;
            var totalPages = (int)Math.Ceiling(totalCount / (decimal)pageSize);
            var listOrder = new List<Order>();
            for (int page = 1; page <= totalPages; page++)
            {
                var orderPaging = codeList.Page(page, pageSize).ToList();
                var orders = await Db.LoadSelectAsync<Order>(
                    c => c.RetailerId == retailerId && orderPaging.Contains(c.Code));
                listOrder.AddRange(orders);
            }
            return listOrder;
        }

        public async Task<List<long>> GetByFilterAsync(int retailerId, DateTime startDate, DateTime endDate, string prefix = "DHSPE")
        {
            var query = Db.From<Order>().WithReadUnCommited()
                .Where(item => item.RetailerId == retailerId && item.CreatedDate >= startDate
                && item.CreatedDate <= endDate
                && item.Status != (int)OrderState.Void
                && item.Code.StartsWith(prefix))
                .Select<Order>((item) => item.Id);
            return await Db.SelectAsync<long>(query);

        }

    }
}
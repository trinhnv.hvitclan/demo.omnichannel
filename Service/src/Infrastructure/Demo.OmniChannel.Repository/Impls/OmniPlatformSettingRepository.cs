﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.ShareKernel.Dto;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Repository.Impls
{
    public class OmniPlatformSettingRepository : BaseRepository<OmniPlatformSetting>, IOmniPlatformSettingRepository
    {
        private readonly List<OmniPlatformSetting> _settingNeedAdd = new List<OmniPlatformSetting>();
        private readonly List<OmniPlatformSetting> _settingNeedUpdate = new List<OmniPlatformSetting>();

        public OmniPlatformSettingRepository(IDbConnection dbConnection) : base(dbConnection)
        {
        }

        public async Task<CreateUpdateSettingCustomer> CreateOrUpdateAsync(CreateUpdateSettingCustomer settingDto,
            int retailerId)
        {
            var settingItemListDto = settingDto.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .ToDictionary(prop => prop.Name, prop => prop.GetValue(settingDto, null)?.ToString())
                .ToList();

            var oldSettingQuery =
                Db.From<OmniPlatformSetting>().Where(x => x.RetailerId == retailerId);
            var lsSetting = (await Db.SelectAsync(oldSettingQuery)).ToList();

            AddUpdateSetting(settingItemListDto, lsSetting, retailerId);


            if (_settingNeedUpdate.Any())
            {
                await UpdateAllAsync(_settingNeedUpdate);
            }

            if (_settingNeedAdd.Any())
            {
                await InsertAllAsync(_settingNeedAdd);
            }

            return settingDto;
        }

        public async Task<OmniPlatformSettingDto> GetByRetailerAsync(int retailerId)
        {
            var settingQuery =
                Db.From<OmniPlatformSetting>().Where(x => x.RetailerId == retailerId);

            var lsSetting = (await Db.SelectAsync(settingQuery)).ToList();

            var settingDto = new OmniPlatformSettingDto
            {
                Data = lsSetting.ToDictionary(x => x.Name, x => x.Value)
            };

            return settingDto;
        }
        

        #region Private method
        private void AddUpdateSetting(List<KeyValuePair<string, string>> settingItemListDto,
            List<OmniPlatformSetting> lsSetting, int retailerId)
        {
            foreach (var item in settingItemListDto)
            {
                if (string.IsNullOrEmpty(item.Key)) continue;
                var setting = lsSetting.FirstOrDefault(x => x.Name == item.Key);
                if (setting != null)
                {
                    setting.Update(item.Value);
                    _settingNeedUpdate.Add(setting);
                }
                else
                {
                    var newSetting = new OmniPlatformSetting(item.Key, item.Value, retailerId);
                    _settingNeedAdd.Add(newSetting);
                }
            }
        }

        #endregion
    }
}

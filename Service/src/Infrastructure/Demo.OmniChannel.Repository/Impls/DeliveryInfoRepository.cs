﻿using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.ShareKernel.Dto;
using Demo.OmniChannel.Utilities;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Impls
{
    public class DeliveryInfoRepository : BaseRepository<DeliveryInfo>, IDeliveryInfoRepository
    {
        public DeliveryInfoRepository(IDbConnection dbConnection) : base(dbConnection)
        {
        }
       

        public async Task<DeliveryInfoDto> GetLastByOrderIdAsync(int retailerId, long orderId)
        {
            using (var transaction = Db.OpenTransaction(IsolationLevel.ReadUncommitted))
            {
                var sqlExpression = Db.From<DeliveryInfo>()
               .LeftJoin<DeliveryPackage>((info, package) => package.RetailerId == retailerId && info.DeliveyPackageId == package.Id)
               .LeftJoin<PartnerDelivery>((info, delivery) => delivery.RetailerId == retailerId && info.DeliveryBy == delivery.Id)
               .Where(info => info.RetailerId == retailerId && info.OrderId == orderId && info.IsCurrent == true)
               .Select<DeliveryInfo, DeliveryPackage, PartnerDelivery>((info, package, partner) => new
               {
                   DeliveryInfoId = info.Id,
                   IsCurrent = info.IsCurrent,
                   Status = info.Status,
                   UseDefaultPartner = info.UseDefaultPartner,
                   DeliveryPackageId = package.Id,
                   DeliveryCode = info.DeliveryCode,
                   Weight = package.Weight,
                   Length = package.Length,
                   Width = package.Length,
                   Height = package.Height,
                   Receiver = package.Receiver,
                   ContactNumber = package.ContactNumber,
                   Address = package.Address,
                   LocationId = package.LocationId,
                   LocationName = package.LocationName,
                   WardName = package.WardName,
                   Comments = package.Comments,
                   UsingCod = package.UsingCod,
                   WardId = package.WardId,
                   ConvertedWeight = package.ConvertedWeight,
                   Type = package.Type,
                   PartnerId = partner.Id,
                   PartnerCode = partner.Code,
                   PartnerName = partner.Name
               });
                return await Db.SingleAsync<DeliveryInfoDto>(sqlExpression);
            }
        }

        public async Task<DeliveryInfo> GetLastByInvoiceIdAsync(long invoiceId)
        {
            return (await Db.LoadSelectAsync<DeliveryInfo>(x => x.InvoiceId == invoiceId && x.IsCurrent == true, new[] { "DeliveryPackage" })).FirstOrDefault();
        }

        public async Task<DeliveryInfo> GetLastByDeliveryCode(string deliveryCode)
        {
            return await Db.SingleAsync<DeliveryInfo>(x => x.DeliveryCode == deliveryCode && x.IsCurrent == true);
        }

        public async Task<List<DeliveryInfo>> GetDeliveryInfoByListOrderId(List<long?> orderIdLst, int retailerId, int pageSize = 50)
        {
            int totalCount = orderIdLst.Count;
            var totalPages = (int)Math.Ceiling(totalCount / (decimal)pageSize);
            var listDeliveryInfo = new List<DeliveryInfo>();
            for (int page = 1; page <= totalPages; page++)
            {
                var orderPaging = orderIdLst.Page(page, pageSize).ToList();
                var query = Db.From<DeliveryInfo>().WithNoLock().Where(x => x.RetailerId == retailerId && orderPaging.Contains(x.OrderId));
                var deliveryInfos = await Db.LoadSelectAsync(query);
                listDeliveryInfo.AddRange(deliveryInfos);
            }
            return listDeliveryInfo;
        }

        public async Task<List<DeliveryInfo>> GetDeliveryInfoByListInvoices(List<long?> invoiceIdLst, int retailerId, int pageSize = 50)
        {
            int totalCount = invoiceIdLst.Count;
            var totalPages = (int)Math.Ceiling(totalCount / (decimal)pageSize);
            var listDeliveryInfo = new List<DeliveryInfo>();
            for (int page = 1; page <= totalPages; page++)
            {
                var invoicePaging = invoiceIdLst.Page(page, pageSize).ToList();

                var query = Db.From<DeliveryInfo>().WithNoLock().Where(c => c.RetailerId == retailerId && invoicePaging.Contains(c.InvoiceId));
                var deliveryInfos = await Db.LoadSelectAsync(query);
                listDeliveryInfo.AddRange(deliveryInfos);
            }
            return listDeliveryInfo;
        }
    }
}
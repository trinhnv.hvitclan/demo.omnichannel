﻿using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.Utilities;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Impls
{
    public class CustomerRepository : BaseRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(IDbConnection dbConnection) : base(dbConnection)
        {
        }

        public async Task<Customer> Get(int retailerId, string phone, string code, bool isManagerCustomerByBranch, int branchId)
        {
            var result = await SingleAsync(c =>
                c.RetailerId == retailerId &&
                (c.IsDeleted == null || c.IsDeleted == false) &&
                (
                    (!string.IsNullOrEmpty(phone) && (c.SearchNumber == phone || c.ContactNumber == phone))
                    ||
                    (!string.IsNullOrEmpty(code) && c.Code == code)
                ) &&
                (
                    (isManagerCustomerByBranch && c.BranchId.HasValue && c.BranchId.Value == branchId)
                    ||
                    !isManagerCustomerByBranch)
                );

            return result;
        }

        public async Task<Customer> GetByFilterAsync(int retailerId, int branchId, string phone, string customerCode, bool isManagerCustomerByBranch)
        {
            var customerQry =
           Db.From<Customer>().WithReadUnCommited()
               .Where(c => c.RetailerId == retailerId)
               .Where(c => c.IsDeleted == null || c.IsDeleted == false);

            var hashNumber = PhoneNumberHelper.GetHashContactNumber(phone);

            if (!string.IsNullOrEmpty(hashNumber))
            {
                customerQry = customerQry.Where(c => c.HashNumber == hashNumber ||
                                                             c.SearchNumber == phone ||
                                                            c.ContactNumber == phone ||
                                                             c.Code == customerCode);
            }
            else
            {
                customerQry = customerQry.Where(c => c.Code == customerCode);
            }
            if (isManagerCustomerByBranch)
                customerQry = customerQry.Where(c => c.BranchId != null && c.BranchId.Value == branchId);

            var customer = await Db.SingleAsync(customerQry);

            return customer;
        }

        public async Task<Customer> GetAsync(int retailerId, int branchId, string phone, string customerCode, bool isManagerCustomerByBranch)
        {
            var customerQry =
           Db.From<Customer>().WithReadUnCommited()
               .Where(c => c.RetailerId == retailerId)
               .Where(c => c.IsDeleted == null || c.IsDeleted == false);

            var hashNumber = PhoneNumberHelper.GetHashNumber(phone);

            if (!string.IsNullOrEmpty(hashNumber))
            {
                customerQry = customerQry.Where(c => c.HashNumber == hashNumber ||
                                                             c.SearchNumber == phone ||
                                                            c.ContactNumber == phone ||
                                                             c.Code == customerCode);
            }
            else
            {
                customerQry = customerQry.Where(c => c.Code == customerCode);
            }
            if (isManagerCustomerByBranch)
                customerQry = customerQry.Where(c => c.BranchId != null && c.BranchId.Value == branchId);

            var customer = await Db.SingleAsync(customerQry);

            return customer;
        }

        public async Task<List<Customer>> GetListByIds(int retailerId, List<long> customerIds)
        {
            int totalCount = customerIds.Count;
            var totalPages = (int)Math.Ceiling(totalCount / (decimal)200);
            var customerLst = new List<Customer>();
            for (int page = 1; page <= totalPages; page++)
            {
                var customerIdsPaging = customerIds.Page(page, 200).ToList();
                var customerResult = await Db.LoadSelectAsync<Customer>(
                    c => c.RetailerId == retailerId && customerIdsPaging.Contains(c.Id));
                customerLst.AddRange(customerResult);
            }
            return customerLst;
        }

    }
}
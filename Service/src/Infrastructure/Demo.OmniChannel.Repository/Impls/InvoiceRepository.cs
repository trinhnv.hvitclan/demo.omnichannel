﻿using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.Utilities;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Repository.Impls
{
    public class InvoiceRepository : BaseRepository<Invoice>, IInvoiceRepository
    {
        public InvoiceRepository(IDbConnection dbConnection) : base(dbConnection)
        {

        }

        public async Task<long?> CheckExistInvoice(long orderId, int retailerId, List<long> ids)
        {
            var invoices = await Db.LoadSelectAsync<Invoice>(x => x.RetailerId == retailerId && x.OrderId == orderId,
                new[] { "InvoiceDetails" });
            if (invoices == null || !invoices.Any())
            {
                return null;
            }
            var details = invoices.SelectMany(x => x.InvoiceDetails).ToList();
            if (!details.Any())
            {
                return null;
            }

            foreach (var item in details)
            {
                if (!ids.Contains(item.ProductId))
                {
                    return null;
                }
            }
            return invoices.FirstOrDefault()?.Id;
        }

        public async Task<List<Invoice>> GetByOrderId(long orderId, int retailerId)
        {
            var query = Db.From<Invoice>().WithNoLock()
                .Where(x => x.RetailerId == retailerId && x.OrderId == orderId);
            return await Db.LoadSelectAsync(query, new[] { "InvoiceDetails" });
        }

        public async Task<List<Invoice>> GetLastExistedInvoiceLst(string originalCode, int retailerId)
        {
            var query = Db.From<Invoice>().WithNoLock()
                .Where(x => x.RetailerId == retailerId && x.Code.StartsWith(originalCode)).OrderByDescending(x => x.CreatedDate);
            return await Db.LoadSelectAsync(query);
        }

        public async Task<List<Invoice>> GetByLstOrderId(List<long?> orderIdLst, int retailerId, int pageSize)
        {
            int totalCount = orderIdLst.Count;
            var totalPages = (int)Math.Ceiling(totalCount / (decimal)pageSize);
            var listInvoice = new List<Invoice>();
            for (int page = 1; page <= totalPages; page++)
            {
                var orderPaging = orderIdLst.Page(page, pageSize).ToList();
                var invoices = await Db.LoadSelectAsync<Invoice>(
                    c => c.RetailerId == retailerId && orderPaging.Contains(c.OrderId));
                listInvoice.AddRange(invoices);
            }
            return listInvoice;
        }

        public async Task<List<Invoice>> GetDetInvoiceByInvoiceCodeLst(List<string> invoiceCodeLst, int retailerId, byte status, int pageSize)
        {
            int totalCount = invoiceCodeLst.Count;
            var totalPages = (int)Math.Ceiling(totalCount / (decimal)pageSize);
            var listInvoice = new List<Invoice>();
            for (int page = 1; page <= totalPages; page++)
            {
                var invoicePaging = invoiceCodeLst.Page(page, pageSize).ToList();
                var invoices = await Db.LoadSelectAsync<Invoice>(
                    c => c.RetailerId == retailerId
                    && c.Debt == 0
                    && invoicePaging.Contains(c.Code)
                    && c.Status == status);
                listInvoice.AddRange(invoices);
            }
            return listInvoice;
        }
    }
}
﻿using System;

namespace Demo.OmniChannel.Redis
{
    public interface IAcquireLockRedis
    {
        IDisposable AcquireLock(string key);
        IDisposable AcquireLock(string key, TimeSpan timeout);
    }
}

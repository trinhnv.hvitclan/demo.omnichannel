﻿using ServiceStack.Redis;

namespace Demo.OmniChannel.Redis
{
    public class KvLockRedis : IKvLockRedis
    {
       private static IRedisClientsManager _redisClientsManager;

        public KvLockRedis(IRedisClientsManager redisClientsManager)
        {
            _redisClientsManager = redisClientsManager;
        }
        public KvAcquireLockRedis GetLockFactory()
        {
            return new KvAcquireLockRedis(_redisClientsManager.GetClient());
        }
    }
}

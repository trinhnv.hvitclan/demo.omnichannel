﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Logging;
using ServiceStack.Redis;

namespace Demo.OmniChannel.Redis
{

    public class KvCacheClient : ICacheClientExtended, IKvCacheClient
    {
        private readonly IRedisClientsManager _redisClients;
        private static readonly ILog Log = LogManager.GetLogger(typeof(KvCacheClient));
        private readonly bool _usingSlave;
        private readonly SemaphoreSlim _asyncLock = new SemaphoreSlim(1, 1);

        public KvCacheClient(IRedisClientsManager redisClients, IAppSettings settings)
        {
            _redisClients = redisClients;
            _usingSlave = settings.Get<bool>("redis:usingslave");
        }
        public void Dispose()
        {

        }

        private IRedisClient GetReadClient()
        {
            try
            {
                if (_usingSlave)
                {
                    return _redisClients.GetReadOnlyClient();
                }
            }
            catch (Exception e)
            {
                Log.Error($"KvCacheClient error: Can't get slave - {e.Message}", e);
            }

            return GetClient();
        }
        private IRedisClient GetClient()
        {
            return _redisClients.GetClient();
        }
        public bool Remove(string key)
        {
            using (var cli = GetClient())
            {
                return cli.Remove(key);
            }
        }
        public void RemoveAll(IEnumerable<string> keys)
        {
            if (keys == null) return;
            var enumerable = keys.ToList();
            using (var cli = GetClient())
            {
                cli.RemoveAll(enumerable);
                return;
            }
        }
        public T Get<T>(string key)
        {
            using (var cli = GetClient())
            {
                return cli.Get<T>(key);
            }

        }
        public virtual async Task<T> GetAndSetAsync<T>(string key, Func<string, Task<T>> factory, TimeSpan? slidingExpireTime = null) where T : class
        {
            T item = null;

            try
            {
                item = await GetOrDefaultAsync<T>(key);
            }
            catch (Exception exAsync)
            {
                Log.Error(exAsync, exAsync.ToString());
            }

            if (item != null) return item;
            // lock
            await _asyncLock.WaitAsync();
            try
            {
                item = await GetOrDefaultAsync<T>(key);

                if (item == null)
                {
                    item = await factory(key);

                    if (item == null)
                    {
                        // unlock
                        _asyncLock.Release();
                        return null;
                    }

                    await SetAsync(key, item, slidingExpireTime);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.ToString());
            }

            // unlock
            _asyncLock.Release();

            return item;
        }
        public long Increment(string key, uint amount)
        {

            using (var cli = GetClient())
            {
                return cli.Increment(key, amount);
            }

        }
        public long Decrement(string key, uint amount)
        {


            using (var cli = GetClient())
            {
                return cli.Decrement(key, amount);
            }

        }
        public bool Add<T>(string key, T value)
        {

            using (var cli = GetClient())
            {
                return cli.Add(key, value);
            }
        }
        public bool Set<T>(string key, T value)
        {

            using (var cli = GetClient())
            {
                return cli.Set(key, value);
            }
        }
        public bool Replace<T>(string key, T value)
        {

            using (var cli = GetClient())
            {
                return cli.Replace(key, value);

            }

        }
        public bool Add<T>(string key, T value, DateTime expiresAt)
        {

            using (var cli = GetClient())
            {
                return cli.Add(key, value, expiresAt);
            }
        }
        public bool Set<T>(string key, T value, DateTime expiresAt)
        {

            using (var cli = GetClient())
            {
                return cli.Set(key, value, expiresAt);
            }
        }
        public bool Replace<T>(string key, T value, DateTime expiresAt)
        {

            using (var cli = GetClient())
            {
                return cli.Replace(key, value, expiresAt);
            }
        }
        public bool Add<T>(string key, T value, TimeSpan expiresIn)
        {

            using (var cli = GetClient())
            {
                return cli.Add(key, value, expiresIn);
            }
        }
        public bool Set<T>(string key, T value, TimeSpan expiresIn)
        {

            using (var cli = GetClient())
            {
                return cli.Set(key, value, expiresIn);
            }
        }
        public virtual Task SetAsync<T>(string key, T value, TimeSpan? slidingExpireTime = null, TimeSpan? absoluteExpireTime = null)
        {
            using (var cli = _redisClients.GetClient())
            {
                cli.Set(key, value, slidingExpireTime ?? TimeSpan.FromHours(1));
            }
            return Task.FromResult(0);
        }
        public bool Replace<T>(string key, T value, TimeSpan expiresIn)
        {

            using (var cli = GetClient())
            {
                return cli.Replace(key, value, expiresIn);
            }
        }
        public void FlushAll()
        {
            using (var cli = GetClient())
            {
                cli.FlushAll();
                return;

            }
        }
        public IDictionary<string, T> GetAll<T>(IEnumerable<string> keys)
        {
            var enumerable = keys.ToList();
            using (var cli = GetClient())
            {
                return cli.GetAll<T>(enumerable);
            }
        }
        public void SetAll<T>(IDictionary<string, T> values)
        {

            using (var cli = GetClient())
            {
                cli.SetAll(values);
                return;

            }
        }
        public TimeSpan? GetTimeToLive(string key)
        {
            using (var cli = GetClient())
            {
                return cli.GetTimeToLive(key);
            }
        }
        public IEnumerable<string> GetKeysByPattern(string pattern)
        {
            using (var cli = GetReadClient())
            {
                return cli.ScanAllKeys(pattern);
            }
        }

        public Dictionary<string, string> GetByPipeLine(List<string> keys)
        {
            var dic = new Dictionary<string, string>();

            using (var cli = GetReadClient())
            {
                using (var pipe = cli.CreatePipeline())
                {
                    foreach (var key in keys)
                    {
                        pipe.QueueCommand(t => t.Get<string>(key), c => dic.Add(key, c));
                        //    dic.Add(key, cli.Get<string>(key));
                    }
                    pipe.Flush();
                }
            }

            return dic;
        }

        public bool ContainsKey(string key)
        {

            using (var cli = GetReadClient())
            {
                return cli.ContainsKey(key);
            }
        }

        public void AddItemToSet(string setid, string item)
        {
            using (var cli = GetClient())
            {
                var type = cli.GetEntryType(setid);
                if (type != RedisKeyType.Set)
                {
                    cli.Remove(setid);
                }

                cli.AddItemToSet(setid, item);
            }
        }

        public void InitSet(string setid, List<string> items)
        {
            using (var cli = GetClient())
            {
                cli.AddRangeToSet(setid, items);
            }
        }
        public void AddItemToSortedSet(string setid, string item)
        {
            using (var cli = GetClient())
            {
                var type = cli.GetEntryType(setid);
                if (type != RedisKeyType.SortedSet)
                {
                    cli.Remove(setid);
                }

                cli.AddItemToSortedSet(setid, item);
            }
        }

        public bool ExpireEntryAt(string key, DateTime expireAt)
        {
            using (var cli = GetClient())
            {
                return cli.ExpireEntryAt(key, expireAt);
            }
        }

        public bool ExpireEntryIn(string key, TimeSpan expireIn)
        {
            using (var cli = GetClient())
            {
                return cli.ExpireEntryIn(key, expireIn);
            }
        }

        public void RemoveItemFromSortedSet(string setId, string item)
        {
            using (var cli = GetClient())
            {
                var type = cli.GetEntryType(setId);
                if (type != RedisKeyType.SortedSet)
                {
                    cli.Remove(setId);
                }
                cli.RemoveItemFromSortedSet(setId, item);
            }
        }

        public void RemoveItemFromSet(string setId, string item)
        {
            using (var cli = GetClient())
            {
                var type = cli.GetEntryType(setId);
                if (type != RedisKeyType.Set)
                {
                    cli.Remove(setId);
                }
                cli.RemoveItemFromSet(setId, item);
            }
        }

        public HashSet<string> GetAllItemsFromSet(string setId)
        {
            var retval = new HashSet<string>();
            using (var cli = GetClient())
            {
                var type = cli.GetEntryType(setId);
                if (type != RedisKeyType.Set)
                {
                    cli.Remove(setId);
                }
                var ls = cli.GetAllItemsFromSet(setId);
                foreach (var item in ls)
                {
                    retval.Add(item);
                }
                return retval;
            }
        }
        public HashSet<string> GetAllItemsFromSortedSet(string setId)
        {
            var retval = new HashSet<string>();
            using (var cli = GetClient())
            {
                var type = cli.GetEntryType(setId);
                if (type != RedisKeyType.SortedSet)
                {
                    cli.Remove(setId);
                }
                var ls = cli.GetAllItemsFromSortedSet(setId);
                foreach (var item in ls)
                {
                    retval.Add(item);
                }
                return retval;
            }
        }
        public long GetFromSetCount(string setId)
        {
            using (var cli = GetClient())
            {
                var type = cli.GetEntryType(setId);
                if (type != RedisKeyType.Set)
                {
                    cli.Remove(setId);
                }
                return cli.GetSetCount(setId);
            }
        }

        public long GetSortedSetCount(string setId)
        {
            using (var cli = GetClient())
            {
                var type = cli.GetEntryType(setId);
                if (type != RedisKeyType.SortedSet)
                {
                    cli.Remove(setId);
                }
                return cli.GetSortedSetCount(setId);
            }
        }
        public bool SetContainsItem(string setId, string item)
        {
            using (var cli = GetClient())
            {
                var type = cli.GetEntryType(setId);
                if (type != RedisKeyType.Set)
                {
                    cli.Remove(setId);
                    return false;
                }
                return cli.SetContainsItem(setId, item);
            }
        }

        public bool SortedSetContainsItem(string setId, string item)
        {
            using (var cli = GetClient())
            {
                var type = cli.GetEntryType(setId);
                if (type != RedisKeyType.SortedSet)
                {
                    cli.Remove(setId);
                    return false;
                }
                return cli.SortedSetContainsItem(setId, item);
            }
        }
        public Dictionary<string, string> GetAllEntriesFromHash(string hashId)
        {
            using (var cli = GetReadClient())
            {
                return cli.GetAllEntriesFromHash(hashId);
            }
        }
        public bool SetEntryInHash(string hashId, string key, string value)
        {
            using (var cli = GetClient())
            {
                return cli.SetEntryInHash(hashId, key, value);
            }
        }
        public bool RemoveEntryFromHash(string hashId, string key)
        {
            using (var cli = GetClient())
            {
                return cli.RemoveEntryFromHash(hashId, key);
            }
        }
        public string GetValueFromHash(string hashId, string key)
        {
            using (var cli = GetReadClient())
            {
                return cli.GetValueFromHash(hashId, key);
            }
        }
        public List<T> GetValues<T>(List<string> keys)
        {
            try
            {
                using (var cli = GetReadClient())
                {
                    return cli.GetValues<T>(keys);
                }
            }
            catch (Exception e)
            {
                Log.Error($"KvCacheClient error get values: {e.Message} - Keys: {keys?.Join(", ")}", e);
            }
            return default(List<T>);
        }

        public void InsertAllByPipeLine<T>(Dictionary<string, T> keys, TimeSpan? expiresIn)
        {

            using (var cli = GetReadClient())
            {
                using (var pipe = cli.CreatePipeline())
                {
                    foreach (var key in keys)
                    {
                        pipe.QueueCommand(t => t.Set(key.Key, key.Value, expiresIn));
                    }
                    pipe.Flush();
                }

            }
        }
        public void RemoveAllByPipeLine<T>(Dictionary<string, T> keys)
        {
            List<string> listKeys = new List<string>();
            using (var cli = GetReadClient())
            {
                foreach (var key in keys)
                {
                    if (cli.ContainsKey(key.Key))
                        listKeys.Add(key.Key);
                }
                using (var pipe = cli.CreatePipeline())
                {
                    foreach (var key in listKeys)
                    {
                        pipe.QueueCommand(t => t.Remove(key));
                    }
                    pipe.Flush();
                }

            }
        }

        public T GetOrDefault<T>(string key) where T : class
        {
            using (var cli = GetClient())
            {
                return cli.Get<T>(key);
            }
        }
        public virtual Task<T> GetOrDefaultAsync<T>(string key) where T : class
        {
            return Task.FromResult(GetOrDefault<T>(key));
        }
    }
}
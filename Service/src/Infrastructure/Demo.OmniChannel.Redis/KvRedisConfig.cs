﻿using System.Collections.Generic;

namespace Demo.OmniChannel.Redis
{
    public class KvRedisConfig
    {
        public List<int> Groups { get; set; }
        public string Servers { get; set; }
        public string SentinelMasterName { get; set; }
        public int DbNumber { get; set; }
        public string AuthPass { get; set; }
        public bool IsSentinel { get; set; }
        /// <summary>
        /// For ServiceStack Redis
        /// </summary>
        public int MaxPoolSize { get; set; }
        /// <summary>
        ///  The number of times to repeat the initial connect cycle if no servers respond
        /// </summary>
        public int ConnectRetry { get; set; }
        /// <summary>
        /// Second
        /// </summary>
        public int MaxPoolTimeout { get; set; }
        public int ConnectTimeout { get; set; }
        public int WaitBeforeForcingMasterFailover { get; set; }
        public string ServersRead { get; set; }

        public List<EventMessage> EventMessages { get; set; }
        public List<string> QueueNames { get; set; }
    }

    public class EventMessage
    {
        public string Name { get; set; }
        public int ThreadSize { get; set; }
        public bool IsActive { get; set; }
    }
}
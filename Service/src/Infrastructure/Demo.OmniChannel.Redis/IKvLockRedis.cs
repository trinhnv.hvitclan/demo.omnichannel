﻿namespace Demo.OmniChannel.Redis
{
    public interface IKvLockRedis
    {
        KvAcquireLockRedis GetLockFactory();
    }
}

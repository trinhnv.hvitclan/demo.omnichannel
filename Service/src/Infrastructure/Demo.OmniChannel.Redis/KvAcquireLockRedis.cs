﻿using System;
using ServiceStack.Redis;

namespace Demo.OmniChannel.Redis
{
    public class KvAcquireLockRedis : IAcquireLockRedis, IDisposable
    {
        private readonly IRedisClient _redisClient;
        public KvAcquireLockRedis(IRedisClient redisClient)
        {
            _redisClient = redisClient;
        }
        public IDisposable AcquireLock(string key)
        {
            return _redisClient.AcquireLock(key);
        }
        public IDisposable AcquireLock(string key, TimeSpan timeout)
        {
            return _redisClient.AcquireLock(key, timeout);
        }
        public void Dispose()
        {
            _redisClient.Dispose();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Redis
{
    public interface IKvCacheClient 
    {
        Dictionary<string, string> GetByPipeLine(List<string> keys);
        bool ContainsKey(string key);
        void AddItemToSet(string setid, string item);
        void InitSet(string setid, List<string> items);
        void AddItemToSortedSet(string setid, string item);
        bool ExpireEntryAt(string key, DateTime expireAt);
        bool ExpireEntryIn(string key, TimeSpan expireIn);
        void RemoveItemFromSet(string setId, string item);
        HashSet<string> GetAllItemsFromSet(string setId);
        long GetFromSetCount(string setId);
        bool SetContainsItem(string setId, string item);

        void RemoveItemFromSortedSet(string setId, string item);
        HashSet<string> GetAllItemsFromSortedSet(string setId);
        long GetSortedSetCount(string setId);
        bool SortedSetContainsItem(string setId, string item);

        Dictionary<string, string> GetAllEntriesFromHash(string hashId);
        bool SetEntryInHash(string hashId, string key, string value);
        bool RemoveEntryFromHash(string hashId, string key);
        string GetValueFromHash(string hashId, string key);
        void InsertAllByPipeLine<T>(Dictionary<string, T> keys, TimeSpan? expiresIn);
        void RemoveAllByPipeLine<T>(Dictionary<string, T> keys);
        List<T> GetValues<T>(List<string> keys);
        Task<T> GetAndSetAsync<T>(string key, Func<string, Task<T>> factory, TimeSpan? slidingExpireTime = null) where T : class;
    }
}

﻿using System;
using System.Collections.Generic;
using ServiceStack.Logging;
using ServiceStack.Redis;

namespace Demo.OmniChannel.Redis
{
    public class KvRedisPoolManager
    {
        static readonly ILog Log = LogManager.GetLogger(typeof(KvRedisPoolManager));
        public static IRedisClientsManager GetClientsManager(KvRedisConfig config)
        {
            IRedisClientsManager pool;
            try
            {
                if (config == null) return null;
                var maxpoolsize = config.MaxPoolSize > 0 ? config.MaxPoolSize : 40;
                var pooltimeout = config.MaxPoolTimeout > 0 ? config.MaxPoolTimeout : 10;
                var connectTimeout = config.ConnectTimeout > 0 ? config.ConnectTimeout : 20000;
                var waitBeforeForcingMasterFailover = config.WaitBeforeForcingMasterFailover > 0 ? config.WaitBeforeForcingMasterFailover : 300;
                if (config.IsSentinel)
                {
                    IEnumerable<string> servers = config.Servers.Split(';');

                    var sentinel = new RedisSentinel(servers, config.SentinelMasterName)
                    {
                        RedisManagerFactory = (master, slaves) => new PooledRedisClientManager(master, slaves, null, config.DbNumber, maxpoolsize, null),
                        ScanForOtherSentinels = false

                    };
                    sentinel.OnFailover += x => Log.Error($"Redis fail over to {sentinel.GetMaster()}");
                    sentinel.HostFilter = host => $"{config.AuthPass}@{host}?db={config.DbNumber}&RetryTimeout=100&ConnectTimeout={connectTimeout}";
                    sentinel.WaitBeforeForcingMasterFailover = TimeSpan.FromSeconds(waitBeforeForcingMasterFailover);
                    pool = sentinel.Start();

                }
                else
                {
                    var redisConnection = $"{config.AuthPass}@{config.Servers}?ConnectTimeout={connectTimeout}";
                    var redisConnectionRead = string.IsNullOrEmpty(config.ServersRead) ? redisConnection : $"{config.AuthPass}@{config.ServersRead}?ConnectTimeout={connectTimeout}";
                    pool = new PooledRedisClientManager(new[] { redisConnection }, new[] { redisConnectionRead }, null, config.DbNumber, maxpoolsize, pooltimeout);
                }
                //Check connect
                using (var cli = (IRedisNativeClient)pool.GetClient())
                {
                    if (cli.Ping())
                    {
                        return pool;
                    }
                }


            }
            catch (Exception e)
            {
                Log.Error($"Redis config error: {e.Message}", e);
            }

            return null;
        }
    }
}
﻿using Demo.OmniChannel.ChannelClient.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Demo.OmniChannel.Services.LogginConfiguration
{
    public class MicrosoftLoggingContextExtension : IMicrosoftLoggingContextExtension
    {
        public LogObjectMicrosoftExtension LogMicrosoftExtension { get; private set; }

        public void SetMicrosoftLogging(LogObjectMicrosoftExtension logContext)
        {
            LogMicrosoftExtension = logContext;
        }
    }
}

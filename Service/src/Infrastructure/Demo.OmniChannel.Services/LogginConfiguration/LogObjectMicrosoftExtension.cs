﻿using Serilog.Context;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace KiotViet.OmniChannel.Services.LogginConfiguration
{
    public class LogObjectMicrosoftExtension
    {
        public LogObjectMicrosoftExtension(ILogger logger, Guid id)
        {
            Id = id;
            Timer = Stopwatch.StartNew();
            StartTime = DateTime.Now.ToString("o");
            Logger = logger;
        }

        public List<Guid> ParentIds { get; set; }
        public Guid Id { get; set; }
        public string Action { get; set; }

        private Stopwatch Timer { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public long? TotalTime { get; set; }
        public long Duration { get; set; }
        public int? RetailerId { get; set; }
        public int? BranchId { get; set; }
        public string RetailerCode { get; set; }
        public string Description { get; set; }
        public long? OmniChannelId { get; set; }
        public object RequestObject { get; set; }
        public object ResponseObject { get; set; }
        public long? TotalItem { get; set; }
        private ILogger Logger { get; set; }
        public string CustomerCentricType { get; set; }
        public byte ChannelType { get; set; }
        public string ChannelTypeCode { get; set; }
        public string ShopId { get; set; }

        public void LogInfo(bool isLogTotalTime = false)
        {
            Timer.Stop();
            EndTime = DateTime.Now.ToString("o");
            if (isLogTotalTime == true)
            {
                TotalTime = Timer.ElapsedMilliseconds;
            }
            else
            {
                Duration = Timer.ElapsedMilliseconds;
            }

            if (RequestObject != null && !(RequestObject is string))
            {
                RequestObject = RequestObject.ToSafeJson();
            }

            if (ResponseObject != null && !(ResponseObject is string))
            {
                ResponseObject = ResponseObject.ToSafeJson();
            }

            Logger.LogInformation(this.ToSafeJson());
        }

        public void LogInfo(string message, bool isLogTotalTime = false)
        {
            Timer.Stop();
            EndTime = DateTime.Now.ToString("o");
            RequestObject = message;
            if (isLogTotalTime)
            {
                TotalTime = Timer.ElapsedMilliseconds;
            }
            else
            {
                Duration = Timer.ElapsedMilliseconds;
            }

            if (RequestObject != null && !(RequestObject is string))
            {
                RequestObject = RequestObject.ToSafeJson();
            }

            if (ResponseObject != null && !(ResponseObject is string))
            {
                ResponseObject = ResponseObject.ToSafeJson();
            }

            Logger.LogInformation(this.ToSafeJson());
        }

        public void LogError(Exception ex, bool isLogTotalTime = false, bool isValidate = false)
        {
            Timer.Stop();
            EndTime = DateTime.Now.ToString("o");
            if (isLogTotalTime == true)
            {
                TotalTime = Timer.ElapsedMilliseconds;
            }
            else
            {
                Duration = Timer.ElapsedMilliseconds;
            }

            if (RequestObject != null && !(RequestObject is string))
            {
                RequestObject = RequestObject.ToSafeJson();
            }

            if (ResponseObject != null && !(ResponseObject is string))
            {
                ResponseObject = ResponseObject.ToSafeJson();
            }

            Description = string.IsNullOrEmpty(Description) ? ex.Message : Description;

            using (LogContext.PushProperty("ExceptionType", Action))
            using (LogContext.PushProperty(nameof(RetailerId), RetailerId))
            using (LogContext.PushProperty(nameof(RetailerCode), RetailerCode))
            using (LogContext.PushProperty(nameof(BranchId), BranchId))
            using (LogContext.PushProperty(nameof(OmniChannelId), OmniChannelId))
            using (LogContext.PushProperty(nameof(ChannelType), ChannelType))
            using (LogContext.PushProperty(nameof(ChannelTypeCode), ChannelTypeCode))
            using (LogContext.PushProperty(nameof(ShopId), ShopId))
            {
                Logger.LogError(ex, this.ToJson());
            }
        }
    }
}

﻿using Demo.OmniChannel.ChannelClient.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Demo.OmniChannel.Services.LogginConfiguration
{
    public interface IMicrosoftLoggingContextExtension
    {
        void SetMicrosoftLogging(LogObjectMicrosoftExtension logContext);

        LogObjectMicrosoftExtension LogMicrosoftExtension { get; }
    }
}

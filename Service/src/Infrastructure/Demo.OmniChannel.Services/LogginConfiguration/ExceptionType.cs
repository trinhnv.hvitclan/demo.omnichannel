﻿namespace Demo.OmniChannel.Services.LogginConfiguration
{
    public static class ExceptionType
    {
        public const string MultiSyncOnHand = "MultiSyncOnHand";
        public const string SyncOnHand = "SyncOnHand";
        public const string SyncOnHandUseMultiMappingSku = "SyncOnHandUseMultiMappingSKU";
        public const string TokenExpired = "TokenExpired";
        public const string MappingProduct = "MappingProduct";
        public const string RemoveMappingBySyncOnHand = "RemoveMappingBySyncOnHand";
        public const string GetProductBranchesWithRetry = "GetProductBranchesWithRetry";
        public const string GetChannelProduct = "GetChannelProduct";
        public const string SyncOnHandTraceProductBranch = "SyncOnHandTraceProductBranch";
        public const string SyncMultiOnHandGetProduct = "SyncMultiOnHandGetProduct";
        public const string SyncMultiOnHandGetProductWithStock = "SyncMultiOnHandGetProductWithStock";
        public const string SyncMultiOnHandPushMessagePage= "SyncMultiOnHandPushMessagePage {0}";
        public const string SyncMultiOnHandGetOnHand = "SyncMultiOnHandGetOnHand";
        public const string SyncMultiOnHandPushKafka = "SyncMultiOnHandPushKafka";
        public const string RetryErrorOrders = "RetryErrorOrders";
        public const string SyncOrderCustomerCentric = "SyncOrderCustomerCentric";
        public const string MultiSyncPrice = "MultiSyncPrice";
        public const string SyncPrice = "SyncPrice";
        public const string RemoveMappingBySyncPrice = "RemoveMappingBySyncPrice";

        #region Invoice
        public const string GetDetailInvoice = "InvoiceGetDetail";
        public const string GetCustomerFromCoreApi = "InvoiceGetCustomerFromCoreApi";
        public const string CreateInvoiceUpdateUpdateDeliveryPackage = "InvoiceCreateUpdateUpdateDeliveryPackage";
        public const string InvoiceGetPriceBook = "InvoiceGetPriceBook";
        public const string CreateInvoice = "InvoiceCreate";
        public const string CreateInitInvoice = "InvoiceInitBeforeCreate";
        public const string SaveSyncInvoiceError = "InvoiceSaveSyncError";
        public const string RemoveSyncInvoiceError = "InvoiceRemoveSyncError";
        public const string InvoiceMappingProduct = "InvoiceMappingProduct";
        public const string InvoiceVoid = "InvoiceVoid";
        public const string InvoiceVoidOrder = "InvoiceVoidOrder";
        public const string InvoiceGetEscrowDetail = "InvoiceGetEscrowDetail";
        public const string CreateInvoiceSuccess = "CreateInvoiceSuccess";

        #endregion

        #region Order
        public const string CreateOrder = "CreateOrderV2";
        public const string CreateCustomer = "CreateCustomerV2";
        public const string CreateCustomerError = "CreateCustomerErrorV2";
        public const string CreatePartnerAndGetAgain = "CreatePartnerAndGetAgainV2";
        public const string CreatePartner = "CreatePartnerV2";
        public const string CreateOrderNotSuccess = "CreateOrderNotSuccessV2";
        public const string CreateOrderSuccess = "CreateOrderSuccessV2";
        public const string TrackingOrderProcessEngine = "TrackingTimeOrderCreatedProcess";

        #endregion

        #region Payment

        public const string SyncPaymentTransaction = "SyncPaymentTransaction";
        public const string SyncLZDFinanceTransaction = "SyncLZDFinanceTransaction";
        public const string SyncLZDPaymentTransaction = "SyncLZDPaymentTransaction";
        public const string SyncLZDTimeoutFinanceTransaction = "SyncLZDTimeoutFinanceTransaction";
        public const string LazadaProcessFeeTransaction = "LazadaProcessFeeTransaction";

        public const string TiktokFinanceTransaction = "TiktokFinanceTransaction";

        #endregion
    }
}

﻿
using AutoMapper;
using Demo.OmniChannel.Services.AutoMapperConfigurations.Profiles;

namespace Demo.OmniChannel.Services.AutoMapperConfigurations
{
    public class AutoMapping
    {
        protected AutoMapping(){}
        public static MapperConfiguration RegisterMappings()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingOmniChannelSettingToDto());
                cfg.AddProfile(new MappingOmniChannelAuthToDto());
                cfg.AddProfile(new MappingOmniChannelScheduleToDto());
                cfg.AddProfile(new MappingOmniChannelWareHouseToDto());
                cfg.AddProfile(new MappingOmniChannelToDto());
                cfg.AddProfile(new MappingProductDataItemToShopeeItemDto());
                cfg.AddProfile(new MappingProductDataItemToShopeeItemDto());
                cfg.AddProfile(new MappingKvRetailerToDto());
                cfg.AddProfile(new MappingPosParameterToDto());
            });
        }
    }
}

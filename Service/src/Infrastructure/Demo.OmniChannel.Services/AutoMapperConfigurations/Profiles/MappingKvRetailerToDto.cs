﻿using AutoMapper;
using Demo.OmniChannel.Domain.ResultModel;
using Demo.OmniChannel.Domain.Model;
using System;
using Demo.OmniChannel.Utilities;

namespace Demo.OmniChannel.Services.AutoMapperConfigurations.Profiles
{
    public class MappingKvRetailerToDto : Profile
    {
        public MappingKvRetailerToDto()
        {
            CreateMap<KvRetailer, KvRetailerDto>()
                .ConstructUsing(src => new KvRetailerDto
                {
                    Id = src.Id,
                    CompanyName = src.CompanyName,
                    CompanyAddress = src.CompanyAddress,
                    Website = src.Website,
                    Phone = src.Phone,
                    Fax = src.Fax,
                    Code = src.Code,
                    LogoUrl = src.LogoUrl,
                    IsActive = src.IsActive,
                    IsAdminActive = src.IsAdminActive,
                    GroupId = src.GroupId,
                    IndustryId = src.IndustryId,
                    ContractType = src.ContractType,
                    MaximumSaleChannels = src.MaximumSaleChannels
                })
                .ForMember(dest => dest.ContractDate,
                    opt => opt.MapFrom(src => src.ContractDate != null ? DateHelper.DateTimeToUnixTimestamp((DateTime)src.ContractDate) : 0))
                .ForMember(dest => dest.ExpiryDate,
                    opt => opt.MapFrom(src => src.ExpiryDate != null ? DateHelper.DateTimeToUnixTimestamp((DateTime)src.ExpiryDate) : 0));
        }
    }
}

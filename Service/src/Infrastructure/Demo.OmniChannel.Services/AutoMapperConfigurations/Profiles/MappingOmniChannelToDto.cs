﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Dto;

namespace Demo.OmniChannel.Services.AutoMapperConfigurations.Profiles
{
    public class MappingOmniChannelToDto : Profile
    {
        public MappingOmniChannelToDto()
        {
            CreateMap<Domain.Model.OmniChannel, OmniChannelDto>()
                .ForMember(dest => dest.OmniChannelAuth, opt => opt.MapFrom(src => src.OmniChannelAuth))
                .ForMember(dest => dest.OmniChannelSchedules, opt => opt.MapFrom(src => src.OmniChannelSchedules))
                .ForMember(dest => dest.OmniChannelSettings, opt => opt.MapFrom(b => ConvertListToObject(b.OmniChannelSettings)))
                .ForMember(dest => dest.OmniChannelWareHouses, opt => opt.MapFrom(b => b.OmniChannelWareHouses));

            CreateMap<OmniChannelDto, Domain.Model.OmniChannel>()
                .ForMember(d => d.OmniChannelSettings, opt => opt.MapFrom<List<OmniChannelSetting>>(s => null));
        }

        private OmniChannelSettingDto ConvertListToObject(List<OmniChannelSetting> omniChannelSettings)
        {
            var omniChannelSettingObject = new OmniChannelSettingObject
            {
                Data = omniChannelSettings?.ToDictionary(x => x.Name, x => x.Value)
            };

            return omniChannelSettingObject.ToObject<OmniChannelSettingDto>();
        }
    }
}

﻿
using AutoMapper;
using Demo.OmniChannel.ShareKernel.Dto;

namespace Demo.OmniChannel.Services.AutoMapperConfigurations.Profiles
{
    public class MappingOmniChannelScheduleToDto : Profile
    {
        public MappingOmniChannelScheduleToDto()
        {
            CreateMap<Domain.Model.OmniChannelSchedule, OmniChannelScheduleDto>();
            CreateMap<OmniChannelScheduleDto, Domain.Model.OmniChannelSchedule>().ReverseMap();
        }
    }
}

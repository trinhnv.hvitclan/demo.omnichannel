﻿using AutoMapper;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Domain.ResultModel;
using Demo.OmniChannel.Utilities;
using System;

namespace Demo.OmniChannel.Services.AutoMapperConfigurations.Profiles
{
    public class MappingPosParameterToDto : Profile
    {
        public MappingPosParameterToDto()
        {
            CreateMap<PosParameter, PosParameterDto>()
                .ConstructUsing(src => new PosParameterDto
                {
                    Id = src.Id,
                    Key = src.Key,
                    Value = src.Value,
                    RetailerId = src.RetailerId,
                    isActive = src.isActive,
                    ParameterType = src.ParameterType,
                    BlockUnit = src.BlockUnit
                })
                .ForMember(dest => dest.CreatedDate,
                    opt => opt.MapFrom(src => DateHelper.DateTimeToUnixTimestamp(src.CreatedDate)))
                .ForMember(dest => dest.ExpiredDate,
                    opt => opt.MapFrom(src => src.ExpiredDate != null ? DateHelper.DateTimeToUnixTimestamp((DateTime)src.ExpiredDate) : 0))
                .ForMember(dest => dest.StartTrialDate,
                    opt => opt.MapFrom(src => src.StartTrialDate != null ? DateHelper.DateTimeToUnixTimestamp((DateTime)src.StartTrialDate) : 0));
        }
    }
}

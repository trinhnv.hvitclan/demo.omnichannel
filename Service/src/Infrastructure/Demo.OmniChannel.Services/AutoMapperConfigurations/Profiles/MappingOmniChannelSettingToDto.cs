﻿
using AutoMapper;
using Demo.OmniChannel.ShareKernel.Dto;

namespace Demo.OmniChannel.Services.AutoMapperConfigurations.Profiles
{
    public class MappingOmniChannelSettingToDto : Profile
    {
        public MappingOmniChannelSettingToDto()
        {
            CreateMap<Domain.Model.OmniChannelSetting, OmniChannelSettingDto>();
            CreateMap<Domain.Model.OmniChannelSetting, OmniChannelSettingDto>().ReverseMap();
        }
    }
}

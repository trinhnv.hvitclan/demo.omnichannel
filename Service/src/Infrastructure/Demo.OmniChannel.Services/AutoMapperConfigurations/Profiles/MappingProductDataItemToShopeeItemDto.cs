﻿using System;
using AutoMapper;
using Demo.OmniChannel.ShareKernel.Dtos;
using Demo.OmniChannel.Utilities;

namespace Demo.OmniChannel.Services.AutoMapperConfigurations.Profiles
{
    public class MappingProductDataItemToShopeeItemDto : Profile
    {
        public MappingProductDataItemToShopeeItemDto()
        {
            CreateMap<ProductDataPushChannel, ShopeeProduct>()
                .ConstructUsing(src => new ShopeeProduct
                {
                    CategoryId = src.CategoryId,
                    Name = src.Name,
                    Description = src.Description,
                    Attributes = src.Attributes,
                    Logistics = src.Logistics,
                    Price = src.Price,
                    Stock = src.Stock ?? 0,
                    Weight = src.Weight,
                    Images = src.Images,
                    ShopId = src.ShopId
                })
                .ForMember(dest => dest.TimeStamp,
                    opt => opt.MapFrom(src => DateHelper.DateTimeToUnixTimestamp(DateTime.UtcNow)))
                .ForMember(dest => dest.Variations, opt => opt.MapFrom(src => src.Variations));
        }

    }
}

﻿
using AutoMapper;
using Demo.OmniChannel.ShareKernel.Dto;

namespace Demo.OmniChannel.Services.AutoMapperConfigurations.Profiles
{
    public class MappingOmniChannelAuthToDto : Profile
    {
        public MappingOmniChannelAuthToDto()
        {
            CreateMap<Domain.Model.OmniChannelAuth, OmniChannelAuthDto>();
            CreateMap<Domain.Model.OmniChannelAuth, OmniChannelAuthDto>().ReverseMap();
        }
    }
}

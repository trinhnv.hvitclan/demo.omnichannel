﻿using AutoMapper;
using Demo.OmniChannel.ShareKernel.Dto;

namespace Demo.OmniChannel.Services.AutoMapperConfigurations.Profiles
{
    public class MappingOmniChannelWareHouseToDto : Profile
    {
        public MappingOmniChannelWareHouseToDto()
        {
            CreateMap<Domain.Model.OmniChannelWareHouse, OmniChannelWareHouseDto>();
            CreateMap<OmniChannelWareHouseDto, Domain.Model.OmniChannelWareHouse>().ReverseMap();
        }
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.ChannelClient.Models;

namespace Demo.OmniChannel.Services.Interfaces
{
    public interface IOmniChannelSettingService
    {
        Task<OmniChannelSettingObject> GetChannelSettings(long channelId, int retailerId);
        Task MarkChannelSentNotificationExpired(bool isMark, int retailerId, List<Domain.Model.OmniChannel> channels);
    }
}
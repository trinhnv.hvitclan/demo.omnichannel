﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.ChannelClient.Models;

namespace Demo.OmniChannel.Services.Interfaces
{
    public interface IOmniChannelPlatformService
    {
        Task<Platform> GetById(long id);

        Task<List<Platform>> GetByIds(List<long> ids);

        Task<Platform> GetCurrent(int platformType, int? appSupport = null);
    }
}
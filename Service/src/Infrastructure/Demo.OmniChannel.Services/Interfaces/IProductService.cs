using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.Domain.Model;

namespace Demo.OmniChannel.Services.Interfaces
{
    public interface IProductService
    {
        Task<List<Product>> GetProducts(InternalProduct internalProduct);
        Task<List<Product>> GetProductByMasterIds(int retailerId, List<long> kvProductIds);
    }
}
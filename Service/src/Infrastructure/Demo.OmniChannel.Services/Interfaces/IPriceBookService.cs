﻿using Demo.OmniChannel.Api.ServiceModel;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;
using System.Collections.Generic;

namespace Demo.OmniChannel.Services.Interfaces
{
    public interface IPriceBookService
    {
        Task<List<PriceBook>> GetPriceBooks(InternalPriceBook internalPriceBook);
    }
}

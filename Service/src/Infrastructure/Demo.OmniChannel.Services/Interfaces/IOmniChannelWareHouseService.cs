﻿using Demo.OmniChannel.Domain.Model;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Services.Interfaces
{
    public interface IOmniChannelWareHouseService
    {
        Task<OmniChannelWareHouse> GetByOmniChannelId(long omniChannelId, long retailerId);
        Task<bool> InsertSync(OmniChannelWareHouse model);
        Task<bool> UpdateAsync(OmniChannelWareHouse model);
    }
}

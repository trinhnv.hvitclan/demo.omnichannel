﻿using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.ShareKernel.Common;
using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.Api.ServiceModel.Request;

namespace Demo.OmniChannel.Services.Interfaces
{
    public interface IOmniChannelService
    {
        Task<(Domain.Model.OmniChannel Channel, long ChannelIdDel)> CreateOrUpdateAsync(OmniChannelRequest channelRequest, bool isSyncOrder, bool isSyncOnHand, bool isSyncPrice, OmniChannelAuth auth);
        Task<List<Domain.Model.OmniChannel>> GetByRetailerAsync(int retailerId, byte? type = null, bool isIncludeInactive = true);
        Task<List<Domain.Model.OmniChannel>> GetByRetailerAsync(int retailerId, int branchId, byte? type = null, bool isIncludeInactive = true);
        Task<List<Domain.Model.OmniChannel>> GetByRetailerAsync(int retailerId, int branchId, byte? type, bool isIncludeInactive, bool isIncludeDeleted);
        Task<List<Domain.Model.OmniChannel>> GetByRetailerAsync(int retailerId, int branchId, List<int> types, bool isIncludeInactive, bool isIncludeDeleted);
        Task<Domain.Model.OmniChannel> GetByIdAsync(long id);
        Task<List<long>> GetChannelId(int retailerId, IList<int> branchIds, IList<ChannelType> channelTypes);
        Task<Domain.Model.OmniChannel> DeleteByIdAsync(long id);
        Task<List<long>> ResetSyncOnHandFormulaBySetting(int retailerId, string settingKey, bool otherSettingValue);
        Task<List<Domain.Model.OmniChannel>> ResetPriceBookSetting(int retailerId, long priceBookId);
        Task<Domain.Model.OmniChannel> DeactivateChannel(long channelId);
        Task<Domain.Model.OmniChannel> NeedRegisterChannel(long channelId);

        Task<bool> DisableChannelByType(long retailerId, byte channelType);
        Task<List<Domain.Model.OmniChannel>> GetByIdentityKey(byte channelType, string identityKey);
        Task<List<Domain.Model.OmniChannel>> GetByChannelIds(List<long> channelIds, int retailerId, bool isIncludeInactive = true, bool isLoadReference = false);
        Task<List<Domain.Model.OmniChannel>> GetByChannelIds(List<long> channelIds, bool isIncludeInactive = true, bool isLoadReference = false);
        Task ValidateDuplicateChannelSendo(int retailerId, string storeId, string shopKey, string channelName, long channelId);
        Task<int> UpdateAllAsync(List<Domain.Model.OmniChannel> channels);
        Task<List<Domain.Model.OmniChannel>> GetDeactiveByChannelIds(List<long> channelIds);


    }
}
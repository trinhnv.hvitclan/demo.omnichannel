﻿using Demo.OmniChannel.ChannelClient.Impls;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Services.Interfaces
{
    public interface IShopeeService
    {
        Task<List<ShopeeProductResponse>> PushProductShopeeChannel(ChannelType channelType, ShareKernel.Dtos.PushDataItemToChannelReq req, int retailerId, List<ProductAttribute> productAttributes = null, List<ShopeeModelImageUploadV2> modelImage = null);
    }
}

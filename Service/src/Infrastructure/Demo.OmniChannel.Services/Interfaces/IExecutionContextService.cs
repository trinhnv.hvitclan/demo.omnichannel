﻿using Demo.OmniChannel.ShareKernel.Auth;

namespace Demo.OmniChannel.Services.Interfaces
{
    public interface IExecutionContextService
    {
        void SetContext(ExecutionContext context);

        ExecutionContext Context { get; }
    }
}

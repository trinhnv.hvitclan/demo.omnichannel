using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.Domain.Model;

namespace Demo.OmniChannel.Services.Interfaces
{
    public interface IProductBranchService
    {
        Task<List<ProductBranch>> GetProductBranches(InternalProductBranch internalProductBranch);
    }
}
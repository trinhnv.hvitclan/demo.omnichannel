﻿using System;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;

namespace Demo.OmniChannel.Services.Interfaces
{
    public interface IOmniChannelAuthService
    {
        Task<ChannelClient.Models.TokenResponse> GetChannelAuth(Domain.Model.OmniChannel channel, Guid logId);

        Task<long> CreateOrUpdate(OmniChannelAuth channelAuthRequest);
    }
}
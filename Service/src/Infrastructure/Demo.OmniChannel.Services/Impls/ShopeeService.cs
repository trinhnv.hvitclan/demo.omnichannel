﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Impls;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee.V2;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Dtos;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.ShareKernel.Models;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using Image = Demo.OmniChannel.ShareKernel.Dtos.Image;

namespace Demo.OmniChannel.Services.Impls
{
    public class ShopeeService : IShopeeService
    {
        private readonly IMapper _mapper;
        private readonly IAppSettings _appSettings;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly IOmniChannelPlatformService _omniChannelPlatform;
        private readonly IOmniChannelAuthService _omniChannelAuthService;
        public ShopeeService(
            IMapper mapper,
            IAppSettings appSettings,
            IDbConnectionFactory dbConnectionFactory,
            IOmniChannelPlatformService omniChannelPlatform,
            IOmniChannelAuthService omniChannelAuthService)
        {
            _mapper = mapper;
            _appSettings = appSettings;
            _dbConnectionFactory = dbConnectionFactory;
            _omniChannelPlatform = omniChannelPlatform;
            _omniChannelAuthService = omniChannelAuthService;
        }

        public async Task<List<ShopeeProductResponse>> PushProductShopeeChannel(ChannelType channelType, PushDataItemToChannelReq req, int retailerId, List<ProductAttribute> productAttributes = null, List<ShopeeModelImageUploadV2> modelImage = null)
        {
            var client = new ShopeeClientV2(_appSettings);

            var responseResultList = new List<ShopeeProductResponse>();
            var shopIdUpload = req.PushDataItemToChannel.ShopIds.FirstOrDefault();
            var channels = await GetListChannel(req.PushDataItemToChannel.ShopIds, retailerId);
            var plat = await _omniChannelPlatform.GetById(channels
                .FirstOrDefault(x => x.IdentityKey == shopIdUpload && x.RetailerId == retailerId)
                ?.PlatformId ?? 0);
            List<ShopeeeUploadImageReponseV2> imgsUploaded;
            if (modelImage != null)
            {
                imgsUploaded = await client.UploadImgV2FromData(new ChannelAuth(), retailerId, Convert.ToInt64(shopIdUpload),
                    modelImage, Guid.NewGuid(), plat);
            }
            else
            {
                imgsUploaded = await client.UploadImgV2(new ChannelAuth(), retailerId, Convert.ToInt64(shopIdUpload),
                    req.PushDataItemToChannel.Images, Guid.NewGuid(), plat);
            }

            if (imgsUploaded == null || !imgsUploaded.Any()) throw new ShopeePushProductException("Ảnh sai định dạng hoặc kích thước");
            var images = new List<Image>();
            foreach (var image in imgsUploaded)
            {
                images.Add(new Image()
                {
                    ImageId = image.Response.ImageInfo.ImageId
                });
            }

            req.PushDataItemToChannel.Images = images;

            var tasks = new List<Task<ShopeeProductResponse>>();
            foreach (var shopItem in req.PushDataItemToChannel.ShopIds)
            {
                var shopId = Convert.ToInt64(shopItem);
                req.PushDataItemToChannel.ShopId = shopId;
                var productData = _mapper.Map<ProductDataPushChannel, ShopeeProduct>(req.PushDataItemToChannel);
                var channel = channels.FirstOrDefault(x => x.IdentityKey == shopItem);
                var auth = await _omniChannelAuthService.GetChannelAuth(channel, Guid.NewGuid());
                var platform = await _omniChannelPlatform.GetById(channels
                    .FirstOrDefault(x => x.IdentityKey == shopItem && x.RetailerId == retailerId)
                    ?.PlatformId ?? 0);
                tasks.Add(client.PushProductToChannel(retailerId, channel?.Id ?? 0, new ChannelAuth { ShopId = shopId, AccessToken = auth.AccessToken},
                    Guid.NewGuid(), productData,
                    platform, productAttributes.ToSafeJson()));
            }

            await Task.WhenAll(tasks);
            tasks.ForEach(task =>
            {
                responseResultList.Add(task.Result);
            });

            return responseResultList;
        }

        #region private method

        private async Task<List<Domain.Model.OmniChannel>> GetListChannel(List<string> shopIds, int retailerId)
        {
            using (var db = await _dbConnectionFactory.OpenAsync())
            {
                var omniChannelRepository = new OmniChannelRepository(db);

                var listOmniChannel =
                    await omniChannelRepository.GetChannelByShopIds(retailerId, shopIds, (byte)ChannelTypeEnum.Shopee);
                return listOmniChannel;
            }
        }
        #endregion
    }
}

﻿using Demo.OmniChannel.Domain.Common;
using Demo.OmniChannel.Services.Interfaces.Retail;
using Demo.OmniChannel.ShareKernel.Common;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.OrmLite;
using System;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Services.Impls.Retail
{
    public class BaseRetailService<T> : IBaseRetailService<T> where T : class
    {
        private ILog _logger;

        private readonly IDbConnectionFactory _dbConnectionFactory;

        private readonly ICacheClient _cacheClient;

        public BaseRetailService(ICacheClient cacheClient, IDbConnectionFactory dbConnectionFactory)
        {
            _cacheClient = cacheClient;
            _dbConnectionFactory = dbConnectionFactory;
        }

        public ILog Log
        {
            get
            {
                _logger = _logger ?? LogManager.GetLogger(GetType());
                return _logger;
            }
        }

        public async Task<KvInternalContext> CreateContext(int retailerId)
        {
            var retailer = await GetRetailer(retailerId);
            if (retailer == null) return null;
            var kvGroup = await GetRetailerGroup(retailer.GroupId);
            if (kvGroup == null) return null;

            var kvInternalContext = new KvInternalContext
            {
                RetailerCode = retailer.Code,
                RetailerId = retailer.Id,
                Group = new KvGroup
                {
                    Id = kvGroup?.Id ?? 0,
                    ConnectionString = kvGroup?.ConnectionString,
                    ConnectionStringName = kvGroup?.ConnectionString.Substring(kvGroup.ConnectionString.IndexOf('=') + 1)
                }
            };
            return kvInternalContext;
        }

        private async Task<Domain.Model.KvRetailer> GetRetailer(long retailerId)
        {
            var kvRetailer = _cacheClient.Get<Domain.Model.KvRetailer>(string.Format(KvConstant.RetailerIdCacheKey, retailerId));
            if (kvRetailer == null)
            {
                using (var dbMaster = await _dbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities))
                {
                    kvRetailer = await dbMaster.SingleAsync<Domain.Model.KvRetailer>(x => x.Id == retailerId);
                    if (kvRetailer != null)
                    {
                        _cacheClient.Set(string.Format(KvConstant.RetailerIdCacheKey, retailerId), kvRetailer, DateTime.Now.AddDays(7));
                    }
                }
            }
            return kvRetailer;
        }

        private async Task<KvGroup> GetRetailerGroup(int groupId)
        {
            var kvGroup = _cacheClient.Get<KvGroup>(string.Format(KvConstant.KvGroupCacheKey, groupId));
            if (kvGroup == null)
            {
                using (var dbMaster = await _dbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities))
                {
                    kvGroup = await dbMaster.SingleAsync<Domain.Common.KvGroup>(x => x.Id == groupId);
                    if (kvGroup != null)
                    {
                        _cacheClient.Set(string.Format(KvConstant.KvGroupCacheKey, groupId), kvGroup, DateTime.Now.AddDays(7));
                    }
                }
            }
            return kvGroup;
        }
    }
}

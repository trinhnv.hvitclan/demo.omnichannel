﻿using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.Services.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Services.Impls
{
    public class OmniChannelWareHouseService : BaseService<Domain.Model.OmniChannelWareHouse>, IOmniChannelWareHouseService
    {
        public IOmniChannelWareHouseRepository OmniChannelWareHouseRepository { get; set; }

        public async Task<OmniChannelWareHouse> GetByOmniChannelId(long omniChannelId, long retailerId)
        {
            return (await OmniChannelWareHouseRepository
                .WhereAsync(item => item.RetailerId == retailerId && item.OmniChannelId == omniChannelId && (item.IsDeleted == null || item.IsDeleted == false)))
                .FirstOrDefault();
        }

        public async Task<bool> InsertSync(OmniChannelWareHouse model)
        {
            await OmniChannelWareHouseRepository.AddAsync(model);
            return true;
        }

        public async Task<bool> UpdateAsync(OmniChannelWareHouse model)
        {
            await OmniChannelWareHouseRepository.UpdateAsync(model);
            return true;
        }
    }
}

﻿using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Services.Impls.Retail;
using Demo.OmniChannel.Services.Interfaces;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Services.Impls
{
    public class PriceBookService : BaseRetailService<PriceBook>, IPriceBookService
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;

        public PriceBookService(ICacheClient cacheClient, IDbConnectionFactory dbConnectionFactory) 
            : base(cacheClient, dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public async Task<List<PriceBook>> GetPriceBooks(InternalPriceBook internalPriceBook)
        {
            var context = await CreateContext(internalPriceBook.RetailerId);
            using (var db = await _dbConnectionFactory.OpenAsync(context.Group.ConnectionStringName.ToLower()))
            {
                var priceBookRepository = new PriceBookRepository(db);
                return await priceBookRepository.GetByIds(internalPriceBook.RetailerId, internalPriceBook.PriceBookIds);
            }
        }
    }
}

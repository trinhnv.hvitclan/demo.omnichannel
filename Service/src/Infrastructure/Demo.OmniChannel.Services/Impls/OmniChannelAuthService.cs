﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.OrmLite;
using ServiceStack.Data;
using Demo.OmniChannel.ShareKernel.Auth;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.Repository.Impls;
using ServiceStack.Caching;
using Demo.OmniChannel.ShareKernel.Exceptions;

namespace Demo.OmniChannel.Services.Impls
{
    public class OmniChannelAuthService : BaseService<OmniChannelAuth>, IOmniChannelAuthService
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly IAppSettings _settings;
        private readonly ICacheClient _cacheClient;

        public OmniChannelAuthService(IDbConnectionFactory dbConnectionFactory,
            IAppSettings settings,
            ICacheClient cacheClient)
        {
            _dbConnectionFactory = dbConnectionFactory;
            _settings = settings;
            _cacheClient = cacheClient;
        }

        public async Task<ChannelClient.Models.TokenResponse> GetChannelAuth(Domain.Model.OmniChannel channel, Guid logId)
        {
            var lockGetAccessTokenKey = string.Format(KvConstant.LockGetAccessTokenChannelId, channel.Id);
            int delayGetAccessTokenCount = 0;
            while (_cacheClient.Get<bool>(lockGetAccessTokenKey) && delayGetAccessTokenCount < 10)
            {
                await Task.Delay(6000);
                delayGetAccessTokenCount++;
            }

            var lockSetAuthKey = KvConstant.LockSetCacheAuthKey.Fmt(channel.RetailerId, channel.Id);
            var delayGetAuthCount = 0;
            while (_cacheClient.Get<bool>(lockSetAuthKey) && delayGetAuthCount < 10)
            {
                Log.Info($"lockSetAuthKey: {lockSetAuthKey}");
                await Task.Delay(1000);
                delayGetAuthCount++;
            }

            var authCacheKey = string.Format(KvConstant.ChannelAuthKey, channel.Id);
            var authKey = _cacheClient.Get<ChannelAuth>(authCacheKey);
            if (authKey == null || authKey.RetailerId == 0 || authKey.ChannelId == 0)
            {
                authKey = await SetCache(channel.RetailerId, channel.Id, logId, authCacheKey);
                if (authKey == null) return null;
            }
            else
            {
                if (authKey.RetailerId != channel.RetailerId || authKey.ChannelId != channel.Id)
                {
                    var exMessage = $"Cache ChannelAuth conflict (RetailerId cache = {authKey.RetailerId} request = {channel.RetailerId}. ChannelId cache = {authKey.ChannelId} request = {channel.Id}.";
                    Log.Error($"{nameof(KvOmniChannelDataConflictException)} error: {exMessage} - Context: {new ExceptionContextModel { LogId = logId }.ToJson()}", new KvOmniChannelDataConflictException(exMessage));
                    return null;
                }

                if (!string.IsNullOrEmpty(authKey.AccessToken))
                {
                    authKey.AccessToken = CryptoHelper.RijndaelDecrypt(authKey.AccessToken);
                }

                if (!string.IsNullOrEmpty(authKey.RefreshToken))
                {
                    authKey.RefreshToken = CryptoHelper.RijndaelDecrypt(authKey.RefreshToken);
                }
            }

            return new ChannelClient.Models.TokenResponse { AccessToken = authKey.AccessToken, RefreshExpiresIn = authKey.RefreshExpiresIn, ExpiresIn = authKey.ExpiresIn, ShopId = authKey.ShopId, Id = authKey.Id };
        }

        async Task<ChannelAuth> SetCache(int retailerId, long channelId, Guid logId, string authCacheKey)
        {
            var cacheKeyLock = KvConstant.LockSetCacheAuthKey.Fmt(retailerId, channelId);
            _cacheClient.Set(cacheKeyLock, true, DateTime.Now.AddMilliseconds(5000));

            using (var db = _dbConnectionFactory.Open())
            {
                var channelRepo = new OmniChannelRepository(db);
                var channelAuthRepository = new OmniChannelAuthRepository(db);

                var channelInDb = await channelRepo.GetByIdAsync(channelId);
                if (channelInDb == null)
                {
                    Log.Error(new KvOmniChannelAuthException($"[LogId = {logId:N}] GetChannelInfo Channel:{channelId} RetailerId:{retailerId} Empty Channel"));
                    _cacheClient.Remove(cacheKeyLock);
                    return null;
                }

                if (channelInDb.RetailerId != retailerId)
                {
                    Log.Error(new KvOmniChannelDataConflictException($"[LogId = {logId:N}] Retailer of Omni Channel Id {channelId} conflict (RetailerId request = {retailerId} Db = {channelInDb.RetailerId})"));
                    _cacheClient.Remove(cacheKeyLock);
                    return null;
                }

                var channelAuth = (await channelAuthRepository.WhereAsync(x => x.OmniChannelId == channelId)).FirstOrDefault();
                if (channelAuth == null)
                {
                    Log.Error(new KvOmniChannelAuthException($"[LogId = {logId:N}] GetAuthInfo Channel:{channelId} RetailerId:{retailerId} Empty ChannelAuth"));
                    _cacheClient.Remove(cacheKeyLock);
                    return null;
                }

                var valueCache = channelAuth.ConvertTo<ChannelAuth>();
                valueCache.RetailerId = retailerId;
                valueCache.ChannelId = channelId;

                _cacheClient.Set(authCacheKey, valueCache, TimeSpan.FromMinutes(NumberHelper.GetValueOrDefault(_settings?.Get<int>("ChannelAuthExpired"), 10)));
                _cacheClient.Remove(cacheKeyLock);

                return new ChannelAuth
                {
                    AccessToken = !string.IsNullOrEmpty(channelAuth.AccessToken)
                        ? CryptoHelper.RijndaelDecrypt(channelAuth.AccessToken)
                        : string.Empty,
                    RefreshToken = !string.IsNullOrEmpty(channelAuth.RefreshToken)
                        ? CryptoHelper.RijndaelDecrypt(channelAuth.RefreshToken)
                        : string.Empty,
                    ExpiresIn = channelAuth.ExpiresIn,
                    RefreshExpiresIn = channelAuth.RefreshExpiresIn,
                    ShopId = channelAuth.ShopId,
                    CreateDate = channelAuth.CreatedDate,
                    ModifiedDate = channelAuth.ModifiedDate,
                    Id = channelAuth.Id,
                    ChannelId = valueCache.ChannelId,
                    RetailerId = valueCache.RetailerId
                };
            }
        }

        public async Task<long> CreateOrUpdate(OmniChannelAuth channelAuthRequest)
        {
            var auth = new OmniChannelAuth
            {
                ShopId = channelAuthRequest.ShopId,
                ExpiresIn = channelAuthRequest.ExpiresIn,
                RefreshExpiresIn = channelAuthRequest.RefreshExpiresIn,
                OmniChannelId = channelAuthRequest.OmniChannelId
            };

            if (!string.IsNullOrEmpty(channelAuthRequest.AccessToken))
            {
                auth.AccessToken = CryptoHelper.RijndaelEncrypt(channelAuthRequest.AccessToken);
            }

            if (!string.IsNullOrEmpty(channelAuthRequest.RefreshToken))
            {
                auth.RefreshToken = CryptoHelper.RijndaelEncrypt(channelAuthRequest.RefreshToken);
            }

            if (channelAuthRequest.ExpiresIn > 0)
            {
                auth.ExpiryDate = DateTime.Now.AddSeconds(channelAuthRequest.ExpiresIn - 60);
            }
            else
            {
                var shopeeTokenExpired = _settings.Get<int>("ShopeeTokenExpired");
                if (shopeeTokenExpired > 0)
                {
                    auth.ExpiryDate = DateTime.Now.AddYears(shopeeTokenExpired);
                }
            }

            _cacheClient.Remove(string.Format(KvConstant.ChannelAuthKey, channelAuthRequest.OmniChannelId));

            using (var db = await _dbConnectionFactory.OpenAsync())
            {
                var omniChannelAuthRepository = new OmniChannelAuthRepository(db);
                if (channelAuthRequest.Id <= 0)
                {
                    var authId =  await omniChannelAuthRepository.AddAsync(auth);
                    await UpdateAllShopId(omniChannelAuthRepository, auth, authId);
                    return authId;
                }

                var existAuth = await omniChannelAuthRepository.GetByIdAsync(channelAuthRequest.Id);
                existAuth.AccessToken = auth.AccessToken;
                existAuth.RefreshToken = auth.RefreshToken;
                existAuth.ExpiresIn = auth.ExpiresIn;
                existAuth.RefreshExpiresIn = auth.RefreshExpiresIn;
                existAuth.ShopId = auth.ShopId;
                existAuth.ExpiryDate = auth.ExpiryDate;
                await omniChannelAuthRepository.UpdateAsync(existAuth);
                return existAuth.Id;
            }
        }

        public async Task UpdateAllShopId(OmniChannelAuthRepository omniChannelAuthRepository,
            OmniChannelAuth request, long id)
        {
            var auths = await omniChannelAuthRepository.WhereAsync(x => x.ShopId == request.ShopId && x.Id != id && x.OmniChannelId != null);
            foreach (var item in auths)
            {
                item.AccessToken = request.AccessToken;
                item.RefreshToken = request.RefreshToken;
                item.ExpiresIn = request.ExpiresIn;
                item.RefreshExpiresIn = request.RefreshExpiresIn;
                item.ShopId = request.ShopId;
                item.ExpiryDate = request.ExpiryDate;
                await omniChannelAuthRepository.UpdateAsync(item);
            }
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Services.Interfaces;
using ServiceStack.Data;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Services.Impls
{
    public class OmniChannelSettingService : BaseService<OmniChannelSetting>, IOmniChannelSettingService
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;
        public OmniChannelSettingService(
            IDbConnectionFactory dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public async Task<OmniChannelSettingObject> GetChannelSettings(long channelId, int retailerId)
        {
            using (var db = await _dbConnectionFactory.OpenAsync())
            {
                var omniChannelSettingRepository = new OmniChannelSettingRepository(db);

                var settings =
                    await omniChannelSettingRepository.GetChannelSettings(channelId, retailerId);

                var settingObject = new OmniChannelSettingObject
                {
                    Data = settings?.ToDictionary(x => x.Name, x => x.Value)
                };
                return settingObject;
            }
        }

        public async Task MarkChannelSentNotificationExpired(bool isMark, int retailerId, List<Domain.Model.OmniChannel> channels)
        {
            using (var db = await _dbConnectionFactory.OpenAsync())
            {
                var omniChannelSettingRepository = new OmniChannelSettingRepository(db);
                await omniChannelSettingRepository.MarkChannelSentNotificationExpired(isMark, retailerId, channels);
            }
        }

    }
}

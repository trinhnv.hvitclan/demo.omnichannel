﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.Utilities;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using Platform = Demo.OmniChannel.ChannelClient.Models.Platform;

namespace Demo.OmniChannel.Services.Impls
{
    public class OmniChannelPlatformService : BaseService<OmniChannelPlatform>,
        IOmniChannelPlatformService
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly ICacheClient _cacheClient;
        public OmniChannelPlatformService(
            IDbConnectionFactory dbConnectionFactory,
            ICacheClient cacheClient
            )
        {
            _dbConnectionFactory = dbConnectionFactory;
            _cacheClient = cacheClient;
        }
        /// <summary>
        /// Lấy danh sách app theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Platform> GetById(long id)
        {
            using (var db = await _dbConnectionFactory.OpenAsync())
            {
                if (id == 0) return null;
                var cacheKey = string.Format(KvConstant.CachePlatform, id);
                var omniChannelPlatformRepository = new OmniChannelPlatformRepository(db);
                var platform =
                    await ((IKvCacheClient)_cacheClient).GetAndSetAsync(cacheKey,
                        key => omniChannelPlatformRepository.GetById(id), TimeSpan.FromDays(1));
                if (platform == null) return null;

                return new Platform
                {
                    Data = platform.Data
                };
            }
        }

        /// <summary>
        /// Lấy danh sách app theo ids
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<List<Platform>> GetByIds(List<long> ids)
        {
            using (var db = await _dbConnectionFactory.OpenAsync())
            {
                if (!ids.Any()) return null;
                var omniChannelPlatformRepository = new OmniChannelPlatformRepository(db);
                var platforms = await omniChannelPlatformRepository.GetByIds(ids);
                var platformsClient = platforms.Select(x => new Platform
                {
                    Id = x.Id,
                    Data = x.Data
                }).ToList();
                return platformsClient;
            }
        }

        /// <summary>
        /// Lấy danh sách app đang sử dụng
        /// </summary>
        /// <param name="platformType"></param>  Lazada = 1, Shopee = 2, Tiki = 3, Sendo = 4, Tiktok = 5
        /// <returns></returns>
        public async Task<Platform> GetCurrent(int platformType, int? appSupport = null)
        {
            using (var db = await _dbConnectionFactory.OpenAsync())
            {
                var cacheKey = string.Format(KvConstant.CachePlatformCurrentType, platformType, NumberHelper.GetValueOrDefault(appSupport, 0));
                var omniChannelPlatformRepository = new OmniChannelPlatformRepository(db);
                var platform =
                    await ((IKvCacheClient)_cacheClient).GetAndSetAsync(cacheKey, (key) => omniChannelPlatformRepository.GetCurrent(platformType, appSupport), TimeSpan.FromDays(1));

                if (platform == null) return null;

                return new Platform
                {
                    Id = platform.Id,
                    Data = platform.Data
                };
            }
        }
    }
}
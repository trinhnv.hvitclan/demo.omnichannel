﻿using ServiceStack.Logging;

namespace Demo.OmniChannel.Services
{
    public class BaseService<T> : IBaseService<T> where T : class
    {
        private ILog _logger;
        public ILog Log
        {
            get
            {
                _logger = _logger ?? LogManager.GetLogger(GetType());
                return _logger;
            }
        }
        //protected ExecutionContext Context;
    }
}
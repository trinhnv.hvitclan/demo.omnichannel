using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Services.Impls.Retail;
using Demo.OmniChannel.Services.Interfaces;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Services.Impls
{
    public class ProductService: BaseRetailService<Product>, IProductService
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;

        public ProductService(ICacheClient cacheClient, IDbConnectionFactory dbConnectionFactory) 
            : base(cacheClient, dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public async Task<List<Product>> GetProducts(InternalProduct internalProduct)
        {
            var context = await CreateContext(internalProduct.RetailerId);
            using (var db = await _dbConnectionFactory.OpenAsync(context.Group.ConnectionStringName.ToLower()))
            {
                var productRepository = new ProductRepository(db);
                return await productRepository.WhereAsync(p =>
                    p.RetailerId == internalProduct.RetailerId && (p.isDeleted == null || p.isDeleted == false) &&
                    p.ProductType == internalProduct.ProductType && internalProduct.ProductIds.Contains(p.Id));
            }
        }
        public async Task<List<Product>> GetProductByMasterIds(int retailerId, List<long> kvProductIds)
        {
            var context = await CreateContext(retailerId);
            using (var db = await _dbConnectionFactory.OpenAsync(context.Group.ConnectionStringName.ToLower()))
            {
                var productRepository = new ProductRepository(db);
                return await productRepository.GetProductByMasterIds(retailerId,kvProductIds);
            }
        }

    }
}
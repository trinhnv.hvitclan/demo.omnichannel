using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Services.Impls.Retail;
using Demo.OmniChannel.Services.Interfaces;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Services.Impls
{
    public class ProductBranchService : BaseRetailService<ProductBranch>, IProductBranchService
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;

        public ProductBranchService(ICacheClient cacheClient, IDbConnectionFactory dbConnectionFactory) 
            : base(cacheClient, dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public async Task<List<ProductBranch>> GetProductBranches(InternalProductBranch internalProductBranch)
        {
            var context = await CreateContext(internalProductBranch.RetailerId);
            using (var db = await _dbConnectionFactory.OpenAsync(context.Group.ConnectionStringName.ToLower()))
            {
                var productBranchRepository = new ProductBranchRepository(db);
                return await productBranchRepository.GetProduct(internalProductBranch.RetailerId, 
                    internalProductBranch.BranchId, internalProductBranch.ProductIds);
            }
        }
    }
}
﻿using Demo.OmniChannel.Domain.Common;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.Resource;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.Utilities;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OmniChannelSchedule = Demo.OmniChannel.Domain.Model.OmniChannelSchedule;
using Demo.OmniChannel.Api.ServiceModel.Request;

namespace Demo.OmniChannel.Services.Impls
{
    public class OmniChannelService : BaseService<Domain.Model.OmniChannel>, IOmniChannelService
    {
        public IOmniChannelRepository OmniChannelRepository { get; set; }
        public IOmniChannelScheduleRepository OmniChannelScheduleRepository { get; set; }
        public IOmniChannelSettingRepository OmniChannelSettingRepository { get; set; }
        public IOmniChannelAuthRepository OmniChannelAuthRepository { get; set; }
        public ICacheClient CacheClient { get; set; }
        public IAppSettings AppSettings { get; set; }
        public IKvLockRedis KvLockRedis { get; set; }
        public IDbConnectionFactory DbConnectionFactory { get; set; }
        public KvInternalContext InternalContext { get; set; }

        public async Task<(Domain.Model.OmniChannel Channel, long ChannelIdDel)> CreateOrUpdateAsync(OmniChannelRequest channelRequest, bool isSyncOrder, bool isSyncOnHand, bool isSyncPrice, OmniChannelAuth auth)
        {
            if (auth != null && !string.IsNullOrEmpty(channelRequest.Name) && !channelRequest.IsActive)
            {
                channelRequest.Status = (byte)ChannelStatusType.Pending;
            }

            using (var cli = KvLockRedis.GetLockFactory())
            {
                using (cli.AcquireLock(string.Format(KvConstant.LockAddChannelKey, channelRequest.RetailerId, channelRequest.IdentityKey), TimeSpan.FromSeconds(15)))
                {

                    var channel = await GetOmniChannelForCreatingOrUpdating(channelRequest);
                    await ValidateAsync(channelRequest, auth, channel);

                    var result = await AddOrUpdateChannel(channel, channelRequest);
                    var channelIdDel = result.ChannelIdDel;
                    channel = result.Channel;

                    var allChannelOfCurrentRetailer =
                        channelRequest.IsApplyAllFormula || (channelRequest.OmniChannelSettings?.IsApplyAllBatchExpire ?? false)
                            ? await OmniChannelRepository.WhereAsync(x =>
                                x.RetailerId == channelRequest.RetailerId && x.Type == channelRequest.Type && x.Id != channel.Id)
                            : new List<Domain.Model.OmniChannel>();

                    if (channelRequest.IsApplyAllFormula)
                    {
                        allChannelOfCurrentRetailer.ForEach(x => x.SyncOnHandFormula = channelRequest.SyncOnHandFormula);
                        await OmniChannelRepository.UpdateAllAsync(allChannelOfCurrentRetailer);
                    }

                    if (auth != null)
                    {
                        var existAuth = await OmniChannelAuthRepository.GetByChannelIdAsync(channel.Id);
                        if (existAuth != null)
                        {
                            existAuth.ShopId = auth.ShopId;
                            existAuth.AccessToken = auth.AccessToken;
                            existAuth.RefreshToken = auth.RefreshToken;
                            existAuth.ExpiresIn = auth.ExpiresIn;
                            existAuth.RefreshExpiresIn = auth.RefreshExpiresIn;
                            await OmniChannelAuthRepository.UpdateAuthWithExpire(existAuth);
                        }
                        else
                        {
                            auth.OmniChannelId = channel.Id;
                            await OmniChannelAuthRepository.UpdateAsync(auth);
                        }
                    }

                    await CreateOrUpdateSchedule(channel, isSyncOrder, isSyncOnHand, isSyncPrice);
                    #region Đánh dấu để gửi thông báo nếu shop bị hết hạn
                    if (channelRequest.IsActive)
                    {
                        if (channelRequest.OmniChannelSettings == null)
                        {
                            channelRequest.OmniChannelSettings = new OmniChannelSettingRequest
                            {
                                IsSentExpirationNotification = false
                            };
                        }
                        else
                        {
                            channelRequest.OmniChannelSettings.IsSentExpirationNotification = false;
                        }
                    }
                    #endregion

                    await OmniChannelSettingRepository.CreateOrUpdateSettingChannel(channelRequest.OmniChannelSettings, channel, allChannelOfCurrentRetailer);

                    return (channel, channelIdDel);
                }
            }
          
        }

     

        public async Task<Domain.Model.OmniChannel> GetByIdAsync(long id)
        {
            return await OmniChannelRepository.GetByIdAsync(id, true);
        }

        public async Task<List<long>> GetChannelId(int retailerId, IList<int> branchIds, IList<ChannelType> channelTypes)
        {
            var result = new List<long>();

            if (retailerId <= 0) return result;

            var query = OmniChannelRepository.BuildGenericQuery(channel => channel.RetailerId == retailerId);

            if (branchIds?.Any() == true)
            {
                query = query.Where(channel => branchIds.Contains(channel.BranchId));
            }

            if (channelTypes?.Any() == true)
            {
                query = query.Where(channel => channelTypes.Select(x => (byte)x).ToList().Contains(channel.Type));
            }

            result = await OmniChannelRepository.GetAsync<long>(query.Select(channel => channel.Id));

            return result;
        }

        public async Task<Domain.Model.OmniChannel> DeleteByIdAsync(long id)
        {

            await OmniChannelAuthRepository.DeleteAsync(x => x.OmniChannelId == id);

            await OmniChannelScheduleRepository.DeleteAsync(x => x.OmniChannelId == id, false);

            var item = await OmniChannelRepository.GetByIdAsync(id, true);
            if (item == null)
            {
                throw new KvValidateException(KvMessage.NotFound);
            }

            var isSoftDelete = !string.IsNullOrEmpty(item.IdentityKey);
            await OmniChannelRepository.DeleteAsync(x => x.Id == id, isSoftDelete);

            return item;
        }

        public async Task<List<long>> ResetSyncOnHandFormulaBySetting(int retailerId, string settingKey, bool otherSettingValue)
        {
            var lstChannelUseSyncOnHand = await OmniChannelRepository.WhereAsync(x => x.RetailerId == retailerId && x.SyncOnHandFormula > (byte)SyncOnHandFormula.OnHand
                                                        && ((settingKey == KvSettingKey.SellAllowOrder && x.SyncOnHandFormula != (byte)SyncOnHandFormula.AddSupplierOrder)
                                                        || (settingKey == KvSettingKey.UseOrderSupplier && x.SyncOnHandFormula != (byte)SyncOnHandFormula.SubOrder)));
            switch (settingKey)
            {
                case KvSettingKey.SellAllowOrder:
                    lstChannelUseSyncOnHand.ForEach(x =>
                    {
                        //Case công thức hiện tại = "Tồn kho + Đặt hàng + Đặt hàng nhập"
                        if (x.SyncOnHandFormula == (byte)SyncOnHandFormula.SubOrderAddSupplierOrder)
                        {
                            if (otherSettingValue) //Nếu đang bật Đặt hàng nhập
                            {
                                //Tồn kho + Đặt hàng nhập
                                x.SyncOnHandFormula = (byte)SyncOnHandFormula.AddSupplierOrder;
                            }
                            else
                            {
                                //Tồn kho
                                x.SyncOnHandFormula = (byte)SyncOnHandFormula.OnHand;
                            }
                        }
                        //Tồn kho + đặt hàng
                        else if (x.SyncOnHandFormula == (byte)SyncOnHandFormula.SubOrder)
                        {
                            x.SyncOnHandFormula = (byte)SyncOnHandFormula.OnHand;
                        }
                    });
                    break;

                case KvSettingKey.UseOrderSupplier:
                    lstChannelUseSyncOnHand.ForEach(x =>
                    {
                        //Case công thức hiện tại = "Tồn kho + Đặt hàng + Đặt hàng nhập"
                        if (x.SyncOnHandFormula == (byte)SyncOnHandFormula.SubOrderAddSupplierOrder)
                        {
                            if (otherSettingValue) //Nếu đang bật Đặt hàng
                            {
                                //Tồn kho + Đặt hàng
                                x.SyncOnHandFormula = (byte)SyncOnHandFormula.SubOrder;
                            }
                            else
                            {
                                //Tồn kho
                                x.SyncOnHandFormula = (byte)SyncOnHandFormula.OnHand;
                            }
                        }
                        //Tồn kho + Đặt hàng nhập
                        else if (x.SyncOnHandFormula == (byte)SyncOnHandFormula.AddSupplierOrder)
                        {
                            x.SyncOnHandFormula = (byte)SyncOnHandFormula.OnHand;
                        }
                    });
                    break;
            }
            if (lstChannelUseSyncOnHand.Any())
            {
                await OmniChannelRepository.UpdateAllAsync(lstChannelUseSyncOnHand);
            }
            return lstChannelUseSyncOnHand.Select(x => x.Id).ToList();
        }

        public async Task<List<Domain.Model.OmniChannel>> ResetPriceBookSetting(int retailerId, long priceBookId)
        {
            var listRetailerChannels = await OmniChannelRepository.WhereAsync(x => x.RetailerId == retailerId && (x.BasePriceBookId == priceBookId || x.PriceBookId == priceBookId));
            if (listRetailerChannels == null || !listRetailerChannels.Any())
            {
                return new List<Domain.Model.OmniChannel>();
            }
            var lstChannelId = listRetailerChannels.Select(p => p.Id).ToArray();
            var listSchedule = await OmniChannelScheduleRepository.WhereAsync(x => lstChannelId.Contains(x.OmniChannelId));
            foreach (var item in listRetailerChannels)
            {
                item.OmniChannelSchedules = listSchedule.Where(x => x.OmniChannelId == item.Id).ToList();
                if (item.BasePriceBookId == priceBookId)
                {
                    item.BasePriceBookId = 0;
                }
                if (item.PriceBookId == priceBookId)
                {
                    item.PriceBookId = 0;
                }
            }
            await OmniChannelRepository.UpdateAllAsync(listRetailerChannels);
            return listRetailerChannels;
        }

        public async Task<Domain.Model.OmniChannel> NeedRegisterChannel(long channelId)
        {
            var channel = await OmniChannelRepository.GetByIdAsync(channelId);
            if (channel != null)
            {
                channel.Status = (byte)ChannelStatusType.NeedRegister;
                return await OmniChannelRepository.UpdateAsync(channel);
            }
            return null;
        }

        public async Task<Domain.Model.OmniChannel> DeactivateChannel(long channelId)
        {
            var channel = await OmniChannelRepository.GetByIdAsync(channelId);
            if (channel != null)
            {
                channel.IsActive = false;
                return await OmniChannelRepository.UpdateAsync(channel);
            }
            return null;
        }

        public async Task<bool> DisableChannelByType(long retailerId, byte channelType)
        {
            var channels = await OmniChannelRepository.WhereAsync(x => x.RetailerId == retailerId && x.Type == channelType);

            if (!channels.Any()) return true;

            var channelIds = new List<long>();
            foreach (var channel in channels)
            {
                channel.IsAutoMappingProduct = false;
                channelIds.Add(channel.Id);
            }
            await OmniChannelScheduleRepository.DeleteAsync(x => channelIds.Contains(x.OmniChannelId));
            await OmniChannelRepository.UpdateAllAsync(channels);
            return true;
        }

        public async Task<List<Domain.Model.OmniChannel>> GetByIdentityKey(byte channelType, string identityKey)
        {
            var channels = await OmniChannelRepository.GetChannelByIdentityKey(channelType, identityKey);
            return channels;
        }
        public async Task<List<Domain.Model.OmniChannel>> GetByChannelIds(List<long> channelIds, int retailerId, bool isIncludeInactive = true, bool isLoadReference = false)
        {
            if (isLoadReference)
            {
                return await OmniChannelRepository.GetHasReferenceAsync(x => x.RetailerId == retailerId && channelIds.Contains(x.Id) && (isIncludeInactive || x.IsActive));
            }
            return await OmniChannelRepository.WhereAsync(x => x.RetailerId == retailerId && channelIds.Contains(x.Id) && (isIncludeInactive || x.IsActive));
        }

        public async Task<List<Domain.Model.OmniChannel>> GetByChannelIds(List<long> channelIds, bool isIncludeInactive = true, bool isLoadReference = false)
        {
            if (isLoadReference)
            {
                return await OmniChannelRepository.GetHasReferenceAsync(x => x.RetailerId == InternalContext.RetailerId && channelIds.Contains(x.Id) && (isIncludeInactive || x.IsActive));
            }
            return await OmniChannelRepository.WhereAsync(x => x.RetailerId == InternalContext.RetailerId && channelIds.Contains(x.Id) && (isIncludeInactive || x.IsActive));
        }

        public async Task ValidateDuplicateChannelSendo(int retailerId, string storeId, string shopKey, string channelName, long channelId)
        {
            if (channelId > 0)
            {
                var isValidStoreId = await OmniChannelRepository.ExistsAsync(x => x.Id == channelId && x.RetailerId == retailerId && x.IdentityKey == storeId);
                if (!isValidStoreId) throw new KvValidateException(KvMessage.sendo_validateShopKeyNotMatchStoreId);
            }

            var channels = await OmniChannelRepository.WhereAsync(x => x.Type == (int)ChannelType.Sendo && x.RetailerId == retailerId && x.IsActive);

            var isExistChannelName = channels?.Any(x => x.Name == channelName);
            var isExistShopOther = channels?.Any(x => x.IdentityKey == storeId && x.ExtraKey == shopKey && x.Name == channelName);

            if (isExistChannelName ?? false) throw new KvValidateException(KvMessage.sendo_validateDuplicateChannelName.Fmt(channelName));

            if (isExistShopOther ?? false) throw new KvValidateException(KvMessage.sendo_validateDuplicateShopKey);
        }

        public async Task<List<Domain.Model.OmniChannel>> GetByRetailerAsync(int retailerId, int branchId, byte? type = null, bool isIncludeInactive = true)
        {
            return await OmniChannelRepository.GetByRetailerAsync(retailerId, branchId, type, isIncludeInactive: isIncludeInactive);
        }

        public async Task<List<Domain.Model.OmniChannel>> GetByRetailerAsync(int retailerId, int branchId, byte? type, bool isIncludeInactive, bool isIncludeDeleted)
        {
            return await OmniChannelRepository.GetByRetailerAsync(retailerId, branchId, type, isIncludeInactive: isIncludeInactive, isIncludeDeleted: isIncludeDeleted);
        }

        public async Task<List<Domain.Model.OmniChannel>> GetByRetailerAsync(int retailerId, int branchId, List<int> types, bool isIncludeInactive, bool isIncludeDeleted)
        {
            return await OmniChannelRepository.GetByRetailerAsync(retailerId, branchId, types, isIncludeInactive: isIncludeInactive, isIncludeDeleted: isIncludeDeleted);
        }
        public async Task<List<Domain.Model.OmniChannel>> GetByRetailerAsync(int retailerId, byte? type = null, bool isIncludeInactive = true)
        {
            return await OmniChannelRepository.GetByRetailerAsync(retailerId, type, isIncludeInactive: isIncludeInactive);
        }
        public async Task<int> UpdateAllAsync(List<Domain.Model.OmniChannel> channels)
        {
            return await OmniChannelRepository.UpdateAllAsync(channels);
        }
        public async Task<List<Domain.Model.OmniChannel>> GetDeactiveByChannelIds(List<long> channelIds)
        {
            return await OmniChannelRepository.WhereAsync(x => channelIds.Contains(x.Id) && !x.IsActive);
        }
        #region Private method
        private async Task<Domain.Model.OmniChannel> GetOmniChannelForCreatingOrUpdating(OmniChannelRequest channelRequest)
        {
            var allChannelIncludeDelete = (await OmniChannelRepository
                       .WhereAsync(x => x.RetailerId == channelRequest.RetailerId &&
                                                       x.Type == channelRequest.Type &&
                                                       x.IdentityKey.Length > 0 &&
                                                       x.IdentityKey == channelRequest.IdentityKey, true));
            var channel = allChannelIncludeDelete.FirstOrDefault(item => item.IsDeleted == null || item.IsDeleted == false);
            // Nếu sàn đã từng liên kết với Demo mà bị xóa, khi kết nối lại thì enable channel đó lên
            if (channel == null && allChannelIncludeDelete.Count > 0)
            {
                return allChannelIncludeDelete.OrderByDescending(item => item.Id).FirstOrDefault(item => item.IsDeleted == true && channelRequest.BranchId == item.BranchId);
            }

            return channel;
        }

        private async Task CreateOrUpdateSchedule(Domain.Model.OmniChannel channel, bool isSyncOrder, bool isSyncOnHand, bool isSyncPrice)
        {
            var newSchedules = new List<OmniChannelSchedule>();
            var removedScheduleIds = new List<long>();

            var types = Enum.GetValues(typeof(ScheduleType)).Cast<int>().ToList();
            var currentSchedules = await OmniChannelScheduleRepository.WhereAsync(p => p.OmniChannelId == channel.Id && types.Contains(p.Type));
            //Sync order
            var existedSyncOrder = currentSchedules.FirstOrDefault(x => x.Type == (int)ScheduleType.SyncOrder);
            var existedSyncPayment = currentSchedules.FirstOrDefault(x => x.Type == (int)ScheduleType.SyncPaymentTransaction);

            HandleCreateOrUpdateScheduleSyncOrder(isSyncOrder, channel, existedSyncOrder, existedSyncPayment,
                ref newSchedules, ref removedScheduleIds);

            //Sync on hand
            var existedSyncOnHand = currentSchedules.FirstOrDefault(x => x.Type == (int)ScheduleType.SyncOnhand);
            if (isSyncOnHand)
            {
                if (existedSyncOnHand == null)
                {
                    newSchedules.Add(new OmniChannelSchedule
                    {
                        OmniChannelId = channel.Id,
                        IsRunning = false,
                        NextRun = DateTime.Now,
                        Type = (int)ScheduleType.SyncOnhand
                    });
                }
            }
            else
            {
                if (existedSyncOnHand != null)
                {
                    removedScheduleIds.Add(existedSyncOnHand.Id);
                }
            }

            //Sync price
            var existedSyncPrice = currentSchedules.FirstOrDefault(x => x.Type == (int)ScheduleType.SyncPrice);
            if (isSyncPrice)
            {
                if (existedSyncPrice == null)
                {
                    newSchedules.Add(new OmniChannelSchedule
                    {
                        OmniChannelId = channel.Id,
                        IsRunning = false,
                        NextRun = DateTime.Now,
                        Type = (int)ScheduleType.SyncPrice
                    });
                }
            }
            else
            {
                if (existedSyncPrice != null)
                {
                    removedScheduleIds.Add(existedSyncPrice.Id);
                }
            }

            var existedSyncModifiedProduct = currentSchedules.FirstOrDefault(x => x.Type == (int)ScheduleType.SyncModifiedProduct);
            if (existedSyncModifiedProduct == null)
            {
                newSchedules.Add(new OmniChannelSchedule
                {
                    OmniChannelId = channel.Id,
                    IsRunning = false,
                    NextRun = DateTime.Now.AddMinutes(5),
                    LastSync = null,
                    Type = (int)ScheduleType.SyncModifiedProduct
                });
            }

            if (removedScheduleIds.Any())
            {
                await OmniChannelScheduleRepository.DeleteAsync(x => removedScheduleIds.Contains(x.Id));
            }
            if (newSchedules.Any())
            {
                await OmniChannelScheduleRepository.InsertAllAsync(newSchedules);
            }
        }

        private void HandleCreateOrUpdateScheduleSyncOrder(bool isSyncOrder, Domain.Model.OmniChannel channel, OmniChannelSchedule existedSyncOrder, OmniChannelSchedule existedSyncPayment, ref List<OmniChannelSchedule> newSchedules, ref List<long> removedScheduleIds)
        {
            if (isSyncOrder)
            {
                if (existedSyncOrder == null)
                {
                    newSchedules.Add(new OmniChannelSchedule
                    {
                        OmniChannelId = channel.Id,
                        IsRunning = false,
                        NextRun = DateTime.Now,
                        Type = (int)ScheduleType.SyncOrder
                    });
                }

                if (existedSyncPayment == null)
                {
                    newSchedules.Add(new OmniChannelSchedule
                    {
                        OmniChannelId = channel.Id,
                        IsRunning = false,
                        NextRun = DateTime.Now,
                        Type = (int)ScheduleType.SyncPaymentTransaction
                    });
                }
            }
            else
            {
                if (existedSyncOrder != null)
                {
                    removedScheduleIds.Add(existedSyncOrder.Id);
                    CacheClient.Remove(KvConstant.ChannelScheduleByIdKey.FormatWith(channel.RetailerId, channel.Id,
                        existedSyncOrder.Id));
                }

                if (existedSyncPayment != null)
                {
                    removedScheduleIds.Add(existedSyncPayment.Id);
                }
            }
        }

        private async Task ValidateAsync(OmniChannelRequest channelNew, OmniChannelAuth auth, Domain.Model.OmniChannel channelExist)
        {
            if (channelNew == null)
            {
                throw new KvValidateException(KvMessage.NotFound);
            }
            if (channelNew.BranchId <= 0)
            {
                throw new KvValidateException(KvMessage.lazadaEmptyBranch);
            }

            // Kết nối mới đã trùng một kết nối khác hoặc cập nhật kết nối mới trùng với kết nối khác
            var isCreateNewDifferenceRetailer = channelNew.Id == 0 && channelExist != null && (channelExist.IsDeleted == null || channelExist.IsDeleted == false);
            var isUpdateDiffrenceRetailer = channelNew.Id != 0 && channelExist != null && channelNew.Id != channelExist.Id
                && (channelExist.IsDeleted == null || channelExist.IsDeleted == false);
            if (isCreateNewDifferenceRetailer || isUpdateDiffrenceRetailer)
            {
                throw new KvValidateException($"{channelNew.Name} đã kết nối với cửa hàng, Vui lòng chọn shop khác.");
            }

            int maximumSaleChannels;
            using (var dbMaster = DbConnectionFactory.Open("KvMasterEntities"))
            {
                var kvRetailer = await dbMaster.SingleAsync<KvRetailer>(x => x.Id == channelNew.RetailerId);
                var maximumSaleChannelsDefault = NumberHelper.GetValueOrDefault(AppSettings.Get<int>("MaximumSaleChannels"), 5);
                maximumSaleChannels = kvRetailer?.MaximumSaleChannels ?? maximumSaleChannelsDefault;

                //KOL-4: Adding posparameter omnichannel
                var kvGroup = await dbMaster.SingleAsync<Domain.Model.KvGroup>(x => x.Id == kvRetailer.GroupId);
                var shardConnectionString = kvGroup.ConnectionString.Substring(kvGroup.ConnectionString.IndexOf('=') + 1)?.ToLower();
                using (var dbShard = DbConnectionFactory.Open(shardConnectionString))
                {
                    var posParameters = await dbShard
                        .SingleAsync<PosParameter>(t => t.RetailerId == kvRetailer.Id && t.Key == nameof(PosSetting.OmniChannel));

                    await ValidateRetailerChannelWithPosParameter(posParameters, kvRetailer, maximumSaleChannels, auth);
                    if (posParameters != null)
                    {
                        return;
                    }
                }
            }
            await ValidateChannelByTypeAsync(channelNew, auth, maximumSaleChannels);
        }

        private async Task ValidateChannelByTypeAsync(OmniChannelRequest obj, OmniChannelAuth auth, int maximumSaleChannels)
        {
            var lstExistBranch = (await OmniChannelRepository.WhereAsync(p => p.Type == obj.Type && p.RetailerId == obj.RetailerId && p.Id != obj.Id && p.BranchId == obj.BranchId)).Select(x => x.BranchId).ToList();
            if (lstExistBranch.Contains(obj.BranchId) && lstExistBranch.Count >= maximumSaleChannels)
            {
                throw new KvValidateException(string.Format(KvMessage.limitMultiBranchError, maximumSaleChannels, ((ChannelType)obj.Type).ToString()));
            }

            switch (obj.Type)
            {
                case (byte)ChannelType.Shopee:
                    if (auth != null && auth.ShopId > 0)
                    {
                        //#12776 Check a Shopee shop has connected with unique branch
                        var connectedChannel =
                            await OmniChannelRepository.CountChannelConnectedAsync(obj.RetailerId, auth.ShopId,
                                obj.Id, obj.Type);
                        if (connectedChannel >= 1)
                        {
                            throw new KvValidateException(string.Format(KvMessage.lazadaMultiRetailError, obj.Name));
                        }
                    }
                    break;

                case (byte)ChannelType.Lazada:
                    if (!string.IsNullOrEmpty(obj.IdentityKey))
                    {
                        var isExistIdentityKey = await OmniChannelRepository.ExistsAsync(x => x.RetailerId == obj.RetailerId && x.Id != obj.Id && x.IdentityKey == obj.IdentityKey);
                        if (isExistIdentityKey)
                        {
                            throw new KvValidateException(string.Format(KvMessage.lazadaMultiRetailError, obj.Name));
                        }
                    }
                    break;

                case (byte)ChannelType.Tiki:
                    await ValidateOmniChannelDTOIdentityKeyAsync(obj);
                    break;

                case (byte)ChannelType.Sendo:
                    await ValidateOmniChannelDTOIdentityKeyAsync(obj);
                    break;
            }
        }

        private async Task ValidateOmniChannelDTOIdentityKeyAsync(OmniChannelRequest obj)
        {
            if (!string.IsNullOrEmpty(obj.IdentityKey))
            {
                var isExistChannel = await OmniChannelRepository.ExistsAsync(x => x.RetailerId == obj.RetailerId && x.Id != obj.Id && x.IdentityKey == obj.IdentityKey);
                if (isExistChannel)
                {
                    throw new KvValidateException(string.Format(KvMessage.lazadaMultiRetailError, obj.Name));
                }
            }
        }

        private async Task ValidateRetailerChannelWithPosParameter(PosParameter omniPosparameter, KvRetailer retailer, int maximumSaleChannels, OmniChannelAuth auth)
        {
            var constBlockDateTime = DateTime.Parse(AppSettings.Get<string>("LicenseKOLBlockDate", "2021-10-15"));

            if (auth == null || (auth.OmniChannelId.HasValue))
            {
                return;
            }


            //Không có hợp đồng omnichannel ở Posparameter => Retailer cũ đã sử dụng.
            if (retailer.ContractDate <= constBlockDateTime)
            {
                return;
            }

            var channelUsed = await OmniChannelRepository.GetActiveChannelByRetailer(retailer.Id);

            #region Trial Retailer
            if (retailer.ContractType == 0) //Trial Contract
            {
                var maximum = omniPosparameter != null && omniPosparameter.isActive ? omniPosparameter.BlockUnit.GetValueOrDefault() : maximumSaleChannels;
                if (channelUsed.Count >= maximum)
                    throw new KvValidateException($"Không thể kết nối thêm. Số lượng kết nối tối đa đến các sàn thương mại điện tử của gian hàng đã đạt {maximum}. Vui lòng liên hệ Hotline 1800 6162 để đăng ký thêm kết nối");
                return;
            }
            #endregion

            #region New Retailer after KOL release
            if (retailer.ContractDate > constBlockDateTime && channelUsed.Count >= omniPosparameter?.BlockUnit.GetValueOrDefault())
            {
                throw new KvValidateException($"Không thể kết nối thêm. Số lượng kết nối tối đa đến các sàn thương mại điện tử của gian hàng đã đạt {omniPosparameter.BlockUnit.GetValueOrDefault()}. Vui lòng liên hệ Hotline 1800 6162 để đăng ký thêm kết nối");
            }
            #endregion

            #region check omni expiredDate
            if (retailer.ContractType != (byte)ContractType.Trial && DateTime.Today > omniPosparameter?.ExpiredDate)
            {
                throw new KvValidateException("Hợp đồng Omni Channel đã hết hạn. Không thể tạo thêm kênh bán");
            }
            #endregion
        }

        private async Task<(Domain.Model.OmniChannel Channel, long ChannelIdDel)> AddOrUpdateChannel(Domain.Model.OmniChannel channel, OmniChannelRequest channelRequest)
        {
            long channelIdDel = 0;
            Domain.Model.OmniChannel channelResult;

            // Add OR Update
            if (channel != null)
            {
                if (channel.IdentityKey == channelRequest.IdentityKey)
                {
                    await Update(channel, channelRequest);
                    channelResult = channel;
                }
                else
                {
                    // Add new
                    channelResult = await Add(channelRequest);

                    channelIdDel = channelRequest.Id;
                }

                if (channelRequest.Id > 0 && channel.Id != channelRequest.Id) channelIdDel = channelRequest.Id;
            }
            else
            {
                if (channelRequest.Id > 0)
                {
                    channel = await OmniChannelRepository.GetByIdAsync(channelRequest.Id);

                    if (channel != null)
                    {
                        if (string.IsNullOrEmpty(channel.IdentityKey))
                        {
                            await Update(channel, channelRequest);
                            channelResult = channel;
                        }
                        else
                        {
                            channelIdDel = channel.Id;

                            // Add new
                            channelResult = await Add(channelRequest);
                        }
                    }
                    else
                    {
                        // Add new
                        channelResult = await Add(channelRequest);
                    }
                }
                else
                {
                    // Add new
                    channelResult = await Add(channelRequest);
                }
            }

            return (channelResult, channelIdDel);
        }

        #region Local func

        async Task Update(Domain.Model.OmniChannel oldChannel, OmniChannelRequest newChannel)
        {
            oldChannel.Update(newChannel.Name, newChannel.Email, newChannel.IsActive, newChannel.Status,
                newChannel.BranchId,
                newChannel.PriceBookId, newChannel.BasePriceBookId, newChannel.SyncOnHandFormula,
                newChannel.SyncOrderFormula ?? oldChannel.SyncOrderFormula,
                newChannel.IsApplyAllFormula, newChannel.IsAutoMappingProduct, newChannel.IsAutoDeleteMapping,
                newChannel.IdentityKey, newChannel.ExtraKey, false, newChannel.PlatformId, newChannel.RegisterDate);
            await OmniChannelRepository.UpdateAsync(oldChannel);
        }

        async Task<Domain.Model.OmniChannel> Add(OmniChannelRequest channel)
        {
            var channelNew = new Domain.Model.OmniChannel(channel.Name, channel.Email, channel.IsActive,
                channel.Status, channel.Type, channel.BranchId, channel.RetailerId, channel.PriceBookId,
                channel.BasePriceBookId,
                channel.SyncOnHandFormula, channel.SyncOrderFormula, channel.IsApplyAllFormula,
                channel.IsAutoMappingProduct, channel.IsAutoDeleteMapping, channel.IdentityKey, channel.ExtraKey, channel.PlatformId);
            var newId = await OmniChannelRepository.AddAsync(channelNew);
            return await OmniChannelRepository.GetByIdAsync(newId);
        }

        #endregion
        #endregion



    }
}
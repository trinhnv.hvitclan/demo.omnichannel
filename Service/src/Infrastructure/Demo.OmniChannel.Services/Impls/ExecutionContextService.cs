﻿using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Auth;

namespace Demo.OmniChannel.Services.Impls
{
    public class ExecutionContextService : IExecutionContextService
    {
        public ExecutionContext Context { get; private set; }

        public void SetContext(ExecutionContext context)
        {
            Context = context;
        }
    }
}

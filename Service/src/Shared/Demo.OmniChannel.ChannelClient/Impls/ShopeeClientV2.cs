﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Helper;
using Demo.OmniChannel.ChannelClient.Impls.NewBase;
using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.RequestDTO;
using Demo.OmniChannel.ChannelClient.RequestDTO.Shopee;
using Demo.OmniChannel.ChannelClient.RequestDTO.Shopee.V2;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee.V2;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Impls;
using Demo.OmniChannel.ShareKernel.Dtos;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.ShareKernel.Models;
using Demo.OmniChannel.Utilities;
using Newtonsoft.Json;
using Polly;
using Polly.Timeout;
using RestSharp;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Logging;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Image = Demo.OmniChannel.ShareKernel.Dtos.Image;
using PlatformChannel = Demo.OmniChannel.ChannelClient.Models;
using SelfAppConfig = Demo.OmniChannel.ChannelClient.Common.SelfAppConfig;

namespace Demo.OmniChannel.ChannelClient.Impls
{
    public class ShopeeClientV2 : IShopeeClientV2
    {
        private const string SellerNeedCompleteSellerRegistration = "SellerNeedCompleteSellerRegistration";
        private readonly IntegrationInternalClient _integrationInternalClient;

        private ILog Log => LogManager.GetLogger(typeof(ShopeeClientV2));
        private IAppSettings AppSettings { get; set; }
        public ShopeeClientV2(IAppSettings settings)
        {
            AppSettings = settings;
            _ = new SelfAppConfig(settings);
            _integrationInternalClient = new IntegrationInternalClient(settings);
        }

        public async Task<PlatformChannel.TokenResponse> CreateToken(string code, PlatformChannel.ChannelAuth auth, PlatformChannel.Platform platform)
        {
            var timeout = SelfAppConfig.ForwarderApiFeature.DefaultForwarder;
            var response = await Policy
                .HandleResult<IRestResponse<ShopeeTokenReponseV2>>(ms => ms.StatusCode != HttpStatusCode.OK)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry,
                    i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (result, timeSpan, retryCount) =>
                    {
                        Log.Warn(
                            $"GetAccessTokenWaitAndRetryAsync with {result.Result.StatusCode}. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount}");
                    }).ExecuteAsync(() =>
                                        ShopeeHelperV2.GetAccessToken(code, auth.ShopId, platform, timeout));
            if (response?.Data == null || string.IsNullOrEmpty(response.Data.AccessToken))
                return null;
            return new PlatformChannel.TokenResponse
            {
                AccessToken = response.Data.AccessToken,
                RefreshToken = response.Data.RefreshToken,
                ExpiresIn = (int)response.Data.ExpireIn,
                RefreshExpiresIn = 24 * 3600 * 30, //30 ngày,
                ShopId = auth.ShopId
            };
        }

        public async Task<PlatformChannel.RefreshTokenResponse> RefreshAccessToken(string refreshToken, long channelId, Guid logId,
            PlatformChannel.ChannelAuth auth, PlatformChannel.Platform platform)
        {
            var refreshAccessTokenLog = new LogObject(Log, logId)
            {
                Action = "ShopeeRefreshAccessToken",
                OmniChannelId = channelId,
                RequestObject = $"{refreshToken}"
            };
            var response = await Policy.HandleResult<IRestResponse<ShopeeRefreshTokenReponseV2>>(ms => ms.StatusCode != HttpStatusCode.OK)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (result, timeSpan, retryCount) =>
                    {
                        Log.Warn(
                            $"GetShopInfoWaitAndRetryAsync with {result.Result.StatusCode}. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount}");
                    }).ExecuteAsync(() => ShopeeHelperV2.RefreshAccessToken(refreshToken, auth, platform));

            if (response?.Data != null && !string.IsNullOrEmpty(response.Data.RefreshToken))
                return new PlatformChannel.RefreshTokenResponse
                {
                    AccessToken = response.Data.AccessToken,
                    RefreshToken = response.Data.RefreshToken,
                    ExpiresIn = response.Data.ExpireIn,
                    RefreshExpiresIn = 24 * 3600 * 30, //30 ngày,
                    ShopId = response.Data.ShopId
                };

            if (!string.IsNullOrEmpty(response?.Data?.Error) && response.Data.Error == "error_auth")
            {
                refreshAccessTokenLog.Description = "DeActive channel because error auth";
                refreshAccessTokenLog.ResponseObject = new { Status = response.StatusCode, response.Content };
                refreshAccessTokenLog.LogError();

                return new PlatformChannel.RefreshTokenResponse
                {
                    IsDeactivateChannel = true
                };
            }

            refreshAccessTokenLog.ResponseObject = new { Status = response?.StatusCode, response?.Content };
            refreshAccessTokenLog.LogError();
            return null;
        }

        public async Task<ShopInfoResponseV2> GetShopInfo(PlatformChannel.ChannelAuth auth, KvInternalContext context,
            bool isOnlyGetInfo = false, PlatformChannel.Platform platform = null)
        {
            var timeout = SelfAppConfig.ForwarderApiFeature.DefaultForwarder;

            if (isOnlyGetInfo && !string.IsNullOrEmpty(auth?.AccessToken))
            {
                var shop = await ShopeeHelperV2.GetShopInfo(auth, platform);
                return new ShopInfoResponseV2
                {
                    Name = shop.Data.ShopName,
                    IdentityKey = auth.ShopId.ToString()
                };
            }
            var response = await Policy.HandleResult<IRestResponse<PlatformChannel.ShopResponse>>(ms => ms.StatusCode != HttpStatusCode.OK)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (result, timeSpan, retryCount) =>
                    {
                        Log.Warn(
                            $"GetShopInfoWaitAndRetryAsync with {result.Result.StatusCode}. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount}");
                    }).ExecuteAsync(() => ShopeeHelperV2.GetShopInfo(auth, platform, timeout));
            if (!string.IsNullOrEmpty(response?.Data.Error))
            {
                var responseError = JsonConvert.DeserializeObject<ShopResponse>(response.Content);
                if (!string.IsNullOrEmpty(responseError?.Message) && SelfAppConfig.ShopeeSellerNeedRegisterMsg != null &&
                            SelfAppConfig.ShopeeSellerNeedRegisterMsg.Any() &&
                            SelfAppConfig.ShopeeSellerNeedRegisterMsg.Contains(responseError?.Message))
                {
                    var exception = new ShopeeNeedCompleteRegisterException(SellerNeedCompleteSellerRegistration);
                    throw exception;
                }
            }

            return new ShopInfoResponseV2
            {
                Name = response.Data.ShopName,
                IdentityKey = auth.ShopId.ToString()
            };
        }

        public async Task<ProductListResponse> GetListProduct(PlatformChannel.ChannelAuth auth, long channelId, Guid logId, bool isFirstSync,
            DateTime? updateTimeFrom = null, KvInternalContext kvContext = null, PlatformChannel.Platform platform = null)
        {
            var getGetProductLog = new LogObject(Log, logId)
            {
                Action = string.Format(EcommerceTypeException.GetListProduct, Ecommerce.Shopee),
                OmniChannelId = channelId,
                RequestObject = $"LastSync: {updateTimeFrom}"
            };
            var needDeletedItem = !isFirstSync;
            long? updateTimeFromUnixTime = null;
            if (isFirstSync || updateTimeFrom == null)
            {
                updateTimeFromUnixTime = null;
            }
            else
            {
                updateTimeFromUnixTime = updateTimeFrom.Value.ToUnixTime();
            }
            var result = await GetFullProductInfoList(auth, kvContext, channelId, updateTimeFromUnixTime, null, needDeletedItem, getGetProductLog, platform);
            getGetProductLog.ResponseObject = $"ProductIds: {result.Data.Select(x => x.ItemId).ToSafeJson()} DeletedIds: {result.RemovedProductIds.ToSafeJson()}";
            getGetProductLog.LogInfo();
            return result;
        }

        public async Task<List<ItemListBase>> GetItemListBaseWithPageSize(PlatformChannel.ChannelAuth auth,
          List<long> itemLst,
          Guid logId,
          PlatformChannel.Platform platform, int pageSize = 30)
        {
            int totalCount = itemLst.Count;
            var totalPages = (int)Math.Ceiling(totalCount / (decimal)pageSize);
            var listItem = new List<ItemListBase>();
            for (int page = 1; page <= totalPages; page++)
            {
                var itemPaging = itemLst.Skip((page - 1) * pageSize).Take(pageSize).ToList();
                var items = await GetProductListBaseInfoPolly(auth, itemPaging.ToArray(), logId, platform);
                if(items != null || items.Any()) listItem.AddRange(items);
            }
            return listItem;
        }

        public async Task<RequestDTO.ProductDetailResponse> GetProductDetail(PlatformChannel.ChannelAuth auth, long channelId, Guid logId,
            object productId, object parentProductId, string productSku, PlatformChannel.Platform platform)
        {
            var getProductDetailLog = new LogObject(Log, logId)
            {
                Action = "ShopeeGetProductDetailV2",
                OmniChannelId = channelId,
                RequestObject = $"{productId} - {productSku}"
            };

            var response = await GetProductListBaseInfoPolly(auth, new long[] { Convert.ToInt64(parentProductId) },
                    getProductDetailLog.Id, platform);

            var prdDetail = new ProductDetailSkus
            {
                ItemId = response?.FirstOrDefault()?.ItemId.ToString(),
                Images = response?.FirstOrDefault()?.Images?.ImageUrlList,
                Description = response?.FirstOrDefault()?.Description,
            };
            if (response != null)
            {
                getProductDetailLog.ResponseObject = response;
                getProductDetailLog.LogInfo();
            }
            var result = new RequestDTO.ProductDetailResponse()
            {
                ItemId = response?.FirstOrDefault()?.ItemId.ToString(),
                ProductDetails = new List<ProductDetailSkus>
                {
                    prdDetail
                }
            };
            return result;
        }

        public Task<(bool, string)> SyncOnHand(
            int retailerId, string kvProductSku, long channelId, Guid logId,
            PlatformChannel.ChannelAuth auth, string itemId, string itemSku, double onHand, byte productType,
            LogObjectMicrosoftExtension loggerExtension, string parentProductId = null, PlatformChannel.Platform platform = null)
        {
            throw new NotImplementedException();
        }

        public Task<(bool, string)> SyncMultiOnHand(
            int retailerId, long channelId, Guid logId, PlatformChannel.ChannelAuth auth,
            List<MultiProductItem> productItems, LogObjectMicrosoftExtension loggerExtension, byte productType,
            PlatformChannel.Platform platform)
        {
            throw new NotImplementedException();
        }

        public async Task<(bool, string, string)> SyncOnHandWithGroupParrent(PlatformChannel.ChannelAuth auth,
          string parentItemId, List<MultiProductItem> productItems,
          LogObjectMicrosoftExtension logger,
          PlatformChannel.Platform platform)
        {
            var loggerStock = logger.Clone(string.Format(EcommerceTypeException.SyncMultiOnHandWithGroupParrent,
                Ecommerce.Shopee));
            loggerStock.RequestObject = productItems;
            try
            {
                var isUseGrpc = SelfAppConfig.ForwarderApiFeature.Enable &&
                    SelfAppConfig.ForwarderApiFeature.DefaultForwarder;
                var result = new List<Product>();
                var request = new ShopeeUpdateStockRequestV2
                {
                    ItemId = long.Parse(parentItemId),
                    StockList = productItems.Select(x => new ShopeeStockList
                    {
                        ModelId = (x.ItemId == parentItemId) ? 0 : long.Parse(x.ItemId),
                        SellerStock = new List<ShopeeSellerStock>
                        {
                            new ShopeeSellerStock { Stock = (int)x.OnHand}
                        }
                    }).ToList()
                };

                var response = await Policy
                     .HandleResult<IRestResponse<ShopUpdateStockResponseV2>>(msg => msg.StatusCode != HttpStatusCode.OK)
                     .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                     (res, timeSpan, retryCount, context) =>
                     {
                         var logRetry = loggerStock.Clone("ShopeeSyncStockV2WaitAndRetry");
                         var messageErr = $"ShopeeSyncStockV2WaitAndRetry with {res.Result?.StatusCode} - RequestId: {res.Result?.Data?.RequestId}. " +
                         $"Waiting {timeSpan:g} before next retry. Retry attempt {retryCount}";
                         var ex = new ShopeeException(messageErr);
                         logRetry.LogError(ex);
                     }).ExecuteAsync(() => ShopeeHelperV2.UpdateStock(request, auth, platform, isUseGrpc));

                if (response?.Data?.Response?.SuccessList != null && response.Data.Response.SuccessList.Any())
                {
                    result.AddRange(response.Data.Response.SuccessList.Select(x => new Product
                    {
                        ItemId = x.ModelId > 0 ? x.ModelId.ToString() : parentItemId,
                        OnHand = x.Stock
                    }));
                }

                if (response?.Data?.Response?.FailureList != null && response.Data.Response.FailureList.Any())
                {
                    result.AddRange(response.Data.Response.FailureList.Select(x => new Product
                    {
                        ItemId = x.ModelId > 0 ? x.ModelId.ToString() : parentItemId,
                        ErrorMessage = x.FailedReason
                    }));
                }

                loggerStock.ResponseObject = response?.Content;
                loggerStock.LogInfo();

                return (result.Any(),
                    (new { response?.Data?.RequestId, Product = result }).ToSafeJson(),
                    response?.Data?.Msg ?? response?.ErrorMessage);
            }
            catch (Exception ex)
            {
                loggerStock.LogError(ex);
                throw;
            }
        }

        public Task<(bool, string)> SyncPrice(int retailerId, string kvProductSku, long channelId, Guid logId,
            PlatformChannel.ChannelAuth auth, string itemId, string itemSku, decimal? basePrice
            , byte productType, string parentItemId, LogObjectMicrosoftExtension loggerExtension,
            decimal? salePrice = null, DateTime? startSaleDateTime = null, DateTime? endSaleDateTime = null,
            PlatformChannel.Platform platform = null)
        {
            throw new NotImplementedException();
        }


        public Task<(bool, string, int?)> SyncMultiPrice(int retailerId, long channelId, Guid logId, PlatformChannel.ChannelAuth auth,
            List<MultiProductItem> productItems, byte productType, LogObjectMicrosoftExtension loggerExtension,
            PlatformChannel.Platform platform)
        {
            throw new NotImplementedException();
        }

        public async Task<(bool, string, string)> SyncPriceWithGroupParrent(PlatformChannel.ChannelAuth auth,
            string parentItemId, List<MultiProductItem> productItems,
            LogObjectMicrosoftExtension logger,
            PlatformChannel.Platform platform)
        {
            var loggerPrice = logger.Clone(string.Format(EcommerceTypeException.SyncMultiPriceWithGroupParrent,
                Ecommerce.Shopee));
            loggerPrice.RequestObject = productItems;
            try
            {
                var isUseGrpc = SelfAppConfig.ForwarderApiFeature.Enable &&
                 SelfAppConfig.ForwarderApiFeature.DefaultForwarder;
                var result = new List<Product>();
                var request = new ShopeeUpdatePriceRequestV2
                {
                    ItemId = long.Parse(parentItemId),
                    PriceList = productItems.Select(x => new ShopeePriceList
                    {
                        ModelId = (x.ItemId == parentItemId) ? 0 : long.Parse(x.ItemId),
                        OriginalPrice = (float)x.Price
                    }).ToList()
                };

                var response = await Policy
                     .HandleResult<IRestResponse<ShopUpdatePriceResponseV2>>(msg => msg.StatusCode != HttpStatusCode.OK)
                     .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                     (res, timeSpan, retryCount, context) =>
                     {
                         var logRetry = loggerPrice.Clone("ShopeeSyncPriceV2WaitAndRetry");
                         var messageErr = $"ShopeeSyncPriceV2WaitAndRetry with {res.Result?.StatusCode} - RequestId: {res.Result?.Data?.RequestId}. " +
                         $"Waiting {timeSpan:g} before next retry. Retry attempt {retryCount}";
                         var ex = new ShopeeException(messageErr);
                         logRetry.LogError(ex);
                     }).ExecuteAsync(() => ShopeeHelperV2.UpdatePrice(request, auth, platform, isUseGrpc));

                if (response?.Data?.Response?.SuccessList != null && response.Data.Response.SuccessList.Any())
                {
                    result.AddRange(response.Data.Response.SuccessList.Select(x => new Product
                    {
                        ItemId = x.ModelId > 0 ? x.ModelId.ToString() : parentItemId,
                        BasePrice = (decimal)x.OriginalPrice
                    }));
                }

                if (response?.Data?.Response?.FailureList != null && response.Data.Response.FailureList.Any())
                {
                    result.AddRange(response.Data.Response.FailureList.Select(x => new Product
                    {
                        ItemId = x.ModelId > 0 ? x.ModelId.ToString() : parentItemId,
                        ErrorMessage = x.FailedReason
                    }));
                }

                loggerPrice.ResponseObject = response?.Content;
                loggerPrice.LogInfo();

                return (result.Any(),
                    (new { response?.Data?.RequestId, Product = result }).ToSafeJson(),
                    response?.Data?.Msg ?? response?.ErrorMessage);
            }
            catch (Exception ex)
            {
                loggerPrice.LogError(ex);
                throw;
            }
        }

        public Task<(bool, List<CreateOrderRequest>)> GetOrders(
            PlatformChannel.ChannelAuth auth,
            OrderRequest request,
            KvInternalContext context,
            LogObjectMicrosoftExtension logInfo,
            PlatformChannel.Platform platform)
        {
            throw new NotImplementedException();
        }

        public async Task<OrderDetailGetResponse> GetOrderDetail(int retailerId, long channelId,
            PlatformChannel.ChannelAuth auth, CreateOrderRequest request, LogObjectMicrosoftExtension logInfo,
            PlatformChannel.Platform platform, OmniChannelSettingObject settings = null)
        {
            var shopeeOrder = await GetOrderDetailPolly(request.OrderId, auth, logInfo, platform);
            if (shopeeOrder == null)
            {
                logInfo.LogWarning("Empty order v2 detail");
                return new OrderDetailGetResponse
                {
                    IsSuccess = false,
                    IsRetryOrder = true
                };
            }
            var trackingInfo = await GetTrackingInfoPolly(request.OrderId, auth, logInfo, platform);
            var shopeeChannelBusiness = new ShopeeBusinessImplimentBehavior(AppSettings);
            var kvOrder = shopeeChannelBusiness.GenKvOrderFromOrderDetailV2(shopeeOrder, trackingInfo, settings,
                request.BranchId, request.UserId, request.SaleChannelId);
            var escrowDetail = await GetEscrowDetailPolly(request.OrderId, auth, logInfo, platform);
            if (escrowDetail == null)
            {
                return new OrderDetailGetResponse
                {
                    IsSuccess = false,
                    IsRetryOrder = true,
                    Order = kvOrder
                };
            }
            kvOrder = shopeeChannelBusiness.ConvertPaymentEscrowToKvOrder(kvOrder, escrowDetail, retailerId, 0);
            return new OrderDetailGetResponse
            {
                IsSuccess = true,
                IsRetryOrder = false,
                Order = kvOrder
            };
        }



        public Task<(bool, bool, bool, List<KvInvoice>)> GetInvoiceDetail(int retailerId, long channelId, Guid logId,
            ChannelAuth auth, CreateInvoiceRequest request, LogObjectMicrosoftExtension loggerExtension,
            PlatformChannel.Platform platform,
            OmniChannelSettingObject settings)
        {
            throw new NotImplementedException();
        }

        public Task<(byte?, string, string)> GetLogisticsStatus(int retailerId, long channelId, LogObjectMicrosoftExtension logInfo,
            string orderId, PlatformChannel.ChannelAuth auth, bool isGetOrderStatus = false, PlatformChannel.Platform platform = null)
        {
            throw new NotImplementedException();
        }


        // Hỗ trợ gọi GRPC nếu failed
        public async Task<(byte?, byte?, byte?)> GetOrderStatus(int retailerId, long channelId, Guid logId, string orderId,
            ChannelAuth auth, LogObjectMicrosoftExtension loggerExtension, bool isGetDeliveryStatus = false,
            PlatformChannel.Platform platform = null, OmniChannelSettingObject settings = null)
        {
            var orderDetail = await GetOrderDetailPolly(orderId, auth, loggerExtension, platform);
            var shopeeChannelBusiness = new ShopeeBusinessImplimentBehavior(AppSettings);
            byte? deliveryStatus = null;
            string logisticsStatus = null;
            ShopeeTrackingInfoV2Response trackingInfo = null;
            if (isGetDeliveryStatus)
            {
                trackingInfo = await GetTrackingInfoPolly(orderId, auth, loggerExtension, platform);
                if (orderDetail != null
                    && (orderDetail.OrderStatus == ShopeeStatus.Completed
                    || orderDetail.OrderStatus == ShopeeStatus.ToConfirmReceive
                    || orderDetail.OrderStatus == ShopeeStatus.Shipped
                    || orderDetail.OrderStatus == ShopeeStatus.Cancelled)
                    && trackingInfo != null)
                {
                    deliveryStatus = shopeeChannelBusiness.ConvertTrackingInfoToKvDeliveryStatus(trackingInfo.LogisticsStatus);
                    logisticsStatus = trackingInfo.LogisticsStatus;
                }
            }
            var orderStatus = orderDetail == null ? null : shopeeChannelBusiness.ConvertOrderDetailToKvOrderStatus(orderDetail.OrderStatus, trackingInfo);
            var invoiceStatus = orderDetail == null ? null : shopeeChannelBusiness.ConvertOrderDetailToKvInvoiceStatus(orderDetail.OrderStatus, logisticsStatus);
            return (orderStatus, invoiceStatus, deliveryStatus);
        }

        public Task<ShopInfoResponseV2> GetShopStatus(string accessToken)
        {
            throw new NotImplementedException();
        }

        public Task EnableUpdateProduct(string accessToken)
        {
            throw new NotImplementedException();
        }

        public Task<CustomerResponse> GetCustomerByOrderId(int retailerId, long channelId, string orderId,
            PlatformChannel.ChannelAuth auth, Guid logId, LogObjectMicrosoftExtension loggerExtension, PlatformChannel.Platform platform)
        {
            throw new NotImplementedException();
        }

        public Task<List<PlatformChannel.KvInvoice>> GetInvoiceDetailByOrderId(int retailerId, Guid logId, PlatformChannel.ChannelAuth auth,
            string orderId, long channelId, LogObjectMicrosoftExtension loggerExtension, PlatformChannel.Platform platform)
        {
            throw new NotImplementedException();
        }

        public async Task<List<PlatformChannel.Logistics>> GetLogisticsByShopId(int retailerId, long channelId, PlatformChannel.ChannelAuth auth,
              Guid logId, PlatformChannel.Platform platform)
        {
            var getLogisticsByShopIdLog = new LogObject(Log, logId)
            {
                Action = "ShopeeGetLogisticsByShopId",
                OmniChannelId = channelId,
                RetailerId = retailerId,
                RequestObject = ""
            };

            try
            {
                var isCallGrpc = SelfAppConfig.ForwarderApiFeature.DefaultForwarder;
                var response = await Policy.HandleResult<IRestResponse<ShopeeLogisticsGetChannelListReponseV2>>(msg => msg.StatusCode != HttpStatusCode.OK)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval), (res, timeSpan, retryCount, context) =>
                        { }).ExecuteAsync(() => ShopeeHelperV2.GetLogisticsChannelList(auth, platform, isCallGrpc));

                if (!string.IsNullOrEmpty(response.Data?.Error))
                    throw new ShopeeException(response.Data?.Error);

                var logistics = response.Data?.Response?.LogisticsChannel?.Select(x => x.ConvertTo<PlatformChannel.Logistics>())
                    .ToList();
                getLogisticsByShopIdLog.ResponseObject = logistics;
                getLogisticsByShopIdLog.LogInfo();
                return logistics;
            }
            catch (Exception e)
            {
                getLogisticsByShopIdLog.LogError(e);
                throw new ShopeeException("Lỗi lấy dữ liệu hãng vận chuyển.");
            }
        }

        public async Task<List<PlatformChannel.ShopeeAttribute>> GetAttributeByShopId(int retailerId, long channelId, PlatformChannel.ChannelAuth auth,
            Guid logId, long categoryId, PlatformChannel.Platform platform)
        {
            var getAttributesByShopIdLog = new LogObject(Log, logId)
            {
                Action = "GetAttributeByShopId",
                OmniChannelId = channelId,
                RetailerId = retailerId,
                RequestObject = ""
            };

            try
            {
                var isCallGrpc = SelfAppConfig.ForwarderApiFeature.DefaultForwarder;
                var response = await Policy.HandleResult<IRestResponse<ShopeeAttributeResponseV2>>(msg => msg.StatusCode != HttpStatusCode.OK)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval), (res, timeSpan, retryCount, context) =>
                        { }).ExecuteAsync(() => ShopeeHelperV2.GetAttributes(auth, platform, categoryId, isCallGrpc));

                if (!string.IsNullOrEmpty(response.Data?.Error))
                    throw new ShopeeException(response.Data?.Error);

                var attributes = response.Data?.Response?.AttributeList?.Select(x =>
                    {
                        var result = x.ConvertTo<PlatformChannel.ShopeeAttribute>();
                        result.Values = x.Values?.Select(value => new PlatformChannel.ShopeeAttributeValue
                        {
                            ValueId = value.ValueId,
                            OriginalValue = value.OriginalValue,
                            TranslateValue = value.TranslateValue,
                        }).ToList() ?? new List<PlatformChannel.ShopeeAttributeValue>();
                        return result;
                    })
                    .ToList();
                getAttributesByShopIdLog.ResponseObject = attributes;
                getAttributesByShopIdLog.LogInfo();
                return attributes;
            }
            catch (Exception e)
            {
                getAttributesByShopIdLog.LogError(e);
                throw new ShopeeException("Lỗi lấy dữ liệu thuộc tính.");
            }
        }

        public static long ToInt64(object obj)
        {
            long retVal;

            try
            {
                retVal = Convert.ToInt64(obj);
            }
            catch
            {
                retVal = 0;
            }

            return retVal;
        }

        public async Task<ShopeeProductResponse> PushProductToChannel(int retailerId, long channelId, PlatformChannel.ChannelAuth auth,
            Guid logId, ShopeeProduct req, PlatformChannel.Platform platform, string kvProductAttributeStr)
        {
            var log = new LogObject(Log, logId)
            {
                Action = "PushProductToChannel",
                RetailerId = retailerId,
                OmniChannelId = channelId,
                RequestObject = auth.ShopId
            };
            try
            {
                var logisticIdsIsFreeStr = AppSettings.Get("logisticIdsIsFree", "59999");
                var logisticIdsIsFree = !string.IsNullOrEmpty(logisticIdsIsFreeStr)
                    ? logisticIdsIsFreeStr.Split(',').Select(ToInt64).ToList()
                    : new List<long>();

                var newReqV2 = new ShopeeProductRequestV2();
                newReqV2.CopyFrom(req, logisticIdsIsFree);

                var isCallGrpc = SelfAppConfig.ForwarderApiFeature.DefaultForwarder;
                var response = await Policy.HandleResult<IRestResponse<ShopeeProductResponse>>(msg => msg.StatusCode != HttpStatusCode.OK)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval), (res, timeSpan, retryCount, context) =>
                        { }).ExecuteAsync(() => ShopeeHelperV2.PushProductToChannel(newReqV2, auth, platform, isCallGrpc));
                response.Data.Message = response.Data?.MessageV2;

                if (!string.IsNullOrEmpty(response.Data.Error))
                {
                    return new ShopeeProductResponse()
                    {
                        ShopId = auth.ShopId.ToString(),
                        Error = response.Data.Error,
                        Message = response.Data.MessageV2,
                    };
                }

                if (!req.Variations.Any()) return response.Data;

                await Task.Delay(SelfAppConfig.Shopee.TimeDelayPushProduct);
                var resultVariation = await PushProductInitTierVariation(retailerId, channelId, auth, logId, req, platform,
                      response.Data.Response.ItemId, kvProductAttributeStr);
                if (!string.IsNullOrEmpty(resultVariation?.Error))
                {
                    await DeleteProductById(retailerId, channelId, auth, logId, platform, response.Data.Response.ItemId);
                    return new ShopeeProductResponse()
                    {
                        ShopId = auth.ShopId.ToString(),
                        Error = resultVariation.Error,
                        Message = resultVariation.Msg,
                    };
                }

                return response.Data;

            }
            catch (ShopeePushProductException e)
            {
                log.LogError(e);
                return new ShopeeProductResponse()
                {
                    ShopId = auth.ShopId.ToString(),
                    Error = e.Message,
                };
            }
        }

        public async Task PushProductModelToChannel(int retailerId, long channelId, PlatformChannel.ChannelAuth auth,
            Guid logId, ShopeeProduct req, PlatformChannel.Platform platform, long itemId)
        {
            var log = new LogObject(Log, logId)
            {
                Action = "PushProductModelToChannel",
                RetailerId = retailerId,
                OmniChannelId = channelId,
                RequestObject = auth.ShopId
            };

            var newReqV2 = new ShopeeModelRequestV2();
            newReqV2.CopyFrom(req, itemId);

            var isCallGrpc = SelfAppConfig.ForwarderApiFeature.DefaultForwarder;
            var result = await Policy.HandleResult<IRestResponse<ShopeeAddModelResponseV2>>(msg => msg.StatusCode != HttpStatusCode.OK)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval), (res, timeSpan, retryCount, context) =>
                    { }).ExecuteAsync(() => ShopeeHelperV2.PushModelProductToChannel(newReqV2, auth, platform, isCallGrpc));
            if (!string.IsNullOrEmpty(result?.Data?.Error))
                throw new ShopeePushProductException(result?.Data?.Error);
        }

        public async Task<ShopeeDeleteProductByIdResponseV2> DeleteProductById(int retailerId, long channelId, PlatformChannel.ChannelAuth auth,
            Guid logId, PlatformChannel.Platform platform, long itemId)
        {
            var log = new LogObject(Log, logId)
            {
                Action = "PushProductVariationToChannel",
                RetailerId = retailerId,
                OmniChannelId = channelId,
                RequestObject = auth.ShopId
            };
            try
            {
                var isCallGrpc = SelfAppConfig.ForwarderApiFeature.DefaultForwarder;
                var result = await Policy.HandleResult<IRestResponse<ShopeeDeleteProductByIdResponseV2>>(msg => msg.StatusCode != HttpStatusCode.OK)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval), (res, timeSpan, retryCount, context) =>
                    { }).ExecuteAsync(() => ShopeeHelperV2.DeleteProductById(itemId, auth, platform, isCallGrpc));

                return result.Data;
            }
            catch (ShopeePushProductException ex)
            {
                log.LogError(ex);
                throw;
            }
        }

        public async Task<ShopeeInitTierVariationResponseV2> PushProductInitTierVariation(int retailerId, long channelId, PlatformChannel.ChannelAuth auth,
            Guid logId, ShopeeProduct req, PlatformChannel.Platform platform, long itemId, string kvProductAttributeStr)
        {
            var log = new LogObject(Log, logId)
            {
                Action = "PushProductVariationToChannel",
                RetailerId = retailerId,
                OmniChannelId = channelId,
                RequestObject = auth.ShopId
            };
            try
            {
                var newReqV2 = new ShopeeInitTierRequestV2();
                newReqV2.CopyFrom(req, itemId, kvProductAttributeStr);

                var isCallGrpc = SelfAppConfig.ForwarderApiFeature.DefaultForwarder;
                var response = await Policy.HandleResult<IRestResponse<ShopeeInitTierVariationResponseV2>>(msg =>
                {
                    log.ResponseObject = msg?.Content;
                    log.LogWarning();

                    if (msg.StatusCode == HttpStatusCode.GatewayTimeout) return true;
                    if (SelfAppConfig.Shopee.ErrorRetryPushProduct.Any(s => msg.Data.Msg.Contains(s))) return true;
                    return false;
                })
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (res, timeSpan, retryCount, context) => { })
                .ExecuteAsync(() => ShopeeHelperV2.PushProductInitTierVariation(newReqV2, auth, platform, isCallGrpc));

                return response.Data;
            }
            catch (ShopeePushProductException ex)
            {
                log.LogError(ex);
                throw;
            }
        }

        public async Task<PlatformChannel.CategoriesResp> GetCategories(int retailerId, long channelId, PlatformChannel.ChannelAuth auth, Guid logId,
            PlatformChannel.Platform platform)
        {
            var log = new LogObject(Log, logId)
            {
                Action = "GetCategories",
                RetailerId = retailerId,
                OmniChannelId = channelId,
                RequestObject = auth.ShopId
            };
            try
            {
                var isCallGrpc = SelfAppConfig.ForwarderApiFeature.DefaultForwarder;
                var response = await Policy.HandleResult<IRestResponse<ShopeeCategoryResponseV2>>(msg => msg.StatusCode != HttpStatusCode.OK)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval), (res, timeSpan, retryCount, context) =>
                    {
                        Log.Warn(
                            $"GetCategories with {res.Result.StatusCode} - RequestId: {res.Result?.Data?.RequestId}. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount}");
                    }).ExecuteAsync(() => ShopeeHelperV2.GetCategories(auth, platform, auth.ShopId, isCallGrpc));
                log.LogInfo();
                if (!string.IsNullOrEmpty(response.Data?.Error))
                    throw new ShopeeException(response.Data?.Msg);

                var result = new PlatformChannel.CategoriesResp();
                result.Categories = response.Data?.Response?.CategoryList?.Select(x => x.ConvertTo<Categories>())
                    .ToList() ?? new List<Categories>();
                return result;
            }
            catch (Exception e)
            {
                log.LogError(e);
                throw new ShopeeException(e.Message);
            }
        }

        public async Task<(bool hasNextPage, int nextOffSet, List<ShopeeModelBrandResponseV2> brandList)> GetBrands(int categoryId, PlatformChannel.ChannelAuth auth, Guid logId,
            PlatformChannel.Platform platform, int nextOffSet)
        {
            var log = new LogObject(Log, logId)
            {
                Action = "GetCategories",
                RequestObject = auth.ShopId
            };
            try
            {
                var isCallGrpc = SelfAppConfig.ForwarderApiFeature.DefaultForwarder;
                var response = await Policy.HandleResult<IRestResponse<ShopeeBrandResponseV2>>(msg => msg.StatusCode != HttpStatusCode.OK)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval), (res, timeSpan, retryCount, context) =>
                    {
                        Log.Warn(
                            $"GetCategories with {res.Result.StatusCode} - RequestId: {res.Result?.Data?.RequestId}. " +
                            $"Waiting {timeSpan:g} before next retry. Retry attempt {retryCount}");
                    }).ExecuteAsync(() => ShopeeHelperV2.GetBrands(auth, platform, categoryId, nextOffSet, isCallGrpc));
                log.LogInfo();
                if (!string.IsNullOrEmpty(response?.Data?.Error))
                    throw new ShopeeException(response.Data?.Error);

                return (response?.Data?.Response?.HasNextPage ?? false, response?.Data?.Response?.NextOffSet ?? 0,
                    response.Data?.Response?.BrandList.ToList() ?? new List<ShopeeModelBrandResponseV2>());
            }
            catch (Exception e)
            {
                log.LogError(e);
                throw new ShopeeException("Lỗi lấy dữ liệu nhãn hàng.");
            }
        }

        public Task<ShopeeUploadImgResponse> UploadImg(PlatformChannel.ChannelAuth auth, int retailerId, long shopId,
            List<Image> images, Guid logId, PlatformChannel.Platform platform)
        {
            throw new NotImplementedException();
        }

        public async Task<List<ShopeeeUploadImageReponseV2>> UploadImgV2(PlatformChannel.ChannelAuth auth, int retailerId, long shopId,
            List<Image> images, Guid logId, PlatformChannel.Platform platform)
        {
            var urls = images.Where(x => x.Url != null).Select(x => x.Url).ToList();
            var tasks = new List<Task<ShopeeeUploadImageReponseV2>>();
            foreach (var url in urls)
            {
                tasks.Add(UploadImgFromUrl(auth, platform, retailerId, shopId, logId, url));
            }
            await Task.WhenAll(tasks);

            var result = new List<ShopeeeUploadImageReponseV2>();
            tasks.ForEach(task =>
            {
                result.Add(task.Result);
            });
            return result;
        }

        public async Task<List<ShopeeeUploadImageReponseV2>> UploadImgV2FromData(PlatformChannel.ChannelAuth auth, int retailerId, long shopId,
            List<ShopeeModelImageUploadV2> images, Guid logId, PlatformChannel.Platform platform)
        {
            var tasks = new List<Task<ShopeeeUploadImageReponseV2>>();
            foreach (var image in images)
            {
                tasks.Add(UploadImgByteData(auth, platform, retailerId, shopId, logId, image.Data, image.FileName));
            }
            await Task.WhenAll(tasks);

            var result = new List<ShopeeeUploadImageReponseV2>();
            tasks.ForEach(task =>
            {
                result.Add(task.Result);
            });
            return result;
        }

        public Task<ProductListResponse> GetListDeletedProduct(PlatformChannel.ChannelAuth auth, long channelId, Guid logId,
            bool isFirstSync, KvInternalContext kvContext = null, DateTime? updateTimeFrom = null)
        {
            throw new NotImplementedException();
        }

        public Task<PlatformChannel.KvOrder> GenListKvOrder(long shopId, int? saleChannelId, int branchId, long userId, int retailerId,
            PlatformChannel.OrderDetail shopeeOrder,
            PlatformChannel.OmniChannelSettingObject settings, LogObjectMicrosoftExtension logInfo, PlatformChannel.Platform platform)
        {
            throw new NotImplementedException();
        }

        Task<CustomerResponse> IBaseClient.GetCustomerByOrderId(int retailerId, long channelId, string orderId,
            PlatformChannel.ChannelAuth auth, Guid logId)
        {
            throw new NotImplementedException();
        }

        public Task<List<PlatformChannel.OrderDetail>> GetListOrderDetail(PlatformChannel.ChannelAuth auth, string[] orderSnList,
            LogObjectMicrosoftExtension logInfo, PlatformChannel.Platform platform)
        {
            throw new NotImplementedException();
        }

        public List<PlatformChannel.KvInvoice> GenKvInvoices(CreateInvoiceRequest request)
        {
            var kvOrderNew = JsonConvert.DeserializeObject<KvOrder>(request.Order);
            var shopeeOrderDetail = kvOrderNew.ChannelOrder.FromJson<OrderDetail>();
            var kvOrder = request.KvOrder;

            if (kvOrder == null || request.KvDelivery == null) return new List<KvInvoice>();

            if (kvOrderNew.ChannelStatus == ShopeeStatus.Unpaid ||
                string.IsNullOrEmpty(kvOrderNew.OrderDelivery?.DeliveryCode) &&
                shopeeOrderDetail.ShippingCarrier != ShopeeShipingCarrier.SellerShipping
            ) return new List<KvInvoice>();

            var shopeeChannelBusiness = new ShopeeBusinessImplimentBehavior(AppSettings);

            //Khởi tạo invoice
            var kvInvoice = new KvInvoice
            {
                Code = $"HDSPE_{request.OrderId}",
                RetailerId = kvOrder.RetailerId,
                OrderId = kvOrder.Id,
                BranchId = kvOrder.BranchId,
                Description = kvOrder.Description,
                PurchaseDate = kvOrderNew.PurchaseDate.AddSeconds(1), //add 1s do kv đang validate không trùng purchase order
                CustomerId = kvOrder.CustomerId,
                UsingCod = true,
                PriceBookId = kvOrder.Extra?.FromJson<Extra>().PriceBookId?.Id ?? 0,
                DeliveryDetail = new InvoiceDelivery
                {
                    Receiver = request.KvDelivery?.Receiver,
                    ContactNumber = request.KvDelivery?.ContactNumber,
                    DeliveryCode = kvOrderNew.OrderDelivery?.DeliveryCode,
                    Address = request.KvDelivery?.Address,
                    UsingPriceCod = true,
                    WardName = request.KvDelivery?.WardName,
                    LocationName = request.KvDelivery?.LocationName,
                    LocationId = request.KvDelivery?.LocationId,
                    Status = (byte)DeliveryStatus.Pending,
                    DeliveryBy = request.KvDelivery.PartnerId,
                    FeeJson = kvOrderNew.OrderDelivery?.FeeJson,
                    Price = kvOrderNew.OrderDelivery?.Price,
                    LatestStatus = kvOrderNew.DeliveryStatus ?? (byte)DeliveryStatus.Pending
                },
                Status = (int)InvoiceState.Pending,
                SaleChannelId = kvOrder.SaleChannelId,
                InvoiceDetails = new List<InvoiceDetail>(),
                NewStatus = shopeeChannelBusiness.ConvertToKvInvoiceStatus(kvOrderNew.NewStatus),
                ChannelStatus = kvOrderNew.ChannelStatus,
                Discount = kvOrderNew.Discount,
                SurCharges = kvOrderNew.SurCharges,
                DeliveryStatus = kvOrderNew.DeliveryStatus
            };

            //Cập nhật invoice detail
            foreach (var detail in kvOrderNew.OrderDetails)
            {
                kvInvoice.InvoiceDetails.Add(new InvoiceDetail
                {
                    ProductChannelId = detail.ProductChannelId,
                    ParentChannelProductId = detail.ParentChannelProductId,
                    ProductChannelName = detail.ProductChannelName,
                    ProductChannelSku = detail.ProductChannelSku,
                    Quantity = detail.Quantity,
                    Price = detail.Price,
                    ItemId = detail.ItemId,
                    VariationId = detail.VariationId,
                    AddOnDealId = detail.AddOnDealId,
                    DiscountPrice = detail.DiscountPrice,
                    Discount = detail.Discount,
                    IsUsed = detail.IsUsed,
                    Uuid = detail.Uuid,
                    UseWarranty = detail.UseWarranty
                });
            }
            kvInvoice.InvoiceWarranties = kvOrderNew.InvoiceWarranties;

            return new List<KvInvoice> { kvInvoice };
        }

        public async Task<(bool, List<PlatformChannel.Transaction>)> GetListTransaction(PlatformChannel.ChannelAuth auth, DateTime createFrom, DateTime createTo,
            LogObjectMicrosoftExtension log, PlatformChannel.Platform platform)
        {
            var logGetListTransaction = log.Clone("ShopeeGetListTransaction");
            logGetListTransaction.RequestObject = $"{new { auth.ShopId, FromDate = createFrom.ToUnixTime(), ToDate = createTo.ToUnixTime(), ChannelId = log.OmniChannelId, log.RetailerId, log.BranchId }}";
            var transactions = new List<PlatformChannel.Transaction>();
            var pageSize = 100;
            var lastSyncSuccess = createFrom.ToUnixTime();
            var dateRanges = DateHelper.SplitDateRange(createFrom, createTo, 14);

            foreach (var tuple in dateRanges)
            {
                var fromDate = tuple.Item1.ToUnixTime();
                var toDate = tuple.Item2.ToUnixTime();
                var hasMore = true;
                var pageNo = 1;

                while (hasMore)
                {
                    var cts = new CancellationTokenSource();
                    var waitRetryPolicy = Policy.HandleResult<IRestResponse<WalletTransactionListResponseV2>>(msg => msg.StatusCode != HttpStatusCode.OK)
                        .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, j => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval), (res, timeSpan, retryCount, context) =>
                        {
                            if (res.Exception != null)
                            {
                                logGetListTransaction.LogError(res.Exception);
                            }
                            else
                            {
                                logGetListTransaction.LogWarning($"GetListTransactionWaitAndRetry with {res.Result?.StatusCode} - RequestId: {res.Result?.Data?.RequestId}. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount} - ResultObject: {res.Result}");
                            }
                            cts.Cancel();
                            cts.Dispose();
                            cts = new CancellationTokenSource();
                        });
                    var response = await waitRetryPolicy.ExecuteAsync(() => ShopeeHelperV2.GetListTransaction(auth, pageNo, pageSize, fromDate, toDate, platform, logGetListTransaction));

                    if (!string.IsNullOrEmpty(response.Data?.Error) || !string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        logGetListTransaction.ResponseObject = $"Error-ListTransactions {transactions.Count}, Lastsync: {lastSyncSuccess}, Error: {response.Data?.Error}, Error Message: {response.ErrorMessage}";
                        logGetListTransaction.LogError(new Exception(response.Data?.Error));
                        return (false, transactions);
                    }
                    pageNo++;

                    if (response.Data.Response.TransactionList != null && response.Data.Response.TransactionList.Any())
                    {
                        transactions.AddRange(response.Data.Response.TransactionList);
                    }
                    hasMore = response?.Data?.Response?.HasMore ?? false;
                }
                lastSyncSuccess = toDate;
            }

            logGetListTransaction.ResponseObject = $"ListTransactions {transactions.Count}, Lastsync: {lastSyncSuccess}";
            logGetListTransaction.LogInfo();

            return (true, transactions);
        }

        public Task<(bool, List<PlatformChannel.Transaction>)> GetListTransaction(int shopId, DateTime createFrom, DateTime createTo, LogObjectMicrosoftExtension log, PlatformChannel.Platform platform)
        {
            throw new NotImplementedException();
        }

        public Task<PlatformChannel.SellerWareHouseV2Response> GetTikiWarehouse(string accessToken)
        {
            throw new NotImplementedException();
        }

        public Task<(bool, List<PlatformChannel.FinanceTransaction>)> GetFinanceTransactions(string token, long channelId, string orderSn, DateTime? startTime, DateTime? endTime, LogObjectMicrosoftExtension logInfo, bool isBreakIfMaximum)
        {
            throw new NotImplementedException();
        }

        public async Task<ShopeeModelListReponseV2> GetModelList(long itemId, long channelId, Guid logId,
           PlatformChannel.ChannelAuth auth, PlatformChannel.Platform platform)
        {
            var response = await Policy.HandleResult<IRestResponse<ShopeeModelListReponseV2>>(ms => ms.StatusCode != HttpStatusCode.OK)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (result, timeSpan, retryCount) =>
                    {
                        Log.Warn(
                            $"GetShopInfoWaitAndRetryAsync with {result.Result.StatusCode}. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount}");
                    }).ExecuteAsync(() => ShopeeHelperV2.GetModelList(itemId, auth, platform));

            return response.Data;
        }

        #region Private method
        private async Task<ShopeeeUploadImageReponseV2> UploadImgByteData(PlatformChannel.ChannelAuth auth, PlatformChannel.Platform platform, int retailerId, long shopId, Guid logId, byte[] data, string fileName)
        {
            var log = new LogObject(Log, logId)
            {
                Action = "UploadImagesV2",
                RetailerId = retailerId,
                RequestObject = shopId
            };
            try
            {
                var isCallGrpc = SelfAppConfig.ForwarderApiFeature.DefaultForwarder;
                var response = await Policy
                    .HandleResult<IRestResponse<ShopeeeUploadImageReponseV2>>(msg => msg.StatusCode != HttpStatusCode.OK)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry,
                        i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                        (res, timeSpan, retryCount, context) => { }).ExecuteAsync(() => ShopeeHelperV2.UploadImg("image", data, fileName, auth, platform, isCallGrpc));
                if (!string.IsNullOrEmpty(response.Data?.Error))
                    throw new ShopeePushProductException(response.Data?.Error);
                return response.Data;
            }
            catch (ShopeePushProductException e)
            {
                log.LogError(e);
                throw new ShopeePushProductException("Lỗi up ảnh. Vui lòng kiểm tra lại định dạng hoặc kính thước ảnh");
            }
        }

        private async Task<ShopeeeUploadImageReponseV2> UploadImgFromUrl(PlatformChannel.ChannelAuth auth, PlatformChannel.Platform platform, int retailerId, long shopId, Guid logId, string fileUrl)
        {
            var log = new LogObject(Log, logId)
            {
                Action = "UploadImagesV2",
                RetailerId = retailerId,
                RequestObject = shopId
            };
            try
            {
                var isCallGrpc = SelfAppConfig.ForwarderApiFeature.DefaultForwarder;
                var data = (new WebClient()).DownloadData(fileUrl);
                var response = await Policy
                    .HandleResult<IRestResponse<ShopeeeUploadImageReponseV2>>(msg => msg.StatusCode != HttpStatusCode.OK)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry,
                        i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                        (res, timeSpan, retryCount, context) => { }).ExecuteAsync(() => ShopeeHelperV2.UploadImg("image", data, GetFileNameFromUrl(fileUrl), auth, platform, isCallGrpc));
                if (!string.IsNullOrEmpty(response.Data?.Error))
                    throw new ShopeePushProductException(response.Data?.Error);
                return response.Data;
            }
            catch (ShopeePushProductException e)
            {
                log.LogError(e);
                throw new ShopeePushProductException("Lỗi up ảnh. Vui lòng kiểm tra lại định dạng hoặc kính thước ảnh");
            }
        }

        private string GetFileNameFromUrl(string url)
        {
            if (string.IsNullOrEmpty(url)) return string.Empty;
            var urls = url.Split('/');
            return urls[urls.Length - 1];
        }

        private async Task<ShopeeModelListReponseV2> GetModelListWithHandleException(long itemId, long channelId, Guid logId,
        PlatformChannel.ChannelAuth auth, PlatformChannel.Platform platform)
        {
            try
            {
                return await GetModelList(itemId, channelId, logId, auth, platform);
            }
            catch
            {
                return null;
            }
        }

        private async Task<OrderDetailV2> GetOrderDetailPolly(string orderId, PlatformChannel.ChannelAuth auth, LogObjectMicrosoftExtension loggerExtension, PlatformChannel.Platform platform = null)
        {
            loggerExtension.EcommerceRequest = Ecommerce.Shopee;
            loggerExtension.Action = "ShopeeGetOrderDetailV2";
            var timeout = SelfAppConfig.ForwarderApiFeature.DefaultForwarder;
            var cts = new CancellationTokenSource();
            var timeoutPolicy = Policy
               .TimeoutAsync(SelfAppConfig.ChannelTimeoutSecond, TimeoutStrategy.Pessimistic);
            var waitRetryPolicy = Policy.HandleResult<IRestResponse<BaseShopeeV2Response<ShopeeOrderDetailV2Response>>>(msg => msg.StatusCode != HttpStatusCode.OK)
                 .Or<TimeoutRejectedException>()
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, j => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval), (res, timeSpan, retryCount, context) =>
                {
                    if (res.Exception is TimeoutRejectedException)
                    {
                        timeout = SelfAppConfig.ForwarderApiFeature.Enable;
                        var msgErr = $"Timed out after {timeSpan.TotalSeconds} seconds, task cancelled: TimeoutRejectedException";
                        loggerExtension.LogError(new ShopeeException(msgErr));
                    }
                    if (res.Exception != null)
                    {
                        loggerExtension.LogError(res.Exception);
                    }
                    else
                    {
                        var ex = new ShopeeException(
                            $"ShopeeGetOrdersDetailWaitAndRetry with {res.Result?.StatusCode} - RequestId: {res.Result?.Data?.RequestId}. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount} - ResultObject: {res.Result}");
                        loggerExtension.LogError(ex);
                    }
                    cts.Cancel();
                    cts.Dispose();
                    cts = new CancellationTokenSource();
                });
            var wrapPolly = waitRetryPolicy.WrapAsync(timeoutPolicy);
            var orders = await wrapPolly.ExecuteAsync(() => ShopeeHelperV2.GetOrdersDetail(auth, orderId, platform, timeout));
            if (!string.IsNullOrEmpty(orders?.Data?.Error))
            {
                var msgErr = $"error: {orders?.Data.Error} - msg: {orders.Data.Msg}";
                var exception = new ShopeeException(msgErr);
                loggerExtension.LogError(exception);
                throw exception;
            }
            return orders?.Data.Response.OrderDetails.FirstOrDefault();
        }

        private async Task HandleSellerNoPermission(KvInternalContext kvContext, long channelId, string message, Guid logId)
        {
            if (!string.IsNullOrEmpty(message) && SelfAppConfig.ShopeeSellerNeedRegisterMsg != null &&
                            SelfAppConfig.ShopeeSellerNeedRegisterMsg.Any() &&
                            SelfAppConfig.ShopeeSellerNeedRegisterMsg.Contains(message))
            {
                await _integrationInternalClient.InternalUpdateRegisterChannel(kvContext, channelId, "Shopee", logId.ToString());
            }
        }

        private async Task<ShopeeTrackingInfoV2Response> GetTrackingInfoPolly(string orderId, PlatformChannel.ChannelAuth auth, LogObjectMicrosoftExtension loggerExtension, PlatformChannel.Platform platform = null)
        {
            loggerExtension.EcommerceRequest = Ecommerce.Shopee;
            loggerExtension.Action = "ShopeeTrackingInfoV2";
            var timeout = SelfAppConfig.ForwarderApiFeature.DefaultForwarder;
            var cts = new CancellationTokenSource();
            var timeoutPolicy = Policy
               .TimeoutAsync(SelfAppConfig.ChannelTimeoutSecond, TimeoutStrategy.Pessimistic);
            var waitRetryPolicy = Policy.HandleResult<IRestResponse<BaseShopeeV2Response<ShopeeTrackingInfoV2Response>>>(msg => msg.StatusCode != HttpStatusCode.OK)
                 .Or<TimeoutRejectedException>()
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, j => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval), (res, timeSpan, retryCount, context) =>
                {
                    if (res.Exception is TimeoutRejectedException)
                    {
                        timeout = SelfAppConfig.ForwarderApiFeature.Enable;
                        var msgErr = $"Timed out after {timeSpan.TotalSeconds} seconds, task cancelled: TimeoutRejectedException";
                        loggerExtension.LogError(new ShopeeException(msgErr));
                    }
                    if (res.Exception != null)
                    {
                        loggerExtension.LogError(res.Exception);
                    }
                    else
                    {
                        var ex = new ShopeeException(
                            $"ShopeeGetTrackingInfoWaitAndRetry with {res.Result?.StatusCode} - RequestId: {res.Result?.Data?.RequestId}. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount} - ResultObject: {res.Result}");
                        loggerExtension.LogError(ex);
                    }
                    cts.Cancel();
                    cts.Dispose();
                    cts = new CancellationTokenSource();
                });
            var wrapPolly = waitRetryPolicy.WrapAsync(timeoutPolicy);
            var orders = await wrapPolly.ExecuteAsync(() => ShopeeHelperV2.GetTrackingInfo(auth, orderId, platform, timeout));
            if (!string.IsNullOrEmpty(orders?.Data?.Error))
            {
                var msgErr = $"error: {orders?.Data.Error} - msg: {orders.Data.Msg}";
                var exception = new ShopeeException(msgErr);
                loggerExtension.LogError(exception);
                throw exception;
            }
            return orders?.Data.Response;
        }

        private async Task<ShopeePaymentEscrowDetailResponse> GetEscrowDetailPolly(string orderId, PlatformChannel.ChannelAuth auth, LogObjectMicrosoftExtension loggerExtension, PlatformChannel.Platform platform = null)
        {
            loggerExtension.EcommerceRequest = Ecommerce.Shopee;
            loggerExtension.Action = "ShopeeGetPaymentEscrowDetailV2";
            var timeout = SelfAppConfig.ForwarderApiFeature.DefaultForwarder;
            var cts = new CancellationTokenSource();
            var timeoutPolicy = Policy
               .TimeoutAsync(SelfAppConfig.ChannelTimeoutSecond, TimeoutStrategy.Pessimistic);
            var waitRetryPolicy = Policy.HandleResult<IRestResponse<BaseShopeeV2Response<ShopeePaymentEscrowDetailResponse>>>(msg => msg.StatusCode != HttpStatusCode.OK)
                 .Or<TimeoutRejectedException>()
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, j => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval), (res, timeSpan, retryCount, context) =>
                {
                    if (res.Exception is TimeoutRejectedException)
                    {
                        timeout = SelfAppConfig.ForwarderApiFeature.Enable;
                        var msgErr = $"Timed out after {timeSpan.TotalSeconds} seconds, task cancelled: TimeoutRejectedException";
                        loggerExtension.LogError(new ShopeeException(msgErr));
                    }
                    if (res.Exception != null)
                    {
                        loggerExtension.LogError(res.Exception);
                    }
                    else
                    {
                        var ex = new ShopeeException(
                            $"ShopeeGetOrdersDetailWaitAndRetry with {res.Result?.StatusCode} - RequestId: {res.Result?.Data?.RequestId}. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount} - ResultObject: {res.Result}");
                        loggerExtension.LogError(ex);
                    }
                    cts.Cancel();
                    cts.Dispose();
                    cts = new CancellationTokenSource();
                });
            var wrapPolly = waitRetryPolicy.WrapAsync(timeoutPolicy);
            var orders = await wrapPolly.ExecuteAsync(() => ShopeeHelperV2.GetPaymentEscrowDetail(auth, orderId, platform, timeout));
            if (!string.IsNullOrEmpty(orders?.Data?.Error))
            {
                var msgErr = $"error: {orders?.Data.Error} - msg: {orders.Data.Msg}";
                var exception = new ShopeeException(msgErr);
                loggerExtension.LogError(exception);
                throw exception;
            }
            return orders?.Data.Response;
        }

        private async Task<ProductListResponse> GetFullProductInfoList(PlatformChannel.ChannelAuth auth,
        KvInternalContext kvContext,
        long channelId,
        long? updateTimeFrom,
        long? updateTimeTo,
        bool needDeletedItems,
        LogObject loggerExtension,
        PlatformChannel.Platform platform)
        {
            var productFullInfoList = new ProductListResponse
            {
                Data = new List<Product>()
            };
            var shopeeConfig = SelfAppConfig.Shopee;
            var productGetList = await GetListProductWithTaskAsync(auth, kvContext, channelId, updateTimeFrom, updateTimeTo, needDeletedItems, loggerExtension, platform);
            if (productGetList == null || productGetList.Count == 0)
            {
                return productFullInfoList;
            }
            var deletedItemIdLst = productGetList.Where(item => item.ItemStatus.Equals(ShopeeProductStatus.Deleted) || item.ItemStatus.Equals(ShopeeProductStatus.Banned))
                .Select(item => item.ItemId.ToString())
                .ToList();
            productFullInfoList.RemovedProductIds.AddRange(deletedItemIdLst);

            var itemIdList = productGetList.Select(item => item.ItemId).ToList();
            var itemListBaseInfoList = await GetItemListBaseWithPageSize(auth, itemIdList, loggerExtension.Id, platform);

            var itemHasModelList = itemListBaseInfoList
                .Where(item => item.HasModel)
                .Select(item => item.ItemId)
                .Distinct()
                .ToList();

            productFullInfoList.ParentChangeIds.AddRange(itemHasModelList.Select(item => item.ToString()));
            var modelItemDic = await GetModelWithItemIdLst(auth, itemHasModelList, loggerExtension, platform, shopeeConfig.ProductDetailThread);
            ProcessModelProductLst(productFullInfoList, modelItemDic, itemListBaseInfoList);
            return productFullInfoList;
        }

        private void ProcessModelProductLst(ProductListResponse productFullInfoList,
            Dictionary<long, ModelListResponse> modelItemDic,
            List<ItemListBase> itemListBaseInfoList)
        {
            List<VariationProduct> variationProductActives = new List<VariationProduct>();
            List<Product> fullProductInfo = new List<Product>();

            foreach (var itemVariationProduct in modelItemDic)
            {
                VariationProduct variationProductInfo =
                    new VariationProduct(itemVariationProduct.Key, itemVariationProduct.Value.Model.Select(item => item.ModelId).ToList());
                variationProductActives.Add(variationProductInfo);
                var modelListByItemId = itemVariationProduct.Value.Model;
                var variationProductLst = modelListByItemId
                    .Select(item => new Product
                    {
                        ItemId = item.ModelId.ToString(),
                        ItemName = GetFullNameProduct(itemListBaseInfoList.FirstOrDefault(x => x.ItemId == itemVariationProduct.Key), item.ModelId, itemVariationProduct.Value),
                        ItemSku = item.ModelSku ?? String.Empty,
                        ItemImages = GetImagesProduct(itemListBaseInfoList.FirstOrDefault(x => x.ItemId == itemVariationProduct.Key), item.ModelId, itemVariationProduct.Value),
                        Type = (byte)ChannelProductType.Variation,
                        ParentItemId = itemVariationProduct.Key.ToString(),
                        Status = ShopeeProductStatus.Nomal
                    });
                fullProductInfo.AddRange(variationProductLst);
            }
            // Xóa hàng cha
            var deletedVariationProductIdLst = fullProductInfo
                .Where(item => productFullInfoList.RemovedProductIds.Contains(item.ParentItemId))
                .Select(item => item.ItemId).ToList();
            if (deletedVariationProductIdLst.Count > 0)
            {
                productFullInfoList.RemovedProductIds.AddRange(deletedVariationProductIdLst);
                fullProductInfo = fullProductInfo
                    .Where(item => !deletedVariationProductIdLst.Contains(item.ItemId)).ToList();
            }
            // Normal
            var normalProductInfo = itemListBaseInfoList.Where(item => !item.HasModel).ToList();
            var nomalProductLst = normalProductInfo
                .Where(item => item.ItemStatus != ShopeeProductStatus.Deleted
                && item.ItemStatus != ShopeeProductStatus.Banned)
                .Select(item => new Product
                {
                    ItemId = item.ItemId.ToString(),
                    ItemName = item.ItemName,
                    ItemSku = item.ItemSku,
                    ItemImages = item.Images?.ImageUrlList,
                    Type = (byte)ChannelProductType.Normal,
                    ParentItemId = item.ItemId.ToString(),
                    Status = item.ItemStatus
                }).ToList();
            fullProductInfo.AddRange(nomalProductLst);
            // Variation
            productFullInfoList.Data = fullProductInfo;
            productFullInfoList.Total = fullProductInfo.Count;
        }

        private List<string> GetImagesProduct(ItemListBase parentInfo, long modelId, ModelListResponse modelListInfo)
        {
            List<string> resultImageLst = new List<string>();
            if (parentInfo == null)
            {
                return new List<string>();
            }
            var modelInfo = modelListInfo.Model.FirstOrDefault(item => item.ModelId.Equals(modelId));
            if (modelInfo == null)
            {
                return parentInfo.Images?.ImageUrlList;
            }
            var tierVariationIndexLst = modelInfo.TierIndex;
            if (tierVariationIndexLst?.Count > 0)
            {
                for (int i = 0; i < tierVariationIndexLst.Count; i++)
                {
                    var tierVariationInfo = modelListInfo.TierVariations[i];
                    var infoImage = tierVariationInfo.OptionList[tierVariationIndexLst[i]].Image;
                    if (infoImage != null)
                    {
                        resultImageLst.Add(infoImage.ImageUrl);
                    }
                }
            }
            if (resultImageLst.Count == 0)
            {
                resultImageLst = parentInfo.Images?.ImageUrlList;
            }
            return resultImageLst;
        }

        private string GetFullNameProduct(ItemListBase parentInfo, long modelId, ModelListResponse modelListInfo)
        {
            if (parentInfo == null)
            {
                return string.Empty;
            }
            StringBuilder bulderNameProduct = new StringBuilder();
            bulderNameProduct.Append(parentInfo.ItemName);
            var separateText = " - ";
            var modelInfo = modelListInfo.Model.FirstOrDefault(item => item.ModelId.Equals(modelId));
            if (modelInfo == null)
            {
                return string.Empty;
            }
            var tierVariationIndexLst = modelInfo.TierIndex;
            if (tierVariationIndexLst?.Count > 0)
            {
                for (int i = 0; i < tierVariationIndexLst.Count; i++)
                {
                    var tierVariationInfo = modelListInfo.TierVariations[i];
                    var infoOption = tierVariationInfo.OptionList[tierVariationIndexLst[i]].Option;
                    if (!string.IsNullOrEmpty(infoOption))
                    {
                        bulderNameProduct.Append($"{separateText}{infoOption}");
                    }
                }
            }
            return bulderNameProduct.ToString();
        }

        private async Task<Dictionary<long, ModelListResponse>> GetModelWithItemIdLst(PlatformChannel.ChannelAuth auth, List<long> itemIdList, LogObject loggerExtension, PlatformChannel.Platform platform, int pageSize = 50)
        {
            Dictionary<long, ModelListResponse> resultDic = new Dictionary<long, ModelListResponse>();
            int totalCount = itemIdList.Count;
            var totalPages = (int)Math.Ceiling(totalCount / (decimal)pageSize);
            for (int page = 1; page <= totalPages; page++)
            {
                var taskLst = new List<Task<ShopeeModelListReponseV2>>();
                var itemIdPaging = itemIdList.Skip((page - 1) * pageSize).Take(pageSize).ToList();
                foreach (var itemId in itemIdPaging)
                {
                    taskLst.Add(GetModelListWithHandleException(itemId, 0, loggerExtension.Id, auth, platform));
                }
                var taskResultList = await Task.WhenAll(taskLst);
                for (int i = 0; i < taskResultList.Length; i++)
                {
                    var taskItemResult = taskResultList[i];
                    if (taskItemResult != null && taskItemResult.Response != null)
                    {
                        resultDic.Add(itemIdPaging[i], taskItemResult?.Response);
                    }
                }
            }
            return resultDic;
        }

        private async Task<List<ItemGetProducts>> GetListProductWithTaskAsync(PlatformChannel.ChannelAuth auth,
          KvInternalContext kvContext,
          long channelId,
          long? updateTimeFrom,
          long? updateTimeTo,
          bool needDeletedItems,
          LogObject loggerExtension,
          PlatformChannel.Platform platform)
        {
            var items = new List<ItemGetProducts>();
            int pageNo = 0;
            int pageSize = 100;
            var response = await GetProductListPolly(auth, kvContext, channelId, pageNo * pageSize, pageSize, updateTimeFrom, updateTimeTo, needDeletedItems, loggerExtension, platform);

            var totalItem = response?.Items?.Count ?? 0;
            var totalSumProduct = response?.TotalCount ?? 0;

            if (totalItem <= 0 || response == null || totalSumProduct == 0)
            {
                return items;
            }
            items.AddRange(response.Items);
            if (totalItem < totalSumProduct && response.HasNextPage)
            {
                int pages = (totalSumProduct / totalItem) + 1;
                var shopeeConfig = SelfAppConfig.Shopee;
                int taskCallMaxPerThread = shopeeConfig.ProductListThread;
                int numberOfBatchCallPageSize = (pages / taskCallMaxPerThread) + 1;
                for (int i = 0; i < numberOfBatchCallPageSize; i++)
                {
                    var taskLst = new List<Task<List<ItemGetProducts>>>();
                    List<int> pageNumberLst = GetPageNumberList(i, taskCallMaxPerThread);
                    foreach (var pageNumber in pageNumberLst)
                    {
                        taskLst.Add(GetGetProductListPollyResponse(auth, kvContext, channelId, pageNumber, pageSize, updateTimeFrom, updateTimeTo, needDeletedItems, loggerExtension, platform));
                    }
                    var taskResultList = await Task.WhenAll(taskLst);
                    foreach (var itemResult in taskResultList)
                    {
                        items.AddRange(itemResult);
                    }
                }
            }
            return items;
        }
        private async Task<List<ItemGetProducts>> GetGetProductListPollyResponse(PlatformChannel.ChannelAuth auth,
            KvInternalContext kvContext,
            long channelId,
            int pageNumber,
            int pageSize,
            long? updateTimeFrom,
            long? updateTimeTo,
            bool needDeletedItems,
            LogObject loggerExtension,
            PlatformChannel.Platform platform = null)
        {
            var response = await GetProductListPolly(auth, kvContext, channelId, pageNumber * pageSize, pageSize, updateTimeFrom, updateTimeTo, needDeletedItems, loggerExtension, platform);
            if (response != null && response.Items != null && response.Items.Count > 0)
            {
                return response.Items;
            }
            return new List<ItemGetProducts>();
        }

        private List<int> GetPageNumberList(int index, int taskCallMaxPerThread)
        {
            int fromPageNumber = index * taskCallMaxPerThread; // = 0
            List<int> result = new List<int>();
            for (int i = 0; i < taskCallMaxPerThread; i++)
            {
                int pageNumber = fromPageNumber + i + 1;
                result.Add(pageNumber);
            }
            return result;
        }

        private async Task<List<ItemListBase>> GetProductListBaseInfoPolly(PlatformChannel.ChannelAuth auth,
           long[] itemIdLst,
           Guid logId,
           PlatformChannel.Platform platform)
        {
            var logProducts = new LogObject(Log, logId)
            {
                Action = "ShopeeGetProductListBaseInfoV2",
            };

            var timeout = SelfAppConfig.ForwarderApiFeature.DefaultForwarder;
            var cts = new CancellationTokenSource();
            var timeoutPolicy = Policy
               .TimeoutAsync(SelfAppConfig.ChannelTimeoutSecond, TimeoutStrategy.Pessimistic);
            var waitRetryPolicy = Policy.HandleResult<IRestResponse<BaseShopeeV2Response<ShopeeGetProductBaseInfoResponse>>>(msg => msg.StatusCode != HttpStatusCode.OK)
                 .Or<TimeoutRejectedException>()
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, j => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval), (res, timeSpan, retryCount, context) =>
                {
                    if (res.Exception is TimeoutRejectedException)
                    {
                        timeout = SelfAppConfig.ForwarderApiFeature.Enable;
                        var msgErr = $"Timed out after {timeSpan.TotalSeconds} seconds, task cancelled: TimeoutRejectedException";
                        logProducts.LogError(new ShopeeException(msgErr));
                    }
                    if (res.Exception != null)
                    {
                        logProducts.LogError(res.Exception);
                    }
                    else
                    {
                        var ex = new ShopeeException(
                            $"ShopeeGetProductListBaseInfoV2WaitAndRetry with {res.Result?.StatusCode} - RequestId: {res.Result?.Data?.RequestId}. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount} - ResultObject: {res.Result}");
                        logProducts.LogError(ex);
                    }
                    cts.Cancel();
                    cts.Dispose();
                    cts = new CancellationTokenSource();
                });
            var wrapPolly = waitRetryPolicy.WrapAsync(timeoutPolicy);
            var productLst = await wrapPolly.ExecuteAsync(() => ShopeeHelperV2.GetProductBaseInfo(auth, itemIdLst, platform, timeout));
            if (!string.IsNullOrEmpty(productLst?.Data?.Error))
            {
                var msgErr = $"error: {productLst?.Data.Error} - msg: {productLst.Data.Msg}";
                var exception = new ShopeeException(msgErr);
                logProducts.LogError(exception);
                throw exception;
            }
            logProducts.ResponseObject = productLst?.Content;
            logProducts.LogInfo();
            return productLst?.Data?.Response?.ItemList;
        }

        private async Task<ShopeeGetProductListV2Response> GetProductListPolly(PlatformChannel.ChannelAuth auth,
            KvInternalContext kvContext,
            long channelId,
            int offset,
            int pageSize,
            long? updateTimeFrom,
            long? updateTimeTo,
            bool needDeletedItems,
            LogObject loggerExtension,
            PlatformChannel.Platform platform = null)
        {
            loggerExtension.Action = "ShopeeGetProductListV2";
            var timeout = SelfAppConfig.ForwarderApiFeature.DefaultForwarder;
            var cts = new CancellationTokenSource();
            var timeoutPolicy = Policy
               .TimeoutAsync(SelfAppConfig.ChannelTimeoutSecond, TimeoutStrategy.Pessimistic);
            var waitRetryPolicy = Policy.HandleResult<IRestResponse<BaseShopeeV2Response<ShopeeGetProductListV2Response>>>(
                msg =>
                {
                    if (msg.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return false;
                    }
                    if (msg.StatusCode != HttpStatusCode.OK)
                    {
                        return true;
                    }
                    return false;
                })
                 .Or<TimeoutRejectedException>()
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, j => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval), (res, timeSpan, retryCount, context) =>
                {
                    if (res.Exception is TimeoutRejectedException)
                    {
                        timeout = SelfAppConfig.ForwarderApiFeature.Enable;
                        var msgErr = $"Timed out after {timeSpan.TotalSeconds} seconds, task cancelled: TimeoutRejectedException";
                        loggerExtension.LogError(new ShopeeException(msgErr));
                    }
                    if (res.Exception != null)
                    {
                        loggerExtension.LogError(res.Exception);
                    }
                    else
                    {
                        var ex = new ShopeeException(
                            $"ShopeeGetProductListV2WaitAndRetry with offset {offset} and pagezise : {pageSize} {res.Result?.StatusCode} - RequestId: {res.Result?.Data?.RequestId}. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount} - ResultObject: {res.Result}");
                        loggerExtension.LogError(ex);
                    }
                    cts.Cancel();
                    cts.Dispose();
                    cts = new CancellationTokenSource();
                });
            var wrapPolly = waitRetryPolicy.WrapAsync(timeoutPolicy);
            var productLst = await wrapPolly.ExecuteAsync(() => ShopeeHelperV2.GetItemList(auth, offset, pageSize, needDeletedItems, platform, updateTimeFrom, updateTimeTo, timeout));
            if (!string.IsNullOrEmpty(productLst?.Data?.Error))
            {
                await HandleSellerNoPermission(kvContext, channelId, productLst?.Data?.Msg, loggerExtension.Id);
                var msgErr = $"error: {productLst?.Data.Error} - msg: {productLst.Data.Msg} with offset {offset} and pagezise";
                var exception = new OmniException(msgErr);
                loggerExtension.LogError(exception);
                throw exception;
            }
            loggerExtension.ResponseObject = $"Products: {productLst?.Data.Response.ToSafeJson()}";
            loggerExtension.LogInfo();
            return productLst?.Data.Response;
        }
        #endregion
    }

    public class ShopeeModelImageUploadV2
    {
        public byte[] Data { get; set; }
        public string FileName { get; set; }
    }
}

﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Helper;
using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.RequestDTO;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Impls;
using Demo.OmniChannel.Sdk.Interfaces;
using Demo.OmniChannel.Sdk.RequestDtos;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Dtos;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.ShareKernel.Models;
using Newtonsoft.Json;
using Polly;
using RestSharp;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using ChannelProductType = Demo.OmniChannel.ChannelClient.Common.ChannelProductType;
using ChannelStatus = Demo.OmniChannel.ChannelClient.Common.ChannelStatus;
using Image = Demo.OmniChannel.ShareKernel.Dtos.Image;
using InvoiceDelivery = Demo.OmniChannel.ChannelClient.Models.InvoiceDelivery;
using InvoiceDetail = Demo.OmniChannel.ChannelClient.Models.InvoiceDetail;
using Logistics = Demo.OmniChannel.ChannelClient.Models.Logistics;
using OrderDetail = Demo.OmniChannel.ChannelClient.Models.OrderDetail;
using Platform = Demo.OmniChannel.ChannelClient.Models.Platform;
using ProductDetailResponse = Demo.OmniChannel.ChannelClient.RequestDTO.ProductDetailResponse;
using SelfAppConfig = Demo.OmniChannel.ChannelClient.Common.SelfAppConfig;
using TikiSellerStatus = Demo.OmniChannel.ChannelClient.Common.TikiSellerStatus;
using TokenResponse = Demo.OmniChannel.ChannelClient.Models.TokenResponse;

namespace Demo.OmniChannel.ChannelClient.Impls
{
    public class TikiClient : IBaseClient
    {
        private IChannelInternalClient _channelInternalClient;
        private SelfAppConfig _config;
        private ILog Log => LogManager.GetLogger(typeof(TikiClient));

        private const string ConnectErrorMsg = "Đồng bộ có lỗi phía Tiki, Vui lòng thực hiện đồng bộ lại sau ít phút";
        private const int TikiTooManyRequests = 429;
        private const string TikiTokenExpired = "token_expired";

        public TikiClient(IAppSettings settings)
        {
            _channelInternalClient = new ChannelInternalClient(settings);
            _config = new SelfAppConfig(settings);
        }

        public async Task<TokenResponse> CreateToken(string code, ChannelAuth auth, Platform platform)
        {
            var response = await Policy
                .HandleResult<IRestResponse<TokenResponse>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (res, timeSpan, retryCount, context) =>
                    {
                        Log.Warn(
                            $"Request failed with {res.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                    }).ExecuteAsync(() => TikiHelper.CreateToken(code));
            return response?.Data;
        }

        public Task<CustomerResponse> GetCustomerByOrderId(int retailerId, long channelId, string orderId, ChannelAuth auth, Guid logId)
        {
            return null;
        }

        public async Task<(byte?, byte?, byte?)> GetOrderStatus(int retailerId, long channelId, Guid logId,
            string orderId, ChannelAuth auth, LogObjectMicrosoftExtension loggerExtension,
            bool isGetDeliveryStatus = false, Platform platform = null, OmniChannelSettingObject settings = null)
        {
            var getOrderStatusLog = new LogObject(Log, logId)
            {
                Action = string.Format(EcommerceTypeException.GetOrderStatus, Ecommerce.Tiki),
                RetailerId = retailerId,
                OmniChannelId = channelId,
                RequestObject = orderId
            };


            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                getOrderStatusLog.ResponseObject = $"Auth:{auth.ToSafeJson()} EmptyAccessToken";
                getOrderStatusLog.LogError();
                throw new OmniException(string.Format(EcommerceTypeException.EmptyAccessToken, Ecommerce.Tiki));
            }

            var tikiOrderDetail = await GetOrderDetail(auth.AccessToken, orderId, loggerExtension);

            getOrderStatusLog.ResponseObject = tikiOrderDetail;
            getOrderStatusLog.LogInfo();

            var kvOrderStatus = ConvertToKvOrderStatus(tikiOrderDetail?.Status);
            var kvInvoiceStatus = ConvertToKvInvoiceStatus(tikiOrderDetail?.Status);
            var deliveryStatus = UpdateDeliveryStatusForInvoice(tikiOrderDetail?.Status);
            return (kvOrderStatus, kvInvoiceStatus, deliveryStatus);
        }

        public async Task<ShopInfoResponseV2> GetShopStatus(string accessToken)
        {
            var response = await Policy
                .HandleResult<IRestResponse<SellerInfoResponse>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (res, timeSpan, retryCount, ct) =>
                    {
                        Log.Warn($"Request Get Tiki seller info failed with {res.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");

                    }).ExecuteAsync(() => TikiHelper.GetSellerInfo(accessToken));
            if (response?.Data == null)
            {
                Log.Error($"{KeywordCommonSearch.CallTikiError} Get Tiki seller info: {new { Response = response?.Content }.ToJson()}");
                return null;
            }

            if (response.Data.Errors != null && response.Data.Errors.Any())
            {
                var errorMessage = response.Data.Errors.Join("; ");
                Log.Error($"{KeywordCommonSearch.CallTikiError} Get Tiki seller info: {new { ErrorMessages = errorMessage }.ToJson()}");
                throw new OmniException(errorMessage);
            }
            return new ShopInfoResponseV2
            {
                Email = response.Data.Email,
                Name = response.Data.Name,
                IdentityKey = response.Data.Id.ToString(),
                Status = ConvertSellerStatus(response.Data.RegistrationStatus),
                Active = response.Data.Active
            };
        }
        public async Task EnableUpdateProduct(string accessToken)
        {
            if (string.IsNullOrEmpty(accessToken))
            {
                throw new OmniException("Access Token invalid");
            }
            var response = await Policy
                         .HandleResult<IRestResponse<SellerUpdateCanEditProductResponse>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout)
                         .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                             (res, timeSpan, retryCount, ct) =>
                             {
                                 Log.Warn($"Request update Tiki can update product {res.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");

                             }).ExecuteAsync(() => TikiHelper.UpdateCanUpdateProduct(accessToken));

            Log.Info($"Response update Tiki can update product: {response?.Data?.ToSafeJson()}");
        }

        public async Task<ShopInfoResponseV2> GetShopInfo(ChannelAuth auth, KvInternalContext context,
            bool isOnlyGetInfo = false, Platform platform = null)
        {
            if (isOnlyGetInfo && !string.IsNullOrEmpty(auth?.AccessToken))
            {
                var shop = await TikiHelper.GetSellerInfo(auth.AccessToken);
                return new ShopInfoResponseV2
                {
                    Email = shop.Data.Email,
                    Name = shop.Data.Name,
                    IdentityKey = shop.Data.Id.ToString()
                };
            }
            if (string.IsNullOrEmpty(auth.Code))
            {
                throw new OmniException("Missing Authorization Code");
            }
            var tokenItem = await CreateToken(auth.Code, null, null);
            if (tokenItem == null || string.IsNullOrEmpty(tokenItem.AccessToken))
            {
                throw new OmniException("Access Token invalid");
            }
            var response = await Policy
                    .HandleResult<IRestResponse<SellerInfoResponse>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                        (res, timeSpan, retryCount, ct) =>
                        {
                            Log.Warn($"Request Get Tiki seller info failed with {res.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");

                        }).ExecuteAsync(() => TikiHelper.GetSellerInfo(tokenItem.AccessToken));

            if (response?.Data == null)
            {
                Log.Error($"{KeywordCommonSearch.CallTikiError} Get Tiki seller info: {new { Response = response?.Content }.ToJson()}");
                return null;
            }

            if (response.Data.Errors != null && response.Data.Errors.Any())
            {
                var errorMessage = response.Data.Errors.Join("; ");
                Log.Error($"{KeywordCommonSearch.CallTikiError} Get Tiki seller info: {new { ErrorMessages = errorMessage }.ToJson()}");
                throw new OmniException(errorMessage);
            }
            long authId = await _channelInternalClient.CreateChannelAuthAsync(context, new ChannelAuthRequest
            {
                AccessToken = tokenItem.AccessToken,
                RefreshToken = tokenItem.RefreshToken,
                ExpiresIn = tokenItem.ExpiresIn,
                RefreshExpiresIn = tokenItem.RefreshExpiresIn
            });

            return new ShopInfoResponseV2
            {
                AuthId = authId,
                Email = response.Data.Email,
                Name = response.Data.Name,
                IdentityKey = response.Data.Id.ToString(),
                Status = ConvertSellerStatus(response.Data.RegistrationStatus),
                IsActive = response.Data.Active == 1
            };
        }

        public async Task<ProductListResponse> GetListProduct(ChannelAuth auth, long channelId, Guid logId,
            bool isFirstSync, DateTime? updateTimeFrom = null, KvInternalContext kvContext = null,
            Platform platform = null)
        {
            var getGetProductsLog = new LogObject(Log, logId)
            {
                Action = string.Format(EcommerceTypeException.GetListProduct, Ecommerce.Tiki),
                OmniChannelId = channelId,
                RequestObject = $"LastSync: {updateTimeFrom}"
            };

            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                getGetProductsLog.ResponseObject = $"Auth:{auth.ToSafeJson()} EmptyAccessToken";
                getGetProductsLog.LogError();
                throw new OmniException(string.Format(EcommerceTypeException.EmptyAccessToken, Ecommerce.Tiki));
            }

            var result = new ProductListResponse();
            var pageNo = 1;
            var totalItem = SelfAppConfig.TikiProductPageSize;

            while (totalItem >= SelfAppConfig.TikiProductPageSize)
            {
                var response = await Policy
                    .HandleResult<IRestResponse<TikiProductsListResponse>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout || (int)msg.StatusCode == TikiTooManyRequests)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                        (res, timeSpan, retryCount) =>
                        {
                            Log.Warn(
                                $"Request failed with {res.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                        }).ExecuteAsync(() => TikiHelper.GetProducts(auth.AccessToken, pageNo, SelfAppConfig.TikiProductPageSize, updateTimeFrom));

                totalItem = response?.Data?.Products?.Count ?? 0;

                if (totalItem > 0 && response?.Data?.Products != null)
                {
                    var products = response.Data.Products.Where(x => (x.Inventory?.InventoryType == null || x.Inventory?.InventoryType.ToLower() != "instock")
                    && (x.Inventory?.FulfillmentType == null || x.Inventory?.FulfillmentType.ToLower() != "seller_delivery"))
                    .Select(x => new Product
                    {
                        ItemId = x.ProductId.ToString(),
                        BasePrice = x.Price,
                        SalePrice = x.Price,
                        ItemName = x.Name,
                        ItemSku = x.OriginalSku,
                        Status = x.Active == 1 ? "active" : "inactive",
                        Type = (byte)ChannelProductType.Normal,
                        ItemImages = string.IsNullOrEmpty(x.Thumbnail) ? new List<string>() : new List<string> { x.Thumbnail },
                        ParentItemId = x.MasterId.ToString(),
                        SuperId = x.SuperId,
                    });
                    result.Data.AddRange(products);

                    if (!isFirstSync)
                    {
                        var removedProductIds = response.Data.Products.Where(x => x.Inventory?.InventoryType?.ToLower() == "instock" || x.Inventory?.FulfillmentType?.ToLower() == "seller_delivery").Select(x => x.ProductId.ToString());
                        result.RemovedProductIds.AddRange(removedProductIds);
                    }
                }

                pageNo++;
            }

            getGetProductsLog.ResponseObject = $"ProductIds: {result.Data.Select(x => x.ItemId).ToSafeJson()}";
            getGetProductsLog.LogInfo();

            result.Total = result.Data.Count() + result.RemovedProductIds.Count();
            return result;
        }

        public async Task<(bool, string)> SyncOnHand(
            int retailerId, string kvProductSku, long channelId, Guid logId,
            ChannelAuth auth, string itemId, string itemSku, double onHand, byte productType,
            LogObjectMicrosoftExtension loggerExtension, string parentProductId = null, Platform platform = null)
        {
            loggerExtension.Action = string.Format(EcommerceTypeException.SyncOnHand, Ecommerce.Tiki);
            loggerExtension.RequestObject = $"ProductId: {itemId}, Onhand: {onHand}";

            try
            {
                if (string.IsNullOrEmpty(auth.AccessToken))
                {
                    var ex = new Exception(string.Format(EcommerceTypeException.EmptyAccessToken, Ecommerce.Tiki));
                    loggerExtension.ResponseObject = $"Auth:{auth.ToSafeJson()} EmptyAccessToken";
                    loggerExtension.LogError(ex);
                    throw ex;
                }

                var products = new List<Product>();
                var result = await UpdateProductStock(auth.AccessToken, itemId, onHand, null, loggerExtension);

                if (result.Data?.Errors?.Any() == true)
                {
                    var errorMsg = result.Data.Errors.Join("; ");
                    var exCustom = new TikiException(errorMsg);
                    loggerExtension.LogError(exCustom);
                    var res = new { Message = errorMsg };
                    return (false, res.ToSafeJson());
                }

                if (result.StatusCode != HttpStatusCode.OK)
                {
                    var res = new { Message = $"{ConnectErrorMsg} ({result.StatusCode})" };
                    return (false, res.ToSafeJson());
                }

                loggerExtension.ResponseObject = result.Data;
                loggerExtension.LogInfo();

                products.Add(new Product
                {
                    ItemId = itemId,
                    OnHand = (int)onHand
                });
                return (true, products.ToSafeJson());
            }
            catch (Exception ex)
            {
                loggerExtension.LogError(ex);
                throw;
            }
        }

        public async Task<(bool, string)> SyncMultiOnHand(
            int retailerId, long channelId, Guid logId, ChannelAuth auth,
            List<MultiProductItem> productItems, LogObjectMicrosoftExtension loggerExtension,
            byte productType, Platform platform)
        {
            var logSyncMultiOnHand = loggerExtension.Clone(string.Format(EcommerceTypeException.SyncMultiOnHand, Ecommerce.Tiki));

            try
            {
                logSyncMultiOnHand.RequestObject = $"(ProductId, Onhand): {productItems.Select(x => $"({x.ItemId}, {x.OnHand})").Join("; ")}";
                if (string.IsNullOrEmpty(auth.AccessToken))
                {
                    var ex = new Exception(string.Format(EcommerceTypeException.EmptyAccessToken, Ecommerce.Tiki));
                    logSyncMultiOnHand.ResponseObject = $"Auth:{auth.ToSafeJson()} EmptyAccessToken";
                    logSyncMultiOnHand.LogError(ex);
                    throw ex;
                }

                var products = new List<Product>();
                foreach (var item in productItems)
                {
                    var result = await UpdateProductStock(auth.AccessToken, item.ItemId, item.OnHand, null,
                        loggerExtension);
                    if (result.StatusCode != HttpStatusCode.OK || result.Data?.Errors?.Any() == true)
                    {
                        if (result.StatusCode != HttpStatusCode.BadRequest || result?.Data?.Errors == null ||
                            !result.Data.Errors.Any())
                        {
                            products.Add(new Product
                            {
                                ItemId = item.ItemId,
                                ErrorMessage = $"{ConnectErrorMsg} ({result.StatusCode})"
                            });
                        }
                        else
                        {
                            var errorMsg = result.Data.Errors.Join("; ");
                            if (!string.IsNullOrEmpty(errorMsg))
                            {
                                var exCustom = new Exception(errorMsg);
                                loggerExtension.LogError(exCustom);
                            }

                            products.Add(new Product
                            {
                                ItemId = item.ItemId,
                                ErrorMessage = errorMsg
                            });
                        }
                    }
                    else
                    {
                        products.Add(new Product
                        {
                            ItemId = item.ItemId,
                            OnHand = (int)item.OnHand
                        });
                    }
                }

                logSyncMultiOnHand.ResponseObject = products;
                logSyncMultiOnHand.LogInfo();

                var res = new { Product = products };
                return (true, res.ToSafeJson());
            }
            catch (Exception ex)
            {
                loggerExtension.LogError(ex);
                throw;
            }
        }

        public async Task<(bool, string)> SyncPrice(int retailerId, string kvProductSku, long channelId, Guid logId, ChannelAuth auth, string itemId, string itemSku, decimal? basePrice, byte productType, string parentItemId, LogObjectMicrosoftExtension loggerExtension, decimal? salePrice = null, DateTime? startSaleDateTime = null, DateTime? endSaleDateTime = null, Platform platform = null)
        {
            loggerExtension.Action = string.Format(EcommerceTypeException.SyncPrice, Ecommerce.Tiki);
            loggerExtension.RequestObject = $"ProductId: {itemId}, Price: {basePrice}";

            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                var ex = new OmniException(string.Format(EcommerceTypeException.EmptyAccessToken, Ecommerce.Tiki));
                loggerExtension.LogError(ex);
                throw ex;
            }

            var products = new List<Product>();
            var result = await UpdateProductTiki(auth.AccessToken, itemId, null, basePrice, loggerExtension);

            if (result.StatusCode != HttpStatusCode.OK || result.Data?.Errors?.Any() == true)
            {
                if (result.StatusCode != HttpStatusCode.BadRequest || result.Data?.Errors?.Any() == false)
                {
                    var res = new { Message = $"{ConnectErrorMsg} ({result.StatusCode})" };
                    return (false, res.ToSafeJson());
                }
                else
                {
                    var errorMsg = result.Data?.Errors?.Join("; ");
                    var res = new { Message = errorMsg };
                    return (false, res.ToSafeJson());
                }
            }

            products.Add(new Product
            {
                ItemId = itemId,
                BasePrice = basePrice ?? 0
            });
            loggerExtension.ResponseObject = products;
            loggerExtension.Description = "Product map after update product success";
            loggerExtension.LogInfo();

            loggerExtension.Description = null;
            loggerExtension.ResponseObject = null;

            return (true, products.ToSafeJson());
        }

        public async Task<(bool, string, int?)> SyncMultiPrice(int retailerId, long channelId, Guid logId,
            ChannelAuth auth, List<MultiProductItem> productItems, byte productType,
            LogObjectMicrosoftExtension loggerExtension,
            Platform platform)
        {
            loggerExtension.Action = string.Format(EcommerceTypeException.SyncMultiPrice, Ecommerce.Tiki);

            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                var ex = new OmniException(string.Format(EcommerceTypeException.EmptyAccessToken, Ecommerce.Tiki));
                loggerExtension.LogError(ex);
                throw ex;
            }


            List<Product> products = new List<Product>();
            foreach (var item in productItems)
            {
                loggerExtension.Action = string.Format(EcommerceTypeException.SyncMultiPriceSyncItem, Ecommerce.Tiki);

                var result = await UpdateProductTiki(auth.AccessToken, item.ItemId, null, item.Price, loggerExtension);

                if (result.StatusCode != HttpStatusCode.OK || (result?.Data?.Errors != null && result.Data.Errors.Any()))
                {
                    if (result.StatusCode != HttpStatusCode.BadRequest || result?.Data?.Errors == null || !result.Data.Errors.Any())
                    {
                        products.Add(new Product
                        {
                            ItemId = item.ItemId,
                            ErrorMessage = $"{ConnectErrorMsg} ({result.StatusCode})"
                        });
                    }
                    else
                    {
                        var stringProductError = result.Data.Errors.Join("; ");
                        products.Add(new Product
                        {
                            ItemId = item.ItemId,
                            ErrorMessage = stringProductError
                        });
                    }
                }
                else
                {
                    products.Add(new Product
                    {
                        ItemId = item.ItemId,
                        BasePrice = item.Price ?? 0,
                    });
                }
            }

            if (products.Any(x => !string.IsNullOrEmpty(x.ErrorMessage)))
            {
                var msgErr = products.Where(x => !string.IsNullOrEmpty(x.ErrorMessage)).Join(";");
                var ex = new TikiException(msgErr);
                loggerExtension.LogError(ex);
            }

            if (products.Any(x => string.IsNullOrEmpty(x.ErrorMessage)))
            {
                loggerExtension.Description = "Product map after update product success";
                loggerExtension.ResponseObject = products.Where(x => string.IsNullOrEmpty(x.ErrorMessage)).ToList();
                loggerExtension.LogInfo();

                loggerExtension.Description = null;
                loggerExtension.ResponseObject = null;
            }

            var restultRes = (new { Product = products });
            return (true, restultRes.ToSafeJson(), null);
        }

        public async Task<ProductDetailResponse> GetProductDetail(ChannelAuth auth, long channelId, Guid logId, object productId, object parentProductId, string productSku, Platform platform)
        {
            var getGetProductDetailLog = new LogObject(Log, logId)
            {
                Action = string.Format(EcommerceTypeException.GetProductDetail, Ecommerce.Tiki),
                OmniChannelId = channelId,
                RequestObject = $"ProductId: {productId} Sku:{productSku}"
            };

            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                getGetProductDetailLog.ResponseObject = $"Auth:{auth.ToSafeJson()} EmptyAccessToken";
                getGetProductDetailLog.LogError();
                throw new OmniException(string.Format(EcommerceTypeException.EmptyAccessToken, Ecommerce.Tiki));
            }

            var response = await Policy
                 .HandleResult<IRestResponse<TikiProductDetail>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout || (int)msg.StatusCode == TikiTooManyRequests)
                 .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                     (res, timeSpan, retryCount, context) =>
                     {
                         Log.Warn(
                             $"Request failed with {res.Result.StatusCode} - Channel {channelId}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                     }).ExecuteAsync(() => TikiHelper.GetProductDetail(auth.AccessToken, DataRequestHelper.ParseItemIdToLong(productId.ToString())));

            getGetProductDetailLog.ResponseObject = response.Data;
            getGetProductDetailLog.LogInfo();

            if (response?.Data == null)
            {
                return null;
            }
            var itemId = response.Data.ProductId;
            var productDetails = new List<ProductDetailSkus>
            {
                new ProductDetailSkus
                {
                    Sku = response.Data.Sku,
                    Name = response.Data.Name,
                    Images = response.Data.Images?.Select(y => y.Url).ToList(),
                    Status = "active",
                    Description = response.Data.Attributes?.Description
                }
            };

            var result = new ProductDetailResponse
            {
                ItemId = itemId.ToString(),
                ProductDetails = productDetails
            };
            return result;
        }

        public async Task<(bool, List<CreateOrderRequest>)> GetOrders(ChannelAuth auth, OrderRequest request,
            KvInternalContext context, LogObjectMicrosoftExtension logInfo, Platform platform)
        {
            var result = new List<CreateOrderRequest>();
            var pageNo = 1;
            var pageSize = SelfAppConfig.TikiOrderPageSize; //Tiki max 50
            var totalItem = pageSize;
            if (request.LastSync.GetValueOrDefault() == default(DateTime))
            {
                request.LastSync = DataRequestHelper.GetOrderLastSyncDateTime(request.SyncOrderFormula.GetValueOrDefault());
            }

            logInfo.Action = string.Format(EcommerceTypeException.GetOrders, Ecommerce.Tiki);
            logInfo.Description = "Tiki client v1";
            logInfo.RequestObject = $"Status: {request.Status} LastSync:{request.LastSync} V1";

            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                var ex = new OmniException($"Auth:{auth.ToSafeJson()} EmptyAccessToken");
                logInfo.LogError(ex);
                throw ex;
            }

            logInfo.LogInfo();

            string errorMessage = string.Empty;
            logInfo.EcommerceRequest = Ecommerce.Tiki;
            while (totalItem >= pageSize)
            {
                var response = await Policy.HandleResult<IRestResponse<TikiOrderResponse>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout || (int)msg.StatusCode == TikiTooManyRequests)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval), (res, timeSpan, retryCount, ct) =>
                    {
                        if (res?.Result?.Data?.Errors?.Any() == true)
                        {
                            var msgErr = $"statusCode: {res.Result.StatusCode} - error: {res.Result.Data.Errors.Join(",")}";
                            var ex = new TikiException(msgErr);
                            logInfo.LogError(ex);
                        }
                        else if (res?.Exception != null)
                        {
                            logInfo.LogError(res.Exception);
                        }
                        else
                        {
                            var msg =
                                $"Request failed with {res?.Result?.StatusCode} - ChannelId {logInfo.OmniChannelId}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}";
                            var ex = new OmniException(msg);
                            logInfo.LogError(ex);
                        }

                    }).ExecuteAsync(() => TikiHelper.GetOrders(auth.AccessToken, pageNo, pageSize, request.LastSync, logInfo));

                if (response?.Data?.Errors != null && response.Data.Errors.Any())
                {
                    errorMessage += $"PageIndex: {pageNo}, statusCode: {response.StatusCode}, Messages: {response.Data.Errors.Join("; ")} - ";
                }

                totalItem = response?.Data?.Orders?.Count ?? 0;

                if (totalItem > 0 && response?.Data?.Orders != null)
                {
                    foreach (var o in response.Data.Orders)
                    {
                        var isSellerDelivery = !string.IsNullOrEmpty(o.FulfillmentType) && o.FulfillmentType.ToLower() == "seller_delivery";
                        var isInstock = o.Items.Any(x => !string.IsNullOrEmpty(x.InventoryType) && x.InventoryType.ToLower() == "instock");
                        if (!(isSellerDelivery || isInstock))
                        {
                            result.Add(new CreateOrderRequest
                            {
                                OrderId = o.OrderCode,
                                OrderStatus = o.Status,
                                UpdateAt = o.UpdatedAt
                            });
                        }
                    }
                }

                var totalPage = response?.Data?.Paging?.LastPage ?? 0;
                if (totalItem < pageSize && pageNo < totalPage)
                {
                    totalItem = pageSize;
                }

                pageNo++;
            }
            logInfo.EcommerceRequest = string.Empty;

            if (!string.IsNullOrEmpty(errorMessage))
            {
                logInfo.LogWarning(errorMessage);
            }

            if (!result.Any()) return (false, null);

            logInfo.ResponseObject = $" OrderIds: {result.Select(p => p.OrderId).ToSafeJson()}";
            logInfo.LogInfo();
            return (true, result);
        }

        public Task<OrderDetailGetResponse> GetOrderDetail(int retailerId, long channelId, ChannelAuth auth,
            CreateOrderRequest request, LogObjectMicrosoftExtension logInfo,
            Platform platform, OmniChannelSettingObject settings = null)
        {
            throw new NotImplementedException();
        }

        public async Task<(bool, bool, bool, List<KvInvoice>)> GetInvoiceDetail(int retailerId, long channelId, Guid logId,
            ChannelAuth auth, CreateInvoiceRequest request,
            LogObjectMicrosoftExtension loggerExtension,
            Platform platform,
            OmniChannelSettingObject settings)
        {
            if (request.KvOrder == null)
            {
                return (false, false, false, null);
            }

            var kvOrder = request.KvOrder;
            if (request.KvDelivery == null)
            {
                return (false, false, false, null);
            }
            var delivery = request.KvDelivery;
            var getGetInvoiceDetailLog = new LogObject(Log, logId)
            {
                Action = string.Format(EcommerceTypeException.GetInvoiceDetail, Ecommerce.Tiki),
                RetailerId = retailerId,
                OmniChannelId = channelId,
                Description = "Tiki client v1",
                RequestObject = request.OrderId
            };

            getGetInvoiceDetailLog.LogInfo();

            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                getGetInvoiceDetailLog.ResponseObject = $"Auth:{auth.ToSafeJson()} EmptyAccessToken";
                Log.Error($"{KeywordCommonSearch.CallTikiError} EmptyAccessToken - Context: {getGetInvoiceDetailLog.ToSafeJson()}");
                throw new OmniException(string.Format(EcommerceTypeException.EmptyAccessToken, Ecommerce.Tiki));
            }

            var orderDetail = await GetOrderDetail(auth.AccessToken, request.OrderId, loggerExtension);

            getGetInvoiceDetailLog.ResponseObject = orderDetail;
            getGetInvoiceDetailLog.LogInfo();

            if (orderDetail == null || !orderDetail.Items.Any() || (orderDetail.Errors != null && orderDetail.Errors.Any()))
            {
                return (false, false, true, null);
            }

            if (kvOrder.Status != (int)OrderState.Void && (orderDetail.Status == TikiStatus.Queueing || orderDetail.Status == TikiStatus.Processing
                                || orderDetail.Status == TikiStatus.WaitingPayment || orderDetail.Status == TikiStatus.Paid
                                || orderDetail.Status == TikiStatus.SellerConfirmed || orderDetail.Status == TikiStatus.SellerCanceled))
            {
                return (false, false, false, null);
            }

            if (kvOrder.Status != (int)OrderState.Void && kvOrder.Status != (int)OrderState.Finalized && orderDetail.Status == TikiStatus.Canceled)
            {
                return (true, true, false, null);
            }

            var deliveryStatus = UpdateDeliveryStatusForInvoice(orderDetail.Status);
            var invoiceStatus = ConvertToKvInvoiceStatus(orderDetail.Status);

            var invoices = new List<KvInvoice>();

            var invoice = new KvInvoice
            {
                Code = $"HDTIKI_{request.OrderId}",
                RetailerId = kvOrder.RetailerId,
                OrderId = kvOrder.Id,
                BranchId = kvOrder.BranchId,
                CustomerId = kvOrder.CustomerId,
                Description = $"Hóa đơn từ Tiki: {request.OrderId}",
                UsingCod = true,
                PriceBookId = kvOrder.Extra?.FromJson<Extra>().PriceBookId?.Id ?? 0,
                DeliveryDetail = new InvoiceDelivery
                {
                    Receiver = delivery.Receiver,
                    ContactNumber = delivery.ContactNumber,
                    Address = delivery.Address,
                    UsingPriceCod = true,
                    WardName = delivery.WardName,
                    LocationName = delivery.LocationName,
                    Status = deliveryStatus ?? (byte)DeliveryStatus.Pending,
                    DeliveryBy = delivery.PartnerId,
                    LatestStatus = deliveryStatus ?? (byte)DeliveryStatus.Pending,
                    Price = kvOrder.OrderDelivery?.Price,
                    FeeJson = kvOrder.OrderDelivery?.FeeJson,
                },
                Status = invoiceStatus,
                InvoiceDetails = new List<InvoiceDetail>(),
                NewStatus = invoiceStatus,
                DeliveryStatus = deliveryStatus
            };

            foreach (var invoiceItem in orderDetail.Items)
            {
                decimal discount = 0;
                if (invoiceItem.DiscountItem?.Amount != null && invoiceItem.Qty != 0)
                {
                    discount = (invoiceItem.DiscountItem.Amount - invoiceItem.DiscountItem.TikiPoint) / (decimal)invoiceItem.Qty;
                }

                invoice.InvoiceDetails.Add(new InvoiceDetail
                {
                    ProductChannelSku = invoiceItem.OriginalSku,
                    ProductChannelId = invoiceItem.ProductId.ToString(),
                    ProductName = invoiceItem.ProductName,
                    Quantity = invoiceItem.Qty,
                    Price = invoiceItem.Price,
                    ProductChannelName = invoiceItem.ProductName,
                    Discount = discount,
                    DiscountPrice = (float)discount
                });
            }
            invoices.Add(invoice);
            return (true, false, false, invoices);
        }

        public async Task<(byte?, string, string)> GetLogisticsStatus(int retailerId, long channelId, LogObjectMicrosoftExtension logInfo,
            string orderId, ChannelAuth auth, bool isGetOrderStatus = false, Platform platform = null)
        {
            (byte?, string, string) result = (null, null, null);
            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                Log.Error($"{KeywordCommonSearch.CallTikiError} EmptyAccessToken TikiGetLogisticsStatus - Context: {new ExceptionContextModel { RetailerId = retailerId, ChannelId = channelId, OrderIds = new List<object> { orderId }, Auth = auth, LogId = logInfo.Id }.ToJson()}");
                throw new OmniException(string.Format(EcommerceTypeException.EmptyAccessToken, Ecommerce.Tiki));
            }

            var orderDetail = await GetOrderDetailLoggerOld(auth.AccessToken, orderId);
            if (orderDetail != null && (orderDetail.Errors == null || !orderDetail.Errors.Any()))
            {
                result.Item1 = UpdateDeliveryStatusForInvoice(orderDetail.Status);
                result.Item2 = orderDetail.Status;
            }

            return result;
        }

        private byte? ConvertToKvOrderStatus(string tikiStatus)
        {
            switch (tikiStatus)
            {
                case TikiStatus.Queueing:
                case TikiStatus.Processing:
                case TikiStatus.WaitingPayment:
                case TikiStatus.Paid:
                case TikiStatus.SellerConfirmed:
                case TikiStatus.SellerCanceled:
                    return (byte)OrderState.Pending;

                case TikiStatus.Picking:
                case TikiStatus.Packaging:
                case TikiStatus.FinishedPacking:
                case TikiStatus.ReadyToShip:
                case TikiStatus.HandoverToPartner:
                case TikiStatus.SuccessfulDelivery:
                case TikiStatus.Returned:
                case TikiStatus.Complete:
                    return (byte)OrderState.Finalized;

                case TikiStatus.Canceled:
                    return (byte)OrderState.Void;
            }
            return null;
        }

        private async Task<IRestResponse<UpdateProductTikiResponse>> UpdateProductTiki(
            string tikiApi, string productId,
            double? quantity, decimal? price,
            LogObjectMicrosoftExtension loggerExtension)
        {
            try
            {
                loggerExtension.EcommerceRequest = Ecommerce.Tiki;
                var response = await Policy
                                    .HandleResult<IRestResponse<UpdateProductTikiResponse>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout || (int)msg.StatusCode == TikiTooManyRequests)
                                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                                        (res, timeSpan, retryCount, context) =>
                                        {
                                            if (retryCount > 1)
                                            {
                                                loggerExtension.RequestObject = null;
                                            }

                                            if (res.Result?.Data?.Errors?.Any() == true)
                                            {
                                                var msgErr = $"statusCode: {res.Result.StatusCode} - error: {res.Result.Data.Errors.Join(",")}";
                                                var ex = new TikiException(msgErr);
                                                loggerExtension.LogError(ex);
                                            }
                                            else if (res.Exception != null)
                                            {
                                                loggerExtension.LogError(res.Exception);
                                            }
                                            else if (res.Result?.ErrorException != null)
                                            {
                                                loggerExtension.LogError(res.Result.ErrorException);
                                            }
                                            else
                                            {
                                                var messageErr =
                                                    $"Request update product failed with {res.Result?.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}";
                                                var exCustom = new TikiException(messageErr);
                                                loggerExtension.LogError(exCustom);
                                            }

                                        }).ExecuteAsync(() => TikiHelper.UpdateProduct(tikiApi, DataRequestHelper.ParseItemIdToLong(productId), quantity, price, loggerExtension));
                loggerExtension.EcommerceRequest = string.Empty;
                return response;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        private async Task<IRestResponse<UpdateProductTikiResponse>> UpdateProductStock(
            string tikiApi, string productId,
            double? quantity, decimal? price,
            LogObjectMicrosoftExtension loggerExtension)
        {
            var logSyncMultiOnHandSyncItem =
                loggerExtension.Clone(string.Format(EcommerceTypeException.SyncMultiOnHandSyncItem,
                    Ecommerce.Tiki));
            logSyncMultiOnHandSyncItem.RequestObject = $"itemId: {productId}, OnHand: {quantity}";
            try
            {
                var response = await Policy
                    .HandleResult<IRestResponse<UpdateProductTikiResponse>>(msg =>
                        msg.StatusCode == HttpStatusCode.RequestTimeout || (int)msg.StatusCode == TikiTooManyRequests)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry,
                        i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                        (res, timeSpan, retryCount, context) =>
                        {
                            if (res?.Result?.Data?.Errors?.Any() == true)
                            {
                                var msgErr =
                                    $"statusCode: {res.Result.StatusCode} - error: {res.Result.Data.Errors.Join(",")}";
                                var ex = new TikiException(msgErr);
                                loggerExtension.LogError(ex);
                            }
                            else if (res?.Exception != null)
                            {
                                loggerExtension.LogError(res.Exception);
                            }
                            else if (res?.Result?.ErrorException != null)
                            {
                                loggerExtension.LogError(res.Result.ErrorException);
                            }
                            else
                            {
                                var msg =
                                    $"Request update product failed with {res?.Result?.StatusCode}. UpdateProductStock. Waiting {timeSpan} before next retry. Retry attempt {retryCount}";
                                var ex = new OmniException(msg);
                                loggerExtension.LogError(ex);
                            }
                        }).ExecuteAsync(() => TikiHelper.UpdateProduct(tikiApi,
                        DataRequestHelper.ParseItemIdToLong(productId), quantity, price, loggerExtension));

                logSyncMultiOnHandSyncItem.ResponseObject = response?.Content;
                logSyncMultiOnHandSyncItem.LogInfo();

                return response;
            }
            catch (Exception ex)
            {
                logSyncMultiOnHandSyncItem.LogError(ex);
                throw;
            }
        }

        public async Task<RefreshTokenResponse> RefreshAccessToken(string refreshToken, long channelId, Guid logId,
            ChannelAuth auth, Platform platform)
        {
            var tikiCallBack = SelfAppConfig.TikiCallBack;
            var tikiClientId = SelfAppConfig.TikiClientAppId;
            var tikiApiSercetKey = SelfAppConfig.TikiApiSercetKey;
            var refeshTokenLog = new LogObject(Log, logId)
            {
                Action = string.Format(EcommerceTypeException.RefreshAccessToken, Ecommerce.Tiki),
                RequestObject = $"TikiClientAppId: {tikiClientId}, TikiApiSercetKey: {tikiApiSercetKey}, TikiCallBack: {tikiCallBack}, RefreshToken: {refreshToken}",
                OmniChannelId = channelId

            };
            var response = await TikiHelper.RefreshToken(refreshToken, tikiCallBack, tikiClientId, tikiApiSercetKey);

            var newToken = response?.Data;
            if (newToken == null || string.IsNullOrEmpty(newToken.AccessToken) || response.StatusCode != HttpStatusCode.OK)
            {
                var refreshError = string.IsNullOrEmpty(response?.Content) ? null : JsonConvert.DeserializeObject<TikiRefeshTokenError>(response.Content);
                if (response?.StatusCode == HttpStatusCode.Unauthorized || response?.StatusCode == HttpStatusCode.Forbidden || refreshError?.TikiErrorDebug == TikiTokenExpired)
                {
                    refeshTokenLog.Description = "Response StatusCode: " + response?.StatusCode;
                    refeshTokenLog.ResponseObject = response?.Content;
                    refeshTokenLog.LogError();

                    return new RefreshTokenResponse
                    {
                        IsDeactivateChannel = true
                    };
                }

                refeshTokenLog.Description = $"TikiRefreshAccessToken failed - StatusCode {response?.StatusCode}";
                refeshTokenLog.ResponseObject = response?.Content;
                refeshTokenLog.LogError();
                return null;
            }

            var tokenResponse = new RefreshTokenResponse
            {
                AccessToken = newToken.AccessToken,
                RefreshExpiresIn = newToken.RefreshExpiresIn,
                ExpiresIn = newToken.ExpiresIn,
                RefreshToken = newToken.RefreshToken,
                SellerId = newToken.ShopId.ToString()
            };

            refeshTokenLog.Description = "Tiki refresh token successful";
            refeshTokenLog.LogInfo();

            return tokenResponse;
        }

        private async Task<TikiOrder> GetOrderDetail(string accessToken, string orderId, LogObjectMicrosoftExtension loggerExtension)
        {
            try
            {
                loggerExtension.EcommerceRequest = Ecommerce.Tiki;
                var response = await Policy.HandleResult<IRestResponse<TikiOrder>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout || (int)msg.StatusCode == TikiTooManyRequests)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval), (res, timeSpan, retryCount, context) =>
                    {
                        if (res?.Result?.Data?.Errors?.Any() == true)
                        {
                            var msgErr = $"statusCode: {res.Result.StatusCode} - error: {res.Result.Data.Errors.Join(",")}";
                            var ex = new TikiException(msgErr);
                            loggerExtension.LogError(ex);
                        }
                        else if (res?.Exception != null)
                        {
                            loggerExtension.LogError(res.Exception);
                        }
                        else
                        {
                            var msg =
                                $"Request failed with {res?.Result?.StatusCode}. GetOrdersDetail. Waiting {timeSpan} before next retry. Retry attempt {retryCount}";
                            var ex = new OmniException(msg);
                            loggerExtension.LogError(ex);
                        }
                    }).ExecuteAsync(() => TikiHelper.GetOrdersDetail(accessToken, orderId, loggerExtension));
                loggerExtension.EcommerceRequest = string.Empty;

                if (response?.Data?.Errors != null && response.Data.Errors.Any())
                {
                    var errorMessage = $"StatusCode: {response.StatusCode}, Messages: {response.Data.Errors.Join("; ")}";
                    var ex = new TikiException(errorMessage);
                    loggerExtension.LogError(ex);
                    return null;
                }

                loggerExtension.ResponseObject = response?.Content;
                loggerExtension.LogInfo();
                return response?.Data;
            }
            catch (Exception ex)
            {
                loggerExtension.LogError(ex);
                throw;
            }
        }

        private async Task<TikiOrder> GetOrderDetailLoggerOld(string accessToken, string orderId)
        {
            var orderDetail = await Policy.HandleResult<IRestResponse<TikiOrder>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout || (int)msg.StatusCode == TikiTooManyRequests)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval), (res, timeSpan, retryCount, context) =>
                {
                    Log.Warn(
                        $"Request failed with {res.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                }).ExecuteAsync(() => TikiHelper.GetOrdersDetailLoggerOld(accessToken, orderId));

            return orderDetail?.Data;
        }

        private byte? UpdateDeliveryStatusForInvoice(string tikiStatus)
        {
            switch (tikiStatus)
            {
                case TikiStatus.Picking:
                    return (byte)DeliveryStatus.InPickup;

                case TikiStatus.Packaging:
                case TikiStatus.FinishedPacking:
                case TikiStatus.ReadyToShip:
                    return (byte)DeliveryStatus.Pickuped;

                case TikiStatus.HandoverToPartner:
                case TikiStatus.Shipping:
                    return (byte)DeliveryStatus.Delivering;

                case TikiStatus.SuccessfulDelivery:
                case TikiStatus.Complete:
                    return (byte)DeliveryStatus.Delivered;

                case TikiStatus.Returned:
                case TikiStatus.Canceled:
                    return (byte)DeliveryStatus.Returning;
            }
            return null;
        }

        private byte ConvertToKvInvoiceStatus(string tikiStatus)
        {
            switch (tikiStatus)
            {
                case TikiStatus.Complete:
                    return (byte)InvoiceState.Issued;

            }
            return (byte)InvoiceState.Pending;
        }

        private byte ConvertSellerStatus(string registrationStatus)
        {
            switch (registrationStatus)
            {
                case TikiSellerStatus.Completed:
                case null:
                    return (byte)ChannelStatus.Approved;

                case TikiSellerStatus.KamRejected:
                    return (byte)ChannelStatus.Reject;
            }
            return (byte)ChannelStatus.Pending;
        }

        public async Task<List<KvInvoice>> GetInvoiceDetailByOrderId(int retailerId, Guid logId, ChannelAuth auth,
            string orderId, long channelId, LogObjectMicrosoftExtension loggerExtension, Platform platform)
        {
            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                Log.Error($"{KeywordCommonSearch.CallTikiError} EmptyAccessToken TikiGetLogisticsStatus - Context: {new ExceptionContextModel { RetailerId = retailerId, ChannelId = channelId, OrderIds = new List<object> { orderId }, Auth = auth, LogId = logId }.ToJson()}");
                throw new OmniException(string.Format(EcommerceTypeException.EmptyAccessToken, Ecommerce.Tiki));
            }
            var orderDetail = await GetOrderDetail(auth.AccessToken, orderId, loggerExtension);

            if (orderDetail == null) return null;

            var invoice = new KvInvoice
            {
                Code = $"HDTIKI_{orderId}",
                InvoiceDetails = new List<InvoiceDetail>()
            };

            foreach (var invoiceItem in orderDetail.Items)
            {
                decimal discount = 0;
                if (invoiceItem.DiscountItem?.Amount != null && invoiceItem.Qty != 0)
                {
                    discount = (invoiceItem.DiscountItem.Amount - invoiceItem.DiscountItem.TikiPoint) / (decimal)invoiceItem.Qty;
                }

                invoice.InvoiceDetails.Add(new InvoiceDetail
                {
                    ProductChannelSku = invoiceItem.OriginalSku,
                    ProductChannelId = invoiceItem.ProductId.ToString(),
                    ProductName = invoiceItem.ProductName,
                    Quantity = invoiceItem.Qty,
                    Price = invoiceItem.Price,
                    ProductChannelName = invoiceItem.ProductName,
                    Discount = discount,
                    DiscountPrice = (float)discount
                });
            }

            return new List<KvInvoice> { invoice };
        }

        Task<List<ShopeeAttribute>> IBaseClient.GetAttributeByShopId(int retailerId, long channelId, ChannelAuth auth,
            Guid logId, long categoryId, Platform platform)
        {
            throw new NotImplementedException();
        }

        public Task<ShopeeProductResponse> PushProductToChannel(int retailerId, long channelId, ChannelAuth auth,
            Guid logId, ShopeeProduct req, Platform platform, string kvProductAttributeStr)
        {
            throw new NotImplementedException();
        }

        public Task<CategoriesResp> GetCategories(int retailerId, long channelId, ChannelAuth auth, Guid logId,
            Platform platform)
        {
            throw new NotImplementedException();
        }
        public Task<ProductListResponse> GetListDeletedProduct(ChannelAuth auth, long channelId, Guid logId,
            bool isFirstSync, KvInternalContext context = null, DateTime? updateTimeFrom = null)
        {
            throw new NotImplementedException();
        }

        public Task<KvOrder> GenListKvOrder(long shopId, int? saleChannelId, int branchId, long userId, int retailerId, OrderDetail shopeeOrder,
            OmniChannelSettingObject settings, LogObjectMicrosoftExtension logInfo, Platform platform)
        {
            throw new NotImplementedException();
        }

        public Task<ShopeeUploadImgResponse> UploadImg(ChannelAuth auth, int retailerId, long shopId,
            List<Image> images, Guid logId, Platform platform)
        {
            throw new NotImplementedException();
        }

        Task<CustomerResponse> IBaseClient.GetCustomerByOrderId(int retailerId, long channelId, string orderId,
            ChannelAuth auth, Guid logId, LogObjectMicrosoftExtension loggerExtension, Platform platform)
        {
            throw new NotImplementedException();
        }
        Task<List<OrderDetail>> IBaseClient.GetListOrderDetail(ChannelAuth auth, string[] orderSnList, LogObjectMicrosoftExtension logInfo, Platform platform)
        { throw new NotImplementedException(); }

        public List<KvInvoice> GenKvInvoices(CreateInvoiceRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<(bool, List<Transaction>)> GetListTransaction(ChannelAuth auth, DateTime createFrom, DateTime createTo, LogObjectMicrosoftExtension log, Platform platform)
        {
            throw new NotImplementedException();
        }

        public Task<SellerWareHouseV2Response> GetTikiWarehouse(string accessToken)
        {
            throw new NotImplementedException();
        }

        public Task<(bool, List<FinanceTransaction>)> GetFinanceTransactions(string token, long channelId, string orderSn, DateTime? startTime, DateTime? endTime, LogObjectMicrosoftExtension logInfo, bool isBreakIfMaximum)
        {
            throw new NotImplementedException();
        }

        public Task<List<Logistics>> GetLogisticsByShopId(int retailerId, long channelId, ChannelAuth auth, Guid logId, Platform platform)
        {
            throw new NotImplementedException();
        }
    }
}
﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Interfaces;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.ChannelClient.Impls
{
    public class ChannelClient : IChannelClient
    {
        private readonly IAppSettings _settings;
        public ChannelClient(IAppSettings settings)
        {
            _settings = settings;
        }

        public ChannelClient()
        {
        }

        public IBaseClient CreateClient(byte channelType, int groupId, long retailerId)
        {
            switch (channelType)
            {
                case (byte)ChannelTypeEnum.Shopee:
                    {
                        return new ShopeeClientV2(_settings);
                    }
                case (byte)ChannelTypeEnum.Lazada:
                    {
                        return new LazadaClient(_settings);
                    }
                case (byte)ChannelTypeEnum.Tiki:
                    {
                        return new TikiClientV21(_settings);
                    }
                case (byte)ChannelTypeEnum.Sendo:
                    {
                        return new SendoClient(_settings);
                    }
                case (byte)ChannelTypeEnum.Tiktok:
                    {
                        return new TikTokClient(_settings);
                    }
                default:
                    {
                        return null;
                    }
            }
        }

        public IBaseClient GetClient(byte channelType, IAppSettings settings)
        {
            switch (channelType)
            {
                case (byte)ChannelTypeEnum.Shopee:
                    {
                        return new ShopeeClientV2(settings);
                    }
                case (byte)ChannelTypeEnum.Lazada:
                    {
                        return new LazadaClient(settings);
                    }
                case (byte)ChannelTypeEnum.Tiki:
                    {
                        return new TikiClientV21(settings);
                    }
                case (byte)ChannelTypeEnum.Sendo:
                    {
                        return new SendoClient(settings);
                    }
                case (byte)ChannelTypeEnum.Tiktok:
                    {
                        return new TikTokClient(settings);
                    }
                default:
                    {
                        return null;
                    }
            }
        }

        public IBaseClient GetClientV2(byte channelType, IAppSettings settings)
        {
            switch (channelType)
            {
                case (byte)ChannelTypeEnum.Shopee:
                    {
                        return new ShopeeClientV2(settings);
                    }
                case (byte)ChannelTypeEnum.Lazada:
                    {
                        return new LazadaClient(settings);
                    }
                case (byte)ChannelTypeEnum.Tiki:
                    {
                        return new TikiClientV21(settings);
                    }
                case (byte)ChannelTypeEnum.Sendo:
                    {
                        return new SendoClient(settings);
                    }
                case (byte)ChannelTypeEnum.Tiktok:
                    {
                        return new TikTokClient(settings);
                    }
                default:
                    {
                        return null;
                    }
            }
        }
    }
}
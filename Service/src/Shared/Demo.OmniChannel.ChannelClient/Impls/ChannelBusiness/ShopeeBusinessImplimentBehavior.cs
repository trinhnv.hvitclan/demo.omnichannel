﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.FeatureToggle;
using Demo.OmniChannel.ChannelClient.Interfaces.NewBase;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Models;
using Demo.OmniChannel.Utilities;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Logging;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using ChannelCommon = Demo.OmniChannel.ChannelClient.Common;
using Order = Demo.OmniChannel.ShareKernel.RedisStreamMessage.Order;
using OrderIncome = Demo.OmniChannel.ShareKernel.RedisStreamMessage.OrderIncome;

namespace Demo.OmniChannel.ChannelClient.Impls.NewBase
{
    public class ShopeeBusinessImplimentBehavior : IShopeeBusinessClient
    {
        private ILog Log => LogManager.GetLogger(typeof(ShopeeBusinessImplimentBehavior));
        private IAppSettings AppSettings { get; set; }
        public ShopeeBusinessImplimentBehavior(IAppSettings appSettings)
        {
            AppSettings = appSettings;
        }

        #region V2 method

        public KvOrder GenKvOrderV2WithLogistics(OrderDetailV2 shopeeOrder,
            ShopeePaymentEscrowDetailResponse paymentEscrowDetail,
            ShopeeTrackingInfoV2Response trackingInfo,
            OmniChannelSettingObject settings,
            int branchId, long userId, int retailerId, int groupId)
        {
            var kvOrder = GenKvOrderV2(shopeeOrder, paymentEscrowDetail, trackingInfo, settings,
                branchId, userId, retailerId, groupId);
            kvOrder = UpdateLogisticsToKvOrder(kvOrder, settings, trackingInfo);
            kvOrder.NewStatus = ConvertToKvOrderStatus(shopeeOrder.OrderStatus, trackingInfo);
            kvOrder.LogisticsChannelStatus = trackingInfo?.LogisticsStatus;
            return kvOrder;
        }

        public KvOrder GenKvOrderFromOrderDetailV2(OrderDetailV2 shopeeOrder,
            ShopeeTrackingInfoV2Response trackingInfo,
            OmniChannelSettingObject settings,
            int branchId, long userId, int? saleChannelId)
        {
            var kvOrderCode = shopeeOrder.OrderSn;
            var kvOrder = new KvOrder
            {
                Code = $"DHSPE_{kvOrderCode}",
                Ordersn = kvOrderCode,
                BranchId = branchId,
                SoldById = userId,
                PurchaseDate = GetPurchaseDateV2(trackingInfo, settings, shopeeOrder.CreateTime),
                Description = GetKvOrderDescription(shopeeOrder),
                IsSyncSuccess = false,
                UsingCod = true,
                SaleChannelId = saleChannelId,
                OrderDelivery = new InvoiceDelivery
                {
                    Receiver = shopeeOrder.RecipientAddress?.Name ?? string.Empty,
                    ContactNumber = shopeeOrder.RecipientAddress?.Phone,
                    Address = shopeeOrder.RecipientAddress?.FullAddress,
                    WardName = shopeeOrder.RecipientAddress?.District,
                    LocationName = shopeeOrder.RecipientAddress?.State,
                    ChannelCity = shopeeOrder.RecipientAddress?.City,
                    ChannelWard = shopeeOrder.RecipientAddress?.District,
                    ChannelState = shopeeOrder.RecipientAddress?.State,
                    UsingPriceCod = true,
                    PartnerDelivery = new PartnerDelivery { Code = "SHOPEE", Name = "SHOPEE" }
                },
                CustomerPhone = !string.IsNullOrEmpty(shopeeOrder.RecipientAddress?.Phone) &&
                                shopeeOrder.RecipientAddress.Phone.StartsWith("84")
                    ? $"0{shopeeOrder.RecipientAddress.Phone.Substring(2)}"
                    : shopeeOrder.RecipientAddress?.Phone,
                CustomerCode = $"KHSPE{kvOrderCode}",
                CustomerName = shopeeOrder.RecipientAddress?.Name,
                CustomerAddress = shopeeOrder.RecipientAddress?.FullAddress,
                OrderDetails = new List<KvOrderDetail>(),
                NewStatus = ConvertOrderDetailToKvOrderStatus(shopeeOrder.OrderStatus, trackingInfo),
                ChannelStatus = shopeeOrder.OrderStatus,
                ChannelCreatedDate = shopeeOrder.CreateTime.FromUnixTime().ToLocalTime(),
                ChannelOrder = shopeeOrder.ToSafeJson()
            };

            foreach (var detail in shopeeOrder.ItemList)
            {
                kvOrder.OrderDetails.Add(new KvOrderDetail
                {
                    ProductChannelId = (detail.ModelId > 0 ? detail.ModelId : detail.ItemId).ToString(),
                    ParentChannelProductId = detail.ItemId.ToString(),
                    ProductChannelSku = !string.IsNullOrEmpty(detail.ModelSku) ? detail.ModelSku : detail.ItemSku,
                    ProductChannelName = !string.IsNullOrEmpty(detail.ModelName) ? $"{detail.ItemName} - {detail.ModelName}" : detail.ItemName,
                    Quantity = detail.ModelQuantityPurchased,
                    Price = detail.ModelOriginalPrice,
                    VariationId = detail.ModelId,
                    ItemId = detail.ItemId,
                    PromotionType = detail.PromotionType,
                    PromotionId = detail.PromotionId,
                    AddOnDealId = detail.AddOnDealId,
                    DiscountPrice = detail.ModelDiscountedPrice,
                    Uuid = StringHelper.GenerateUUID(),
                });
            }
            return kvOrder;
        }

        public KvOrder GenKvOrderV2(OrderDetailV2 shopeeOrder, ShopeePaymentEscrowDetailResponse paymentEscrowDetail,
            ShopeeTrackingInfoV2Response trackingInfo, OmniChannelSettingObject settings,
            int branchId, long userId, int retailerId, int groupId)
        {
            var kvOrder = GenKvOrderFromOrderDetailV2(shopeeOrder, trackingInfo, settings, branchId, userId, null);
            return ConvertPaymentEscrowToKvOrder(kvOrder, paymentEscrowDetail, retailerId, groupId);
        }

        public KvOrder ConvertPaymentEscrowToKvOrder(KvOrder kvOrder, ShopeePaymentEscrowDetailResponse paymentEscrowDetail, int retailerId, int groupId)
        {
            if (paymentEscrowDetail == null)
            {
                return kvOrder;
            }
            return ProcessCalPaymentEscrowDetailToKvOrder(kvOrder, paymentEscrowDetail, retailerId, groupId);
        }
        private KvOrder ProcessCalPaymentEscrowDetailToKvOrder(KvOrder kvOrder, ShopeePaymentEscrowDetailResponse paymentEscrowDetail, int retailerId, int groupId)
        {
            if (paymentEscrowDetail == null)
            {
                return kvOrder;
            }
            kvOrder = GenSurChargesV2(kvOrder, paymentEscrowDetail);
            // Activity detail
            kvOrder = CaculateActivityDetailV2(kvOrder, paymentEscrowDetail.OrderIncome?.Items);
            kvOrder = CacultateDiscountV2(kvOrder, paymentEscrowDetail.OrderIncome?.Items);
            // Fee
            var orderImcomeFeeV1 = paymentEscrowDetail.OrderIncome?.ConvertTo<OrderIncome>();
            var feeList = Helpers.ConvertFeeToList(orderImcomeFeeV1, FeeList.InitialShoppeFee());
            ReCalShopeeAddFeeOrSurcharge(kvOrder, feeList, orderImcomeFeeV1, retailerId, groupId);

            kvOrder.Discount = CalDiscountByDetails(paymentEscrowDetail.OrderIncome?.VoucherFromSeller, paymentEscrowDetail.OrderIncome?.SellerCoinCashBack);
            return kvOrder;
        }
        private KvOrder GenSurChargesV2(KvOrder kvOrder, ShopeePaymentEscrowDetailResponse paymentEscrowDetail)
        {
            return GenSurCharges(kvOrder, paymentEscrowDetail?.OrderIncome?.ShopeeDiscount);
        }
        public string GetKvOrderDescription(OrderDetailV2 order)
        {
            var orderDetail = order.ConvertTo<Order>();
            return GetKvOrderDescription(orderDetail);
        }
        private KvOrder CacultateDiscountV2(KvOrder kvOrder, List<Item> items)
        {
            foreach (var item in kvOrder.OrderDetails)
            {
                var quantity = item.Quantity;
                var discount = item.Discount;
                var escrowDetailList = items;
                var temp =
                    escrowDetailList.LastOrDefault(x =>
                        x.ModelId > 0 &&
                        x.ModelId == item.VariationId &&
                        item.IsUsed != true &&
                        x.DiscountedPrice >= 0 &&
                        DateHelper.CompareQuantity(x.QuantityPurchased, quantity) &&
                        x.ItemId == item.ItemId &&
                        x.ActivityType != ShopeePromotionType.BundleDeal &&
                        x.ActivityId == item.PromotionId) ??

                    escrowDetailList.LastOrDefault(x =>
                        x.ItemId > 0 &&
                        x.ItemId == item.ItemId &&
                        x.ModelId == item.VariationId &&
                        x.ActivityType != ShopeePromotionType.BundleDeal &&
                        item.IsUsed != true &&
                        x.DiscountedPrice >= 0 &&
                        DateHelper.CompareQuantity(x.QuantityPurchased, quantity) &&
                        x.ActivityId == item.PromotionId) ??

                    escrowDetailList.LastOrDefault(x =>
                        Regex.Replace(x.ModelSku.Trim(), @"\t|\n|\r", string.Empty) == item.ProductCode &&
                        item.IsUsed != true && x.DiscountedPrice >= 0 &&
                        x.ActivityType != ShopeePromotionType.BundleDeal &&
                        DateHelper.CompareQuantity(x.QuantityPurchased, quantity) &&
                        x.ItemId == item.ItemId &&
                        x.ModelId == item.VariationId &&
                        x.ActivityId == item.PromotionId) ??

                    escrowDetailList.LastOrDefault(x =>
                        Regex.Replace(x.ItemSku.Trim(), @"\t|\n|\r", string.Empty) == item.ProductCode &&
                        item.IsUsed != true &&
                        x.DiscountedPrice >= 0 &&
                        x.ActivityType != ShopeePromotionType.BundleDeal &&
                        DateHelper.CompareQuantity(x.QuantityPurchased, quantity) &&
                        x.ItemId == item.ItemId &&
                        x.ModelId == item.VariationId &&
                        x.ActivityId == item.PromotionId) ??

                    escrowDetailList.LastOrDefault(x =>
                        x.ItemId > 0 &&
                        x.ItemId == item.ItemId &&
                        x.ModelId == item.VariationId &&
                        x.ActivityType != ShopeePromotionType.BundleDeal &&
                        item.IsUsed != true &&
                        x.DiscountedPrice >= 0 &&
                        DateHelper.CompareQuantity(x.QuantityPurchased, quantity) &&
                        x.ActivityId == item.AddOnDealId);

                if (temp != null && (temp.SellerDiscount > 0 || temp.ShopeeDiscount > 0) && (discount == null || discount <= 0))
                    item.Discount = (temp.SellerDiscount + temp.ShopeeDiscount) / (decimal)quantity;
            }

            return kvOrder;
        }

        private void ReCalShopeeAddFeeOrSurcharge(KvOrder kvOrder, List<FeeDetail> fees, Demo.OmniChannel.ShareKernel.RedisStreamMessage.OrderIncome orderIncome, int retailerId = 0, int groupId = 0)
        {
            if (orderIncome == null) return;

            if (!new ShopeeCalFeeV2Toggle(AppSettings).Enable(retailerId, groupId))
            {
                AddFeeOrder(kvOrder, fees);
                return;
            }

            var totalShipFee = orderIncome.FinalShippingFee + orderIncome.BuyerPaidShippingFee;

            if (orderIncome.FinalShippingFee == 0 || totalShipFee >= 0)
            {
                fees = fees.Where(x => x.Name != FeeList.ShopeeFeeIgnoreWhenNoFinalFee).ToList();
            }

            if (totalShipFee > 0 && orderIncome.FinalShippingFee != 0)
            {
                Log.Info($"ReCalShopeeAddFeeOrSurcharge - AddSurcharge - ShopeeBusinessImplimentBehavior - retailerId={retailerId} - groupId={groupId} - kvOrder.RetailerId={kvOrder.RetailerId} - orderSn={kvOrder.Ordersn}");
                AddSurchargeWhenTotalFeeBiggerZero(kvOrder, totalShipFee);
            }

            AddFeeOrder(kvOrder, fees);
        }

        private void AddFeeOrder(KvOrder kvOrder, List<FeeDetail> fees)
        {
            var feeDetail = new Fee { FeeList = fees, ChannelType = (byte)ChannelTypeEnum.Shopee };
            kvOrder.OrderDelivery.FeeJson = feeDetail.ToSafeJson();
            kvOrder.OrderDelivery.Price = (decimal)feeDetail.TotalFee();
        }

        private void AddSurchargeWhenTotalFeeBiggerZero(KvOrder kvOrder, float totalShipFee)
        {
            if (totalShipFee <= 0) return;
            var surcharge = new SurCharge()
            {
                Value = (decimal)totalShipFee,
                Name = ShopeeMessage.ShopeeSurchargeMessage,
                Type = (byte)ShopeeSurchargeType.FeeShip,
            };
            if (kvOrder.SurCharges != null && kvOrder.SurCharges.Any())
            {
                var isExisted = kvOrder.SurCharges.Any(x => x.Name == ShopeeMessage.ShopeeSurchargeMessage);
                if (!isExisted) kvOrder.SurCharges.Add(surcharge);
            }
            else
            {
                kvOrder.SurCharges = new List<SurCharge>() { surcharge };
            }
        }

        private KvOrder CaculateActivityDetailV2(KvOrder kvOrder, List<Item> activityDetailLst)
        {
            if (activityDetailLst == null || !activityDetailLst.Any(x => x.ActivityType.Equals(ShopeePromotionType.BundleDeal)))
            {
                return kvOrder;
            }
            else
            {
                var activities = activityDetailLst.Where(p => p.ActivityType.Equals(ShopeePromotionType.BundleDeal)).ToList();
                var total = activities.Count;
                for (var i = 0; i < total; i++)
                {
                    var item = activities[i];
                    var orderItem = kvOrder.OrderDetails.FirstOrDefault(x =>
                        x.VariationId == item.ModelId && x.ItemId == item.ItemId &&
                        DateHelper.CompareQuantity(item.QuantityPurchased, x.Quantity) && x.IsUsed != true &&
                        x.PromotionType.Equals(ShopeePromotionType.BundleDeal));
                    if (orderItem == null)
                    {
                        continue;
                    }
                    orderItem.DiscountPrice = item.DiscountedPrice;
                    orderItem.Discount = (decimal)(item.OriginalPrice - item.DiscountedPrice) / item.QuantityPurchased;
                    orderItem.IsUsed = true;
                }
            }
            return kvOrder;
        }

        #endregion
        public byte? ConvertTrackingInfoToKvDeliveryStatus(string logisticsStatus)
        {
            return ConvertToKvDeliveryStatus(logisticsStatus);
        }
        public byte? ConvertOrderDetailToKvInvoiceStatus(string shopeeStatus, string logisticsStatus)
        {
            if (shopeeStatus == ChannelCommon.ShopeeStatus.Shipped || shopeeStatus == ChannelCommon.ShopeeStatus.ToConfirmReceive ||
           shopeeStatus == ChannelCommon.ShopeeStatus.Completed || shopeeStatus == ChannelCommon.ShopeeStatus.ReadyToShip)
            {
                return (byte)InvoiceState.Pending;
            }

            if (shopeeStatus == ChannelCommon.ShopeeStatus.Cancelled)
            {
                if (logisticsStatus == ShopeeDeliveryStatus.LogisticsDeliveryFailed)
                {
                    return (byte)DeliveryStatus.Returning;
                }
                return (byte)InvoiceState.Void;
            }
            return null;
        }
        public byte? ConvertOrderDetailToKvOrderStatus(string shopeeStatus,
            ShopeeTrackingInfoV2Response trackingInfo = null)
        {
            return ConvertToKvOrderStatus(shopeeStatus, trackingInfo);
        }
        private KvOrder UpdateLogisticsToKvOrder(KvOrder kvOrder,
            OmniChannelSettingObject settings,
            ShopeeTrackingInfoV2Response trackingInfo)
        {
            if (kvOrder.ChannelStatus == ChannelCommon.ShopeeStatus.Unpaid) return kvOrder;

            if (kvOrder.ChannelStatus == ChannelCommon.ShopeeStatus.Cancelled
                && (string.IsNullOrEmpty(trackingInfo?.LogisticsStatus) ||
                 trackingInfo.LogisticsStatus != ShopeeDeliveryStatus.LogisticsDeliveryFailed))
            {
                kvOrder.ChannelIsCancelOrder = true;
            }

            if (kvOrder.ChannelStatus == ChannelCommon.ShopeeStatus.ReadyToShip
                && (trackingInfo == null || string.IsNullOrEmpty(trackingInfo.TrackingNumber)))
            {
                return kvOrder;
            }

            if ((kvOrder.ChannelStatus == ChannelCommon.ShopeeStatus.Completed
                || kvOrder.ChannelStatus == ChannelCommon.ShopeeStatus.Shipped
                || kvOrder.ChannelStatus == ChannelCommon.ShopeeStatus.ToConfirmReceive) && trackingInfo == null)
            {
                return kvOrder;
            }

            if (kvOrder.ChannelStatus == ChannelCommon.ShopeeStatus.Completed
                || kvOrder.ChannelStatus == ChannelCommon.ShopeeStatus.Shipped
                || kvOrder.ChannelStatus == ChannelCommon.ShopeeStatus.Cancelled
                || kvOrder.ChannelStatus == ChannelCommon.ShopeeStatus.ToConfirmReceive)
            {
                if (trackingInfo != null && settings.IsConfirmReturning)
                {
                    kvOrder.DeliveryStatus = ConvertToKvDeliveryStatus(trackingInfo?.LogisticsStatus);
                }
                else
                {
                    if (trackingInfo != null && trackingInfo?.LogisticsStatus == ShopeeDeliveryStatus.LogisticsDeliveryFailed)
                    {
                        kvOrder.DeliveryStatus = (byte)DeliveryStatus.Returned;
                    }
                    else
                    {
                        kvOrder.DeliveryStatus = ConvertToKvDeliveryStatus(trackingInfo?.LogisticsStatus);
                    }
                }
            }
            kvOrder.OrderDelivery.DeliveryCode = trackingInfo?.TrackingNumber;
            return kvOrder;
        }
        private static byte? ConvertToKvDeliveryStatus(string shopeeDeliveryStatus)
        {
            if (string.IsNullOrEmpty(shopeeDeliveryStatus))
            {
                return null;
            }
            if (shopeeDeliveryStatus == ChannelCommon.ShopeeDeliveryStatus.LogisticsDeliveryDone)
            {
                return (byte)ChannelCommon.DeliveryStatus.Delivered;
            }
            if (shopeeDeliveryStatus == ChannelCommon.ShopeeDeliveryStatus.LogisticsDeliveryFailed)
            {
                return (byte)ChannelCommon.DeliveryStatus.Returning;
            }
            return (byte)ChannelCommon.DeliveryStatus.Delivering;
        }
        private decimal? CalDiscountByDetails(float? voucherSeller, float? sellerCoinCashBack)
        {
            decimal? discount = null;
            if (voucherSeller.HasValue && voucherSeller > 0)
            {
                discount = (decimal?)voucherSeller;
            }
            if (sellerCoinCashBack.HasValue && sellerCoinCashBack > 0)
            {
                discount = (discount != null) ? (discount + (decimal)sellerCoinCashBack) : (decimal)sellerCoinCashBack;
            }
            return discount;
        }
        private bool IsValidKvOrder(KvOrder kvOrder)
        {
            if (kvOrder == null)
            {
                return false;
            }
            return true;
        }
        private KvOrder GenSurCharges(KvOrder kvOrder, float? sellerRebate)
        {
            if (IsValidKvOrder(kvOrder))
            {
                if (sellerRebate == null ||
                !(sellerRebate > 0)) return kvOrder;
                var surcharge = new SurCharge
                {
                    Value = (decimal)sellerRebate,
                    Name = "Trợ giá Shopee",
                    Type = (byte)ShopeeSurchargeType.Subsidize,
                };
                kvOrder.SurCharges = new List<SurCharge> { surcharge };
            }
            return kvOrder;
        }
        private string GetKvOrderDescription(Order shopeeOrder)
        {
            if (shopeeOrder == null)
            {
                return "";
            }

            string description = "";

            if (string.IsNullOrEmpty(shopeeOrder.MessageToSeller) && string.IsNullOrEmpty(shopeeOrder.Note))
            {
                return $"Đơn hàng tự động tạo từ đơn Shopee {shopeeOrder.OrderSn}, người mua {shopeeOrder.BuyerUsername}";
            }

            if (!string.IsNullOrEmpty(shopeeOrder.MessageToSeller))
            {
                description += $"{shopeeOrder.MessageToSeller}\n";
            }

            if (!string.IsNullOrEmpty(shopeeOrder.Note))
            {
                description += $"{shopeeOrder.Note}\n\n";
            }
            else
            {
                description += "\n";
            }
            description += $"Đơn hàng tự động tạo từ đơn Shopee {shopeeOrder.OrderSn}, người mua {shopeeOrder.BuyerUsername}";
            return description;
        }
        private byte? ConvertToKvOrderStatus(string shopeeStatus,
            ShopeeTrackingInfoV2Response trackingInfo)
        {
            if (shopeeStatus == ChannelCommon.ShopeeStatus.Processed
                || shopeeStatus == ChannelCommon.ShopeeStatus.Shipped
                || shopeeStatus == ChannelCommon.ShopeeStatus.ToConfirmReceive
                || shopeeStatus == ChannelCommon.ShopeeStatus.Completed
                || shopeeStatus == ChannelCommon.ShopeeStatus.ReadyToShip)
            {
                return (byte)OrderState.Finalized;
            }

            if (shopeeStatus == ChannelCommon.ShopeeStatus.Cancelled)
            {
                if (trackingInfo?.LogisticsStatus == ShopeeDeliveryStatus.LogisticsDeliveryFailed)
                {
                    return (byte)OrderState.Finalized;
                }
                return (byte)OrderState.Void;
            }
            return null;
        }
        /// <summary>
        /// Lấy purchase date
        /// </summary>
        /// <param name="tracking">Lịch sử giao hàng</param>
        /// <param name="createTime">Thời gian tạo đơn</param>
        /// <returns></returns>
        private DateTime GetPurchaseDateV2(ShopeeTrackingInfoV2Response shopeeTracking,
            OmniChannelSettingObject settings, long shopeeCreateTime)
        {
            if (settings.SyncOrderSaleTimeType == (int)SyncOrderSaleTimeType.TimeCreateOrder)
                return shopeeCreateTime.FromUnixTime().ToLocalTime();

            if (shopeeTracking == null)
                return DateTime.Now;

            var trackingInfos = shopeeTracking?.TrackingInfo ?? new List<ShareKernel.Models.TrackingInfo>();
            var getOrderCreate = trackingInfos.FirstOrDefault(x => x.LogisticsStatus == ShopeeTrackingStatus.ORDER_CREATED.ToString());

            if (getOrderCreate == null)
                return DateTime.Now;

            return getOrderCreate.UpdateTime.FromUnixTime().ToLocalTime();
        }

        public KvOrder GenListKvOrder(int branchId, long userId, int retailerId, int groupId, OmniChannelSettingObject settings, string orderRawInfo)
        {
            throw new NotImplementedException();
        }

        public byte? ConvertToKvInvoiceStatus(byte? orderStatus)
        {
            if (orderStatus == (byte)OrderState.Finalized)
            {
                return (byte)InvoiceState.Pending;
            }
            if (orderStatus == (byte)OrderState.Void)
            {
                return (byte)InvoiceState.Void;
            }

            return null;
        }
    }
}

﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.FeatureToggle;
using Demo.OmniChannel.ChannelClient.Interfaces.NewBase;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Tiktok;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Models;
using Demo.OmniChannel.ShareKernel.RedisStreamMessage;
using Demo.OmniChannel.Utilities;
using Newtonsoft.Json;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Demo.OmniChannel.ChannelClient.Impls.NewBase
{
    public class TiktokBusinessImplimentBehaviorV2 : ITiktokBusinessClientV2
    {
        private readonly IAppSettings _appSettings;
        public TiktokBusinessImplimentBehaviorV2(IAppSettings appSettings)
        {
            _appSettings = appSettings;
            _ = new SelfAppConfig(appSettings);
        }

        /// <summary>
        /// Generate kvOrder
        /// </summary>
        /// <param name="branchId"></param>
        /// <param name="userId"></param>
        /// <param name="retailerId"></param>
        /// <param name="groupId"></param>
        /// <param name="settings"></param>
        /// <param name="orderRawInfo"></param>
        /// <returns></returns>
        public KvOrder GenListKvOrder(int branchId, long userId, int retailerId, int groupId,
           OmniChannelSettingObject settings, string orderRawInfo)
        {
            var titokOrderStream = JsonConvert.DeserializeObject<TiktokCreateOrderStreamMessage>(orderRawInfo);
            var tiktokOrder = JsonConvert.DeserializeObject<TiktokOrderDetail>(titokOrderStream.Order?.ToString());
            var shippingInfo = JsonConvert.DeserializeObject<TiktokShippingInfo>(titokOrderStream?.Logistics?.TitokShippingInfo?.ToString()
                ?? string.Empty);
            var reverse = JsonConvert.DeserializeObject<TiktokReverseOrderListResponse>(titokOrderStream?.Reverse?.TiktokReverseInfo?.ToString()
              ?? string.Empty);
            var kvOrder = GenKvOrder(tiktokOrder, shippingInfo, reverse, settings, branchId, userId, retailerId);
            kvOrder = CalFeeKvOrder(kvOrder, tiktokOrder, groupId);
            kvOrder.Discount = CalDiscount(tiktokOrder);
            return kvOrder;
        }


        /// <summary>
        /// Generate base kvOrder
        /// </summary>
        /// <param name="tiktokOrder"></param>
        /// <param name="shippingInfo"></param>
        /// <param name="reverse"></param>
        /// <param name="settings"></param>
        /// <param name="branchId"></param>
        /// <param name="userId"></param>
        /// <param name="retailerId"></param>
        /// <returns></returns>
        public KvOrder GenKvOrder(TiktokOrderDetail tiktokOrder, TiktokShippingInfo shippingInfo,
            TiktokReverseOrderListResponse reverse, OmniChannelSettingObject settings,
            int branchId, long userId, int retailerId)
        {
            var kvOrderCode = tiktokOrder.OrderId;
            var kvOrder = new KvOrder
            {
                Code = $"{ChannelTypeHelper.GetOrderPrefix((byte)ChannelTypeEnum.Tiktok)}_{kvOrderCode}",
                Ordersn = kvOrderCode,
                BranchId = branchId,
                SoldById = userId,
                RetailerId = retailerId,
                PurchaseDate = GetPurchaseDate(settings, shippingInfo, tiktokOrder.CreateTime),
                Description = GetDescription(tiktokOrder),
                IsSyncSuccess = false,
                UsingCod = true,
                Discount = CalDiscount(tiktokOrder),
                OrderDelivery = new InvoiceDelivery
                {
                    Receiver = tiktokOrder.RecipientAddress?.Name ?? string.Empty,
                    ContactNumber = tiktokOrder.RecipientAddress?.Phone,
                    Address = tiktokOrder.RecipientAddress?.FullAddress,
                    UsingPriceCod = true,
                    PartnerDelivery = new PartnerDelivery { Code = "TIKTOK", Name = "TIKTOK" },
                    DeliveryCode = tiktokOrder.TrackingNumber
                },
                CustomerPhone = tiktokOrder.RecipientAddress?.Phone,
                CustomerCode = $"{ChannelTypeHelper.GetCustomerPrefix((byte)ChannelTypeEnum.Tiktok)}{tiktokOrder.BuyerUid}",
                CustomerName = $"{tiktokOrder.RecipientAddress?.Name}",
                CustomerAddress = tiktokOrder.RecipientAddress?.FullAddress,
                OrderDetails = new List<KvOrderDetail>(),
                NewStatus = ConvertToKvOrderStatus(retailerId, tiktokOrder, reverse),
                ChannelStatus = tiktokOrder.OrderStatus.ToString(),
                ChannelCreatedDate = tiktokOrder.CreateTime.FromUnixTimeMs().ToLocalTime(),
                ChannelOrder = tiktokOrder.ToSafeJson(),

            };

            foreach (var detail in tiktokOrder.ItemList)
            {
                kvOrder.OrderDetails.Add(new KvOrderDetail
                {
                    ProductChannelId = (detail.SkuId > 0 ? detail.SkuId : detail.ProductId).ToString(),
                    ParentChannelProductId = detail.ProductId.ToString(),
                    ProductChannelSku = !string.IsNullOrEmpty(detail.SellerSku) ? detail.SellerSku : string.Empty,
                    ProductChannelName = $"{detail.ProductName} - {detail.SkuName}",
                    Quantity = detail.Quantity,
                    Price = detail.SkuOriginalPrice,
                    VariationId = detail.SkuId,
                    ItemId = detail.ProductId,
                    Uuid = StringHelper.GenerateUUID()
                });
            }
            kvOrder.DeliveryStatus = ConvertToKvDeliveryStatus(retailerId, tiktokOrder, reverse, settings);
            return kvOrder;
        }

        /// <summary>
        /// Caculator fee
        /// </summary>
        /// <param name="kvOrder"></param>
        /// <param name="tiktokOrder"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public KvOrder CalFeeKvOrder(KvOrder kvOrder, TiktokOrderDetail tiktokOrder, int groupId)
        {
            List<TiktokSettlement> settlementList = tiktokOrder.SettlementList ?? new List<TiktokSettlement>();
            settlementList = settlementList.Where(item => item.SettStatus != TiktokPaymentStatus.NotPay).ToList();
            List<FeeDetail> feeList;
            var enableTiktokCalFeeV2Toggle = new TiktokCalFeeV2Toggle(_appSettings).Enable(kvOrder.RetailerId, groupId);
            if (!enableTiktokCalFeeV2Toggle)
            {
                feeList = TiktokFeeHelper.OnConvertFeeJson(tiktokOrder, settlementList);
            }
            else
            {
                feeList = TiktokFeeHelper.OnConvertFeeJsonV2(tiktokOrder, settlementList);
                ReCalTiktokAddFeeOrSurcharge(feeList, kvOrder, settlementList, tiktokOrder);
            }

            if (feeList != null && feeList.Count > 0)
            {
                var feeDetail = new Fee { FeeList = feeList, ChannelType = (byte)ChannelTypeEnum.Tiktok };
                kvOrder.OrderDelivery.FeeJson = feeDetail.ToSafeJson();
                kvOrder.OrderDelivery.Price = (decimal)feeDetail.TotalFee();
            }
            return kvOrder;
        }

        /// <summary>
        /// Convert tiktok status => kv status
        /// </summary>
        /// <param name="tiktokOrder"></param>
        /// <param name="reverse"></param>
        /// <returns></returns>
        public byte? ConvertToKvOrderStatus(int retailerId, TiktokOrderDetail tiktokOrder,
         TiktokReverseOrderListResponse reverse)
        {
            var tiktokReverse = reverse?.ReverseList?.
                OrderByDescending(x => x.ReverseUpdate_time.FromUnixTime().ToLocalTime()).FirstOrDefault();

            switch (tiktokOrder.OrderStatus)
            {
                case TiktokOrderStatus.AwaitingCollection:
                case TiktokOrderStatus.InTransit:
                case TiktokOrderStatus.Delivered:
                case TiktokOrderStatus.Completed:
                    return (byte)OrderState.Finalized;
                case TiktokOrderStatus.Cancelled:
                    return GetTiktokOrderStatusCancelled(retailerId, tiktokOrder, tiktokReverse);
            }
            return null;
        }

        private byte? GetTiktokOrderStatusCancelled(int retailerId, TiktokOrderDetail tiktokOrder, TiktokReverseInfo tiktokReverse)
        {
            if (tiktokOrder == null)
            {
                return null;
            }
            if (tiktokReverse?.ReverseType == TikTokReverseType.CANCEL
                    && !string.IsNullOrEmpty(tiktokReverse?.OrderId) && !string.IsNullOrEmpty(tiktokOrder.TrackingNumber))
            {
                if (IsConfirmReturnWithCancelStatus(retailerId, tiktokOrder))
                {
                    return (byte)OrderState.Finalized;
                }
                return (byte)OrderState.Void;
            }
            else if (tiktokReverse?.ReverseType == TikTokReverseType.CANCEL
                || tiktokReverse?.ReverseType == TikTokReverseType.REQUEST_CANCEL
                || tiktokReverse?.ReverseType == TikTokReverseType.REFUND_ONLY
                || string.IsNullOrEmpty(tiktokOrder.TrackingNumber))
            {
                return (byte)OrderState.Void;
            }
            return null;
        }




        /// <summary>
        /// Convert tiktok status => delivery status
        /// </summary>
        /// <param name="tiktokOrder"></param>
        /// <param name="reverse"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        public byte? ConvertToKvDeliveryStatus(int retailerId, TiktokOrderDetail tiktokOrder,
           TiktokReverseOrderListResponse reverse, OmniChannelSettingObject settings)
        {
            int tiktokDeliveryStatus = tiktokOrder.OrderStatus;
            var tiktokReverse = reverse?.ReverseList?.
                 OrderBy(x => x.ReverseUpdate_time.FromUnixTime().ToLocalTime()).FirstOrDefault();

            if (IsDelivered(tiktokOrder, tiktokReverse))
            {
                return (byte)DeliveryStatus.Delivered;
            }

            if (tiktokDeliveryStatus == TiktokOrderStatus.AwaitingCollection)
            {
                return (byte)DeliveryStatus.Pending;
            }

            if (tiktokDeliveryStatus == TiktokOrderStatus.InTransit)
            {
                return (byte)DeliveryStatus.Delivering;
            }

            if (IsReturning(retailerId, tiktokOrder, tiktokReverse))
            {
                return settings.IsConfirmReturning
                    ? (byte)DeliveryStatus.Returning
                    : (byte)DeliveryStatus.Returned;
            }

            if (IsCancel(retailerId, tiktokOrder, tiktokReverse))
            {
                return (byte)DeliveryStatus.Void;
            }
            return null;
        }


        /// <summary>
        /// Convert status invoice
        /// </summary>
        /// <param name="orderStatus"></param>
        /// <returns></returns>
        public byte? ConvertToKvInvoiceStatus(byte? orderStatus)
        {
            if (orderStatus == null) return null;
            if (orderStatus == (byte)OrderState.Finalized)
            {
                return (byte)InvoiceState.Pending;
            }
            if (orderStatus == (byte)OrderState.Void)
            {
                return (byte)InvoiceState.Void;
            }
            return null;
        }

        #region Private method

        private bool IsReturning(int retailerId, TiktokOrderDetail tiktokOrder,
            TiktokReverseInfo reverse)
        {
            if (tiktokOrder.OrderStatus == TiktokOrderStatus.Cancelled
                && reverse?.ReverseType == TikTokReverseType.CANCEL
                && reverse?.ReverseStatusValue == 100
                && !string.IsNullOrEmpty(reverse.ReverseOrderId))
            {
                return IsConfirmReturnWithCancelStatus(retailerId, tiktokOrder);
            }
            if (tiktokOrder.OrderStatus == TiktokOrderStatus.Delivered
                && reverse?.ReverseType == TikTokReverseType.RETURN_AND_REFUND
                && reverse?.ReverseStatusValue == 100
                && !string.IsNullOrEmpty(reverse.ReverseOrderId))
                return true;
            if (tiktokOrder.OrderStatus == TiktokOrderStatus.Completed
                && reverse?.ReverseType == TikTokReverseType.RETURN_AND_REFUND)
                return true;
            return false;
        }

        private decimal? CalDiscount(TiktokOrderDetail tiktokOrder)
        {
            //Giảm giá = KOL-17516
            var discount = tiktokOrder.PaymentInfo.SellerDiscount;
            return discount;
        }

        private void ReCalTiktokAddFeeOrSurcharge(List<FeeDetail> feeList, KvOrder kvOrder, List<TiktokSettlement> settlementList,
            TiktokOrderDetail detail)
        {
            if (detail == null || settlementList == null || settlementList?.Count <= 0) return;

            var realShippingFee = settlementList
                .Where(item => item.SettlementInfo != null && StringExtension.IsValidStringNumber(item.SettlementInfo.ShippingFee))
                .Sum(item => Helpers.ToFloat(item.SettlementInfo.ShippingFee, 0));
            var totalShipFee = detail?.PaymentInfo?.ShippingFee - realShippingFee;
            if (totalShipFee.HasValue && totalShipFee.Value > 0)
            {
                AddSurchargeWhenTotalFeeBiggerZero(kvOrder, totalShipFee.Value);
            }
            else if (totalShipFee.HasValue && totalShipFee.Value < 0)
            {
                TiktokFeeHelper.ConvertTransactionFeeToShippingFee(feeList, Math.Abs(totalShipFee.Value));
            }
        }

        private void AddSurchargeWhenTotalFeeBiggerZero(KvOrder kvOrder, float totalShipFee)
        {
            if (totalShipFee <= 0) return;
            var surcharge = new SurCharge()
            {
                Value = (decimal)totalShipFee,
                Name = TiktokMessage.TiktokSurchargeMessage,
                Type = (byte)TiktokSurchargeType.FeeShip,
            };
            if (kvOrder.SurCharges != null && kvOrder.SurCharges.Any())
            {
                var isExisted = kvOrder.SurCharges.Any(x => x.Name == TiktokMessage.TiktokSurchargeMessage);
                if (!isExisted) kvOrder.SurCharges.Add(surcharge);
            }
            else
            {
                kvOrder.SurCharges = new List<SurCharge>() { surcharge };
            }
        }

        private string GetDescription(TiktokOrderDetail tiktokOrder)
        {
            if (null == tiktokOrder)
            {
                return string.Empty;
            }

            if (string.IsNullOrEmpty(tiktokOrder.BuyerMessage) && string.IsNullOrEmpty(tiktokOrder.SellerNote))
            {
                return $"Đơn hàng tự động tạo từ đơn Tiktok Shop {tiktokOrder.OrderId}, " +
                    $"người mua {tiktokOrder.RecipientAddress?.Name}.";
            }

            string description = "";
            if (!string.IsNullOrEmpty(tiktokOrder.BuyerMessage))
            {
                description += $"{tiktokOrder.BuyerMessage}.\n";
            }

            if (!string.IsNullOrEmpty(tiktokOrder.SellerNote))
            {
                description += $"{tiktokOrder.SellerNote}.\n\n";
            }
            else
            {
                description += "\n";
            }

            if (!string.IsNullOrEmpty(tiktokOrder.OrderId))
            {
                description += $"Đơn hàng tự động tạo từ đơn Tiktok Shop {tiktokOrder.OrderId}, " +
                    $"người mua {tiktokOrder.RecipientAddress?.Name}.";
            }

            return description;

        }

        private bool IsDelivered(TiktokOrderDetail tiktokOrder, TiktokReverseInfo reverse)
        {
            var paymentMethod = tiktokOrder.PaymentMethod;
            bool isCod = true;
            if (!string.IsNullOrEmpty(paymentMethod) && !paymentMethod.ToLower().Equals(TiktokPaymentMethod.Cod.ToLower()))
            {
                isCod = false;
            }
            if (tiktokOrder.OrderStatus == TiktokOrderStatus.Delivered &&
                reverse?.ReverseType != TikTokReverseType.RETURN_AND_REFUND)
                return true;

            if (tiktokOrder.OrderStatus == TiktokOrderStatus.Completed
                && !isCod
                && reverse?.ReverseType == TikTokReverseType.REFUND_ONLY
                && reverse?.ReverseStatusValue == TiktokReverseOrderStatus.COMPLETE)
                return false;
            if (tiktokOrder.OrderStatus == TiktokOrderStatus.Completed &&
                reverse?.ReverseType != TikTokReverseType.RETURN_AND_REFUND)
                return true;

            return false;
        }

        private bool IsCancel(int retailerId, TiktokOrderDetail tiktokOrder, TiktokReverseInfo reverse)
        {
            var paymentMethod = tiktokOrder.PaymentMethod;
            bool isCod = true;
            if (!string.IsNullOrEmpty(paymentMethod) && !paymentMethod.ToLower().Equals(TiktokPaymentMethod.Cod.ToLower()))
            {
                isCod = false;
            }
            if (tiktokOrder.OrderStatus == TiktokOrderStatus.Completed
               && !isCod
               && reverse?.ReverseType == TikTokReverseType.REFUND_ONLY
               && reverse?.ReverseStatusValue == TiktokReverseOrderStatus.COMPLETE)
                return true;

            if (tiktokOrder.OrderStatus == TiktokOrderStatus.Cancelled
                && string.IsNullOrEmpty(tiktokOrder.TrackingNumber))
                return true;

            if (tiktokOrder.OrderStatus == TiktokOrderStatus.Cancelled
                && reverse?.ReverseType == TikTokReverseType.CANCEL
                && reverse?.ReverseStatusValue == 100
                && !string.IsNullOrEmpty(reverse.ReverseOrderId))
            {
                return !IsConfirmReturnWithCancelStatus(retailerId, tiktokOrder);
            }
            if (tiktokOrder.OrderStatus == TiktokOrderStatus.Cancelled
            && reverse?.ReverseType == TikTokReverseType.REQUEST_CANCEL
            && reverse?.ReverseStatusValue == 100)
                return true;

            return false;
        }

        private bool IsConfirmReturnWithCancelStatus(int retailerId, TiktokOrderDetail tiktokOrder)
        {
            var useTikTokReturnConfirmFeature = SelfAppConfig.UseTikTokReturnConfirmFeature;
            if (useTikTokReturnConfirmFeature != null
                && useTikTokReturnConfirmFeature.Enable
                && (useTikTokReturnConfirmFeature.IncludeRetail.Count == 0 || useTikTokReturnConfirmFeature.IncludeRetail.Contains(retailerId))
                && !string.IsNullOrEmpty(tiktokOrder.CancelReason)
                 && !string.IsNullOrEmpty(tiktokOrder.CancelUser)
                && useTikTokReturnConfirmFeature.CancelReasonLst.Contains(tiktokOrder.CancelReason.ToLower())
                && useTikTokReturnConfirmFeature.CancelUserLst.Contains(tiktokOrder.CancelUser.ToLower()))
            {
                return true;
            }
            return false;
        }

        private DateTime GetPurchaseDate(OmniChannelSettingObject settings,
            TiktokShippingInfo shippingInfo, long orderCreateTime)
        {
            if (settings.SyncOrderSaleTimeType == (int)SyncOrderSaleTimeType.TimeCreateOrder)
                return orderCreateTime.FromUnixTimeMs().ToLocalTime();

            if (shippingInfo == null)
                return orderCreateTime.FromUnixTimeMs().ToLocalTime();

            var trackingInfos = shippingInfo?.TrackingInfoList?.Where(x => x.TrackingInfo != null).
                SelectMany(x => x.TrackingInfo).OrderBy(d => d.UpdateTime.FromUnixTimeMs().ToLocalTime()).ToList();

            //Lấy thời điểm sẵn sàng giao
            if (trackingInfos != null && trackingInfos.Any() && trackingInfos.Count > 1)
            {
                var getOrderCreate = trackingInfos[1];
                return getOrderCreate.UpdateTime.FromUnixTimeMs().ToLocalTime();
            }

            return orderCreateTime.FromUnixTimeMs().ToLocalTime();
        }

        #endregion

    }
}

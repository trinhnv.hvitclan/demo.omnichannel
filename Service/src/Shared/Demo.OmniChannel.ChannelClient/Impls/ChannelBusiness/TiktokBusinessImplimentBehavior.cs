﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.FeatureToggle;
using Demo.OmniChannel.ChannelClient.Interfaces.NewBase;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Tiktok;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Models;
using Demo.OmniChannel.ShareKernel.RedisStreamMessage;
using Newtonsoft.Json;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Demo.OmniChannel.ChannelClient.Impls.NewBase
{
    public class TiktokBusinessImplimentBehavior : ITiktokBusinessClient
    {
        private readonly IAppSettings _appSettings;
        private readonly UseTikTokReturnConfirmFeature _useTikTokReturnConfirmFeature;
        public TiktokBusinessImplimentBehavior(IAppSettings appSettings)
        {
            _appSettings = appSettings;
            _useTikTokReturnConfirmFeature = _appSettings.Get<UseTikTokReturnConfirmFeature>("UseTikTokReturnConfirmFeature");
            _ = new SelfAppConfig(appSettings);
        }
        public KvOrder GenListKvOrder(int branchId, long userId, int retailerId, int groupId,
            OmniChannelSettingObject settings, string orderRawInfo)
        {
            var titokOrderStream = JsonConvert.DeserializeObject<TiktokCreateOrderStreamMessage>(orderRawInfo);
            var tiktokOrder = JsonConvert.DeserializeObject<TiktokOrderDetail>(titokOrderStream.Order?.ToString());
            var shippingInfo = JsonConvert.DeserializeObject<TiktokShippingInfo>(titokOrderStream?.Logistics?.TitokShippingInfo?.ToString()
                ?? string.Empty);
            var kvOrder = GenKvOrder(tiktokOrder, shippingInfo, settings, branchId, userId, retailerId);
            kvOrder = CalFeeKvOrder(kvOrder, tiktokOrder, groupId);
            kvOrder.Discount = CalDiscount(tiktokOrder);
            return kvOrder;
        }

        #region private method
        private KvOrder GenKvOrder(TiktokOrderDetail tiktokOrder, TiktokShippingInfo shippingInfo,
            OmniChannelSettingObject settings, int branchId, long userId, int retailerId)
        {
            var kvOrderCode = tiktokOrder.OrderId;
            var kvOrder = new KvOrder
            {
                Code = $"DHTTS_{kvOrderCode}",
                Ordersn = kvOrderCode,
                BranchId = branchId,
                SoldById = userId,
                RetailerId = retailerId,
                PurchaseDate = GetPurchaseDate(settings, shippingInfo, tiktokOrder.CreateTime),
                Description = GetDescription(tiktokOrder),
                IsSyncSuccess = false,
                UsingCod = true,
                OrderDelivery = new InvoiceDelivery
                {
                    Receiver = tiktokOrder.RecipientAddress?.Name ?? string.Empty,
                    ContactNumber = tiktokOrder.RecipientAddress?.Phone,
                    Address = tiktokOrder.RecipientAddress?.FullAddress,
                    UsingPriceCod = true,
                    PartnerDelivery = new PartnerDelivery { Code = "TIKTOK", Name = "TIKTOK" },
                    DeliveryCode = tiktokOrder.TrackingNumber
                },
                CustomerPhone = tiktokOrder.RecipientAddress?.Phone,
                CustomerCode = $"KHTTS{tiktokOrder.BuyerUid}",
                CustomerName = tiktokOrder.RecipientAddress?.Name,
                CustomerAddress = tiktokOrder.RecipientAddress?.FullAddress,
                CustomerLocation = $"{tiktokOrder.RecipientAddress?.State} - {tiktokOrder.RecipientAddress?.City}",
                OrderDetails = new List<KvOrderDetail>(),
                NewStatus = ConvertToKvOrderStatus(tiktokOrder),
                ChannelStatus = tiktokOrder.OrderStatus.ToString(),
                ChannelCreatedDate = tiktokOrder.CreateTime.FromUnixTimeMs().ToLocalTime(),
                ChannelOrder = tiktokOrder.ToSafeJson()
            };

            foreach (var detail in tiktokOrder.ItemList)
            {
                kvOrder.OrderDetails.Add(new KvOrderDetail
                {
                    ProductChannelId = (detail.SkuId > 0 ? detail.SkuId : detail.ProductId).ToString(),
                    ParentChannelProductId = detail.ProductId.ToString(),
                    ProductChannelSku = !string.IsNullOrEmpty(detail.SellerSku) ? detail.SellerSku : string.Empty,
                    ProductChannelName = $"{detail.ProductName} - {detail.SkuName}",
                    Quantity = detail.Quantity,
                    Price = detail.SkuOriginalPrice,
                    VariationId = detail.SkuId,
                    ItemId = detail.ProductId,
                });
            }
            kvOrder.DeliveryStatus = ConvertToKvDeliveryStatus(tiktokOrder, settings);
            return kvOrder;
        }

        private byte? ConvertToKvDeliveryStatus(TiktokOrderDetail tiktokOrder,
            OmniChannelSettingObject settings)
        {
            int tiktokDeliveryStatus = tiktokOrder.OrderStatus;
            if (IsDelivered(tiktokOrder))
            {
                return (byte)DeliveryStatus.Delivered;
            }

            if (tiktokDeliveryStatus == TiktokOrderStatus.AwaitingCollection)
            {
                return (byte)DeliveryStatus.Pending;
            }

            if (tiktokDeliveryStatus == TiktokOrderStatus.InTransit)
            {
                return (byte)DeliveryStatus.Delivering;
            }

            if (IsCheckUseReturning() && tiktokDeliveryStatus == TiktokOrderStatus.Cancelled
                && IsReturing(tiktokOrder))
            {
                return settings.IsConfirmReturning ? (byte)DeliveryStatus.Returning : (byte)DeliveryStatus.Returned;
            }

            if (IsCancel(tiktokOrder))
            {
                return (byte)DeliveryStatus.Void;
            }
            return (byte)DeliveryStatus.Delivering;
        }

        private bool IsReturing(TiktokOrderDetail tiktokOrder)
        {
            if (!string.IsNullOrEmpty(tiktokOrder.CancelReason)
                && !string.IsNullOrEmpty(tiktokOrder.CancelUser)
                 && IsValidLst(_useTikTokReturnConfirmFeature.CancelUserLst)
                 && IsValidLst(_useTikTokReturnConfirmFeature.CancelReasonLst)
                 && (_useTikTokReturnConfirmFeature.CancelUserLst.Contains(tiktokOrder.CancelUser)
                 || tiktokOrder.CancelUser.Equals(TiktokCancelMessage.CanncelUser, StringComparison.OrdinalIgnoreCase))
                 && (_useTikTokReturnConfirmFeature.CancelReasonLst.Contains(tiktokOrder.CancelReason)
                 || tiktokOrder.CancelReason.Equals(TiktokCancelMessage.CanncelReason, StringComparison.OrdinalIgnoreCase)
                 || tiktokOrder.CancelReason.Equals(TiktokCancelMessage.CanncelReasonEn, StringComparison.OrdinalIgnoreCase)))
            {
                return true;
            }
            return false;
        }

        private bool IsValidLst(List<string> input)
        {
            if (input != null && input.Any())
            {
                return true;
            }
            return false;
        }
        private bool IsCheckUseReturning()
        {
            if (!_useTikTokReturnConfirmFeature.Enable)
            {
                return false;
            }
            return true;
        }

        private byte? ConvertToKvOrderStatus(TiktokOrderDetail tiktokOrder)
        {
            switch (tiktokOrder.OrderStatus)
            {
                case TiktokOrderStatus.AwaitingCollection:
                case TiktokOrderStatus.InTransit:
                case TiktokOrderStatus.Delivered:
                    return (byte)OrderState.Finalized;
                case TiktokOrderStatus.Completed:
                    if (!string.IsNullOrEmpty(tiktokOrder.CancelUser))
                    {
                        return (byte)OrderState.Void;
                    }
                    return (byte)OrderState.Finalized;
                case TiktokOrderStatus.Cancelled:
                    if (IsReturing(tiktokOrder))
                    {
                        return (byte)OrderState.Finalized;
                    }
                    return (byte)OrderState.Void;
            }
            return null;
        }

        private decimal? CalDiscount(TiktokOrderDetail tiktokOrder)
        {
            //Tổng tiền hàng
            var toltalAmount = tiktokOrder.ItemList.Sum(p => p.Quantity * p.SkuOriginalPrice);

            //Khách cần trả
            var toltalNeedReturn = tiktokOrder.PaymentInfo.SubTotal + tiktokOrder.PaymentInfo.PlatformDiscount
                - tiktokOrder.PaymentInfo.ShippingFeesellerDiscount;

            //Giảm giá = KOL-17516
            var discount = tiktokOrder.PaymentInfo.SellerDiscount;
            return discount;
        }


        private KvOrder CalFeeKvOrder(KvOrder kvOrder, TiktokOrderDetail tiktokOrder, int groupId)
        {
            List<TiktokSettlement> settlementList = tiktokOrder.SettlementList;
            settlementList = settlementList.Where(item => item.SettStatus != TiktokPaymentStatus.NotPay).ToList();
            List<FeeDetail> feeList;
            var enableTiktokCalFeeV2Toggle = new TiktokCalFeeV2Toggle(_appSettings).Enable(kvOrder.RetailerId, groupId);
            if (!enableTiktokCalFeeV2Toggle)
            {
                feeList = TiktokFeeHelper.OnConvertFeeJson(tiktokOrder, settlementList);
            }
            else
            {
                feeList = TiktokFeeHelper.OnConvertFeeJsonV2(tiktokOrder, settlementList);
                ReCalTiktokAddFeeOrSurcharge(feeList, kvOrder, settlementList, tiktokOrder);
            }

            if (feeList != null && feeList.Count > 0)
            {
                var feeDetail = new Fee { FeeList = feeList, ChannelType = (byte)ChannelTypeEnum.Tiktok };
                kvOrder.OrderDelivery.FeeJson = feeDetail.ToSafeJson();
                kvOrder.OrderDelivery.Price = (decimal)feeDetail.TotalFee();
            }
            return kvOrder;
        }

        private void ReCalTiktokAddFeeOrSurcharge(List<FeeDetail> feeList, KvOrder kvOrder, List<TiktokSettlement> settlementList, TiktokOrderDetail detail)
        {
            if (detail == null || settlementList == null || settlementList?.Count <= 0) return;

            var realShippingFee = settlementList
                .Where(item => item.SettlementInfo != null && StringExtension.IsValidStringNumber(item.SettlementInfo.ShippingFee))
                .Sum(item => Helpers.ToFloat(item.SettlementInfo.ShippingFee, 0));
            var totalShipFee = detail?.PaymentInfo?.ShippingFee - realShippingFee;
            if (totalShipFee.HasValue && totalShipFee.Value > 0)
            {
                AddSurchargeWhenTotalFeeBiggerZero(kvOrder, totalShipFee.Value);
            }
            else if (totalShipFee.HasValue && totalShipFee.Value < 0)
            {
                TiktokFeeHelper.ConvertTransactionFeeToShippingFee(feeList, Math.Abs(totalShipFee.Value));
            }
        }

        private void AddSurchargeWhenTotalFeeBiggerZero(KvOrder kvOrder, float totalShipFee)
        {
            if (totalShipFee <= 0) return;
            var surcharge = new SurCharge()
            {
                Value = (decimal)totalShipFee,
                Name = TiktokMessage.TiktokSurchargeMessage,
                Type = (byte)TiktokSurchargeType.FeeShip,
            };
            if (kvOrder.SurCharges != null && kvOrder.SurCharges.Any())
            {
                var isExisted = kvOrder.SurCharges.Any(x => x.Name == TiktokMessage.TiktokSurchargeMessage);
                if (!isExisted) kvOrder.SurCharges.Add(surcharge);
            }
            else
            {
                kvOrder.SurCharges = new List<SurCharge>() { surcharge };
            }
        }

        private string GetDescription(TiktokOrderDetail tiktokOrder)
        {
            if (null == tiktokOrder)
            {
                return string.Empty;
            }

            if (string.IsNullOrEmpty(tiktokOrder.BuyerMessage) && string.IsNullOrEmpty(tiktokOrder.SellerNote))
            {
                return $"Đơn hàng tự động tạo từ đơn Tiktok Shop {tiktokOrder.OrderId}, người mua {tiktokOrder.RecipientAddress?.Name}.";
            }

            string description = "";
            if (!string.IsNullOrEmpty(tiktokOrder.BuyerMessage))
            {
                description += $"{tiktokOrder.BuyerMessage}.\n";
            }

            if (!string.IsNullOrEmpty(tiktokOrder.SellerNote))
            {
                description += $"{tiktokOrder.SellerNote}.\n\n";
            }
            else
            {
                description += "\n";
            }

            if (!string.IsNullOrEmpty(tiktokOrder.OrderId))
            {
                description += $"Đơn hàng tự động tạo từ đơn Tiktok Shop {tiktokOrder.OrderId}, người mua {tiktokOrder.RecipientAddress?.Name}.";
            }

            return description;

        }

        private bool IsDelivered(TiktokOrderDetail tiktokOrder)
        {
            if (tiktokOrder.OrderStatus == TiktokOrderStatus.Delivered)
                return true;

            if (tiktokOrder.OrderStatus == TiktokOrderStatus.Completed &&
                string.IsNullOrEmpty(tiktokOrder.CancelUser))
            {
                return true;
            }
            return false;
        }

        private bool IsCancel(TiktokOrderDetail tiktokOrder)
        {
            if (tiktokOrder.OrderStatus == TiktokOrderStatus.Cancelled)
                return true;
            if (tiktokOrder.OrderStatus == TiktokOrderStatus.Completed && !string.IsNullOrEmpty(tiktokOrder.CancelUser))
                return true;
            return false;
        }

        private DateTime GetPurchaseDate(OmniChannelSettingObject settings,
            TiktokShippingInfo shippingInfo, long orderCreateTime)
        {
            if (settings.SyncOrderSaleTimeType == (int)SyncOrderSaleTimeType.TimeCreateOrder)
                return orderCreateTime.FromUnixTimeMs().ToLocalTime();

            if (shippingInfo == null)
                return orderCreateTime.FromUnixTimeMs().ToLocalTime();

            var trackingInfos = shippingInfo?.TrackingInfoList?.Where(x => x.TrackingInfo != null).
                SelectMany(x => x.TrackingInfo).OrderBy(d => d.UpdateTime.FromUnixTimeMs().ToLocalTime()).ToList();

            //Lấy thời điểm sẵn sàng giao
            if (trackingInfos != null && trackingInfos.Any() && trackingInfos.Count > 1)
            {
                var getOrderCreate = trackingInfos[1];
                return getOrderCreate.UpdateTime.FromUnixTimeMs().ToLocalTime();
            }

            return orderCreateTime.FromUnixTimeMs().ToLocalTime();
        }
        #endregion
    }
}

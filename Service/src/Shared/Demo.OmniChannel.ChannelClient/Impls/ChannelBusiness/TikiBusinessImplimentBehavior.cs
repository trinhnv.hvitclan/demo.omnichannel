﻿using Demo.OmniChannel.ChannelClient.Interfaces.NewBase;
using ServiceStack.Configuration;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ShareKernel.Models;
using System.Collections.Generic;
using ServiceStack;
using Demo.OmniChannel.ShareKernel.Common;
using System;

namespace Demo.OmniChannel.ChannelClient.Impls.ChannelBusiness
{
    public class TikiBusinessImplimentBehavior : ITikiBusinessClient
    {
        private readonly IAppSettings _settings;
        public TikiBusinessImplimentBehavior(IAppSettings appSettings)
        {
            _settings = appSettings;
        }
        public KvOrder CalFeeKvOrder(KvOrder kvOrder, TikiOrderV2 tikiOrder)
        {
            if (tikiOrder?.Invoice?.TotalSellerFee != null)
            {
                var configFeeTikiV21Feature = _settings.Get<FeeTikiV21Feature>("FeeTikiV21Feature");
                List<FeeDetail> feeList = new List<FeeDetail>();
                if(configFeeTikiV21Feature != null)
                {
                    tikiOrder.Invoice.TotalSellerFee = (decimal)(configFeeTikiV21Feature?.TotalSellerFee == 0 ? tikiOrder.Invoice.TotalSellerFee : configFeeTikiV21Feature?.TotalSellerFee);
                    if (tikiOrder.Invoice.TotalSellerFee < 1)
                    {
                        tikiOrder.Invoice.TotalSellerFee = Math.Abs(tikiOrder.Invoice.TotalSellerFee);
                        feeList.Add(new FeeDetail()
                        {
                            Name = "TikiFee",
                            Value = Helpers.ToFloat(tikiOrder.Invoice.TotalSellerFee, 0)
                        });
                    }
                    else
                    {
                        feeList.Add(new FeeDetail()
                        {
                            Name = "TikiFee",
                            Value = Helpers.ToFloat(-tikiOrder.Invoice.TotalSellerFee, 0)
                        });
                        tikiOrder.Invoice.TotalSellerFee = 0;
                    }

                    var feeDetail = new Fee { FeeList = feeList, ChannelType = (byte)ChannelTypeEnum.Tiki };
                    kvOrder.OrderDelivery.FeeJson = feeDetail.ToSafeJson();
                    kvOrder.OrderDelivery.Price = tikiOrder.Invoice.TotalSellerFee;
                }
            }
            return kvOrder;
        }

    }
}

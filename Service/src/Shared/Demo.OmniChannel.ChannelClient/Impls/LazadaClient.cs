﻿using Demo.Lazada.Sdk;
using Demo.Lazada.Sdk.Models;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.EndPoints;
using Demo.OmniChannel.ChannelClient.Helper;
using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.RequestDTO;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Lazada;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Impls;
using Demo.OmniChannel.Sdk.Interfaces;
using Demo.OmniChannel.Sdk.RequestDtos;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Dtos;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.ShareKernel.Models;
using Demo.OmniChannel.Utilities;
using Newtonsoft.Json;
using Polly;
using Polly.Timeout;
using RestSharp;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Logging;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Image = Demo.OmniChannel.ShareKernel.Dtos.Image;
using InvoiceDelivery = Demo.OmniChannel.ChannelClient.Models.InvoiceDelivery;
using InvoiceDetail = Demo.OmniChannel.ChannelClient.Models.InvoiceDetail;
using LazadaStatus = Demo.OmniChannel.ChannelClient.Common.LazadaStatus;
using Logistics = Demo.OmniChannel.ChannelClient.Models.Logistics;
using Order = Demo.OmniChannel.ChannelClient.Models.Order;
using OrderDetail = Demo.OmniChannel.ChannelClient.Models.OrderDetail;
using PartnerDelivery = Demo.OmniChannel.ChannelClient.Models.PartnerDelivery;
using Platform = Demo.OmniChannel.ChannelClient.Models.Platform;
using ProductDetailResponse = Demo.OmniChannel.ChannelClient.RequestDTO.ProductDetailResponse;
using SelfAppConfig = Demo.OmniChannel.ChannelClient.Common.SelfAppConfig;
using TokenResponse = Demo.OmniChannel.ChannelClient.Models.TokenResponse;

namespace Demo.OmniChannel.ChannelClient.Impls
{
    public class LazadaClient : IBaseClient, ILazadaClient
    {
        private const string LazadaSuccessCode = "0";
        private readonly IChannelInternalClient _channelInternalClient;
        private SelfAppConfig _config;
        private const string LazadaInvalidTokenCode = "IllegalAccessToken";
        private const string LazadaInvalidRefreshTokenCode = "IllegalRefreshToken";
        private const string LazadaServerErrorMessage = "The request has failed due to RPC timeout";
        private const string LazadaApiCallLimit = "ApiCallLimit";

        private ILog Log => LogManager.GetLogger(typeof(LazadaClient));

        public LazadaClient(IAppSettings settings)
        {
            _channelInternalClient = new ChannelInternalClient(settings);
            _config = new SelfAppConfig(settings);
        }

        public async Task<ShopInfoResponseV2> GetShopInfo(ChannelAuth auth, KvInternalContext context,
            bool isOnlyGetInfo = false, Platform platform = null)
        {
            SellerProfileResponse shop;
            if (isOnlyGetInfo && !string.IsNullOrEmpty(auth?.AccessToken))
            {
                shop = await GetSellerInfo(auth.AccessToken);
                return new ShopInfoResponseV2
                {
                    Email = shop.Data.Email,
                    Name = shop.Data.Name,
                    IdentityKey = shop.Data.SellerId
                };
            }
            if (string.IsNullOrEmpty(auth.Code))
            {
                throw new OmniException("Missing Auth Code");
            }
            var tokenItem = await CreateToken(auth.Code, null, null);
            if (tokenItem == null || string.IsNullOrEmpty(tokenItem.AccessToken))
            {
                throw new OmniException("Access Token invalid");
            }
            shop = await GetSellerInfo(tokenItem.AccessToken);
            var authId = await _channelInternalClient.CreateChannelAuthAsync(context, new ChannelAuthRequest
            {
                AccessToken = tokenItem.AccessToken,
                RefreshToken = tokenItem.RefreshToken,
                ExpiresIn = tokenItem.ExpiresIn,
                RefreshExpiresIn = tokenItem.RefreshExpiresIn
            });
            return new ShopInfoResponseV2
            {
                AuthId = authId,
                Email = shop.Data.Email,
                Name = shop.Data.Name,
                IdentityKey = shop.Data.SellerId
            };
        }

        public async Task<TokenResponse> CreateToken(string code, ChannelAuth auth, Platform platform)
        {
            var param = new Dictionary<string, string> {
            {
                "code", code
            } };

            var client = new LazopClient(SelfAppConfig.LazadaHostAuthApi,
                SelfAppConfig.LazadaClientAppId,
                SelfAppConfig.LazadaApiSercetKey);
            LazopRequest lzdRequest = new LazopRequest();
            lzdRequest.SetApiName(LazadaEndpoint.CreateToken);
            lzdRequest.SetHttpMethod("POST");
            foreach (var pa in param)
            {
                lzdRequest.AddApiParameter(pa.Key, pa.Value);
            }
            var res = await client.ExecuteAsync(lzdRequest, null);
            try
            {
                return res.Content.FromJson<TokenResponse>();
            }
            catch
            {
                return null;
            }
        }

        public Task<CustomerResponse> GetCustomerByOrderId(int retailerId, long channelId, string orderId,
            ChannelAuth auth, Guid logId)
        {
            return null;
        }

        private async Task<SellerProfileResponse> GetSellerInfo(string accessToken)
        {
            try
            {
                var isUseGrpc = SelfAppConfig.ForwarderApiFeature.Enable &&
            SelfAppConfig.ForwarderApiFeature.DefaultForwarder;
                var response = await Policy
                    .HandleResult<IRestResponse<LazopResponse>>(msg => msg.StatusCode == HttpStatusCode.GatewayTimeout)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                        (result, timeSpan, retryCount, context) => { })
                    .ExecuteAsync(async () =>
                        {
                            var client = new LazopClient(SelfAppConfig.LazadaHostApi, SelfAppConfig.LazadaClientAppId, SelfAppConfig.LazadaApiSercetKey);
                            LazopRequest lzdRequest = new LazopRequest();
                            lzdRequest.SetApiName(LazadaEndpoint.GetSellerProfile);
                            lzdRequest.SetHttpMethod("GET");
                            return await ExecuteOrGrpc(lzdRequest, accessToken, isUseGrpc);
                        });
                return response.Content.FromJson<SellerProfileResponse>();
            }
            catch
            {
                return null;
            }
        }

        public async Task<ProductListResponse> GetListProduct(ChannelAuth auth, long channelId, Guid logId,
            bool isFirstSync, DateTime? updateTimeFrom = null, KvInternalContext kvContext = null,
            Platform platform = null)
        {
            List<string> requestIds = new List<string>();
            var getListProductLog = new LogObject(Log, logId)
            {
                Action = string.Format(EcommerceTypeException.GetListProduct, Ecommerce.Lazada),
                OmniChannelId = channelId,
                RequestObject = updateTimeFrom
            };

            var result = new ProductListResponse
            {
                Data = new List<Product>()
            };
            var response = await GetListProductWithTaskAsync(auth.AccessToken, logId, updateTimeFrom);
            var ignoreStatusLst = SelfAppConfig.LazadaProductIgnoreStatus;
            foreach (var item in response)
            {
                var prods = item.Skus.Where(x => !ignoreStatusLst.Contains(x.Status));
                result.Data.AddRange(prods.Select(x => new Product
                {
                    ItemId = x.SkuId.ToString(),
                    ParentItemId = item.ItemId.ToString(),
                    ItemSku = x.SellerSku,
                    ItemName = GetFullNameProduct(item.Attribute?.Name, x.SaleProp),
                    ItemImages = x.Images.Where(r => !string.IsNullOrEmpty(r)).ToList(),
                    Type = 1,
                    Status = x.Status
                }));
            }
            var products = result.Data.GroupBy(x => new { x.ItemId, x.ItemSku }).Select(p => p.FirstOrDefault()).ToList();
            result.Data = products;
            result.Total = products.Count;
            if (!isFirstSync)
            {
                result.ParentChangeIds = products.Select(x => x.ParentItemId.ToString()).Distinct().ToList();
            }
            getListProductLog.TotalItem = products.Count;
            getListProductLog.ResponseObject = $"RequestIds: {requestIds.ToSafeJson()} - ProductSkus: {result.Data?.Select(x => x.ItemSku).ToSafeJson()}";
            getListProductLog.LogInfo(true);

            return result;
        }

        private string GetFullNameProduct(string name, Dictionary<string, string> saleProp)
        {
            if (string.IsNullOrEmpty(name))
            {
                return string.Empty;
            }
            StringBuilder bulderNameProduct = new StringBuilder();
            bulderNameProduct.Append(name);
            if (saleProp.Count == 0)
            {
                return bulderNameProduct.ToString();
            }
            var separateText = " - ";
            foreach (KeyValuePair<string, string> entry in saleProp)
            {
                if (entry.Value != null)
                {
                    bulderNameProduct.Append($"{separateText}{entry.Value}");
                }
            }
            return bulderNameProduct.ToString();
        }
        private async Task<List<ProductMap>> GetListProductWithMaxOffset(int fromOffset, int? maxOffset, int pageSize, string accessToken, Guid logId, DateTime? updateTimeFrom = null, bool isGetDeleted = false)
        {
            var items = new List<ProductMap>();
            var response = await GetLazadaProducts(fromOffset, pageSize, updateTimeFrom, accessToken, logId, isGetDeleted);
            var totalItem = response.Data?.Products?.Count ?? 0;
            var totalSumProduct = maxOffset ?? response?.Data?.Total ?? 0;
            if (totalItem <= 0 || response == null || totalSumProduct == 0)
            {
                return items;
            }
            items.AddRange(response.Data.Products);
            if (totalItem < totalSumProduct)
            {
                int pages = (int)(totalSumProduct % totalItem == 0 ? (totalSumProduct / totalItem) : (totalSumProduct / totalItem) + 1);
                int lazadaRequestGetProductMaximum = SelfAppConfig.LazadaRequestGetProductMaximum;
                if (lazadaRequestGetProductMaximum > 0 && pages > lazadaRequestGetProductMaximum)
                {
                    pages = lazadaRequestGetProductMaximum;
                }
                int taskCallMaxPerThread = SelfAppConfig.LazadaTaskCallMaxPerThread;
                int numberOfBatchCallPageSize = pages % taskCallMaxPerThread == 0 ? pages / taskCallMaxPerThread : (pages / taskCallMaxPerThread) + 1;
                for (int i = 0; i < numberOfBatchCallPageSize; i++)
                {
                    int maxSize = (pages - (i + 1) * taskCallMaxPerThread) > 0 ? taskCallMaxPerThread : (pages - i * taskCallMaxPerThread);
                    var taskLst = new List<Task<List<ProductMap>>>();
                    List<int> pageNumberLst = GetPageNumberList(i, maxSize);
                    foreach (var pageNumber in pageNumberLst)
                    {
                        int offset = pageNumber * pageSize + fromOffset;
                        taskLst.Add(GetGetProductListPollyResponse(offset, pageSize, updateTimeFrom, accessToken, logId, isGetDeleted));
                    }
                    var taskResultList = await Task.WhenAll(taskLst);
                    foreach (var itemResult in taskResultList)
                    {
                        items.AddRange(itemResult);
                    }
                }
            }
            return items;
        }

        private async Task<List<ProductMap>> GetListProductWithTaskAsync(string accessToken, Guid logId, DateTime? updateTimeFrom = null, bool isGetDeleted = false)
        {
            var pageSize = SelfAppConfig.LazadaProductPageSize;
            return await GetListProductWithMaxOffset(0, null, pageSize, accessToken, logId, updateTimeFrom, isGetDeleted);
        }

        private async Task<List<ProductMap>> GetGetProductListPollyResponse(int offset, int pageSize, DateTime? updateTimeFrom, string accessToken, Guid logId, bool isGetDeleted = false)
        {
            try
            {
                var response = await GetLazadaProducts(offset, pageSize, updateTimeFrom, accessToken, logId, isGetDeleted);
                if (response?.Data?.Products?.Count > 0)
                {
                    return response.Data.Products;
                }
                return new List<ProductMap>();
            }
            catch (LazadaException ex)
            {
                if (ex.Message.Equals(LazadaServerErrorMessage))
                {
                    int size = pageSize / 4;
                    return await GetListProductWithMaxOffset(offset, pageSize, size, accessToken, logId, updateTimeFrom);
                }
            }
            return new List<ProductMap>();
        }

        private List<int> GetPageNumberList(int index, int taskCallMaxPerThread)
        {
            int fromPageNumber = index * taskCallMaxPerThread; // = 0
            List<int> result = new List<int>();
            for (int i = 0; i < taskCallMaxPerThread; i++)
            {
                int pageNumber = fromPageNumber + i + 1;
                result.Add(pageNumber);
            }
            return result;
        }

        public async Task<ProductListResponse> GetListDeletedProduct(ChannelAuth auth, long channelId, Guid logId,
          bool isFirstSync, KvInternalContext kvContext = null, DateTime? updateTimeFrom = null)
        {
            try
            {
                List<string> requestIds = new List<string>();
                var getListProductLog = new LogObject(Log, logId)
                {
                    Action = string.Format(EcommerceTypeException.GetListDeletedProduct, Ecommerce.Lazada),
                    OmniChannelId = channelId,
                    RequestObject = DateTime.Now,
                };
                var result = new ProductListResponse
                {
                    Data = new List<Product>(),
                    VariationProductActives = new List<VariationProduct>()
                };
                var response = await GetListProductWithTaskAsync(auth.AccessToken, logId, updateTimeFrom, true);

                result.VariationProductActives.AddRange(response.Where(x => x.Skus.Any())
                         .Select(x => new VariationProduct
                         {
                             ParentId = x.ItemId.ToString(),
                             VariationProductActiveIds = x.Skus.Select(sku => sku.SkuId.ToString()).ToList()
                         }).ToList() ?? new List<VariationProduct>());

                getListProductLog.ResponseObject = $"RequestIds: {requestIds.ToSafeJson()} - TotalProduct: {result.Total.ToSafeJson()}";
                getListProductLog.LogInfo(true);

                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public async Task<ProductDetailResponse> GetProductDetail(ChannelAuth auth, long channelId,
            Guid logId, object productId, object parentProductId, string productSku, Platform platform)
        {
            var getGetProductDetailLog = new LogObject(Log, logId)
            {
                Action = string.Format(EcommerceTypeException.GetProductDetail, Ecommerce.Lazada),
                OmniChannelId = channelId,
                RequestObject = $"Id: {productId} Sku:{productSku}"
            };
            var productDetail = await GetLazadaProductDetail(DataRequestHelper.ParseItemIdToLong(parentProductId?.ToString()), productSku, auth.AccessToken);

            if (productDetail?.Data == null)
            {
                return null;
            }

            var prdDetails = productDetail.Data.Skus.Where(x => x.Status == "active")
                .Select(x => new ProductDetailSkus
                {
                    Sku = x.SellerSku,
                    Status = x.Status,
                    Images = x.Images.Where(p => !string.IsNullOrEmpty(p)).ToList(),
                    Description = productDetail.Data.Attribute.Description ?? productDetail.Data.Attribute.ShortDescription
                }).ToList();

            var result = new ProductDetailResponse
            {
                ItemId = productDetail.Data.ItemId.ToString(),
                ProductDetails = prdDetails
            };
            if (result.ProductDetails != null)
            {
                getGetProductDetailLog.ResponseObject = productDetail;
                getGetProductDetailLog.LogInfo();
            }
            return result;
        }

        public async Task<(bool, string)> SyncOnHand(int retailerId, string kvProductSku, long channelId, Guid logId,
            ChannelAuth auth, string itemId, string itemSku, double onHand, byte productType,
            LogObjectMicrosoftExtension loggerExtension,
            string parentProductId = null, Platform platform = null)
        {
            loggerExtension.Action = string.Format(EcommerceTypeException.SyncOnHand, Ecommerce.Lazada);
            loggerExtension.RequestObject = $"Id: {itemId} Sku: {itemSku} OnHand:{onHand}";

            try
            {
                var response = await UpdateOnHandLazada(itemSku, onHand, auth.AccessToken, loggerExtension);
                var message =
                    !string.IsNullOrEmpty(
                        response?.Detail?.FirstOrDefault()?.Message)
                        ? response?.Detail?.FirstOrDefault()?.Message
                        : response?.Message;

                loggerExtension.ResponseObject = response;
                loggerExtension.LogInfo();
                return (response?.Code == "0", (new { response?.RequestId, Message = message }).ToSafeJson());
            }
            catch (Exception ex)
            {
                loggerExtension.LogError(ex);
                throw;
            }
        }

        public async Task<(bool, string)> SyncMultiOnHand(
            int retailerId, long channelId, Guid logId, ChannelAuth auth,
            List<MultiProductItem> productItems, LogObjectMicrosoftExtension loggerExtension,
            byte productType, Platform platform)
        {
            loggerExtension.Action = string.Format(EcommerceTypeException.SyncMultiOnHand, Ecommerce.Lazada);
            loggerExtension.RequestObject = productItems;

            try
            {
                var response = await UpdateMultiOnHandLazada(productItems.Select(x => new ProductUpdate
                {
                    SellerSku = x.ItemSku,
                    Quantity = (int)x.OnHand
                }).ToList(), auth.AccessToken, loggerExtension);
                if (string.IsNullOrEmpty(response?.Code))
                {
                    return (false, (new { response?.RequestId }).ToJson());
                }

                var detail = response.Detail;
                var message = new List<Product>();
                if (detail == null || !detail.Any())
                {
                    message = productItems.Select(p => new Product
                    {
                        ItemId = p.ItemId,
                        ItemSku = p.ItemSku,
                        OnHand = (int)p.OnHand
                    }).ToList();
                    return (true, (new { response?.RequestId, Product = message }.ToJson()));
                }

                var errorSkus = detail.Where(x => !string.IsNullOrEmpty(x.Message)).Select(x => x.SellerSku).ToList();

                var successItems = productItems.Where(x => !errorSkus.Contains(x.ItemSku)).ToList();
                if (successItems.Any())
                {
                    message.AddRange(successItems.Select(p => new Product
                    {
                        ItemId = p.ItemId,
                        ItemSku = p.ItemSku,
                        OnHand = (int)p.OnHand
                    }));
                }
                message.AddRange(detail.Where(p => !string.IsNullOrEmpty(p.Message)).Select(p =>
                {
                    return new Product
                    {
                        ItemId = productItems?.FirstOrDefault(x => x.ItemSku.Equals(p.SellerSku))?.ItemId,
                        ItemSku = p.SellerSku,
                        ErrorMessage = p.Message
                    };
                }));

                loggerExtension.ResponseObject = response;
                loggerExtension.LogInfo();

                return (response.Code == "0", (new { response.RequestId, Product = message }).ToSafeJson());
            }
            catch (Exception ex)
            {
                loggerExtension.LogError(ex);
                throw;
            }
        }

        public async Task<(bool, string)> SyncPrice(int retailerId, string kvProductSku, long channelId, Guid logId,
            ChannelAuth auth, string itemId, string itemSku, decimal? basePrice
            , byte productType, string parentItemId, LogObjectMicrosoftExtension loggerExtension,
            decimal? salePrice = null, DateTime? startSaleDateTime = null, DateTime? endSaleDateTime = null,
            Platform platform = null)
        {
            try
            {
                loggerExtension.Action = string.Format(EcommerceTypeException.SyncPrice, Ecommerce.Lazada);
                loggerExtension.RequestObject = $"Id:{itemId} Sku:{itemSku} BasePrice:{basePrice} SalePrice:{salePrice}";
                var response = await UpdatePriceLazada(logId, itemSku, basePrice, salePrice, loggerExtension, auth.AccessToken,
                    startSaleDateTime, endSaleDateTime);
                var message =
                    !string.IsNullOrEmpty(
                        response.Detail?.FirstOrDefault()?.Message)
                        ? response?.Detail?.FirstOrDefault()?.Message
                        : response.Message;
                loggerExtension.ResponseObject = response;
                loggerExtension.LogInfo();
                return (response.Code == "0", (new { response?.RequestId, Message = message }).ToSafeJson());
            }
            catch (Exception ex)
            {
                loggerExtension.LogError(ex);
                throw;
            }
        }

        public async Task<(bool, string, int?)> SyncMultiPrice(int retailerId, long channelId, Guid logId,
            ChannelAuth auth, List<MultiProductItem> productItems, byte productType,
            LogObjectMicrosoftExtension loggerExtension,
            Platform platform)
        {
            var logSyncMultiPrice =
                loggerExtension.Clone(string.Format(EcommerceTypeException.SyncMultiPrice, Ecommerce.Lazada));
            try
            {
                var response = await UpdateMultiPriceLazada(productItems, logSyncMultiPrice, auth.AccessToken);
                if (response?.HttpStatusCode == (int)HttpStatusCode.BadGateway)
                {
                    return (false, (new { response?.RequestId }).ToJson(), (int)HttpStatusCode.BadGateway);
                }
                if (string.IsNullOrEmpty(response?.Code))
                {
                    return (false, (new { response?.RequestId }).ToJson(), null);
                }
                //KOL-68: Case update price lazada out of range 0.5 - 1.0. Lazada return SellerSku with additional text. eg: ItemSku = 'ProductTest1', SellerSku (fail update response): 'ProductTest1->special_price'
                var detail = response.Detail?.Select(t =>
                {
                    t.SellerSku = t.SellerSku?.Split(new[] { "->" }, StringSplitOptions.None)[0];
                    return t;
                }).ToList();
                var message = new List<Product>();
                if (detail == null || !detail.Any())
                {
                    message = productItems.Select(p => new Product
                    {
                        ItemId = p.ItemId,
                        ItemSku = p.ItemSku,
                        SalePrice = p.SalePrice,
                        BasePrice = p.Price ?? 0

                    }).ToList();
                    return (true, (new { response?.RequestId, Product = message }.ToJson()), null);
                }

                var errorSkus = detail.Where(x => !string.IsNullOrEmpty(x.Message)).Select(x => x.SellerSku).ToList();

                var successItems = productItems.Where(x => !errorSkus.Any(err => err.Contains(x.ItemSku))).ToList();

                if (successItems.Any())
                {
                    message.AddRange(successItems.Select(p => new Product
                    {
                        ItemId = p.ItemId,
                        ItemSku = p.ItemSku,
                        SalePrice = p.SalePrice,
                        BasePrice = p.Price ?? 0
                    }));
                }
                message.AddRange(detail.Where(p => !string.IsNullOrEmpty(p.Message)).Select(p =>
                {
                    return new Product
                    {
                        ItemId = productItems?.FirstOrDefault(x => x.ItemSku.Equals(p.SellerSku))?.ItemId,
                        ItemSku = p.SellerSku,
                        ErrorMessage = p.Message
                    };
                }));
                if (response.Detail != null)
                {
                    logSyncMultiPrice.ResponseObject = response;
                    logSyncMultiPrice.LogInfo();
                }
                return (response.Code == "0", (new { response?.RequestId, Product = message }).ToSafeJson(), null);
            }
            catch (Exception ex)
            {
                logSyncMultiPrice.LogError(ex);
                throw;
            }
        }

        public async Task<(bool, List<CreateOrderRequest>)> GetOrders(ChannelAuth auth, OrderRequest request,
            KvInternalContext context,
            LogObjectMicrosoftExtension logInfo, Platform platform)
        {
            var result = new List<CreateOrderRequest>();
            var logGetOrder = logInfo.Clone("GetOrders");
            logGetOrder.Description = "Begin call LZD GetOrders";
            logGetOrder.LogInfo();
            if (request.LastSync == null)
            {
                List<CreateOrderRequest> ordersNotLastSync;
                var lastSync = DataRequestHelper.GetOrderLastSyncDateTime(request.SyncOrderFormula.GetValueOrDefault());
                // Get order with status unpaid
                (_, ordersNotLastSync) = await GetOrders(string.Empty, lastSync, auth.AccessToken, logGetOrder);
                if (ordersNotLastSync != null && ordersNotLastSync.Any())
                {
                    result.AddRange(ordersNotLastSync);
                }
            }
            else
            {
                List<CreateOrderRequest> orders;
                (_, orders) = await GetOrders(string.Empty, request.LastSync, auth.AccessToken, logGetOrder);
                if (orders != null && orders.Any())
                {
                    result.AddRange(orders);
                }
            }
            logGetOrder.ResponseObject = result;
            logGetOrder.LogInfo(true);
            return (true, result);
        }

        public async Task<OrderDetailGetResponse> GetOrderDetail(int retailerId, long channelId, ChannelAuth auth,
            CreateOrderRequest request, LogObjectMicrosoftExtension logInfo,
            Platform platform, OmniChannelSettingObject settings = null)
        {
            var order = request?.Order?.FromJson<Order>();

            if (string.IsNullOrEmpty(order?.OrderId) && !string.IsNullOrEmpty(request?.OrderId))
            {
                var orderLzd = await GetOrderById(request.OrderId, auth.AccessToken, logInfo);
                order = orderLzd?.Data;
            }

            if (string.IsNullOrEmpty(order?.OrderId))
            {
                return new OrderDetailGetResponse
                {
                    IsSuccess = false,
                    IsRetryOrder = false
                };
            }

            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                var ex = new LazadaException("EmptyAccessToken");
                logInfo.LogError(ex);
                throw ex;
            }
            var orderDetail = await GetOrderDetail(auth.AccessToken, order.OrderId, logInfo);
            if (orderDetail == null)
            {
                return new OrderDetailGetResponse
                {
                    IsSuccess = false,
                    IsRetryOrder = true
                };
            }

            var logicticsGetOrderTrace = await GetOrderTrace(auth, request.OrderId, logInfo);

            var orderItems = orderDetail.Data
                            .Where(p => !p.ShipmentProvider.Equals(LazadaShipmentProvider.SellerDelivery, StringComparison.OrdinalIgnoreCase)
                            && !p.Status.Equals(LazadaStatus.Cancelled, StringComparison.OrdinalIgnoreCase)).ToList();
            if (!orderItems.Any())
            {
                return new OrderDetailGetResponse
                {
                    IsSuccess = false,
                    IsRetryOrder = false
                };
            }

            var add = $"{order.AddressBilling.Address1}, {order.AddressBilling.Address5}, {order.AddressBilling.Address4}, {order.AddressBilling.Address3}";
            add = add.Trim().Trim(',').Replace("  ", " ").Replace(", ,", ",");

            var lsStatus = order.Statuses;
            var lzdStatus = string.Empty;
            if (lsStatus.Any())
            {
                lzdStatus = AggregateLazadaStatuses(lsStatus,
                                                    new List<string>(new[] { LazadaStatus.Cancelled, LazadaStatus.ShippedBackSuccess,
                                                                                LazadaStatus.ReadyToShipPending }),
                                                    new List<string>(new[] { LazadaStatus.Delivered, LazadaStatus.ReadyToShip,
                                                                                LazadaStatus.Shipped, LazadaStatus.ShippedBack }));
            }

            var kvOrder = new KvOrder
            {
                Code = $"{ChanelCodeName.Dhlzd}_{order.OrderId}",
                BranchId = request.BranchId,
                SoldById = request.UserId,
                Ordersn = order.OrderId,
                SaleChannelId = request.SaleChannelId,
                PurchaseDate = order.CreatedAt,
                Description = $"Đơn hàng từ Lazada {order.OrderNumber}",
                IsSyncSuccess = false,
                UsingCod = true,
                RetailerId = retailerId,
                OrderDelivery = new InvoiceDelivery
                {
                    Receiver = order.AddressShipping.FirstName + " " + order.AddressShipping.LastName,
                    ContactNumber = order.AddressShipping.Phone,
                    Address = order.AddressShipping.Address1,
                    WardName = order.AddressShipping.Ward,
                    LocationName = order.AddressShipping.Region,
                    ChannelCity = order.AddressShipping?.City,
                    ChannelWard = order.AddressShipping?.Ward,
                    ChannelState = order.AddressShipping?.Region,
                    UsingPriceCod = true,
                    PartnerDelivery = new PartnerDelivery
                    {
                        Code = "LAZADA",
                        Name = "LAZADA"
                    }
                },
                CustomerPhone = !string.IsNullOrEmpty(order.AddressBilling?.Phone) &&
                                order.AddressBilling.Phone.StartsWith("84")
                  ? $"0{order.AddressBilling.Phone.Substring(2)}"
                  : order.AddressBilling?.Phone,
                CustomerCode = $"KHLZD{order.OrderId}",
                CustomerName = !string.IsNullOrEmpty(order.CustomerFirstName) ? order.CustomerFirstName : order.AddressBilling.FirstName + " " + order.AddressBilling.LastName,
                CustomerAddress = add,
                NewStatus = ConvertToKvOrderStatus(lzdStatus),
                OrderDetails = new List<KvOrderDetail>(),
                ChannelCreatedDate = order.CreatedAt
            };
            var details = orderItems
                .Where(item => !item.Status.Equals(LazadaStatus.Cancelled))
                .GroupBy(x => new { x.Sku, x.ItemPrice }).Select(p => new OrderItem
                {
                    Sku = p.Key.Sku,
                    Name = p.FirstOrDefault(x => x.Sku == p.Key.Sku)?.Name,
                    ItemPrice = p.Key.ItemPrice,
                    Quantity = p.Count(),
                    VoucherSeller = p.FirstOrDefault(x => x.Sku == p.Key.Sku)?.VoucherSeller,
                    SkuId = p.FirstOrDefault().SkuId,
                    ProductId = p.FirstOrDefault().ProductId,
                });
            foreach (var item in details)
            {
                kvOrder.OrderDetails.Add(new KvOrderDetail
                {
                    ProductChannelSku = item.Sku,
                    ProductChannelId = item.SkuId,
                    ParentChannelProductId = item.ProductId,
                    Quantity = item.Quantity,
                    Price = item.ItemPrice,
                    ProductChannelName = item.Name,
                    Discount = item.VoucherSeller ?? null,
                    Uuid = StringHelper.GenerateUUID(),
                });
            }
            return new OrderDetailGetResponse
            {
                IsSuccess = true,
                IsRetryOrder = false,
                Order = kvOrder
            };
        }

        public async Task<(bool, bool, bool, List<KvInvoice>)> GetInvoiceDetail(int retailerId, long channelId,
            Guid logId, ChannelAuth auth, CreateInvoiceRequest request, LogObjectMicrosoftExtension loggerExtension,
            Platform platform, OmniChannelSettingObject settings)
        {

            var order = request.Order?.FromJson<Order>();

            if (string.IsNullOrEmpty(order?.OrderId) && !string.IsNullOrEmpty(request.OrderId))
            {
                var orderLzd = await GetOrderById(request.OrderId, auth.AccessToken, loggerExtension);
                order = orderLzd?.Data;
            }

            if (order == null)
            {
                loggerExtension.LogWarning("order is null");
                return (false, false, false, null);
            }

            if (request.KvOrder == null)
            {
                loggerExtension.LogWarning("KvOrder is null");
                return (false, false, false, null);
            }

            var kvOrder = request.KvOrder;
            if (request.KvDelivery == null)
            {
                loggerExtension.LogWarning("KvDelivery is null");
                return (false, false, false, null);
            }
            var delivery = request.KvDelivery;
            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                var ex = new LazadaException("EmptyAccessToken");
                loggerExtension.LogError(ex);
                throw ex;
            }

            var orderDetail = await GetOrderDetail(auth.AccessToken, request.OrderId, loggerExtension);

            var logicticsGetOrderTrace = await GetOrderTrace(auth, request.OrderId, loggerExtension);

            if (orderDetail == null || orderDetail.Data == null)
            {
                loggerExtension.LogWarning("orderDetail is null");
                return (false, false, false, null);
            }

            if (kvOrder.Status != (int)OrderState.Void && orderDetail.Data.All(t => t.Status == LazadaStatus.Cancelled))
            {
                loggerExtension.LogWarning("Status is invalid");
                return (true, true, false, null);
            }

            foreach (var item in orderDetail.Data)
            {
                if (string.IsNullOrEmpty(item.PackageId) && string.IsNullOrEmpty(item.TrackingCode))
                {
                    item.PackageId = "EmptyPackageId";
                    item.TrackingCode = "EmptyTrackingCode";
                }
            }
            var lst = orderDetail.Data.Where(p => !p.ShipmentProvider.Equals(LazadaShipmentProvider.SellerDelivery, StringComparison.OrdinalIgnoreCase) &&
            !p.Status.Equals(LazadaStatus.Cancelled) &&
            IsSupportedInvoiceDetailStatus(p.Status))
                .GroupBy(g => new { g.Sku, g.ItemPrice, g.Status, g.PackageId, g.TrackingCode, g.VoucherSeller }).Select(
                    t => new
                    {
                        t.Key.Sku,
                        t.Key.ItemPrice,
                        t.Key.TrackingCode,
                        t.Key.Status,
                        t.Key.PackageId,
                        t.Key.VoucherSeller,
                        t.FirstOrDefault()?.Name,
                        t.FirstOrDefault()?.UpdateAt,
                        Quantity = t.Count(),
                        t.FirstOrDefault()?.SkuId,
                        t.FirstOrDefault()?.ProductId
                    }).ToList();
            var batch = lst.GroupBy(g => new { g.PackageId }).Select(p => new
            {
                p.Key.PackageId,
                OrderItems = p.Select(s => new
                {
                    s.Sku,
                    s.ItemPrice,
                    s.Name,
                    s.Quantity,
                    s.Status,
                    s.UpdateAt,
                    s.TrackingCode,
                    s.VoucherSeller,
                    s.SkuId,
                    s.ProductId
                })
            });
            var count = 0;
            var purchaseDate = GetPurchaseDate(settings, logicticsGetOrderTrace, order.CreatedAt);
            List<KvInvoice> invoices = new List<KvInvoice>();
            foreach (var item in batch)
            {
                var detailItems = item.OrderItems.Map(o => o.ConvertTo<OrderItem>());
                var kvInvoice = new KvInvoice
                {
                    Code = $"HDLZD_{request.OrderId}.{count + 1}",
                    RetailerId = kvOrder.RetailerId,
                    OrderId = kvOrder.Id,
                    BranchId = kvOrder.BranchId,
                    PurchaseDate = purchaseDate,
                    CustomerId = kvOrder.CustomerId,
                    UsingCod = true,
                    PriceBookId = kvOrder.Extra?.FromJson<Extra>().PriceBookId?.Id ?? 0,
                    DeliveryDetail = new InvoiceDelivery
                    {
                        Receiver = delivery.Receiver,
                        ContactNumber = delivery.ContactNumber,
                        Address = delivery.Address,
                        UsingPriceCod = true,
                        DeliveryCode = detailItems.FirstOrDefault()?.TrackingCode,
                        WardName = delivery.WardName,
                        LocationName = delivery.LocationName,
                        Status = (byte)DeliveryStatus.Pending,
                        DeliveryBy = delivery.PartnerId
                    },
                    Status = (int)InvoiceState.Pending,
                    InvoiceDetails = new List<InvoiceDetail>(),
                    NewStatus = (int)InvoiceState.Pending,
                    DeliveryCode = detailItems.FirstOrDefault()?.TrackingCode,
                    Description = kvOrder.Description
                };

                count++;

                if (detailItems.All(p => IsSupportedInvoiceDetailStatus(p.Status)))
                {
                    kvInvoice.DeliveryStatus = UpdateDeliveryStatusForInvoice(detailItems.FirstOrDefault()?.Status, request.IsConfirmReturning);
                    kvInvoice.NewStatus = ConvertToKvInvoiceStatus(detailItems.FirstOrDefault()?.Status);
                }
                kvInvoice.DeliveryDetail.LatestStatus = kvInvoice.DeliveryStatus ?? (byte)DeliveryStatus.Pending;

                foreach (var invoiceItem in detailItems)
                {
                    kvInvoice.InvoiceDetails.Add(new InvoiceDetail
                    {
                        ProductChannelSku = invoiceItem.Sku,
                        ProductChannelId = invoiceItem.SkuId,
                        ParentChannelProductId = invoiceItem.ProductId,
                        Quantity = invoiceItem.Quantity,
                        Price = invoiceItem.ItemPrice,
                        ProductChannelName = invoiceItem.Name,
                        Discount = invoiceItem.VoucherSeller ?? null,
                        Uuid = StringHelper.GenerateUUID()
                    });
                }

                invoices.Add(kvInvoice);
            }

            loggerExtension.Action = string.Format(EcommerceTypeException.MappingInvoiceLzdLog, Ecommerce.Lazada);
            loggerExtension.Description = "Mapping success";
            loggerExtension.LogInfo();

            return (true, false, false, invoices);
        }

        public async Task<(byte?, string, string)> GetLogisticsStatus(int retailerId, long channelId,
            LogObjectMicrosoftExtension logInfo,
            string orderId, ChannelAuth auth, bool isGetOrderStatus = false, Platform platform = null)
        {
            (byte?, string, string) result = (null, null, null);
            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                Log.Error(
                    $"{KeywordCommonSearch.CallLazadaError} EmptyAccessToken LazadaGetOrderDetail {nameof(GetLogisticsStatus)} - Context: {new ExceptionContextModel { RetailerId = retailerId, ChannelId = channelId, OrderIds = new List<object> { orderId }, Auth = auth, LogId = logInfo.Id }.ToJson()}");
                throw new OmniException(string.Format(EcommerceTypeException.EmptyAccessToken, Ecommerce.Lazada));
            }
            var orderDetail = await GetOrderDetail(auth.AccessToken, orderId);
            if (orderDetail?.Data == null)
            {
                return result;
            }
            if (orderDetail.Data.All(p => IsSupportedLogisticStatus(p.Status)))
            {
                result.Item1 = UpdateDeliveryStatusForInvoice(orderDetail.Data.FirstOrDefault()?.Status);
            }
            result.Item3 = orderDetail.Data.FirstOrDefault()?.TrackingCode;
            return result;
        }

        public async Task<(byte?, byte?, byte?)> GetOrderStatus(int retailerId, long channelId, Guid logId,
            string orderId, ChannelAuth auth, LogObjectMicrosoftExtension loggerExtension,
            bool isGetDeliveryStatus = false, Platform platform = null, OmniChannelSettingObject settings = null)
        {
            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                var ex = new LazadaException(EcommerceTypeException.EmptyAccessToken);
                loggerExtension.LogError(ex);
                throw ex;

            }
            var orders = await GetOrderById(orderId, auth.AccessToken, loggerExtension);
            loggerExtension.EcommerceRequest = string.Empty;
            if (orders?.Data == null)
            {
                return (null, null, null);
            }
            var lsStatus = orders.Data.Statuses;
            string lzdStatus = string.Empty;
            if (lsStatus.Any())
            {
                lzdStatus = AggregateLazadaStatuses(lsStatus,
                                              new List<string>(new[] { LazadaStatus.Cancelled, LazadaStatus.ShippedBackSuccess,
                                                                        LazadaStatus.ReadyToShipPending }),
                                              new List<string>(new[] { LazadaStatus.Delivered, LazadaStatus.ReadyToShip,
                                                                        LazadaStatus.Shipped, LazadaStatus.ShippedBack,
                                                                        LazadaStatus.ShippedBackFailed, LazadaStatus.FailedDelivery,
                                                                        LazadaStatus.LostBy3PL, LazadaStatus.DamagedBy3PL,
                                                                        LazadaStatus.PackageScrapped }));
            }
            return (ConvertToKvOrderStatus(lzdStatus), null, UpdateDeliveryStatusForInvoice(lzdStatus, settings?.IsConfirmReturning));
        }

        public Task<ShopInfoResponseV2> GetShopStatus(string accessToken)
        {
            throw new NotImplementedException();
        }

        public Task EnableUpdateProduct(string accessToken)
        {
            throw new NotImplementedException();
        }

        public async Task<List<KvInvoice>> GetInvoiceDetailByOrderId(int retailerId, Guid logId, ChannelAuth auth,
            string orderId, long channelId, LogObjectMicrosoftExtension loggerExtension, Platform platform)
        {
            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                var ex = new OmniException("EmptyAccessToken");
                loggerExtension.LogError(ex);
                throw ex;
            }
            var orderDetail = await GetOrderDetail(auth.AccessToken, orderId, loggerExtension);

            if (orderDetail == null) return null;

            var lst = orderDetail.Data.Where(p => p.ShippingType.Equals(LazadaShippingType.Dropshipping, StringComparison.OrdinalIgnoreCase)
                                                  && !p.ShipmentProvider.Equals(LazadaShipmentProvider.SellerDelivery, StringComparison.OrdinalIgnoreCase)
                                                  && IsSupportedInvoiceDetailStatus(p.Status))
                .GroupBy(g => new { g.Sku, g.ItemPrice, g.Status, g.PackageId, g.TrackingCode }).Select(
                    t => new
                    {
                        t.Key.Sku,
                        t.Key.ItemPrice,
                        t.Key.TrackingCode,
                        t.Key.Status,
                        t.Key.PackageId,
                        t.FirstOrDefault()?.Name,
                        t.FirstOrDefault()?.UpdateAt,
                        Quantity = t.Count()
                    }).ToList();

            var batch = lst.GroupBy(g => new { g.PackageId }).Select(p => new
            {
                p.Key.PackageId,
                OrderItems = p.Select(s => new
                {
                    s.Sku,
                    s.ItemPrice,
                    s.Name,
                    s.Quantity,
                    s.Status,
                    s.UpdateAt,
                    s.TrackingCode
                })
            });

            List<KvInvoice> invoices = new List<KvInvoice>();
            var count = 1;
            foreach (var item in batch)
            {
                var detailItems = item.OrderItems.Map(o => o.ConvertTo<OrderItem>());
                var kvInvoice = new KvInvoice
                {
                    Code = $"HDLZD_{orderId}.{count + 1}",
                    InvoiceDetails = new List<InvoiceDetail>(),
                    DeliveryCode = detailItems.FirstOrDefault()?.TrackingCode
                };

                count++;

                foreach (var invoiceItem in detailItems)
                {
                    kvInvoice.InvoiceDetails.Add(new InvoiceDetail
                    {
                        ProductChannelSku = invoiceItem.Sku,
                        ProductChannelId = invoiceItem.SkuId,
                        ParentChannelProductId = invoiceItem.ProductId,
                        Quantity = invoiceItem.Quantity,
                        Price = invoiceItem.ItemPrice,
                        ProductChannelName = invoiceItem.Name
                    });
                }

                invoices.Add(kvInvoice);
            }

            return invoices;
        }

        public Task<List<ShopeeAttribute>> GetAttributeByShopId(int retailerId, long channelId, ChannelAuth auth,
            Guid logId, long categoryId, Platform platform)
        {
            throw new NotImplementedException();
        }

        public Task<ShopeeProductResponse> PushProductToChannel(int retailerId, long channelId, ChannelAuth auth,
            Guid logId, ShopeeProduct req, Platform platform, string kvProductAttributeStr)
        {
            throw new NotImplementedException();
        }

        public Task<CategoriesResp> GetCategories(int retailerId, long channelId, ChannelAuth auth, Guid logId, Platform platform)
        {
            throw new NotImplementedException();
        }

        public Task<ShopeeUploadImgResponse> UploadImg(ChannelAuth auth, int retailerId, long shopId,
            List<Image> images, Guid logId, Platform platform)
        {
            throw new NotImplementedException();
        }

        private async Task<ProductMappingData> GetLazadaDeletedProducts(int offset, DateTime? updateTimeFrom, string accessToken)
        {
            var countRetry = 0;
            var maxRetry = SelfAppConfig.ChannelRetry > SelfAppConfig.LazadaGetEmptyRetry ? SelfAppConfig.ChannelRetry : SelfAppConfig.LazadaGetEmptyRetry;

            var filterParam = "deleted";
            var response = await Policy
                .HandleResult<IRestResponse<LazopResponse>>(msg =>
                {
                    if (countRetry <= SelfAppConfig.ChannelRetry && msg.StatusCode == HttpStatusCode.GatewayTimeout)
                    {
                        countRetry++;
                        return true;
                    }
                    var products = msg.Content?.FromJson<ProductMappingData>();
                    if (countRetry <= SelfAppConfig.LazadaGetEmptyRetry && !string.IsNullOrEmpty(msg.Content) && (products?.Data?.Products == null || !products.Data.Products.Any()))
                    {
                        countRetry++;
                        return true;
                    }
                    return false;
                })
                .WaitAndRetryAsync(maxRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                        (result, timeSpan, retryCount, context) =>
                        {
                            Log.Warn(
                                $"Request failed with {result.Exception}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                        }).ExecuteAsync(async () =>
                        {
                            var param = new Dictionary<string, string> { { "offset", offset.ToString() }, { "limit", SelfAppConfig.LazadaProductPageSize.ToString() } };
                            param.Add("filter", filterParam);
                            if (updateTimeFrom.HasValue)
                            {
                                var updateDateTime = updateTimeFrom.Value.ToLocalTime();
                                param.Add("update_after", updateDateTime.ToString("yyyy-MM-ddTHH:mm:sszzz"));
                            }
                            var client = new LazopClient(SelfAppConfig.LazadaHostApi,
                                SelfAppConfig.LazadaClientAppId,
                                SelfAppConfig.LazadaApiSercetKey);
                            LazopRequest lzdRequest = new LazopRequest();
                            lzdRequest.SetApiName(LazadaEndpoint.GetProducts);
                            lzdRequest.SetHttpMethod("GET");
                            foreach (var pa in param)
                            {
                                lzdRequest.AddApiParameter(pa.Key, pa.Value);
                            }
                            return await client.ExecuteAsync(lzdRequest, accessToken);
                        });
            try
            {
                return response.Content.FromJson<ProductMappingData>();
            }
            catch
            {
                return new ProductMappingData();
            }
        }

        public async Task<(bool, List<FinanceTransaction>)> GetFinanceTransactions(string token, long channelId, string orderSn, DateTime? startTime, DateTime? endTime, LogObjectMicrosoftExtension logInfo, bool isBreakIfMaximum)
        {
            logInfo.Description = string.Format(EcommerceTypeException.GetFinanceTransactions, Ecommerce.Lazada);
            List<string> requestIds = new List<string>();
            var result = new FinanceTransactionReponse
            {
                Data = new List<FinanceTransaction>()
            };
            try
            {
                bool isReachMaximumRequest = false;
                var offset = 0;
                var step = 0;
                var totalItem = SelfAppConfig.LazadaFinanceTransactionPageSize;
                var limitOffset = SelfAppConfig.LazadaFinanceTransactionLimitOffset;

                while (totalItem > 0)
                {
                    var financeTransactionLst = await GetFinanceTransactionWithOffSet(offset, orderSn, startTime, endTime, token);
                    if (!string.IsNullOrEmpty(financeTransactionLst?.Code)
                        && !financeTransactionLst.Code.Equals(LazadaSuccessCode)
                        )
                    {
                        var ex = new LazadaException(financeTransactionLst?.Message);
                        logInfo.LogError(ex);
                        throw ex;
                    }
                    // Nếu offset trả về bằng thì không gọi lên lấy nữa
                    if (financeTransactionLst == null
                        || financeTransactionLst?.Data == null
                        || financeTransactionLst?.Data?.Count < totalItem)
                    {
                        totalItem = 0;
                    }
                    requestIds.Add(financeTransactionLst?.RequestId);
                    if (financeTransactionLst?.Data?.Count > 0)
                    {
                        result.Data.AddRange(financeTransactionLst.Data);
                    }
                    step++;
                    offset = step * SelfAppConfig.LazadaFinanceTransactionPageSize;

                    if (SelfAppConfig.LazadaFinanceTransactionMaximumRequests > 0 && step >= SelfAppConfig.LazadaFinanceTransactionMaximumRequests && isBreakIfMaximum)
                    {
                        isReachMaximumRequest = true;
                        break;
                    }
                    if (offset > limitOffset)
                    {
                        var ex = new LazadaException($"Out of offset config;  offset call : {offset}");
                        logInfo.LogError(ex);
                        throw ex;
                    }

                }
                if (SelfAppConfig.LazadaEnableLogGetTransactionDetail)
                {
                    logInfo.ResponseObject = result.Data?.ToSafeJson();
                    logInfo.LogInfo();
                }
                if (isReachMaximumRequest)
                {
                    return (false, new List<FinanceTransaction>());
                }
                return (true, result.Data);
            }
            catch (Exception ex)
            {
                logInfo.ResponseObject = $"RequestIds: {requestIds.ToSafeJson()} - TotalProduct: {result.Data.ToSafeJson()}";
                logInfo.LogError(ex);
                throw;
            }
        }

        private async Task<FinanceTransactionReponse> GetFinanceTransactionWithOffSet(int offset, string orderSn, DateTime? startTime, DateTime? endTime, string accessToken)
        {
            var param = new Dictionary<string, string> { { "offset", offset.ToString() }, { "limit", SelfAppConfig.LazadaFinanceTransactionPageSize.ToString() } };
            if (startTime.HasValue)
            {
                param.Add("start_time", startTime.Value.ToString("yyyy-MM-dd"));
            }
            if (endTime.HasValue)
            {
                param.Add("end_time", endTime.Value.ToString("yyyy-MM-dd"));
            }
            if (!string.IsNullOrEmpty(orderSn))
            {
                param.Add("trade_order_id", orderSn);
            }
            var client = new LazopClient(SelfAppConfig.LazadaHostApi,
                SelfAppConfig.LazadaClientAppId,
                SelfAppConfig.LazadaApiSercetKey);
            LazopRequest lzdRequest = new LazopRequest();
            lzdRequest.SetApiName(LazadaEndpoint.GetFinanceTransactions);
            lzdRequest.SetHttpMethod("GET");
            foreach (var pa in param)
            {
                lzdRequest.AddApiParameter(pa.Key, pa.Value);
            }
            var resultAPI = await client.ExecuteAsync(lzdRequest, accessToken);
            return resultAPI.Content.FromJson<FinanceTransactionReponse>();
        }

        Task<CustomerResponse> IBaseClient.GetCustomerByOrderId(int retailerId, long channelId, string orderId, ChannelAuth auth, Guid logId, LogObjectMicrosoftExtension loggerExtension, Platform platform)
        {
            throw new NotImplementedException();
        }

        public Task<List<OrderDetail>> GetListOrderDetail(ChannelAuth auth, string[] orderSnList, LogObjectMicrosoftExtension logInfo, Platform platform)
        { throw new NotImplementedException(); }

        public Task<KvOrder> GenListKvOrder(long shopId, int? saleChannelId, int branchId, long userId, int retailerId, OrderDetail shopeeOrder, OmniChannelSettingObject settings, LogObjectMicrosoftExtension logInfo, Platform platform)
        {
            throw new NotImplementedException();
        }

        public List<KvInvoice> GenKvInvoices(CreateInvoiceRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<(bool, List<Transaction>)> GetListTransaction(ChannelAuth auth, DateTime createFrom, DateTime createTo, LogObjectMicrosoftExtension log, Platform platform)
        {
            throw new NotImplementedException();
        }

        public Task<SellerWareHouseV2Response> GetTikiWarehouse(string accessToken)
        {
            throw new NotImplementedException();
        }

        public async Task<GetOrderTraceResponse> GetOrderTrace(ChannelAuth auth, string orderId, LogObjectMicrosoftExtension logInfo)
        {
            var logOrderTrace = logInfo.Clone(string.Format(EcommerceTypeException.GetOrderTrace, Ecommerce.Lazada));
            logOrderTrace.RequestObject = orderId;

            try
            {
                var response = await Policy
                .HandleResult<IRestResponse<LazopResponse>>(msg => msg.StatusCode == HttpStatusCode.GatewayTimeout)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (result, timeSpan, retryCount, context) =>
                    {
                        Log.Warn(
                           $"Request failed with {result.Exception}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                    }).ExecuteAsync(() => LazadaHelper.GetOrderTrace(auth, orderId));
                logOrderTrace.ResponseObject = response?.Content;
                logOrderTrace.LogInfo();
                var resultResponse = response?.Content?.FromJson<GetOrderTraceResponse>();
                if (resultResponse == null) return new GetOrderTraceResponse();
                if (resultResponse.IsErrorResponse() || resultResponse.IsError())
                {
                    var msg = !string.IsNullOrEmpty(response.ErrorMessage) ? response.ErrorMessage : response.Content;
                    logOrderTrace.LogError(new OmniException(msg));
                    return null;
                }
                return resultResponse;
            }
            catch (Exception ex)
            {
                logOrderTrace.LogError(ex);
                return null;
            }
        }


        #region Private method
        private static byte? ConvertToKvOrderStatus(string lazadaStatus)
        {
            switch (lazadaStatus)
            {
                case LazadaStatus.ReadyToShip:
                case LazadaStatus.Shipped:
                case LazadaStatus.Failed:
                case LazadaStatus.Delivered:
                case LazadaStatus.Returned3PL:
                case LazadaStatus.BackToShipper:
                case LazadaStatus.ReadyToShipPending:
                case LazadaStatus.FailedDelivery:
                case LazadaStatus.ShippedBack:
                case LazadaStatus.ShippedBackSuccess:
                case LazadaStatus.ShippedBackFailed:
                case LazadaStatus.LostBy3PL:
                case LazadaStatus.DamagedBy3PL:
                    return (byte)OrderState.Finalized;
                case LazadaStatus.Cancelled:
                    return (byte)OrderState.Void;
            }
            return null;
        }

        private static byte? ConvertToKvInvoiceStatus(string lazadaStatus)
        {
            switch (lazadaStatus)
            {
                case LazadaStatus.Cancelled:
                    return (byte)InvoiceState.Void;
                case LazadaStatus.Pending:
                case LazadaStatus.Repacked:
                case LazadaStatus.Packed:
                    return null;
                default:
                    return (byte)InvoiceState.Pending;
            }
        }

        private byte? UpdateDeliveryStatusForInvoice(string lzdStatus, bool? isConfirmReturning = false)
        {
            switch (lzdStatus)
            {
                case LazadaStatus.Delivered:
                    return (byte)DeliveryStatus.Delivered;
                case LazadaStatus.FailedDelivery:
                case LazadaStatus.ShippedBack:
                case LazadaStatus.PackageScrapped:
                case LazadaStatus.ShippedBackFailed:
                case LazadaStatus.LostBy3PL:
                case LazadaStatus.DamagedBy3PL:
                    return (byte)DeliveryStatus.Returning;
                case LazadaStatus.ShippedBackSuccess:
                    return isConfirmReturning != null && (bool)isConfirmReturning
                        ? (byte)DeliveryStatus.Returning
                        : (byte)DeliveryStatus.Returned;
                case LazadaStatus.ReadyToShip:
                case LazadaStatus.ReadyToShipPending:
                    return (byte)DeliveryStatus.Pending;
                case LazadaStatus.Shipped:
                    return (byte)DeliveryStatus.Delivering;
                case LazadaStatus.Cancelled:
                    return (byte)DeliveryStatus.Void;
            }
            return null;
        }

        private bool IsSupportedInvoiceDetailStatus(string lzdStatus)
        {
            var supportedStatuses = new[] { LazadaStatus.Pending, LazadaStatus.Delivered,
                                                   LazadaStatus.Shipped, LazadaStatus.Failed,
                                                   LazadaStatus.Cancelled, LazadaStatus.ReadyToShip,
                                                   LazadaStatus.Packed, LazadaStatus.Repacked,
                                                   LazadaStatus.ReadyToShipPending, LazadaStatus.FailedDelivery,
                                                   LazadaStatus.ShippedBack, LazadaStatus.ShippedBackFailed,
                                                   LazadaStatus.PackageScrapped, LazadaStatus.ShippedBackSuccess,
                                                   LazadaStatus.LostBy3PL, LazadaStatus.DamagedBy3PL };

            var doesContain = supportedStatuses.Contains(lzdStatus);

            return doesContain;
        }

        private bool IsSupportedLogisticStatus(string lzdStatus)
        {
            var supportedStatuses = new[] { LazadaStatus.Delivered, LazadaStatus.Shipped,
                                                   LazadaStatus.ReadyToShip, LazadaStatus.FailedDelivery,
                                                   LazadaStatus.ShippedBack, LazadaStatus.ShippedBackFailed,
                                                   LazadaStatus.PackageScrapped, LazadaStatus.ShippedBackSuccess,
                                                   LazadaStatus.LostBy3PL, LazadaStatus.DamagedBy3PL };

            var doesContain = supportedStatuses.Contains(lzdStatus);

            return doesContain;
        }

        private string AggregateLazadaStatuses(List<string> statuses, List<string> allEqualStatuses, List<string> anyEqualStatuses)
        {
            var uniqueStatuses = statuses.Where(x => !string.IsNullOrEmpty(x)).Distinct().ToList();
            // KOL-796 sản phẩm bị canncel
            if (uniqueStatuses.Count > 1 && uniqueStatuses.Any(item => item.Equals(LazadaStatus.Cancelled)))
            {
                uniqueStatuses = uniqueStatuses.Where(item => !item.Equals(LazadaStatus.Cancelled)).ToList();
            }
            var aggregation = anyEqualStatuses?.FirstOrDefault(x => uniqueStatuses.Contains(x)) ?? string.Empty;

            if (uniqueStatuses.Count == 1 && aggregation == string.Empty && !statuses.Exists(x => string.IsNullOrEmpty(x)))
            {
                aggregation = allEqualStatuses?.FirstOrDefault(x => x == uniqueStatuses.First()) ?? string.Empty;
            }

            return aggregation;
        }

        private async Task<(List<string>, List<CreateOrderRequest>)> GetOrders(string status, DateTime? lastSync,
            string accessToken, LogObjectMicrosoftExtension logInfo)
        {
            var result = new List<CreateOrderRequest>();
            var pageSize = SelfAppConfig.LazadaOrderPageSize > 0 ? SelfAppConfig.LazadaOrderPageSize : 100;
            var totalItem = pageSize;
            var currentPage = 0;
            var offset = 0;
            List<Order> ordersLzd = new List<Order>();
            List<string> requestIds = new List<string>();
            while (totalItem > 0)
            {
                var response = await GetOrderByStatus(offset, status, lastSync, accessToken, logInfo);
                var lzdOrders = response?.Data;

                if (!string.IsNullOrEmpty(response?.RequestId))
                {
                    requestIds.Add(response.RequestId);
                }

                if (lzdOrders == null || lzdOrders.Count <= 0)
                {
                    totalItem = 0;
                    continue;
                }

                var lstOrder = lzdOrders.Orders.Where(p => !p.Statuses.Any(x => x.Equals(LazadaStatus.Unpaid))).ToList();
                ordersLzd.AddRange(lstOrder);
                totalItem = lzdOrders.Count;
                currentPage++;
                offset = currentPage * pageSize;
            }

            foreach (var item in ordersLzd)
            {
                var lsStatus = item.Statuses;
                string lzdStatus = string.Empty;
                if (lsStatus.Any())
                {
                    lzdStatus = AggregateLazadaStatuses(lsStatus,
                                                        new List<string>(new[] { LazadaStatus.Cancelled, LazadaStatus.Delivered,
                                                                                  LazadaStatus.ReadyToShip, LazadaStatus.ReadyToShipPending,
                                                                                  LazadaStatus.Shipped, LazadaStatus.ShippedBack,
                                                                                  LazadaStatus.ShippedBackSuccess }),
                                                        null);
                }
                result.Add(new CreateOrderRequest
                {
                    OrderId = item.OrderId,
                    Order = item.ToSafeJson(),
                    OrderStatus = lzdStatus,
                    CreateDate = item.CreatedAt,
                    UpdateAt = item.UpdateAt ?? item.CreatedAt
                });
            }
            return (requestIds, result);
        }

        public async Task<RefreshTokenResponse> RefreshAccessToken(string refreshToken, long channelId, Guid logId,
            ChannelAuth auth, Platform platform)
        {
            var refreshAccessTokenLog = new LogObject(Log, logId)
            {
                Action = "LazadaRefreshAccessToken",
                OmniChannelId = channelId,
                RequestObject = $"{refreshToken}"
            };

            var response = await RefreshToken(refreshToken);
            if (response != null && response.Code.Equals(LazadaInvalidRefreshTokenCode))
            {
                refreshAccessTokenLog.Description = "LazadaInvalidRefreshTokenCode";
                refreshAccessTokenLog.ResponseObject = response;
                refreshAccessTokenLog.LogError();

                return new RefreshTokenResponse
                {
                    IsDeactivateChannel = true
                };
            }

            var token = response?.Body?.FromJson<TokenResponse>();
            if (token == null || string.IsNullOrEmpty(token.AccessToken))
            {
                refreshAccessTokenLog.Description = "Token is null";
                refreshAccessTokenLog.ResponseObject = response;
                refreshAccessTokenLog.LogError();
                return null;
            }

            var tokenResponse = new RefreshTokenResponse
            {
                AccessToken = token.AccessToken,
                RefreshExpiresIn = token.RefreshExpiresIn,
                ExpiresIn = token.ExpiresIn,
                RefreshToken = token.RefreshToken,
                SellerId = token.ShopId.ToString()
            };

            refreshAccessTokenLog.Description = "Lazada refresh token successful";
            refreshAccessTokenLog.LogInfo();

            return tokenResponse;
        }
        private async Task<LazopResponse> RefreshToken(string refreshTokenKey)
        {
            var response = await Policy
                .HandleResult<IRestResponse<LazopResponse>>(msg => msg.StatusCode == HttpStatusCode.GatewayTimeout)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                       (result, timeSpan, retryCount, context) =>
                       {
                           Log.Warn(
                               $"Request failed with {result.Exception}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                       }).ExecuteAsync(async () =>
                       {
                           var param = new Dictionary<string, string> { { "refresh_token", refreshTokenKey } };
                           var client = new LazopClient(SelfAppConfig.LazadaHostAuthApi,
                               SelfAppConfig.LazadaClientAppId,
                               SelfAppConfig.LazadaApiSercetKey);
                           LazopRequest lzdRequest = new LazopRequest();
                           lzdRequest.SetApiName(LazadaEndpoint.RefeshToken);
                           lzdRequest.SetHttpMethod("POST");
                           foreach (var pa in param)
                           {
                               lzdRequest.AddApiParameter(pa.Key, pa.Value);
                           }
                           return await client.ExecuteAsync(lzdRequest, null);
                           //return LazadaHelper.PostRequest(SelfAppConfig.LazadaHostAuthApi, LazadaEndpoint.RefeshToken, param, SelfAppConfig.LazadaClientAppId, SelfAppConfig.LazadaApiSercetKey);
                       });
            try
            {
                var result = response.Content.FromJson<LazopResponse>();

                if (string.IsNullOrEmpty(result.Body)) result.Body = result.ConvertTo<TokenResponse>()?.ToJson();

                return result;
            }
            catch
            {
                return new LazopResponse();
            }
        }
        private async Task<ProductMappingData> GetLazadaProducts(int offset, int pageSize, DateTime? updateTimeFrom, string accessToken, Guid logId, bool isGetDeleted = false)
        {
            var isUseRpc = SelfAppConfig.ForwarderApiFeature.Enable &&
                           SelfAppConfig.ForwarderApiFeature.DefaultForwarder;

            var maxRetry = SelfAppConfig.ChannelRetry > SelfAppConfig.LazadaGetEmptyRetry ? SelfAppConfig.ChannelRetry : SelfAppConfig.LazadaGetEmptyRetry;
            var logGetProduct = new LogObject(Log, logId)
            {
                Action = string.Format(EcommerceTypeException.GetListProductPage, Ecommerce.Lazada),
            };

            var waitRetryPolicy = Policy
                .HandleResult<IRestResponse<LazopResponse>>(msg =>
                {
                    if (msg.StatusCode == HttpStatusCode.GatewayTimeout || (msg?.Data?.Code == LazadaApiCallLimit))
                    {
                        var logError = new
                        {
                            Action = "LazadaGetProducts",
                            Request = $"offset: {offset} - limit: {SelfAppConfig.LazadaProductPageSize}",
                            Response = msg?.Content,
                            Exception = new LazadaException(msg.ErrorMessage)
                        };
                        Log.Error(logError.ToJson());
                        return true;
                    }
                    return false;

                })
                .WaitAndRetryAsync(maxRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (result, timeSpan, retryCount, context) =>
                    {
                        if (result.Exception is TimeoutRejectedException)
                        {
                            isUseRpc = SelfAppConfig.ForwarderApiFeature.Enable;
                        }
                        Log.Warn($"Request failed with {result.Exception} LazadaGetProducts. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                    });

            var response = await waitRetryPolicy.ExecuteAsync(async () => await ExecuteGetLazadaProducts(offset, pageSize, updateTimeFrom, accessToken, isUseRpc, isGetDeleted));

            var resultResponse = response?.Content?.FromJson<ProductMappingData>();

            if (resultResponse == null || response.IsErrorResponse() || !resultResponse.Code.Equals(LazadaSuccessCode))
            {
                resultResponse = JsonConvert.DeserializeObject<ProductMappingData>(response?.Content);
                var ex = new OmniException(resultResponse?.Code, resultResponse?.Message);
                logGetProduct.LogError(ex);
                throw ex;
            }
            logGetProduct.Description = $"CallAPILZD isUseRpc : {isUseRpc} ,offset :{offset}, isGetDeleted : {isGetDeleted}";
            logGetProduct.LogInfo();
            return resultResponse;
        }

        private async Task<IRestResponse<LazopResponse>> ExecuteGetLazadaProducts(int offset, int pageSize, DateTime? updateTimeFrom, string accessToken, bool isUseRpc = false, bool isGetDeleted = false)
        {
            var param = new Dictionary<string, string> { { "offset", offset.ToString() }, { "limit", pageSize.ToString() } };
            if (updateTimeFrom.HasValue)
            {
                var updateDateTime = updateTimeFrom.Value.ToLocalTime();
                param.Add("update_after", updateDateTime.ToString("yyyy-MM-ddTHH:mm:sszzz"));
            }
            param.Add("filter", isGetDeleted ? "deleted" : "all");
            var client = new LazopClient(SelfAppConfig.LazadaHostApi,
                SelfAppConfig.LazadaClientAppId,
                SelfAppConfig.LazadaApiSercetKey);
            LazopRequest lzdRequest = new LazopRequest();
            lzdRequest.SetApiName(LazadaEndpoint.GetProducts);
            lzdRequest.SetHttpMethod("GET");
            foreach (var pa in param)
            {
                lzdRequest.AddApiParameter(pa.Key, pa.Value);
            }

            if (!isUseRpc) return await client.ExecuteAsync(lzdRequest, accessToken);

            var requestInfo = client.GetLazapRequestInfo(lzdRequest, accessToken);
            var emptyHeader = new Dictionary<string, string>();
            return await RequestHelper.ForwarderApiCallWork<LazopResponse>(Guid.NewGuid().ToString(), requestInfo.Endpoint, "", "GET", emptyHeader.ToSafeJson(), requestInfo.DicParamns.ToSafeJson());
        }

        private async Task<Models.ProductDetailResponse> GetLazadaProductDetail(long productId, string sku, string accessToken)
        {
            try
            {
                var response = await Policy
                .HandleResult<IRestResponse<LazopResponse>>(msg => msg.StatusCode == HttpStatusCode.GatewayTimeout)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (result, timeSpan, retryCount, context) =>
                    {
                        Log.Warn(
                            $"Request failed with {result.Exception}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                    }).ExecuteAsync(async () =>
                    {
                        var param = new Dictionary<string, string> { { "item_id", productId.ToString() } };
                        if (!string.IsNullOrEmpty(sku))
                        {
                            param.Add("seller_sku", sku);
                        }
                        var client = new LazopClient(SelfAppConfig.LazadaHostApi,
                            SelfAppConfig.LazadaClientAppId,
                            SelfAppConfig.LazadaApiSercetKey);
                        LazopRequest lzdRequest = new LazopRequest();
                        lzdRequest.SetApiName(LazadaEndpoint.GetProductDetail);
                        lzdRequest.SetHttpMethod("GET");
                        foreach (var pa in param)
                        {
                            lzdRequest.AddApiParameter(pa.Key, pa.Value);
                        }
                        return await client.ExecuteAsync(lzdRequest, accessToken);
                    });

                return response.Content.FromJson<Models.ProductDetailResponse>();
            }
            catch
            {
                return new Models.ProductDetailResponse();
            }
        }

        private async Task<LazadaProductResponse> UpdateOnHandLazada(string itemSku, double onHand, string accessToken,
            LogObjectMicrosoftExtension logMicrosoftExtension)
        {
            try
            {
                logMicrosoftExtension.Action = string.Format(EcommerceTypeException.UpdateOnHand, Ecommerce.Lazada);
                var response = await Policy
                .HandleResult<IRestResponse<LazopResponse>>(msg => msg.StatusCode == HttpStatusCode.GatewayTimeout)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (result, timeSpan, retryCount, context) =>
                    {
                        var messageErr =
                            $"Request failed with {result.Exception}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}";
                        var ex = new LazadaException(messageErr);
                        logMicrosoftExtension.ResponseObject = null;
                        logMicrosoftExtension.LogError(ex);
                        if (result.Exception != null)
                        {
                            logMicrosoftExtension.LogError(result.Exception);
                        }

                    }).ExecuteAsync(async () =>
                    {
                        var param = new Dictionary<string, string> {
                            {
                                "payload", new RequestItem
                                {
                                    Products = new LzdProduct
                                    {
                                        Items = new List<ProductUpdate>
                                        {
                                            new ProductUpdate {
                                                SellerSku = itemSku,
                                                Quantity = (int)onHand
                                            }
                                        }
                                    }
                                }.ToXml()
                            }
                        };

                        var client = new LazopClient(SelfAppConfig.LazadaHostApi,
                            SelfAppConfig.LazadaClientAppId,
                            SelfAppConfig.LazadaApiSercetKey);
                        LazopRequest lzdRequest = new LazopRequest();
                        lzdRequest.SetApiName(LazadaEndpoint.UpdatePriceAndStock);
                        lzdRequest.SetHttpMethod("POST");
                        foreach (var pa in param)
                        {
                            lzdRequest.AddApiParameter(pa.Key, pa.Value);
                        }

                        var requestObject =
                            $"url: {SelfAppConfig.LazadaHostApi}/{LazadaEndpoint.UpdatePriceAndStock} - " +
                            $"request: {lzdRequest.GetParameters().ToJson()}";
                        logMicrosoftExtension.RequestObject = requestObject;
                        logMicrosoftExtension.LogInfo();

                        var date = DateTime.UtcNow;
                        return await client.ExecuteAsync(lzdRequest, accessToken, date);
                    });

                var resultResponse = response.Content.FromJson<LazadaProductResponse>();
                if (resultResponse.Code != "0")
                {
                    var msg = $"request_id: {resultResponse.RequestId} - code: {resultResponse.Code} - type: {resultResponse.Type} - msg: {resultResponse.Message}";
                    var ex = new LazadaException(msg);
                    logMicrosoftExtension.LogError(ex);
                    return resultResponse;
                }

                logMicrosoftExtension.ResponseObject = resultResponse;
                if (response.IsErrorResponse())
                {
                    var ex = new LazadaException(resultResponse.Message);
                    logMicrosoftExtension.LogError(ex);
                }
                else
                {
                    logMicrosoftExtension.LogInfo();
                }
                return resultResponse;
            }
            catch (Exception ex)
            {

                logMicrosoftExtension.LogError(ex);
                return new LazadaProductResponse();
            }
        }

        private async Task<LazadaProductResponse> UpdateMultiOnHandLazada(List<ProductUpdate> items, string accessToken, LogObjectMicrosoftExtension logMicrosoftExtension)
        {
            try
            {
                var isUseGrpc = SelfAppConfig.ForwarderApiFeature.Enable &&
                            SelfAppConfig.ForwarderApiFeature.DefaultForwarder;

                logMicrosoftExtension.EcommerceRequest = Ecommerce.Lazada;
                var response = await Policy
                    .HandleResult<IRestResponse<LazopResponse>>(msg => msg.StatusCode == HttpStatusCode.GatewayTimeout)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                        (result, timeSpan, retryCount, context) =>
                        {
                            if (retryCount > 1)
                            {
                                logMicrosoftExtension.ResponseObject = null;
                            }

                            if (result.Exception != null)
                            {
                                logMicrosoftExtension.LogError(result.Exception);
                            }
                            else if (result.Result?.ErrorException != null)
                            {
                                logMicrosoftExtension.LogError(result.Result?.ErrorException);
                            }
                            else
                            {
                                var messageErr =
                                    $"Request failed with UpdateMultiOnHandLazada. Waiting {timeSpan} before next retry. Retry attempt {retryCount}";
                                var ex = new LazadaException(messageErr);
                                logMicrosoftExtension.LogError(ex);
                            }

                        }).ExecuteAsync(async () =>
                        {

                            var param = new Dictionary<string, string> { { "payload", new RequestItem { Products = new LzdProduct { Items = items } }.ToXml() } };
                            var client = new LazopClient(SelfAppConfig.LazadaHostApi,
                                SelfAppConfig.LazadaClientAppId,
                                SelfAppConfig.LazadaApiSercetKey);
                            LazopRequest lzdRequest = new LazopRequest();
                            lzdRequest.SetApiName(LazadaEndpoint.UpdatePriceAndStock);
                            lzdRequest.SetHttpMethod("POST");
                            foreach (var pa in param)
                            {
                                lzdRequest.AddApiParameter(pa.Key, pa.Value);
                            }

                            var requestObject =
                                $"url: {SelfAppConfig.LazadaHostApi}/{LazadaEndpoint.UpdatePriceAndStock} - " +
                                $"request: {lzdRequest.GetParameters().ToJson()}";
                            logMicrosoftExtension.RequestObject = requestObject;
                            logMicrosoftExtension.LogInfo();

                            return await ExecuteOrGrpc(lzdRequest, accessToken, isUseGrpc);
                        });
                var resultResponse = response.Content.FromJson<LazadaProductResponse>();
                logMicrosoftExtension.EcommerceRequest = Ecommerce.Lazada;

                if (resultResponse == null)
                {
                    var msg = $"Call UpdatePriceAndStock received from LAZADA failed with content : {response.Content} on LogId : {logMicrosoftExtension.Id}";
                    var ex = new LazadaException(msg);
                    logMicrosoftExtension.ResponseObject = new { response.StatusCode, response.ErrorMessage, response.ErrorException?.Message, response.Content };
                    logMicrosoftExtension.LogError(ex);
                    throw ex;
                }

                if (resultResponse.Code != "0")
                {
                    var msg = $"request_id: {resultResponse.RequestId} - code: {resultResponse.Code} - type: {resultResponse.Type} - msg: {resultResponse.Message}";
                    var ex = new LazadaException(msg);
                    logMicrosoftExtension.LogError(ex);
                    return resultResponse;
                }

                if (response.IsErrorResponse())
                {
                    var msg = !string.IsNullOrEmpty(response.ErrorMessage) ? response.ErrorMessage : response.Content;
                    var ex = new LazadaException(msg);
                    logMicrosoftExtension.LogError(ex);
                    throw ex;
                }

                logMicrosoftExtension.ResponseObject = resultResponse;
                logMicrosoftExtension.LogInfo();

                return resultResponse;
            }
            catch (Exception ex)
            {
                logMicrosoftExtension.LogError(ex);
                return new LazadaProductResponse();
            }
        }
        private async Task<LazadaProductResponse> UpdateMultiPriceLazada(List<MultiProductItem> items, LogObjectMicrosoftExtension loggerExtension, string accessToken)
        {
            var logSyncMultiPrice =
                loggerExtension.Clone(string.Format(EcommerceTypeException.SyncMultiPriceSyncBatch, Ecommerce.Lazada));
            try
            {
                var response = await Policy
                .HandleResult<IRestResponse<LazopResponse>>(msg => msg.StatusCode == HttpStatusCode.GatewayTimeout)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (result, timeSpan, retryCount, context) =>
                    {
                        if (result.Exception != null)
                        {
                            logSyncMultiPrice.LogError(result.Exception);
                        }
                        else if (result.Result?.ErrorException != null)
                        {
                            logSyncMultiPrice.LogError(result.Result?.ErrorException);
                        }
                        else
                        {
                            var messageErr =
                                $"Request failed with UpdatePriceLazada. Waiting {timeSpan} before next retry. Retry attempt {retryCount}";
                            var ex = new LazadaException(messageErr);
                            logSyncMultiPrice.LogError(ex);
                        }
                    }).ExecuteAsync(async () =>
                    {
                        var request = new RequestItem
                        {
                            Products = new LzdProduct
                            {
                                Items = new List<ProductUpdate>()
                            }
                        };
                        foreach (var item in items)
                        {
                            var product = new ProductUpdate
                            {
                                SellerSku = item.ItemSku,
                                Price = item.Price
                            };
                            if (item.SalePrice > 0 && item.StartSaleDate != null && item.StartSaleDate != default(DateTime) && item.EndSaleDate != null && item.EndSaleDate != default(DateTime))
                            {
                                product.SalePrice = item.SalePrice;
                                product.SaleStartDate = item.StartSaleDate.Value.ToString("yyyy-MM-dd HH:mm");
                                product.SaleEndDate = item.EndSaleDate.Value.ToString("yyyy-MM-dd HH:mm");
                            }
                            request.Products.Items.Add(product);
                        }
                        var param = new Dictionary<string, string>
                        {
                            {
                                "payload", request.ToXml()
                            }
                        };
                        var client = new LazopClient(SelfAppConfig.LazadaHostApi,
                            SelfAppConfig.LazadaClientAppId,
                            SelfAppConfig.LazadaApiSercetKey);
                        LazopRequest lzdRequest = new LazopRequest();
                        lzdRequest.SetApiName(LazadaEndpoint.UpdatePriceAndStock);
                        lzdRequest.SetHttpMethod("POST");
                        foreach (var pa in param)
                        {
                            lzdRequest.AddApiParameter(pa.Key, pa.Value);
                        }

                        var requestObject =
                            $"url: {SelfAppConfig.LazadaHostApi}/{LazadaEndpoint.UpdatePriceAndStock} - " +
                            $"params: {lzdRequest.GetParameters().ToJson()}";
                        logSyncMultiPrice.RequestObject = requestObject;

                        return await client.ExecuteAsync(lzdRequest, accessToken, DateTime.UtcNow);
                    });
                var resultResponse = response.Content.FromJson<LazadaProductResponse>();
                if (resultResponse.Code != "0")
                {
                    var msg = $"request_id: {resultResponse.RequestId} - code: {resultResponse.Code} - type: {resultResponse.Type} - msg: {resultResponse.Message}";
                    var ex = new LazadaException(msg);
                    logSyncMultiPrice.LogError(ex);
                    return resultResponse;
                }

                if (response.IsErrorResponse())
                {
                    var msg = !string.IsNullOrEmpty(response.ErrorMessage) ? response.ErrorMessage : response.Content;
                    var ex = new LazadaException(msg);
                    logSyncMultiPrice.LogError(ex);
                    throw ex;
                }

                logSyncMultiPrice.ResponseObject = resultResponse;
                logSyncMultiPrice.LogInfo();

                resultResponse.HttpStatusCode = (int)response.StatusCode;
                return resultResponse;
            }
            catch (Exception ex)
            {
                logSyncMultiPrice.LogError(ex);
                throw;
            }
        }
        private async Task<LazadaProductResponse> UpdatePriceLazada(Guid logId, string itemSku, decimal? basePrice, decimal? salePrice, LogObjectMicrosoftExtension loggerExtension, string accessToken, DateTime? startSaleTime = null, DateTime? endSaleTime = null)
        {
            try
            {
                loggerExtension.EcommerceRequest = Ecommerce.Lazada;
                var response = await Policy
                        .HandleResult<IRestResponse<LazopResponse>>(msg => msg.StatusCode == HttpStatusCode.GatewayTimeout)
                        .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                            (result, timeSpan, retryCount, context) =>
                            {
                                if (retryCount > 1)
                                {
                                    loggerExtension.ResponseObject = null;
                                }

                                if (result.Exception != null)
                                {
                                    loggerExtension.LogError(result.Exception);
                                }
                                else if (result.Result?.ErrorException != null)
                                {
                                    loggerExtension.LogError(result.Result?.ErrorException);
                                }
                                else
                                {
                                    var messageErr =
                                        $"Request failed with UpdatePriceLazada. Waiting {timeSpan} before next retry. Retry attempt {retryCount}";
                                    var ex = new LazadaException(messageErr);
                                    loggerExtension.LogError(ex);
                                }

                            }).ExecuteAsync(async () =>
                            {
                                var request = new RequestItem
                                {
                                    Products = new LzdProduct { Items = new List<ProductUpdate> { new ProductUpdate { SellerSku = itemSku, Price = basePrice } } }
                                };
                                foreach (var item in request.Products.Items)
                                {
                                    if (salePrice > 0 && startSaleTime != null && startSaleTime != default(DateTime) && endSaleTime != null && endSaleTime != default(DateTime))
                                    {
                                        item.SalePrice = salePrice;
                                        item.SaleStartDate = startSaleTime.Value.ToString("yyyy-MM-dd HH:mm");
                                        item.SaleEndDate = endSaleTime.Value.ToString("yyyy-MM-dd HH:mm");
                                    }
                                }
                                var param = new Dictionary<string, string>
                            {
                                {
                                    "payload", request.ToXml()
                                }
                            };

                                var client = new LazopClient(SelfAppConfig.LazadaHostApi,
                                    SelfAppConfig.LazadaClientAppId,
                                    SelfAppConfig.LazadaApiSercetKey);
                                LazopRequest lzdRequest = new LazopRequest();
                                lzdRequest.SetApiName(LazadaEndpoint.UpdatePriceAndStock);
                                lzdRequest.SetHttpMethod("POST");
                                foreach (var pa in param)
                                {
                                    lzdRequest.AddApiParameter(pa.Key, pa.Value);
                                }

                                var requestObject =
                                    $"url: {SelfAppConfig.LazadaHostApi}/{LazadaEndpoint.UpdatePriceAndStock} - " +
                                    $"request: {lzdRequest.GetParameters().ToJson()}";

                                loggerExtension.RequestObject = requestObject;
                                loggerExtension.LogInfo();
                                loggerExtension.ResetTimer();
                                var date = DateTime.UtcNow;
                                var reval = await client.ExecuteAsync(lzdRequest, accessToken, date);
                                return reval;
                            });
                loggerExtension.EcommerceRequest = string.Empty;
                var resultResponse = response.Content.FromJson<LazadaProductResponse>();
                if (resultResponse.Code != "0")
                {
                    var msg = $"request_id: {resultResponse.RequestId} - code: {resultResponse.Code} - type: {resultResponse.Type} - msg: {resultResponse.Message}";
                    var ex = new LazadaException(msg);
                    loggerExtension.LogError(ex);
                    return resultResponse;
                }

                if (response.IsErrorResponse())
                {
                    var msg = !string.IsNullOrEmpty(response.ErrorMessage) ? response.ErrorMessage : response.Content;
                    var ex = new LazadaException(msg);
                    loggerExtension.LogError(ex);
                    throw ex;
                }

                loggerExtension.ResponseObject = resultResponse;
                loggerExtension.LogInfo();

                return resultResponse;
            }
            catch (Exception ex)
            {
                loggerExtension.LogError(ex);
                throw;
            }
        }

        private async Task<OrderResponse> GetOrderByStatus(int offset, string status, DateTime? lastSync, string accessToken, LogObjectMicrosoftExtension logInfo)
        {
            var getOrderByStatus = logInfo.Clone("GetOrderByStatus");
            try
            {
                var waitRetryPolicy = Policy
                 .HandleResult<IRestResponse<LazopResponse>>(msg =>
                 {
                     if (msg.StatusCode == HttpStatusCode.GatewayTimeout)
                     {
                         logInfo.ResponseObject = $"offset: {offset} - limit: {SelfAppConfig.LazadaOrderPageSize}";
                         var ex = new LazadaException(msg.ErrorMessage);
                         logInfo.LogError(ex);
                         return true;
                     }
                     return false;
                 })
                 .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (result, timeSpan, retryCount, context) =>
                    {
                        if (result.Result?.Data?.Code != "0")
                        {
                            var responseLazada = result.Result?.Data;
                            var msgError = $"WaitAndRetryAsync Request_id: {responseLazada?.RequestId} - status code: {result.Result?.StatusCode} - code: {responseLazada?.Code} - type: {responseLazada?.Type} - msg: {responseLazada?.Message}";
                            logInfo.LogWarning(msgError);
                        }
                        else if (result?.Exception != null)
                        {
                            logInfo.LogError(result?.Exception);
                        }
                        else if (result.Result?.ErrorException != null)
                        {
                            logInfo.LogError(result.Result.ErrorException);
                        }
                        else
                        {
                            var ex = new LazadaException(
                                $"Request failed with {result.Exception}. GetOrderByStatusLZD Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                            logInfo.LogError(ex);
                        }
                    });
                var response = await waitRetryPolicy.ExecuteAsync(async () => await ExecuteGetOrderByStatus(offset, status, lastSync, accessToken));
                var resultResponse = response.Content.FromJson<OrderResponse>();
                if (resultResponse == null)
                {
                    var ex = new LazadaException(response.Content);
                    logInfo.LogError(ex);
                }
                if (resultResponse?.Code != "0")
                {
                    var msg = $"request_id: {resultResponse?.RequestId} - code: {resultResponse?.Code} - type: {resultResponse?.Type} - msg: {resultResponse?.Message}";
                    var ex = new LazadaException(msg);
                    logInfo.LogError(ex);
                    return resultResponse;
                }

                if (response.IsErrorResponse())
                {
                    var ex = new LazadaException(response.Content);
                    logInfo.LogError(ex);
                    throw ex;
                }

                getOrderByStatus.ResponseObject = resultResponse?.Data?.Orders?.Select(x=> new { x.OrderId, x.Statuses });
                getOrderByStatus.LogInfo(true);

                return resultResponse;
            }
            catch (Exception ex)
            {
                logInfo.LogError(ex);
                throw;
            }
        }

        private async Task<IRestResponse<LazopResponse>> ExecuteGetOrderByStatus(int offset, string status, DateTime? lastSync, string accessToken, bool isCallGrpcRetry = false)
        {
            var param = new Dictionary<string, string>
            {
                {"filter", "all"}, {"offset", offset.ToString()},
                {"limit", SelfAppConfig.LazadaOrderPageSize.ToString()}
            };
            if (lastSync.HasValue)
            {
                param.Add("update_after", lastSync.Value.ToLocalTime().ToString("yyyy-MM-ddTHH:mm:sszzz"));
            }

            if (!string.IsNullOrEmpty(status))
            {
                param.Add("status", status);
            }

            var client = new LazopClient(SelfAppConfig.LazadaHostApi,
                SelfAppConfig.LazadaClientAppId,
                SelfAppConfig.LazadaApiSercetKey);
            LazopRequest request = new LazopRequest();
            request.SetApiName(LazadaEndpoint.GetOrders);
            request.SetHttpMethod("GET");
            foreach (var pa in param)
            {
                request.AddApiParameter(pa.Key, pa.Value);
            }

            IRestResponse<LazopResponse> result;
            if (isCallGrpcRetry)
            {
                var requestInfo = client.GetLazapRequestInfo(request, accessToken);
                var emptyHeader = new Dictionary<string, string>();
                result = await RequestHelper.ForwarderApiCallWork<LazopResponse>(Guid.NewGuid().ToString(), requestInfo.Endpoint, "", "GET", emptyHeader.ToSafeJson(), requestInfo.DicParamns.ToSafeJson());
            }
            else
            {
                result = await client.ExecuteAsync(request, accessToken);
            }
            return result;
        }
        private async Task<OrderIdResponse> GetOrderById(string orderId, string accessToken, LogObjectMicrosoftExtension logInfo)
        {
            try
            {
                var response = await Policy
                .HandleResult<IRestResponse<LazopResponse>>(msg => msg.StatusCode != HttpStatusCode.OK)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (result, timeSpan, retryCount, context) =>
                    {
                        var ex = new LazadaException(
                            $"Request failed with {result.Exception}.GetOrderByIdLZD Waiting {timeSpan} before next retry. Retry attempt {retryCount}");

                        logInfo.LogError(ex);

                    }).ExecuteAsync(async () =>
                    {
                        var param = new Dictionary<string, string> { { "order_id", orderId } };

                        var client = new LazopClient(SelfAppConfig.LazadaHostApi,
                            SelfAppConfig.LazadaClientAppId,
                            SelfAppConfig.LazadaApiSercetKey);

                        LazopRequest request = new LazopRequest();
                        request.SetApiName(LazadaEndpoint.GetOrderId);
                        request.SetHttpMethod("GET");
                        foreach (var pa in param)
                        {
                            request.AddApiParameter(pa.Key, pa.Value);
                        }

                        var requestObject =
                            $"url: {SelfAppConfig.LazadaHostApi}/{LazadaEndpoint.GetOrderId} - " +
                            $"request: {request.GetParameters().ToSafeJson()}";
                        logInfo.RequestObject = requestObject;

                        return await client.ExecuteAsync(request, accessToken);
                    });

                var resultResponse = response?.Content?.FromJson<OrderIdResponse>();

                if (resultResponse?.Code != "0")
                {
                    var msg = $"request_id: {resultResponse?.RequestId} - code: {resultResponse?.Code} - type: {resultResponse?.Type} - msg: {resultResponse?.Message}";
                    var ex = new LazadaException(msg);
                    logInfo.LogError(ex, true);
                    return resultResponse;
                }

                if (response.IsErrorResponse())
                {
                    var msg = !string.IsNullOrEmpty(response.ErrorMessage) ? response.ErrorMessage : response.Content;
                    var ex = new LazadaException(msg);
                    logInfo.LogError(ex, true);
                    throw ex;
                }

                return resultResponse;
            }
            catch (Exception ex)
            {
                logInfo.LogError(ex);
                return new OrderIdResponse();
            }
        }

        private async Task<OrderItemResponse> GetOrderDetail(string accessToken, string orderId, LogObjectMicrosoftExtension logInfo)
        {
            var logGetOrderDetail = logInfo.Clone("GetOrderDetail");
            try
            {
                var response = await Policy
                .HandleResult<IRestResponse<LazopResponse>>(msg => msg.StatusCode != HttpStatusCode.OK)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                (result, timeSpan, retryCount, context) =>
                {
                    var ex = new Exception(
                        $"Request failed with {result.Exception}. GetOrderDetailLZD Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                    logGetOrderDetail.LogError(ex);
                }).ExecuteAsync(async () =>
                {
                    var param = new Dictionary<string, string> { { "order_id", orderId } };
                    var client = new LazopClient(SelfAppConfig.LazadaHostApi,
                        SelfAppConfig.LazadaClientAppId,
                        SelfAppConfig.LazadaApiSercetKey);

                    LazopRequest request = new LazopRequest();
                    request.SetApiName(LazadaEndpoint.GetOrderItem);
                    request.SetHttpMethod("GET");
                    foreach (var pa in param)
                    {
                        request.AddApiParameter(pa.Key, pa.Value);
                    }
                    return await client.ExecuteAsync(request, accessToken);
                });
                var resultResponse = response?.Content?.FromJson<OrderItemResponse>();

                if (resultResponse?.Code != "0")
                {
                    resultResponse = JsonConvert.DeserializeObject<OrderItemResponse>(response?.Content);
                    var msg = $"Request_id: {resultResponse?.RequestId} - code: {resultResponse?.Code} - type: {resultResponse?.Type} - msg: {resultResponse?.Message}";
                    var ex = new LazadaException(msg);
                    logGetOrderDetail.LogError(ex);
                    return resultResponse;
                }

                if (response.IsErrorResponse())
                {
                    var msg = !string.IsNullOrEmpty(response.ErrorMessage) ? response.ErrorMessage : response.Content;
                    var ex = new LazadaException(msg);
                    logGetOrderDetail.LogError(ex);
                    throw ex;
                }
                logGetOrderDetail.ResponseObject = resultResponse;
                logGetOrderDetail.LogInfo(true);
                return resultResponse;
            }
            catch (Exception ex)
            {
                logGetOrderDetail.LogError(ex);
                return new OrderItemResponse();
            }
        }

        /// <summary>
        /// Get detail orders and response to API, when use loggerMicrosoftExtension will remove this method
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        private async Task<OrderItemResponse> GetOrderDetail(string accessToken, string orderId)
        {
            var response = await Policy
                .HandleResult<IRestResponse<LazopResponse>>(msg => msg.StatusCode != HttpStatusCode.OK)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (result, timeSpan, retryCount, context) =>
                    {
                        Log.Warn(
                            $"Request failed with {result.Exception}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                    }).ExecuteAsync(async () =>
                    {
                        var param = new Dictionary<string, string> { { "order_id", orderId } };
                        var client = new LazopClient(SelfAppConfig.LazadaHostApi,
                            SelfAppConfig.LazadaClientAppId,
                            SelfAppConfig.LazadaApiSercetKey);
                        LazopRequest request = new LazopRequest();
                        request.SetApiName(LazadaEndpoint.GetOrderItem);
                        request.SetHttpMethod("GET");
                        foreach (var pa in param)
                        {
                            request.AddApiParameter(pa.Key, pa.Value);
                        }
                        return await client.ExecuteAsync(request, accessToken);
                    });
            try
            {
                return response.Content.FromJson<OrderItemResponse>();
            }
            catch
            {
                return new OrderItemResponse();
            }
        }

        private async Task<IRestResponse<LazopResponse>> ExecuteOrGrpc(LazopRequest request, string accessToken, bool isCallGrpc)
        {
            var date = DateTime.UtcNow;
            var client = new LazopClient(SelfAppConfig.LazadaHostApi,
                SelfAppConfig.LazadaClientAppId, SelfAppConfig.LazadaApiSercetKey);
            if (isCallGrpc)
            {
                var requestInfo = client.GetLazapRequestInfo(request, accessToken);
                var emptyHeader = new Dictionary<string, string>();
                var result = await RequestHelper.ForwarderApiCallWork<LazopResponse>(Guid.NewGuid().ToString(),
                    requestInfo.Endpoint, "", request.GetHttpMethod(), emptyHeader.ToSafeJson(), requestInfo.DicParamns.ToSafeJson());
                return result;
            }

            return await client.ExecuteAsync(request, accessToken, date);
        }

        public Task<List<Logistics>> GetLogisticsByShopId(int retailerId, long channelId, ChannelAuth auth, Guid logId, Platform platform)
        {
            throw new NotImplementedException();
        }

        private DateTime GetPurchaseDate(OmniChannelSettingObject settings,
           GetOrderTraceResponse orderTrace, DateTime orderCreateTime)
        {
            if (settings?.SyncOrderSaleTimeType == (int)SyncOrderSaleTimeType.TimeCreateOrder)
                return orderCreateTime;

            if (orderTrace == null)
                return DateTime.Now;

            var logicticsInfo = orderTrace?.Result?.Module?.FirstOrDefault()?.
                PackageDetailInfoList?.FirstOrDefault()?.
                LogisticDetailInfoList?.FirstOrDefault(x => x.DetailType == LazadaDetailType.ReadyTo);

            if (logicticsInfo == null)
                return DateTime.Now;

            return logicticsInfo.EventTime.FromUnixTimeMs().ToLocalTime();
        }
        #endregion
    }
}

﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Helper;
using Demo.OmniChannel.ChannelClient.Impls.NewBase;
using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.RequestDTO;
using Demo.OmniChannel.ChannelClient.RequestDTO.Shopee;
using Demo.OmniChannel.ChannelClient.RequestDTO.Tiktok;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Tiktok;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Impls;
using Demo.OmniChannel.ShareKernel.Dtos;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.ShareKernel.Models;
using Demo.OmniChannel.Utilities;
using Newtonsoft.Json;
using Polly;
using RestSharp;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using ChannelProductType = Demo.OmniChannel.ShareKernel.Common.ChannelProductType;
using Image = Demo.OmniChannel.ShareKernel.Dtos.Image;
using Logistics = Demo.OmniChannel.ChannelClient.Models.Logistics;
using OmniException = Demo.OmniChannel.Sdk.OmniExceptions.OmniException;
using Platform = Demo.OmniChannel.ChannelClient.Models.Platform;
using ProductDetailResponse = Demo.OmniChannel.ChannelClient.RequestDTO.ProductDetailResponse;
using SelfAppConfig = Demo.OmniChannel.ChannelClient.Common.SelfAppConfig;

namespace Demo.OmniChannel.ChannelClient.Impls
{
    public class TikTokClient : ITiktokClient
    {
        private const string RequestIsLimitedStatusCode = "107000";
        private const string TooManyRequestsStatusCode = "36009002";
        private const string RequestIsLimitedMessage = "request is limited";
        private readonly IAppSettings _appSettings;
        private ILog Log => LogManager.GetLogger(typeof(LazadaClient));
        private readonly IntegrationInternalClient _integrationInternalClient;
        private readonly TiktokBusinessImplimentBehaviorV2 _tiktokBusiness;

        public TikTokClient(IAppSettings settings)
        {
            _appSettings = settings;
            _integrationInternalClient = new IntegrationInternalClient(settings);
            _tiktokBusiness = new TiktokBusinessImplimentBehaviorV2(settings);
            _ = new SelfAppConfig(settings);
        }

        public async Task<Models.TokenResponse> CreateToken(string code, ChannelAuth auth, Platform platform)
        {
            var timeout = SelfAppConfig.ForwarderApiFeature.DefaultForwarder;
            var response = await Policy
                .HandleResult<IRestResponse<TiktokBaseResponse<TiktokTokenResponse>>>(msg => msg.StatusCode == HttpStatusCode.GatewayTimeout)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry,
                    i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (result, timeSpan, retryCount) =>
                    {
                        Log.Warn(
                            $"GetAccessTokenWaitAndRetryAsync with {result.Result.StatusCode}. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount}");
                    }).ExecuteAsync(() =>
                    TikTokHelper.GetAccessToken(code, platform, timeout));

            if (response?.Data == null) return null;
            if (response.Data.IsError()) throw new OmniException(response.Data.Message);
            return new Models.TokenResponse
            {
                AccessToken = response.Data.Response.AccessToken,
                RefreshToken = response.Data.Response.RefreshToken,
                ExpiresIn = (int)(DateHelper.UnixTimeStampToDateTime(response.Data.Response.AccessTokenExpireIn)
                - DateTime.Now).TotalSeconds,
                RefreshExpiresIn = (int)(DateHelper.UnixTimeStampToDateTime(response.Data.Response.RefreshTokenExpireIn)
                - DateTime.Now).TotalSeconds,
            };
        }

        public async Task<RefreshTokenResponse> RefreshAccessToken(string refreshToken, long channelId, Guid logId,
            ChannelAuth auth, Platform platform)
        {
            var refreshAccessTokenLog = new LogObject(Log, logId)
            {
                Action = "TiktokRefreshAccessToken",
                OmniChannelId = channelId,
                RequestObject = $"{refreshToken}"
            };
            var response = await Policy.HandleResult<IRestResponse<TiktokBaseResponse<TiktokTokenResponse>>>(msg => msg.StatusCode == HttpStatusCode.GatewayTimeout)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (result, timeSpan, retryCount) =>
                    {
                        Log.Warn(
                            $"GetShopInfoWaitAndRetryAsync with {result.Result.StatusCode}. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount}");
                    }).ExecuteAsync(() => TikTokHelper.RefreshAccessToken(refreshToken, platform));
            if (response?.Data == null) return null;
            if (response.Data.IsError() && (response.Data.Code == TiktokErrorCode.RefreshTokenExpired ||
                response.Data.Code == TiktokErrorCode.AuthorizationIsCancelled))
            {
                refreshAccessTokenLog.Description = "DeActive channel because error auth";
                refreshAccessTokenLog.ResponseObject = new { Status = response.StatusCode, response.Content };
                refreshAccessTokenLog.LogWarning();

                return new RefreshTokenResponse
                {
                    IsDeactivateChannel = true
                };
            }
            refreshAccessTokenLog.ResponseObject = new { Status = response?.StatusCode, response?.Content };
            if (response?.Data == null)
            {
                refreshAccessTokenLog.LogWarning();
                return null;
            }
            refreshAccessTokenLog.LogInfo();
            return new RefreshTokenResponse
            {
                AccessToken = response.Data.Response.AccessToken,
                RefreshToken = response.Data.Response.RefreshToken,
                ExpiresIn = (int)(DateHelper.UnixTimeStampToDateTime(response.Data.Response.AccessTokenExpireIn)
                - DateTime.Now).TotalSeconds,
                RefreshExpiresIn = (int)(DateHelper.UnixTimeStampToDateTime(response.Data.Response.RefreshTokenExpireIn)
                - DateTime.Now).TotalSeconds,
            };
        }

        public async Task<ShopInfoResponseV2> GetShopInfo(ChannelAuth auth, KvInternalContext context,
            bool isOnlyGetInfo = false, Platform platform = null)
        {
            var timeout = SelfAppConfig.ForwarderApiFeature.DefaultForwarder;
            if (isOnlyGetInfo && !string.IsNullOrEmpty(auth?.AccessToken))
            {
                var shop = await TikTokHelper.GetShopInfo(auth, platform);
                return new ShopInfoResponseV2
                {
                    Name = shop.Data.Response.ShopList.FirstOrDefault().ShopName,
                    IdentityKey = shop.Data.Response.ShopList.FirstOrDefault().ShopId
                };
            }
            var response = await Policy.HandleResult<IRestResponse<TiktokBaseResponse<TiktokShopListReponse>>>(msg => msg.StatusCode == HttpStatusCode.GatewayTimeout)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (result, timeSpan, retryCount) =>
                    {
                        Log.Warn(
                            $"GetShopInfoWaitAndRetryAsync with {result.Result.StatusCode}. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount}");
                    }).ExecuteAsync(() => TikTokHelper.GetShopInfo(auth, platform, timeout));

            if (response?.Data != null && response.Data.IsError()) throw new OmniException(response.Data.Message);

            return new ShopInfoResponseV2
            {
                Name = response.Data.Response.ShopList.FirstOrDefault().ShopName,
                IdentityKey = response.Data.Response.ShopList.FirstOrDefault().ShopId
            };
        }


        private async Task<IRestResponse<TiktokBaseResponse<TiktokProductsResponse>>> GetListProductPolly(int pageNo, ChannelAuth auth, long channelId, LogObject getGetProductsLog,
            DateTime? updateTimeFrom = null, KvInternalContext kvContext = null, Platform platform = null, bool isCallGrpcRetry = false)
        {
            var response = await Policy
                 .HandleResult<IRestResponse<TiktokBaseResponse<TiktokProductsResponse>>>(msg =>
                 {
                     if (msg.StatusCode == HttpStatusCode.GatewayTimeout || msg.StatusCode == HttpStatusCode.InternalServerError)
                     {
                         return true;
                     }
                     if (msg?.Data?.Code == RequestIsLimitedStatusCode || msg?.Data?.Code == TooManyRequestsStatusCode || msg?.Data?.Message == RequestIsLimitedMessage)
                     {
                         getGetProductsLog.Description = $"Request GetListProductPolly is limited with requestId : {msg.Data?.RequestId}";
                         return true;
                     }
                     return false;
                 })
                 .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                     (res, timeSpan, retryCount) =>
                     {
                         Log.Warn(
                             $"Request failed with {res.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                     }).ExecuteAsync(() => TikTokHelper.GetProducts(auth, platform, pageNo, SelfAppConfig.Tiktok.ProductPageSize, updateTimeFrom));
            if (!string.IsNullOrEmpty(response.ErrorMessage) || response.Data.IsError())
            {
                if (response?.Data?.Code == TiktokErrorCode.TokenExpired || response?.Data?.Code == TiktokErrorCode.SellerIsInactivated)
                {
                    getGetProductsLog.Description = $"DeActive channel because {response?.Data?.Code}";
                    await _integrationInternalClient.DeactivateChannel(kvContext, channelId, "Tiktok", null, getGetProductsLog.Id.ToString());
                }
                getGetProductsLog.ResponseObject = new { Status = response.StatusCode, response?.Content };
                getGetProductsLog.LogError();

                var ex = new OmniException($"Call tiktok with error {response?.Data?.Message ?? response.ErrorMessage}");
                throw ex;
            }
            return response;
        }

        private async Task<List<TiktokProductResponse>> GetListProductWithTaskAsync(ChannelAuth auth, long channelId, LogObject getGetProductsLog,
            DateTime? updateTimeFrom = null, KvInternalContext kvContext = null, Platform platform = null)
        {
            var isCallGrpc = SelfAppConfig.ForwarderApiFeature.Enable && SelfAppConfig.ForwarderApiFeature.DefaultForwarder;
            var variations = new List<TiktokProductResponse>();
            int pageNo = 1;
            var response = await GetListProductPolly(pageNo, auth, channelId, getGetProductsLog, updateTimeFrom, kvContext,
               platform, isCallGrpc);

            var totalItem = response?.Data?.Response.Products?.Count ?? 0;
            var totalSumProduct = response?.Data?.Response?.Total ?? 0;

            if (totalItem <= 0 || response?.Data?.Response?.Products == null || totalSumProduct == 0)
            {
                return variations;
            }
            variations.AddRange(response?.Data?.Response?.Products.Where(x => x.Skus != null && x.Skus.Any()).ToList());
            if (totalItem < totalSumProduct)
            {
                int pages = (totalSumProduct / totalItem) + 1;
                var tiktokConfig = SelfAppConfig.Tiktok;
                int taskCallMaxPerThread = tiktokConfig.ProductListThread;

                int numberOfBatchCallPageSize = (pages / taskCallMaxPerThread) + 1;
                for (int i = 0; i < numberOfBatchCallPageSize; i++)
                {
                    var taskLst = new List<Task<List<TiktokProductResponse>>>();
                    List<int> pageNumberLst = GetPageNumberList(i, taskCallMaxPerThread);
                    foreach (var pageNumber in pageNumberLst)
                    {
                        taskLst.Add(GetTiktokProductsResponse(pageNumber, auth, channelId, getGetProductsLog, updateTimeFrom, kvContext, platform));
                    }
                    var taskResultList = await Task.WhenAll(taskLst);
                    foreach (var itemResult in taskResultList)
                    {
                        variations.AddRange(itemResult.Where(x => x.Skus != null && x.Skus.Any()).ToList());
                    }
                }
            }
            return variations;
        }

        private List<int> GetPageNumberList(int index, int taskCallMaxPerThread)
        {
            int fromPageNumber = index * taskCallMaxPerThread; // = 0
            List<int> result = new List<int>();
            for (int i = 0; i < taskCallMaxPerThread; i++)
            {
                int pageNumber = fromPageNumber + i + 2;
                result.Add(pageNumber);
            }
            return result;
        }

        private async Task<List<TiktokProductResponse>> GetTiktokProductsResponse(int pageNo, ChannelAuth auth, long channelId, LogObject getGetProductsLog,
            DateTime? updateTimeFrom = null, KvInternalContext kvContext = null, Platform platform = null)
        {
            var variations = new List<TiktokProductResponse>();
            var response = await GetListProductPolly(pageNo, auth, channelId, getGetProductsLog, updateTimeFrom, kvContext, platform);
            var totalItem = response?.Data?.Response.Products?.Count ?? 0;
            if (totalItem <= 0 || response?.Data?.Response?.Products == null)
            {
                return variations;
            }
            return response?.Data?.Response?.Products.Where(x => x.Skus != null && x.Skus.Any()).ToList();
        }

        private async Task<Dictionary<string, ProductDetailSkus>> GetProductDetailAsyncLst(ChannelAuth auth, long channelId, List<string> itemIdList, LogObject loggerExtension, Platform platform, int pageSize = 10)
        {
            Dictionary<string, ProductDetailSkus> resultDic = new Dictionary<string, ProductDetailSkus>();
            int totalCount = itemIdList.Count;
            var totalPages = (int)Math.Ceiling(totalCount / (decimal)pageSize);
            for (int page = 1; page <= totalPages; page++)
            {
                var taskLst = new List<Task<ProductDetailSkus>>();
                var itemIdPaging = itemIdList.Skip((page - 1) * pageSize).Take(pageSize).ToList();
                foreach (var itemId in itemIdPaging)
                {
                    taskLst.Add(GetProductDetailWithHandleException(auth, channelId, loggerExtension.Id, itemId, platform));
                }
                var taskResultList = await Task.WhenAll(taskLst);
                for (int i = 0; i < taskResultList.Length; i++)
                {
                    var taskItemResult = taskResultList[i];
                    if (taskItemResult != null)
                    {
                        resultDic.Add(itemIdPaging[i], taskItemResult);
                    }
                }
            }
            return resultDic;
        }


        public async Task<ProductListResponse> GetListProduct(ChannelAuth auth, long channelId, Guid logId, bool isFirstSync,
            DateTime? updateTimeFrom = null, KvInternalContext kvContext = null, Platform platform = null)
        {
            var getGetProductsLog = new LogObject(Log, logId)
            {
                Action = string.Format(EcommerceTypeException.GetListProduct, Ecommerce.TikTok),
                OmniChannelId = channelId,
                RequestObject = $"LastSync: {updateTimeFrom}"
            };
            var result = new ProductListResponse();
            var variations = new List<TiktokProductResponse>();
            var tiktokConfig = SelfAppConfig.Tiktok;
            variations.AddRange(await GetListProductWithTaskAsync(auth, channelId, getGetProductsLog, updateTimeFrom, kvContext, platform));
            var newProduct = variations.Where(p => p.status == TiktokProductStatus.Live && p.Skus != null)
                .SelectMany(p => p.Skus, (parent, child) => new Product
                {
                    ItemId = child.Id.ToString(),
                    ItemSku = DateHelper.TrimIfNotNull(child.SellerSku),
                    ParentItemId = parent.Id.ToString(),
                    Type = (byte)ChannelProductType.Variation
                }).ToList();
            var removeProduct = variations.Where(p => p.status == TiktokProductStatus.Deleted && p.Skus != null)
                .SelectMany(p => p.Skus)
                .Select(sku => sku.Id.ToString()).ToList();
            var productMapDic = await GetProductDetailAsyncLst(auth, channelId, newProduct.Select(item => item.ParentItemId).Distinct().ToList(), getGetProductsLog, platform, tiktokConfig.ProductDetailThread);
            ProcessMapProductDetail(newProduct, productMapDic);
            result.Data.AddRange(newProduct);
            result.RemovedProductIds.AddRange(removeProduct);
            if (!isFirstSync)
            {
                result.ParentChangeIds = variations.Select(x => x.Id).Distinct().ToList();
            }

            if (result.Data.Any())
            {
                getGetProductsLog.ResponseObject = $"ProductIds: {result.Data.Select(x => x.ItemId).ToSafeJson()}";
                getGetProductsLog.LogInfo();
            }
            result.Total = result.Data.Count;
            return result;
        }

        private void ProcessMapProductDetail(List<Product> productLst, Dictionary<string, ProductDetailSkus> productMapDic)
        {
            foreach (var productItem in productLst)
            {
                if (productMapDic.TryGetValue(productItem.ParentItemId, out ProductDetailSkus productDetailSkusInfo))
                {
                    productItem.ItemName = GetFullNameProduct(productItem, productDetailSkusInfo);
                    productItem.ItemImages = GetFullImages(productItem, productDetailSkusInfo);
                }
            }
        }

        private List<string> GetFullImages(Product productInfo, ProductDetailSkus productDetailSkusInfo)
        {
            List<string> fullImages = new List<string>();
            if (productDetailSkusInfo == null)
            {
                return fullImages;
            }
            var variations = productDetailSkusInfo?.Variations?.Select(x => (TiktokProductDetailsSku)x).ToList();
            var attributeInfo = variations.FirstOrDefault(item => item.Id == productInfo.ItemId);
            if (attributeInfo == null || attributeInfo.SalesAttributes == null || attributeInfo.SalesAttributes.Count == 0)
            {
                return productDetailSkusInfo.Images;
            }
            foreach (var salesAttribute in attributeInfo.SalesAttributes)
            {
                if (salesAttribute.SkuImg != null && salesAttribute.SkuImg.UrlList != null && salesAttribute.SkuImg.UrlList.Count > 0)
                {
                    fullImages.AddRange(salesAttribute.SkuImg.UrlList);
                }
            }
            return fullImages.Distinct().ToList();
        }

        private string GetFullNameProduct(Product productInfo, ProductDetailSkus productDetailSkusInfo)
        {
            if (productDetailSkusInfo == null)
            {
                return String.Empty;
            }
            var variations = productDetailSkusInfo?.Variations?.Select(x => (TiktokProductDetailsSku)x).ToList();
            var attributeInfo = variations.FirstOrDefault(item => item.Id == productInfo.ItemId);
            if (attributeInfo == null || attributeInfo.SalesAttributes == null || attributeInfo.SalesAttributes.Count == 0)
            {
                return productDetailSkusInfo.Name;
            }
            var separateText = " - ";
            return $"{productDetailSkusInfo.Name}{separateText}{string.Join(separateText, attributeInfo.SalesAttributes.Select(item => item.ValueName))}";

        }

        private async Task<ProductDetailSkus> GetProductDetailWithHandleException(ChannelAuth auth, long channelId, Guid logId, string parentProductId, Platform platform)
        {
            try
            {
                var result = await GetProductDetail(auth, channelId, logId, null, parentProductId, null, platform);
                return result?.ProductDetails?.FirstOrDefault() ?? null;
            }
            catch
            {
                return null;
            }
        }

        public async Task<ProductDetailResponse> GetProductDetail(ChannelAuth auth, long channelId, Guid logId,
            object productId, object parentProductId, string productSku, Platform platform)
        {
            var getGetProductDetailLog = new LogObject(Log, logId)
            {
                Action = "TiktokGetProductDetail",
                OmniChannelId = channelId,
                RequestObject = $"ProductId: {productId} Sku:{productSku}"
            };

            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                getGetProductDetailLog.ResponseObject = $"Auth:{auth.ToSafeJson()} EmptyAccessToken";
                getGetProductDetailLog.LogError();
                throw new OmniException("EmptyAccessToken");
            }
            bool isRateLimit = false;
            var isCallGrpc = SelfAppConfig.ForwarderApiFeature.DefaultForwarder;
            var response = await Policy
                 .HandleResult<IRestResponse<TiktokBaseResponse<TiktokProductDetailsResponse>>>(msg =>
                 {
                     isRateLimit = false;
                     if (msg.StatusCode == HttpStatusCode.GatewayTimeout || msg.StatusCode == HttpStatusCode.InternalServerError)
                     {
                         return true;
                     }
                     if (msg?.Data?.Code == RequestIsLimitedStatusCode || msg?.Data?.Code == TooManyRequestsStatusCode || msg?.Data?.Message == RequestIsLimitedMessage)
                     {
                         isRateLimit = true;
                         getGetProductDetailLog.Description = $"Request TiktokGetProductDetail is limited with requestId : {msg.Data?.RequestId}";
                         return true;
                     }
                     return false;
                 })
                 .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                     (res, timeSpan, retryCount, context) =>
                     {
                         Log.Warn(
                             $"Request failed with {res.Result.StatusCode} - Channel {channelId}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                     }).ExecuteAsync(() => TikTokHelper.GetProductDetail(auth, platform, parentProductId?.ToString() ?? productId.ToString(), isCallGrpc));

            if (response?.Data == null || response?.Data?.Response == null)
            {
                if (isRateLimit)
                {
                    getGetProductDetailLog.Description = $"Cannot get product detail because of rate limit";
                    getGetProductDetailLog.LogWarning();
                }
                return null;
            }
            getGetProductDetailLog.ResponseObject = response.Data;
            getGetProductDetailLog.LogInfo();

            var prdDetail = new ProductDetailSkus
            {
                ItemId = response.Data.Response.ProductId,
                Name = response.Data.Response.ProductName,
                Status = response.Data.Response.ProductStatus != TiktokProductStatus.Deleted ? "active" : "inactive",
                Images = GetImages(response.Data.Response.Images),
                Description = response.Data.Response.Description,
                Variations = response.Data.Response.Skus.Select(x => (object)x).ToList()
            };

            var result = new ProductDetailResponse
            {
                ItemId = response.Data.Response.ProductId,
                ProductDetails = new List<ProductDetailSkus>
                {
                    prdDetail
                }
            };
            return result;
        }

        private List<string> GetImages(List<TiktokImage> images)
        {
            List<string> result = new List<string>();
            if (images != null && images.Count > 0)
            {
                foreach (var image in images)
                {
                    if (image.UrlList != null && image.UrlList.Count > 0)
                    {
                        result.AddRange(image.UrlList);
                    }
                }
            }

            return result.Distinct().ToList();
        }

        public Task<(bool, string)> SyncOnHand(
            int retailerId, string kvProductSku, long channelId, Guid logId,
            ChannelAuth auth, string itemId, string itemSku, double onHand, byte productType,
            LogObjectMicrosoftExtension loggerExtension, string parentProductId = null, Platform platform = null)
        {
            throw new NotImplementedException();
        }

        public async Task<(bool, string)> SyncMultiOnHand(
            int retailerId, long channelId, Guid logId, ChannelAuth auth,
            List<MultiProductItem> productItems, LogObjectMicrosoftExtension loggerExtension, byte productType,
            Platform platform)
        {
            var logger = loggerExtension.Clone(string.Format(EcommerceTypeException.SyncMultiOnHand, Ecommerce.TikTok));
            logger.RequestObject = productItems;

            //Hiện tại do tiktok chưa hỗ trợ multi productId. Đồng bộ 1 List Parrent 
            var request = productItems.GroupBy(grp => grp.ParentItemId).Select(parrent => new TiktokUpdateStockRequest
            {
                ProductId = parrent.Key,
                Skus = parrent.Select(child => new TiktokSku
                {
                    Id = child.ItemId,
                    ItemSku = child.ItemSku,
                    StockInfos = new List<TiktokSkuInfo>
                    {
                        new TiktokSkuInfo
                        {
                            AvailableStock = Convert.ToInt32(child.OnHand),
                        }
                    }
                }).ToList(),
            }).FirstOrDefault();
            var resultProduct = new List<Product>();
            var response = await Policy.HandleResult<IRestResponse<TiktokBaseResponse<TiktokUpdateStockResponse>>>(msg => msg.StatusCode == HttpStatusCode.GatewayTimeout)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (result, timeSpan, retryCount) =>
                    {
                        Log.Warn(
                            $"UpdateStockWaitAndRetryAsync with {result.Result.StatusCode}. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount}");
                    }).ExecuteAsync(() => TikTokHelper.UpdateProductStock(auth, platform, request));

            if (!string.IsNullOrEmpty(response?.ErrorMessage))
            {
                var msg = $" RequestId: {response?.Data?.RequestId} - msg: {response?.Data?.Message ?? response?.ErrorMessage}";
                logger.LogError(new TiktokException(msg));
            }

            resultProduct.AddRange(request.Skus.Select(x => new Product
            {
                ItemId = x.Id,
                OnHand = x.StockInfos.FirstOrDefault().AvailableStock,
                ItemSku = x.ItemSku,
                ErrorMessage = response?.Data?.IsError() ?? !string.IsNullOrEmpty(response.ErrorMessage) ?
                response?.Data?.Message ?? response?.ErrorMessage : string.Empty
            }));

            logger.ResponseObject = response?.Content;
            logger.LogInfo();
            return (!response?.Data?.IsError() ?? true, (new { Product = resultProduct }).ToSafeJson());
        }

        public async Task<(bool, string, string)> SyncOnHandWithGroupParrent(ChannelAuth auth,
            string parentItemId, List<MultiProductItem> productItems, LogObjectMicrosoftExtension logger, Platform platform)
        {
            var loggerStock = logger.Clone(string.Format(EcommerceTypeException.SyncMultiOnHandWithGroupParrent,
                Ecommerce.TikTok));
            loggerStock.RequestObject = productItems;
            try
            {
                var result = new List<Product>();
                var request = new TiktokUpdateStockRequest
                {
                    ProductId = parentItemId,
                    Skus = productItems.Select(p => new TiktokSku
                    {
                        Id = p.ItemId,
                        StockInfos = new List<TiktokSkuInfo>
                        {
                            new TiktokSkuInfo{ AvailableStock = Convert.ToInt32(p.OnHand) }
                        }
                    }).ToList()
                };
                var response = await Policy
                     .HandleResult<IRestResponse<TiktokBaseResponse<TiktokUpdateStockResponse>>>(msg => msg.StatusCode != HttpStatusCode.OK)
                     .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                     (res, timeSpan, retryCount, context) =>
                     {
                         var logRetry = loggerStock.Clone("TiktokSyncStockV2WaitAndRetry");
                         var messageErr = $"TiktokSyncStockV2WaitAndRetry with {res.Result?.StatusCode} - RequestId: {res.Result?.Data?.RequestId}. " +
                         $"Waiting {timeSpan:g} before next retry. Retry attempt {retryCount}";
                         var ex = new TiktokException(messageErr);
                         logRetry.LogError(ex);
                     }).ExecuteAsync(() => TikTokHelper.UpdateProductStock(auth, platform, request));

                loggerStock.ResponseObject = response?.Content;
                loggerStock.LogInfo();

                if (!string.IsNullOrEmpty(response?.ErrorMessage))
                {
                    var msg = $" RequestId: {response?.Data?.RequestId} - msg: {response?.Data?.Message ?? response?.ErrorMessage}";
                    logger.LogError(new TiktokException(msg));
                }

                result.AddRange(request.Skus.Select(x => new Product
                {
                    ItemId = x.Id,
                    OnHand = x.StockInfos.FirstOrDefault().AvailableStock,
                    ItemSku = x.ItemSku,
                    ErrorMessage = response?.Data?.IsError() ?? !string.IsNullOrEmpty(response.ErrorMessage) ?
                    response?.Data?.Message ?? response?.ErrorMessage : string.Empty
                }));

                return (result.Any(),
                    (new { response?.Data?.RequestId, Product = result }).ToSafeJson(),
                    response?.Data?.Message ?? response?.ErrorMessage);
            }
            catch (Exception ex)
            {
                loggerStock.LogError(ex);
                throw;
            }
        }

        public Task<(bool, string)> SyncPrice(int retailerId, string kvProductSku, long channelId, Guid logId,
            ChannelAuth auth, string itemId, string itemSku, decimal? basePrice
            , byte productType, string parentItemId, LogObjectMicrosoftExtension loggerExtension,
            decimal? salePrice = null, DateTime? startSaleDateTime = null, DateTime? endSaleDateTime = null,
            Platform platform = null)
        {
            throw new NotImplementedException();
        }

        public async Task<(bool, string, int?)> SyncMultiPrice(int retailerId, long channelId, Guid logId, ChannelAuth auth,
            List<MultiProductItem> productItems, byte productType, LogObjectMicrosoftExtension loggerExtension,
            Platform platform)
        {
            var logger = loggerExtension.Clone(string.Format(EcommerceTypeException.SyncMultiPrice, Ecommerce.TikTok));
            logger.RequestObject = productItems;
            //Hiện tại do tiktok chưa hỗ trợ multi productId. Đồng bộ 1 List Parrent 
            var request = productItems.GroupBy(grp => grp.ParentItemId).Select(parrent => new TiktokUpdatePriceRequest
            {
                ProductId = parrent.Key,
                Skus = parrent.Select(child => new TiktokPriceSku
                {
                    Id = child.ItemId,
                    ItemSku = child.ItemSku,
                    OriginalPrice = child.Price.ToString()
                }).ToList(),
            }).FirstOrDefault();
            var resultProduct = new List<Product>();
            var response = await Policy.HandleResult<IRestResponse<TiktokBaseResponse<TiktokUpdatePriceResponse>>>(msg => msg.StatusCode == HttpStatusCode.GatewayTimeout)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (result, timeSpan, retryCount) =>
                    {
                        Log.Warn(
                            $"UpdatePriceWaitAndRetryAsync with {result.Result.StatusCode}. Waiting {timeSpan:g} " +
                            $"before next retry. Retry attempt {retryCount}");
                    }).ExecuteAsync(() => TikTokHelper.UpdateProductPrice(auth, platform, request));

            resultProduct.AddRange(request.Skus.Select(x => new Product
            {
                ItemId = x.Id,
                ItemSku = x.ItemSku,
                BasePrice = decimal.Parse(x.OriginalPrice),
                ErrorMessage = response.Data.IsError() ? response.Data.Message : string.Empty
            }));

            logger.ResponseObject = response.Content;
            logger.LogInfo();
            return (!response.Data.IsError(), (new { Product = resultProduct }).ToSafeJson(), null);
        }

        public async Task<(bool, string, string)> SyncPriceWithGroupParrent(ChannelAuth auth,
            string parentItemId, List<MultiProductItem> productItems,
            LogObjectMicrosoftExtension logger,
            Platform platform)
        {
            var loggerPrice = logger.Clone(string.Format(EcommerceTypeException.SyncMultiPriceWithGroupParrent,
                Ecommerce.TikTok));
            loggerPrice.RequestObject = productItems;
            try
            {
                var result = new List<Product>();
                var request = new TiktokUpdatePriceRequest
                {
                    ProductId = parentItemId,
                    Skus = productItems.Select(p => new TiktokPriceSku
                    {
                        Id = p.ItemId,
                        OriginalPrice = p.Price.ToString()
                    }).ToList()
                };

                var response = await Policy.HandleResult<IRestResponse<TiktokBaseResponse<TiktokUpdatePriceResponse>>>(msg => msg.StatusCode == HttpStatusCode.GatewayTimeout)
                          .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                     (res, timeSpan, retryCount, context) =>
                     {
                         var logRetry = loggerPrice.Clone("TiktokSyncPriceV2WaitAndRetry");
                         var messageErr = $"TiktokSyncPriceV2WaitAndRetry with {res.Result?.StatusCode} - RequestId: {res.Result?.Data?.RequestId}. " +
                         $"Waiting {timeSpan:g} before next retry. Retry attempt {retryCount}";
                         var ex = new ShopeeException(messageErr);
                         logRetry.LogError(ex);
                     }).ExecuteAsync(() => TikTokHelper.UpdateProductPrice(auth, platform, request));

                loggerPrice.ResponseObject = response?.Content;
                loggerPrice.LogInfo();

                if (!string.IsNullOrEmpty(response?.ErrorMessage))
                {
                    var msg = $" RequestId: {response?.Data?.RequestId} - msg: {response?.Data?.Message ?? response?.ErrorMessage}";
                    logger.LogError(new TiktokException(msg));
                }

                result.AddRange(request.Skus.Select(x => new Product
                {
                    ItemId = x.Id,
                    ItemSku = x.ItemSku,
                    BasePrice = decimal.Parse(x.OriginalPrice),
                    ErrorMessage = response?.Data?.IsError() ?? !string.IsNullOrEmpty(response.ErrorMessage) ?
                    response?.Data?.Message ?? response?.ErrorMessage : string.Empty
                }));

                return (result.Any(),
                    (new { response?.Data?.RequestId, Product = result }).ToSafeJson(),
                    response?.Data?.Message ?? response?.ErrorMessage);
            }
            catch (Exception ex)
            {
                loggerPrice.LogError(ex);
                throw;
            }
        }

        public Task<(bool, List<CreateOrderRequest>)> GetOrders(
            ChannelAuth auth,
            OrderRequest request,
            KvInternalContext context,
            LogObjectMicrosoftExtension logInfo,
            Platform platform)
        {
            throw new NotImplementedException();
        }

        public async Task<OrderDetailGetResponse> GetOrderDetail(int retailerId, long channelId, ChannelAuth auth,
            CreateOrderRequest request, LogObjectMicrosoftExtension logInfo, Platform platform,
            OmniChannelSettingObject settings = null)
        {
            if (string.IsNullOrEmpty(request.OrderId))
            {
                logInfo.LogWarning("Order id is null or empty");
                return new OrderDetailGetResponse
                {
                    IsSuccess = false,
                    IsRetryOrder = false
                };
            }
            var tiktokOrder = await GetChannelOrderDetailList(channelId, auth, request.OrderId, platform, logInfo);
            if (tiktokOrder == null) {
                return new OrderDetailGetResponse
                {
                    IsSuccess = false,
                    IsRetryOrder = true
                };
            } 
            var settlementList = await GetOrderSettlements(channelId, auth, request.OrderId, platform, logInfo);
            var shippingInfo = await GetShippingInfo(auth, platform, request.OrderId);
            var reserveOrder = await GetReverseOrderList(auth, platform, request.OrderId);
            tiktokOrder.SettlementList = settlementList;
            var kvOrder = _tiktokBusiness.GenKvOrder(tiktokOrder, shippingInfo, reserveOrder,
                settings, request.BranchId, request.UserId, retailerId);
            kvOrder = _tiktokBusiness.CalFeeKvOrder(kvOrder, tiktokOrder, request.GroupId);
            return new OrderDetailGetResponse
            {
                IsSuccess = true,
                IsRetryOrder = false,
                Order = kvOrder
            };
        }


        public async Task<List<TiktokSettlement>> GetOrderSettlements(long channelId, ChannelAuth auth,
           string orderId,
           Platform platform,
           LogObjectMicrosoftExtension logInfo)
        {
            var getOrderDetailLog = new LogObject(Log, logInfo.Id)
            {
                Action = "GetOrderSettlements",
                OmniChannelId = channelId,
                RequestObject = $"orderId: {orderId}"
            };

            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                logInfo.ResponseObject = $"Auth:{auth.ToSafeJson()} EmptyAccessToken";
                var ex = new OmniException("EmptyAccessToken");
                logInfo.LogError(ex);
                throw ex;
            }
            var response = await Policy
             .HandleResult<IRestResponse<TiktokBaseResponse<GetOrderSettlementsResponse>>>(msg => msg.StatusCode == HttpStatusCode.GatewayTimeout)
             .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                 (res, timeSpan, retryCount, context) =>
                 {
                     Log.Warn(
                         $"Request failed with {res.Result.StatusCode} - ChannelAuth {auth.ShopId}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                 }).ExecuteAsync(() => TikTokHelper.GetGetOrderSettlements(auth, orderId, platform, logInfo));

            if (response?.Data == null
                || response.Data.Response == null
                || response.Data.Response.SettlementList == null
                || !response.Data.Response.SettlementList.Any())
            {
                logInfo.ResponseObject = $"request_id: {response?.Data?.RequestId}";
                logInfo.LogWarning("Empty order detail");
                return null;
            }
            getOrderDetailLog.ResponseObject = response.Data;
            getOrderDetailLog.LogInfo();
            return response.Data.Response.SettlementList;
        }


        public Task<(bool, bool, bool, List<KvInvoice>)> GetInvoiceDetail(int retailerId, long channelId, Guid logId,
            ChannelAuth auth, CreateInvoiceRequest request, LogObjectMicrosoftExtension loggerExtension,
            Platform platform,
            OmniChannelSettingObject settings)
        {
            throw new NotImplementedException();
        }

        public Task<(byte?, string, string)> GetLogisticsStatus(int retailerId, long channelId, LogObjectMicrosoftExtension logInfo,
            string orderId, ChannelAuth auth, bool isGetOrderStatus = false, Platform platform = null)
        {
            throw new NotImplementedException();
        }

        public async Task<(byte?, byte?, byte?)> GetOrderStatus(int retailerId, long channelId, Guid logId, string orderId,
            ChannelAuth auth, LogObjectMicrosoftExtension loggerExtension, bool isGetDeliveryStatus = false,
            Platform platform = null, OmniChannelSettingObject settings = null)
        {
            var tiktokOrder = await GetChannelOrderDetailList(channelId, auth, orderId, platform, loggerExtension);
            var reserveOrder = await GetReverseOrderList(auth, platform, orderId);
            var orderStatus = tiktokOrder == null ? null : _tiktokBusiness.ConvertToKvOrderStatus(retailerId, tiktokOrder, reserveOrder);
            var invoiceStatus = tiktokOrder == null ? null : _tiktokBusiness.ConvertToKvInvoiceStatus(orderStatus);
            byte? deliveryStatus = tiktokOrder == null ? null : _tiktokBusiness.ConvertToKvDeliveryStatus(retailerId, tiktokOrder, reserveOrder, settings);
            return (orderStatus, invoiceStatus, deliveryStatus);
        }

        public Task<ShopInfoResponseV2> GetShopStatus(string accessToken)
        {
            throw new NotImplementedException();
        }

        public Task EnableUpdateProduct(string accessToken)
        {
            throw new NotImplementedException();
        }

        public Task<CustomerResponse> GetCustomerByOrderId(int retailerId, long channelId, string orderId,
            ChannelAuth auth, Guid logId, LogObjectMicrosoftExtension loggerExtension, Platform platform)
        {
            throw new NotImplementedException();
        }

        public Task<List<KvInvoice>> GetInvoiceDetailByOrderId(int retailerId, Guid logId, ChannelAuth auth,
            string orderId, long channelId, LogObjectMicrosoftExtension loggerExtension, Platform platform)
        {
            throw new NotImplementedException();
        }

        public Task<List<Logistics>> GetLogisticsByShopId(int retailerId, long channelId, ChannelAuth auth,
            Guid logId)
        {
            throw new NotImplementedException();
        }

        public Task<List<Logistics>> GetLogisticsByShopId(int retailerId, long channelId, ChannelAuth auth, Guid logId, Platform platform)
        {
            throw new NotImplementedException();
        }

        public Task<List<ShopeeAttribute>> GetAttributeByShopId(int retailerId, long channelId, ChannelAuth auth,
            Guid logId, long categoryId, Platform platform)
        {
            throw new NotImplementedException();
        }

        public Task<ShopeeProductResponse> PushProductToChannel(int retailerId, long channelId, ChannelAuth auth,
            Guid logId, ShopeeProduct req, Platform platform, string kvProductAttributeStr)
        {
            throw new NotImplementedException();
        }

        public Task<CategoriesResp> GetCategories(int retailerId, long channelId, ChannelAuth auth, Guid logId,
            Platform platform)
        {
            throw new NotImplementedException();
        }

        public Task<ShopeeUploadImgResponse> UploadImg(ChannelAuth auth, int retailerId, long shopId,
            List<Image> images, Guid logId, Platform platform)
        {
            throw new NotImplementedException();
        }

        public Task<ProductListResponse> GetListDeletedProduct(ChannelAuth auth, long channelId, Guid logId,
            bool isFirstSync, KvInternalContext kvContext = null, DateTime? updateTimeFrom = null)
        {
            throw new NotImplementedException();
        }

        public Task<KvOrder> GenListKvOrder(long shopId, int? saleChannelId, int branchId, long userId, int retailerId,
            OrderDetail shopeeOrder,
            OmniChannelSettingObject settings, LogObjectMicrosoftExtension logInfo, Platform platform)
        {
            throw new NotImplementedException();
        }

        Task<CustomerResponse> IBaseClient.GetCustomerByOrderId(int retailerId, long channelId, string orderId,
            ChannelAuth auth, Guid logId)
        {
            throw new NotImplementedException();
        }

        public Task<List<OrderDetail>> GetListOrderDetail(ChannelAuth auth, string[] orderSnList,
            LogObjectMicrosoftExtension logInfo, Platform platform)
        {
            throw new NotImplementedException();
        }

        public List<KvInvoice> GenKvInvoices(CreateInvoiceRequest request)
        {
            var kvOrderNew = JsonConvert.DeserializeObject<KvOrder>(request.Order);
            var kvOrder = request.KvOrder;
            if (kvOrder == null || request.KvDelivery == null) return new List<KvInvoice>();

            if (kvOrderNew.ChannelStatus == TiktokOrderStatus.Unpaid.ToString() ||
                kvOrderNew.ChannelStatus == TiktokOrderStatus.AwaitingShipment.ToString() ||
                string.IsNullOrEmpty(kvOrderNew.OrderDelivery?.DeliveryCode))
            {
                return new List<KvInvoice>();
            }
            //Khởi tạo invoice
            var kvInvoice = new KvInvoice
            {
                Code = $"HDTTS_{request.OrderId}",
                RetailerId = kvOrder.RetailerId,
                OrderId = kvOrder.Id,
                BranchId = kvOrder.BranchId,
                Description = kvOrder.Description,
                PurchaseDate = kvOrderNew.PurchaseDate,
                CustomerId = kvOrder.CustomerId,
                UsingCod = true,
                PriceBookId = kvOrder.Extra?.FromJson<Extra>().PriceBookId?.Id ?? 0,
                DeliveryDetail = new InvoiceDelivery
                {
                    Receiver = request.KvDelivery?.Receiver,
                    ContactNumber = request.KvDelivery?.ContactNumber,
                    DeliveryCode = kvOrderNew.OrderDelivery?.DeliveryCode,
                    Address = request.KvDelivery?.Address,
                    UsingPriceCod = true,
                    WardName = request.KvDelivery?.WardName,
                    LocationName = request.KvDelivery?.LocationName,
                    LocationId = request.KvDelivery?.LocationId,
                    Status = (byte)DeliveryStatus.Pending,
                    DeliveryBy = request.KvDelivery.PartnerId,
                    FeeJson = kvOrderNew.OrderDelivery?.FeeJson,
                    Price = kvOrderNew.OrderDelivery?.Price,
                    LatestStatus = kvOrderNew.DeliveryStatus ?? (byte)DeliveryStatus.Pending
                },
                Status = (int)InvoiceState.Pending,
                SaleChannelId = kvOrder.SaleChannelId,
                InvoiceDetails = new List<InvoiceDetail>(),
                NewStatus = _tiktokBusiness.ConvertToKvInvoiceStatus(kvOrderNew.NewStatus),
                ChannelStatus = kvOrderNew.ChannelStatus,
                Discount = kvOrderNew.Discount,
                SurCharges = kvOrderNew.SurCharges,
                DeliveryStatus = kvOrderNew.DeliveryStatus
            };

            //Cập nhật invoice detail
            foreach (var detail in kvOrderNew.OrderDetails)
            {
                kvInvoice.InvoiceDetails.Add(new InvoiceDetail
                {
                    ProductChannelId = detail.ProductChannelId,
                    ParentChannelProductId = detail.ParentChannelProductId,
                    ProductChannelName = detail.ProductChannelName,
                    ProductChannelSku = detail.ProductChannelSku,
                    Quantity = detail.Quantity,
                    Price = detail.Price,
                    ItemId = detail.ItemId,
                    VariationId = detail.VariationId,
                    AddOnDealId = detail.AddOnDealId,
                    DiscountPrice = detail.DiscountPrice,
                    Discount = detail.Discount,
                    IsUsed = detail.IsUsed,
                    Uuid = detail.Uuid,
                    UseWarranty = detail.UseWarranty
                });
            }
            return new List<KvInvoice> { kvInvoice };
        }
        public Task<(bool, List<Transaction>)> GetListTransaction(ChannelAuth auth, DateTime createFrom, DateTime createTo,
            LogObjectMicrosoftExtension log, Platform platform)
        {
            throw new NotImplementedException();
        }

        public Task<SellerWareHouseV2Response> GetTikiWarehouse(string accessToken)
        {
            throw new NotImplementedException();
        }

        public Task<(bool, List<FinanceTransaction>)> GetFinanceTransactions(string token, long channelId, string orderSn, DateTime? startTime, DateTime? endTime, LogObjectMicrosoftExtension logInfo, bool isBreakIfMaximum)
        {
            throw new NotImplementedException();
        }

        public async Task<TiktokShippingInfo> GetShippingInfo(ChannelAuth auth, Platform platform, string orderId)
        {
            var response = await Policy.HandleResult<IRestResponse<TiktokBaseResponse<TiktokShippingInfo>>>(msg => msg.StatusCode == HttpStatusCode.GatewayTimeout)
               .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                   (result, timeSpan, retryCount) =>
                   {
                       Log.Warn(
                           $"GetShippingWaitAndRetryAsync with {result.Result.StatusCode}. Waiting {timeSpan:g} " +
                           $"before next retry. Retry attempt {retryCount}");
                   }).ExecuteAsync(() => TikTokHelper.GetShippingInfo(auth, platform, orderId));
            if (response?.Data == null || response.Data.IsError())
            {
                return null;
            }

            return response?.Data.Response;
        }

        public async Task<TiktokReverseOrderListResponse> GetReverseOrderList(ChannelAuth auth,
            Platform platform, string orderId)
        {
            var getReverseLog = new LogObject(Log, Guid.NewGuid())
            {
                Action = "GetReverseOrderList",
                RequestObject = $"orderId: {orderId}"
            };

            try
            {
                var request = new TiktokReverseOrderRequest
                {
                    Offset = 0,
                    Size = 100,
                    OrderId = long.Parse(orderId)
                };
                var isMore = true;
                var reverseResult = new List<TiktokReverseInfo>();

                while (isMore)
                {
                    var response = await Policy.HandleResult<IRestResponse<TiktokBaseResponse<TiktokReverseOrderListResponse>>>(msg => msg.StatusCode == HttpStatusCode.GatewayTimeout)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                      (result, timeSpan, retryCount) =>
                      {
                          Log.Warn(
                              $"GetReverseOrderListWaitAndRetryAsync with {result.Result.StatusCode}. Waiting {timeSpan:g} " +
                              $"before next retry. Retry attempt {retryCount}");
                      }).ExecuteAsync(() => TikTokHelper.GetReverseOrderList(auth, platform, request));
                    if (response?.Data == null || response.Data.IsError())
                    {
                        var msg = $" RequestId: {response?.Data?.RequestId}  - {auth.ToSafeJson()} - {platform.ToSafeJson()} - msg: {response?.Data?.Message ?? response?.ErrorMessage}";
                        getReverseLog.LogError(new TiktokException(msg));
                        throw new TiktokException(msg);
                    }
                    reverseResult.AddRange(response.Data.Response.ReverseList ?? new List<TiktokReverseInfo>());
                    isMore = response.Data.Response.More;
                    if (isMore) request.Offset += request.Size;
                }

                return new TiktokReverseOrderListResponse
                {
                    ReverseList = reverseResult,
                };
            }
            catch (Exception ex)
            {
                getReverseLog.LogError(ex);
                throw;
            }
        }


        #region private method
        private async Task<TiktokOrderDetail> GetChannelOrderDetailList(long channelId, ChannelAuth auth,
            string orderId,
            Platform platform,
            LogObjectMicrosoftExtension logInfo)
        {
            var getOrderDetailLog = new LogObject(Log, logInfo.Id)
            {
                Action = "GetChannelOrderDetailList",
                OmniChannelId = channelId,
                RequestObject = $"orderId: {orderId}"
            };

            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                logInfo.ResponseObject = $"Auth:{auth.ToSafeJson()} EmptyAccessToken";
                var ex = new OmniException("EmptyAccessToken");
                logInfo.LogError(ex);
                throw ex;
            }
            var response = await Policy
             .HandleResult<IRestResponse<TiktokBaseResponse<TiktokOrderDetailsResponse>>>(msg => msg.StatusCode == HttpStatusCode.GatewayTimeout)
             .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                 (res, timeSpan, retryCount, context) =>
                 {
                     Log.Warn(
                         $"Request failed with {res.Result.StatusCode} - ChannelAuth {auth.ShopId}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                 }).ExecuteAsync(() => TikTokHelper.GetOrdersDetail(auth, new[] { orderId }, platform, logInfo));

            if (response?.Data == null
                || response.Data.Response == null
                || response.Data.Response.OrderDetailLst == null
                || !response.Data.Response.OrderDetailLst.Any())
            {
                logInfo.ResponseObject = $"request_id: {response?.Data?.RequestId} - {auth.ToSafeJson()} - {platform.ToSafeJson()}";
                logInfo.LogWarning("Empty order detail");
                return null;
            }
            getOrderDetailLog.ResponseObject = response.Data;
            getOrderDetailLog.LogInfo();
            return response.Data.Response.OrderDetailLst.FirstOrDefault();
        }
        #endregion
    }
}




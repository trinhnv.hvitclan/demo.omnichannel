﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Helper;
using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.RequestDTO;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Impls;
using Demo.OmniChannel.Sdk.Interfaces;
using Demo.OmniChannel.Sdk.RequestDtos;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Dtos;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.ShareKernel.Models;
using Newtonsoft.Json;
using Polly;
using RestSharp;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Logging;
using ChannelProductType = Demo.OmniChannel.ChannelClient.Common.ChannelProductType;
using ChannelStatus = Demo.OmniChannel.ChannelClient.Common.ChannelStatus;
using Image = Demo.OmniChannel.ShareKernel.Dtos.Image;
using InvoiceDelivery = Demo.OmniChannel.ChannelClient.Models.InvoiceDelivery;
using InvoiceDetail = Demo.OmniChannel.ChannelClient.Models.InvoiceDetail;
using Logistics = Demo.OmniChannel.ChannelClient.Models.Logistics;
using OrderDetail = Demo.OmniChannel.ChannelClient.Models.OrderDetail;
using PartnerDelivery = Demo.OmniChannel.ChannelClient.Models.PartnerDelivery;
using Platform = Demo.OmniChannel.ChannelClient.Models.Platform;
using ProductDetailResponse = Demo.OmniChannel.ChannelClient.RequestDTO.ProductDetailResponse;
using SelfAppConfig = Demo.OmniChannel.ChannelClient.Common.SelfAppConfig;
using TikiSellerStatus = Demo.OmniChannel.ChannelClient.Common.TikiSellerStatus;
using TokenResponse = Demo.OmniChannel.ChannelClient.Models.TokenResponse;

namespace Demo.OmniChannel.ChannelClient.Impls
{
    public class TikiClientV2 : IBaseClient
    {
        private readonly IChannelInternalClient _channelInternalClient;
        private SelfAppConfig _config;
        private ILog Log => LogManager.GetLogger(typeof(TikiClientV2));

        private const string ConnectErrorMsg = "Đồng bộ có lỗi phía Tiki, Vui lòng thực hiện đồng bộ lại sau ít phút";
        private const int TikiTooManyRequests = 429;
        private const string TikiRefreshTokenExpired = "token_expired";
        private const string TikiRefreshTokenNotFound = ": not_found";

        public TikiClientV2(IAppSettings settings)
        {
            _channelInternalClient = new ChannelInternalClient(settings);
            _config = new SelfAppConfig(settings);
        }

        public async Task<TokenResponse> CreateToken(string code, ChannelAuth auth, Platform platform)
        {
            var response = await Policy
                .HandleResult<IRestResponse<TokenResponse>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (res, timeSpan, retryCount, context) =>
                    {
                        Log.Warn(
                            $"Request failed with {res.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                    }).ExecuteAsync(() => TikiHelperV2.CreateToken(code));
            return response?.Data;
        }

        public async Task<List<KvInvoice>> GetInvoiceDetailByOrderId(int retailerId, Guid logId, ChannelAuth auth,
            string orderId, long channelId, LogObjectMicrosoftExtension loggerExtension, Platform platform)
        {
            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                var ex = new OmniException($"Auth:{auth.ToSafeJson()} EmptyAccessToken");
                loggerExtension.LogError(ex);
                throw ex;
            }
            var orderDetail = await GetOrderDetail(auth.AccessToken, orderId, loggerExtension);

            if (orderDetail == null) return null;

            var invoice = new KvInvoice
            {
                Code = $"HDTIKI_{orderId}",
                InvoiceDetails = new List<InvoiceDetail>()
            };

            foreach (var invoiceItem in orderDetail.Items)
            {
                decimal discount = 0;
                if (invoiceItem?.Invoice?.DiscountAmount != null && invoiceItem.Invoice?.Quantity != 0)
                {
                    discount = (invoiceItem.Invoice.DiscountAmount - invoiceItem.Invoice.DiscountTikixu) / (decimal)invoiceItem.Invoice.Quantity;
                }

                invoice.InvoiceDetails.Add(new InvoiceDetail
                {
                    ProductChannelId = invoiceItem?.Product?.Id.ToString(),
                    ProductChannelSku = invoiceItem?.Product?.Sku,
                    ProductName = invoiceItem?.Product?.Name,
                    Quantity = invoiceItem?.Invoice?.Quantity ?? 0,
                    Price = invoiceItem?.Invoice?.Price ?? 0,
                    ProductChannelName = invoiceItem?.Product?.Name,
                    Discount = discount,
                    DiscountPrice = (float)discount
                });
            }

            return new List<KvInvoice> { invoice };
        }

        public Task<CustomerResponse> GetCustomerByOrderId(int retailerId, long channelId, string orderId, ChannelAuth auth, Guid logId)
        {
            throw new NotImplementedException();
        }
        public Task<CustomerResponse> GetCustomerByOrderId(int retailerId, long channelId, string orderId,
            ChannelAuth auth, Guid logId, LogObjectMicrosoftExtension loggerExtension, Platform platform)
        {
            throw new NotImplementedException();
        }

        public async Task<(byte?, byte?, byte?)> GetOrderStatus(int retailerId, long channelId, Guid logId,
            string orderId, ChannelAuth auth, LogObjectMicrosoftExtension loggerExtension,
            bool isGetDeliveryStatus = false, Platform platform = null, OmniChannelSettingObject settings = null)
        {
            loggerExtension.Action = string.Format(EcommerceTypeException.GetOrderStatus, Ecommerce.Tiki);

            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                var ex = new OmniException($"Auth:{auth.ToSafeJson()} EmptyAccessToken");
                loggerExtension.LogError(ex);
                throw ex;
            }
            
            var tikiOrderDetail = await GetOrderDetail(auth.AccessToken, orderId, loggerExtension);

            var kvOrderStatus = ConvertToKvOrderStatus(tikiOrderDetail?.Status);
            var kvInvoiceStatus = ConvertToKvInvoiceStatus(tikiOrderDetail?.Status);
            var deliveryStatus = UpdateDeliveryStatusForInvoice(tikiOrderDetail?.Status);
            return (kvOrderStatus, kvInvoiceStatus, deliveryStatus);
        }

        public async Task<ShopInfoResponseV2> GetShopStatus(string accessToken)
        {
            var response = await Policy
                .HandleResult<IRestResponse<SellerInfoV2Response>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (res, timeSpan, retryCount, ct) =>
                    {
                        Log.Warn($"Request Get Tiki seller info failed with {res.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");

                    }).ExecuteAsync(() => TikiHelperV2.GetSellerInfo(accessToken));
            if (response?.Data == null)
            {
                Log.Error($"{KeywordCommonSearch.CallTikiError} Get Tiki seller info: {new { Response = response?.Content }.ToJson()}");
                return null;
            }

            if (response.Data.Errors != null && response.Data.Errors.Any())
            {
                var errorMessage = response.Data.Errors.Join("; ");
                Log.Error($"{KeywordCommonSearch.CallTikiError} Get Tiki seller info: {new {ErrorMessages = errorMessage}.ToJson()}");
                throw new OmniException(errorMessage);
            }
            return new ShopInfoResponseV2
            {
                Email = response.Data.Email,
                Name = response.Data.Name,
                IdentityKey = response.Data.Id.ToString(),
                Status = ConvertSellerStatus(response.Data.RegistrationStatus),
                Active = response.Data.Active
            };
        }
        public async Task EnableUpdateProduct(string accessToken)
        {
            if (string.IsNullOrEmpty(accessToken))
            {
                throw new OmniException("Access Token invalid");
            }
            var response = await Policy
                         .HandleResult<IRestResponse<SellerUpdateCanEditProductV2Response>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout)
                         .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                             (res, timeSpan, retryCount, ct) =>
                             {
                                 Log.Warn($"Request update Tiki can update product {res.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");

                             }).ExecuteAsync(() => TikiHelperV2.UpdateCanUpdateProduct(accessToken));

            Log.Info($"Response update Tiki can update product: {response?.Data?.ToSafeJson()}");
        }

        public async Task<ShopInfoResponseV2> GetShopInfo(ChannelAuth auth, KvInternalContext context,
            bool isOnlyGetInfo = false, Platform platform = null)
        {
            if (isOnlyGetInfo && !string.IsNullOrEmpty(auth?.AccessToken))
            {
                var shop = await TikiHelperV2.GetSellerInfo(auth.AccessToken);
                return new ShopInfoResponseV2
                {
                    Email = shop.Data.Email,
                    Name = shop.Data.Name,
                    IdentityKey = shop.Data.Id.ToString()
                };
            }
            if (auth == null || string.IsNullOrEmpty(auth.Code))
            {
                throw new OmniException("Missing Authorization Code");
            }
            var tokenItem = await CreateToken(auth.Code, null, null);
            if (tokenItem == null || string.IsNullOrEmpty(tokenItem.AccessToken))
            {
                throw new OmniException("Access Token invalid");
            }
            var response = await Policy
                    .HandleResult<IRestResponse<SellerInfoV2Response>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                        (res, timeSpan, retryCount, ct) =>
                        {
                            Log.Warn($"Request Get Tiki seller info failed with {res.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");

                        }).ExecuteAsync(() => TikiHelperV2.GetSellerInfo(tokenItem.AccessToken));

            if (response?.Data == null)
            {
                Log.Error($"{KeywordCommonSearch.CallTikiError} Get Tiki seller info: {new { Response = response?.Content }.ToJson()}");
                return null;
            }

            if (response.Data.Errors != null && response.Data.Errors.Any())
            {
                var errorMessage = response.Data.Errors.Join("; ");
                Log.Error($"{KeywordCommonSearch.CallTikiError} Get Tiki seller info: {new { ErrorMessages = errorMessage }.ToJson()}");
                throw new OmniException(errorMessage);
            }

            long authId = await _channelInternalClient.CreateChannelAuthAsync(context, new ChannelAuthRequest
            {
                AccessToken = tokenItem.AccessToken,
                RefreshToken = tokenItem.RefreshToken,
                ExpiresIn = tokenItem.ExpiresIn,
                RefreshExpiresIn = tokenItem.RefreshExpiresIn
            });

            return new ShopInfoResponseV2
            {
                AuthId = authId,
                Email = response.Data.Email,
                Name = response.Data.Name,
                IdentityKey = response.Data.Id.ToString(),
                Status = ConvertSellerStatus(response.Data.RegistrationStatus),
                IsActive = response.Data.Active == 1,
            };
        }

        public async Task<ProductListResponse> GetListProduct(ChannelAuth auth, long channelId, Guid logId,
            bool isFirstSync, DateTime? updateTimeFrom = null, KvInternalContext kvContext = null,
            Platform platform = null)
        {
            var getGetProductsLog = new LogObject(Log, logId)
            {
                Action = "TikiGetListProduct",
                OmniChannelId = channelId,
                RequestObject = $"LastSync: {updateTimeFrom}"
            };

            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                getGetProductsLog.ResponseObject = $"Auth:{auth.ToSafeJson()} EmptyAccessToken";
                getGetProductsLog.LogError();
                throw new OmniException("EmptyAccessToken");
            }
            
            var result = new ProductListResponse();
            var pageNo = 1;
            var totalItem = SelfAppConfig.TikiProductPageSize;

            while (totalItem >= SelfAppConfig.TikiProductPageSize)
            {
                var response = await Policy
                    .HandleResult<IRestResponse<TikiProductsV2ListResponse>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout || (int)msg.StatusCode == TikiTooManyRequests)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                        (res, timeSpan, retryCount) =>
                        {
                            Log.Warn(
                                $"Request failed with {res.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                        }).ExecuteAsync(() => TikiHelperV2.GetProducts(auth.AccessToken, pageNo, SelfAppConfig.TikiProductPageSize, updateTimeFrom));

                totalItem = response?.Data?.Products?.Count ?? 0;

                if (totalItem > 0 && response?.Data?.Products != null)
                {
                    var products = response.Data.Products.Where(x => (x.Inventory?.InventoryType == null || x.Inventory?.InventoryType.ToLower() != "instock")
                    && (x.Inventory?.FulfillmentType == null || x.Inventory?.FulfillmentType.ToLower() != "seller_delivery"))
                    .Select(x => new Product
                    {
                        ItemId = x.ProductId.ToString(),
                        BasePrice = x.Price,
                        SalePrice = x.Price,
                        ItemName = x.Name,
                        ItemSku = x.OriginalSku,
                        Status = x.Active == 1 ? "active" : "inactive",
                        Type = (byte)ChannelProductType.Normal,
                        ItemImages = string.IsNullOrEmpty(x.Thumbnail) ? new List<string>() : new List<string> { x.Thumbnail },
                        ParentItemId = x.MasterId.ToString(),
                        SuperId = x.SuperId,
                    });
                    result.Data.AddRange(products);

                    if (!isFirstSync)
                    {
                        var removedProductIds = response.Data.Products.Where(x => x.Inventory?.InventoryType?.ToLower() == "instock" || x.Inventory?.FulfillmentType?.ToLower() == "seller_delivery").Select(x => x.ProductId.ToString());
                        result.RemovedProductIds.AddRange(removedProductIds);
                    }
                }

                pageNo++;
            }
            
            getGetProductsLog.ResponseObject = $"ProductIds: {result.Data.Select(x => x.ItemId).ToSafeJson()}";
            getGetProductsLog.LogInfo();

            result.Total = result.Data.Count() + result.RemovedProductIds.Count();
            return result;
        }

        public async Task<(bool, string)> SyncOnHand(
            int retailerId, string kvProductSku, long channelId, Guid logId,
            ChannelAuth auth, string itemId, string itemSku, double onHand, byte productType,
            LogObjectMicrosoftExtension loggerExtension, string parentProductId = null, Platform platform = null)
        {
            loggerExtension.Action = string.Format(EcommerceTypeException.SyncOnHand, Ecommerce.Tiki);
            loggerExtension.RequestObject = $"ProductId: {itemId}, Onhand: {onHand}";

            try
            {
                if (string.IsNullOrEmpty(auth.AccessToken))
                {
                    var ex = new Exception(string.Format(EcommerceTypeException.EmptyAccessToken, Ecommerce.Tiki));
                    loggerExtension.ResponseObject = $"Auth:{auth.ToSafeJson()} EmptyAccessToken";
                    loggerExtension.LogError(ex);
                    throw ex;
                }

                var products = new List<Product>();
                var result = await UpdateProductStock(auth.AccessToken, itemId, onHand, null, loggerExtension);

                if (result.Data?.Errors?.Any() == true)
                {
                    var errorMsg = result.Data.Errors.Join("; ");
                    var exCustom = new TikiException(errorMsg);
                    loggerExtension.LogError(exCustom);
                    var res = new { Message = errorMsg };
                    return (false, res.ToSafeJson());
                }

                if (result.StatusCode != HttpStatusCode.OK)
                {
                    var res = new { Message = $"{ConnectErrorMsg} ({result.StatusCode})" };
                    return (false, res.ToSafeJson());
                }

                products.Add(new Product
                {
                    ItemId = itemId,
                    OnHand = (int)onHand
                });
                return (true, products.ToSafeJson());
            }
            catch (Exception ex)
            {
                loggerExtension.LogError(ex);
                throw;
            }
        }

        public async Task<(bool, string)> SyncMultiOnHand(
            int retailerId, long channelId, Guid logId, ChannelAuth auth,
            List<MultiProductItem> productItems, LogObjectMicrosoftExtension loggerExtension,
            byte productType, Platform platform)
        {
            var logSyncMultiOnHand = loggerExtension.Clone(string.Format(EcommerceTypeException.SyncMultiOnHand, Ecommerce.Tiki));
            logSyncMultiOnHand.RequestObject = $"(ProductId, Onhand): {productItems.Select(x => $"({x.ItemId}, {x.OnHand})").Join("; ")}";

            try
            {
                if (string.IsNullOrEmpty(auth.AccessToken))
                {
                    var ex = new Exception(string.Format(EcommerceTypeException.EmptyAccessToken, Ecommerce.Tiki));
                    logSyncMultiOnHand.ResponseObject = $"Auth:{auth.ToSafeJson()} EmptyAccessToken";
                    logSyncMultiOnHand.LogError(ex);
                    throw ex;
                }

                var products = new List<Product>();
                foreach (var item in productItems)
                {
                    var result = await UpdateProductStock(auth.AccessToken, item.ItemId, item.OnHand, null, loggerExtension);

                    if (result.StatusCode != HttpStatusCode.OK || result.Data?.Errors?.Any() == true)
                    {
                        if (result.StatusCode == HttpStatusCode.BadRequest || result.Data?.Errors == null || !result.Data.Errors.Any())
                        {
                            products.Add(new Product
                            {
                                ItemId = item.ItemId,
                                ErrorMessage = $"{ConnectErrorMsg} ({result.StatusCode})"
                            });
                        }
                        else
                        {
                            var errorMsg = result.Data.Errors.Join("; ");
                            if (!string.IsNullOrEmpty(errorMsg))
                            {
                                var exCustom = new TikiException(errorMsg);
                                loggerExtension.LogError(exCustom);
                            }
                            products.Add(new Product
                            {
                                ItemId = item.ItemId,
                                ErrorMessage = errorMsg
                            });
                        }
                    }
                    else
                    {
                        products.Add(new Product
                        {
                            ItemId = item.ItemId,
                            OnHand = (int)item.OnHand
                        });
                    }
                    
                }

                logSyncMultiOnHand.ResponseObject = products;
                logSyncMultiOnHand.LogInfo();

                var res = (new { Product = products });
                return (true, res.ToSafeJson());
            }
            catch (Exception ex)
            {
                logSyncMultiOnHand.LogError(ex);
                throw;
            }
        }

        public async Task<(bool, string)> SyncPrice(int retailerId, string kvProductSku, long channelId, Guid logId,
            ChannelAuth auth, string itemId, string itemSku, decimal? basePrice, byte productType, string parentItemId,
            LogObjectMicrosoftExtension loggerExtension, decimal? salePrice = null, DateTime? startSaleDateTime = null,
            DateTime? endSaleDateTime = null, Platform platform= null)
        {
            loggerExtension.Action = string.Format(EcommerceTypeException.SyncPrice, Ecommerce.Tiki);
            loggerExtension.RequestObject = $"ProductId: {itemId}, Price: {basePrice}";

            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                var ex = new OmniException(string.Format(EcommerceTypeException.EmptyAccessToken, Ecommerce.Tiki));
                loggerExtension.LogError(ex);
                throw ex;
            }

            List<Product> products = new List<Product>();
            var result = await UpdateProductTiki(auth.AccessToken, itemId, null, basePrice, loggerExtension);


            if (result.StatusCode != HttpStatusCode.OK || (result?.Data?.Errors != null && result.Data.Errors.Any()))
            {
                if (result.StatusCode == HttpStatusCode.BadRequest || result?.Data?.Errors == null || !result.Data.Errors.Any())
                {
                    var res = new { Message = $"{ConnectErrorMsg} ({result.StatusCode})" };
                    return (false, res.ToSafeJson());
                }
                else
                {
                    var errorMsg = result.Data.Errors.Join("; ");
                    var res = new { Message = errorMsg };
                    return (false, res.ToSafeJson());
                }
            }

            products.Add(new Product
            {
                ItemId = itemId,
                BasePrice = basePrice ?? 0
            });
            return (true, products.ToSafeJson());
        }

        public async Task<(bool, string, int?)> SyncMultiPrice(int retailerId, long channelId, Guid logId,
            ChannelAuth auth, List<MultiProductItem> productItems, byte productType,
            LogObjectMicrosoftExtension loggerExtension,
            Platform platform)
        {
            loggerExtension.Action = string.Format(EcommerceTypeException.SyncMultiPrice, Ecommerce.Tiki);

            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                var ex = new OmniException(string.Format(EcommerceTypeException.EmptyAccessToken, Ecommerce.Tiki));
                loggerExtension.LogError(ex);
                throw ex;
            }
            

            List<Product> products = new List<Product>();
            foreach (var item in productItems)
            {
                loggerExtension.Action = string.Format(EcommerceTypeException.SyncMultiPriceSyncItem, Ecommerce.Tiki);
                var result = await UpdateProductTiki(auth.AccessToken, item.ItemId, null, item.Price, loggerExtension);

                if (result.StatusCode != HttpStatusCode.OK || (result?.Data?.Errors != null && result.Data.Errors.Any()))
                {
                    if (result.StatusCode == HttpStatusCode.BadRequest || result?.Data?.Errors == null || !result.Data.Errors.Any())
                    {
                        products.Add(new Product
                        {
                            ItemId = item.ItemId,
                            ErrorMessage = $"{ConnectErrorMsg} ({result.StatusCode})"
                        });
                        var ex = new TikiException($"{ConnectErrorMsg} ({result.StatusCode})");
                        loggerExtension.LogError(ex);
                    }
                    else
                    {
                        products.Add(new Product
                        {
                            ItemId = item.ItemId,
                            ErrorMessage = result.Data.Errors.Join("; ")
                        });
                    }
                }
                else
                {
                    products.Add(new Product
                    {
                        ItemId = item.ItemId,
                        BasePrice = item.Price ?? 0,
                    });
                }
            }

            if (products.Any(x => !string.IsNullOrEmpty(x.ErrorMessage)))
            {
                var msgErr = products.Where(x => !string.IsNullOrEmpty(x.ErrorMessage)).Join(";");
                var ex = new TikiException(msgErr);
                loggerExtension.LogError(ex);
            }

            if (products.Any(x => string.IsNullOrEmpty(x.ErrorMessage)))
            {
                loggerExtension.Description = "Product map after update product success";
                loggerExtension.ResponseObject = products.Where(x => string.IsNullOrEmpty(x.ErrorMessage)).ToList();
                loggerExtension.LogInfo();

                loggerExtension.Description = null;
                loggerExtension.ResponseObject = null;
            }

            var restultRes = (new { Product = products });
            return (true, restultRes.ToSafeJson(), null);
        }

        public async Task<ProductDetailResponse> GetProductDetail(ChannelAuth auth, long channelId, Guid logId, object productId, object parentProductId, string productSku, Platform platform)
        {
            var getGetProductDetailLog = new LogObject(Log, logId)
            {
                Action = "TikiGetProductDetail",
                OmniChannelId = channelId,
                RequestObject = $"ProductId: {productId} Sku:{productSku}"
            };

            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                getGetProductDetailLog.ResponseObject = $"Auth:{auth.ToSafeJson()} EmptyAccessToken";
                getGetProductDetailLog.LogError();
                throw new OmniException("EmptyAccessToken");
            }
            
            var response = await Policy
                 .HandleResult<IRestResponse<TikiProductDetailV2>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout || (int)msg.StatusCode == TikiTooManyRequests)
                 .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                     (res, timeSpan, retryCount, context) =>
                     {
                         Log.Warn(
                             $"Request failed with {res.Result.StatusCode} - Channel {channelId}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                     }).ExecuteAsync(() => TikiHelperV2.GetProductDetail(auth.AccessToken, DataRequestHelper.ParseItemIdToLong(productId.ToString())));

            if (response?.Data == null)
            {
                return null;
            }
            var itemId = response.Data.ProductId;
            var productDetails = new List<ProductDetailSkus>
            {
                new ProductDetailSkus
                {
                    Sku = response.Data.Sku,
                    Name = response.Data.Name,
                    Images = response.Data.Images?.Select(y => y.Url).ToList(),
                    Status = "active",
                    Description = response.Data.Attributes?.Description
                }
            };

            var result = new ProductDetailResponse
            {
                ItemId = itemId.ToString(),
                ProductDetails = productDetails
            };
            return result;
        }

        public async Task<(bool, List<CreateOrderRequest>)> GetOrders(ChannelAuth auth, OrderRequest request,
            KvInternalContext context, LogObjectMicrosoftExtension logInfo, Platform platform)
        {
            try
            {   
                var result = new List<CreateOrderRequest>();
                var pageNo = 1;
                var pageSize = SelfAppConfig.TikiOrderPageSize; //Tiki max 50
                var totalItem = pageSize;

                if (request.LastSync.GetValueOrDefault() == default(DateTime))
                {
                    request.LastSync = DataRequestHelper.GetOrderLastSyncDateTime(request.SyncOrderFormula.GetValueOrDefault());
                }

                if (string.IsNullOrEmpty(auth.AccessToken))
                {
                    var ex = new OmniException($"Auth:{auth.ToSafeJson()} EmptyAccessToken");
                    logInfo.LogError(ex);
                    throw ex;
                }

                var errorMessage = new StringBuilder();
                var allocateLog = new StringBuilder();
                while (totalItem >= pageSize)
                {
                    var response = await Policy.HandleResult<IRestResponse<TikiOrderV2Response>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout || (int)msg.StatusCode == TikiTooManyRequests)
                        .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval), (res, timeSpan, retryCount, ct) =>
                        {
                            if (res?.Result?.Data?.Errors?.Any() == true)
                            {
                                var msgErr = $"statusCode: {res.Result.StatusCode} - error: {res.Result.Data.Errors.Join(",")}";
                                var ex = new TikiException(msgErr);
                                logInfo.LogError(ex);
                            }
                            else if (res?.Exception != null)
                            {
                                logInfo.LogError(res.Exception);
                            }
                            else
                            {
                                var msg =
                                    $"Request failed with {res?.Result?.StatusCode} - ChannelId {request.ChannelId}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}";
                                var ex = new OmniException(msg);
                                logInfo.LogError(ex);
                            }
                        }).ExecuteAsync(() => TikiHelperV2.GetOrders(auth.AccessToken, pageNo, pageSize, request.LastSync, logInfo));

                    if (response?.Data?.Errors?.Any() == true)
                    {
                        errorMessage.Append($"PageIndex: {pageNo}, statusCode: {response.StatusCode}, Messages: {response.Data.Errors.Join("; ")} - ");
                    }

                    totalItem = response?.Data?.Orders?.Count ?? 0;

                    if (totalItem > 0 && response?.Data?.Orders != null)
                    {
                        foreach (var o in response.Data.Orders)
                        {
                            var isSellerDelivery = !string.IsNullOrEmpty(o.FulfillmentType) && o.FulfillmentType.ToLower() == "seller_delivery";
                            var isInstock = o.Items.Any(x => !string.IsNullOrEmpty(x.Product.InventoryType) && x.Product.InventoryType.ToLower() == "instock");
                            var isPendingOrder =
                                !string.IsNullOrEmpty(o.Status) && o.Status.ToLower() == TikiStatus.WaitingPayment ||
                                o.Status.ToLower() == TikiStatus.Paid || o.Status.ToLower() == TikiStatus.Processing;

                            if ((isSellerDelivery && isInstock) || isPendingOrder)
                            {
                                allocateLog.Append($"Skip {o.Code} - ");
                            }
                            else
                            {
                                result.Add(new CreateOrderRequest
                                {
                                    OrderId = o.Code,
                                    OrderStatus = o.Status,
                                    StatusHistories = o.StatusHistories,
                                    CreateDate = o.CreatedAt,
                                    UpdateAt = o.UpdatedAt
                                });
                            }
                        }
                    }

                    var totalPage = response?.Data?.Paging?.LastPage ?? 0;
                    if (totalItem < pageSize && pageNo < totalPage)
                    {
                        totalItem = pageSize;
                    }

                    pageNo++;
                }
                var errorMessageStr = errorMessage.ToString();
                if (!string.IsNullOrEmpty(errorMessageStr))
                {
                    logInfo.Description = errorMessageStr;
                }

                if (!result.Any()) return (false, null);

                var filterOrder = result.Where(x => !IsExceptOrder(x.StatusHistories, request.LastSync)).ToList();

                logInfo.ResponseObject =
                    $"OrderIds: {filterOrder.Select(p => p.OrderId).ToSafeJson()}; " +
                    $"OrderIds except {result.Select(x => x.OrderId).Except(filterOrder.Select(y => y.OrderId)).ToSafeJson()} " +
                    $"because status done before lastsync {request.LastSync}";
                logInfo.LogInfo();

                return (true, filterOrder);
            }
            catch (Exception ex)
            {
                logInfo.LogError(ex);
                throw;
            }
        }

        public Task<OrderDetailGetResponse> GetOrderDetail(int retailerId, long channelId, ChannelAuth auth, 
            CreateOrderRequest request, LogObjectMicrosoftExtension logInfo,
            Platform platform, OmniChannelSettingObject settings = null)
        {
            throw new NotImplementedException();
        }

        public async Task<(bool, bool, bool, List<KvInvoice>)> GetInvoiceDetail(int retailerId, long channelId, Guid logId,
            ChannelAuth auth, CreateInvoiceRequest request, 
            LogObjectMicrosoftExtension loggerExtension,
            Platform platform,
            OmniChannelSettingObject settings)
        {
            try
            {
                if (request.KvOrder == null)
                {
                    return (false, false, false, null);
                }

                var kvOrder = request.KvOrder;
                if (request.KvDelivery == null)
                {
                    return (false, false, false, null);
                }
                var delivery = request.KvDelivery;
                loggerExtension.Action = string.Format(EcommerceTypeException.GetInvoiceDetail, Ecommerce.Tiki);
                loggerExtension.Description = "Tiki client v2";

                if (string.IsNullOrEmpty(auth.AccessToken))
                {
                    var ex = new Exception(string.Format(EcommerceTypeException.EmptyAccessToken, Ecommerce.Tiki));
                    loggerExtension.ResponseObject = $"Auth:{auth.ToSafeJson()} EmptyAccessToken";
                    loggerExtension.LogError(ex);
                    throw ex;
                }

                var orderDetail = await GetOrderDetail(auth.AccessToken, request.OrderId, loggerExtension);

                if (orderDetail == null || !orderDetail.Items.Any() || (orderDetail.Errors != null && orderDetail.Errors.Any()))
                {
                    return (false, false, true, null);
                }

                if (kvOrder.Status != (int)OrderState.Void && (orderDetail.Status == TikiStatus.Queueing || orderDetail.Status == TikiStatus.Processing
                                    || orderDetail.Status == TikiStatus.WaitingPayment || orderDetail.Status == TikiStatus.Paid
                                    || orderDetail.Status == TikiStatus.SellerConfirmed || orderDetail.Status == TikiStatus.SellerCanceled))
                {
                    return (false, false, false, null);
                }

                if (kvOrder.Status != (int)OrderState.Void && kvOrder.Status != (int)OrderState.Finalized && orderDetail.Status == TikiStatus.Canceled)
                {
                    return (true, true, false, null);
                }

                var deliveryStatus = UpdateDeliveryStatusForInvoice(orderDetail.Status);
                var invoiceStatus = ConvertToKvInvoiceStatus(orderDetail.Status);

                var invoices = new List<KvInvoice>();

                var invoice = new KvInvoice
                {
                    Code = $"HDTIKI_{request.OrderId}",
                    RetailerId = kvOrder.RetailerId,
                    OrderId = kvOrder.Id,
                    BranchId = kvOrder.BranchId,
                    CustomerId = kvOrder.CustomerId,
                    Description = $"Hóa đơn từ Tiki: {request.OrderId}",
                    UsingCod = true,
                    PriceBookId = kvOrder.Extra?.FromJson<Extra>().PriceBookId?.Id ?? 0,
                    DeliveryDetail = new InvoiceDelivery
                    {
                        Receiver = delivery.Receiver,
                        ContactNumber = delivery.ContactNumber,
                        Address = delivery.Address,
                        UsingPriceCod = true,
                        WardName = delivery.WardName,
                        LocationName = delivery.LocationName,
                        Status = deliveryStatus ?? (byte)DeliveryStatus.Pending,
                        DeliveryBy = delivery.PartnerId,
                        LatestStatus = deliveryStatus ?? (byte)DeliveryStatus.Pending,
                        Price = kvOrder.OrderDelivery?.Price,
                        FeeJson = kvOrder.OrderDelivery?.FeeJson,
                    },
                    Status = invoiceStatus,
                    InvoiceDetails = new List<InvoiceDetail>(),
                    NewStatus = invoiceStatus,
                    DeliveryStatus = deliveryStatus
                };

                foreach (var detail in orderDetail.Items)
                {
                    decimal discount = 0;
                    if (detail.Invoice?.DiscountAmount != null && detail.Invoice?.Quantity != 0)
                    {
                        discount = (detail.Invoice.DiscountAmount - detail.Invoice.DiscountTikixu) / (decimal)detail.Invoice.Quantity;
                    }

                    invoice.InvoiceDetails.Add(new InvoiceDetail
                    {
                        ProductChannelId = detail.Product?.Id.ToString(),
                        ProductChannelSku = detail.Product?.Sku,
                        ProductChannelName = detail.Product?.Name,
                        Quantity = detail.Invoice?.Quantity ?? 0,
                        ProductName = detail.Product?.Name,
                        Price = detail.Invoice?.Price ?? 0,
                        Discount = discount,
                        DiscountPrice = (float)discount
                    });
                }
                invoices.Add(invoice);
                return (true, false, false, invoices);
            }
            catch (Exception ex)
            {
                loggerExtension.LogError(ex);
                throw;
            }
        }

        public async Task<(byte?, string, string)> GetLogisticsStatus(int retailerId, long channelId, LogObjectMicrosoftExtension logInfo,
            string orderId, ChannelAuth auth, bool isGetOrderStatus = false, Platform platform = null)
        {
            (byte?, string, string) result = (null, null, null);
            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                Log.Error($"{KeywordCommonSearch.CallTikiError} EmptyAccessToken TikiGetLogisticsStatus - Context: {new ExceptionContextModel {RetailerId = retailerId, ChannelId = channelId, OrderIds = new List<object> {orderId}, Auth = auth, LogId = logInfo.Id}.ToJson()}");
                throw new OmniException("EmptyAccessToken");
            }

            var orderDetail = await GetOrderDetailLoggerOld(auth.AccessToken, orderId);
            if (orderDetail != null && (orderDetail.Errors == null || !orderDetail.Errors.Any()))
            {
                result.Item1 = UpdateDeliveryStatusForInvoice(orderDetail.Status);
                result.Item2 = orderDetail.Status;
            }

            return result;
        }

        private byte? ConvertToKvOrderStatus(string tikiStatus)
        {
            switch (tikiStatus)
            {
                case TikiStatus.Queueing:
                case TikiStatus.Processing:
                case TikiStatus.WaitingPayment:
                case TikiStatus.Paid:
                case TikiStatus.SellerConfirmed:
                case TikiStatus.SellerCanceled:
                    return (byte)OrderState.Pending;

                case TikiStatus.Picking:
                case TikiStatus.Packaging:
                case TikiStatus.FinishedPacking:
                case TikiStatus.ReadyToShip:
                case TikiStatus.HandoverToPartner:
                case TikiStatus.SuccessfulDelivery:
                case TikiStatus.Returned:
                case TikiStatus.Complete:
                    return (byte)OrderState.Finalized;

                case TikiStatus.Canceled:
                    return (byte)OrderState.Void;
            }
            return null;
        }

        private async Task<IRestResponse<UpdateProductTikiResponse>> UpdateProductTiki(
            string tikiApi, string productId,
            double? quantity, decimal? price,
            LogObjectMicrosoftExtension loggerExtension)
        {
            try
            {
                var response = await Policy
                                    .HandleResult<IRestResponse<UpdateProductTikiResponse>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout || (int)msg.StatusCode == TikiTooManyRequests)
                                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                                        (res, timeSpan, retryCount, context) =>
                                        {
                                            if (retryCount > 1)
                                            {
                                                loggerExtension.RequestObject = null;
                                            }

                                            if (res.Result?.Data?.Errors?.Any() == true)
                                            {
                                                var msgErr = $"statusCode: {res.Result.StatusCode} - error: {res.Result.Data.Errors.Join(",")}";
                                                var ex = new TikiException(msgErr);
                                                loggerExtension.LogError(ex);
                                            }
                                            else if (res.Exception != null)
                                            {
                                                loggerExtension.LogError(res.Exception);
                                            }
                                            else if (res.Result?.ErrorException != null)
                                            {
                                                loggerExtension.LogError(res.Result.ErrorException);
                                            }
                                            else
                                            {
                                                var messageErr =
                                                    $"Request update product failed with {res.Result?.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}";
                                                var exCustom = new TikiException(messageErr);
                                                loggerExtension.LogError(exCustom);
                                            }

                                        }).ExecuteAsync(() => TikiHelper.UpdateProduct(tikiApi, DataRequestHelper.ParseItemIdToLong(productId), quantity, price, loggerExtension));

                return response;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        private async Task<IRestResponse<UpdateProductTikiResponse>> UpdateProductStock(
            string tikiApi, string productId,
            double? quantity, decimal? price,
            LogObjectMicrosoftExtension loggerExtension)
        {
            var logSyncMultiOnHandSyncItem =
                loggerExtension.Clone(string.Format(EcommerceTypeException.SyncMultiOnHandSyncItem,
                    Ecommerce.Tiki));
            try
            {
                logSyncMultiOnHandSyncItem.RequestObject = $"itemId: {productId}, OnHand: {quantity}";
                var response = await Policy
                    .HandleResult<IRestResponse<UpdateProductTikiResponse>>(msg =>
                        msg.StatusCode == HttpStatusCode.RequestTimeout || (int)msg.StatusCode == TikiTooManyRequests)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry,
                        i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                        (res, timeSpan, retryCount, context) =>
                        {
                            if (res?.Result?.Data?.Errors?.Any() == true)
                            {
                                var msgErr =
                                    $"statusCode: {res.Result.StatusCode} - error: {res.Result.Data.Errors.Join(",")}";
                                var ex = new TikiException(msgErr);
                                loggerExtension.LogError(ex);
                            }
                            else if (res?.Exception != null)
                            {
                                loggerExtension.LogError(res.Exception);
                            }
                            else if (res?.Result?.ErrorException != null)
                            {
                                loggerExtension.LogError(res.Result.ErrorException);
                            }
                            else
                            {
                                var msg =
                                    $"Request update product failed with {res?.Result?.StatusCode}. UpdateProductStock. Waiting {timeSpan} before next retry. Retry attempt {retryCount}";
                                var ex = new OmniException(msg);
                                loggerExtension.LogError(ex);
                            }
                        }).ExecuteAsync(() => TikiHelper.UpdateProductStock(tikiApi,
                        DataRequestHelper.ParseItemIdToLong(productId), quantity, price, loggerExtension));
                logSyncMultiOnHandSyncItem.ResponseObject = response?.Content;
                logSyncMultiOnHandSyncItem.LogInfo();

                return response;
            }
            catch (Exception ex)
            {
                logSyncMultiOnHandSyncItem.LogError(ex);
                throw;
            }
        }

        public async Task<RefreshTokenResponse> RefreshAccessToken(string refreshToken, long channelId, Guid logId,
            ChannelAuth auth, Platform platform)
        {
            var tikiCallBack = SelfAppConfig.TikiCallBack;
            var tikiClientId = SelfAppConfig.TikiClientAppId;
            var tikiApiSercetKey = SelfAppConfig.TikiApiSercetKey;
            var refeshTokenLog = new LogObject(Log, logId)
            {
                Action = "TikiRefreshAccessToken",
                RequestObject = $"TikiClientAppId: {tikiClientId}, TikiApiSercetKey: {tikiApiSercetKey}, TikiCallBack: {tikiCallBack}, RefreshToken: {refreshToken}",
                OmniChannelId = channelId

            };
            var response = await TikiHelperV2.RefreshToken(refreshToken, tikiCallBack, tikiClientId, tikiApiSercetKey);

            var newToken = response?.Data;
            if (newToken == null || string.IsNullOrEmpty(newToken.AccessToken) || response.StatusCode != HttpStatusCode.OK)
            {
                var refreshError = string.IsNullOrEmpty(response?.Content) ? null : JsonConvert.DeserializeObject<TikiRefeshTokenError>(response.Content);
                if (response?.StatusCode == HttpStatusCode.Unauthorized || response?.StatusCode == HttpStatusCode.Forbidden || refreshError?.TikiErrorDebug == TikiRefreshTokenExpired || refreshError?.TikiErrorDebug == TikiRefreshTokenNotFound)
                {
                    refeshTokenLog.Description = "Response StatusCode: " + response?.StatusCode;
                    refeshTokenLog.LogError();

                    return new RefreshTokenResponse
                    {
                        IsDeactivateChannel = true
                    };
                }

                refeshTokenLog.Description = $"TikiRefreshAccessToken failed - StatusCode {response?.StatusCode}";
                refeshTokenLog.ResponseObject = response?.Content;
                refeshTokenLog.LogError();
                return null;
            }

            var tokenResponse = new RefreshTokenResponse
            {
                AccessToken = newToken.AccessToken,
                RefreshExpiresIn = newToken.RefreshExpiresIn,
                ExpiresIn = newToken.ExpiresIn,
                RefreshToken = newToken.RefreshToken,
                SellerId = newToken.ShopId.ToString()
            };

            refeshTokenLog.Description = "Tiki refresh token successful";
            refeshTokenLog.LogInfo();

            return tokenResponse;
        }

        private async Task<TikiOrderV2> GetOrderDetail(string accessToken, string orderId, LogObjectMicrosoftExtension logInfo)
        {
            try
            {
                var response = await Policy.HandleResult<IRestResponse<TikiOrderV2>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout || (int)msg.StatusCode == TikiTooManyRequests)
                        .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval), (res, timeSpan, retryCount, context) =>
                        {
                            if (res?.Result?.Data?.Errors?.Any() == true)
                            {
                                var msgErr = $"statusCode: {res.Result.StatusCode} - error: {res.Result.Data.Errors.Join(",")}";
                                var ex = new TikiException(msgErr);
                                logInfo.LogError(ex);
                            }
                            else if (res?.Exception != null)
                            {
                                logInfo.LogError(res.Exception);
                            }
                            else
                            {
                                var msg =
                                    $"Request failed with {res?.Result?.StatusCode}. GetOrdersDetail. Waiting {timeSpan} before next retry. Retry attempt {retryCount}";
                                var ex = new OmniException(msg);
                                logInfo.LogError(ex);
                            }
                        }).ExecuteAsync(() => TikiHelperV2.GetOrdersDetail(accessToken, orderId, logInfo));

                if (response?.Data?.Errors != null && response.Data.Errors.Any())
                {
                    var errorMessage = $"StatusCode: {response.StatusCode}, Messages: {response.Data.Errors.Join("; ")}";
                    var ex = new TikiException(errorMessage);
                    logInfo.LogError(ex);
                    return null;
                }

                return response?.Data;
            }
            catch (Exception ex)
            {
                logInfo.LogError(ex);
                throw;
            }
        }

        private async Task<TikiOrderV2> GetOrderDetailLoggerOld(string accessToken, string orderId)
        {
            var orderDetail = await Policy.HandleResult<IRestResponse<TikiOrderV2>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout || (int)msg.StatusCode == TikiTooManyRequests)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval), (res, timeSpan, retryCount, context) =>
                {
                    Log.Warn(
                        $"Request failed with {res.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                }).ExecuteAsync(() => TikiHelperV2.GetOrdersDetailLoggerOld(accessToken, orderId));

            return orderDetail?.Data;
        }

        private byte? UpdateDeliveryStatusForInvoice(string tikiStatus)
        {
            switch (tikiStatus)
            {
                case TikiStatus.Picking:
                    return (byte)DeliveryStatus.InPickup;

                case TikiStatus.Packaging:
                case TikiStatus.FinishedPacking:
                case TikiStatus.ReadyToShip:
                    return (byte)DeliveryStatus.Pickuped;

                case TikiStatus.HandoverToPartner:
                case TikiStatus.Shipping:
                    return (byte)DeliveryStatus.Delivering;

                case TikiStatus.SuccessfulDelivery:
                case TikiStatus.Complete:
                    return (byte)DeliveryStatus.Delivered;

                case TikiStatus.Returned:
                case TikiStatus.Canceled:
                    return (byte)DeliveryStatus.Returning;
            }
            return null;
        }

        private byte ConvertToKvInvoiceStatus(string tikiStatus)
        {
            switch (tikiStatus)
            {
                //case TikiStatus.Returned:
                //case TikiStatus.Canceled:
                case TikiStatus.Complete:
                    return (byte)InvoiceState.Issued;

            }
            return (byte)InvoiceState.Pending;
        }

        private byte ConvertSellerStatus(string registrationStatus)
        {
            switch (registrationStatus)
            {
                case TikiSellerStatus.Completed:
                case null:
                    return (byte)ChannelStatus.Approved;

                case TikiSellerStatus.KamRejected:
                    return (byte)ChannelStatus.Reject;
            }
            return (byte)ChannelStatus.Pending;
        }

        /// <summary>
        /// Loại bỏ các đơn hàng hoàn thành sau thời điểm lastsync 
        /// </summary>
        /// <param name="statusHistories">Lịch sử cập nhập trạng thái đơn trên sàn</param>
        /// <param name="lastSync">Đồng bộ đơn hàng sau thời điểm thiết lập</param>
        /// <returns></returns>
        private bool IsExceptOrder(List<StatusHistory> statusHistories, DateTime? lastSync)
        {
            if (statusHistories == null || !statusHistories.Any()) return false;
            var historyOrderDone =
                statusHistories.FirstOrDefault(x => x.Status == TikiStatus.Complete || x.Status == TikiStatus.Returned);
            if (historyOrderDone?.CreatedAt == null || lastSync == null) return false;
            return historyOrderDone.CreatedAt.Value.ToUniversalTime() < lastSync.Value.ToUniversalTime();
        }

        public Task<List<ShopeeAttribute>> GetAttributeByShopId(int retailerId, long channelId, ChannelAuth auth,
            Guid logId, long categoryId, Platform platform)
        {
            throw new NotImplementedException();
        }

        public Task<ShopeeProductResponse> PushProductToChannel(int retailerId, long channelId, ChannelAuth auth,
            Guid logId, ShopeeProduct req, Platform platform, string kvProductAttributeStr)
        {
            throw new NotImplementedException();
        }

        public Task<CategoriesResp> GetCategories(int retailerId, long channelId, ChannelAuth auth, Guid logId,
            Platform platform)
        {
            throw new NotImplementedException();
        }

        public Task<ShopeeUploadImgResponse> UploadImg(ChannelAuth auth, int retailerId, long shopId,
            List<Image> images, Guid logId, Platform platform)
        {
            throw new NotImplementedException();
        }
        public Task<ProductListResponse> GetListDeletedProduct(ChannelAuth auth, long channelId, Guid logId,
            bool isFirstSync, KvInternalContext context = null, DateTime? updateTimeFrom = null)
        {
            throw new NotImplementedException();
        }

        
        Task<List<OrderDetail>> IBaseClient.GetListOrderDetail(ChannelAuth auth, string[] orderSnList, LogObjectMicrosoftExtension logInfo, Platform platform)
        { throw new NotImplementedException(); }
        public Task<KvOrder> GenListKvOrder(long shopId, int? saleChannelId, int branchId, long userId, int retailerId, OrderDetail shopeeOrder, OmniChannelSettingObject settings, LogObjectMicrosoftExtension logInfo, Platform platform)
        {
            throw new NotImplementedException();
        }

        public List<KvInvoice> GenKvInvoices(CreateInvoiceRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<(bool, List<Transaction>)> GetListTransaction(ChannelAuth auth, DateTime createFrom, DateTime createTo, LogObjectMicrosoftExtension log, Platform platform)
        {
            throw new NotImplementedException();
        }

        public async Task<SellerWareHouseV2Response> GetTikiWarehouse(string accessToken)
        {
            var reponseSellerWareHouse = await Policy
                    .HandleResult<IRestResponse<SellerWareHouseV2Response>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (res, timeSpan, retryCount, ct) => {
                        Log.Warn($"Request Get Tiki seller warehouse failed with {res.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                    }).ExecuteAsync(() => TikiHelperV2.GetSellerWarehouse(accessToken));

            if (reponseSellerWareHouse?.Data == null)
            {
                Log.Error($"{KeywordCommonSearch.CallTikiError} Get Tiki warehouse info: {new { Response = reponseSellerWareHouse?.Content }.ToJson()}");
                return null;
            }

            if (reponseSellerWareHouse.Data.Errors != null && reponseSellerWareHouse.Data.Errors.Any())
            {
                var errorMessage = reponseSellerWareHouse.Data.Errors.Join("; ");
                Log.Error($"{KeywordCommonSearch.CallTikiError} Get Tiki warehouse info: {new { ErrorMessages = errorMessage }.ToJson()}");
                throw new OmniException(errorMessage);
            }
            return reponseSellerWareHouse.Data;
        }

        public Task<(bool, List<FinanceTransaction>)> GetFinanceTransactions(string token, long channelId, string orderSn, DateTime? startTime, DateTime? endTime, LogObjectMicrosoftExtension logInfo, bool isBreakIfMaximum)
        {
            throw new NotImplementedException();
        }

        public Task<List<Logistics>> GetLogisticsByShopId(int retailerId, long channelId, ChannelAuth auth, Guid logId, Platform platform)
        {
            throw new NotImplementedException();
        }
    }
}

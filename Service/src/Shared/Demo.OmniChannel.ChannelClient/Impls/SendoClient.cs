﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Helper;
using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.RequestDTO;
using Demo.OmniChannel.ChannelClient.ResponseDTO;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Impls;
using Demo.OmniChannel.Sdk.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Dtos;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.ShareKernel.Models;
using Demo.OmniChannel.Utilities;
using Newtonsoft.Json;
using Polly;
using RestSharp;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Logging;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ChannelProductType = Demo.OmniChannel.ChannelClient.Common.ChannelProductType;
using Image = Demo.OmniChannel.ShareKernel.Dtos.Image;
using Logistics = Demo.OmniChannel.ChannelClient.Models.Logistics;
using Platform = Demo.OmniChannel.ChannelClient.Models.Platform;
using ProductDetailResponse = Demo.OmniChannel.ChannelClient.RequestDTO.ProductDetailResponse;
using SelfAppConfig = Demo.OmniChannel.ChannelClient.Common.SelfAppConfig;
using SendoOrderStatus = Demo.OmniChannel.ShareKernel.Common.SendoOrderStatus;
using TokenResponse = Demo.OmniChannel.ChannelClient.Models.TokenResponse;

namespace Demo.OmniChannel.ChannelClient.Impls
{
    public class SendoClient : IBaseClient, ISendoClient
    {
        private ILog Log => LogManager.GetLogger(typeof(LazadaClient));
        private IChannelInternalClient _channelInternalClient;
        private SelfAppConfig _config;

        public SendoClient(IAppSettings settings)
        {
            _channelInternalClient = new ChannelInternalClient(settings);
            _config = new SelfAppConfig(settings);
        }

        public async Task<SendoLoginResponse> Login(string shopKey, string secretKey)
        {
            // Login
            var loginRes = await SendoHelper.Login(shopKey, secretKey);

            if (loginRes?.Data?.Success == true) return loginRes?.Data?.Result;

            return null;
        }

        public Task<ShopInfoResponseV2> GetShopInfo(ChannelAuth auth, KvInternalContext context,
            bool isOnlyGetInfo = false, Platform platform = null)
        {
            throw new NotImplementedException();
        }

        public Task<TokenResponse> CreateToken(string code, ChannelAuth auth, Platform platform)
        {
            throw new NotImplementedException();
        }

        public async Task<ProductListResponse> GetListProduct(ChannelAuth auth, long channelId, Guid logId,
            bool isFirstSync, DateTime? updateTimeFrom = null, KvInternalContext kvContext = null,
            Platform platform = null)
        {
            var getGetProductLog = new LogObject(Log, logId)
            {
                Action = "SendoGetListProduct",
                OmniChannelId = channelId,
                RequestObject = $"LastSync: {updateTimeFrom}"
            };

            var result = new ProductListResponse();
            var totalItem = SelfAppConfig.SendoProductPageSize;
            var pageToken = "";
            while (totalItem >= SelfAppConfig.SendoProductPageSize)
            {
                var response = await Policy
                    .HandleResult<IRestResponse<SendoProductsListResponse>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                        (res, timeSpan, retryCount) =>
                        {
                            Log.Warn($"Request failed with {res.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                        }).ExecuteAsync(() => SendoHelper.GetProducts(auth.AccessToken, pageToken, SelfAppConfig.SendoProductPageSize, updateTimeFrom, isFirstSync));

                var responseData = response?.Data;
                if (responseData?.Result?.Products == null || responseData.Result?.Products.Count == 0) break;
                if (updateTimeFrom.HasValue)
                {
                    responseData.Result.Products = responseData.Result.Products.Where(x => x.UpdatedAtTimestamp >= updateTimeFrom.Value.AddHours(-1).ToUnixTime()).ToList();
                }

                var products = responseData.Result.Products.Where(x => x.Status == SendoProductStatus.Approved || x.Status == SendoProductStatus.Submitted).ToList();
                var normalProducts = GetNormalProducts(products);
                var parentProducts = products.Where(x => x.IsConfigVariant || x.VariantsLength > 0).ToList();
                var variantProducts = await GetVariantProducts(auth.AccessToken, parentProducts, channelId, logId);

                result.Data.AddRange(normalProducts);
                result.Data.AddRange(variantProducts);

                if (!isFirstSync)
                {
                    var removedProduct = responseData.Result.Products
                        .Where(x => x.Status == SendoProductStatus.Deleted ||
                                    x.Status == SendoProductStatus.Rejected ||
                                    x.Status == SendoProductStatus.Cancelled).ToList();
                    var parentIds = variantProducts.Select(x => x.ParentItemId).Distinct().ToList();
                    result.RemovedProductIds.AddRange(removedProduct.Select(x => x.Id.ToString()).ToList());

                    result.ParentChangeIds.AddRange(parentIds);
                    result.ParentChangeIds.AddRange(removedProduct
                        .Where(x => x.IsConfigVariant || x.VariantsLength > 0).Select(x => x.Id.ToString()).ToList());
                }

                if (string.IsNullOrEmpty(responseData.Result?.NextToken))
                {
                    break;
                }

                totalItem = responseData.Result?.Products.Count ?? 0;
                pageToken = responseData.Result.NextToken;
            }
            result.Total = result.Data.Count + result.RemovedProductIds.Count;
            getGetProductLog.ResponseObject = result;
            getGetProductLog.LogInfo();
            return result;
        }

        private async Task<List<Product>> GetVariantProducts(string accessToken, List<SendoProduct> parentProducts, long channelId, Guid logId)
        {
            var log = new LogObject(Log, logId)
            {
                Action = "SendoGetVariantProducts",
                OmniChannelId = channelId,
                RequestObject = parentProducts
            };
            var variantProducts = new List<Product>();

            foreach (var parent in parentProducts)
            {
                var response = await Policy
                    .HandleResult<IRestResponse<SendoRequestBase<SendoProductDetail>>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                        (res, timeSpan, retryCount) =>
                        {
                            Log.Warn($"Request failed with {res.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                        }).ExecuteAsync(() => SendoHelper.GetProductDetail(accessToken, parent.Id));

                if (response?.Data?.Result?.Variants?.Any() != true)
                {
                    continue;
                }

                var attributes = new Dictionary<string, string>();

                if (parent.Attributes?.Any(x => x.AttributeValues != null) == true)
                {
                    foreach (var item in parent.Attributes)
                    {
                        var itemAttributes = item.AttributeValues.ToDictionary(x => $"{item.AttributeId}_{x.OptionId}", y => y.Value);
                        attributes = attributes.Concat(itemAttributes).ToDictionary(x => x.Key, y => y.Value);
                    }
                }

                var variants = response.Data.Result.Variants;
                foreach (var variant in variants)
                {
                    var variantProduct = new Product
                    {
                        ItemId = $"{parent.Id}_{variant.VariantAttributeHash}",
                        ParentItemId = parent.Id.ToString(),
                        ItemSku = variant.VariantSku,
                        BasePrice = variant.VariantPrice ?? 0,
                        SalePrice = variant.VariantSpecialPrice,
                        OnHand = variant.VariantQuantity ??0,
                        ItemName = response.Data.Result.Name,
                        Status = response.Data.Result.Status == SendoProductStatus.Approved ? "active" : "inactive",
                        Type = (byte)ChannelProductType.Variation
                    };

                    if (variant.VariantAttributes != null && variant.VariantAttributes.Any())
                    {
                        var attrIds = variant.VariantAttributes.Select(x => $"{x.AttributeId}_{x.OptionId}").ToList();
                        var nameVariants = attributes.Where(x => attrIds.Contains(x.Key)).Select(x => x.Value).Join(" - ");
                        if (!string.IsNullOrEmpty(nameVariants))
                        {
                            variantProduct.ItemName += $" - {nameVariants}";
                        }
                    }
                    variantProducts.Add(variantProduct);
                }
            }
            log.ResponseObject = variantProducts;
            log.LogInfo();
            return variantProducts;
        }

        public async Task<ProductDetailResponse> GetProductDetail(ChannelAuth auth, long channelId, Guid logId, object productId, object parentProductId, string productSku, Platform platform)
        {
            var getGetProductDetailLog = new LogObject(Log, logId)
            {
                Action = "SendoGetProductDetail",
                OmniChannelId = channelId,
                RequestObject = $"ParentProductId {parentProductId} ProductId: {productId} Sku:{productSku}"
            };

            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                getGetProductDetailLog.ResponseObject = $"Auth:{auth.ToSafeJson()} EmptyAccessToken";
                getGetProductDetailLog.LogError();
                throw new KvException("EmptyAccessToken");
            }

            var response = await Policy
                .HandleResult<IRestResponse<SendoRequestBase<SendoProductDetail>>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout)
                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                    (res, timeSpan, retryCount) =>
                    {
                        Log.Warn($"Request failed with {res.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                    }).ExecuteAsync(() => SendoHelper.GetProductDetail(auth.AccessToken, DataRequestHelper.ParseItemIdToLong(parentProductId.ToString())));

            getGetProductDetailLog.ResponseObject = response?.Data;
            getGetProductDetailLog.LogInfo();

            if (response?.Data?.Result == null)
            {
                return null;
            }
            var productDetail = response.Data.Result;
            var sku = productDetail.Sku;
            var itemId = productDetail.Id.ToString();
            if (productDetail.Variants?.Any() == true)
            {
                var variantDetail = productDetail.Variants.FirstOrDefault(x => x.VariantSku == productSku || x.VariantAttributeHash == productId.ToString());
                sku = variantDetail?.VariantSku;
                itemId = variantDetail?.VariantAttributeHash;
            }
            var productDetails = new List<ProductDetailSkus>
            {
                new ProductDetailSkus
                {
                    Sku = sku,
                    Name = productDetail.Name,
                    Status = productDetail.Status == SendoProductStatus.Approved ? "active" : "inactive",
                    Images = productDetail.Pictures?.Select(y => y.PictureUrl).ToList(),
                    Description = productDetail.Description,
                    Variations = productDetail.Variants?.Select(x => (object)x).ToList(),
                    ItemId = itemId
                }
            };

            var result = new ProductDetailResponse
            {
                ItemId = itemId,
                ProductDetails = productDetails
            };
            return result;
        }

        public async Task<(bool, string)> SyncOnHand(
            int retailerId, string kvProductSku, long channelId, Guid logId,
            ChannelAuth auth, string itemId, string itemSku, double onHand, byte productType,
            LogObjectMicrosoftExtension loggerExtension, string parentProductId = null, Platform platform = null)
        {
            IRestResponse<SendoUpdateProductResponse> response = null;
            var syncOnHandLog =
                loggerExtension.Clone(string.Format(EcommerceTypeException.SyncOnHand, Ecommerce.Sendo));
            syncOnHandLog.RequestObject = $"Id:{itemId} Sku:{itemSku} Onhand:{onHand} channelId: {channelId}";

            try
            {
                switch (productType)
                {
                    case (byte)ChannelProductType.Normal:
                    {
                        var bodyRequest = new List<SendoUpdateProduct>
                        {
                            new SendoUpdateProduct
                            {
                                Id = DataRequestHelper.ParseItemIdToLong(itemId),
                                StockQuantity = (int)onHand,
                                FieldMask = new List<string> { "quantity" }
                            }
                        };
                        response = await Policy
                        .HandleResult<IRestResponse<SendoUpdateProductResponse>>(msg => msg.StatusCode != HttpStatusCode.OK)
                        .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                            (res, timeSpan, retryCount, context) =>
                            {
                                var messageErr =
                                    $"SendoSyncOnHandWaitAndRetry with {res.Result.StatusCode}. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount}";

                                var ex = new Exception(messageErr);
                                syncOnHandLog.LogError(ex);

                                if (res.Exception != null)
                                {
                                    syncOnHandLog.LogError(res.Exception);
                                }
                            }).ExecuteAsync(() => SendoHelper.UpdatePriceOrStock(bodyRequest, auth.AccessToken, channelId, false, syncOnHandLog));
                        break;
                    }

                    case (byte)ChannelProductType.Variation:
                    {
                        var variantAttributeHash = itemId.Substring(itemId.IndexOf('_') + 1);
                        var bodyRequest = new List<SendoUpdateProduct>
                        {
                            new SendoUpdateProduct
                            {
                                Id = DataRequestHelper.ParseItemIdToLong(parentProductId),
                                Variants = new List<Variant>
                                {
                                    new Variant
                                    {
                                        VariantAttributeHash = variantAttributeHash,
                                        VariantQuantity = (int)onHand,
                                        FieldMask = new List<string> { "quantity" }
                                    }
                                }
                            }
                        };
                        response = await Policy
                            .HandleResult<IRestResponse<SendoUpdateProductResponse>>(msg =>
                                msg.StatusCode != HttpStatusCode.OK)
                            .WaitAndRetryAsync(SelfAppConfig.ChannelRetry,
                                i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                                (res, timeSpan, retryCount, context) =>
                                {
                                    var messageErr =
                                        $"SendoSyncOnHandWaitAndRetry with {res.Result.StatusCode}. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount}";
                                    var ex = new Exception(messageErr);
                                    syncOnHandLog.LogError(ex);

                                    if (res.Exception != null)
                                    {
                                        syncOnHandLog.LogError(res.Exception);
                                    }
                                }).ExecuteAsync(() => SendoHelper.UpdatePriceOrStock(bodyRequest, auth.AccessToken, channelId, false, syncOnHandLog));
                        break;
                    }
                }

                var errorMessage = response?.Data?.Result?.Result?.FirstOrDefault()?.ErrorMessage ?? response?.Data?.Error?.ToSafeJson();
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    var exCustom = new Exception(errorMessage);
                    syncOnHandLog.LogError(exCustom);
                    return (false, (new { Message = errorMessage }).ToSafeJson());
                }

                syncOnHandLog.ResponseObject = response?.Data;
                syncOnHandLog.LogInfo();

                return (string.IsNullOrEmpty(errorMessage), (new { Message = errorMessage }).ToSafeJson());
            }
            catch (Exception ex)
            {
                syncOnHandLog.LogError(ex);
                throw;
            }
        }

        public async Task<(bool, string)> SyncMultiOnHand(int retailerId,
            long channelId,
            Guid logId,
            ChannelAuth auth,
            List<MultiProductItem> productItems,
            LogObjectMicrosoftExtension loggerExtension,
            byte productType, 
            Platform platform)
        {
            IRestResponse<SendoUpdateProductResponse> response = null;
            var syncMultiOnHandLog =
                loggerExtension.Clone(string.Format(EcommerceTypeException.SyncMultiOnHand, Ecommerce.Sendo));

            syncMultiOnHandLog.RequestObject = productItems;

            try
            {
                switch (productType)
                {
                    case (byte)ChannelProductType.Normal:
                        {
                            List<SendoUpdateProduct> bodyRequests = new List<SendoUpdateProduct>();
                            foreach (var item in productItems)
                            {
                                bodyRequests.Add(new SendoUpdateProduct
                                {
                                    Id = DataRequestHelper.ParseItemIdToLong(item.ItemId),
                                    StockQuantity = (int)item.OnHand,
                                    FieldMask = new List<string> { "quantity" }
                                });
                            }

                            response = await Policy
                                .HandleResult<IRestResponse<SendoUpdateProductResponse>>(msg => msg.StatusCode != HttpStatusCode.OK)
                                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                                    (res, timeSpan, retryCount, context) =>
                                    {
                                        if (res.Exception != null)
                                        {
                                            syncMultiOnHandLog.LogError(res.Exception);
                                            return;
                                        }

                                        if (res.Result?.Data?.Error != null)
                                        {
                                            var msg = res.Result.Data.Error.ToJson();
                                            var ex = new SendoException(msg);
                                            syncMultiOnHandLog.LogError(ex);
                                            return;
                                        }

                                        if (!string.IsNullOrEmpty(res.Result?.ErrorMessage))
                                        {
                                            var msg = res.Result.ErrorMessage;
                                            var ex = new SendoException(msg);
                                            syncMultiOnHandLog.LogError(ex);
                                            return;
                                        }

                                        var messageErr =
                                            $"SendoSyncMultiOnHandWaitAndRetry with {res.Result?.StatusCode}. UpdatePriceOrStock Normal. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount}";

                                        var exRetry = new SendoException(messageErr);

                                        syncMultiOnHandLog.LogError(exRetry);
                                    }).ExecuteAsync(() => SendoHelper.UpdatePriceOrStock(bodyRequests, auth.AccessToken, channelId, false, syncMultiOnHandLog));
                            break;
                        }

                    case (byte)ChannelProductType.Variation:
                        {
                            var bodyRequests = productItems.GroupBy(x => x.ParentItemId).Select(x => new SendoUpdateProduct
                            {
                                Id = DataRequestHelper.ParseItemIdToLong(x.Key),
                                Variants = x.Select(item =>
                                {
                                    var variantAttributeHash = item.ItemId.Substring(item.ItemId.IndexOf('_') + 1);

                                    return new Variant
                                    {
                                        VariantAttributeHash = variantAttributeHash,
                                        VariantQuantity = (int)item.OnHand,
                                        FieldMask = new List<string> { "quantity" }
                                    };
                                }).ToList()
                            }).ToList();

                            response = await Policy
                                .HandleResult<IRestResponse<SendoUpdateProductResponse>>(msg => msg.StatusCode != HttpStatusCode.OK)
                                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                                    (res, timeSpan, retryCount, context) =>
                                    {
                                        if (res.Exception != null)
                                        {
                                            syncMultiOnHandLog.LogError(res.Exception);
                                            return;
                                        }

                                        if (res.Result?.Data?.Error != null)
                                        {
                                            var msg = res.Result.Data.Error.ToJson();
                                            var ex = new SendoException(msg);
                                            syncMultiOnHandLog.LogError(ex);
                                            return;
                                        }

                                        if (!string.IsNullOrEmpty(res.Result?.ErrorMessage))
                                        {
                                            var msg = res.Result.ErrorMessage;
                                            var ex = new SendoException(msg);
                                            syncMultiOnHandLog.LogError(ex);
                                            return;
                                        }

                                        var messageErr =
                                            $"SendoSyncMultiOnHandWaitAndRetry with {res.Result?.StatusCode}. UpdatePriceOrStock Variation. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount}";

                                        var exRetry = new SendoException(messageErr);

                                        syncMultiOnHandLog.LogError(exRetry);
                                    }).ExecuteAsync(() => SendoHelper.UpdatePriceOrStock(bodyRequests, auth.AccessToken, channelId, false, syncMultiOnHandLog));
                            break;
                        }
                }
                var message = new List<Product>();
                if (response?.Data.Result != null)
                {
                    var listSendoProductUpdates = response.Data?.Result?.Result ?? new List<SendoUpdateProductItemDetailResponse>();
                    foreach (var sendoProduct in listSendoProductUpdates)
                    {
                        var products = productItems.Where(x => x.ParentItemId == sendoProduct.ProductId.ToString()).ToList();
                        if (!products.Any()) continue;
                        
                        var errorMsg = sendoProduct.ErrorMessage;
                        if (!string.IsNullOrEmpty(errorMsg))
                        {
                            errorMsg = $"Hàng hóa phân loại đang có lỗi {errorMsg} dẫn đến không thể cập nhật các sản phẩm cùng hàng cha. Vui lòng kiểm tra lại tất cả các hàng phân loại và đồng bộ lại.";
                        }

                        message.AddRange(products.Select(x => new Product
                        {
                            ItemId = x.ItemId,
                            OnHand = (int)x.OnHand,
                            ErrorMessage = errorMsg
                        }).ToList());
                    }
                }

                var messageError = response?.Data?.Error?.ToSafeJson() ?? string.Empty;
                if (!string.IsNullOrEmpty(messageError))
                {
                    var exCustom = new SendoException(messageError);
                    syncMultiOnHandLog.LogError(exCustom);
                    return (false, (new { Message = response?.Data?.Error }).ToSafeJson());
                }

                syncMultiOnHandLog.ResponseObject = response?.Data;
                syncMultiOnHandLog.LogInfo();

                return 
                    (response?.Data?.Success ?? false, (response?.Data?.Success ?? false)
                    ? (new { Product = message} ).ToSafeJson()
                    : (new { Message = response?.Data?.Error }).ToSafeJson());
            }
            catch (Exception ex)
            {
                syncMultiOnHandLog.LogError(ex);
                throw;
            }
        }

        public async Task<(bool, string)> SyncPrice(int retailerId, string kvProductSku, long channelId, Guid logId, ChannelAuth auth, string itemId, string itemSku, decimal? basePrice
            , byte productType, string parentItemId, LogObjectMicrosoftExtension loggerExtension, decimal? salePrice = null, DateTime? startSaleDateTime = null, DateTime? endSaleDateTime = null, Platform platform = null)
        {
            try
            {
                IRestResponse<SendoUpdateProductResponse> response = null;
                var syncPriceLog = new LogObject(Log, logId)
                {
                    Action = string.Format(EcommerceTypeException.SyncPrice, Ecommerce.Sendo),
                    OmniChannelId = channelId
                };
                syncPriceLog.Description = "ParentItem " + parentItemId + ", basePrice " + basePrice + "ParentItem " + salePrice + "ParentItem " + salePrice + ", startSaleDateTime" + startSaleDateTime;
                syncPriceLog.LogInfo();
                var sendoProduct = new SendoUpdateProduct();
                if (String.IsNullOrEmpty(parentItemId))
                    return (false, "parent ItemId of variation is null");
                switch (productType)
                {
                    case (byte)ChannelProductType.Normal:
                        {
                            var bodyRequest = new List<SendoUpdateProduct>();
                            sendoProduct.Id = DataRequestHelper.ParseItemIdToLong(itemId);
                            sendoProduct.Price = basePrice ?? 0;
                            if (salePrice < basePrice && salePrice > 0 && startSaleDateTime != null && startSaleDateTime != default(DateTime) && endSaleDateTime != null && endSaleDateTime != default(DateTime))
                            {
                                sendoProduct.SalePrice = salePrice;
                                sendoProduct.SaleStartDate = startSaleDateTime.Value.ToString("yyyy-MM-dd");
                                sendoProduct.SaleEndDate = endSaleDateTime.Value.ToString("yyyy-MM-dd");
                                sendoProduct.FieldMask = new List<string> { "price", "special_price", "promotion_from_date", "promotion_to_date" };
                                syncPriceLog.Description =
                                    startSaleDateTime.Value.ToString("yyyy-MM-dd") + "@@@@" +
                                    startSaleDateTime.Value.ToUniversalTime().ToString("yyyy-MM-dd");
                            }
                            else
                            {
                                sendoProduct.SalePrice = 0;
                                sendoProduct.SaleStartDate = "";
                                sendoProduct.SaleEndDate = "";
                                sendoProduct.FieldMask = new List<string> { "price", "special_price", "promotion_from_date", "promotion_to_date" };
                            }
                            bodyRequest.Add(sendoProduct);
                            syncPriceLog.RequestObject = JsonConvert.SerializeObject(bodyRequest);
                            response = await Policy
                                .HandleResult<IRestResponse<SendoUpdateProductResponse>>(msg => msg.StatusCode != HttpStatusCode.OK)
                                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                                    (res, timeSpan, retryCount, context) =>
                                    {
                                        var msg = res.Result?.Data?.Error?.ToSafeJson() ?? string.Empty;
                                        if (!string.IsNullOrEmpty(msg))
                                        {
                                            var exCustom = new SendoException(msg);
                                            loggerExtension.LogError(exCustom);
                                        }
                                        else if (res.Result?.ErrorException != null)
                                        {
                                            loggerExtension.LogError(res.Result.ErrorException);
                                        }
                                        else if (res.Exception != null)
                                        {
                                            loggerExtension.LogError(res.Exception);
                                        }
                                        else
                                        {
                                            var messageErr =
                                                $"SendoSyncPriceWaitAndRetry with {res.Result?.StatusCode}. SyncPrice - Normal - UpdatePriceOrStock. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount}";
                                            var ex = new Exception(messageErr);
                                            loggerExtension.LogError(ex);
                                        }

                                    }).ExecuteAsync(() => SendoHelper.UpdatePriceOrStock(bodyRequest, auth.AccessToken, channelId, true, loggerExtension));
                            break;
                        }

                    case (byte)ChannelProductType.Variation:
                        {
                            var variantAttributeHash = itemId.Substring(itemId.IndexOf('_') + 1);
                            var sendoVariant = new Variant();
                            sendoVariant.VariantAttributeHash = variantAttributeHash;
                            sendoVariant.VariantPrice = basePrice ?? 0;
                            if (salePrice < basePrice && salePrice > 0 && startSaleDateTime != null && startSaleDateTime != default(DateTime) && endSaleDateTime != null && endSaleDateTime != default(DateTime))
                            {
                                sendoVariant.VariantSalePrice = salePrice;
                                sendoVariant.VariantSaleStartDate = startSaleDateTime.Value.ToString("yyyy-MM-dd");
                                sendoVariant.VariantSaleEndDate = endSaleDateTime.Value.ToString("yyyy-MM-dd");
                                sendoVariant.FieldMask = new List<string> { "price", "special_price", "promotion_start_timestamp", "promotion_end_timestamp" };
                                syncPriceLog.Description =
                                    startSaleDateTime.Value.ToString("yyyy-MM-dd") + "@@@@" +
                                    startSaleDateTime.Value.ToUniversalTime().ToString("yyyy-MM-dd");
                            }
                            else
                            {
                                sendoVariant.VariantSalePrice = 0;
                                sendoVariant.VariantSaleStartDate = "";
                                sendoVariant.VariantSaleEndDate = "";
                                sendoVariant.FieldMask = new List<string> { "price", "special_price", "promotion_start_timestamp", "promotion_end_timestamp" };
                            }

                            var bodyRequest = new List<SendoUpdateProduct>
                            {
                            new SendoUpdateProduct
                            {
                                Id = DataRequestHelper.ParseItemIdToLong(parentItemId),
                                Variants =  new List<Variant>{sendoVariant}
                            }
                        };

                            syncPriceLog.RequestObject = JsonConvert.SerializeObject(bodyRequest);
                            response = await Policy
                            .HandleResult<IRestResponse<SendoUpdateProductResponse>>(msg =>
                                msg.StatusCode != HttpStatusCode.OK)
                            .WaitAndRetryAsync(SelfAppConfig.ChannelRetry,
                                i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                                (res, timeSpan, retryCount, context) =>
                                {
                                    var msg = res.Result?.Data?.Error?.ToSafeJson() ?? string.Empty;
                                    if (!string.IsNullOrEmpty(msg))
                                    {
                                        var exCustom = new SendoException(msg);
                                        loggerExtension.LogError(exCustom);
                                    }
                                    else if (res.Result?.ErrorException != null)
                                    {
                                        loggerExtension.LogError(res.Result.ErrorException);
                                    }
                                    else if (res.Exception != null)
                                    {
                                        loggerExtension.LogError(res.Exception);
                                    }
                                    else
                                    {
                                        var messageErr =
                                            $"SendoSyncPriceWaitAndRetry with {res.Result?.StatusCode}. SyncPrice - Variation - UpdatePriceOrStock. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount}";
                                        var ex = new Exception(messageErr);
                                        loggerExtension.LogError(ex);
                                    }
                                }).ExecuteAsync(() => SendoHelper.UpdatePriceOrStock(bodyRequest, auth.AccessToken, channelId, true, loggerExtension));
                            break;

                        }
                }
                syncPriceLog.Action = string.Format(EcommerceTypeException.SyncPrice, Ecommerce.Sendo);

                var errorMessage = response?.Data.Result?.Result.FirstOrDefault()?.ErrorMessage ?? response?.Data.Error.ToSafeJson();
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    var exCustom = new Exception(errorMessage);
                    loggerExtension.LogError(exCustom);
                    return (false, (new { Message = errorMessage }).ToSafeJson());
                }

                syncPriceLog.ResponseObject = response?.Data;
                syncPriceLog.LogInfo();

                return (string.IsNullOrEmpty(errorMessage), (new { Message = errorMessage }).ToSafeJson());
            }
            catch (Exception ex)
            {
                loggerExtension.LogError(ex);
                throw;
            }
        }

        public async Task<(bool, string, int?)> SyncMultiPrice(int retailerId,
            long channelId,
            Guid logId,
            ChannelAuth auth,
            List<MultiProductItem> productItems,
            byte productType,
            LogObjectMicrosoftExtension loggerExtension,
            Platform platform)
        {

            var syncMultiPriceLog =
                loggerExtension.Clone(string.Format(EcommerceTypeException.SyncMultiPrice, Ecommerce.Sendo));

            try
            {
                IRestResponse<SendoUpdateProductResponse> response = null;
                switch (productType)
                {
                    case (byte)ChannelProductType.Normal:
                        {
                            List<SendoUpdateProduct> bodyRequests = new List<SendoUpdateProduct>();
                            foreach (var item in productItems)
                            {
                                var sendoProduct = new SendoUpdateProduct
                                {
                                    Id = DataRequestHelper.ParseItemIdToLong(item.ItemId),
                                    Price = item.Price ?? 0,
                                    FieldMask = new List<string> { "price" }
                                };
                                if (item.SalePrice < item.Price && item.SalePrice > 0 && item.StartSaleDate != null && item.StartSaleDate != default(DateTime) && item.EndSaleDate != null && item.EndSaleDate != default(DateTime))
                                {
                                    sendoProduct.SalePrice = item.SalePrice;
                                    sendoProduct.SaleStartDate = item.StartSaleDate.Value.ToString("yyyy-MM-dd");
                                    sendoProduct.SaleEndDate = item.EndSaleDate.Value.ToString("yyyy-MM-dd");
                                    sendoProduct.FieldMask = new List<string> { "price", "special_price", "promotion_from_date", "promotion_to_date" };
                                    syncMultiPriceLog.Description =
                                        item.StartSaleDate.Value.ToString("yyyy-MM-dd") + "@@@@" +
                                        item.StartSaleDate.Value.ToUniversalTime().ToString("yyyy-MM-dd");
                                }
                                else
                                {
                                    sendoProduct.SalePrice = 0;
                                    sendoProduct.SaleStartDate = "";
                                    sendoProduct.SaleEndDate = "";
                                    sendoProduct.FieldMask = new List<string> { "price", "special_price", "promotion_from_date", "promotion_to_date" };
                                }
                                bodyRequests.Add(sendoProduct);

                            }
                            syncMultiPriceLog.RequestObject = JsonConvert.SerializeObject(bodyRequests);

                            response = await Policy
                                .HandleResult<IRestResponse<SendoUpdateProductResponse>>(msg => msg.StatusCode != HttpStatusCode.OK)
                                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                                    (res, timeSpan, retryCount, context) =>
                                    {
                                        var msg = res.Result?.Data?.Error?.ToSafeJson() ?? string.Empty;
                                        if (!string.IsNullOrEmpty(msg))
                                        {
                                            var exCustom = new SendoException(msg);
                                            syncMultiPriceLog.LogError(exCustom);
                                        }
                                        else if (res.Result?.ErrorException != null)
                                        {
                                            syncMultiPriceLog.LogError(res.Result.ErrorException);
                                        }
                                        else if (res.Exception != null)
                                        {
                                            syncMultiPriceLog.LogError(res.Exception);
                                        }
                                        else
                                        {
                                            var messageErr =
                                                $"SendoSyncMultiPriceWaitAndRetry with {res.Result?.StatusCode}. SyncMultiPrice - Normal - UpdatePriceOrStock. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount}";
                                            var ex = new Exception(messageErr);
                                            syncMultiPriceLog.LogError(ex);
                                        }
                                    }).ExecuteAsync(() => SendoHelper.UpdatePriceOrStock(bodyRequests, auth.AccessToken, channelId, true, loggerExtension));
                            break;
                        }

                    case (byte)ChannelProductType.Variation:
                        {
                            var bodyRequests = productItems.GroupBy(x => x.ParentItemId).Select(x => new SendoUpdateProduct
                            {
                                Id = DataRequestHelper.ParseItemIdToLong(x.Key),
                                Variants = x.Select(item =>
                                {
                                    var variantAttributeHash = item.ItemId.Substring(item.ItemId.IndexOf('_') + 1);
                                    var sendoVariant = new Variant();
                                    sendoVariant.VariantAttributeHash = variantAttributeHash;
                                    sendoVariant.VariantPrice = item.Price ?? 0;
                                    if (item.SalePrice < item.Price && item.SalePrice > 0 && item.StartSaleDate != null && item.StartSaleDate != default(DateTime) && item.EndSaleDate != null && item.EndSaleDate != default(DateTime))
                                    {
                                        sendoVariant.VariantSalePrice = item.SalePrice;
                                        sendoVariant.VariantSaleStartDate = item.StartSaleDate.Value.ToString("yyyy-MM-dd");
                                        sendoVariant.VariantSaleEndDate = item.EndSaleDate.Value.ToString("yyyy-MM-dd");
                                        sendoVariant.FieldMask = new List<string> { "price", "special_price", "promotion_start_timestamp", "promotion_end_timestamp" };
                                        syncMultiPriceLog.Description =
                                            item.StartSaleDate.Value.ToString("yyyy-MM-dd") + "@@@@" +
                                            item.StartSaleDate.Value.ToUniversalTime().ToString("yyyy-MM-dd");
                                    }
                                    else
                                    {
                                        sendoVariant.VariantSalePrice = 0;
                                        sendoVariant.VariantSaleStartDate = "";
                                        sendoVariant.VariantSaleEndDate = "";
                                        sendoVariant.FieldMask = new List<string> { "price", "special_price", "promotion_start_timestamp", "promotion_end_timestamp" };
                                    }
                                    return sendoVariant;
                                }).ToList()
                            }).ToList();

                            syncMultiPriceLog.RequestObject = JsonConvert.SerializeObject(bodyRequests);
                            response = await Policy
                                .HandleResult<IRestResponse<SendoUpdateProductResponse>>(msg => msg.StatusCode != HttpStatusCode.OK)
                                .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                                    (res, timeSpan, retryCount, context) =>
                                    {
                                        var msg = res.Result?.Data?.Error?.ToSafeJson() ?? string.Empty;
                                        if (!string.IsNullOrEmpty(msg))
                                        {
                                            var exCustom = new SendoException(msg);
                                            syncMultiPriceLog.LogError(exCustom);
                                        }
                                        else if (res.Result?.ErrorException != null)
                                        {
                                            syncMultiPriceLog.LogError(res.Result.ErrorException);
                                        }
                                        else if (res.Exception != null)
                                        {
                                            syncMultiPriceLog.LogError(res.Exception);
                                        }
                                        else
                                        {
                                            var messageErr =
                                                $"SendoSyncMultiPriceWaitAndRetry with {res.Result?.StatusCode}. SyncMultiPrice - Variation - UpdatePriceOrStock. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount}";
                                            var ex = new Exception(messageErr);
                                            syncMultiPriceLog.LogError(ex);
                                        }

                                    }).ExecuteAsync(() => SendoHelper.UpdatePriceOrStock(bodyRequests, auth.AccessToken, channelId, true, loggerExtension));
                            break;
                        }
                }
                var message = new List<Product>();
                if (response?.Data.Result != null)
                {
                    for (int i = 0; i < response?.Data.Result.Result.Count; i++)
                    {
                        var products = productItems.Where(x => x.ParentItemId == response.Data.Result.Result[i].ProductId.ToString());
                        if (products.Any())
                        {
                            var errorMsg = response.Data.Result.Result[i].ErrorMessage;
                            message.AddRange(products.Select(x => new Product
                            {
                                ItemId = x.ItemId,
                                BasePrice = x.Price.GetValueOrDefault(),
                                SalePrice = x.SalePrice.GetValueOrDefault(),
                                ErrorMessage = errorMsg
                            }).ToList());
                        }
                    }

                }

                var isSuccess = response?.Data?.Success ?? false;
                var messageResponse = isSuccess
                    ? (new { Product = message }).ToSafeJson()
                    : (new { Message = response?.Data?.Error }).ToSafeJson();

                if (!isSuccess)
                {
                    var exCustom = new Exception(messageResponse);
                    syncMultiPriceLog.LogError(exCustom);
                    return (false, messageResponse, null);
                }

                syncMultiPriceLog.ResponseObject = response?.Data;
                syncMultiPriceLog.LogInfo();

                return (true, messageResponse, null);
            }
            catch (Exception ex)
            {
                syncMultiPriceLog.LogError(ex);
                throw;
            }
        }

        public async Task<(bool, List<CreateOrderRequest>)> GetOrders(ChannelAuth auth, OrderRequest request, KvInternalContext context, LogObjectMicrosoftExtension logInfo, Platform platform)
        {
            try
            {
                if (string.IsNullOrEmpty(auth.AccessToken))
                {
                    var ex = new OmniException(
                        $"{KeywordCommonSearch.CallSendoError} Empty access token");
                    logInfo.LogError(ex);
                    throw ex;
                }

                var allocateLog = new StringBuilder();
                var pageToken = string.Empty;
                var currentToDate = request.LastSync ?? DataRequestHelper.GetOrderLastSyncDateTime(request.SyncOrderFormula.GetValueOrDefault());
                var sendoOrders = new List<SendoOrder>();
                var toDay = DateTime.Now;
                while (currentToDate <= toDay)
                {
                    var orderStatusDateTo = currentToDate.Date.AddDays(2);
                    var (responseOrders, message) = await GetOrderPerTime(request.ChannelId, auth, pageToken,
                        SelfAppConfig.SendoOrderPageSize, currentToDate, orderStatusDateTo, logInfo);
                    if (responseOrders != null && responseOrders.Any())
                    {
                        sendoOrders.AddRange(responseOrders);
                    }
                    allocateLog.Append($"currentToDate: {currentToDate}, count: {sendoOrders?.Count}, message: {message} - ");
                    
                    currentToDate = orderStatusDateTo.Date.AddDays(1);
                }

                var orders = (from o in sendoOrders
                    where o.SaleOrder != null
                    select new CreateOrderRequest
                    {
                        OrderId = o.SaleOrder.OrderNumber,
                        OrderStatus = EnumHelper.GetNameByType<SendoOrderStatus>(o.SaleOrder.OrderStatus),
                        CreateDate = DateTime.SpecifyKind(o.SaleOrder.CreatedDateTimeStamp.FromUnixTime(),
                            DateTimeKind.Local),
                        UpdateAt = o.SaleOrder.UpdatedDateTimeStamp > 0
                            ? DateTime.SpecifyKind(
                                DateTimeOffset.FromUnixTimeSeconds(o.SaleOrder.UpdatedDateTimeStamp).DateTime,
                                DateTimeKind.Local)
                            : toDay
                    }).ToList();

                logInfo.Description = allocateLog.ToString();
                logInfo.ResponseObject = sendoOrders;
                logInfo.TotalItem = sendoOrders.Count;
                logInfo.LogInfo(true);

                return (true, orders);
            }
            catch (Exception ex)
            {
                logInfo.LogError(ex);
                throw;
            }
        }

        private async Task<(List<SendoOrder>, string)> GetOrderPerTime(
            long channelId, ChannelAuth auth, string pageToken, int pageSize,
            DateTime? orderStatusDateFrom, DateTime? orderStatusDateTo, LogObjectMicrosoftExtension logInfo)
        {
            try
            {
                var totalItem = pageSize;
                var sendoOrders = new List<SendoOrder>();
                var idx = 0;
               
                while (totalItem >= pageSize)
                {
                    var response = await Policy
                        .HandleResult<IRestResponse<SendoOrderListResponse>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout)
                        .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                            (res, timeSpan, retryCount, ct) =>
                            {
                                var msg =
                                    $"Request failed with {res.Result?.StatusCode}. GetOrderPerTime - GetOrders. Waiting {timeSpan} before next retry. Retry attempt {retryCount}";
                                if (res.Result?.Data?.Error != null)
                                {
                                    msg += $". Err: {res.Result.Data.Error.ToSafeJson()}";
                                }
                                else if (res?.Exception != null)
                                {
                                    logInfo.LogError(res.Exception);
                                }
                                var ex = new SendoException(msg);
                                logInfo.LogError(ex);
                            })
                        .ExecuteAsync(() => SendoHelper.GetOrders(auth.AccessToken, pageToken, pageSize, channelId, orderStatusDateFrom, orderStatusDateTo, logInfo));

                    if (!string.IsNullOrEmpty(response?.Data?.Error))
                    {
                        return (sendoOrders, $"PageIndex: {idx}, PageToken: {pageToken}, Status code: {response?.StatusCode}, Messages: {response?.Data?.Error}");
                    }
                    if (response?.Data?.Result?.Orders == null)
                    {
                        return (null, new { response?.ErrorMessage, response?.Content }.ToSafeJson() ?? "Response is null");
                    }
                    sendoOrders.AddRange(response.Data.Result.Orders);
                    if (string.IsNullOrEmpty(response.Data.Result.NextToken))
                    {
                        return (sendoOrders, string.Empty);
                    }
                    totalItem = response.Data.Result.Orders.Count;
                    pageToken = response.Data.Result.NextToken;
                    ++idx;
                }
                
                return (sendoOrders, string.Empty);
            }
            catch (Exception ex)
            {
                logInfo.LogError(ex);
                throw;
            }
        }

        public async Task<OrderDetailGetResponse> GetOrderDetail(int retailerId, long channelId, ChannelAuth auth, 
            CreateOrderRequest request, LogObjectMicrosoftExtension logInfo,
            Platform platform, OmniChannelSettingObject settings = null)
        {
            var logDetail = logInfo.Clone("GetOrderDetail");
            try
            {
                if (string.IsNullOrEmpty(request.OrderId))
                {
                    logDetail.ResponseObject = "Empty or null order id";
                    logDetail.LogInfo(true);
                    return new OrderDetailGetResponse
                    {
                        IsSuccess = false,
                        IsRetryOrder = false
                    };
                }
                if (string.IsNullOrEmpty(auth.AccessToken))
                {
                    var ex = new OmniException(
                        $"{KeywordCommonSearch.CallSendoError} EmptyAccessToken SendoGetLogisticsStatus");
                    logDetail.LogError(ex);
                    throw ex;
                }
                var orderDetail = await Policy.HandleResult<IRestResponse<SendoOrderDetailResponse>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval), (res, timeSpan, retryCount, context) =>
                    {
                        var msg =
                            $"Request failed with {res.Result?.StatusCode}. GetOrdersDetail. Waiting {timeSpan} before next retry. Retry attempt {retryCount}";
                        if (res.Result?.Data?.Error != null)
                        {
                            msg += $". Err: {res.Result.Data.Error.ToSafeJson()}";
                        }
                        else if (res.Exception != null)
                        {
                            logDetail.LogError(res.Exception);
                        }
                        var ex = new SendoException(msg);
                        logDetail.LogError(ex);
                    }).ExecuteAsync(() => SendoHelper.GetOrdersDetail(auth.AccessToken, request.OrderId, channelId, logDetail));
                logDetail.ResponseObject = new { orderDetail?.Content, orderDetail?.ErrorMessage, orderDetail?.StatusCode };

                if (orderDetail?.Data?.Result == null)
                {
                    logDetail.Description = "Empty order detail";
                    logDetail.LogInfo(true);
                    return new OrderDetailGetResponse
                    {
                        IsSuccess = false,
                        IsRetryOrder = true
                    };
                }

                var sendoOrder = orderDetail.Data.Result;
                var kvOrder = new KvOrder
                {
                    Code = $"DHSDO_{request.OrderId}",
                    BranchId = request.BranchId,
                    SoldById = request.UserId,
                    SaleChannelId = request.SaleChannelId,
                    PurchaseDate = sendoOrder.SaleOrder.OrderDateTimeStamp > 0 ?
                                    DateTime.SpecifyKind(sendoOrder.SaleOrder.OrderDateTimeStamp.FromUnixTime(), DateTimeKind.Local) : DateTime.Now,
                    Description = $"Đơn hàng từ Sendo {request.OrderId}",
                    IsSyncSuccess = false,
                    UsingCod = true,
                    OrderDelivery = new InvoiceDelivery
                    {
                        Receiver = sendoOrder.SaleOrder?.ReceiverName ?? string.Empty,
                        ContactNumber = sendoOrder.SaleOrder?.ShippingContactPhone,
                        Address = string.IsNullOrEmpty(sendoOrder.SaleOrder?.ShipToAddress) ? sendoOrder.SaleOrder?.ReceiverAddress : sendoOrder.SaleOrder.ShipToAddress,
                        WardName = sendoOrder.SaleOrder?.ShipToWard,
                        ChannelWard = sendoOrder.SaleOrder?.ShipToWard,
                        ReceiverDistrictId = sendoOrder.SaleOrder?.ShipToDistrictId ?? 0,
                        UsingPriceCod = true,
                        PartnerDelivery = new PartnerDelivery { Code = "SENDO", Name = "SENDO" }
                    },
                    CustomerPhone =
                        !string.IsNullOrEmpty(sendoOrder.SaleOrder?.ShippingContactPhone) ? sendoOrder.SaleOrder.ShippingContactPhone : sendoOrder.SaleOrder?.BuyerPhone,
                    CustomerCode = $"KHSDO{request.OrderId}",
                    CustomerName = sendoOrder.SaleOrder?.ReceiverName,
                    CustomerAddress = string.IsNullOrEmpty(sendoOrder.SaleOrder?.ShipToAddress) ? sendoOrder.SaleOrder?.ReceiverAddress : sendoOrder.SaleOrder.ShipToAddress,
                    //CustomerLocation = sendoOrder.SaleOrder?.BuyerRegion + " - " + sendoOrder.SaleOrder?.BuyerDistrict,
                    CustomerWard = sendoOrder.SaleOrder?.ShipToWard,
                    OrderDetails = new List<KvOrderDetail>(),
                    NewStatus = ConvertToKvOrderStatus(sendoOrder.SaleOrder.OrderStatus),
                    Status = ConvertToKvOrderStatus(sendoOrder.SaleOrder.OrderStatus) ?? (byte)OrderState.Pending,
                    ChannelStatus = EnumHelper.GetNameByType<SendoOrderStatus>(sendoOrder.SaleOrder.OrderStatus),
                    Total = sendoOrder.SaleOrder?.SubTotal ?? 0,
                    Discount = sendoOrder.SaleOrder?.ShopVoucherAmount,
                    ChannelCreatedDate = DateTime.SpecifyKind(sendoOrder.SaleOrder.CreatedDateTimeStamp.FromUnixTime(), DateTimeKind.Local)
                };
                foreach (var detail in sendoOrder.SkuDetails)
                {
                    kvOrder.OrderDetails.Add(new KvOrderDetail
                    {
                        ProductChannelId = !string.IsNullOrEmpty(detail.AttributeHash) ? $"{detail.ProductVariantId}_{detail.AttributeHash}" : detail.ProductVariantId,
                        ParentChannelProductId = detail.ProductVariantId,
                        ProductChannelSku = detail.Sku,
                        ProductChannelName = detail.ProductName,
                        Quantity = detail.Quantity,
                        Price = detail.Price,
                        Uuid = StringHelper.GenerateUUID()
                    });
                }
                logDetail.ResponseObject = kvOrder;
                logDetail.LogInfo(true);
                return new OrderDetailGetResponse
                {
                    IsSuccess = true,
                    IsRetryOrder = false,
                    Order = kvOrder
                };
            }
            catch (Exception ex)
            {
                logDetail.LogError(ex);
                throw;
            }
        }

        private byte? ConvertToKvOrderStatus(byte sendoOrderStatus)
        {
            var status = EnumHelper.GetNameByType<SendoOrderStatus>(sendoOrderStatus);
            if (status == SendoOrderStatusName.Shipping || status == SendoOrderStatusName.Pod ||
                status == SendoOrderStatusName.Completed)
            {
                return (byte)OrderState.Finalized;
            }

            if (status == SendoOrderStatusName.Cancelled)
            {
                return (byte)OrderState.Void;
            }
            return null;
        }

        public async Task<(bool, bool, bool, List<KvInvoice>)> GetInvoiceDetail(int retailerId, long channelId,
            Guid logId, ChannelAuth auth, CreateInvoiceRequest request,
            LogObjectMicrosoftExtension loggerExtension, 
            Platform platform,
            OmniChannelSettingObject settings)
        {
            try
            {
                if (request.KvOrder == null)
                {
                    return (false, false, false, null);
                }
                var kvOrder = request.KvOrder;
                if (request.KvDelivery == null)
                {
                    return (false, false, false, null);
                }

                var delivery = request.KvDelivery;
                loggerExtension.Action = string.Format(EcommerceTypeException.GetInvoiceDetail, Ecommerce.Sendo);
                if (string.IsNullOrEmpty(auth.AccessToken))
                {
                    var ex = new OmniException(
                        $"{KeywordCommonSearch.CallSendoError} EmptyAccessToken SendoGetLogisticsStatus");
                    loggerExtension.LogError(ex);
                    throw ex;
                }
                loggerExtension.LogInfo();

                loggerExtension.EcommerceRequest = Ecommerce.Sendo;
                var orderDetail =
                    await Policy.HandleResult<IRestResponse<SendoOrderDetailResponse>>(
                        msg => msg.StatusCode == HttpStatusCode.RequestTimeout
                    )
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval), (res, timeSpan, retryCount, context) =>
                    {
                        var msg =
                            $"Request failed with {res.Result?.StatusCode}. GetOrdersDetail. Waiting {timeSpan} before next retry. Retry attempt {retryCount}";
                        if (res.Result?.Data?.Error != null)
                        {
                            msg += $". Err: {res.Result.Data.Error.ToJson()}";
                        }
                        else if (res.Exception != null)
                        {
                            loggerExtension.LogError(res.Exception);
                        }
                        var ex = new SendoException(msg);
                        loggerExtension.LogError(ex);
                    }).ExecuteAsync(() => SendoHelper.GetOrdersDetail(auth.AccessToken, request.OrderId, channelId, loggerExtension));
                loggerExtension.EcommerceRequest = string.Empty;

                if (orderDetail?.Data?.Result?.SaleOrder == null || orderDetail.Data.Error != null)
                {
                    return (false, false, true, null);
                }

                var sendoOrder = orderDetail.Data.Result;
                if (kvOrder.Status != (int)OrderState.Void && (sendoOrder.SaleOrder.OrderStatus == (byte)SendoOrderStatus.New ||
                                                                sendoOrder.SaleOrder.OrderStatus ==
                                                                (byte)SendoOrderStatus.Proccessing))
                {
                    return (false, false, false, null);
                }

                if (kvOrder.Status != (int)OrderState.Void && kvOrder.Status != (int)OrderState.Finalized &&
                    sendoOrder.SaleOrder.OrderStatus == (byte)SendoOrderStatus.Cancelled &&
                    sendoOrder.SaleOrder.DeliveryStatus != (byte)SendoDeliveryStatus.ReturnSeller)
                {
                    return (true, true, false, null);
                }
                var createdDate = sendoOrder.SaleOrder.OrderDateTimeStamp > 0 ?
                                    DateTime.SpecifyKind(sendoOrder.SaleOrder.OrderDateTimeStamp.FromUnixTime(), DateTimeKind.Local) : DateTime.Now;
                var deliveryStatus = UpdateDeliveryStatusForInvoice(sendoOrder.SaleOrder.OrderStatus, sendoOrder.SaleOrder.DeliveryStatus);
                var kvInvoice = new KvInvoice
                {
                    Code = $"HDSDO_{request.OrderId}",
                    RetailerId = kvOrder.RetailerId,
                    OrderId = kvOrder.Id,
                    BranchId = kvOrder.BranchId,
                    Description = kvOrder.Description,
                    PurchaseDate = GetPurchaseDate(settings, sendoOrder.SaleOrder, createdDate),
                    CustomerId = kvOrder.CustomerId,
                    UsingCod = true,
                    PriceBookId = kvOrder.Extra?.FromJson<Extra>().PriceBookId?.Id ?? 0,
                    DeliveryDetail = new InvoiceDelivery
                    {
                        Receiver = delivery?.Receiver,
                        ContactNumber = delivery?.ContactNumber,
                        DeliveryCode = sendoOrder.SaleOrder.TrackingNumber,
                        Address = delivery?.Address,
                        UsingPriceCod = true,
                        WardName = delivery?.WardName,
                        LocationName = delivery?.LocationName,
                        LocationId = delivery?.LocationId,
                        Status = (byte)DeliveryStatus.Pending,
                        DeliveryBy = delivery?.PartnerId,
                        LatestStatus = deliveryStatus ?? (byte)DeliveryStatus.Pending
                    },
                    Status = (int)InvoiceState.Pending,
                    SaleChannelId = kvOrder.SaleChannelId,
                    Total = sendoOrder.SaleOrder?.SubTotal ?? 0,
                    Discount = sendoOrder.SaleOrder?.ShopVoucherAmount,
                    InvoiceDetails = new List<InvoiceDetail>(),
                    DeliveryStatus = deliveryStatus,
                    NewStatus = ConvertToKvInvoiceStatus(sendoOrder.SaleOrder.OrderStatus),
                };
                foreach (var detail in sendoOrder.SkuDetails)
                {
                    kvInvoice.InvoiceDetails.Add(new InvoiceDetail
                    {
                        ProductChannelId = !string.IsNullOrEmpty(detail.AttributeHash) ? $"{detail.ProductVariantId}_{detail.AttributeHash}" : detail.ProductVariantId,
                        ParentChannelProductId = detail.ProductVariantId,
                        ProductChannelSku = detail.Sku,
                        ProductChannelName = detail.ProductName,
                        Quantity = detail.Quantity,
                        Price = detail.Price,
                        Uuid = StringHelper.GenerateUUID()
                    });
                }
                return (true, false, false, new List<KvInvoice> { kvInvoice });
            }
            catch (Exception ex)
            {
                loggerExtension.LogError(ex);
                throw;
            }
        }

        private DateTime GetPurchaseDate(OmniChannelSettingObject settings,
           SaleOrder saleOrder, DateTime orderCreateTime)
        {
            if (settings.SyncOrderSaleTimeType == (int)SyncOrderSaleTimeType.TimeCreateOrder)
                return orderCreateTime.ToLocalTime();

            if (saleOrder == null)
                return orderCreateTime.ToLocalTime();

            if (saleOrder.SubmitDateTimeStamp.HasValue && saleOrder.SubmitDateTimeStamp > 0)
                return DateTime.SpecifyKind(saleOrder.SubmitDateTimeStamp.Value.FromUnixTime(), DateTimeKind.Local);

            return orderCreateTime.ToLocalTime();
        }

        private static byte? ConvertToKvInvoiceStatus(byte? sendoStatus)
        {
            switch (sendoStatus)
            {
                case (byte)SendoOrderStatus.Shipping:
                case (byte)SendoOrderStatus.Pod:
                case (byte)SendoOrderStatus.Completed:
                case (byte)SendoOrderStatus.Returned:
                case (byte)SendoOrderStatus.Returning:
                case (byte)SendoOrderStatus.Cancelled:
                    {
                        return (byte)InvoiceState.Pending;
                    }
            }

            return null;
        }

        private byte? UpdateDeliveryStatusForInvoice(byte? sendoStatus, byte? deliveryStatus)
        {
            if (sendoStatus.HasValue && deliveryStatus.HasValue && sendoStatus == (byte)SendoOrderStatus.Cancelled &&
                deliveryStatus == (byte)SendoDeliveryStatus.ReturnSellerReceived)
            {
                return (byte)DeliveryStatus.Returned;
            }

            if (sendoStatus.HasValue && deliveryStatus.HasValue && sendoStatus == (byte)SendoOrderStatus.Cancelled &&
                deliveryStatus == (byte)SendoDeliveryStatus.ReturnSeller)
            {
                return (byte)DeliveryStatus.Returning;
            }
            switch (sendoStatus)
            {
                case (byte)SendoOrderStatus.Shipping:
                case (byte)SendoOrderStatus.Pod:
                    {
                        return (byte)DeliveryStatus.Delivering;
                    }
                case (byte)SendoOrderStatus.Completed:
                    {
                        return (byte)DeliveryStatus.Delivered;
                    }
            }
            return null;
        }

        public async Task<(byte?, string, string)> GetLogisticsStatus(int retailerId, long channelId, LogObjectMicrosoftExtension logInfo, string orderId, ChannelAuth auth, bool isGetOrderStatus = false, Platform platform = null)
        {
            (byte?, string, string) result = (null, null, null);
            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                Log.Error($"{KeywordCommonSearch.CallSendoError} EmptyAccessToken SendoGetLogisticsStatus - Context: {new ExceptionContextModel { RetailerId = retailerId, ChannelId = channelId, OrderIds = new List<object> { orderId }, Auth = auth, LogId = logInfo.Id }.ToJson()}");
                throw new KvException("EmptyAccessToken");
            }

            var orderDetail = await SendoHelper.GetOrdersDetailLoggerOld(auth.AccessToken, orderId, channelId);
            if (orderDetail != null && (orderDetail.Data.Error != null || orderDetail.Data == null))
            {
                result.Item1 = UpdateDeliveryStatusForInvoice(orderDetail.Data.Result.SaleOrder.OrderStatus, orderDetail.Data.Result.SaleOrder.DeliveryStatus);
                result.Item2 = orderDetail.Data.Result.SaleOrder.TrackingNumber;
            }

            return result;
        }

        public async Task<List<KvInvoice>> GetInvoiceDetailByOrderId(int retailerId, Guid logId, ChannelAuth auth,
            string orderId, long channelId, LogObjectMicrosoftExtension loggerExtension, Platform platform)
        {
            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                var ex = new OmniException(
                    $"{KeywordCommonSearch.CallSendoError} EmptyAccessToken SendoGetLogisticsStatus");
                loggerExtension.LogError(ex);
                throw ex;
            }

            try
            {
                loggerExtension.EcommerceRequest = Ecommerce.Sendo;
                var orderDetail = await SendoHelper.GetOrdersDetail(auth.AccessToken, orderId, channelId, loggerExtension);
                loggerExtension.EcommerceRequest = string.Empty;
                if (orderDetail == null || (orderDetail.Data.Error == null && orderDetail.Data != null)) return null;
                var kvInvoice = new KvInvoice
                {
                    Code = $"HDSDO_{orderId}",
                };
                kvInvoice.InvoiceDetails =
                    (from detail in orderDetail.Data.Result.SkuDetails
                     select new InvoiceDetail
                     {
                         ProductChannelId = !string.IsNullOrEmpty(detail.AttributeHash)
                         ? $"{detail.ProductVariantId}_{detail.AttributeHash}"
                         : detail.ProductVariantId,
                         ParentChannelProductId = detail.ProductVariantId,
                         ProductChannelSku = detail.Sku,
                         ProductChannelName = detail.ProductName,
                         Quantity = detail.Quantity,
                         Price = detail.Price
                     }).ToList();

                return new List<KvInvoice> { kvInvoice };
            }
            catch (Exception ex)
            {
                loggerExtension.LogError(ex);
                throw;
            }
        }

        public async Task<(byte?, byte?, byte?)> GetOrderStatus(int retailerId, long channelId, Guid logId,
            string orderId, ChannelAuth auth, LogObjectMicrosoftExtension loggerExtension,
            bool isGetDeliveryStatus = false, Platform platform = null, OmniChannelSettingObject settings = null)
        {
            var getOrderStatusLog = new LogObject(Log, logId)
            {
                Action = string.Format(EcommerceTypeException.GetOrderStatus, Ecommerce.Sendo),
                RetailerId = retailerId,
                OmniChannelId = channelId,
                RequestObject = orderId
            };
            var sendoOrderDetail =
                await SendoHelper.GetOrdersDetail(auth.AccessToken, orderId, channelId, loggerExtension);
            getOrderStatusLog.ResponseObject = sendoOrderDetail?.Content;
            getOrderStatusLog.LogInfo();
            var orderDetail = sendoOrderDetail?.Data?.Result;
            var orderStatus = orderDetail == null ? null : ConvertToKvOrderStatus(orderDetail.SaleOrder.OrderStatus);
            var invoiceStatus =
                orderDetail == null ? null : ConvertToKvInvoiceStatus(orderDetail.SaleOrder.OrderStatus);
            var deliveryStatus = orderDetail == null
                ? null
                : UpdateDeliveryStatusForInvoice(orderDetail.SaleOrder.OrderStatus,
                    orderDetail.SaleOrder.DeliveryStatus);
            return (orderStatus, invoiceStatus, deliveryStatus);
        }

        public Task<ShopInfoResponseV2> GetShopStatus(string accessToken)
        {
            throw new NotImplementedException();
        }

        public Task EnableUpdateProduct(string accessToken)
        {
            throw new NotImplementedException();
        }

        public async Task<RefreshTokenResponse> RefreshAccessToken(string refreshToken, long channelId, Guid logId,
            ChannelAuth auth, Platform platform)
        {
            var refreshAccessTokenLog = new LogObject(Log, logId)
            {
                Action = string.Format(EcommerceTypeException.RefreshAccessToken, Ecommerce.Sendo),
                OmniChannelId = channelId
            };

            try
            {
                var shopKey = SendoHelper.GetShopKeyFromRefreshToken(refreshToken);
                var secretKey = SendoHelper.GetSecretKeyFromRefreshToken(refreshToken);
                var loginRes = await Policy
                    .HandleResult<IRestResponse<SendoRequestBase<SendoLoginResponse>>>(msg =>
                        msg.StatusCode != HttpStatusCode.OK)
                    .WaitAndRetryAsync(SelfAppConfig.ChannelRetry,
                        i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval),
                        (res, timeSpan, retryCount, context) =>
                        {
                            Log.Warn($"[LogId: {logId.ToString("N")}] SendoRefreshAccessTokenWaitAndRetry with {res.Result.StatusCode}. Waiting {timeSpan:g} before next retry. Retry attempt {retryCount}");
                        }).ExecuteAsync(() => SendoHelper.Login(shopKey, secretKey));

                var accessToken = loginRes?.Data?.Result?.Token;

                if (loginRes?.StatusCode == HttpStatusCode.BadRequest && string.IsNullOrEmpty(accessToken))
                {
                    refreshAccessTokenLog.ResponseObject = loginRes.Data;
                    refreshAccessTokenLog.Description = "SendoInvalidRefreshTokenCode Can not get access token";
                    refreshAccessTokenLog.LogError();

                    return new RefreshTokenResponse
                    {
                        IsDeactivateChannel = true
                    };
                }

                if (loginRes?.Data?.Result?.Expires == null) return null;

                var tokenResponse = new RefreshTokenResponse
                {
                    AccessToken = accessToken,
                    ExpiresIn = (int)(loginRes.Data.Result.Expires - DateTime.Now).TotalSeconds,
                    RefreshToken = refreshToken
                };

                refreshAccessTokenLog.Description = "Sendo refresh token successful";
                refreshAccessTokenLog.LogInfo();

                return tokenResponse;
            }
            catch (Exception e)
            {
                refreshAccessTokenLog.LogError(e);
                throw;
            }
        }

        public Task<CustomerResponse> GetCustomerByOrderId(int retailerId, long channelId, string orderId, ChannelAuth auth, Guid logId)
        {
            return null;
        }

        public async Task<List<SendoLocation>> GetSendoLocation(string accessToken)
        {
            var logObject = new LogObject(Log, Guid.NewGuid())
            {
                Action = "GetSendoLocation"
            };

            var locations = new List<SendoLocation>();

            var provincesResponse = await Policy.HandleResult<IRestResponse<SendoLocationResponse<SendoProvinceResponse>>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout)
              .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval), (res, timeSpan, retryCount, ct) =>
              {
                  Log.Warn($"Request failed with {res.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
              }).ExecuteAsync(() => SendoHelper.GetProvinces(accessToken));

            if (provincesResponse?.Data?.Result?.Count > 0)
            {
                foreach (var province in provincesResponse.Data.Result)
                {
                    var districtResponse = await Policy.HandleResult<IRestResponse<SendoLocationResponse<SendoDistrictResponse>>>(msg => msg.StatusCode == HttpStatusCode.RequestTimeout)
                     .WaitAndRetryAsync(SelfAppConfig.ChannelRetry, i => TimeSpan.FromSeconds(SelfAppConfig.ChannelRetryInterval), (res, timeSpan, retryCount, ct) =>
                     {
                         Log.Warn($"Request failed with {res.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                     }).ExecuteAsync(() => SendoHelper.GetDistricts(accessToken, province.Id));

                    if (districtResponse?.Data?.Result?.Count > 0)
                    {
                        foreach (var dis in districtResponse.Data.Result)
                        {
                            var location = new SendoLocation
                            {
                                DistrictId = dis.Id,
                                FullName = $"{province.Name} - {dis.Name}"
                            };

                            locations.Add(location);
                        }
                    }
                }
            }
            if (locations.Count > 0)
            {
                logObject.ResponseObject = locations;
                logObject.LogInfo();
            }
            else
            {
                logObject.ResponseObject = provincesResponse?.Content;
                logObject.LogError();
            }
            return locations;
        }

        Task<List<ShopeeAttribute>> IBaseClient.GetAttributeByShopId(int retailerId, long channelId, ChannelAuth auth,
            Guid logId, long categoryId, Platform platform)
        {
            throw new NotImplementedException();
        }

        public Task<ShopeeProductResponse> PushProductToChannel(int retailerId, long channelId, ChannelAuth auth,
            Guid logId, ShopeeProduct req, Platform platform, string kvProductAttributeStr)
        {
            throw new NotImplementedException();
        }

        public Task<CategoriesResp> GetCategories(int retailerId, long channelId, ChannelAuth auth, Guid logId,
            Platform platform)
        {
            throw new NotImplementedException();
        }

        public Task<ShopeeUploadImgResponse> UploadImg(ChannelAuth auth, int retailerId, long shopId,
            List<Image> images, Guid logId, Platform platform)
        {
            throw new NotImplementedException();
        }

        public Task<ProductListResponse> GetListDeletedProduct(ChannelAuth auth, long channelId, Guid logId,
            bool isFirstSync, KvInternalContext context = null, DateTime? updateTimeFrom = null)
        {
            throw new NotImplementedException();
        }

        public Task<KvOrder> GenListKvOrder(long shopId, int? saleChannelId, int branchId, long userId, int retailerId, OrderDetail shopeeOrder,
            OmniChannelSettingObject settings, LogObjectMicrosoftExtension logInfo, Platform platform)
            {
            throw new NotImplementedException();
        }

        Task<CustomerResponse> IBaseClient.GetCustomerByOrderId(int retailerId, long channelId, string orderId,
            ChannelAuth auth, Guid logId, LogObjectMicrosoftExtension loggerExtension, Platform platform)
        {
            throw new NotImplementedException();
        }
        Task<List<OrderDetail>> IBaseClient.GetListOrderDetail(ChannelAuth auth, string[] orderSnList, LogObjectMicrosoftExtension logInfo, Platform platform)
        { throw new NotImplementedException(); }

        public List<KvInvoice> GenKvInvoices(CreateInvoiceRequest request)
        {
            throw new NotImplementedException();
        }

        
        public Task<(bool, List<Transaction>)> GetListTransaction(ChannelAuth auth, DateTime createFrom, DateTime createTo,
            LogObjectMicrosoftExtension log, Platform platform)
        {
            throw new NotImplementedException();
        }

        public Task<SellerWareHouseV2Response> GetTikiWarehouse(string accessToken)
        {
            throw new NotImplementedException();
        }

        public Task<(bool, List<FinanceTransaction>)> GetFinanceTransactions(string token, long channelId, string orderSn, DateTime? startTime, DateTime? endTime, LogObjectMicrosoftExtension logInfo, bool isBreakIfMaximum)
        {
            throw new NotImplementedException();
        }

        #region private method
        private List<Product> GetNormalProducts(List<SendoProduct> products)
        {
            return products.Where(x => !x.IsConfigVariant)
                .Select(x => new Product
                {
                    ItemId = x.Id.ToString(),
                    BasePrice = x.Price,
                    SalePrice = x.Price,
                    ItemName = x.Name,
                    ItemSku = x.Sku,
                    Status = x.Status == SendoProductStatus.Approved ? "active" : "inactive",
                    Type = 1,
                    ItemImages = string.IsNullOrEmpty(x.Image) ? new List<string>() : new List<string> {x.Image},
                    ParentItemId = x.Id.ToString(),
                }).ToList();
        }

        public Task<List<Logistics>> GetLogisticsByShopId(int retailerId, long channelId, ChannelAuth auth, Guid logId, Platform platform)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}




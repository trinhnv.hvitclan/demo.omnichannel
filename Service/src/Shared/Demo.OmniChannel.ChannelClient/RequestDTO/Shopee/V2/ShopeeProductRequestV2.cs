﻿using System.Collections.Generic;
using System.Linq;
using Demo.OmniChannel.ShareKernel.Dtos;
using Newtonsoft.Json;

namespace Demo.OmniChannel.ChannelClient.RequestDTO.Shopee.V2
{
    public class ShopeeProductRequestV2
    {
        [JsonProperty(PropertyName = "category_id")]
        public long CategoryId { get; set; }
        [JsonProperty(PropertyName = "item_name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "item_sku")]
        public string ItemSku { get; set; }
        [JsonProperty(PropertyName = "variation_sku")]
        public string VariationSku { get; set; }
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
        [JsonProperty(PropertyName = "original_price")]
        public float Price { get; set; }
        [JsonProperty(PropertyName = "seller_stock")]
        public List<ShopeeSellerStockReqV2> Stock { get; set; }
        [JsonProperty(PropertyName = "attribute_list")]
        public List<ShopeeAttributeRequestV2> Attributes { get; set; }
        [JsonProperty(PropertyName = "image")]
        public ShopeeImageReqV2 Image { get; set; }
        [JsonProperty(PropertyName = "logistic_info")]
        public List<Logistics> Logistics { get; set; }
        [JsonProperty(PropertyName = "weight")]
        public float Weight { get; set; }
        [JsonProperty(PropertyName = "dimension")]
        public ShopeeDimensionReqV2 Dimension { get; set; }
        [JsonProperty(PropertyName = "brand")]
        public ShopeeBrandReqV2 Brand { get; set; }

        public void CopyFrom(ShopeeProduct from, List<long> logisticIdsFree)
        {
            CategoryId = from.CategoryId;
            Name = from.Name;
            if (!from.Variations.Any())
            {
                ItemSku = from.ItemSku;
            }
            Description = from.Description;
            Price = from.Price;
            Stock = new List<ShopeeSellerStockReqV2>()
            {
                new ShopeeSellerStockReqV2()
                {
                    Stock = from.Stock < 0 ? 0 : from.Stock
                }
            };
            Weight = from.Weight;
            Logistics = from.Logistics;
            Logistics.ForEach(item =>
            {
                if (logisticIdsFree.Contains(item.LogisticId))
                {
                    item.IsFree = true;
                }
            });
            Image = new ShopeeImageReqV2
            {
                ImageIdList = from.Images.Select(x => x.ImageId).ToList(),
            };
            Dimension = new ShopeeDimensionReqV2
            {
                Height = from.Height,
                Length = from.Length,
                Width = from.Width,
            };

            Attributes = from.Attributes?.Select(item => new ShopeeAttributeRequestV2()
            {
                AttributeId = item.AttributeId,
                ValueList = new List<ShopeeAttributeValueRequestV2>()
                {
                    new ShopeeAttributeValueRequestV2()
                    {
                        Value = item.Value,
                        ValueId = item.ValueId,
                        ValueUnit = item.ValueUnit,
                    }
                }
            }).ToList() ?? new List<ShopeeAttributeRequestV2>();
            Brand = new ShopeeBrandReqV2()
            {
                BrandId = from.Brand?.BrandId ?? 0
            };
        }

    }

    public class ShopeeSellerStockReqV2
    {
        [JsonProperty(PropertyName = "stock")]
        public int Stock { get; set; }
    }

    public class ShopeeBrandReqV2
    {
        [JsonProperty(PropertyName = "brand_id")]
        public int BrandId { get; set; }
    }

    public class ShopeeImageReqV2
    {
        [JsonProperty(PropertyName = "image_id_list")]
        public List<string> ImageIdList { get; set; }
    }

    public class ShopeeDimensionReqV2
    {
        [JsonProperty(PropertyName = "package_height")]
        public int? Height { get; set; }
        [JsonProperty(PropertyName = "package_length")]
        public int? Length { get; set; }
        [JsonProperty(PropertyName = "package_width")]
        public int? Width { get; set; }
    }
}

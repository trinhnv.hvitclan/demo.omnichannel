﻿
using Newtonsoft.Json;

namespace Demo.OmniChannel.ChannelClient.RequestDTO.Shopee
{
    public class ShopeeRefreshTokenRequestV2
    {
        [JsonProperty(PropertyName = "refresh_token")]
        public string RefreshToken { get; set; }
        [JsonProperty(PropertyName = "shop_id")]
        public long ShopeeId { get; set; }
        [JsonProperty(PropertyName = "partner_id")]
        public long PartnerId { get; set; }
    }
}

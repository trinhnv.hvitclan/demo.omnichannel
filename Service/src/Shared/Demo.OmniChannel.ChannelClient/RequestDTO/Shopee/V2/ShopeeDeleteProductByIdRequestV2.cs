﻿using Newtonsoft.Json;

namespace Demo.OmniChannel.ChannelClient.RequestDTO.Shopee.V2
{
    public class ShopeeDeleteProductByIdRequestV2
    {
        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }
    }
}

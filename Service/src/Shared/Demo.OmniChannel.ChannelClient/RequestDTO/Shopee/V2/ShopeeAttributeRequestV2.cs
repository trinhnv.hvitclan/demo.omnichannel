﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.RequestDTO.Shopee.V2
{
    public class ShopeeAttributeRequestV2
    {
        [JsonProperty(PropertyName = "attribute_id")]
        public long AttributeId { get; set; }
        [JsonProperty(PropertyName = "attribute_value_list")]
        public List<ShopeeAttributeValueRequestV2> ValueList { get; set; }
    }

    public class ShopeeAttributeValueRequestV2
    {
        [JsonProperty(PropertyName = "value_id")]
        public int ValueId { get; set; }
        [JsonProperty(PropertyName = "original_value_name")]
        public string Value { get; set; }
        [JsonProperty(PropertyName = "value_unit")]
        public string ValueUnit { get; set; }
    }
}

﻿
using Newtonsoft.Json;

namespace Demo.OmniChannel.ChannelClient.RequestDTO.Shopee
{
    public class ShopeeTokenRequestV2
    {
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }
        [JsonProperty(PropertyName = "shop_id")]
        public long ShopeeId { get; set; }
        [JsonProperty(PropertyName = "partner_id")]
        public long PartnerId { get; set; }
    }
}

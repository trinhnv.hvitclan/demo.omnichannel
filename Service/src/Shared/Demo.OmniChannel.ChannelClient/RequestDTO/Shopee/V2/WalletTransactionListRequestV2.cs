﻿using Newtonsoft.Json;

namespace Demo.OmniChannel.ChannelClient.RequestDTO.Shopee
{
    public class WalletTransactionListRequestV2
    {
    
        [JsonProperty(PropertyName = "shop_id")]
        public long ShopId { get; set; }
        [JsonProperty(PropertyName = "partner_id")]
        public long PartnerId { get; set; }
        [JsonProperty(PropertyName = "access_token")]
        public int AccessToken { get; set; }
        [JsonProperty(PropertyName = "sign")]
        public string Sign { get; set; }
        [JsonProperty(PropertyName = "page_no")]
        public int PageNo { get; set; }
        [JsonProperty(PropertyName = "page_size")]
        public int PageSize { get; set; }
        [JsonProperty(PropertyName = "create_time_from")]
        public long CreateTimeFrom { get; set; }
        [JsonProperty(PropertyName = "create_time_to")]
        public long CreateTimeTo { get; set; }
        [JsonProperty(PropertyName = "timestamp")]
        public long TimeStamp { get; set; }
    }
}

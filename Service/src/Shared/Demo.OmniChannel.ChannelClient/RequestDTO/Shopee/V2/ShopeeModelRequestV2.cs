﻿using Demo.OmniChannel.ShareKernel.Dtos;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace Demo.OmniChannel.ChannelClient.RequestDTO.Shopee.V2
{
    public class ShopeeModelRequestV2
    {
        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }
        [JsonProperty(PropertyName = "model_list")]
        public List<ShopeeModelListReqV2> ModelList { get; set; }

        public void CopyFrom(ShopeeProduct from, long itemId)
        {
            ItemId = itemId;
            if (from.Variations == null || !from.Variations.Any()) return;
            ModelList = new List<ShopeeModelListReqV2>();
            var count = 0;
            foreach (var variation in from.Variations)
            {
                ModelList.Add(new ShopeeModelListReqV2()
                {
                    TierIndex = new List<int>() { count },
                    SellerStock = new List<ShopeeModelSellerStockReqV2>()
                    {
                        new ShopeeModelSellerStockReqV2()
                        {
                            Stock = variation.Stock,
                        }
                    },
                    Price = variation.Price,
                    ModelSku = variation.VariationSku,
                });
                count++;
            }
        }
    }

    public class ShopeeModelListReqV2
    {
        [JsonProperty(PropertyName = "tier_index")]
        public List<int> TierIndex { get; set; }
        [JsonProperty(PropertyName = "seller_stock")]
        public List<ShopeeModelSellerStockReqV2> SellerStock { get; set; }
        [JsonProperty(PropertyName = "original_price")]
        public float Price { get; set; }
        [JsonProperty(PropertyName = "model_sku")]
        public string ModelSku { get; set; }
    }

    public class ShopeeModelSellerStockReqV2
    {
        [JsonProperty(PropertyName = "stock")]
        public int Stock { get; set; }
    }
}

﻿using Newtonsoft.Json;

namespace Demo.OmniChannel.ChannelClient.RequestDTO.Shopee.V2
{
    public class ShopeeLogisticsGetChannelListRequestV2
    {
        [JsonProperty(PropertyName = "shop_id")]
        public long ShopId { get; set; }
        [JsonProperty(PropertyName = "partner_id")]
        public long PartnerId { get; set; }
        [JsonProperty(PropertyName = "access_token")]
        public int AccessToken { get; set; }
        [JsonProperty(PropertyName = "sign")]
        public string Sign { get; set; }
        [JsonProperty(PropertyName = "timestamp")]
        public long TimeStamp { get; set; }
    }
}

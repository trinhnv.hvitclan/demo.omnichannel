﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.RequestDTO.Shopee
{
    public class ShopeeUpdateStockRequestV2
    {
        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }
        [JsonProperty(PropertyName = "stock_list")]
        public List<ShopeeStockList> StockList { get; set; }
    }

    public class ShopeeStockList
    {
        [JsonProperty(PropertyName = "model_id")]
        public long ModelId { get; set; }
        [JsonProperty(PropertyName = "seller_stock")]
        public List<ShopeeSellerStock> SellerStock { get; set; }
    }

    public class ShopeeSellerStock
    {
        [JsonProperty(PropertyName = "location_id")]
        public string LocationId { get; set; }
        [JsonProperty(PropertyName = "stock")]
        public int Stock { get; set; }
    }
}

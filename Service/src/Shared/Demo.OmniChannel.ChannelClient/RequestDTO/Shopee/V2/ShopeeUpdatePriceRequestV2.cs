﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.RequestDTO.Shopee
{
    public class ShopeeUpdatePriceRequestV2
    {
        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }
        [JsonProperty(PropertyName = "price_list")]
        public List<ShopeePriceList> PriceList { get; set; }
    }

    public class ShopeePriceList
    {
        [JsonProperty(PropertyName = "model_id")]
        public long ModelId { get; set; }
        [JsonProperty(PropertyName = "original_price")]
        public float OriginalPrice { get; set; }
    }
}

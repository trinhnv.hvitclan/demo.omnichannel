using Demo.OmniChannel.ShareKernel.Dtos;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace Demo.OmniChannel.ChannelClient.RequestDTO.Shopee.V2
{
    public class ShopeeInitTierRequestV2
    {
        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }
        [JsonProperty(PropertyName = "tier_variation")]
        public List<ShopeeInitTierVariationRequestV2> TierVariation { get; set; }
        [JsonProperty(PropertyName = "model")]
        public List<ShopeeModelListReqV2> ModelList { get; set; }
        public void CopyFrom(ShopeeProduct from, long itemId, string kvProductAttributeStr)
        {
            ItemId = itemId;
            if (from.Variations == null || !from.Variations.Any()) return;
            TierVariation = InitTier(from, kvProductAttributeStr);
            var tierNameList = TierVariation.SelectMany(x => x.OptionList).Select(x => x.Option).ToList();
            ModelList = from.Variations.Select(x => new ShopeeModelListReqV2()
            {
                TierIndex = new List<int>() { tierNameList.IndexOf(x.Name) },
                SellerStock = new List<ShopeeModelSellerStockReqV2>()
                    {
                        new ShopeeModelSellerStockReqV2()
                        {
                            Stock = x.Stock,
                        }
                    },
                Price = x.Price,
                ModelSku = x.VariationSku,
            }).ToList();
        }


        public List<ShopeeInitTierVariationRequestV2> InitTier(ShopeeProduct from, string kvProductAttributeStr)
        {
            TierVariation = new List<ShopeeInitTierVariationRequestV2>();

            TierVariation.Add(new ShopeeInitTierVariationRequestV2()
            {
                Name = "Loại hàng",
                OptionList = from.Variations.Select(x => new ShopeeTierVariationOptionListRequestV2()
                {
                    Option = x.Name,
                }).ToList(),
            });
            return TierVariation;
        }
    }

    public class ShopeeInitTierVariationRequestV2
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "option_list")]
        public List<ShopeeTierVariationOptionListRequestV2> OptionList { get; set; }
    }

    public class ShopeeTierVariationOptionListRequestV2
    {
        [JsonProperty(PropertyName = "option")]
        public string Option { get; set; }
        public List<long> ProductIds { get; set; }
    }

    public class KvProductAttribute
    {
        public long Id { get; set; }
        public long AttributeId { get; set; }
        public long ProductId { get; set; }
        public string Value { get; set; }
        public KvAttribute Attribute { get; set; }
    }

    public class KvAttribute
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}

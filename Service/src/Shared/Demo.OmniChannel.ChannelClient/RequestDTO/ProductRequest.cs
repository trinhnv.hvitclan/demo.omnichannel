﻿using System;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.RequestDTO
{
    public class MultiProductItem
    {
        public string ItemSku { get; set; }
        public string ItemId { get; set; }
        public string ParentItemId { get; set; }
        public byte ItemType { get; set; }
        public double OnHand { get; set; }
        public decimal? Price { get; set; }
        public decimal? SalePrice { get; set; }
        public DateTime? StartSaleDate { get; set; }
        public DateTime? EndSaleDate { get; set; }
        public string KvProductSku { get; set; }
    }
}
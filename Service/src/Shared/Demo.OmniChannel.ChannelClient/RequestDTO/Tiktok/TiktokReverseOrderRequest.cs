﻿
using Newtonsoft.Json;

namespace Demo.OmniChannel.ChannelClient.RequestDTO.Tiktok
{
    public class TiktokReverseOrderRequest
    {
        [JsonProperty(PropertyName = "offset")]
        public int Offset { get; set; }

        [JsonProperty(PropertyName = "size")]
        public int Size { get; set; }

        [JsonProperty(PropertyName = "order_id")]
        public long OrderId { get; set; }
    }
}

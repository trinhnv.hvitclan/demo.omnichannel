﻿
using Newtonsoft.Json;

namespace Demo.OmniChannel.ChannelClient.RequestDTO.Shopee
{
    public class TiktokTokenRequest
    {
        [JsonProperty(PropertyName = "app_key")]
        public string AppKey { get; set; }

        [JsonProperty(PropertyName = "app_secret")]
        public string AppSecret { get; set; }

        [JsonProperty(PropertyName = "auth_code")]
        public string AuthCode { get; set; }

        [JsonProperty(PropertyName = "grant_type")]
        public string GrantType { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.RequestDTO.Shopee
{
    public class TiktokUpdateStockRequest
    {
        [JsonProperty(PropertyName = "product_id")]
        public string ProductId { get; set; }

        [JsonProperty(PropertyName = "skus")]
        public List<TiktokSku> Skus { get; set; }
    }

    public class TiktokSku
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonIgnore]
        public string ItemSku { get; set; }
        [JsonProperty(PropertyName = "stock_infos")]
        public List<TiktokSkuInfo> StockInfos { get; set; }
    }
    public class TiktokSkuInfo
    {
        [JsonProperty(PropertyName = "warehouse_id")]
        public string WareHouseId { get; set; }

        [JsonProperty(PropertyName = "available_stock")]
        public int AvailableStock { get; set; }
    }
}

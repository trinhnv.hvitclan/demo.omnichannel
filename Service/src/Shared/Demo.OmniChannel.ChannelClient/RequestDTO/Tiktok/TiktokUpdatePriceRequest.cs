﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.RequestDTO.Shopee
{
    public class TiktokUpdatePriceRequest
    {
        [JsonProperty(PropertyName = "product_id")]
        public string ProductId { get; set; }

        [JsonProperty(PropertyName = "skus")]
        public List<TiktokPriceSku> Skus { get; set; }
    }

    public class TiktokPriceSku
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonIgnore]
        public string ItemSku { get; set; }
        [JsonProperty(PropertyName = "original_price")]
        public string OriginalPrice { get; set; }
    }
}

﻿using Newtonsoft.Json;

namespace Demo.OmniChannel.ChannelClient.RequestDTO.Tiktok
{
    public class TiktokOrderDetailRequest
    {
        [JsonProperty(PropertyName = "order_id_list")]
        public string[] OrderIdLst { get; set; }
    }
}

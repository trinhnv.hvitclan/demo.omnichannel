﻿
using Newtonsoft.Json;

namespace Demo.OmniChannel.ChannelClient.RequestDTO.Shopee
{
    public class TiktokRefreshTokenRequest
    {
        [JsonProperty(PropertyName = "app_key")]
        public string AppKey { get; set; }

        [JsonProperty(PropertyName = "app_secret")]
        public string AppSecret { get; set; }

        [JsonProperty(PropertyName = "refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty(PropertyName = "grant_type")]
        public string GrantType { get; set; }
    }
}

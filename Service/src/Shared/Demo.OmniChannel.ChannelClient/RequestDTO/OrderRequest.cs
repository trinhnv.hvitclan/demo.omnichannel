﻿using System;
using System.Collections.Generic;
using Demo.OmniChannel.ChannelClient.Models;

namespace Demo.OmniChannel.ChannelClient.RequestDTO
{
    public class OrderRequest
    {
        public string Status { get; set; }
        public DateTime? LastSync { get; set; }
        public int? SyncOrderFormula { get; set; }
        public long ChannelId { get; set; }
    }

    public class CreateOrderRequest
    {
        public string OrderId { get; set; }
        public int GroupId { get; set; }
        public string Order { get; set; }
        public int BranchId { get; set; }
        public long UserId { get; set; }
        public long PriceBookId { get; set; }
        public int SaleChannelId { get; set; }
        public string RequestId { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateAt { get; set; }
        public string OrderStatus { get; set; }
        public int RetryCount { get; set; }
        public List<StatusHistory> StatusHistories { get; set; }
    }

    public class CreateInvoiceRequest
    {
        public string OrderId { get; set; }
        public string Order { get; set; }
        public int BranchId { get; set; }
        public long UserId { get; set; }
        public long PriceBookId { get; set; }
        public int SaleChannelId { get; set; }
        public KvOrder KvOrder { get; set; }
        public DeliveryInfoDTO KvDelivery { get; set; }
        public List<string> Codes  { get; set; }
        public int RetryCount { get; set; }
        public bool IsConfirmReturning { get; set; }
    }
}
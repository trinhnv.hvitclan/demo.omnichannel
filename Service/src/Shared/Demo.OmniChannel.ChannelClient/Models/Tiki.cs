﻿using System;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;

namespace Demo.OmniChannel.ChannelClient.Models
{
    #region Base
    public class TikiListBaseRequest
    {
        [JsonProperty(PropertyName = "page")]
        public int Page { get; set; }
        [JsonProperty(PropertyName = "limit")]
        public int Limit { get; set; }
    }

    public class TikiPage
    {
        [JsonProperty(PropertyName = "total")]
        public int Total { get; set; }
        [JsonProperty(PropertyName = "current_page")]
        public int CurrentPage { get; set; }
        [JsonProperty(PropertyName = "last_page")]
        public int LastPage { get; set; }
    }
    #endregion

    #region SellerInfo
    public class SellerInfoResponse
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }
        [JsonProperty(PropertyName = "sid")]
        public string SId { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }
        [JsonProperty(PropertyName = "logo")]
        public string Logo { get; set; }
        [JsonProperty(PropertyName = "active")]
        public byte Active { get; set; }
        [JsonProperty(PropertyName = "can_update_product")]
        public byte CanUpdateProduct { get; set; }
        [JsonProperty(PropertyName = "registration_status")]
        public string RegistrationStatus { get; set; }
        [JsonProperty(PropertyName = "errors")]
        public List<string> Errors { get; set; }
    }

    public class SellerUpdateCanEditProductRequest
    {
        [JsonProperty(PropertyName = "can_update_product")]
        public byte CanUpdateProduct { get; set; }
    }

    public class SellerUpdateCanEditProductResponse
    {
        [JsonProperty(PropertyName = "errors")]
        public List<string> Errors { get; set; }
        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }
    }
    #endregion

    #region Product
    public class TikiProductsListResponse
    {

        [JsonProperty(PropertyName = "data")]
        public List<TikiProduct> Products { get; set; }
        [JsonProperty(PropertyName = "paging")]
        public TikiPage Paging { get; set; }
    }

    public class TikiProduct
    {
        [JsonProperty(PropertyName = "product_id")]
        public long ProductId { get; set; }
        [JsonProperty(PropertyName = "sku")]
        public string Sku { get; set; }
        [JsonProperty(PropertyName = "master_sku")]
        public string MasterSku { get; set; }
        [JsonProperty(PropertyName = "original_sku")]
        public string OriginalSku { get; set; }
        [JsonProperty(PropertyName = "master_id")]
        public long MasterId { get; set; }
        [JsonProperty(PropertyName = "super_sku")]
        public string SuperSku { get; set; }
        [JsonProperty(PropertyName = "super_id")]
        public long SuperId { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "entity_type")]
        public string EntityType { get; set; }
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
        [JsonProperty(PropertyName = "price")]
        public decimal Price { get; set; }
        [JsonProperty(PropertyName = "active")]
        public byte Active { get; set; }
        [JsonProperty(PropertyName = "created_at")]
        public string CreatedAt { get; set; }
        [JsonProperty(PropertyName = "created_by")]
        public string CreatedBy { get; set; }
        [JsonProperty(PropertyName = "thumbnail")]
        public string Thumbnail { get; set; }
        [JsonProperty(PropertyName = "images")]
        public List<TikiImage> Images { get; set; }
        [JsonProperty(PropertyName = "categories")]
        public List<object> Categories { get; set; }
        [JsonProperty(PropertyName = "inventory")]
        public TikiInventory Inventory { get; set; }
    }

    public class TikiProductDetail
    {
        [JsonProperty(PropertyName = "product_id")]
        public long ProductId { get; set; }
        [JsonProperty(PropertyName = "sku")]
        public string Sku { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "attributes")]
        public TikiAttributes Attributes { get; set; }
        [JsonProperty(PropertyName = "images")]
        public List<TikiImage> Images { get; set; }
        
    }

    public class TikiImage
    {
        [JsonProperty(PropertyName = "url")]
        public string Url { get; set; }
    }

    public class TikiInventory
    {
        [JsonProperty(PropertyName = "inventory_type")]
        public string InventoryType { get; set; }
        [JsonProperty(PropertyName = "fulfillment_type")]
        public string FulfillmentType { get; set; }
    }
    public class TikiAttributes
    {
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
    }

    public class UpdateProductTikiRequest
    {
        [JsonProperty(PropertyName = "product_id")]
        public long ProductId { get; set; }
        [JsonProperty(PropertyName = "price")]
        public decimal? Price { get; set; }
        [JsonProperty(PropertyName = "quantity")]
        public double? Quantity { get; set; }
        [JsonProperty(PropertyName = "active")]
        public int? Active { get; set; }
    }

    public class UpdateProductTikiResponse
    {
        [JsonProperty(PropertyName = "errors")]
        public List<string> Errors { get; set; }
    }
    #endregion

    public class TikiOrdersListRequest : TikiListBaseRequest
    {
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
        [JsonProperty(PropertyName = "created_from_date")]
        public string CreatedFromDate { get; set; }
        [JsonProperty(PropertyName = "created_to_date")]
        public string CreatedToDate { get; set; }

    }
    public class TikiOrderResponse
    {
        [JsonProperty(PropertyName = "data")]
        public List<TikiOrder> Orders { get; set; }
        [JsonProperty(PropertyName = "paging")]
        public TikiPage Paging { get; set; }

        [JsonProperty(PropertyName = "errors")]
        public List<string> Errors { get; set; }

    }
    public class TikiOrder
    {
        [JsonProperty(PropertyName = "order_code")]
        public string OrderCode { get; set; }
        [JsonProperty(PropertyName = "coupon_code")]
        public string CouponCode { get; set; }
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
        [JsonProperty(PropertyName = "total_price_before_discount")]
        public decimal BasePrice { get; set; }
        [JsonProperty(PropertyName = "total_price_after_discount")]
        public decimal Price { get; set; }
        [JsonProperty(PropertyName = "created_at")]
        public DateTime? CreatedAt { get; set; }
        [JsonProperty(PropertyName = "updated_at")]
        public DateTime? UpdatedAt { get; set; }
        [JsonProperty(PropertyName = "purchased_at")]
        public DateTime? PurchasedAt { get; set; }
        [JsonProperty(PropertyName = "fulfillment_type")]
        public string FulfillmentType { get; set; }
        [JsonProperty(PropertyName = "shipping")]
        public TikiShipping Shipping { get; set; }
        [JsonProperty(PropertyName = "items")]
        public List<TikiOrderItem> Items { get; set; }
        [JsonProperty(PropertyName = "discount")]
        public TikiDiscount Discount { get; set; }
        [JsonProperty(PropertyName = "errors")]
        public List<string> Errors { get; set; }
        public TikiOrder()
        {
            Items = new List<TikiOrderItem>();
        }
    }
    public class TikiDiscount
    {
        [JsonProperty(PropertyName = "discount_amount")]
        public decimal Amount { get; set; }
        [JsonProperty(PropertyName = "discount_coupon")]
        public decimal Coupon { get; set; }
        [JsonProperty(PropertyName = "discount_tiki_point")]
        public decimal TikiPoint { get; set; }
    }
    public class TikiShipping
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "street")]
        public string Street { get; set; }
        [JsonProperty(PropertyName = "ward")]
        public string Ward { get; set; }
        [JsonProperty(PropertyName = "city")]
        public string City { get; set; }
        [JsonProperty(PropertyName = "region")]
        public string Region { get; set; }
        [JsonProperty(PropertyName = "country")]
        public string Country { get; set; }
        [JsonProperty(PropertyName = "phone")]
        public string Phone { get; set; }
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }
        [JsonProperty(PropertyName = "estimate_description")]
        public string EstimateDescription { get; set; }
        [JsonProperty(PropertyName = "shipping_fee")]
        public string ShippingFee { get; set; }
    }

    public class TikiOrderItem
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }
        [JsonProperty(PropertyName = "product_id")]
        public long ProductId { get; set; }
        [JsonProperty(PropertyName = "product_name")]
        public string ProductName { get; set; }
        [JsonProperty(PropertyName = "sku")]
        public string Sku { get; set; }
        [JsonProperty(PropertyName = "original_sku")]
        public string OriginalSku { get; set; }
        [JsonProperty(PropertyName = "qty")]
        public float Qty { get; set; }
        [JsonProperty(PropertyName = "price")]
        public decimal Price { get; set; }
        [JsonProperty(PropertyName = "discount")]
        public TikiDiscount DiscountItem { get; set; }
        [JsonProperty(PropertyName = "inventory_type")]
        public string InventoryType { get; set; }
    }

    public class TikiRefeshTokenError
    {
        [JsonProperty(PropertyName = "error")]
        public string TikiError { get; set; }
        [JsonProperty(PropertyName = "error_debug")]
        public string TikiErrorDebug { get; set; }
    }

    public class RegistrationStatus
    {
        public const string Draft = "draft";
        public const string Waiting = "waiting";
        public const string SellerSupplementing = "seller_supplementing";
        public const string KamRejected = "kam_rejected";
        public const string Completed = "completed";
    }
}
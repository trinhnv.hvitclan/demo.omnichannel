﻿using System;

namespace Demo.OmniChannel.ChannelClient.Models
{
    public class SurCharge
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public bool ForAllBranch { get; set; }

        public Decimal? Value { get; set; }

        public double? ValueRatio { get; set; }

        public bool isActive { get; set; }

        public bool isAuto { get; set; }

        public int RetailerId { get; set; }

        public DateTime CreatedDate { get; set; }

        public long CreatedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public long? ModifiedBy { get; set; }

        public byte? Order { get; set; }

        public bool? isReturnAuto { get; set; }
        public decimal? Price { get; set; }
        public decimal? SurValue { get; set; }
        public byte? Type { get; set; }
    }
}

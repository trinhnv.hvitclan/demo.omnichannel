﻿using Demo.OmniChannel.ShareKernel.Dtos;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace Demo.OmniChannel.ChannelClient.Models
{
    #region Request
    public class BaseRequest
    {
        [JsonProperty(PropertyName = "timestamp")]
        public long TimeStamp { get; set; }
        [JsonProperty(PropertyName = "shopid")]
        public long ShopId { get; set; }
        [JsonProperty(PropertyName = "partner_id")]
        public long PartnerId { get; set; }
    }
    public class IncludeLanguageBaseRequest : BaseRequest
    {
        [JsonProperty(PropertyName = "language")]
        public string Language { get; set; }
    }

    #region Shop
    public class ShopRequest
    {
        [JsonProperty(PropertyName = "shopid")]
        public long ShopId { get; set; }
        [JsonProperty(PropertyName = "partner_id")]
        public long PartnerId { get; set; }
        [JsonProperty(PropertyName = "timestamp")]
        public long TimeStamp { get; set; }
    }
    #endregion

    #region Product
    public class ProductRequest : BaseRequest
    {
        [JsonProperty(PropertyName = "pagination_offset")]
        public int PaginationOffset { get; set; }
        [JsonProperty(PropertyName = "pagination_entries_per_page")]
        public int PaginationEntriesPerPage { get; set; }
        [JsonProperty(PropertyName = "update_time_from", NullValueHandling = NullValueHandling.Ignore)]
        public long? UpdateTimeFrom { get; set; }
        [JsonProperty(PropertyName = "update_time_to", NullValueHandling = NullValueHandling.Ignore)]
        public long? UpdateTimeTo { get; set; }
        [JsonProperty(PropertyName = "need_deleted_item")]
        public bool NeedDeletedItem { get; set; }
    }

    public class ProductDetailShopeeRequest : BaseRequest
    {
        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }
    }

    public class UpdatePriceVariationReq : BaseRequest
    {
        [JsonProperty(PropertyName = "variations")]
        public List<Variations> Variations { get; set; }
    }

    public class UpdatePriceNotVarReq : BaseRequest
    {
        [JsonProperty(PropertyName = "items")]
        public List<ProductItem> Items { get; set; }

    }
    public class UpdateStockRequest : BaseRequest
    {
        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }
        [JsonProperty(PropertyName = "variation_id")]
        public long? VariationId { get; set; }
        [JsonProperty(PropertyName = "stock")]
        public int Stock { get; set; }
    }

    public class UpdatePriceRequest : BaseRequest
    {
        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }
        [JsonProperty(PropertyName = "variation_id")]
        public long? VariationId { get; set; }
        [JsonProperty(PropertyName = "price")]
        public float Price { get; set; }
    }

    public class UpdateMultiNormalStockRequest : BaseRequest
    {
        [JsonProperty(PropertyName = "items")]
        public List<ProductItem> Items { get; set; }
    }
    public class UpdateMultiNormalPriceRequest : BaseRequest
    {
        [JsonProperty(PropertyName = "items")]
        public List<ProductItem> Items { get; set; }
    }
    public class UpdateMultiVariationStockRequest : BaseRequest
    {
        [JsonProperty(PropertyName = "variations")]
        public List<Variations> Variations { get; set; }
    }
    public class UpdateMultiVariationPriceRequest : BaseRequest
    {
        [JsonProperty(PropertyName = "variations")]
        public List<Variations> Variations { get; set; }
    }
    #endregion

    #region Order

    public class GetListOrderRequest : BaseRequest
    {
        [JsonProperty(PropertyName = "pagination_offset")]
        public int PaginationOffset { get; set; }
        [JsonProperty(PropertyName = "pagination_entries_per_page")]
        public int PaginationEntriesPerPage { get; set; }
        [JsonProperty(PropertyName = "update_time_from", NullValueHandling = NullValueHandling.Ignore)]
        public long? UpdateTimeFrom { get; set; }
        [JsonProperty(PropertyName = "update_time_to", NullValueHandling = NullValueHandling.Ignore)]
        public long? UpdateTimeTo { get; set; }
        [JsonProperty(PropertyName = "create_time_from", NullValueHandling = NullValueHandling.Ignore)]
        public long? CreateTimeFrom { get; set; }
        [JsonProperty(PropertyName = "create_time_to", NullValueHandling = NullValueHandling.Ignore)]
        public long? CreateTimeTo { get; set; }
        [JsonProperty(PropertyName = "order_status", NullValueHandling = NullValueHandling.Ignore)]
        public string Status { get; set; }
    }
    public class GetOrderDetailRequest : BaseRequest
    {
        [JsonProperty(PropertyName = "ordersn_list")]
        public string[] OrderSnList { get; set; }
    }
    public class EscrowDetailRequest : BaseRequest
    {
        [JsonProperty(PropertyName = "ordersn")]
        public string OrderSn { get; set; }
    }

    public class LogisticsMessageRequest : BaseRequest
    {
        [JsonProperty(PropertyName = "ordersn")]
        public string OrderSn { get; set; }
    }

    public class MyIncomeMessageRequest : BaseRequest
    {
        [JsonProperty(PropertyName = "ordersn")]
        public string OrderSn { get; set; }
    }
    #endregion

    #endregion

    #region Response
    #region Shop
    public class ShopResponse : BaseShopeeResponse
    {
        [JsonProperty(PropertyName = "shop_id")]
        public long ShopId { get; set; }
        [JsonProperty(PropertyName = "shop_name")]
        public string ShopName { get; set; }
        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }
    }
    #endregion

    #region Products
    public class UpdateMultiResponse : BaseShopeeResponse
    {
        [JsonProperty(PropertyName = "batch_result")]
        public BatchResult BatchResult { get; set; }
    }
    public class ProductResponse : BaseShopeeResponse
    {
        [JsonProperty(PropertyName = "items")]
        public List<ProductItem> Items { get; set; }
        [JsonProperty(PropertyName = "more")]
        public bool More { get; set; }
    }

    public class ProductDetailShopeeResponse : BaseShopeeResponse
    {
        [JsonProperty(PropertyName = "item")]
        public ProductItemDetail Item { get; set; }

        public byte IsSuccess => (byte)HttpStatusCode.OK;
    }

    public class UpdatePriceVariationRes
    {
        [JsonProperty(PropertyName = "request_id")]
        public string RequestId { get; set; }
        [JsonProperty(PropertyName = "batch_result")]
        public BatchResult BatchResult { get; set; }
    }

    public class UpdatePriceNotVarRes
    {
        [JsonProperty(PropertyName = "request_id")]
        public string RequestId { get; set; }
        [JsonProperty(PropertyName = "batch_result")]
        public BatchResult BatchResult { get; set; }

    }

    public class UpdateStockResponse : BaseShopeeResponse
    {
        [JsonProperty(PropertyName = "item")]
        public UpdateStockItem Item { get; set; }

    }
    public class UpdatePriceResponse : BaseShopeeResponse
    {

        [JsonProperty(PropertyName = "item")]
        public UpdatePriceItem Item { get; set; }
    }
    #endregion

    #region Order
    public class LogisticsMessageResponse : BaseShopeeResponse
    {
        [JsonProperty(PropertyName = "tracking_number")]
        public string TrackingNumber { get; set; }
        [JsonProperty(PropertyName = "tracking_info")]
        public List<TrackingInfo> TrackingInfos { get; set; }
        [JsonProperty(PropertyName = "logistics_status")]
        public string LogisticsStatus { get; set; }
    }
    public class OrderDetailResponse : BaseShopeeResponse
    {
        [JsonProperty(PropertyName = "errors")]
        public string[] Errors { get; set; }
        [JsonProperty(PropertyName = "orders")]
        public List<OrderDetail> OrderDetails { get; set; }
    }
    public class OrderListResponse : BaseShopeeResponse
    {
        [JsonProperty(PropertyName = "orders")]
        public List<OrderListItem> Orders { get; set; }
        [JsonProperty(PropertyName = "more")]
        public bool More { get; set; }
    }

    public class Logistics
    {
        [JsonProperty(PropertyName = "logistic_id")]
        public long LogisticId { get; set; }

        [JsonProperty(PropertyName = "logistic_name")]
        public string LogisticName { get; set; }

        [JsonProperty(PropertyName = "has_cod")]
        public bool HasCod { get; set; }

        [JsonProperty(PropertyName = "enabled")]
        public bool Enabled { get; set; }

        [JsonProperty(PropertyName = "fee_type")]
        public string FeeType { get; set; }

        [JsonProperty(PropertyName = "sizes")]
        public List<ShopeeSizes> Sizes { get; set; }

        [JsonProperty(PropertyName = "weight_limits")]
        public WeightLimit WeightLimit { get; set; }

        [JsonProperty(PropertyName = "item_max_dimension")]
        public ItemMaxDimension ItemMaxDimension { get; set; }

        [JsonProperty(PropertyName = "logistics_description")]
        public string LogisticsDescription { get; set; }

        [JsonProperty(PropertyName = "force_enabled")]
        public bool ForceEnabled { get; set; }

        [JsonProperty(PropertyName = "mask_channel_id")]
        public long MaskChannelId { get; set; }
    }

    public class LogisticsResponse : BaseShopeeResponse
    {
        [JsonProperty(PropertyName = "logistics")]
        public List<Logistics> Logistics { get; set; }

    }

    public class ShopeeSizes
    {
        [JsonProperty(PropertyName = "size_id")]
        public long SizeId { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "default_price")]
        public decimal DefaultPrice { get; set; }
    }

    public class WeightLimit
    {
        [JsonProperty(PropertyName = "item_max_weight")]
        public float ItemMaxWeight { get; set; }

        [JsonProperty(PropertyName = "item_min_weight")]
        public float ItemMinWeight { get; set; }
    }

    public class ItemMaxDimension
    {
        [JsonProperty(PropertyName = "height")]
        public float Height { get; set; }

        [JsonProperty(PropertyName = "width")]
        public float Width { get; set; }

        [JsonProperty(PropertyName = "length")]
        public float Length { get; set; }

        [JsonProperty(PropertyName = "unit")]
        public string Unit { get; set; }
    }

    public class EscrowDetailResponse : BaseShopeeResponse
    {
        [JsonProperty(PropertyName = "order")]
        public EscrowDetail EscrowDetail { get; set; }
    }

    public class MyIncomeResponse : BaseShopeeResponse
    {
        [JsonProperty(PropertyName = "order_income")]
        public MyIncome MyIncome { get; set; }
    }
    #endregion

    #endregion

    #region Models

    #region Product
    public class UpdatePriceItem
    {
        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }
        [JsonProperty(PropertyName = "price")]
        public float Price { get; set; }
        [JsonProperty(PropertyName = "variation_id")]
        public long VariationId { get; set; }
    }

    public class UpdateStockItem
    {
        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }
        [JsonProperty(PropertyName = "variation_id")]
        public long VariationId { get; set; }
        [JsonProperty(PropertyName = "stock")]
        public int Stock { get; set; }
    }
    public class BatchResult
    {
        [JsonProperty(PropertyName = "modifications")]
        public List<Modification> Modifications { get; set; }
        [JsonProperty(PropertyName = "failures")]
        public List<Failure> Failures { get; set; }
    }
    public class Modification
    {
        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }
        [JsonProperty(PropertyName = "variation_id")]
        public long VariationId { get; set; }
        public string VariationSku { get; set; }
        public decimal? Price { get; set; }
        public int? Stock { get; set; }
    }

    public class Failure
    {
        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }
        [JsonProperty(PropertyName = "variation_id")]
        public long VariationId { get; set; }
        [JsonProperty(PropertyName = "error_description")]
        public string ErrorDescription { get; set; }
        public string VariationSku { get; set; }
    }

    public class ProductItem
    {
        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }
        [JsonProperty(PropertyName = "item_sku")]
        public string ItemSku { get; set; }
        [JsonProperty(PropertyName = "variations")]
        public List<Variations> Variations { get; set; }
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
        [JsonProperty(PropertyName = "price")]
        public decimal? Price { get; set; }
        [JsonProperty(PropertyName = "stock")]
        public int? Stock { get; set; }
        //[JsonProperty(PropertyName = "is_2tier_item")]
        //public bool Is2TierItem { get; set; }
    }

    public class ProductItemDetail
    {
        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }
        [JsonProperty(PropertyName = "item_sku")]
        public string ItemSku { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "images")]
        public List<string> Images { get; set; }
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
        [JsonProperty(PropertyName = "variations")]
        public List<Variations> Variations { get; set; }
    }

    public class Variations
    {
        [JsonProperty(PropertyName = "variation_id")]
        public long VariationId { get; set; }
        [JsonProperty(PropertyName = "variation_sku")]
        public string VariationSku { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "price")]
        public decimal? Price { get; set; }
        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }
        [JsonProperty(PropertyName = "stock")]
        public int? Stock { get; set; }

        public byte? Type { get; set; }
    }

    public class CategoriesResp : BaseShopeeResponse
    {
        [JsonProperty(PropertyName = "categories")]
        public List<Categories> Categories { get; set; }
    }

    #endregion

    #region Orders
    public class OrderListItem
    {
        [JsonProperty(PropertyName = "ordersn")]
        public string OrderSn { get; set; }
        [JsonProperty(PropertyName = "order_status")]
        public string OrderStatus { get; set; }
        [JsonProperty(PropertyName = "update_time")]
        public long UpdateTime { get; set; }
    }
    public class OrderDetail
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is mandatory")]
        [JsonProperty(PropertyName = "ordersn")]
        public string OrderSn { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is mandatory")]
        [JsonProperty(PropertyName = "recipient_address")]
        public RecipientAddress RecipientAddress { get; set; }

        [JsonProperty(PropertyName = "message_to_seller")]
        public string MessageToSeller { get; set; }

        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is mandatory")]
        [JsonProperty(PropertyName = "create_time")]
        public long CreateTime { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is mandatory")]
        [JsonProperty(PropertyName = "items")]
        public List<OrderDetailItem> OrderDetailItem { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is mandatory")]
        [JsonProperty(PropertyName = "order_status")]
        public string OrderStatus { get; set; }

        [JsonProperty(PropertyName = "tracking_no")]
        public string TrackingNo { get; set; }

        [JsonProperty(PropertyName = "buyer_username")]
        public string BuyerUsername { get; set; }

        [JsonProperty(PropertyName = "shipping_carrier")]
        public string ShippingCarrier { get; set; }
    }
    public class EscrowDetail
    {
        [JsonProperty(PropertyName = "ordersn")]
        public string OrderSn { get; set; }
        [JsonProperty(PropertyName = "income_details")]
        public IncomeDetail IncomeDetail { get; set; }
        [JsonProperty(PropertyName = "items")]
        public List<ItemDetail> Items { get; set; }
        [JsonProperty(PropertyName = "activity")]
        public List<ActivityDetail> ActivityDetails { get; set; }
    }

    public class MyIncome
    {
        [Description("Phí vận chuyển (không tính trợ giá)")]
        public float TotalShippingFee => -(FinalShippingFee + BuyerPaidShippingFee);

        [JsonProperty(PropertyName = "final_shipping_fee"), Description("Phí vận chuyển thực tế")]
        public float FinalShippingFee { get; set; }

        [JsonProperty(PropertyName = "commission_fee"), Description("Phí cố định")]
        public float CommissionFee { get; set; }

        [JsonProperty(PropertyName = "service_fee"), Description("Phí dịch vụ")]
        public float ServiceFee { get; set; }

        [JsonProperty(PropertyName = "seller_transaction_fee"), Description("Phí thanh toán")]
        public float SellerTransactionFee { get; set; }

        [JsonProperty(PropertyName = "buyer_paid_shipping_fee"), Description("phí vận chuyển ng mua trả")]
        public float BuyerPaidShippingFee { get; set; }
        [JsonProperty(PropertyName = "seller_coin_cash_back"), Description("Giá trị giảm giá hoàn xu")]
        public float SellerCoinCashBack { get; set; }

    }
    public class ItemDetail
    {
        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }

        [JsonProperty(PropertyName = "item_sku")]
        public string ItemSku { get; set; }

        [JsonProperty(PropertyName = "variation_sku")]
        public string VariationSku { get; set; }

        [JsonProperty(PropertyName = "variation_id")]
        public long VariationId { get; set; }

        [JsonProperty(PropertyName = "discounted_price")]
        public float DiscountedPrice { get; set; }

        [JsonProperty(PropertyName = "quantity_purchased")]
        public int QuantityPurchased { get; set; }

        [JsonProperty(PropertyName = "add_on_deal_id")]
        public long AddOnDealId { get; set; }
    }
    public class ActivityDetail
    {
        [JsonProperty(PropertyName = "discounted_price")]
        public float DiscountedPrice { get; set; }
        [JsonProperty(PropertyName = "original_price")]
        public float OriginalPrice { get; set; }
        [JsonProperty(PropertyName = "items")]
        public List<ActivityItem> ActivityItems { get; set; }
        [JsonProperty(PropertyName = "activity_type")]
        public string ActivityType { get; set; }
    }
    public class ActivityItem
    {
        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }
        [JsonProperty(PropertyName = "variation_id")]
        public long VariationId { get; set; }
        [JsonProperty(PropertyName = "original_price")]
        public float OriginalPrice { get; set; }
        [JsonProperty(PropertyName = "quantity_purchased")]
        public int QuantityPurchased { get; set; }
    }
    public class RecipientAddress
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is mandatory")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is mandatory")]
        [JsonProperty(PropertyName = "phone")]
        public string Phone { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is mandatory")]
        [JsonProperty(PropertyName = "city")]
        public string City { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is mandatory")]
        [JsonProperty(PropertyName = "district")]
        public string District { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is mandatory")]
        [JsonProperty(PropertyName = "state")]
        public string State { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is mandatory")]
        [JsonProperty(PropertyName = "full_address")]
        public string FullAddress { get; set; }
    }
    public class IncomeDetail
    {
        [JsonProperty(PropertyName = "voucher_seller"), Description("Giảm giá")]
        public float VoucherSeller { get; set; }
        [JsonProperty(PropertyName = "seller_rebate")]
        public float SellerRebate { get; set; }
    }
    public class OrderDetailItem
    {
        [JsonProperty(PropertyName = "item_sku")]
        public string ItemSku { get; set; }

        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }

        [JsonProperty(PropertyName = "variation_sku")]
        public string VariationSku { get; set; }

        [JsonProperty(PropertyName = "variation_id")]
        public long VariationId { get; set; }

        [JsonProperty(PropertyName = "variation_name")]
        public string VariationName { get; set; }

        [JsonProperty(PropertyName = "variation_original_price")]
        public float VariationOriginalPrice { get; set; }

        [JsonProperty(PropertyName = "item_name")]
        public string ItemName { get; set; }

        [JsonProperty(PropertyName = "variation_quantity_purchased")]
        public int VariationQuantityPurchased { get; set; }

        [JsonProperty(PropertyName = "variation_discounted_price")]
        public float? VariationDiscountedPrice { get; set; }

        [JsonProperty(PropertyName = "promotion_type")]
        public string PromotionType { get; set; }

        [JsonProperty(PropertyName = "add_on_deal_id")]
        public long AddOnDealId { get; set; }

    }
    public class TrackingInfo
    {
        [JsonProperty(PropertyName = "ctime")]
        public int Ctime { get; set; }
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
    }
    #endregion

    #region PAYMENT

    public class TransactionListRequest : BaseRequest
    {
        [JsonProperty(PropertyName = "pagination_offset")]
        public int PaginationOffset { get; set; }
        [JsonProperty(PropertyName = "pagination_entries_per_page")]
        public int PaginationEntriesPerPage { get; set; }
        [JsonProperty(PropertyName = "create_time_from")]
        public long CreateTimeFrom { get; set; }
        [JsonProperty(PropertyName = "create_time_to")]
        public long CreateTimeTo { get; set; }
    }

    public class TransactionListResponse : BaseShopeeResponse
    {
        [JsonProperty(PropertyName = "has_more")]
        public bool HasMore { get; set; }
        [JsonProperty(PropertyName = "transaction_list")]
        public List<Transaction> TransactionList { get; set; }
    }

    public class Transaction
    {
        [JsonProperty(PropertyName = "transaction_id")]
        public long TransactionId { get; set; }
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
        [JsonProperty(PropertyName = "wallet_type")]
        public string WalletType { get; set; }
        [JsonProperty(PropertyName = "transaction_type")]
        public string TransactionType { get; set; }
        [JsonProperty(PropertyName = "amount")]
        public decimal Amount { get; set; }
        [JsonProperty(PropertyName = "current_balance")]
        public decimal CurrentBalance { get; set; }
        [JsonProperty(PropertyName = "create_time")]
        public int CreateTime { get; set; }
        [JsonProperty(PropertyName = "ordersn")]
        public string OrderSn { get; set; }

        [JsonProperty(PropertyName = "order_sn")]
        public string OrderSnV2 { get { return this.OrderSn; } set { OrderSn = value; } }
        [JsonProperty(PropertyName = "refund_sn")]
        public string RefundSn { get; set; }
        [JsonProperty(PropertyName = "withdrawal_type")]
        public string WithdrawalType { get; set; }
        [JsonProperty(PropertyName = "transaction_fee")]
        public decimal TransactionFee { get; set; }
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
        [JsonProperty(PropertyName = "buyer_name")]
        public string BuyerName { get; set; }
        [JsonProperty(PropertyName = "pay_order_list")]
        public List<PayOrder> PayOrderList { get; set; }
        [JsonProperty(PropertyName = "withdraw_id")]
        public float WithdrawId { get; set; }
        [JsonProperty(PropertyName = "reason")]
        public string Reason { get; set; }
        [JsonProperty(PropertyName = "root_withdrawal_id")]
        public float RootWithdrawalId { get; set; }
    }

    public class PayOrder
    {
        [JsonProperty(PropertyName = "ordersn")]
        public string OrderSn { get; set; }
        [JsonProperty(PropertyName = "shop_name")]
        public string ShopName { get; set; }
    }

    #endregion

    #endregion

    #region webhook
    public class ShopeeSetPushConfigRequest : BaseRequest
    {
        [JsonProperty(PropertyName = "callback_url")]
        public string CallbackUrl { get; set; }
        [JsonProperty(PropertyName = "blocked_shopid")]
        public int[] BlockedShopId { get; set; }
        [JsonProperty(PropertyName = "detailed_config")]
        public ShopeeDetailConfig DetailConfig { get; set; }
    }

    public class ShopeeDetailConfig
    {
        [JsonProperty(PropertyName = "order_status")]
        public byte OrderStatus { get; set; }
        [JsonProperty(PropertyName = "order_trackingno")]
        public byte OrderTrackingno { get; set; }
        [JsonProperty(PropertyName = "shop_update")]
        public byte ShopUpdate { get; set; }
        [JsonProperty(PropertyName = "banned_item")]
        public byte BannedItem { get; set; }
        [JsonProperty(PropertyName = "item_promotion")]
        public byte ItemPromotion { get; set; }
        [JsonProperty(PropertyName = "reserved_stock_change")]
        public byte ReservedStockChange { get; set; }
    }

    public class ShopeeSetPushConfigResponse : BaseShopeeResponse
    {
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
    }
    #endregion
}
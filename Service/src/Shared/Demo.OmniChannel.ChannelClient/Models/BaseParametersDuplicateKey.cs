﻿using Newtonsoft.Json;

namespace Demo.OmniChannel.ChannelClient.Models
{
    public class BaseParametersDuplicateKey<TKey,TValue>
    {
        [JsonProperty(PropertyName = "key")]
        public TKey Key { get; set; }

        [JsonProperty(PropertyName = "value")]
        public TValue Value { get; set; }

        public BaseParametersDuplicateKey(TKey key, TValue value)
        {
            Key = key;
            Value = value;
        }
    }
}

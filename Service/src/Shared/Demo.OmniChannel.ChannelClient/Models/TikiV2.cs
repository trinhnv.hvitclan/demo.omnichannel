﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using Newtonsoft.Json;

namespace Demo.OmniChannel.ChannelClient.Models
{
    #region SellerInfo
    public class SellerInfoV2Response
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }
        [JsonProperty(PropertyName = "sid")]
        public string SId { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }
        [JsonProperty(PropertyName = "logo")]
        public string Logo { get; set; }
        [JsonProperty(PropertyName = "active")]
        public byte Active { get; set; }
        [JsonProperty(PropertyName = "can_update_product")]
        public byte CanUpdateProduct { get; set; }
        [JsonProperty(PropertyName = "registration_status")]
        public string RegistrationStatus { get; set; }
        [JsonProperty(PropertyName = "errors")]
        public List<string> Errors { get; set; }
    }

    public class SellerUpdateCanEditProductV2Request
    {
        [JsonProperty(PropertyName = "can_update_product")]
        public byte CanUpdateProduct { get; set; }
    }

    public class SellerUpdateCanEditProductV2Response
    {
        [JsonProperty(PropertyName = "errors")]
        public List<string> Errors { get; set; }
        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }
    }
    #endregion

    #region Product
    public class TikiProductsV2ListResponse
    {

        [JsonProperty(PropertyName = "data")]
        public List<TikiProductV2> Products { get; set; }
        [JsonProperty(PropertyName = "paging")]
        public TikiPage Paging { get; set; }
    }

    public class TikiProductV2
    {
        [JsonProperty(PropertyName = "product_id")]
        public long ProductId { get; set; }
        [JsonProperty(PropertyName = "sku")]
        public string Sku { get; set; }
        [JsonProperty(PropertyName = "master_sku")]
        public string MasterSku { get; set; }
        [JsonProperty(PropertyName = "original_sku")]
        public string OriginalSku { get; set; }
        [JsonProperty(PropertyName = "master_id")]
        public long MasterId { get; set; }
        [JsonProperty(PropertyName = "super_sku")]
        public string SuperSku { get; set; }
        [JsonProperty(PropertyName = "super_id")]
        public long SuperId { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "entity_type")]
        public string EntityType { get; set; }
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
        [JsonProperty(PropertyName = "price")]
        public decimal Price { get; set; }
        [JsonProperty(PropertyName = "active")]
        public byte Active { get; set; }
        [JsonProperty(PropertyName = "created_at")]
        public string CreatedAt { get; set; }
        [JsonProperty(PropertyName = "created_by")]
        public string CreatedBy { get; set; }
        [JsonProperty(PropertyName = "thumbnail")]
        public string Thumbnail { get; set; }
        [JsonProperty(PropertyName = "images")]
        public List<TikiImageV2> Images { get; set; }
        [JsonProperty(PropertyName = "categories")]
        public List<object> Categories { get; set; }
        [JsonProperty(PropertyName = "inventory")]
        public TikiInventoryV2 Inventory { get; set; }
    }

    public class TikiProductDetailV2
    {
        [JsonProperty(PropertyName = "product_id")]
        public long ProductId { get; set; }
        [JsonProperty(PropertyName = "sku")]
        public string Sku { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "attributes")]
        public TikiAttributes Attributes { get; set; }
        [JsonProperty(PropertyName = "images")]
        public List<TikiImage> Images { get; set; }
        
    }

    public class TikiImageV2
    {
        [JsonProperty(PropertyName = "url")]
        public string Url { get; set; }
    }

    public class TikiInventoryV2
    {
        [JsonProperty(PropertyName = "inventory_type")]
        public string InventoryType { get; set; }
        [JsonProperty(PropertyName = "fulfillment_type")]
        public string FulfillmentType { get; set; }
    }
    public class TikiAttributesV2
    {
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
    }

    public class UpdateProductTikiV2Request
    {
        [JsonProperty(PropertyName = "product_id")]
        public long ProductId { get; set; }
        [JsonProperty(PropertyName = "price")]
        public decimal? Price { get; set; }
        [JsonProperty(PropertyName = "quantity")]
        public double? Quantity { get; set; }
        [JsonProperty(PropertyName = "active")]
        public int? Active { get; set; }
    }

    public class UpdateProductTikiV2Response
    {
        [JsonProperty(PropertyName = "errors")]
        public List<string> Errors { get; set; }
    }
    #endregion

    public class TikiOrdersV2ListRequest : TikiListBaseRequest
    {
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
        [JsonProperty(PropertyName = "created_from_date")]
        public string CreatedFromDate { get; set; }
        [JsonProperty(PropertyName = "created_to_date")]
        public string CreatedToDate { get; set; }

    }
    public class TikiOrderV2Response
    {
        [JsonProperty(PropertyName = "data")]
        public List<TikiOrderV2> Orders { get; set; }
        [JsonProperty(PropertyName = "paging")]
        public TikiPage Paging { get; set; }

        [JsonProperty(PropertyName = "errors")]
        public List<string> Errors { get; set; }

    }
    public class TikiOrderV2
    {
        #region General information
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }
        [JsonProperty(PropertyName = "relation_code")]
        public string RelationCode { get; set; }
        [JsonProperty(PropertyName = "fulfillment_type")]
        public string FulfillmentType { get; set; }
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
        [JsonProperty(PropertyName = "is_rma")]
        public bool IsRma { get; set; }
        [JsonProperty(PropertyName = "is_vat_exporting")]
        public bool IsVatExporting { get; set; }
        [JsonProperty(PropertyName = "has_backorder_items")]
        public bool HasBackorderItems { get; set; }

        [JsonProperty(PropertyName = "created_at")]
        public DateTime? CreatedAt { get; set; }

        [JsonProperty(PropertyName = "updated_at")]
        public DateTime? UpdatedAt { get; set; }
        #endregion

        [JsonProperty(PropertyName = "invoice")]
        public TikiInvoice Invoice { get; set; }

        [JsonProperty(PropertyName = "tiki_warehouse")]
        public TikiWarehouse Warehouse { get; set; }
        
        [JsonProperty(PropertyName = "items")]
        public List<TikiOrderV2Item> Items { get; set; }

        [JsonProperty(PropertyName = "shipping")]
        public TikiShippingV2 Shipping { get; set; }

        [JsonProperty(PropertyName = "payment")]
        public TikiPayment Payment { get; set; }

        [JsonProperty(PropertyName = "customer")]
        public TikiCustomer Customer { get; set; }

        [JsonProperty(PropertyName = "installment_info")]
        public InstallmentInfo InstallmentInfo { get; set; }

        [JsonProperty(PropertyName = "cancel_info")]
        public CancelInfo CancelInfo { get; set; }

        [JsonProperty(PropertyName = "status_histories")]
        public List<StatusHistory> StatusHistories { get; set; }

        [JsonProperty(PropertyName = "errors")]
        public List<string> Errors { get; set; }

        public TikiOrderV2()
        {
            Items = new List<TikiOrderV2Item>();
        }
    }

    public class TikiCustomer
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "full_name")]
        public string FullName { get; set; }
    }

    public class StatusHistory
    {
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "created_at")]
        public DateTime? CreatedAt { get; set; }
    }

    public class InstallmentInfo
    {
        [JsonProperty(PropertyName = "installment_fee")]
        public decimal InstallmentFee { get; set; }

        [JsonProperty(PropertyName = "month")]
        public int Month { get; set; }

        [JsonProperty(PropertyName = "monthly_pay")]
        public decimal MonthlyPay { get; set; }
    }

    public class CancelInfo
    {
        [JsonProperty(PropertyName = "reason_code")]
        public string ReasonCode { get; set; }

        [JsonProperty(PropertyName = "reason_text")]
        public string ReasonText { get; set; }

        [JsonProperty(PropertyName = "comment")]
        public string Comment { get; set; }
    }


    public class TikiPayment
    {
        [JsonProperty(PropertyName = "method")]
        public string Method { get; set; }

        [JsonProperty(PropertyName = "is_prepaid")]
        public bool IsPrepaid { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
    }

    public class TikiWarehouse
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }
    }

    public class TikiInvoice : TikiAbstractDiccount
    {
        [JsonProperty(PropertyName = "items_count")]
        public int ItemsCount { get; set; }

        [JsonProperty(PropertyName = "items_quantity")]
        public int ItemsQuantity { get; set; }

        [JsonProperty(PropertyName = "subtotal")]
        public decimal Subtotal { get; set; }

        [JsonProperty(PropertyName = "grand_total")]
        public decimal GrandTotal { get; set; }

        [JsonProperty(PropertyName = "collectible_amount")]
        public decimal CollectibleAmount { get; set; }

        [JsonProperty(PropertyName = "gift_card_amount")]
        public decimal GiftCardAmount { get; set; }

        [JsonProperty(PropertyName = "gift_card_code")]
        public string GiftCardCode { get; set; }

        [JsonProperty(PropertyName = "coupon_code")]
        public string CouponCode { get; set; }

        [JsonProperty(PropertyName = "shipping_amount_after_discount")]
        public decimal ShippingAmountAfterDiscount { get; set; }

        [JsonProperty(PropertyName = "shipping_discount_amount")]
        public decimal ShippingDiscountAmount { get; set; }

        [JsonProperty(PropertyName = "handling_fee")]
        public decimal HandlingFee { get; set; }

        [JsonProperty(PropertyName = "other_fee")]
        public decimal OtherFee { get; set; }

        [JsonProperty(PropertyName = "total_seller_fee")]
        public decimal TotalSellerFee { get; set; }

        [JsonProperty(PropertyName = "total_seller_income")]
        public decimal TotalSellerIncome { get; set; }

        [JsonProperty(PropertyName = "purchased_at")]
        public DateTime? PurchasedAt { get; set; }

        [JsonProperty(PropertyName = "tax_info")]
        public TikiTaxInfo TaxInfo { get; set; }
    }

    public class TikiTaxInfo
    {
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [JsonProperty(PropertyName = "address")]
        public string Address { get; set; }

        [JsonProperty(PropertyName = "company_name")]
        public string CompanyName { get; set; }
    }

    public class TikiShippingV2
    {
        [JsonProperty(PropertyName = "partner_id")]
        public string PartnerId { get; set; }
        [JsonProperty(PropertyName = "partner_name")]
        public string PartnerName { get; set; }
        [JsonProperty(PropertyName = "tracking_code")]
        public string TrackingCode { get; set; }
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "plan")]
        public TikiPlan Plan { get; set; }
        [JsonProperty(PropertyName = "address")]
        public TikiAddress Address { get; set; }
    }

    public class TikiPlan
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "is_free_shipping")]
        public string IsFreeShipping { get; set; }

        [JsonProperty(PropertyName = "promised_delivery_date")]
        public DateTime? PromisedDeliveryDate { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
    }

    public class TikiAddress
    {
        [JsonProperty(PropertyName = "full_name")]
        public string FullName { get; set; }

        [JsonProperty(PropertyName = "street")]
        public string Street { get; set; }

        [JsonProperty(PropertyName = "ward")]
        public string Ward { get; set; }

        [JsonProperty(PropertyName = "ward_tiki_code")]
        public string WardTikiCode { get; set; }

        [JsonProperty(PropertyName = "district")]
        public string District { get; set; }

        [JsonProperty(PropertyName = "district_tiki_code")]
        public string DistrictTikiCode { get; set; }

        [JsonProperty(PropertyName = "region")]
        public string Region { get; set; }

        [JsonProperty(PropertyName = "region_tiki_code")]
        public string RegionTikiCode { get; set; }

        [JsonProperty(PropertyName = "country")]
        public string Country { get; set; }

        [JsonProperty(PropertyName = "country_id")]
        public string CountryId { get; set; }

        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "phone")]
        public string Phone { get; set; }
    }

    public class TikiOrderV2Item
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "product")]
        public TikiOrderProduct Product { get; set; }

        [JsonProperty(PropertyName = "invoice")]
        public TikiItemInvoice Invoice { get; set; }

        [JsonProperty(PropertyName = "seller")]
        public TikiSeller Seller { get; set; }

        [JsonProperty(PropertyName = "confirmation")]
        public TikiConfirmation Confirmation { get; set; }

        [JsonProperty(PropertyName = "inventory_requisition")]
        public TikiInventoryRequisition InventoryRequisition { get; set; }
    }

    public class TikiOrderProduct
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "super_id")]
        public long SuperId { get; set; }

        [JsonProperty(PropertyName = "master_id")]
        public long MasterId { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "sku")]
        public string Sku { get; set; }

        [JsonProperty(PropertyName = "catalog_group_name")]
        public string CatalogGroupName { get; set; }

        [JsonProperty(PropertyName = "inventory_type")]
        public string InventoryType { get; set; }

        [JsonProperty(PropertyName = "thumbnail")]
        public string Thumbnail { get; set; }

        [JsonProperty(PropertyName = "seller_product_code")]
        public string SellerProductCode { get; set; }

        [JsonProperty(PropertyName = "seller_supply_method")]
        public string SellerSupplyMethod { get; set; }
    }

    public class TikiItemInvoice : TikiAbstractDiccount
    {
        [JsonProperty(PropertyName = "price")]
        public decimal Price { get; set; }

        [JsonProperty(PropertyName = "quantity")]
        public double Quantity { get; set; }

        [JsonProperty(PropertyName = "subtotal")]
        public decimal SubTotal { get; set; }

        [JsonProperty(PropertyName = "row_total")]
        public decimal RowTotal { get; set; }

        [JsonProperty(PropertyName = "discount_tikier")]
        public decimal DiscountTikier { get; set; }

        [JsonProperty(PropertyName = "discount_tiki_first")]
        public decimal DiscountTikiFirst { get; set; }

        [JsonProperty(PropertyName = "is_taxable")]
        public bool IsTaxable { get; set; }

        [JsonProperty(PropertyName = "fob_price")]
        public decimal FobPrice { get; set; }

        [JsonProperty(PropertyName = "seller_fee")]
        public decimal SellerFee { get; set; }

        [JsonProperty(PropertyName = "seller_income")]
        public decimal SellerIncome { get; set; }

        [JsonProperty(PropertyName = "fees")]
        public List<TikiFee> Fees { get; set; }

        [JsonProperty(PropertyName = "is_seller_discount_coupon")]
        public bool IsSellerDiscountCoupon { get; set; }

        [Description("Tổng giảm giá mà seller phải chịu. Demo tự tính")]
        public decimal CaculatorDiscountSeller
        {
            get
            {
                var discountAmount = DiscountAmount;
                //Nếu is_seller_discount_coupon = false. Giảm giá này tiki chịu không cần đồng bộ về
                if (!IsSellerDiscountCoupon) discountAmount = discountAmount - DiscountCoupon;

                //Discount_tikixu: Giảm giá này tiki chịu không cần đồng bộ về
                discountAmount = discountAmount - DiscountTikixu;
                return discountAmount;
            }
        }
    }

    public class TikiSeller
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
    }
    public class TikiConfirmation
    {
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
        [JsonProperty(PropertyName = "confirmed_at")]
        public DateTime? ConfirmedAt { get; set; }
        [JsonProperty(PropertyName = "available_confirm_sla")]
        public DateTime? AvailableConfirmSla { get; set; }
        [JsonProperty(PropertyName = "pickup_confirm_sla")]
        public DateTime? PickupConfirmSla { get; set; }
        [JsonProperty(PropertyName = "histories")]
        public List<TikiHistories> Histories { get; set; }
    }

    public class TikiHistories
    {
        [JsonProperty(PropertyName = "confirmed_at")]
        public DateTime? ConfirmedAt { get; set; }

        [JsonProperty(PropertyName = "order_item_id")]
        public long OrderItemId { get; set; }

        [JsonProperty(PropertyName = "sla_confirmed_at")]
        public DateTime? SlaConfirmedAt { get; set; }
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
    }

    public abstract class TikiAbstractDiccount
    {
        /*Tổng giảm giá = discount_promotion + discount_percent 
         * + discount_coupon + discount_other + discount_tikixu
         */
        [JsonProperty(PropertyName = "discount_amount")]
        public decimal DiscountAmount { get; set; }

        [JsonProperty(PropertyName = "discount_promotion")]
        public decimal DiscountPromotion { get; set; }

        [JsonProperty(PropertyName = "discount_percent")]
        public decimal DiscountPercent { get; set; }

        [JsonProperty(PropertyName = "discount_coupon")]
        public decimal DiscountCoupon { get; set; }

        [JsonProperty(PropertyName = "discount_other")]
        public decimal DiscountOther { get; set; }

        [JsonProperty(PropertyName = "discount_tikixu")]
        public decimal DiscountTikixu { get; set; }

    }

    public class TikiFee
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "fee_type_key")]
        public string FeeTypeKey { get; set; }
        [JsonProperty(PropertyName = "fee_type_name")]
        public string FeeTypeName { get; set; }
        [JsonProperty(PropertyName = "status")]
        public int Status { get; set; }
        [JsonProperty(PropertyName = "quantity")]
        public int Quantity { get; set; }
        [JsonProperty(PropertyName = "base_amount")]
        public decimal BaseAmount { get; set; }
        [JsonProperty(PropertyName = "total_amount")]
        public decimal TotalAmount { get; set; }
        [JsonProperty(PropertyName = "discount_amount")]
        public decimal DiscountAmount { get; set; }
        [JsonProperty(PropertyName = "final_amount")]
        public decimal FinalAmount { get; set; }
    }

    public class TikiInventoryRequisition
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [JsonProperty(PropertyName = "seller_inventory_id")]
        public int SellerInventoryId { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "is_printed")]
        public bool IsPrinted { get; set; }

        [JsonProperty(PropertyName = "pickup_method")]
        public string PickupMethod { get; set; }

        [JsonProperty(PropertyName = "warehouse_code")]
        public string WarehouseCode { get; set; }

        [JsonProperty(PropertyName = "error")]
        public string Error { get; set; }

        [JsonProperty(PropertyName = "created_at")]
        public DateTime? CreatedAt { get; set; }
    }

    #region SellerWarehouse

    public class SellerWareHouseV2Response
    {
        [JsonProperty(PropertyName = "data")]
        public List<SellerWareHouseInfo> Data { get; set; }
        [JsonProperty(PropertyName = "paging")]
        public SellerWareHousePaging Paging { get; set; }
        [JsonProperty(PropertyName = "errors")]
        public List<string> Errors { get; set; }
    }

    public class SellerWareHouseInfo
    {
        [JsonProperty(PropertyName = "id")]
        public long WareHouseId { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "street")]
        public string StreetInfo{ get; set; }
    }

    public class SellerWareHousePaging
    {
        [JsonProperty(PropertyName = "total")]
        public int Total { get; set; }
        [JsonProperty(PropertyName = "per_page")]
        public int PerPage { get; set; }
        [JsonProperty(PropertyName = "current_page")]
        public int CurrentPage { get; set; }
        [JsonProperty(PropertyName = "last_page")]
        public int LastPage { get; set; }
        [JsonProperty(PropertyName = "from")]
        public int From { get; set; }
        [JsonProperty(PropertyName = "to")]
        public int To { get; set; }
    }

    #endregion

    #region TikiDeleteProducts
    public class TikiDeleteProductsV2ListResponse
    {

        [JsonProperty(PropertyName = "data")]
        public List<TikiDeleteProductsV2> Products { get; set; }
        [JsonProperty(PropertyName = "paging")]
        public TikiPage Paging { get; set; }
    }

    public class TikiDeleteProductsV2
    {
        [JsonProperty(PropertyName = "id")]
        public long ProductId { get; set; }
        [JsonProperty(PropertyName = "sku")]
        public string Sku { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "master_id")]
        public long MasterId { get; set; }
        [JsonProperty(PropertyName = "master_sku")]
        public string MasterSku { get; set; }
        [JsonProperty(PropertyName = "super_id")]
        public long SuperId { get; set; }
        [JsonProperty(PropertyName = "super_sku")]
        public string SuperSku { get; set; }
        [JsonProperty(PropertyName = "original_sku")]
        public string OriginalSku { get; set; }
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
        [JsonProperty(PropertyName = "entity_type")]
        public string EntityType { get; set; }
        [JsonProperty(PropertyName = "price")]
        public decimal Price { get; set; }
        [JsonProperty(PropertyName = "market_price")]
        public decimal MarketPrice { get; set; }
        [JsonProperty(PropertyName = "version")]
        public int Version { get; set; }
        [JsonProperty(PropertyName = "created_at")]
        public string CreatedAt { get; set; }
        [JsonProperty(PropertyName = "created_by")]
        public string CreatedBy { get; set; }
        [JsonProperty(PropertyName = "updated_at")]
        public string UpdatedAt { get; set; }
        [JsonProperty(PropertyName = "active")]
        public byte Active { get; set; }
        [JsonProperty(PropertyName = "is_hidden")]
        public bool IsHidden { get; set; }
    }
    #endregion
}
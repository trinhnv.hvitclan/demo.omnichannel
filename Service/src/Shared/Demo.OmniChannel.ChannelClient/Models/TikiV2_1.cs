﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.Models
{
    public class TikiV21
    {
        #region Product
        public class TikiProductsV21ListResponse
        {
            [JsonProperty(PropertyName = "data")]
            public List<TikiProductV21> Products { get; set; }
            [JsonProperty(PropertyName = "paging")]
            public TikiPageV21 Paging { get; set; }
        }

        public class TikiPageV21
        {
            [JsonProperty(PropertyName = "total")]
            public int Total { get; set; }
            [JsonProperty(PropertyName = "per_page")]
            public int PerPage { get; set; }
            [JsonProperty(PropertyName = "current_page")]
            public int CurrentPage { get; set; }
            [JsonProperty(PropertyName = "last_page")]
            public int LastPage { get; set; }
            [JsonProperty(PropertyName = "from")]
            public int From { get; set; }
            [JsonProperty(PropertyName = "to")]
            public int To { get; set; }
        }

        public class TikiProductV21
        {
            [JsonProperty(PropertyName = "product_id")]
            public long ProductId { get; set; }
            [JsonProperty(PropertyName = "active")]
            public byte Active { get; set; } // product is active (1) or inactive
            [JsonProperty(PropertyName = "sku")]
            public string Sku { get; set; }
            [JsonProperty(PropertyName = "master_sku")]
            public string MasterSku { get; set; }
            [JsonProperty(PropertyName = "master_id")]
            public long MasterId { get; set; }
            [JsonProperty(PropertyName = "super_sku")]
            public string SuperSku { get; set; }
            [JsonProperty(PropertyName = "super_id")]
            public long SuperId { get; set; }
            [JsonProperty(PropertyName = "name")]
            public string Name { get; set; }
            [JsonProperty(PropertyName = "entity_type")]
            public string EntityType { get; set; }
            [JsonProperty(PropertyName = "type")]
            public string Type { get; set; }
            [JsonProperty(PropertyName = "price")]
            public long Price { get; set; } // the sell price of a product
            [JsonProperty(PropertyName = "created_at")]
            public string CreatedAt { get; set; }
            [JsonProperty(PropertyName = "updated_at")]
            public string UpdateAt { get; set; }
            [JsonProperty(PropertyName = "created_by")]
            public string CreatedBy { get; set; }
            [JsonProperty(PropertyName = "original_sku")]
            public string OriginalSku { get; set; } // seller product code
            [JsonProperty(PropertyName = "market_price")]
            public long MarketPrice { get; set; } //the price before discount of a product
            [JsonProperty(PropertyName = "version")]
            public int Version { get; set; }
            [JsonProperty(PropertyName = "thumbnail")]
            public string Thumbnail { get; set; }
            [JsonProperty(PropertyName = "images")]
            public List<TikiImageV21> Images { get; set; }
            [JsonProperty(PropertyName = "categories")]
            public List<object> Categories { get; set; }
            [JsonProperty(PropertyName = "inventory")]
            public TikiInventoryV21 Inventory { get; set; }
            [JsonProperty(PropertyName = "attributes")]
            public object Attributes { get; set; }
        }

        public class TikiImageV21
        {
            [JsonProperty(PropertyName = "id")]
            public long Id { get; set; }
            [JsonProperty(PropertyName = "path")]
            public string Path { get; set; }
            [JsonProperty(PropertyName = "url")]
            public string Url { get; set; }
            [JsonProperty(PropertyName = "position")]
            public long Position { get; set; }
            [JsonProperty(PropertyName = "label")]
            public string Label { get; set; }
            [JsonProperty(PropertyName = "is_gallery")]
            public bool IsGallery { get; set; }
        }

        public class TikiWarehouseV21
        {
            [JsonProperty(PropertyName = "warehouse_id")]
            public int WarehouseId { get; set; }
            [JsonProperty(PropertyName = "quantity")]
            public int Quantity { get; set; }
            [JsonProperty(PropertyName = "quantity_available")]
            public int QuantityAvailable { get; set; }
            [JsonProperty(PropertyName = "quantity_sellable")]
            public int QuantitySellable { get; set; }
            [JsonProperty(PropertyName = "quantity_reserved")]
            public int QuantityReserved { get; set; }
        }

        public class TikiInventoryV21
        {
            [JsonProperty(PropertyName = "inventory_type")]
            public string InventoryType { get; set; }
            [JsonProperty(PropertyName = "fulfillment_type")]
            public string FulfillmentType { get; set; }
            [JsonProperty(PropertyName = "warehouse_stocks")]
            public List<TikiWarehouseV21> WarehouseStocks { get; set; }

        }


        public class TikiProductDetailV21
        {
            [JsonProperty(PropertyName = "id")]
            public long ProductId { get; set; }
            [JsonProperty(PropertyName = "sku")]
            public string Sku { get; set; }
            [JsonProperty(PropertyName = "name")]
            public string Name { get; set; }
            [JsonProperty(PropertyName = "attributes")]
            public TikiAttributes Attributes { get; set; }
            [JsonProperty(PropertyName = "images")]
            public List<TikiImage> Images { get; set; }
            [JsonProperty(PropertyName = "active")]
            public string Active { get; set; } // product is active (1) or inactive
        }

        public class UpdateProductTikiV21Request
        {
            [JsonProperty(PropertyName = "product_id")]
            public long ProductId { get; set; }
            [JsonProperty(PropertyName = "price")]
            public decimal? Price { get; set; }
            [JsonProperty(PropertyName = "active")]
            public int? Active { get; set; }
            [JsonProperty(PropertyName = "original_sku")]
            public string SKU { get; set; }
            [JsonProperty(PropertyName = "seller_warehouse")]
            public string SellerWarehouse { get; set; }
            [JsonProperty(PropertyName = "warehouse_quantities")]
            public List<WarehouseQuantitiesV21> WarehouseQuantities { get; set; }
        }

        public class WarehouseQuantitiesV21
        {
            [JsonProperty(PropertyName = "warehouse_id")]
            public long WarehouseId { get; set; }
            [JsonProperty(PropertyName = "qty_available")]
            public long QtyAvailable { get; set; }
        }

        public class UpdateProductTikiV21Response
        {
            [JsonProperty(PropertyName = "errors")]
            public List<string> Errors { get; set; }
            [JsonProperty(PropertyName = "state")]
            public string State { get; set; }
        }

        public class TikiIBaseModelExtra
        {
            public long? WareHouseId { get; set; }
        }


        #endregion
    }
}

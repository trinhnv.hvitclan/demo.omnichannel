﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Demo.OmniChannel.ChannelClient.Models
{
    public class KvInvoice
    {
        public long Id { get; set; }
        public string Uuid { get; set; }
        public string Code { get; set; }
        public DateTime PurchaseDate { get; set; }
        public string BranchName { get; set; }
        public long SoldById { get; set; }
        public int? SaleChannelId { get; set; }
        public int RetailerId { get; set; }
        public int BranchId { get; set; }
        public string SoldByName { get; set; }
        public long? CustomerId { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public long? OrderId { get; set; }
        public string OrderCode { get; set; }
        public decimal Total { get; set; }
        public decimal TotalPayment { get; set; }
        public decimal? Discount { get; set; }
        public double? DiscountRatio { get; set; }
        public byte Status { get; set; }
        public string StatusValue { get; set; }
        public string Description { get; set; }
        public long PriceBookId { get; set; }
        public string PriceBookName { get; set; }
        public bool UsingCod { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public InvoiceDelivery InvoiceDelivery { get; set; }
        public InvoiceDelivery DeliveryDetail { get; set; }

        private List<InvoiceDetail> _invoiceDetails;
        public List<InvoiceDetail> InvoiceDetails
        {
            get => _invoiceDetails;
            set { _invoiceDetails = value?.Where(x => x != null).ToList(); }
        }

        public byte? DeliveryStatus { get; set; }
        public byte? NewStatus { get; set; }
        public string ChannelStatus { get; set; }
        public string DeliveryCode { get; set; }
        private List<SurCharge> _surCharges;
        public List<SurCharge> SurCharges
        {
            get => _surCharges;
            set { _surCharges = value?.Where(x => x != null).ToList(); }
        }
        public List<InvoiceWarranties> InvoiceWarranties { get; set; }


        public void UpdatePriceBookId(PriceBook priceBook)
        {
            this.PriceBookId = priceBook?.Id ?? 0;
            this.PriceBookName = priceBook?.Name ?? string.Empty;
        }

        public void UpdateSaleChannelId(int? saleChannelId)
        {
            this.SaleChannelId = saleChannelId;
        }
    }
}
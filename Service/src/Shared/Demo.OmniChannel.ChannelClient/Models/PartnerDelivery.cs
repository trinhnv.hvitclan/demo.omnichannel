﻿namespace Demo.OmniChannel.ChannelClient.Models
{
    public class PartnerDelivery
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string ContactNumber { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public int RetailerId { get; set; }
        public bool IsOmniChannel { get; set; }
    }
}
﻿using System;

namespace Demo.OmniChannel.ChannelClient.Models
{
    public class ChannelAuth
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public int ExpiresIn { get; set; }
        public int RefreshExpiresIn { get; set; }
        public long ShopId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long ChannelId { get; set; }
        public int RetailerId { get; set; }
    }

    public class RefreshTokenResponse
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public int ExpiresIn { get; set; }
        public int RefreshExpiresIn { get; set; }
        public string SellerId { get; set; }
        public bool IsDeactivateChannel { get; set; }
        public long ShopId { get; set; }
    }
}

﻿namespace Demo.OmniChannel.ChannelClient.Models
{
    public class OrderDetailGetResponse
    {
        public bool IsSuccess { get; set; }
        public bool IsRetryOrder { get; set; }
        public bool IsIgnore { get; set; } 
        public KvOrder Order { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Demo.OmniChannel.ChannelClient.Models
{
    #region Requests
    [DataContract(Name = "Request", Namespace = "")]
    public class RequestItem
    {
        [DataMember(Name = "Product")]
        public LzdProduct Products { get; set; }
    }


    #endregion

    #region Responses

    [DataContract]
    public class TokenResponse
    {
        [DataMember(Name = "access_token")]
        public string AccessToken { get; set; }
        [DataMember(Name = "refresh_token")]
        public string RefreshToken { get; set; }
        [DataMember(Name = "account")]
        public string Account { get; set; }
        [DataMember(Name = "refresh_expires_in")]
        public int RefreshExpiresIn { get; set; }
        [DataMember(Name = "expires_in")]
        public int ExpiresIn { get; set; }

        public long ShopId { get; set; }
        public long Id { get; set; }
    }
    [DataContract]
    public class SellerProfileResponse
    {
        [DataMember(Name = "code")]
        public string Code { get; set; }

        [DataMember(Name = "message")]
        public string Message { get; set; }
        [DataMember(Name = "data")]
        public SellerProfile Data { get; set; }
    }
    [DataContract]
    public class SellerProfile
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }
        [DataMember(Name = "email")]
        public string Email { get; set; }
        [DataMember(Name = "seller_id")]
        public string SellerId { get; set; }
        [DataMember(Name = "short_code")]
        public string ShortCode { get; set; }
    }

    [DataContract]
    public class LazadaProductResponse : BaseResponse
    {
        [DataMember(Name = "detail")]
        public List<Detail> Detail { get; set; }
        public int HttpStatusCode { get; set; }
    }

    [DataContract]
    public class ProductMappingResponse
    {
        [DataMember(Name = "total_products")]
        public long Total { get; set; }
        [DataMember(Name = "products")]
        public List<ProductMap> Products { get; set; }
    }
    [DataContract]
    public class ProductMappingData : BaseResponse
    {
        [DataMember(Name = "data")]
        public ProductMappingResponse Data { get; set; }
    }
    [DataContract]
    public class BaseResponse
    {
        [DataMember(Name = "code")]
        public string Code { get; set; }
        [DataMember(Name = "type")]
        public string Type { get; set; }
        //[DataMember(Name = "data")]
        //public string Data { get; set; }
        [DataMember(Name = "message")]
        public string Message { get; set; }
        [DataMember(Name = "request_id")]
        public string RequestId { get; set; }

        public bool IsError()
        {
            return !string.IsNullOrEmpty(Code) && !Code.Equals("0");
        }
    }
    [DataContract]
    public class OrderResponse : BaseResponse
    {
        [DataMember(Name = "data")]
        public DataResponse Data { get; set; }
    }
    [DataContract]
    public class DataResponse
    {
        [DataMember(Name = "count")]
        public int Count { get; set; }
        [DataMember(Name = "orders")]
        public List<Order> Orders { get; set; }
    }
    [DataContract]
    public class OrderItemResponse : BaseResponse
    {
        [DataMember(Name = "data")]
        public List<OrderItem> Data { get; set; }
    }
    [DataContract]
    public class OrderIdResponse : BaseResponse
    {
        [DataMember(Name = "data")]
        public Order Data { get; set; }
    }
    [DataContract]
    public class ProductDetailResponse : BaseResponse
    {
        [DataMember(Name = "data")]
        public ProductMap Data { get; set; }
    }

    [DataContract]
    public class FinanceTransactionReponse : BaseResponse
    {
        [DataMember(Name = "data")]
        public List<FinanceTransaction> Data { get; set; }
    }
    #endregion

    #region Models
    [DataContract]
    public class Detail
    {
        [DataMember(Name = "message")]
        public string Message { get; set; }
        [DataMember(Name = "seller_sku")]
        public string SellerSku { get; set; }
    }
    [DataContract(Name = "Product", Namespace = "")]
    public class LzdProduct
    {
        [DataMember(Name = "Skus")]
        public List<ProductUpdate> Items { get; set; }
    }
    [DataContract]
    public class ProductMap
    {
        [DataMember(Name = "item_id")]
        public long ItemId { get; set; }
        [DataMember(Name = "skus")]
        public List<Sku> Skus { get; set; }
        [DataMember(Name = "attributes")]
        public Attribute Attribute { get; set; }
    }
    [DataContract]
    public class Attribute
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }
        [DataMember(Name = "description")]
        public string Description { get; set; }
        [DataMember(Name = "short_description")]
        public string ShortDescription { get; set; }
    }

    public class Sku
    {
        [DataMember(Name = "SkuId")]
        public long SkuId { get; set; }
        [DataMember(Name = "SellerSku")]
        public string SellerSku { get; set; }
        [DataMember(Name = "Status")]
        public string Status { get; set; }
        [DataMember(Name = "Images")]
        public List<string> Images { get; set; }
        [DataMember(Name = "saleProp")]
        public Dictionary<string, string> SaleProp { get; set; }
    }
    [DataContract(Name = "Sku", Namespace = "")]
    public class ProductUpdate
    {
        public long ProductId { get; set; }
        [DataMember(Name = "SellerSku")]
        public string SellerSku { get; set; }
        [DataMember(Name = "Quantity")]
        public int? Quantity { get; set; }
        [DataMember(Name = "Price")]
        public decimal? Price { get; set; }
        [DataMember(Name = "SalePrice")]
        public decimal? SalePrice { get; set; }
        [DataMember(Name = "SaleStartDate")]
        public string SaleStartDate { get; set; }
        [DataMember(Name = "SaleEndDate")]
        public string SaleEndDate { get; set; }
    }
    [DataContract]
    public class Order
    {
        [DataMember(Name = "remarks")]
        public string Remarks { get; set; }
        [DataMember(Name = "order_id")]
        public string OrderId { get; set; }
        [DataMember(Name = "order_number")]
        public string OrderNumber { get; set; }
        [DataMember(Name = "warehouse_code")]
        public string WarehouseCode { get; set; }
        [DataMember(Name = "created_at")]
        public DateTime CreatedAt { get; set; }
        [DataMember(Name = "updated_at")]
        public DateTime? UpdateAt { get; set; }
        [DataMember(Name = "address_shipping")]
        public AddressShipping AddressShipping { get; set; }
        [DataMember(Name = "address_billing")]
        public AddressBilling AddressBilling { get; set; }
        [DataMember(Name = "customer_first_name")]
        public string CustomerFirstName { get; set; }
        [DataMember(Name = "delivery_info")]
        public string DeliveryInfo { get; set; }
        [DataMember(Name = "statuses")]
        public List<string> Statuses { get; set; }
        [DataMember(Name = "shipping_fee")]
        public float ShippingFee { get; set; }
    }
    [DataContract]
    public class AddressShipping
    {
        [DataMember(Name = "first_name")]
        public string FirstName { get; set; }
        [DataMember(Name = "last_name")]
        public string LastName { get; set; }
        [DataMember(Name = "phone")]
        public string Phone { get; set; }
        [DataMember(Name = "address1")]
        public string Address1 { get; set; }
        [DataMember(Name = "city")]
        public string City { get; set; }
        [DataMember(Name = "ward")]
        public string Ward { get; set; }
        [DataMember(Name = "region")]
        public string Region { get; set; }
    }
    [DataContract]
    public class AddressBilling
    {
        [DataMember(Name = "first_name")]
        public string FirstName { get; set; }
        [DataMember(Name = "last_name")]
        public string LastName { get; set; }
        [DataMember(Name = "phone")]
        public string Phone { get; set; }
        [DataMember(Name = "address1")]
        public string Address1 { get; set; }
        [DataMember(Name = "address5")]
        public string Address5 { get; set; }
        [DataMember(Name = "address4")]
        public string Address4 { get; set; }
        [DataMember(Name = "address3")]
        public string Address3 { get; set; }
    }
    [DataContract]
    public class OrderItem
    {
        [DataMember(Name = "sku")]
        public string Sku { get; set; }
        [DataMember(Name = "name")]
        public string Name { get; set; }
        [DataMember(Name = "shipping_type")]
        public string ShippingType { get; set; }
        [DataMember(Name = "shipment_provider")]
        public string ShipmentProvider { get; set; }
        [DataMember(Name = "item_price")]
        public decimal ItemPrice { get; set; }
        [DataMember(Name = "tracking_code")]
        public string TrackingCode { get; set; }
        [DataMember(Name = "sku_id")]
        public string SkuId { get; set; }
        [DataMember(Name = "product_id")]
        public string ProductId { get; set; }
        [DataMember(Name = "order_item_id")]
        public string OrderItemId { get; set; }
        [DataMember(Name = "package_id")]
        public string PackageId { get; set; }
        [DataMember(Name = "quantity")]
        public int Quantity { get; set; }
        [DataMember(Name = "status")]
        public string Status { get; set; }
        [DataMember(Name = "created_at")]
        public DateTime CreatedAt { get; set; }
        [DataMember(Name = "updated_at")]
        public DateTime? UpdateAt { get; set; }
        [DataMember(Name = "voucher_seller")]
        public decimal? VoucherSeller { get; set; }
    }

    [DataContract]
    public class FinanceTransaction
    {
        [DataMember(Name = "order_no")]
        public string OrderNo { get; set; }

        [DataMember(Name = "transaction_date")]
        public string TransactionDate { get; set; }

        [DataMember(Name = "amount")]
        public string Amount { get; set; }

        [DataMember(Name = "paid_status")]
        public string PaidStatus { get; set; }

        [DataMember(Name = "shipping_provider")]
        public string ShippingProvider { get; set; }

        [DataMember(Name = "WHT_included_in_amount")]
        public string WHTIncludedInAmount { get; set; }

        [DataMember(Name = "lazada_sku")]
        public string LazadaSKU { get; set; }

        [DataMember(Name = "transaction_type")]
        public string TransactionType { get; set; }

        [DataMember(Name = "orderItem_no")]
        public string OrderItemNo { get; set; }

        [DataMember(Name = "orderItem_status")]
        public string OrderItemStatus { get; set; }

        [DataMember(Name = "reference")]
        public string Reference { get; set; }

        [DataMember(Name = "fee_name")]
        public string FeeName { get; set; }

        [DataMember(Name = "shipping_speed")]
        public string ShippingSpeed { get; set; }

        [DataMember(Name = "WHT_amount")]
        public string WHTAmount { get; set; }

        [DataMember(Name = "transaction_number")]
        public string TransactionNumber { get; set; }

        [DataMember(Name = "seller_sku")]
        public string SellerSku { get; set; }

        [DataMember(Name = "statement")]
        public string Statement { get; set; }

        [DataMember(Name = "details")]
        public string Details { get; set; }

        [DataMember(Name = "VAT_in_amount")]
        public string VATInAmount { get; set; }

        [DataMember(Name = "shipment_type")]
        public string ShipmentType { get; set; }
        [DataMember(Name = "fee_type")]
        public string FeeType { get; set; }
    }

    #endregion
}
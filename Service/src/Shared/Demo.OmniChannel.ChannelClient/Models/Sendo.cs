﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Demo.OmniChannel.ChannelClient.Models
{
    public class SendoRequestBase<T> where T : class, new()
    {
        [JsonProperty(PropertyName = "result")]
        public T Result { get; set; }

        [JsonProperty(PropertyName = "success")]
        public bool Success { get; set; }

        [JsonProperty(PropertyName = "error")]
        public object Error { get; set; }

        [JsonProperty(PropertyName = "total_records")]
        public int? TotalRecords { get; set; }

        [JsonProperty(PropertyName = "params")]
        public object Params { get; set; }
    }

    public class BaseListResponse<T> where T : class
    {
        [JsonProperty(PropertyName = "data")]
        public List<T> Data { get; set; }
        [JsonProperty(PropertyName = "total_records")]
        public int? TotalRecords { get; set; }
        [JsonProperty(PropertyName = "next_token")]
        public string NextToken { get; set; }
        [JsonProperty(PropertyName = "previous_token")]
        public string PrevToken { get; set; }
    }

    public class SendoLoginRequest
    {
        [JsonProperty(PropertyName = "shop_key")]
        public string ShopKey { get; set; }

        [JsonProperty(PropertyName = "secret_key")]
        public string SecretKey { get; set; }
    }

    public class SendoLoginResponse
    {
        [JsonProperty(PropertyName = "token")]
        public string Token { get; set; }

        [JsonProperty(PropertyName = "expires")]
        public DateTime Expires { get; set; }
    }

    #region products
    public class SendoProductsListRequest
    {
        [JsonProperty(PropertyName = "page_size")]
        public int PageSize { get; set; }
        [JsonProperty(PropertyName = "date_from")]
        public DateTime? UpdatedFromDate { get; set; }
        [JsonProperty(PropertyName = "token")]
        public string Token { get; set; }
        [JsonProperty(PropertyName = "statuses")]
        public List<int> Statuses { get; set; }
    }

    public class SendoProductsListResponse
    {
        [JsonProperty(PropertyName = "result")]
        public SendoProductListData Result { get; set; }
        [JsonProperty(PropertyName = "status_code")]
        public int StatusCode { get; set; }
        [JsonProperty(PropertyName = "error")]
        public string Error { get; set; }
        [JsonProperty(PropertyName = "success")]
        public bool Success { get; set; }

    }

    public class SendoProductListData : BaseListResponse<SendoProduct>
    {
        [JsonProperty(PropertyName = "data")]
        public List<SendoProduct> Products { get; set; }
        public SendoProductListData()
        {
            Products = new List<SendoProduct>();
        }
    }

    public class SendoAttributeValue
    {
        [JsonProperty("id")]
        public long? OptionId { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }

    public class SendoAttribute
    {
        [JsonProperty("attribute_id")]
        public int? AttributeId { get; set; }

        [JsonProperty("attribute_name")]
        public string AttributeName { get; set; }

        [JsonProperty("attribute_values")]
        public List<SendoAttributeValue> AttributeValues { get; set; }
    }

    public class SendoProduct
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("attributes")]
        public List<SendoAttribute> Attributes { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("status")] //1: chờ duyệt, 2: đã duyệt, 3: từ chối, 4: hủy, 5: đã xóa
        public int Status { get; set; }
        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("updated_at_timestamp")]
        public double UpdatedAtTimestamp { get; set; }

        [JsonProperty("is_config_variant")]
        public bool IsConfigVariant { get; set; }

        [JsonProperty("variants_length")]
        public int VariantsLength { get; set; }

    }


    public class SendoVariantAttribute
    {
        [JsonProperty("attribute_id")]
        public long AttributeId { get; set; }

        [JsonProperty("attribute_code")]
        public string AttributeCode { get; set; }

        [JsonProperty("option_id")]
        public long OptionId { get; set; }
    }

    public class SendoVariant
    {
        [JsonProperty("variant_attribute_hash")]
        public string VariantAttributeHash { get; set; }

        [JsonProperty("variant_sku")]
        public string VariantSku { get; set; }
        [JsonProperty("variant_attributes")]
        public List<SendoVariantAttribute> VariantAttributes { get; set; }

        [JsonProperty("variant_price")]
        public decimal? VariantPrice { get; set; }

        [JsonProperty("variant_special_price")]
        public decimal? VariantSpecialPrice { get; set; }

        [JsonProperty("variant_quantity")]
        public int? VariantQuantity { get; set; }

    }

    public class SendoPicture
    {
        [JsonProperty("picture_url")]
        public string PictureUrl { get; set; }
    }



    public class SendoProductDetail
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("pictures")]
        public List<SendoPicture> Pictures { get; set; }

        [JsonProperty("price")]
        public double Price { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("variants")]
        public List<SendoVariant> Variants { get; set; }

        [JsonProperty("is_config_variant")]
        public bool IsConfigVariant { get; set; }
    }
    public class SendoUpdateProduct
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }
        //[JsonProperty(PropertyName = "is_config_variant")]
        //public bool IsConfigVariant { get; set; }
        [JsonProperty(PropertyName = "stock_quantity", NullValueHandling = NullValueHandling.Ignore)]
        public int? StockQuantity { get; set; }
        [JsonProperty(PropertyName = "price", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Price { get; set; }
        [JsonProperty(PropertyName = "variants", NullValueHandling = NullValueHandling.Ignore)]
        public List<Variant> Variants { get; set; }
        [JsonProperty(PropertyName = "field_mask", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> FieldMask { get; set; }
        [JsonProperty(PropertyName = "special_price", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? SalePrice { get; set; }
        [JsonProperty(PropertyName = "promotion_from_date", NullValueHandling = NullValueHandling.Ignore)]
        public string SaleStartDate { get; set; }
        [JsonProperty(PropertyName = "promotion_to_date", NullValueHandling = NullValueHandling.Ignore)]
        public string SaleEndDate { get; set; }
       

    }
    public class Variant
    {
        [JsonProperty(PropertyName = "variant_sku", NullValueHandling = NullValueHandling.Ignore)]
        public string VariantSku { get; set; }
        [JsonProperty(PropertyName = "variant_price", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? VariantPrice { get; set; }
        [JsonProperty(PropertyName = "variant_quantity", NullValueHandling = NullValueHandling.Ignore)]
        public int? VariantQuantity { get; set; }
        [JsonProperty(PropertyName = "variant_attribute_hash", NullValueHandling = NullValueHandling.Ignore)]
        public string VariantAttributeHash { get; set; }
        [JsonProperty(PropertyName = "field_mask", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> FieldMask { get; set; }
        public long ParentProductId { get; set; }
        [JsonProperty(PropertyName = "variant_special_price", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? VariantSalePrice { get; set; }
        [JsonProperty(PropertyName = "variant_promotion_start_date", NullValueHandling = NullValueHandling.Ignore)]
        public string VariantSaleStartDate { get; set; }
        [JsonProperty(PropertyName = "variant_promotion_end_date", NullValueHandling = NullValueHandling.Ignore)]
        public string VariantSaleEndDate { get; set; }
    }

    public class SendoUpdateProductRequest
    {
        public List<SendoUpdateProduct> Products { get; set; }
    }
    public class SendoUpdateProductResponse : SendoRequestBase<SendoUpdateProductDetailResponse>
    {

    }

    public class SendoUpdateProductDetailResponse
    {
        [JsonProperty(PropertyName = "result")]
        public List<SendoUpdateProductItemDetailResponse> Result { get; set; }
    }

    public class SendoUpdateProductItemDetailResponse
    {
        [JsonProperty(PropertyName = "product_id")]
        public long ProductId { get; set; }
        [JsonProperty(PropertyName = "error_message")]
        public string ErrorMessage { get; set; }
    }

    public class ErrorList
    {
        [JsonProperty(PropertyName = "product_id")]
        public int ProductId { get; set; }
        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }
    }

    public class SuccessList
    {
        [JsonProperty(PropertyName = "product_id")]
        public int ProductId { get; set; }
    }
    #endregion

    #region Orders
    public class SendoOrderListRequest
    {
        [JsonProperty(PropertyName = "page_size")]
        public int PageSize { get; set; }
        [JsonProperty(PropertyName = "order_status_date_from", NullValueHandling = NullValueHandling.Ignore)]
        public string OrderStatusDateFrom { get; set; }
        [JsonProperty(PropertyName = "order_status_date_to", NullValueHandling = NullValueHandling.Ignore)]
        public string OrderStatusDateTo { get; set; }
        [JsonProperty(PropertyName = "token")]
        public string Token { get; set; }
    }
    public class SendoOrderListResponse
    {
        [JsonProperty(PropertyName = "result")]
        public SendoOrderListData Result { get; set; }
        [JsonProperty(PropertyName = "status_code")]
        public int StatusCode { get; set; }
        [JsonProperty(PropertyName = "error")]
        public string Error { get; set; }
        [JsonProperty(PropertyName = "success")]
        public bool Success { get; set; }

    }

    public class SendoOrderListData : BaseListResponse<SendoOrder>
    {
        [JsonProperty(PropertyName = "data")]
        public List<SendoOrder> Orders { get; set; }
        public SendoOrderListData()
        {
            Orders = new List<SendoOrder>();
        }
    }

    public class SendoOrderDetailResponse : SendoRequestBase<SendoOrder>
    {

    }

    public class SendoOrder
    {
        [JsonProperty(PropertyName = "sales_order")]
        public SaleOrder SaleOrder { get; set; }
        [JsonProperty(PropertyName = "sku_details")]
        public List<SkuDetail> SkuDetails { get; set; }
    }

    public class SaleOrder
    {
        [JsonProperty(PropertyName = "order_number")]
        public string OrderNumber { get; set; }
        [JsonProperty(PropertyName = "order_status")]
        public byte OrderStatus { get; set; }

        [JsonProperty(PropertyName = "order_date_time_stamp")]
        public long OrderDateTimeStamp { get; set; }
        [JsonProperty(PropertyName = "created_date_time_stamp")]
        public long CreatedDateTimeStamp { get; set; }

        [JsonProperty(PropertyName = "submit_date_time_stamp")]
        public long? SubmitDateTimeStamp { get; set; }

        [JsonProperty(PropertyName = "updated_date_time_stamp")]
        public long UpdatedDateTimeStamp { get; set; }

        [JsonProperty(PropertyName = "delivery_status")]
        public byte? DeliveryStatus { get; set; }
        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }
        [JsonProperty(PropertyName = "tracking_number")]
        public string TrackingNumber { get; set; }
        [JsonProperty(PropertyName = "receiver_name")]
        public string ReceiverName { get; set; }
        [JsonProperty(PropertyName = "receiver_address")]
        public string ReceiverAddress { get; set; }
        [JsonProperty(PropertyName = "ship_to_address")]
        public string ShipToAddress { get; set; }
        [JsonProperty(PropertyName = "ship_to_ward")]
        public string ShipToWard { get; set; }
        [JsonProperty(PropertyName = "ship_to_district_id")]
        public int ShipToDistrictId { get; set; }
        [JsonProperty(PropertyName = "buyer_phone")]
        public string BuyerPhone { get; set; }
        [JsonProperty(PropertyName = "buyer_address")]
        public string BuyerAddress { get; set; }
        [JsonProperty(PropertyName = "shipping_contact_phone")]
        public string ShippingContactPhone { get; set; }
        [JsonProperty(PropertyName = "sub_total")]
        public decimal SubTotal { get; set; }
        [JsonProperty(PropertyName = "shop_voucher_amount")]
        public decimal? ShopVoucherAmount { get; set; }
    }

    public class SkuDetail
    {
        [JsonProperty(PropertyName = "product_variant_id")]
        public string ProductVariantId { get; set; }
        [JsonProperty(PropertyName = "sku")]
        public string Sku { get; set; }
        [JsonProperty(PropertyName = "product_name")]
        public string ProductName { get; set; }
        [JsonProperty(PropertyName = "quantity")]
        public int Quantity { get; set; }
        [JsonProperty(PropertyName = "price")]
        public decimal Price { get; set; }
        [JsonProperty(PropertyName = "attribute_hash")]
        public string AttributeHash { get; set; }
    }
    #endregion

    #region location
    public class SendoLocationResponse<T> where T : class
    {
        [JsonProperty(PropertyName = "result")]
        public List<T> Result { get; set; }
        [JsonProperty(PropertyName = "status_code")]
        public int StatusCode { get; set; }
        [JsonProperty(PropertyName = "error")]
        public string Error { get; set; }
        [JsonProperty(PropertyName = "success")]
        public bool Success { get; set; }

        public SendoLocationResponse() 
        {
            Result = new List<T>();
        }
    }

    public class SendoProvinceResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<SendoDistrictResponse> Districts { get; set; }
    }


    public class SendoDistrictResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<SendoWardResponse> Wards { get; set; }

    }

    public class SendoWardResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    #endregion
}
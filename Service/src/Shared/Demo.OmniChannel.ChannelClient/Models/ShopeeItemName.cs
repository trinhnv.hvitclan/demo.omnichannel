﻿using System;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.Models
{
    public class GroupShopeeItemName
    {
        public long RetailerId { get; set; }
        public long ChannelId { get; set; }
        public List<string> ItemIds { get; set; }
    }

    public class ShopeeItemName
    {
        public DateTime CreateTime { get; set; }
        public DateTime? CompletedTime { get; set; }
        public string ItemId { get; set; }
        public string ItemName { get; set; }
        public List<Variations> Variationses { get; set; }
    }
}

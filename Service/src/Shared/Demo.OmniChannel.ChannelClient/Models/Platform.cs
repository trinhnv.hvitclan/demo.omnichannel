﻿using Newtonsoft.Json;

namespace Demo.OmniChannel.ChannelClient.Models
{
    public class Platform
    {
        public long Id { get; set; }
        public bool IsCurrent { get; set; }
        public string Data { get; set; }
        public BaseApp DataObject => ParseJsonObject(Data);
        private BaseApp ParseJsonObject(string data)
        {
            if (string.IsNullOrEmpty(data)) return null;
            try
            {
                return JsonConvert.DeserializeObject<BaseApp>(data);
            }
            catch
            {
                return null;
            }
        }
    }
    public class BaseApp
    {
        public long PartnerId { get; set; }
        public string Token { get; set; }
        public string AppKey { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.Models
{
    public class BaseShopeeResponse
    {
        [JsonProperty(PropertyName = "request_id")]
        public string RequestId { get; set; }
        public string Request { get; set; }
        [JsonProperty(PropertyName = "error")]
        public string Error { get; set; }
        [JsonProperty(PropertyName = "msg")]
        public string Msg { get; set; }
        [JsonProperty(PropertyName = "error_desc")]
        public string ErrorDesc { get; set; }

        [JsonProperty(PropertyName = "warning")]
        public List<string> WarningLst { get; set; }
    }
}

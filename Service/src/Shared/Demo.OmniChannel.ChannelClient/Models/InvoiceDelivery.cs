﻿using System;

namespace Demo.OmniChannel.ChannelClient.Models
{
    public class InvoiceDelivery
    {
        public long? InvoiceId { get; set; }
        public long? DeliveryBy { get; set; }
        public string DeliveryCode { get; set; }
        public string ServiceType { get; set; }
        public string ServiceTypeText { get; set; }
        public byte Status { get; set; }
        public string StatusValue { get; set; }
        public decimal? Price { get; set; }
        public string Receiver { get; set; }
        public string ContactNumber { get; set; }
        public string Address { get; set; }
        public int? LocationId { get; set; }
        public string LocationName { get; set; }
        public string WardName { get; set; }
        public double? Weight { get; set; }
        public double? Length { get; set; }
        public double? Width { get; set; }
        public double? Height { get; set; }
        public bool? UsingPriceCod { get; set; }
        public decimal? PriceCodPayment { get; set; }
        public long? PartnerDeliveryId { get; set; }
        public PartnerDelivery PartnerDelivery { get; set; }
        public long? BranchTakingAddressId { get; set; }
        public string BranchTakingAddressStr { get; set; }
        public DateTime? ExpectedDelivery { get; set; }
        public string ChannelState { get; set; }
        public string ChannelCity { get; set; }
        public string ChannelWard { get; set; }
        public int ReceiverDistrictId { get; set; }
        public string FeeJson { get; set; }
        public byte LatestStatus { get; set; }
    }
}
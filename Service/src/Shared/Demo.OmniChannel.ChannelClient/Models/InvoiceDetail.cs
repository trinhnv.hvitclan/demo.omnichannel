﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Demo.OmniChannel.ChannelClient.Models
{
    public class InvoiceDetail
    {
        public double ReturnQuantity;
        public long ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public double Quantity { get; set; }
        public Decimal Price { get; set; }
        public Decimal? Discount { get; set; }
        public double? DiscountRatio { get; set; }
        public bool? UsePoint { get; set; }
        public Decimal SubTotal { get; set; }
        public string Note { get; set; }
        public string SerialNumbers { get; set; }
        public string ProductChannelId { get; set; }
        public string ProductChannelSku { get; set; }
        public string ProductChannelName { get; set; }
        public string ParentChannelProductId { get; set; }
        public float DiscountPrice { get; set; }
        public bool UseProductBatchExpire { get; set; }
        public bool UseProductSerial { get; set; }
        public long? ProductBatchExpireId { get; set; }
        public long VariationId { get; set; }
        public long ItemId { get; set; }
        public bool? IsUsed { get; set; }
        public long AddOnDealId { get; set; }
        public bool IsBatchExpireControl { get; set; }
        public bool IsLotSerialControl { get; set; }
        public string Uuid { get; set; }
        public bool UseWarranty { get; set; }

        public void UpdatePropetiesUseWarranty(List<InvoiceWarranties> invoiceWarranties)
        {
            if (invoiceWarranties == null) return;
            this.UseWarranty = invoiceWarranties.Where(x => x.ProductId == this.ProductId).Any();
        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.Models
{
    public class DeliveryInfo
    {
        public long Id { get; set; }
        public long? InvoiceId { get; set; }
        public long? OrderId { get; set; }
        public int RetailerId { get; set; }
        public long? DeliveyPackageId { get; set; }
        public long? DeliveryBy { get; set; }
        public string DeliveryCode { get; set; }
        public string ServiceType { get; set; }
        public string ServiceAdd { get; set; }
        public decimal? Price { get; set; }
        public decimal? PriceCodPayment { get; set; }
        public DateTime? ExpectedDelivery { get; set; }
        public string ExpectedDeliveryText { get; set; }
        public bool? UseDefaultPartner { get; set; }
        public byte Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public string ServiceTypeText { get; set; }
        public bool? IsCurrent { get; set; }
        public bool? IsVoidByCarrier { get; set; }
        public string SortCode { get; set; }
        public string DebtControlCode { get; set; }
        public string StatusNote { get; set; }
        public decimal? TotalPrice { get; set; }
        public long? BranchTakingAddressId { get; set; }
        public string BranchTakingAddressStr { get; set; }
        public string FeeJson { get; set; }
        public decimal? OriginalCod { get; set; }
        public DeliveryPackage DeliveryPackage { get; set; }
        public List<PartnerDelivery> PartnerDelivery { get; set; }
    }
    public class DeliveryInfoDTO
    {
        public long DeliveryInfoId { get; set; }
        public bool? IsCurrent { get; set; }
        public byte Status { get; set; }
        public bool? UseDefaultPartner { get; set; }
        public long? DeliveryPackageId { get; set; }
        public string DeliveryCode { get; set; }
        public double? Weight { get; set; }
        public double? Length { get; set; }
        public double? Width { get; set; }
        public double? Height { get; set; }
        public string Receiver { get; set; }
        public string ContactNumber { get; set; }
        public string Address { get; set; }
        public int? LocationId { get; set; }
        public string LocationName { get; set; }
        public string WardName { get; set; }
        public string Comments { get; set; }
        public bool? UsingCod { get; set; }
        public long? WardId { get; set; }
        public double? ConvertedWeight { get; set; }
        public byte? Type { get; set; }
        public long? PartnerId { get; set; }
        public string PartnerCode { get; set; }
        public string PartnerName { get; set; }
    }
}
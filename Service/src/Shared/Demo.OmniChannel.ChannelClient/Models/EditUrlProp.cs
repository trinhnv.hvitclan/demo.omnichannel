﻿namespace Demo.OmniChannel.ChannelClient.Models
{
    public class EditUrlProp
    {
        public byte ChannelType { get; set; }
        public string ParentItemId { get; set; }
        public string ShopId { get; set; }
        public string UrlTemplate { get; set; }
        public long? SuperId { get; set; }

        public EditUrlProp(byte channelType, string parentItemId, string shopId, long? superId, string urlTemplate)
        {
            this.ChannelType = channelType;
            this.ParentItemId = parentItemId;
            this.ShopId = shopId;
            this.SuperId = superId;
            this.UrlTemplate = urlTemplate;
        }
    }
}

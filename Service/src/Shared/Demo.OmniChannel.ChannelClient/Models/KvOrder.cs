﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Demo.OmniChannel.ChannelClient.Models
{
    public class KvOrder
    {
        public long Id { get; set; }
        public string Uuid { get; set; }
        public string Code { get; set; }
        public string Ordersn { get; set; }
        public DateTime PurchaseDate { get; set; }
        public int BranchId { get; set; }
        public string BranchName { get; set; }
        public long? SoldById { get; set; }
        public string SoldByName { get; set; }
        public long? CustomerId { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerLocation { get; set; }
        public string CustomerWard { get; set; }
        public string Extra { get; set; }
        public decimal Total { get; set; }
        public decimal TotalPayment { get; set; }
        public decimal? Discount { get; set; }
        public double? DiscountRatio { get; set; }
        public int Status { get; set; }
        public string StatusValue { get; set; }
        public int RetailerId { get; set; }
        public string Description { get; set; }
        public bool UsingCod { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public InvoiceDelivery OrderDelivery { get; set; }
        public int? SaleChannelId { get; set; }
        public List<KvOrderDetail> OrderDetails { get; set; }
        public List<InvoiceWarranties> InvoiceWarranties { get; set; }
        public bool IsSyncSuccess { get; set; }
        public byte? NewStatus { get; set; }
        public string ChannelStatus { get; set; }
        public string ChannelOrder { get; set; }
        public string ChannelDiscount { get; set; }
        public bool ChannelIsCancelOrder { get; set; }
        public bool IsRetryOrder { get; set; }
        public string LogisticsChannelStatus { get; set; }
        public byte? DeliveryStatus { get; set; }
        public DateTime ChannelCreatedDate { get; set; }
        public float ShippingFee { get; set; }
        private List<SurCharge> _surCharges;
        public List<SurCharge> SurCharges
        {
            get => _surCharges;
            set { _surCharges = value?.Where(x => x != null).ToList(); }
        }
        public decimal? Surcharge { get; set; }
        public void UpdateCustomerCodeAndName(long id, string code, string name, string contactNumber)
        {
            CustomerId = id == 0 ? (long?) null : id;
            CustomerCode = code;
            CustomerName = name;
            CustomerPhone = contactNumber;
        }
    }

    public class InvoiceWarranties
    {
        public string Description { get; set; }
        public int NumberTime { get; set; }
        public int TimeType { get; set; }
        public int WarrantyType { get; set; }
        public long ProductId { get; set; }
        public string ProductCode { get; set; }
        public DateTime? ExpireDate { get; set; }
        public int Status { get; set; }
        public string InvoiceDetailUuid { get; set; }
        public int? ProductQty { get; set; }
    }
}
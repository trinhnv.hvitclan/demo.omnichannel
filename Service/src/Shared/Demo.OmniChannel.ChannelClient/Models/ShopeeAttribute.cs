﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Demo.OmniChannel.ShareKernel.Dtos;
using Newtonsoft.Json;

namespace Demo.OmniChannel.ChannelClient.Models
{
    public class ShopeeCategoryMessageResponse : BaseShopeeResponse
    {
        [JsonProperty(PropertyName = "categories")]
        public List<ShopeeCategory> Categories { get; set; }
    }

    public class ShopeeCategory
    {
        [JsonProperty(PropertyName = "category_id")]
        public long CategoryId { get; set; }

        [JsonProperty(PropertyName = "parent_id")]
        public long ParentId { get; set; }

        [JsonProperty(PropertyName = "category_name")]
        public string CategoryName { get; set; }

        [JsonProperty(PropertyName = "has_children")]
        public bool HasChildren { get; set; }

        [JsonProperty(PropertyName = "days_to_ship_limits")]
        public List<DaysToShipLimit> DaysToShipLimits { get; set; }

        [JsonProperty(PropertyName = "is_supp_sizechart")]
        public bool IsSuppSizechart { get; set; }
    }

    public class DaysToShipLimit
    {
        [JsonProperty(PropertyName = "max_limit")]
        public float MaxLimit { get; set; }

        [JsonProperty(PropertyName = "min_limit")]
        public float MinLimit { get; set; }
    }

    public class GetListAttributeRequest : IncludeLanguageBaseRequest
    {
        [JsonProperty(PropertyName = "category_id")]
        public long CategoryId { get; set; }
        [JsonProperty(PropertyName = "country")]
        public string Country { get; set; }
    }

    public class ShopeeAttributeResponse : BaseShopeeResponse
    {
        [JsonProperty(PropertyName = "attributes")]
        public List<ShopeeAttribute> ShopeeAttributes { get; set; }
    }

    public class ShopeeAttribute
    {
        [JsonProperty(PropertyName = "attribute_id")]
        public long AttributeId { get; set; }

        [JsonProperty(PropertyName = "attribute_name")]
        public string AttributeName { get; set; }

        [JsonProperty(PropertyName = "is_mandatory")]
        public bool IsMandatory { get; set; }

        [JsonProperty(PropertyName = "attribute_type")]
        public string AttributeType { get; set; }

        [JsonProperty(PropertyName = "input_type")]
        public string InputType { get; set; }

        [JsonProperty(PropertyName = "options")]
        public List<string> Options { get; set; }

        [JsonProperty(PropertyName = "values")]
        public List<ShopeeAttributeValue> Values { get; set; }

        [JsonProperty(PropertyName = "format_type")]
        public string FormatType { get; set; }

        [JsonProperty(PropertyName = "unit_list")]
        public List<string> UnitList { get; set; }
    }

    public class ShopeeAttributeValue
    {
        [JsonProperty(PropertyName = "original_value")]
        public string OriginalValue { get; set; }

        [JsonProperty(PropertyName = "translate_value")]
        public string TranslateValue { get; set; }
        public int ValueId { get; set; }
    }
}

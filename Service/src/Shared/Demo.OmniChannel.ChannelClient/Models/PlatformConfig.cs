using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.Models
{
    public class PlatformConfig
    {
        public byte Type { get; set; }

        public List<int> IncludeRetail { get; set; } = new List<int>();
    }
}
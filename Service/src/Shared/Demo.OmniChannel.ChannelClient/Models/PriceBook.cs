﻿namespace Demo.OmniChannel.ChannelClient.Models
{
    public class PriceBook
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
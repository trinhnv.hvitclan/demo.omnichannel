﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using Demo.OmniChannel.ShareKernel.Common;

namespace Demo.OmniChannel.ChannelClient.Models
{
    public class OmniChannelSettingObject
    {
        public Dictionary<string, string> Data { get; set; }
        public bool IsConfirmReturning
        {
            get
            {
                bool.TryParse(GetPropValue(MethodBase.GetCurrentMethod().Name, true), out var boolParse);
                return boolParse;
            }
            set => SetPropValue(MethodBase.GetCurrentMethod().Name, value);
        }
        public bool IsAutoSyncBatchExpire
        {
            get
            {
                bool.TryParse(GetPropValue(MethodBase.GetCurrentMethod().Name, false), out var boolParse);
                return boolParse;
            }
            set => SetPropValue(MethodBase.GetCurrentMethod().Name, value);
        }
        public int SyncOrderFormulaType
        {
            get
            {
                int.TryParse(GetPropValue(MethodBase.GetCurrentMethod().Name, 0), out var intParse);
                return intParse;
            }
            set => SetPropValue(MethodBase.GetCurrentMethod().Name, value);
        }
        public DateTime? SyncOrderFormulaDateTime
        {
            get
            {
                var getValue = GetPropValue(MethodBase.GetCurrentMethod().Name, default);
                if (DateTimeOffset.TryParse(getValue,
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.AssumeUniversal,
                    out var dateTime))
                {
                    return dateTime.DateTime;
                }
                return null;
            }
            set => SetPropValue(MethodBase.GetCurrentMethod().Name, value);
        }
        public bool IsAutoCreatingProduct
        {
            get
            {
                bool.TryParse(GetPropValue(MethodBase.GetCurrentMethod().Name, false), out var boolParse);
                return boolParse;
            }
            set => SetPropValue(MethodBase.GetCurrentMethod().Name, value);
        }

        public bool DistributeMultiMapping
        {
            get
            {
                var disVal = GetPropValue(MethodBase.GetCurrentMethod().Name, true);
                if (string.IsNullOrEmpty(disVal)) return true;
                bool.TryParse(disVal, out var boolParse);
                return boolParse;
            }
            set => SetPropValue(MethodBase.GetCurrentMethod().Name, value);
        }

        public bool IsSentExpirationNotification
        {
            get
            {
                bool.TryParse(GetPropValue(MethodBase.GetCurrentMethod().Name, false), out var boolParse);
                return boolParse;
            }
            set => SetPropValue(MethodBase.GetCurrentMethod().Name, value);
        }

        public int SyncOrderSaleTimeType
        {
            get
            {
                int.TryParse(GetPropValue(MethodBase.GetCurrentMethod().Name, 0), out var intParse);
                return intParse;
            }
            set => SetPropValue(MethodBase.GetCurrentMethod().Name, value);
        }
        public string GetPropValue(string propName, object def)
        {
            propName = propName.Replace("get_", "").Replace("set_", "");
            var val = def?.ToString();
            if (Data != null && Data.ContainsKey(propName))
            {
                val = Data[propName];
            }
            return val;
        }
        public void SetPropValue(string propName, object value)
        {
            propName = propName.Replace("get_", "").Replace("set_", "");
            if (Data.ContainsKey(propName))
            {
                Data[propName] = value.ToString();
            }
            else
            {
                Data.Add(propName, value.ToString());
            }
        }
        public bool IsAutoCopyProduct
        {
            get
            {
                bool.TryParse(GetPropValue(MethodBase.GetCurrentMethod().Name, false), out var boolParse);
                return boolParse;
            }
            set => SetPropValue(MethodBase.GetCurrentMethod().Name, value);
        }
    }
}
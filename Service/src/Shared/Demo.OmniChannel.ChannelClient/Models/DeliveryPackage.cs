﻿namespace Demo.OmniChannel.ChannelClient.Models
{
    public class DeliveryPackage
    {
        public long Id { get; set; }
        public long? InvoiceId { get; set; }
        public long? OrderId { get; set; }
        public double? Weight { get; set; }
        public double? Length { get; set; }
        public double? Width { get; set; }
        public double? Height { get; set; }
        public string Receiver { get; set; }
        public string ContactNumber { get; set; }
        public string Address { get; set; }
        public int? LocationId { get; set; }
        public string LocationName { get; set; }
        public string WardName { get; set; }
        public string Comments { get; set; }
        public bool? UsingCod { get; set; }
        public long? WardId { get; set; }
        public double? ConvertedWeight { get; set; }
        public byte? Type { get; set; }
        public int RetailerId { get; set; }
    }
}
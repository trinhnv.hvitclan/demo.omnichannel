﻿using Demo.OmniChannel.ChannelClient.FeatureToggle.@base;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.ChannelClient.FeatureToggle
{
    public class OmniSearchProductElasticToggle : OmniBaseFeatureToggle
    {
        public OmniSearchProductElasticToggle(IAppSettings appSettings) : base(appSettings)
        {
        }
    }
}

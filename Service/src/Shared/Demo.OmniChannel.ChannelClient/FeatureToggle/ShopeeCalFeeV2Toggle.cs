﻿using Demo.OmniChannel.ChannelClient.FeatureToggle.@base;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.ChannelClient.FeatureToggle
{
    public class ShopeeCalFeeV2Toggle : OmniBaseFeatureToggle
    {
        public ShopeeCalFeeV2Toggle(IAppSettings appSettings) : base(appSettings)
        {
        }
    }
}

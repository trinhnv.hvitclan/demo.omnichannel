﻿using Demo.OmniChannel.ChannelClient.FeatureToggle.@base;
using ServiceStack.Configuration;
using System.ComponentModel;

namespace Demo.OmniChannel.ChannelClient.FeatureToggle
{
    [Description("Use Feature to publish message to other queue, what is config in appSetting, name=TiktokSyncOnHandMessageV2_Topic")]
    public class TiktokSyncOnHandMessageV2Toggle : OmniBaseFeatureToggle
    {
        public TiktokSyncOnHandMessageV2Toggle(IAppSettings appSettings) : base(appSettings)
        {
        }
    }
}

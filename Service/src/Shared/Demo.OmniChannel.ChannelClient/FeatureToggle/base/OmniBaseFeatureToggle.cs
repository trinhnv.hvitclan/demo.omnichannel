﻿using System;
using System.Collections.Generic;
using System.Linq;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.ChannelClient.FeatureToggle.@base
{
    public class OmniBaseFeatureToggle
    {
        private IAppSettings AppSettings { get; set; }

        public OmniBaseFeatureToggle(IAppSettings appSettings)
        {
            AppSettings = appSettings;
        }

        public OmniFeatureToggleConfig GetConfig()
        {
            var type = GetType();

            var retailerIdSetting = AppSettings.Get($"{type.Name}_RetailerId", "");
            var retailerIds = string.IsNullOrEmpty(retailerIdSetting) ? new List<int>() : retailerIdSetting.Split(',')
                .Select(ConvertToInt32).ToList();

            var retailerIdIgnoreSetting = AppSettings.Get($"{type.Name}_RetailerIdIgnore", "");
            var retailerIdsIgnore = string.IsNullOrEmpty(retailerIdIgnoreSetting) ? new List<int>() : retailerIdIgnoreSetting.Split(',')
                .Select(ConvertToInt32).ToList();

            var groupIdSetting = AppSettings.Get($"{type.Name}_GroupId", "");
            var groupIds = string.IsNullOrEmpty(groupIdSetting) ? new List<int>() : groupIdSetting.Split(',').Select(ConvertToInt32)
                .ToList();

            var groupIdIgnoreSetting = AppSettings.Get($"{type.Name}_GroupIdIgnore", "");
            var groupIdsIgnore = string.IsNullOrEmpty(groupIdIgnoreSetting) ? new List<int>() : groupIdIgnoreSetting.Split(',')
                .Select(ConvertToInt32).ToList();

            var config = new OmniFeatureToggleConfig()
            {
                All = AppSettings.Get($"{type.Name}_All", false),
                GroupIds = groupIds,
                GroupIdsIgnore = groupIdsIgnore,
                RetailerIds = retailerIds,
                RetailerIdsIgnore = retailerIdsIgnore,
            };
            return config;
        }

        public bool Enable(int retailerId, int groupId)
        {
            var config = GetConfig();
            return config.FeatureEnable(retailerId, groupId);
        }

        private int ConvertToInt32(object obj)
        {
            int retVal;

            try
            {
                retVal = Convert.ToInt32(obj);
            }
            catch
            {
                retVal = 0;
            }

            return retVal;
        }
    }

    public class OmniFeatureToggleConfig
    {
        public bool All { get; set; }
        public List<int> GroupIds { get; set; }
        public List<int> GroupIdsIgnore { get; set; }
        public List<int> RetailerIds { get; set; }
        public List<int> RetailerIdsIgnore { get; set; }

        public bool FeatureEnable(int retailerId, int groupId)
        {
            if (RetailerIdsIgnore.Any() && RetailerIdsIgnore.Contains(retailerId)) return false;
            if (GroupIdsIgnore.Any() && GroupIdsIgnore.Contains(groupId)) return false;

            return All || GroupIds.Any() && GroupIds.Contains(groupId) ||
                   RetailerIds.Any() && RetailerIds.Contains(retailerId);
        }
    }
}

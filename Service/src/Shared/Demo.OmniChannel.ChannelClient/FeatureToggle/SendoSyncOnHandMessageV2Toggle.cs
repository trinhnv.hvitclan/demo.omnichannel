﻿using Demo.OmniChannel.ChannelClient.FeatureToggle.@base;
using ServiceStack.Configuration;
using System.ComponentModel;

namespace Demo.OmniChannel.ChannelClient.FeatureToggle
{
    [Description("Use Feature to publish message to other queue, what is config in appSetting, name=SendoSyncOnHandMessageV2_Topic")]
    public class SendoSyncOnHandMessageV2Toggle : OmniBaseFeatureToggle
    {
        public SendoSyncOnHandMessageV2Toggle(IAppSettings appSettings) : base(appSettings)
        {
        }
    }
}

﻿using Demo.OmniChannel.ChannelClient.FeatureToggle.@base;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.ChannelClient.FeatureToggle
{
    public class OmniSearchProductV2Toggle : OmniBaseFeatureToggle
    {
        public OmniSearchProductV2Toggle(IAppSettings appSettings) : base(appSettings)
        {
        }
    }
}

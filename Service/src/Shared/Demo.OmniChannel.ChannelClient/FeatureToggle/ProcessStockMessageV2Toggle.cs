﻿using Demo.OmniChannel.ChannelClient.FeatureToggle.@base;
using ServiceStack.Configuration;
using System.ComponentModel;

namespace Demo.OmniChannel.ChannelClient.FeatureToggle
{
    [Description("Use Feature to publish message to other queue, what is config in appSetting, name=ProcessStockMessageV2_Topic")]
    public class ProcessStockMessageV2Toggle : OmniBaseFeatureToggle
    {
        public ProcessStockMessageV2Toggle(IAppSettings appSettings) : base(appSettings)
        {
        }
    }
}

﻿using Demo.OmniChannel.ChannelClient.FeatureToggle.@base;
using ServiceStack.Configuration;
using System.ComponentModel;

namespace Demo.OmniChannel.ChannelClient.FeatureToggle
{
    [Description("Use Feature to publish message to other queue, what is config in appSetting, name=TikiSyncPriceMessageV2_Topic")]
    public class TikiSyncPriceMessageV2Toggle : OmniBaseFeatureToggle
    {
        public TikiSyncPriceMessageV2Toggle(IAppSettings appSettings) : base(appSettings)
        {

        }
    }
}

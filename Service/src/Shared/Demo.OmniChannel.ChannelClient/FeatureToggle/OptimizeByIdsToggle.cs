﻿using Demo.OmniChannel.ChannelClient.FeatureToggle.@base;
using ServiceStack.Configuration;
using System.ComponentModel;

namespace Demo.OmniChannel.ChannelClient.FeatureToggle
{
    [Description("Use when retailer many data, avoid index scan")]
    public class OptimizeByIdsToggle : OmniBaseFeatureToggle
    {
        public OptimizeByIdsToggle(IAppSettings appSettings) : base(appSettings)
        {
        }
    }
}

﻿using Demo.OmniChannel.ChannelClient.FeatureToggle.@base;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.ChannelClient.FeatureToggle
{
    public class TiktokCalFeeV2Toggle : OmniBaseFeatureToggle
    {
        public TiktokCalFeeV2Toggle(IAppSettings appSettings) : base(appSettings)
        {
        }
    }
}

﻿using Demo.OmniChannel.ChannelClient.FeatureToggle.@base;
using ServiceStack.Configuration;
using System.ComponentModel;

namespace Demo.OmniChannel.ChannelClient.FeatureToggle
{
    [Description("Use Feature to publish message to other queue, what is config in appSetting, name=TiktokSyncPriceMessageV2_Topic")]
    public class SendoSyncPriceMessageV2Toggle : OmniBaseFeatureToggle
    {
        public SendoSyncPriceMessageV2Toggle(IAppSettings appSettings) : base(appSettings)
        {

        }
    }
}

﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.EndPoints;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.RequestDTO.Shopee;
using Demo.OmniChannel.ChannelClient.RequestDTO.Tiktok;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Tiktok;
using Demo.OmniChannel.ShareKernel.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Demo.OmniChannel.ChannelClient.Helper
{
    public class TikTokHelper
    {
        private static string grantType = "authorized_code";
        /// <summary>
        /// Lấy danh sách accessToken và refreshToken
        /// </summary>
        /// <param name="code"></param>
        /// <param name="platform"></param>
        /// <returns></returns>
        public static async Task<IRestResponse<TiktokBaseResponse<TiktokTokenResponse>>> GetAccessToken(string code,
            Platform platform, bool isCallGrpcRetry = false)
        {
            var request = new TiktokTokenRequest
            {
                AppKey = platform.DataObject.AppKey,
                AppSecret = platform.DataObject.Token,
                AuthCode = code,
                GrantType = grantType
            };

            var url = $"{SelfAppConfig.Tiktok.AuthEndPoint}/{TiktokEndpoint.GetToken}";
            var result = await RequestHelper.PostRequest<TiktokBaseResponse<TiktokTokenResponse>, TiktokTokenRequest>(url, request, null, false, null, isCallGrpcRetry);
            return result;
        }


        /// <summary>
        /// RefreshToken lại accessToken
        /// </summary>
        /// <param name="refreshTokenResponse"></param>
        /// <param name="auth"></param>
        /// <param name="platform"></param>
        /// <returns></returns>
        public static async Task<IRestResponse<TiktokBaseResponse<TiktokTokenResponse>>> RefreshAccessToken(
            string refreshTokenResponse,
            Platform platform)
        {
            var request = new TiktokRefreshTokenRequest
            {
                AppKey = platform.DataObject.AppKey,
                AppSecret = platform.DataObject.Token,
                RefreshToken = refreshTokenResponse,
                GrantType = "refresh_token"
            };

            var url = $"{SelfAppConfig.Tiktok.AuthEndPoint}/{TiktokEndpoint.RefreshToken}";
            var result = await RequestHelper.PostRequest<TiktokBaseResponse<TiktokTokenResponse>, TiktokRefreshTokenRequest>(url, request, null, false);
            return result;
        }

        /// <summary>
        ///  Lấy thông tin cơ bản gian hàng
        /// </summary>
        /// <param name="auth"></param>
        /// <param name="platform"></param> 
        /// <returns></returns>
        public static async Task<IRestResponse<TiktokBaseResponse<TiktokShopListReponse>>> GetShopInfo(ChannelAuth auth, Platform platform,
           bool isCallGrpcRetry = false)
        {
            var url = $"{SelfAppConfig.Tiktok.Endpoint}/{TiktokEndpoint.GetAuthorizedShop}";
            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var param = new Dictionary<string, object>
            {
                { "timestamp", timestamp },
                { "app_key", platform.DataObject.AppKey }
            };

            var signature = GenerateSignature(platform.DataObject.Token, TiktokEndpoint.GetAuthorizedShop,
                param);
            param.Add("sign", signature);
            param.Add("access_token", auth.AccessToken);

            return await RequestHelper.GetRequest<TiktokBaseResponse<TiktokShopListReponse>>(url, param, null, false, isCallGrpcRetry);
        }

        /// <summary>
        /// Tạo chữ ký 
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="token"></param>
        /// <param name="timestamp"></param>
        /// <param name="accessToken"></param>
        /// <param name="shopId"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string GenerateSignature(string serectKey, string path, Dictionary<string, object> queryParams)
        {
            var partnerKey = Encoding.UTF8.GetBytes(serectKey);
            var signstring = $"{serectKey}{path}";
            foreach (var param in queryParams.OrderBy(x => x.Key))
            {
                signstring = signstring + param.Key + param.Value;
            }
            signstring = signstring + serectKey;
            var partnerString = Encoding.UTF8.GetBytes($"{signstring}");
            var hash = new HMACSHA256(partnerKey);
            var tmpSign = hash.ComputeHash(partnerString);
            return BitConverter.ToString(tmpSign).Replace("-", "").ToLower();
        }

        /// <summary>
        /// Lấy danh sách sản phẩm
        /// </summary>
        /// <param name="auth"></param>
        /// <param name="platform"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <param name="updatedFrom"></param>
        /// <returns></returns>
        public static async Task<IRestResponse<TiktokBaseResponse<TiktokProductsResponse>>> GetProducts(ChannelAuth auth,
            Platform platform, int pageNo, int pageSize, DateTime? updatedFrom = null, bool isCallGrpcRetry = false)
        {
            var url = $"{SelfAppConfig.Tiktok.Endpoint}/{TiktokEndpoint.GetProducts}";
            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var request = new TiktokProductsRequest
            {
                PageSize = pageSize,
                PageNumber = pageNo,
            };
            var param = new Dictionary<string, object>
            {
                { "timestamp", timestamp },
                { "app_key", platform.DataObject.AppKey }
            };
            if (updatedFrom.HasValue)
            {
                param.Add("update_time_from", ((DateTimeOffset)updatedFrom.Value).ToUnixTimeSeconds());
            }
            var signature = GenerateSignature(platform.DataObject.Token, TiktokEndpoint.GetProducts,
               param);
            param.Add("sign", signature);
            param.Add("access_token", auth.AccessToken);
            return await RequestHelper.PostRequest<TiktokBaseResponse<TiktokProductsResponse>, TiktokProductsRequest>(url, request, null, true, param, isCallGrpcRetry);
        }

        /// <summary>
        /// Lấy chi tiết đơn hàng
        /// </summary>
        /// <param name="auth"></param>
        /// <param name="orderSnList"></param>
        /// <param name="platform"></param>
        /// <param name="logInfo"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        public static async Task<IRestResponse<TiktokBaseResponse<TiktokOrderDetailsResponse>>> GetOrdersDetail(ChannelAuth auth, string[] orderSnList,
            Platform platform, LogObjectMicrosoftExtension logInfo, CancellationToken ct = default)
        {
            var logTiktokHelper = logInfo.Clone("TiktokHelperGetOrdersDetail");
            try
            {
                var request = new TiktokOrderDetailRequest
                {
                    OrderIdLst = orderSnList
                };

                var url = $"{SelfAppConfig.Tiktok.Endpoint}/{TiktokEndpoint.GetOrdersDetail}";
                var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
                var header = new Dictionary<string, string>();
                if (url.Contains("kvpos.com"))
                {
                    header.Add("kv-shop-id", auth.ShopId.ToString());
                }
                var param = new Dictionary<string, object>
            {
                { "app_key", platform.DataObject.AppKey },
                { "timestamp", timestamp },
                { "shop_id", auth.ShopId }
            };
                var signature = GenerateSignature(platform.DataObject.Token, TiktokEndpoint.GetOrdersDetail,
                   param);
                param.Add("sign", signature);
                param.Add("access_token", auth.AccessToken);

                return await RequestHelper.PostRequest<TiktokBaseResponse<TiktokOrderDetailsResponse>, TiktokOrderDetailRequest>(url, request, header, false, param);
            }
            catch (Exception ex)
            {
                if (logTiktokHelper == null) throw;
                logTiktokHelper.LogError(ex);
                throw;
            }
        }

        /// <summary>
        /// Lấy chi tiết phí
        /// </summary>
        /// <param name="auth"></param>
        /// <param name="orderSnList"></param>
        /// <param name="platform"></param>
        /// <param name="logInfo"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        public static async Task<IRestResponse<TiktokBaseResponse<GetOrderSettlementsResponse>>> GetGetOrderSettlements(ChannelAuth auth, string orderSn,
            Platform platform, LogObjectMicrosoftExtension logInfo, CancellationToken ct = default)
        {
            var logTiktokHelper = logInfo.Clone("GetGetOrderSettlements");
            try
            {
                var url = $"{SelfAppConfig.Tiktok.Endpoint}/{TiktokEndpoint.GetOrderSettlements}";
                var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
                var param = new Dictionary<string, object>
            {
                { "app_key", platform.DataObject.AppKey },
                { "timestamp", timestamp },
                { "shop_id", auth.ShopId },
                { "order_id", orderSn }
            };
                var signature = GenerateSignature(platform.DataObject.Token, TiktokEndpoint.GetOrderSettlements,
                   param);
                param.Add("sign", signature);
                param.Add("access_token", auth.AccessToken);

                return await RequestHelper.GetRequest<TiktokBaseResponse<GetOrderSettlementsResponse>>(url, param);
            }
            catch (Exception ex)
            {
                if (logTiktokHelper == null) throw;
                logTiktokHelper.LogError(ex);
                throw;
            }
        }

        /// <summary>
        /// Lấy chi tiết sản phẩm
        /// </summary>
        /// <param name="auth"></param>
        /// <param name="platform"></param>
        /// <param name="productId"></param>
        /// <returns></returns>

        public static async Task<IRestResponse<TiktokBaseResponse<TiktokProductDetailsResponse>>> GetProductDetail(ChannelAuth auth,
         Platform platform, string productId, bool isCallGrpc = false)
        {
            var url = $"{SelfAppConfig.Tiktok.Endpoint}/{TiktokEndpoint.GetProductDetail}";
            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var param = new Dictionary<string, object>
            {
                { "timestamp", timestamp },
                { "app_key", platform.DataObject.AppKey },
                { "shop_id", auth.ShopId },
                { "product_id", productId }
            };
            var signature = GenerateSignature(platform.DataObject.Token, TiktokEndpoint.GetProductDetail,
               param);
            param.Add("sign", signature);
            param.Add("access_token", auth.AccessToken);
            return await RequestHelper.GetRequest<TiktokBaseResponse<TiktokProductDetailsResponse>>(url, param, isCallGrpc: isCallGrpc);
        }

        /// <summary>
        /// Cập nhật tồn kho
        /// </summary>
        /// <param name="auth"></param>
        /// <param name="platform"></param>
        /// <param name="req"></param>
        /// <returns></returns>
        public static async Task<IRestResponse<TiktokBaseResponse<TiktokUpdateStockResponse>>> UpdateProductStock(ChannelAuth auth,
            Platform platform, TiktokUpdateStockRequest req)
        {
            var url = $"{SelfAppConfig.Tiktok.Endpoint}/{TiktokEndpoint.UpdateStock}";
            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var param = new Dictionary<string, object>
            {
                { "app_key", platform.DataObject.AppKey },
                { "timestamp", timestamp },
                { "shop_id", auth.ShopId }
            };
            var signature = GenerateSignature(platform.DataObject.Token, TiktokEndpoint.UpdateStock,
               param);
            param.Add("sign", signature);
            param.Add("access_token", auth.AccessToken);
            return await RequestHelper.PutRequest<TiktokBaseResponse<TiktokUpdateStockResponse>, TiktokUpdateStockRequest>(url, req, null, false, param);
        }

        /// <summary>
        /// Cập nhật giá
        /// </summary>
        /// <param name="auth"></param>
        /// <param name="platform"></param>
        /// <param name="req"></param>
        /// <returns></returns>
        public static async Task<IRestResponse<TiktokBaseResponse<TiktokUpdatePriceResponse>>> UpdateProductPrice(ChannelAuth auth,
            Platform platform, TiktokUpdatePriceRequest req)
        {
            var url = $"{SelfAppConfig.Tiktok.Endpoint}/{TiktokEndpoint.UpdatePrice}";
            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var param = new Dictionary<string, object>
            {
                { "app_key", platform.DataObject.AppKey },
                { "timestamp", timestamp },
                { "shop_id", auth.ShopId }
            };
            var signature = GenerateSignature(platform.DataObject.Token, TiktokEndpoint.UpdatePrice,
               param);
            param.Add("sign", signature);
            param.Add("access_token", auth.AccessToken);
            return await RequestHelper.PutRequest<TiktokBaseResponse<TiktokUpdatePriceResponse>, TiktokUpdatePriceRequest>(url, req, null, false, param);
        }

        /// <summary>
        /// Lấy thông tin giao hàng
        /// </summary>
        /// <param name="auth"></param>
        /// <param name="platform"></param>
        /// <param name="req"></param>
        /// <returns></returns>
        public static async Task<IRestResponse<TiktokBaseResponse<TiktokShippingInfo>>> GetShippingInfo(ChannelAuth auth,
            Platform platform, string orderId)
        {
            var url = $"{SelfAppConfig.Tiktok.Endpoint}/{TiktokEndpoint.GetShippingInfo}";
            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var param = new Dictionary<string, object>
            {
                { "app_key", platform.DataObject.AppKey },
                { "timestamp", timestamp },
                { "shop_id", auth.ShopId },
                { "order_id", orderId }
            };
            var signature = GenerateSignature(platform.DataObject.Token, TiktokEndpoint.GetShippingInfo,
               param);
            param.Add("sign", signature);
            param.Add("access_token", auth.AccessToken);
            return await RequestHelper.GetRequest<TiktokBaseResponse<TiktokShippingInfo>>(url, param, null, false);
        }

        /// <summary>
        /// Lấy thông tin chuyển hoàn, trả hàng..
        /// </summary>
        /// <param name="auth"></param>
        /// <param name="platform"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public static async Task<IRestResponse<TiktokBaseResponse<TiktokReverseOrderListResponse>>> GetReverseOrderList(ChannelAuth auth,
            Platform platform, TiktokReverseOrderRequest request)
        {
            var url = $"{SelfAppConfig.Tiktok.Endpoint}/{TiktokEndpoint.GetReverseOrderList}";
            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var param = new Dictionary<string, object>
            {
                { "app_key", platform.DataObject.AppKey },
                { "timestamp", timestamp },
                { "shop_id", auth.ShopId }
            };
            var signature = GenerateSignature(platform.DataObject.Token, TiktokEndpoint.GetReverseOrderList,
               param);
            param.Add("sign", signature);
            param.Add("access_token", auth.AccessToken);
            return await RequestHelper.PostRequest<TiktokBaseResponse<TiktokReverseOrderListResponse>, TiktokReverseOrderRequest>(url, request, null, true, param);
        }

        public static async Task SendToWebHookAsync(TiktokWebhookModel req)
        {
            var url = $"{SelfAppConfig.WebHookKvOnlineApiEndpointV2}/{TiktokEndpoint.Webhook}";
            await RequestHelper.PostRequest<TiktokWebhookModel, object>(url, req, null, false, null);
        }
    }
}
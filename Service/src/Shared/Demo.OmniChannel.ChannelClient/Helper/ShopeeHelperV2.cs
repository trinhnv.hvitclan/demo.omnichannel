﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.EndPoints;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.RequestDTO.Shopee;
using Demo.OmniChannel.ChannelClient.RequestDTO.Shopee.V2;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee.V2;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.ShareKernel.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Platform = Demo.OmniChannel.ChannelClient.Models.Platform;

namespace Demo.OmniChannel.ChannelClient.Helper
{
    public static class ShopeeHelperV2
    {
        /// <summary>
        /// Lấy danh sách accessToken và refreshToken
        /// </summary>
        /// <param name="code"></param>
        /// <param name="shopId"></param>
        /// <param name="platform"></param>
        /// <returns></returns>
        public static async Task<IRestResponse<ShopeeTokenReponseV2>> GetAccessToken(string code, long shopId,
            Platform platform, bool isCallGrpcRetry = false)
        {
            var request = new ShopeeTokenRequestV2
            {
                PartnerId = platform.DataObject.PartnerId,
                Code = code,
                ShopeeId = shopId
            };

            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var signature = GenerateSignature(platform.DataObject.PartnerId,
                platform.DataObject.Token, timestamp, string.Empty, null, ShopeeEndpointV2.GetToken);
            var param = new Dictionary<string, object>
            {
                { "sign", signature },
                { "partner_id", platform.DataObject.PartnerId },
                { "timestamp", timestamp }
            };
            var url = $"{SelfAppConfig.ShopeeApiEndpointV2}/{ShopeeEndpointV2.GetToken}";
            return await RequestHelper.PostRequest<ShopeeTokenReponseV2, ShopeeTokenRequestV2>(url, request, null, false, param, isCallGrpcRetry);
        }


        /// <summary>
        /// RefreshToken  lại accessToken
        /// </summary>
        /// <param name="refreshTokenResponse"></param>
        /// <param name="auth"></param>
        /// <param name="platform"></param>
        /// <returns></returns>
        public static async Task<IRestResponse<ShopeeRefreshTokenReponseV2>> RefreshAccessToken(
            string refreshTokenResponse,
            ChannelAuth auth,
            Platform platform)
        {
            var request = new ShopeeRefreshTokenRequestV2
            {
                PartnerId = platform.DataObject.PartnerId,
                RefreshToken = refreshTokenResponse,
                ShopeeId = auth.ShopId
            };

            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var signature = GenerateSignature(platform.DataObject.PartnerId,
                platform.DataObject.Token, timestamp, string.Empty, null, ShopeeEndpointV2.RefreshToken);
            var param = new Dictionary<string, object>
            {
                { "sign", signature },
                { "partner_id", platform.DataObject.PartnerId },
                { "timestamp", timestamp }
            };
            var url = $"{SelfAppConfig.ShopeeApiEndpointV2}/{ShopeeEndpointV2.RefreshToken}";
            var result = await RequestHelper.PostRequest<ShopeeRefreshTokenReponseV2, ShopeeRefreshTokenRequestV2>(url, request,
                null, false, param);

            return result;
        }

        /// <summary>
        ///  Lấy thông tin cơ bản gian hàng
        /// </summary>
        /// <param name="auth"></param>
        /// <param name="platform"></param> 
        /// <returns></returns>
        public static async Task<IRestResponse<ShopResponse>> GetShopInfo(ChannelAuth auth, Platform platform,
          bool isCallGrpcRetry = false)
        {
            var url = $"{SelfAppConfig.ShopeeApiEndpointV2}/{ShopeeEndpointV2.GetShopInfo}";
            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var signature = GenerateSignature(platform.DataObject.PartnerId,
                platform.DataObject.Token, timestamp, auth.AccessToken, auth.ShopId, ShopeeEndpointV2.GetShopInfo);
            var param = new Dictionary<string, object>
            {
                { "partner_id", platform.DataObject.PartnerId },
                { "timestamp", timestamp },
                { "access_token", auth.AccessToken },
                { "shop_id", auth.ShopId },
                { "sign", signature },
            };

            return await RequestHelper.GetRequest<ShopResponse>(url, param, null, false, isCallGrpcRetry);
        }

        /// <summary>
        /// Ký chữ ký Signature
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="token"></param>
        /// <param name="timestamp"></param>
        /// <param name="accessToken"></param>
        /// <param name="shopId"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string GenerateSignature(long partnerId, string token, long timestamp, string accessToken,
            long? shopId, string url)
        {
            var path = $"/api/v2/{url}";
            var partnerKey = Encoding.UTF8.GetBytes(token);
            var partnerString = Encoding.UTF8.GetBytes($"{partnerId}{path}{timestamp}{accessToken}{shopId}");
            var hash = new HMACSHA256(partnerKey);
            var tmpSign = hash.ComputeHash(partnerString);
            string sign = BitConverter.ToString(tmpSign).Replace("-", "").ToLower();
            return sign;
        }





        /// <summary>
        ///  Get model list of an item.
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="auth"></param>
        /// <param name="platform"></param> 
        /// <returns></returns>
        public static async Task<IRestResponse<ShopeeModelListReponseV2>> GetModelList(long itemId, ChannelAuth auth, Platform platform)
        {
            var url = $"{SelfAppConfig.ShopeeApiEndpointV2}/{ShopeeEndpointV2.GetModelList}";
            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var signature = GenerateSignature(platform.DataObject.PartnerId,
                platform.DataObject.Token, timestamp, auth.AccessToken, auth.ShopId, ShopeeEndpointV2.GetModelList);
            var param = new Dictionary<string, object>
            {
                { "partner_id", platform.DataObject.PartnerId },
                { "timestamp", timestamp },
                { "access_token", auth.AccessToken },
                { "shop_id", auth.ShopId },
                { "sign", signature },
                { "item_id", itemId}
            };

            return await RequestHelper.GetRequest<ShopeeModelListReponseV2>(url, param);
        }


        /// <summary>
        ///  Update price
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="auth"></param>
        /// <param name="platform"></param> 
        /// <returns></returns>
        public static async Task<IRestResponse<ShopUpdatePriceResponseV2>> UpdatePrice(ShopeeUpdatePriceRequestV2 request,
            ChannelAuth auth, Platform platform, bool isCallGrpc = false)
        {
            var url = $"{SelfAppConfig.ShopeeApiEndpointV2}/{ShopeeEndpointV2.UpdatePrice}";
            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var signature = GenerateSignature(platform.DataObject.PartnerId,
                platform.DataObject.Token, timestamp, auth.AccessToken, auth.ShopId, ShopeeEndpointV2.UpdatePrice);
            var param = new Dictionary<string, object>
            {
                { "partner_id", platform.DataObject.PartnerId },
                { "timestamp", timestamp },
                { "access_token", auth.AccessToken },
                { "shop_id", auth.ShopId },
                { "sign", signature },
            };

            var result = await RequestHelper.PostRequest<ShopUpdatePriceResponseV2, ShopeeUpdatePriceRequestV2>(url, request,
               null, false, param, isCallGrpc);

            return result;
        }


        /// <summary>
        ///  Update stock
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="auth"></param>
        /// <param name="platform"></param> 
        /// <returns></returns>
        public static async Task<IRestResponse<ShopUpdateStockResponseV2>> UpdateStock(ShopeeUpdateStockRequestV2 request,
            ChannelAuth auth,
            Platform platform,
            bool isCallGrpc = false)
        {
            var url = $"{SelfAppConfig.ShopeeApiEndpointV2}/{ShopeeEndpointV2.UpdateStock}";
            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var signature = GenerateSignature(platform.DataObject.PartnerId,
                platform.DataObject.Token, timestamp, auth.AccessToken, auth.ShopId, ShopeeEndpointV2.UpdateStock);
            var param = new Dictionary<string, object>
            {
                { "partner_id", platform.DataObject.PartnerId },
                { "timestamp", timestamp },
                { "access_token", auth.AccessToken },
                { "shop_id", auth.ShopId },
                { "sign", signature },
            };

            var result = await RequestHelper.PostRequest<ShopUpdateStockResponseV2, ShopeeUpdateStockRequestV2>(url, request,
               null, false, param, isCallGrpc);

            return result;
        }

        public static async Task<IRestResponse<BaseShopeeV2Response<ShopeeOrderDetailV2Response>>> GetOrdersDetail(ChannelAuth auth, string orderSnList,
         Platform platform, bool isCallGrpcRetry = false)
        {
            var url = $"{SelfAppConfig.ShopeeApiEndpointV2}/{ShopeeEndpointV2.GetOrderDetail}";
            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var signature = GenerateSignature(platform.DataObject.PartnerId,
            platform.DataObject.Token, timestamp, auth.AccessToken, auth.ShopId, ShopeeEndpointV2.GetOrderDetail);
            var param = new Dictionary<string, object>();
            param.Add("partner_id", platform.DataObject.PartnerId);
            param.Add("timestamp", timestamp);
            param.Add("access_token", auth.AccessToken);
            param.Add("shop_id", auth.ShopId);
            param.Add("sign", signature);
            param.Add("order_sn_list", orderSnList);
            param.Add("response_optional_fields", $"pickup_done_time,invoice_data,checkout_shipping_carrier,payment_method,item_list");
            return await RequestHelper.GetRequest<BaseShopeeV2Response<ShopeeOrderDetailV2Response>>(url, param, null, false, isCallGrpcRetry);
        }


        public static async Task<IRestResponse<BaseShopeeV2Response<ShopeeGetProductBaseInfoResponse>>> GetProductBaseInfo(ChannelAuth auth, long[] itemIdLst, Platform platform, bool isCallGrpcRetry = false)
        {
            var url = $"{SelfAppConfig.ShopeeApiEndpointV2}/{ShopeeEndpointV2.GetItemBaseInfo}";
            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var signature = GenerateSignature(platform.DataObject.PartnerId,
            platform.DataObject.Token, timestamp, auth.AccessToken, auth.ShopId, ShopeeEndpointV2.GetItemBaseInfo);
            var param = new Dictionary<string, object>();
            param.Add("partner_id", platform.DataObject.PartnerId);
            param.Add("timestamp", timestamp);
            param.Add("access_token", auth.AccessToken);
            param.Add("shop_id", auth.ShopId);
            param.Add("sign", signature);
            param.Add("item_id_list", $"{string.Join(",", itemIdLst)}");
            return await RequestHelper.GetRequest<BaseShopeeV2Response<ShopeeGetProductBaseInfoResponse>>(url, param, null, false, isCallGrpcRetry);
        }


        public static async Task<IRestResponse<BaseShopeeV2Response<ShopeeGetProductListV2Response>>> GetItemList(ChannelAuth auth, int offset, int pageSize,
          bool needDeletedItem, Platform platform, long? updateTimeFrom = null, long? updateTimeTo = null, bool isCallGrpcRetry = false)
        {
            var url = $"{SelfAppConfig.ShopeeApiEndpointV2}/{ShopeeEndpointV2.GetItemList}";
            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var signature = GenerateSignature(platform.DataObject.PartnerId,
            platform.DataObject.Token, timestamp, auth.AccessToken, auth.ShopId, ShopeeEndpointV2.GetItemList);
            var param = new List<BaseParametersDuplicateKey<string, string>>();
            param.Add(new BaseParametersDuplicateKey<string, string>("partner_id", platform.DataObject.PartnerId.ToString()));
            param.Add(new BaseParametersDuplicateKey<string, string>("timestamp", timestamp.ToString()));
            param.Add(new BaseParametersDuplicateKey<string, string>("access_token", auth.AccessToken));
            param.Add(new BaseParametersDuplicateKey<string, string>("shop_id", auth.ShopId.ToString()));
            param.Add(new BaseParametersDuplicateKey<string, string>("sign", signature));
            param.Add(new BaseParametersDuplicateKey<string, string>("offset", offset.ToString()));
            param.Add(new BaseParametersDuplicateKey<string, string>("page_size", pageSize.ToString()));
            if (updateTimeFrom.HasValue)
            {
                param.Add(new BaseParametersDuplicateKey<string, string>("update_time_from", updateTimeFrom.ToString()));
            }
            if (updateTimeTo.HasValue)
            {
                param.Add(new BaseParametersDuplicateKey<string, string>("updateTimeTo", updateTimeTo.ToString()));
            }
            if (needDeletedItem)
            {
                param.Add(new BaseParametersDuplicateKey<string, string>("item_status", ShopeeProductStatus.Deleted));
                param.Add(new BaseParametersDuplicateKey<string, string>("item_status", ShopeeProductStatus.Banned));
            }
            param.Add(new BaseParametersDuplicateKey<string, string>("item_status", ShopeeProductStatus.Nomal));
            param.Add(new BaseParametersDuplicateKey<string, string>("item_status", ShopeeProductStatus.Unlist));
            return await RequestHelper.GetRequestWithDuplicateKeyParams<BaseShopeeV2Response<ShopeeGetProductListV2Response>>(url, param, null, true, isCallGrpcRetry);
        }

        public static async Task<IRestResponse<BaseShopeeV2Response<ShopeePaymentEscrowDetailResponse>>> GetPaymentEscrowDetail(ChannelAuth auth, string orderId,
           Platform platform, bool isCallGrpcRetry = false)
        {
            var url = $"{SelfAppConfig.ShopeeApiEndpointV2}/{ShopeeEndpointV2.GetPaymentEscrowDetail}";
            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var signature = GenerateSignature(platform.DataObject.PartnerId,
            platform.DataObject.Token, timestamp, auth.AccessToken, auth.ShopId, ShopeeEndpointV2.GetPaymentEscrowDetail);
            var param = new Dictionary<string, object>();
            param.Add("partner_id", platform.DataObject.PartnerId);
            param.Add("timestamp", timestamp);
            param.Add("access_token", auth.AccessToken);
            param.Add("shop_id", auth.ShopId);
            param.Add("sign", signature);
            param.Add("order_sn", orderId);
            return await RequestHelper.GetRequest<BaseShopeeV2Response<ShopeePaymentEscrowDetailResponse>>(url, param, null, false, isCallGrpcRetry);
        }


        public static async Task<IRestResponse<BaseShopeeV2Response<ShopeeTrackingInfoV2Response>>> GetTrackingInfo(ChannelAuth auth, string orderId, Platform platform, bool isCallGrpcRetry = false)
        {
            var url = $"{SelfAppConfig.ShopeeApiEndpointV2}/{ShopeeEndpointV2.GetTrackingInfo}";
            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var signature = GenerateSignature(platform.DataObject.PartnerId,
            platform.DataObject.Token, timestamp, auth.AccessToken, auth.ShopId, ShopeeEndpointV2.GetTrackingInfo);
            var param = new Dictionary<string, object>();
            param.Add("partner_id", platform.DataObject.PartnerId);
            param.Add("timestamp", timestamp);
            param.Add("access_token", auth.AccessToken);
            param.Add("shop_id", auth.ShopId);
            param.Add("sign", signature);
            param.Add("order_sn", orderId);
            return await RequestHelper.GetRequest<BaseShopeeV2Response<ShopeeTrackingInfoV2Response>>(url, param, null, false, isCallGrpcRetry);
        }


        /// <summary>
        /// Lấy thông tin sàn thanh toán
        /// </summary>
        /// <param name="shopId"></param>
        /// <param name="pageOffSet"></param>
        /// <param name="pageSize"></param>
        /// <param name="createFrom"></param>
        /// <param name="createTo"></param>
        /// <param name="platform"></param>
        /// <param name="loggerExtension"></param>
        /// <returns></returns>
        public static async Task<IRestResponse<WalletTransactionListResponseV2>> GetListTransaction(ChannelAuth auth,
          int pageNo, int pageSize, long createFrom, long createTo, Platform platform,
          LogObjectMicrosoftExtension loggerExtension, bool isCallGrpcRetry = false)
        {
            var logShopeeHelper = loggerExtension.Clone("ShopeeHelperGetListTransaction");
            try
            {
                var timeStamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
                var signature = GenerateSignature(platform.DataObject.PartnerId,
                    platform.DataObject.Token, timeStamp,
                    auth.AccessToken, auth.ShopId, ShopeeEndpointV2.GetWalletTransactionList);

                var param = new Dictionary<string, object>
                {
                    { "partner_id", GetPartnerId(platform) },
                    { "timestamp", timeStamp},
                    { "access_token", auth.AccessToken },
                    { "shop_id", auth.ShopId },
                    { "sign", signature },
                    { "page_no", pageNo},
                    { "page_size", pageSize},
                    { "create_time_from", createFrom},
                    { "create_time_to", createTo},
                };

                logShopeeHelper.RequestObject = param;
                var url = $"{SelfAppConfig.ShopeeApiEndpointV2}/{ShopeeEndpointV2.GetWalletTransactionList}";
                var result = await RequestHelper.GetRequest<WalletTransactionListResponseV2>(url, param, null, false, isCallGrpcRetry);
                logShopeeHelper.ResponseObject = result.Data;

                if (string.IsNullOrEmpty(result.Data?.Error)) return result;
                logShopeeHelper.LogError(new ShopeeException(result.Data.Error + " " + result.Data.Message, new Exception
                {
                    Source = $"RequestId: {result.Data.RequestId} - error: {result.Data.Error} - msg: {result.Data.Message}"
                }));
                return result;
            }
            catch (Exception ex)
            {
                logShopeeHelper.EcommerceRequest = Ecommerce.Shopee;
                logShopeeHelper.LogError(ex);
                throw;
            }
        }

        public static async Task<IRestResponse<ShopeeLogisticsGetChannelListReponseV2>> GetLogisticsChannelList(ChannelAuth auth, Platform platform, bool isCallGrpc = false)
        {
            var timeStamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var signature = GenerateSignature(platform.DataObject.PartnerId,
                platform.DataObject.Token, timeStamp,
                auth.AccessToken, auth.ShopId, ShopeeEndpointV2.GetLogisticsChannelList);
            var language = SelfAppConfig.ShopeeLanguageRequestV2;
            var param = new Dictionary<string, object>
                {
                    { "partner_id", GetPartnerId(platform) },
                    { "timestamp", timeStamp},
                    { "access_token", auth.AccessToken },
                    { "shop_id", auth.ShopId },
                    { "sign", signature },
                    { "language", language },
                };
            var url = $"{SelfAppConfig.ShopeeApiEndpointV2}/{ShopeeEndpointV2.GetLogisticsChannelList}";
            var result = await RequestHelper.GetRequest<ShopeeLogisticsGetChannelListReponseV2>(url, param, null, false, isCallGrpc);

            return result;
        }

        public static async Task<IRestResponse<ShopeeAttributeResponseV2>> GetAttributes(ChannelAuth auth, Platform platform, long categoryId, bool isCallGrpc = false)
        {
            var timeStamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var signature = GenerateSignature(platform.DataObject.PartnerId,
                platform.DataObject.Token, timeStamp,
                auth.AccessToken, auth.ShopId, ShopeeEndpointV2.GetAttributes);
            var language = SelfAppConfig.ShopeeLanguageRequestV2;
            var param = new Dictionary<string, object>
            {
                { "partner_id", GetPartnerId(platform) },
                { "timestamp", timeStamp},
                { "access_token", auth.AccessToken },
                { "shop_id", auth.ShopId },
                { "sign", signature },
                { "language", language },
                { "category_id", categoryId },
            };
            var url = $"{SelfAppConfig.ShopeeApiEndpointV2}/{ShopeeEndpointV2.GetAttributes}";
            var result = await RequestHelper.GetRequest<ShopeeAttributeResponseV2>(url, param, null, false, isCallGrpc);

            return result;
        }

        public static async Task<IRestResponse<ShopeeBrandResponseV2>> GetBrands(ChannelAuth auth, Platform platform, long categoryId, int offset, bool isCallGrpc = false)
        {
            var timeStamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var signature = GenerateSignature(platform.DataObject.PartnerId,
                platform.DataObject.Token, timeStamp,
                auth.AccessToken, auth.ShopId, ShopeeEndpointV2.GetBrands);
            var language = SelfAppConfig.ShopeeLanguageRequestV2;
            var param = new Dictionary<string, object>
            {
                { "partner_id", GetPartnerId(platform) },
                { "timestamp", timeStamp},
                { "access_token", auth.AccessToken },
                { "shop_id", auth.ShopId },
                { "sign", signature },
                { "language", language },
                { "category_id", categoryId },
                { "page_size", 100 },
                { "status", 1 },
                { "offset", offset },
            };
            var url = $"{SelfAppConfig.ShopeeApiEndpointV2}/{ShopeeEndpointV2.GetBrands}";
            var result = await RequestHelper.GetRequest<ShopeeBrandResponseV2>(url, param, null, false, isCallGrpc);

            return result;
        }

        public static async Task<IRestResponse<ShopeeCategoryResponseV2>> GetCategories(ChannelAuth auth, Platform platform, long shopId, bool isCallGrpc = false)
        {
            var url = $"{SelfAppConfig.ShopeeApiEndpointV2}/{ShopeeEndpointV2.GetCategories}";
            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var signature = GenerateSignature(platform.DataObject.PartnerId,
                platform.DataObject.Token, timestamp, auth.AccessToken, auth.ShopId, ShopeeEndpointV2.GetCategories);
            var language = SelfAppConfig.ShopeeLanguageRequestV2;
            var param = new Dictionary<string, object>
            {
                { "partner_id", platform.DataObject.PartnerId },
                { "timestamp", timestamp },
                { "access_token", auth.AccessToken },
                { "shop_id", auth.ShopId },
                { "sign", signature },
                { "language", language },
            };

            var result = await RequestHelper.GetRequest<ShopeeCategoryResponseV2>(url, param, null, true, isCallGrpc);
            return result;
        }

        public static async Task<IRestResponse<ShopeeeUploadImageReponseV2>> UploadImg(string fieldName, byte[] data, string fileName,
            ChannelAuth auth,
            Platform platform,
            bool isCallGrpc = false)
        {
            var url = $"{SelfAppConfig.ShopeeApiEndpointV2}/{ShopeeEndpointV2.UploadImage}";
            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var signature = GenerateSignature(platform.DataObject.PartnerId,
                platform.DataObject.Token, timestamp, auth.AccessToken, auth.ShopId, ShopeeEndpointV2.UploadImage);
            var param = new Dictionary<string, object>
            {
                { "partner_id", platform.DataObject.PartnerId },
                { "timestamp", timestamp },
                { "access_token", auth.AccessToken },
                { "shop_id", auth.ShopId },
                { "sign", signature },
            };

            var result = await RequestHelper.PostImage<ShopeeeUploadImageReponseV2>(url, fieldName,
                data, fieldName, null, param);

            return result;
        }

        public static async Task<IRestResponse<ShopeeProductResponse>> PushProductToChannel(ShopeeProductRequestV2 req, ChannelAuth auth, Platform platform, bool isCallGrpc = false)
        {
            req.Weight = req.Weight / 1000; // đổi cân nặng sp từ kg -> gram
            var url = $"{SelfAppConfig.ShopeeApiEndpointV2}/{ShopeeEndpointV2.PushProductToChannel}";

            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var signature = GenerateSignature(platform.DataObject.PartnerId,
                platform.DataObject.Token, timestamp, auth.AccessToken, auth.ShopId, ShopeeEndpointV2.PushProductToChannel);
            var param = new Dictionary<string, object>
            {
                { "partner_id", platform.DataObject.PartnerId },
                { "timestamp", timestamp },
                { "access_token", auth.AccessToken },
                { "shop_id", auth.ShopId },
                { "sign", signature },
            };

            var result = await RequestHelper.PostRequest<ShopeeProductResponse, ShopeeProductRequestV2>(url, req, null, param: param, isCallGrpc: isCallGrpc);
            result.Data.ShopId = auth.ShopId.ToString();
            return result;
        }

        public static async Task SendToWebHookAsync(ShopeeWebhookModel req)
        {
            var url = $"{SelfAppConfig.WebHookKvOnlineApiEndpointV2}/{ShopeeEndpointV2.ShopeeWebhook}";
            await RequestHelper.PostRequest<ShopeeWebhookModel, object>(url, req, null, false, null);
        }

        public static async Task<IRestResponse<ShopeeDeleteProductByIdResponseV2>> DeleteProductById(long itemId, ChannelAuth auth, Platform platform, bool isCallGrpc = false)
        {
            var url = $"{SelfAppConfig.ShopeeApiEndpointV2}/{ShopeeEndpointV2.DeleteProductById}";

            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var signature = GenerateSignature(platform.DataObject.PartnerId,
                platform.DataObject.Token, timestamp, auth.AccessToken, auth.ShopId, ShopeeEndpointV2.DeleteProductById);
            var param = new Dictionary<string, object>
            {
                { "partner_id", platform.DataObject.PartnerId },
                { "timestamp", timestamp },
                { "access_token", auth.AccessToken },
                { "shop_id", auth.ShopId },
                { "sign", signature },
            };

            var req = new ShopeeDeleteProductByIdRequestV2()
            {
                ItemId = itemId,
            };

            var result = await RequestHelper.PostRequest<ShopeeDeleteProductByIdResponseV2, ShopeeDeleteProductByIdRequestV2>(url, req, null, param: param, isCallGrpc: isCallGrpc);
            return result;
        }

        public static async Task<IRestResponse<ShopeeAddModelResponseV2>> PushModelProductToChannel(ShopeeModelRequestV2 req, ChannelAuth auth, Platform platform, bool isCallGrpc = false)
        {
            var url = $"{SelfAppConfig.ShopeeApiEndpointV2}/{ShopeeEndpointV2.AddProductModel}";

            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var signature = GenerateSignature(platform.DataObject.PartnerId,
                platform.DataObject.Token, timestamp, auth.AccessToken, auth.ShopId, ShopeeEndpointV2.AddProductModel);
            var param = new Dictionary<string, object>
            {
                { "partner_id", platform.DataObject.PartnerId },
                { "timestamp", timestamp },
                { "access_token", auth.AccessToken },
                { "shop_id", auth.ShopId },
                { "sign", signature },
            };

            var result = await RequestHelper.PostRequest<ShopeeAddModelResponseV2, ShopeeModelRequestV2>(url, req, null, param: param, isCallGrpc: isCallGrpc);
            return result;
        }

        public static async Task<IRestResponse<ShopeeInitTierVariationResponseV2>> PushProductInitTierVariation(ShopeeInitTierRequestV2 req, ChannelAuth auth, Platform platform, bool isCallGrpc = false)
        {
            var url = $"{SelfAppConfig.ShopeeApiEndpointV2}/{ShopeeEndpointV2.InitTierVariation}";

            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var signature = GenerateSignature(platform.DataObject.PartnerId,
                platform.DataObject.Token, timestamp, auth.AccessToken, auth.ShopId, ShopeeEndpointV2.InitTierVariation);
            var param = new Dictionary<string, object>
            {
                { "partner_id", platform.DataObject.PartnerId },
                { "timestamp", timestamp },
                { "access_token", auth.AccessToken },
                { "shop_id", auth.ShopId },
                { "sign", signature },
            };

            var result = await RequestHelper.PostRequest<ShopeeInitTierVariationResponseV2, ShopeeInitTierRequestV2>(url, req, null, param: param, isCallGrpc: isCallGrpc);
            return result;
        }

        #region private method
        private static long GetPartnerId(Platform platform)
        {
            if (platform?.DataObject?.PartnerId == null)
                return SelfAppConfig.ShopeePartnerId;
            return platform.DataObject.PartnerId;
        }
        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.EndPoints;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ShareKernel.Exceptions;
using RestSharp;
using ServiceStack;
using ServiceStack.Logging;

namespace Demo.OmniChannel.ChannelClient.Helper
{
    public class TikiHelperV2
    {
        private static ILog Log => LogManager.GetLogger(typeof(TikiHelperV2));

        public static async Task<IRestResponse<SellerInfoV2Response>> GetSellerInfo(string accessToken)
        {
            var url = $"{SelfAppConfig.TikiApiEndpoint}/{TikiEndpoint.GetSellerInfo}";
            var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };
            var result = await RequestHelper.GetRequest<SellerInfoV2Response>(url, null, header);

            WriteLogRequest(accessToken, url, result.StatusCode);

            return result;
        }

        public static async Task<IRestResponse<SellerUpdateCanEditProductV2Response>> UpdateCanUpdateProduct(string accessToken, bool canUpdateProduct = true)
        {
            var url = $"{SelfAppConfig.TikiApiEndpoint}/{TikiEndpoint.UpdateCanUpdateProduct}";
            var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };
            var request = new SellerUpdateCanEditProductV2Request
            {
                CanUpdateProduct = (byte)(canUpdateProduct ? 1 : 0)
            };

            var result = await RequestHelper.PostRequest<SellerUpdateCanEditProductV2Response, SellerUpdateCanEditProductV2Request>(url, request, header);

            WriteLogRequest(accessToken, url, result.StatusCode);

            return result;
        }

        public static async Task<IRestResponse<TokenResponse>> CreateToken(string code)
        {
            var url = $"{SelfAppConfig.TikiOAuthURL}/{TikiEndpoint.CreateToken}";
            var param = new Dictionary<string, string>
            {
                {"code", code}, {"grant_type", "authorization_code"}, {"redirect_uri", SelfAppConfig.TikiCallBack},
                {"client_id", SelfAppConfig.TikiClientAppId}
            };
            var result = await RequestHelper.PostRequestWithFormUrlencoded<TokenResponse>(url, param, null,
                SelfAppConfig.TikiClientAppId, SelfAppConfig.TikiApiSercetKey);
            return result;
        }
        public static async Task<IRestResponse<TokenResponse>> RefreshToken(string refreshToken, string tikiCallBack, string tikiClientAppId, string tikiApiSercetKey)
        {
            var url = $"{SelfAppConfig.TikiOAuthURL}/{TikiEndpoint.CreateToken}";
            var param = new Dictionary<string, string>
            {
                { "grant_type", "refresh_token" },
                { "redirect_uri",  tikiCallBack},
                { "refresh_token", refreshToken },
                { "client_id", tikiClientAppId }
            };
            var result = await RequestHelper.PostRequestWithFormUrlencoded<TokenResponse>(url, param, null,
                tikiClientAppId, tikiApiSercetKey);
            return result;
        }
        public static async Task<IRestResponse<TikiProductsV2ListResponse>> GetProducts(string accessToken, int pageNo, int pageSize, DateTime? updatedFrom = null)
        {
            var url = $"{SelfAppConfig.TikiApiEndpoint}/{TikiEndpoint.GetProducts}";
            var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };
            var param = new Dictionary<string, object> { { "page", pageNo }, { "limit", pageSize }, { "include", TikiInclude.Inventory } };
            if (updatedFrom.HasValue)
            {
                param.Add("updated_from_date", updatedFrom.Value.ToString("yyyy-MM-dd HH':'mm':'ss"));
            }
            var result = await RequestHelper.GetRequest<TikiProductsV2ListResponse>(url, param, header);

            WriteLogRequest(accessToken, url, result.StatusCode);

            return result;
        }

        public static async Task<IRestResponse<TikiProductDetailV2>> GetProductDetail(string accessToken, long productId)
        {
            var url = $"{SelfAppConfig.TikiApiEndpoint}/{TikiEndpoint.GetProducts}/{productId}";
            var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };
            var result = await RequestHelper.GetRequest<TikiProductDetailV2>(url, null, header);

            WriteLogRequest(accessToken, url, result.StatusCode);

            return result;
        }

        public static async Task<IRestResponse<TikiOrderV2Response>> GetOrders(string accessToken, int pageNo, int pageSize, DateTime? createFrom, LogObjectMicrosoftExtension logInfo)
        {
            try
            {
                var url = $"{SelfAppConfig.TikiApiEndpointV2}/{TikiEndpoint.GetOrders}";
                var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };
                var param = new Dictionary<string, object> { { "page", pageNo }, { "limit", pageSize }, { "include", TikiInclude.StatusHistories } };
                if (createFrom.HasValue)
                {
                    param.Add("updated_from_date", createFrom.Value.ToString("yyyy-MM-dd HH':'mm':'ss"));
                }

                logInfo.RequestObject = $"url: {url} - header: Bearer {accessToken} - param: {param.ToJson()}";

                var result = await RequestHelper.GetRequestToEcommerce<TikiOrderV2Response>(logInfo, url, param, header);
                logInfo.ResponseObject = result?.Data;
                if (result?.Data?.Errors?.Any() == true)
                {
                    var msg = $"Status code: {result.StatusCode} - error: {result.Data.Errors.Join(",")}";
                    var ex = new TikiException(msg);
                    logInfo.LogError(ex);
                    return result;
                }

                if (result?.ErrorException != null)
                {
                    logInfo.LogError(result.ErrorException);
                    return result;
                }

                return result;
            }
            catch (Exception ex)
            {
                logInfo.LogError(ex);
                throw;
            }
        }

        public static async Task<IRestResponse<TikiOrderV2>> GetOrdersDetail(string accessToken, string orderCode, LogObjectMicrosoftExtension logInfo)
        {
            try
            {
                var url = $"{SelfAppConfig.TikiApiEndpointV2}/{TikiEndpoint.GetOrders}/{orderCode}";
                var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };

                logInfo.RequestObject = $"url: {url} - header: Bearer {accessToken}";

                var result = await RequestHelper.GetRequestToEcommerce<TikiOrderV2>(logInfo, url, null, header);
                logInfo.ResponseObject = result?.Data;

                if (result?.Data?.Errors?.Any() == true)
                {
                    var msg = $"statusCode: {result.StatusCode} - error: {result.Data.Errors.Join(",")}";
                    var ex = new TikiException(msg);
                    logInfo.LogError(ex);
                    return result;
                }

                if (result?.ErrorException != null)
                {
                    logInfo.LogError(result.ErrorException);
                    return result;
                }

                logInfo.ResponseObject = result?.Content;
                logInfo.LogInfo(true);

                return result;
            }
            catch (Exception ex)
            {
                logInfo.LogError(ex);
                throw;
            }
        }

        public static async Task<IRestResponse<TikiOrderV2>> GetOrdersDetailLoggerOld(string accessToken, string orderCode)
        {
            var url = $"{SelfAppConfig.TikiApiEndpointV2}/{TikiEndpoint.GetOrders}/{orderCode}";
            var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };
            var result = await RequestHelper.GetRequest<TikiOrderV2>(url, null, header);

            WriteLogRequest(accessToken, url, result.StatusCode);

            return result;
        }

        public static async Task<IRestResponse<UpdateProductTikiV2Response>> UpdateProduct(string accessToken, long productId, double? quantity, decimal? price, int? active = null)
        {
            var url = $"{SelfAppConfig.TikiApiEndpoint}/{TikiEndpoint.UpdateProduct}";
            var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };
            var request = new UpdateProductTikiV2Request
            {
                ProductId = productId,
                Active = active,
                Quantity = quantity,
                Price = price
            };

            var result = await RequestHelper.PostRequest<UpdateProductTikiV2Response, UpdateProductTikiV2Request>(url, request, header);

            WriteLogRequest(accessToken, url, result.StatusCode);

            return result;
        }

        public static async Task<IRestResponse<SellerWareHouseV2Response>> GetSellerWarehouse(string accessToken)
        {
            var url = $"{SelfAppConfig.TikiApiEndpointV2}/{TikiEndPointV2.GetSellerWarehouse}";
            var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };
            var param = new Dictionary<string, object> { { "status", 1 }, { "type", 1 }};

            var result = await RequestHelper.GetRequest<SellerWareHouseV2Response>(url, param, header);

            WriteLogRequest(accessToken, url, result.StatusCode);

            return result;
        }

        private static void WriteLogRequest(string accessToken, string endpoint, HttpStatusCode statusCode)
        {
            if (SelfAppConfig.IsWriteLogRequestTiki && accessToken != null && accessToken.Length >= 8)
            {
                var sellerKey = $"{accessToken.Substring(0, 8)}{accessToken.Substring(accessToken.Length - 8)}";
                var logObj = new LogObject(Log, Guid.NewGuid())
                {
                    Action = "RequestToTiki",
                    RequestObject = $"SellerKey: {sellerKey}",
                    ResponseObject = $"EndPoint: {endpoint} - StatusCode: {statusCode}"
                };

                logObj.LogInfo();
            }
        }
        public static async Task<IRestResponse<TikiDeleteProductsV2ListResponse>> GetDeletedProducts(string accessToken, int pageNo, int pageSize, DateTime? updatedFrom = null)
        {
            var url = $"{SelfAppConfig.TikiApiEndpointV2}/{TikiEndpoint.GetProducts}/{TikiEndpoint.GetHiddenSkus}";
            var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };
            var param = new Dictionary<string, object> { { "page", pageNo }, { "limit", pageSize } };
            if (updatedFrom.HasValue)
            {
                param.Add("updated_at_from", updatedFrom.Value.ToString("yyyy-MM-dd HH':'mm':'ss"));
            }
            var result = await RequestHelper.GetRequest<TikiDeleteProductsV2ListResponse>(url, param, header);

            WriteLogRequest(accessToken, url, result.StatusCode);

            return result;
        }
    }
}
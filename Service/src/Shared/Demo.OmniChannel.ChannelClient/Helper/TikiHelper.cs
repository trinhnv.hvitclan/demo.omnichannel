﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.EndPoints;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ShareKernel.Exceptions;
using RestSharp;
using ServiceStack;
using ServiceStack.Logging;

namespace Demo.OmniChannel.ChannelClient.Helper
{
    public class TikiHelper
    {
        private static ILog Log => LogManager.GetLogger(typeof(TikiHelper));

        public static async Task<IRestResponse<SellerInfoResponse>> GetSellerInfo(string accessToken)
        {
            var url = $"{SelfAppConfig.TikiApiEndpoint}/{TikiEndpoint.GetSellerInfo}";
            var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };
            var result = await RequestHelper.GetRequest<SellerInfoResponse>(url, null, header);

            WriteLogRequest(accessToken, url, result.StatusCode);

            return result;
        }

        public static async Task<IRestResponse<SellerUpdateCanEditProductResponse>> UpdateCanUpdateProduct(string accessToken, bool canUpdateProduct = true)
        {
            var url = $"{SelfAppConfig.TikiApiEndpoint}/{TikiEndpoint.UpdateCanUpdateProduct}";
            var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };
            var request = new SellerUpdateCanEditProductRequest
            {
                CanUpdateProduct = (byte)(canUpdateProduct ? 1 : 0)
            };

            var result = await RequestHelper.PostRequest<SellerUpdateCanEditProductResponse, SellerUpdateCanEditProductRequest>(url, request, header);

            WriteLogRequest(accessToken, url, result.StatusCode);

            return result;
        }

        public static async Task<IRestResponse<TokenResponse>> CreateToken(string code)
        {
            var url = $"{SelfAppConfig.TikiOAuthURL}/{TikiEndpoint.CreateToken}";
            var param = new Dictionary<string, string>
            {
                {"code", code}, {"grant_type", "authorization_code"}, {"redirect_uri", SelfAppConfig.TikiCallBack},
                {"client_id", SelfAppConfig.TikiClientAppId}
            };
            var result = await RequestHelper.PostRequestWithFormUrlencoded<TokenResponse>(url, param, null,
                SelfAppConfig.TikiClientAppId, SelfAppConfig.TikiApiSercetKey);
            return result;
        }
        public static async Task<IRestResponse<TokenResponse>> RefreshToken(string refreshToken, string tikiCallBack, string tikiClientAppId, string tikiApiSercetKey)
        {
            var url = $"{SelfAppConfig.TikiOAuthURL}/{TikiEndpoint.CreateToken}";
            var param = new Dictionary<string, string>
            {
                { "grant_type", "refresh_token" },
                { "redirect_uri",  tikiCallBack},
                { "refresh_token", refreshToken },
                { "client_id", tikiClientAppId }
            };
            var result = await RequestHelper.PostRequestWithFormUrlencoded<TokenResponse>(url, param, null,
                tikiClientAppId, tikiApiSercetKey);
            return result;
        }
        public static async Task<IRestResponse<TikiProductsListResponse>> GetProducts(string accessToken, int pageNo, int pageSize, DateTime? updatedFrom = null)
        {
            var url = $"{SelfAppConfig.TikiApiEndpoint}/{TikiEndpoint.GetProducts}";
            var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };
            var param = new Dictionary<string, object> { { "page", pageNo }, { "limit", pageSize }, { "include", "inventory" } };
            if (updatedFrom.HasValue)
            {
                param.Add("updated_from_date", updatedFrom.Value.ToString("yyyy-MM-dd HH':'mm':'ss"));
            }
            var result = await RequestHelper.GetRequest<TikiProductsListResponse>(url, param, header);

            WriteLogRequest(accessToken, url, result.StatusCode);

            return result;
        }

        public static async Task<IRestResponse<TikiProductDetail>> GetProductDetail(string accessToken, long productId)
        {
            var url = $"{SelfAppConfig.TikiApiEndpoint}/{TikiEndpoint.GetProducts}/{productId}";
            var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };
            var result = await RequestHelper.GetRequest<TikiProductDetail>(url, null, header);

            WriteLogRequest(accessToken, url, result.StatusCode);

            return result;
        }

        public static async Task<IRestResponse<TikiOrderResponse>> GetOrders(string accessToken, int pageNo, int pageSize, DateTime? createFrom, LogObjectMicrosoftExtension loggerExtension)
        {
            try
            {
                var url = $"{SelfAppConfig.TikiApiEndpoint}/{TikiEndpoint.GetOrders}";
                var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };
                var param = new Dictionary<string, object> { { "status", "all" }, { "page", pageNo }, { "limit", pageSize } };
                if (createFrom.HasValue)
                {
                    param.Add("updated_from_date", createFrom.Value.ToString("yyyy-MM-dd HH':'mm':'ss"));
                }
                loggerExtension.RequestObject = $"url: {url} - header: {header} - param: {param}";
                loggerExtension.LogInfo();
                var result = await RequestHelper.GetRequestToEcommerce<TikiOrderResponse>(loggerExtension, url, param, header);

                if (result.Data?.Errors?.Any() == true)
                {
                    var msg = $"statusCode: {result.StatusCode} - error: {result.Data.Errors.Join(",")}";
                    var ex = new TikiException(msg);
                    loggerExtension.LogError(ex);
                    return result;
                }

                if (result.ErrorException != null)
                {
                    loggerExtension.LogError(result.ErrorException);
                    return result;
                }

                WriteLogInfoRequest(accessToken, url, result.StatusCode, param, loggerExtension);

                return result;
            }
            catch (Exception ex)
            {
                loggerExtension.LogError(ex);
                throw;
            }
        }

        public static async Task<IRestResponse<TikiOrder>> GetOrdersDetail(string accessToken, string orderCode, LogObjectMicrosoftExtension loggerExtension)
        {
            try
            {
                var url = $"{SelfAppConfig.TikiApiEndpoint}/{TikiEndpoint.GetOrders}/{orderCode}";
                var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };

                loggerExtension.RequestObject = $"url: {url} - header: {accessToken}";
                loggerExtension.LogInfo();

                var result = await RequestHelper.GetRequestToEcommerce<TikiOrder>(loggerExtension, url, null, header);

                if (result.Data?.Errors?.Any() == true)
                {
                    var msg = $"statusCode: {result.StatusCode} - error: {result.Data.Errors.Join(",")}";
                    var ex = new TikiException(msg);
                    loggerExtension.LogError(ex);
                    return result;
                }

                if (result.ErrorException != null)
                {
                    loggerExtension.LogError(result.ErrorException);
                    return result;
                }

                loggerExtension.ResponseObject = result.Data;
                loggerExtension.LogInfo();

                return result;
            }
            catch (Exception ex)
            {
                loggerExtension.LogError(ex);
                throw;
            }
        }

        public static async Task<IRestResponse<TikiOrder>> GetOrdersDetailLoggerOld(string accessToken, string orderCode)
        {
            var url = $"{SelfAppConfig.TikiApiEndpoint}/{TikiEndpoint.GetOrders}/{orderCode}";
            var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };
            var result = await RequestHelper.GetRequest<TikiOrder>(url, null, header);

            WriteLogRequest(accessToken, url, result.StatusCode);

            return result;
        }

        public static async Task<IRestResponse<UpdateProductTikiResponse>> UpdateProductStock(string accessToken, long productId, double? quantity, decimal? price, LogObjectMicrosoftExtension loggerExtension, int? active = null)
        {
            var logUpdateProduct = loggerExtension.Clone("HelperSyncMultiOnHand");
            try
            {
                var url = $"{SelfAppConfig.TikiApiEndpoint}/{TikiEndpoint.UpdateProduct}";
                var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };
                var request = new UpdateProductTikiRequest
                {
                    ProductId = productId,
                    Active = active,
                    Quantity = quantity,
                    Price = price
                };

                logUpdateProduct.RequestObject = $"url: {url} - header: {header} - request: {request.ToJson()}";

                var result = await RequestHelper.PostRequestToEcommerce<UpdateProductTikiResponse, UpdateProductTikiRequest>(loggerExtension, url, request, header);

                if (result.Data?.Errors?.Any() == true)
                {
                    var msg = $"statusCode: {result.StatusCode} - error: {result.Data.Errors.Join(",")}";
                    var ex = new TikiException(msg);
                    logUpdateProduct.LogError(ex);
                    return result;
                }

                if (result.ErrorException != null)
                {
                    logUpdateProduct.LogError(result.ErrorException);
                    return result;
                }

                return result;
            }
            catch (Exception ex)
            {
                logUpdateProduct.LogError(ex);
                throw;
            }
        }

        public static async Task<IRestResponse<UpdateProductTikiResponse>> UpdateProduct(string accessToken, long productId, double? quantity, decimal? price, LogObjectMicrosoftExtension loggerExtension, int? active = null)
        {
            var logUpdateProduct = loggerExtension.Clone("HelperSyncMultiOnHand");
            try
            {
                var url = $"{SelfAppConfig.TikiApiEndpoint}/{TikiEndpoint.UpdateProduct}";
                var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };
                var request = new UpdateProductTikiRequest
                {
                    ProductId = productId,
                    Active = active,
                    Quantity = quantity,
                    Price = price
                };

                logUpdateProduct.RequestObject = $"url: {url} - header: {header} - param: {request.ToJson()}";

                var result = await RequestHelper.PostRequestToEcommerce<UpdateProductTikiResponse, UpdateProductTikiRequest>(logUpdateProduct, url, request, header);

                if (result.Data?.Errors?.Any() == true)
                {
                    var msg = $"statusCode: {result.StatusCode} - error: {result.Data.Errors.Join(",")}";
                    var ex = new TikiException(msg);
                    logUpdateProduct.LogError(ex);
                    return result;
                }

                if (result.ErrorException != null)
                {
                    logUpdateProduct.LogError(result.ErrorException);
                    return result;
                }
                return result;
            }
            catch (Exception ex)
            {
                logUpdateProduct.LogError(ex);
                throw;
            }
        }
        
        private static void WriteLogRequest(string accessToken, string endpoint, HttpStatusCode statusCode)
        {
            if (SelfAppConfig.IsWriteLogRequestTiki && accessToken != null && accessToken.Length >= 8)
            {
                var sellerKey = $"{accessToken.Substring(0, 8)}{accessToken.Substring(accessToken.Length - 8)}";
                var logObj = new LogObject(Log, Guid.NewGuid())
                {
                    Action = "RequestToTiki",
                    RequestObject = $"SellerKey: {sellerKey}",
                    ResponseObject = $"EndPoint: {endpoint} - StatusCode: {statusCode}"
                };

                logObj.LogInfo();
            }
        }

        private static void WriteLogRequestWithLoggerMicrosoft(string accessToken, string endpoint, HttpStatusCode statusCode, LogObjectMicrosoftExtension loggerExtension)
        {
            if (SelfAppConfig.IsWriteLogRequestTiki && accessToken != null && accessToken.Length >= 8)
            {
                var sellerKey = $"{accessToken.Substring(0, 8)}{accessToken.Substring(accessToken.Length - 8)}";
                loggerExtension.Action = "RequestToTiki";
                loggerExtension.RequestObject = $"SellerKey: {sellerKey}";
                loggerExtension.ResponseObject = $"EndPoint: {endpoint} - StatusCode: {statusCode}";

                loggerExtension.LogInfo();
            }
        }

        private static void WriteLogInfoRequest(string accessToken, string endpoint, HttpStatusCode statusCode, Dictionary<string, object> parameters, LogObjectMicrosoftExtension loggerExtension)
        {
            if (SelfAppConfig.IsWriteLogRequestTiki && accessToken != null && accessToken.Length >= 8)
            {
                var sellerKey = $"{accessToken.Substring(0, 8)}{accessToken.Substring(accessToken.Length - 8)}";

                var paramStringRequest = "";
                foreach (var parameter in parameters)
                {
                    paramStringRequest += $"Key/value:{parameter.Key}/{parameter.Value}. ";
                }

                loggerExtension.Action = "RequestToTikiV2";
                loggerExtension.RequestObject = $"SellerKey: {sellerKey}";
                loggerExtension.ResponseObject =
                    $"EndPoint: {endpoint} - StatusCode: {statusCode} - ParametersRequest: [{paramStringRequest}]";
                loggerExtension.LogInfo();
            }
        }
    }
}
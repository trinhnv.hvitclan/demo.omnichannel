﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.Lazada.Sdk;
using Demo.Lazada.Sdk.Models;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.EndPoints;
using Demo.OmniChannel.ChannelClient.Models;
using RestSharp;
using ServiceStack.Logging;

namespace Demo.OmniChannel.ChannelClient.Helper
{
    public static class LazadaHelper
    {
        private static ILog Log => LogManager.GetLogger(typeof(LazadaHelper));

        public static async Task<IRestResponse<LazopResponse>> GetOrderTrace(ChannelAuth auth, string orderId)
        {
            var param = new Dictionary<string, string> { { "order_id", orderId } };
            var client = new LazopClient(SelfAppConfig.LazadaHostApi,
                SelfAppConfig.LazadaClientAppId,
                SelfAppConfig.LazadaApiSercetKey);
            LazopRequest request = new LazopRequest();
            request.SetApiName(LazadaEndpoint.GetOrderTrace);
            request.SetHttpMethod("GET");
            foreach (var pa in param)
            {
                request.AddApiParameter(pa.Key, pa.Value);
            }
            return await client.ExecuteAsync(request, auth.AccessToken);
        }
    }
}
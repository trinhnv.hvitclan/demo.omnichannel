﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.EndPoints;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ShareKernel.Exceptions;
using RestSharp;
using ServiceStack;
using ServiceStack.Logging;

namespace Demo.OmniChannel.ChannelClient.Helper
{
    public class SendoHelper
    {
        private static ILog Log => LogManager.GetLogger(typeof(SendoHelper));

        public static async Task<IRestResponse<SendoRequestBase<SendoLoginResponse>>> Login(string shopKey, string secretKey)
        {
            var url = $"{SelfAppConfig.SendoApiEndpoint}/{SendoEndpoint.Login}";
            var body = new SendoLoginRequest {ShopKey = shopKey, SecretKey = secretKey};

            var result = await RequestHelper.PostRequest<SendoRequestBase<SendoLoginResponse>, SendoLoginRequest>(url, body, null);

            return result;
        }

        public static async Task<IRestResponse<SendoProductsListResponse>> GetProducts(string accessToken, string pageToken, int pageSize, DateTime? updatedFrom = null, bool isFirstSync = false)
        {
            var sendoApiEndpoint = SelfAppConfig.SendoApiEndpoint;
            if (!sendoApiEndpoint.Contains("api/partner")) sendoApiEndpoint += "/api/partner";
            var url = $"{sendoApiEndpoint}/{SendoEndpoint.GetProducts}";
            var header = new Dictionary<string, string> { { "authorization", $"bearer {accessToken}" } };
            var bodyReq = new SendoProductsListRequest
            {
                Token = pageToken,
                PageSize = pageSize,
                UpdatedFromDate = null,
                Statuses = new List<int> { SendoProductStatus.Approved, SendoProductStatus.Submitted }
            };
            if (updatedFrom.HasValue) bodyReq.UpdatedFromDate = updatedFrom;
            //Sau lần đồng bộ đầu sẽ lấy cả sản phẩm xóa, hủy, từ chối duyệt
            if (!isFirstSync)
            {
                bodyReq.Statuses.AddRange(new List<int> {SendoProductStatus.Deleted, SendoProductStatus.Rejected, SendoProductStatus.Cancelled});
            }
            var result = await RequestHelper.PostRequest<SendoProductsListResponse, SendoProductsListRequest>(url, bodyReq, header);
            return result;
        }

        public static async Task<IRestResponse<SendoRequestBase<SendoProductDetail>>> GetProductDetail(string accessToken, long productId)
        {
            var sendoApiEndpoint = SelfAppConfig.SendoApiEndpoint;
            if (!sendoApiEndpoint.Contains("api/partner"))
            {
                sendoApiEndpoint += "/api/partner";
            }
            var url = $"{sendoApiEndpoint}/{SendoEndpoint.GetProductDetail}";
            var header = new Dictionary<string, string> { { "authorization", $"bearer {accessToken}" } };
            var param = new Dictionary<string, object> { { "id", productId } };

            var result = await RequestHelper.GetRequest<SendoRequestBase<SendoProductDetail>>(url, param, header);
            return result;
        }

        public static async Task<IRestResponse<SendoUpdateProductResponse>> UpdatePriceOrStock(
            List<SendoUpdateProduct> body, string accessToken, long channelId, 
            bool isUpdatePrice, LogObjectMicrosoftExtension loggerExtension)
        {
            var log = loggerExtension.Clone(string.Format(EcommerceTypeException.SyncUpdatePriceStockHelper, Ecommerce.Sendo));
            try
            {
                var url = $"{SelfAppConfig.SendoApiEndpoint}/{SendoEndpoint.UpdatePriceAndStock}";
                var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };

                var result =
                    await RequestHelper.PostRequestToEcommerce<SendoUpdateProductResponse, List<SendoUpdateProduct>>(loggerExtension, url, body, header);
                log.RequestObject = $"Body: {body.ToSafeJson()} ,header: {header.ToSafeJson()}";
                log.ResponseObject = result?.Content;
                log.LogInfo();

                return await RetryQuantityAndStock(body, accessToken, channelId, isUpdatePrice, url, header, result, loggerExtension);
            }
            catch (Exception ex)
            {
                log.LogError(ex);
                throw;
            }
        }

        /// <summary>
        /// Retry Quantity and Price when Sendo response success but not update in ban.sendo.vn
        /// </summary>
        /// <param name="body">request body</param>
        /// <param name="accessToken">access token</param>
        /// <param name="channelId">omni channel id</param>
        /// <param name="isUpdatePrice">retry price or stock</param>
        /// <param name="url">enpoint update price or stock</param>
        /// <param name="header">request header</param>
        /// <param name="result"></param>
        /// <param name="loggerExtension"></param>
        /// <returns>response sendo update-price-stock request</returns>
        private static async Task<IRestResponse<SendoUpdateProductResponse>> RetryQuantityAndStock(
            List<SendoUpdateProduct> body, string accessToken, long channelId, bool isUpdatePrice,
            string url, Dictionary<string, string> header, IRestResponse<SendoUpdateProductResponse> result, LogObjectMicrosoftExtension loggerExtension)
        {
            if (result.Data != null && result.StatusCode == HttpStatusCode.OK &&
                SelfAppConfig.IsRetryRequestPriceAndStockSendo && result.Data.Success)
            {
                var totalRetries = 0;
                if (result.Data.Result?.Result != null && result.Data.Result.Result.Any())
                {
                    var groupItems = result.Data.Result.Result.Where(p => string.IsNullOrEmpty(p.ErrorMessage))
                        .GroupBy(x => x.ProductId).Select(p => p.FirstOrDefault()).ToList();

                    while (totalRetries < SelfAppConfig.MaxRetryRequestPriceAndStockSendo && groupItems.Any())
                    {
                        List<SendoUpdateProduct> retryItem = new List<SendoUpdateProduct>();
                        foreach (var item in groupItems)
                        {
                            var detail = await GetProductDetail(accessToken, item.ProductId);
                            if (detail.StatusCode != HttpStatusCode.OK || !detail.Data.Success)
                            {
                                continue;
                            }
                            retryItem.AddRange(GetItemRetry(detail, body, item, isUpdatePrice));
                        }

                        if (!retryItem.Any())
                        {
                            break;
                        }

                        loggerExtension.ResetTimer();
                        result =
                            await RequestHelper.PostRequestToEcommerce<SendoUpdateProductResponse, List<SendoUpdateProduct>>(loggerExtension, url, retryItem,
                                header);

                        loggerExtension.Action = "SendoRetriesStockAndPrice";
                        var requestUrl = $"url: {url} - header: Bearer {accessToken} - params: {retryItem.ToJson()}";
                        loggerExtension.RequestObject = requestUrl;
                        loggerExtension.ResponseObject = result.Data;
                        var msgError = result.Data?.Error?.ToSafeJson();
                        if (!string.IsNullOrEmpty(msgError))
                        {
                            var ex = new SendoException(msgError);
                            loggerExtension.LogError(ex);
                        }
                        else
                        {
                            loggerExtension.LogInfo();
                        }
                        await Task.Delay(SelfAppConfig.MaxRetryRequestPriceAndStockSendo);
                        totalRetries++;
                    }
                }
                else if (result.Data.Result?.Result == null && result.StatusCode == HttpStatusCode.OK)
                {
                    while (totalRetries < SelfAppConfig.MaxRetryRequestPriceAndStockSendo)
                    {
                        loggerExtension.ResetTimer();
                        result =
                            await RequestHelper.PostRequestToEcommerce<SendoUpdateProductResponse, List<SendoUpdateProduct>>(loggerExtension, url, body,
                                header);
                        if (result.StatusCode == HttpStatusCode.OK && result.Data.Result != null)
                        {
                            break;
                        }
                        loggerExtension.Action = "SendoRetriesStockAndPrice";
                        var requestUrl = $"url: {url} - header: Bearer {accessToken} - params: {body.ToJson()}";
                        loggerExtension.RequestObject = requestUrl;
                        loggerExtension.ResponseObject = result.Data;
                        
                        var msgError = result.Data?.Error?.ToSafeJson();
                        if (!string.IsNullOrEmpty(msgError))
                        {
                            var ex = new SendoException(msgError);
                            loggerExtension.LogError(ex);
                        }
                        else
                        {
                            loggerExtension.LogInfo();
                        }

                        await Task.Delay(SelfAppConfig.MaxRetryRequestPriceAndStockSendo);
                        totalRetries++;
                    }
                }
           
            }
            return result;
        }

        public static async Task<IRestResponse<SendoOrderListResponse>> GetOrders(string accessToken, string pageToken,
            int pageSize, long channelId, DateTime? orderStatusDateFrom, DateTime? orderStatusDateTo, LogObjectMicrosoftExtension logInfo)
        {
            try
            {
                var url = $"{SelfAppConfig.SendoApiEndpoint}/{SendoEndpoint.GetOrders}";
                var header = new Dictionary<string, string>() { { "Authorization", $"Bearer {accessToken}" } };
                var body = new SendoOrderListRequest()
                {
                    Token = pageToken,
                    PageSize = pageSize
                };

                if (orderStatusDateFrom.HasValue && orderStatusDateFrom.Value != default(DateTime) && orderStatusDateTo.HasValue && orderStatusDateTo.Value != default(DateTime))
                {
                    body.OrderStatusDateFrom = orderStatusDateFrom.Value.ToString("yyyy/MM/dd");
                    body.OrderStatusDateTo = orderStatusDateTo.Value.ToString("yyyy/MM/dd");
                }
                logInfo.RequestObject = $"Url: {url} - header: {header.ToJson()} - body: {body.ToJson()}";

                var result =
                    await RequestHelper.PostRequestToEcommerce<SendoOrderListResponse, SendoOrderListRequest>(logInfo, url, body, header);
                if (result.ErrorException != null)
                {
                    logInfo.LogError(result.ErrorException);
                    throw result.ErrorException;
                }

                var msgErr = result.Data?.Error?.ToSafeJson();
                if (!string.IsNullOrEmpty(msgErr))
                {
                    var ex = new SendoException(msgErr);
                    logInfo.LogError(ex);
                    return result;
                }

                return result;
            }
            catch (Exception ex)
            {
                logInfo.LogError(ex);
                throw;
            }
        }

        public static async Task<IRestResponse<SendoOrderDetailResponse>> GetOrdersDetailLoggerOld(string accessToken, string orderId, long channelId)
        {
            var url = $"{SelfAppConfig.SendoApiEndpoint}/{SendoEndpoint.GetOrderDetail}/{orderId}";
            var header = new Dictionary<string, string>() { { "Authorization", $"Bearer {accessToken}" } };
            var result =
                await RequestHelper.GetRequest<SendoOrderDetailResponse>(url, null, header, true);
            WriteLogRequest(channelId, url, string.Empty, result.StatusCode, result.Content);
            return result;
        }

        public static async Task<IRestResponse<SendoOrderDetailResponse>> GetOrdersDetail(string accessToken, string orderId, long channelId, LogObjectMicrosoftExtension logInfo)
        {
            var logDetail = logInfo.Clone("GetOrdersDetailSendoHelper");
            try
            {
                var url = $"{SelfAppConfig.SendoApiEndpoint}/{SendoEndpoint.GetOrderDetail}/{orderId}";
                var header = new Dictionary<string, string>() { { "Authorization", $"Bearer {accessToken}" } };
                logDetail.RequestObject = $"Url: {url} - header: {header.ToJson()}";

                var result =
                    await RequestHelper.GetRequestToEcommerce<SendoOrderDetailResponse>(logDetail, url, null, header, true);
                if (result.ErrorException != null)
                {
                    logDetail.LogError(result.ErrorException);
                    throw result.ErrorException;
                }

                var msgErr = result.Data?.Error?.ToSafeJson();
                if (!string.IsNullOrEmpty(msgErr))
                {
                    var ex = new SendoException(msgErr);
                    logDetail.LogError(ex);
                    return result;
                }
                return result;
            }
            catch (Exception ex)
            {
                logDetail.LogError(ex);
                throw;
            }
        }
        public static string JoinRefreshToken(string shopKey, string secretKey)
        {
            var result = $"{shopKey},{secretKey}";

            return result;
        }

        public static string[] SplitRefreshToken(string refreshTokenDecrypt)
        {
            var data = refreshTokenDecrypt?.Split(',');
            if (data?.Length != 2) throw new KvException("SendoInvalidRefreshToken Shop key and Secret key storage invalid");

            return data;
        }

        public static string GetShopKeyFromRefreshToken(string refreshTokenDecrypt)
        {
            var shopKey = SplitRefreshToken(refreshTokenDecrypt)[0];

            return shopKey;
        }

        public static string GetSecretKeyFromRefreshToken(string refreshTokenDecrypt)
        {
            var secretKey = SplitRefreshToken(refreshTokenDecrypt)[1];

            return secretKey;
        }

        public static async Task<IRestResponse<SendoLocationResponse<SendoProvinceResponse>>> GetProvinces(string accessToken)
        {
            var sendoApiEndpoint = SelfAppConfig.SendoApiEndpoint.Replace("/api/partner", "");
            var url = $"{sendoApiEndpoint}/{SendoEndpoint.GetProvince}";
            var header = new Dictionary<string, string> { { "authorization", $"bearer {accessToken}" } };

            var result = await RequestHelper.GetRequest<SendoLocationResponse<SendoProvinceResponse>>(url, null, header);
            return result;
        }

        public static async Task<IRestResponse<SendoLocationResponse<SendoDistrictResponse>>> GetDistricts(string accessToken, int regionId)
        {
            var sendoApiEndpoint = SelfAppConfig.SendoApiEndpoint.Replace("/api/partner", "");
            var url = $"{sendoApiEndpoint}/{SendoEndpoint.GetDistrict}";
            var header = new Dictionary<string, string> { { "authorization", $"bearer {accessToken}" } };
            var param = new Dictionary<string, object> { { "regionId", regionId } };
            var result = await RequestHelper.GetRequest<SendoLocationResponse<SendoDistrictResponse>>(url, param, header);
            return result;
        }

        public static async Task<IRestResponse<SendoLocationResponse<SendoWardResponse>>> GetWards(string accessToken, int districtId)
        {
            var sendoApiEndpoint = SelfAppConfig.SendoApiEndpoint.Replace("/api/partner", "");
            var url = $"{sendoApiEndpoint}/{SendoEndpoint.GetWard}";
            var header = new Dictionary<string, string> { { "authorization", $"bearer {accessToken}" } };
            var param = new Dictionary<string, object> { { "districtId", districtId } };
            var result = await RequestHelper.GetRequest<SendoLocationResponse<SendoWardResponse>>(url, param, header);
            return result;
        }

        #region Private method

        private static void WriteLogRequest(long channelId, string endpoint, string body, HttpStatusCode statusCode, string responseContent)
        {
            if (SelfAppConfig.IsWriteLogRequestSendo && (SelfAppConfig.LogRequestSendoByChannels?.Any() != true || SelfAppConfig.LogRequestSendoByChannels.Any(x => x == channelId)))
            {
                var logObj = new LogObject(Log, Guid.NewGuid())
                {
                    Action = "RequestToSendo",
                    OmniChannelId = channelId,
                    RequestObject = body,
                    ResponseObject = $"EndPoint: {endpoint} - StatusCode: {statusCode} - Content: {responseContent}"
                };

                logObj.LogInfo();
            }
        }

        private static void WriteLogRequestWithLoggerMicrosoft(string endpoint, string body, HttpStatusCode statusCode, string responseContent, LogObjectMicrosoftExtension loggerExtension)
        {
            loggerExtension.Action = "RequestToSendo";
            loggerExtension.ResponseObject = $"EndPoint: {endpoint} - StatusCode: {statusCode} - Content: {responseContent}";
            loggerExtension.RequestObject = body;
            loggerExtension.LogInfo();
        }

        private static List<SendoUpdateProduct> GetItemRetry(IRestResponse<SendoRequestBase<SendoProductDetail>> detail,
            List<SendoUpdateProduct> body, SendoUpdateProductItemDetailResponse item, bool isUpdatePrice)
        {
            var retryItem = new List<SendoUpdateProduct>();
            if (detail.Data.Result.IsConfigVariant)
            {
                var variants = new List<SendoUpdateProduct>();
                var kvItems = body.Where(x => x.Id == item.ProductId)
                  .SelectMany(x => x.Variants,
                      (p, c) => new Variant
                      {
                          VariantAttributeHash = c.VariantAttributeHash,
                          VariantQuantity = !isUpdatePrice ? c.VariantQuantity : null,
                          VariantPrice = isUpdatePrice ? c.VariantPrice : null,
                          ParentProductId = p.Id
                      }).ToList();
                foreach (var kvItem in kvItems)
                {
                    var sendoItems = detail.Data.Result.Variants.FirstOrDefault(x =>
                        x.VariantAttributeHash.Equals(kvItem.VariantAttributeHash));

                    if (sendoItems == null ||
                        isUpdatePrice && sendoItems.VariantPrice == kvItem.VariantPrice ||
                        !isUpdatePrice && sendoItems.VariantQuantity == kvItem.VariantQuantity)
                        continue;

                    variants.Add(new SendoUpdateProduct
                    {
                        Id = kvItem.ParentProductId,
                        Variants = new List<Variant>
                        {
                            new Variant
                            {
                                VariantAttributeHash = kvItem.VariantAttributeHash,
                                VariantPrice = isUpdatePrice && sendoItems.VariantPrice != kvItem.VariantPrice
                                    ? kvItem.VariantPrice
                                    : null,
                                VariantQuantity = !isUpdatePrice && sendoItems.VariantQuantity != kvItem.VariantQuantity
                                    ? kvItem.VariantQuantity
                                    : null,
                                FieldMask = isUpdatePrice && sendoItems.VariantPrice != kvItem.VariantPrice
                                    ? new List<string> {"price"}
                                    : new List<string> {"quantity"}
                            }
                        }
                    });
                }

                retryItem.AddRange(variants.GroupBy(x => x.Id).Select(x =>
                    new SendoUpdateProduct
                    {
                        Id = x.Key,
                        Variants = x.Where(v => v.Variants != null).SelectMany(y => y.Variants).ToList()
                    }).ToList());
            }
            else
            {
                var kvItem = body.FirstOrDefault(x =>
                    x.Id == detail.Data.Result.Id && x.Price != null &&
                    x.Price.Value != (decimal)detail.Data.Result.Price);

                if (kvItem == null) return retryItem;

                retryItem.Add(new SendoUpdateProduct
                {
                    Id = kvItem.Id,
                    Price = isUpdatePrice ? kvItem.Price : null,
                    StockQuantity = !isUpdatePrice ?  kvItem.StockQuantity : null,
                    FieldMask = isUpdatePrice ? new List<string> { "price" } : new List<string> { "quantity" }
                });
            }

            return retryItem;
        }

        #endregion


    }
}
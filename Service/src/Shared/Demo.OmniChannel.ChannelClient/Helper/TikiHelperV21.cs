﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.EndPoints;
using Demo.OmniChannel.ChannelClient.Models;
using RestSharp;
using ServiceStack;
using ServiceStack.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using static Demo.OmniChannel.ChannelClient.Models.TikiV21;

namespace Demo.OmniChannel.ChannelClient.Helper
{
    public static class TikiHelperV21
    {
        private static ILog Log => LogManager.GetLogger(typeof(TikiHelperV21));



        public static async Task<IRestResponse<SellerWareHouseV2Response>> GetSellerWarehouse(string accessToken)
        {
            var url = $"{SelfAppConfig.TikiApiEndpointV2}/{TikiEndPointV2.GetSellerWarehouse}";
            var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };
            var param = new Dictionary<string, object> { { "status", 1 }, { "type", 1 } };
            var result = await RequestHelper.GetRequest<SellerWareHouseV2Response>(url, param, header);
            WriteLogRequest(accessToken, url, result.StatusCode);
            return result;
        }


        public static async Task<IRestResponse<TikiProductsV21ListResponse>> GetProducts(string accessToken, int pageNo, int pageSize, DateTime? updatedFrom = null)
        {
            var url = $"{SelfAppConfig.TikiApiEndpointV21}/{TikiEndpoint.GetProducts}";
            var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };
            var param = new Dictionary<string, object> { { "page", pageNo }, { "limit", pageSize }, { "include", TikiInclude.Inventory } };
            if (updatedFrom.HasValue)
            {
                param.Add("updated_from_date", updatedFrom.Value.ToString("yyyy-MM-dd HH':'mm':'ss"));
            }
            var result = await RequestHelper.GetRequest<TikiProductsV21ListResponse>(url, param, header);

            WriteLogRequest(accessToken, url, result.StatusCode);

            return result;
        }

        public static async Task<IRestResponse<TikiProductDetailV21>> GetProductDetail(string accessToken, long productId)
        {
            var url = $"{SelfAppConfig.TikiApiEndpointV21}/{TikiEndpoint.GetProducts}/{productId}";
            var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };
            var param = new Dictionary<string, object> { { "include", $"{TikiInclude.Attributes},{TikiInclude.Images}" } };
            var result = await RequestHelper.GetRequest<TikiProductDetailV21>(url, param, header);
            WriteLogRequest(accessToken, url, result.StatusCode);
            return result;
        }

        public static async Task<IRestResponse<UpdateProductTikiV21Response>> UpdateProduct(string accessToken, long productId, long? quantity, decimal? price, int? active = null)
        {
            var url = $"{SelfAppConfig.TikiApiEndpointV21}/{TikiEndpoint.UpdateProduct}";
            var header = new Dictionary<string, string> { { "Authorization", $"Bearer {accessToken}" } };

            var request = new UpdateProductTikiV21Request
            {
                ProductId = productId,
                Active = active
            };
            if (quantity != null)
            {
                var reponseSellerWareHouse = await GetSellerWarehouse(accessToken);
                if (reponseSellerWareHouse.Data.Errors != null && reponseSellerWareHouse.Data.Errors.Any())
                {
                    var errorMessage = reponseSellerWareHouse.Data.Errors.Join("; ");
                    return new RestResponse<UpdateProductTikiV21Response>
                    {
                        StatusCode = reponseSellerWareHouse.StatusCode,
                        Data = new UpdateProductTikiV21Response
                            { Errors = new List<string> { errorMessage } }
                    };
                }

                var validateMultiWareHouse = ValidateMultiWareHouse(reponseSellerWareHouse.Data);
                if (validateMultiWareHouse.Error)
                {
                    return new RestResponse<UpdateProductTikiV21Response>
                    {
                        StatusCode = HttpStatusCode.OK,
                        Data = new UpdateProductTikiV21Response
                            {  Errors = new List<string> { validateMultiWareHouse.Message } }
                    };
                }
                    
                var wareHouseId = reponseSellerWareHouse.Data?.Data?.FirstOrDefault()?.WareHouseId ?? 0;
                WarehouseQuantitiesV21 warehouseQuantitiesV21 = new WarehouseQuantitiesV21
                {
                    WarehouseId = wareHouseId,
                    QtyAvailable = (long)quantity
                };
                request.WarehouseQuantities = new List<WarehouseQuantitiesV21> { warehouseQuantitiesV21 };
            }
            if (price != null)
            {
                request.Price = price;
            }
            var result = await RequestHelper.PutRequest<UpdateProductTikiV21Response, UpdateProductTikiV21Request>(url, request, header);
            WriteLogRequest(accessToken, url, result.StatusCode);
            return result;
        }

        private static (bool Error, string Message) ValidateMultiWareHouse(SellerWareHouseV2Response sellerWareHouseV2Response)
        {
            if (sellerWareHouseV2Response.Paging.Total == 0)
            {
                return (true, "Không tồn tại kho hàng trên TIKI, vui lòng thử lại.");
            }
            if (sellerWareHouseV2Response.Paging.Total > 1)
            {
                return (true, "Demo chưa hỗ trợ đồng bộ đa kho lên TIKI, vui lòng thử lại.");
            }
            if (sellerWareHouseV2Response.Paging.Total == 1)
            {
                return (false, string.Empty);
            }
            return (true, "Demo chưa hỗ trợ đồng bộ đa kho lên TIKI, vui lòng thử lại!");
        }

        private static void WriteLogRequest(string accessToken, string endpoint, HttpStatusCode statusCode)
        {
            if (SelfAppConfig.IsWriteLogRequestTiki && accessToken != null && accessToken.Length >= 8)
            {
                var sellerKey = $"{accessToken.Substring(0, 8)}{accessToken.Substring(accessToken.Length - 8)}";
                var logObj = new LogObject(Log, Guid.NewGuid())
                {
                    Action = "RequestToTiki",
                    RequestObject = $"SellerKey: {sellerKey}",
                    ResponseObject = $"EndPoint: {endpoint} - StatusCode: {statusCode}"
                };

                logObj.LogInfo();
            }
        }
    }
}

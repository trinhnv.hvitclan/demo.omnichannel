﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Models;
using RestSharp;
using ServiceStack;

namespace Demo.OmniChannel.ChannelClient.Validators
{
    public static class ShopeeValidator
    {
        /// <summary>
        /// Validate chi tiết đơn hàng
        /// </summary>
        /// <param name="orderDetail">Chi tiết đơn shopee trả về</param>
        /// <param name="logInfo"></param>
        public static void WarningOrderDetail(OrderDetail orderDetail, LogObjectMicrosoftExtension logInfo)
        {
            //Kiểm tra kiểu dữ liệu, các thuộc tính bắt buộc
            var validationResults = new List<ValidationResult>();
            Validator.TryValidateObject(orderDetail, new ValidationContext(orderDetail), validationResults, true);
            if (validationResults.Any())
            {
                logInfo.WarningType = (byte)WarningType.PropertiesError;
                logInfo.LogWarning(validationResults.Select(x=>x.ErrorMessage).ToSafeJson());
                ResetLog(logInfo);
            }

            //Kiểm tra trạng thái đơn hàng có hỗ trợ
            var isValidOrderStatus = OrderStatusInvalid(orderDetail.OrderStatus);
            if (!isValidOrderStatus)
            {
                logInfo.WarningType = (byte)WarningType.NoSupport;
                logInfo.LogWarning(string.Format("Trạng thái đơn hàng không hỗ trợ: {0}", orderDetail.OrderStatus));
                ResetLog(logInfo);
            }
        }

        /// <summary>
        /// Validate vận chuyển
        /// </summary>
        /// <param name="logistics">vận chuyển </param>
        /// <param name="logInfo"></param>
        public static void WarningLogistics(IRestResponse<LogisticsMessageResponse> logistics, LogObjectMicrosoftExtension logInfo)
        {
            //Kiểm tra kiểu dữ liệu, các thuộc tính bắt buộc
            var validationResults = new List<ValidationResult>();
            Validator.TryValidateObject(logistics, new ValidationContext(logistics), validationResults, true);
            if (validationResults.Any())
            {
                logInfo.WarningType = (byte)WarningType.PropertiesError;
                logInfo.LogWarning(validationResults.Select(x => x.ErrorMessage).ToSafeJson());
                ResetLog(logInfo);
            }

            //Kiểm tra trạng thái đơn hàng có hỗ trợ
            var isValidOrderStatus = LogisticsStatusInvalid(logistics?.Data?.LogisticsStatus);
            if (!string.IsNullOrEmpty(logistics?.Data?.LogisticsStatus) && !isValidOrderStatus)
            {
                logInfo.WarningType = (byte)WarningType.NoSupport;
                logInfo.LogWarning(string.Format("Trạng thái logistics không hỗ trợ: {0}", logistics?.Data?.LogisticsStatus));
                ResetLog(logInfo);
            }
        }

        #region Private method
        private static bool OrderStatusInvalid(string status)
        {
            var listOrderStatusSupport = typeof(ShopeeStatus).GetFields(BindingFlags.Static | BindingFlags.Public).Select(p => p.GetValue( null).ToString()).ToList();
            if (listOrderStatusSupport.Contains(status)) return true;
            return false;
        }
        private static bool LogisticsStatusInvalid(string status)
        {
            var listOrderStatusSupport = typeof(ShopeeDeliveryStatus).GetFields(BindingFlags.Static | BindingFlags.Public).Select(p => p.GetValue(null).ToString()).ToList();
            if (listOrderStatusSupport.Contains(status)) return true;
            return false;
        }
        private static void ResetLog(LogObjectMicrosoftExtension log)
        {
            log.WarningType = null;
            log.Description = string.Empty;
        }
        #endregion
    }

}

﻿using Demo.OmniChannel.ChannelClient.Impls.NewBase;
using Demo.OmniChannel.ChannelClient.Interfaces.NewBase;
using Microsoft.Extensions.DependencyInjection;

namespace Demo.OmniChannel.ChannelClient.Extensions
{
    public static class DIExtensions
    {
        public static void AddDIBusinessClient(this IServiceCollection services)
        {
            services.AddTransient<ITiktokBusinessClientV2, TiktokBusinessImplimentBehaviorV2>();
            services.AddTransient<IShopeeBusinessClient, ShopeeBusinessImplimentBehavior>();
        }
    }
}

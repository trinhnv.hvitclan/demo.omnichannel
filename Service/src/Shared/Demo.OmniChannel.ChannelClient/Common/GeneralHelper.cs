﻿using Demo.OmniChannel.ChannelClient.Models;
using System;
using System.Collections.Generic;
using ChannelType = Demo.OmniChannel.Sdk.Common.ChannelType;

namespace Demo.OmniChannel.ChannelClient.Common
{
    public static class GeneralHelper
    {
        public static string GenerateEditProductUrl(EditUrlProp editUrlProp)
        {
            try
            {
                if (editUrlProp.ChannelType == (byte)ChannelType.Tiki)
                {
                    string id = editUrlProp.ParentItemId;
                    if (editUrlProp.SuperId != null && editUrlProp.SuperId != 0)
                    {
                        id = editUrlProp.SuperId.ToString();
                    }

                    return string.Format(editUrlProp.UrlTemplate, id, editUrlProp?.ShopId);
                }
                return $"{editUrlProp.UrlTemplate}{editUrlProp.ParentItemId}";
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public static string GetKvMessage(string rawMessage)
        {
            if (string.IsNullOrEmpty(rawMessage)) return "";

            // In: "stock count is invalid; detail:sku id:1729430419376211992, less reserve stock limit,current reseve stock:215"
            // Out: "Sản phẩm sku id:1729430419376211992 đang tham gia CTKM. Vui lòng cập nhật số lượng tồn lớn hơn số lượng 215 tham gia CTKM."
            if (rawMessage.Contains("less reserve stock limit,current reseve stock:"))
            {
                rawMessage = rawMessage.Replace("stock count is invalid; detail:sku id:", "Sản phẩm sku id:").Replace(", less reserve stock limit,current reseve stock:", " đang tham gia CTKM. Vui lòng cập nhật số lượng tồn lớn hơn số lượng ");
                rawMessage += " tham gia CTKM.";
            }
            foreach (KeyValuePair<string, string> mapping in GeneralHelper.MappingMessageSyncErrors)
            {
                if (rawMessage.Contains(mapping.Key))
                {
                    return mapping.Value;
                }
            }
            return rawMessage;
        }

        private static readonly Dictionary<string, string> MappingMessageSyncErrors = new Dictionary<string, string>
        {
          {
            "This product is regisstered for a campaign anh it's not allowed to be edited during the campaign period",
            "Sản phẩm đang tham gia chương trình khuyến mãi. Vui lòng đồng bộ lại khi chương trình kết thúc."
          },
          {
            "Can not edit product of locking",
            "Sản phẩm này đã bị khóa. Vui lòng liên hệ Lazada để được hỗ trợ."
          },
          {
            "No data update",
            "Gian hàng sử dụng đa kho không hỗ trợ đồng bộ Demo, Vui lòng liên hệ Lazada để tắt đa kho."
          },
          {
            "Negative sellable stock over sale. Negative. Reserved stock 1 and allocate stock 0",
            "Tồn kho của sản phẩm đang nhỏ hơn tổng số lượng đã được đặt trên Lazada. Vui lòng cập nhật lại tồn kho và đồng bộ lại."
          },
          {
            "IC_F_DOMAIN_IMAGE_00_01_001",
            "Lỗi khi cập nhật ảnh sản phẩm lên Lazada. Vui lòng liên hệ Lazada để được hỗ trợ."
          },
          {
            "SKU_PROMOTION_PRICE_OUT_OF_RATE_RANGE",
            "Sản phẩm đang tham gia chương trình khuyến mãi. Vui lòng đồng bộ lại khi chương trình kết thúc."
          },
          {
            "negative sellable on inventory",
            "Tồn kho của sản phẩm đang nhỏ hơn tổng số lượng đã được đặt trên Lazada. Vui lòng cập nhật lại tồn kho và đồng bộ lại."
          },
          {
            "Invalid Price:",
            "Giá bán không được thấp hơn 1000, Vui lòng cập nhật lại giá bán."
          },
          {
            "IC_F_DOMAIN_PACKAGE_00_01_001",
            "Khối lượng sản phẩm trên Lazada phải lớn hơn 0."
          },
          {
            "IC_F_IC_SERVICE_EDIT_002",
            "Sản phẩm đang được cập nhật. Vui lòng đồng bộ lại sau."
          },
          {
            "Shop is under vacation mode",
            "Gian hàng Shopee của bạn đang trong chế độ tạm nghỉ. Vui lòng tắt thiết lập này trên Shopee tại mục Thiết Lập Shop > Thiết Lập Cơ Bản > Chế độ Tạm nghỉ và thực hiện đồng bộ lại."
          },
          {
            "This item is not allowed to edit",
            "Hàng hóa đã xóa hoặc không có quyền cập nhật, Vui lòng lên shopee để cập nhật lại."
          },
          {
            "spex error : Item in promotion cannot change stock",
            "Sản phẩm đang tham gia chương trình khuyến mãi. Vui lòng đồng bộ lại khi chương trình kết thúc."
          },
          {
            "ERROR_ADD_ON_DEAL_PRICE_LOCK",
            "Hàng hóa đang tham gia CTKM, Vui lòng đồng bộ lại sau khi CTKM kết thúc."
          },
          {
            "spex error : shop is in holiday mode,user cannot change models.",
            " Gian hàng Shopee của bạn đang trong chế độ tạm nghỉ. Vui lòng tắt thiết lập này trên Shopee tại mục Thiết Lập Shop > Thiết Lập Cơ Bản > Chế độ Tạm nghỉ và thực hiện đồng bộ lại."
          },
          {
            "In ongoing/upcoming promotion, model price cannot be editted",
            "Hàng hóa đang tham gia CTKM, Vui lòng đồng bộ lại sau khi CTKM kết thúc."
          },
          {
            "Price exceeds min limitation 1000.0",
            "Giá bán không được thấp hơn 1000 đồng."
          },
          {
            "price differences are too large. Limitations detail: ID - 10 times; SG/MY - 7 times; TW/PH/VN - 5 times",
            "Giá của hàng phân loại trên Shopee chênh lệch nhau quá 5 lần. Vui lòng cập nhật giá bán của sản phẩm trên Shopee"
          },
          {
            "When item in wholesale, all of models should use the same price",
            "Sản phẩm thiết lập mua nhiều giảm giá thì giá đồng bộ lên không được khác giá hàng phân loại."
          },
          {
            "Interal error, please contact openapi team",
            "Có lỗi trong quá trình đồng bộ với sàn Shopee. Vui lòng đồng bộ lại sau."
          },
          {
            "Seller price must be less than listing price of master product",
            "Giá bán phải nhỏ hơn giá niêm yết của sản phẩm, Vui lòng cập nhật lại giá bán"
          },
          {
            "Price is invalid",
            "Giá bán phải nhỏ hơn giá niêm yết của sản phẩm và không nhỏ hơn quá 10% giá niêm yết."
          },
          {
            "Price is not valid because less than ten percent listing price",
            "Giá hàng hóa không được nhỏ hơn 10% giá niêm yết,Vui lòng cập nhật lại giá"
          },
          {
            "can/t save to db, please try again",
            "Có lỗi trong quá trình đồng bộ với sàn Tiki. Vui lòng đồng bộ lại sau."
          },
          {
            "No data to update",
            "Demo chưa hỗ trợ các gian hàng sử dụng chức năng bán đa kho trên Lazada. Vui lòng tắt tính năng này trên Lazada và thực hiện đồng bộ lại."
          },
          {
            "ITEM_NOT_FOUND",
            "Không tìm thấy seller sku trên Lazada, Vui lòng thực hiện đồng bộ lại"
          },
          {
            "IC_F_IC_DOMAIN_PRICE_007",
            "Giá khuyến mại không được phép lớn hơn giá bán hoặc nhỏ hơn 50% giá bán. Vui lòng cập nhật lại giá khuyến mại của sản phẩm."
          },
          {
            "ERROR_MODEL_ORIGINAL_PRICE_EDITING_LOCK",
            "Sản phẩm đang tham gia chương trình khuyến mãi. Vui lòng đồng bộ lại khi chương trình kết thúc."
          },
          {
            "this item is not allowed to edit",
            "Sản phẩm đã bị xóa hoặc bạn không có quyền cập nhật trên Shopee."
          },
          {
            "spex error : shop is in holiday mode,user cannot change models",
            "Shop nghỉ bán"
          },
          {
            "Shop is under vocation mode",
            "Shop nghỉ bán trên sàn shopee"
          },
          {
            "The commodity industry does not allow the application of promotional prices",
            "Ngành hàng không cho phép áp dụng giá khuyến mại. Vui lòng thay đổi ngành hàng hoặc cập nhật lại giá khuyến mại bằng giá bán để đồng bộ."
          },
          {
            "The price is out of range, should between (0.50, 1.00)",
            "Giá khuyến mại không được phép lớn hơn giá bán hoặc nhỏ hơn 50% giá bán. Vui lòng cập nhật lại giá khuyến mại của sản phẩm."
          },
          {
            "The price is out of range, should between (1.00, 1.00)",
            "Ngành hàng không cho phép áp dụng giá khuyến mại. Vui lòng thay đổi ngành hàng hoặc cập nhật lại giá khuyến mại bằng giá bán để đồng bộ."
          },
          {
            "Item_id is not found",
            "Không tìm thấy sản phẩm trên sàn."
          },
          {
            "Price cannot be changed when model is under promotion",
            "Không thể cập nhật giá bán do sản phẩm đang trong CTKM. Vui lòng đồng bộ lại khi CTKM trên sàn kết thúc."
          },
          {
            "Original_price is less than min price limit",
            "Giá sản phẩm nhỏ hơn giá trị tối thiểu sàn quy định. Vui lòng kiểm tra lại giá bán và đồng bộ lại."
          },
          {
            "Original_price cannot be edited when item is under promotion",
            "Không thể cập nhật giá bán do sản phẩm đang trong CTKM. Vui lòng đồng bộ lại khi CTKM trên sàn kết thúc."
          },
          {
            "spex error : Editing of product info is locked:info_type=,lock_type=PromotionLock",
            "Không thể cập nhật do sản phẩm đang trong CTKM. Vui lòng đồng bộ lại khi CTKM trên sàn kết thúc."
          },
          {
            "commit update failed,table name:auction_auctions,primary key:406602462",
            "Có lỗi trong quá trình đồng bộ với sàn Lazada. Vui lòng đồng bộ lại sau."
          },
          {
            "commit update failed,table name:auction_auctions,primary key:1438422982",
            "Tồn kho của sản phẩm đang nhỏ hơn tổng số lượng đã được đặt trên Lazada. Vui lòng cập nhật lại tồn kho và đồng bộ lại."
          },
          {
            "SKU_PROMOTION_PRICE_OUT_OF_RATE_RANGE; negative sellable on inventory,entityId=773920081,id=5,329,729,666",
            "Không thể cập nhật giá bán do sản phẩm đang trong CTKM. Vui lòng đồng bộ lại khi CTKM trên sàn kết thúc."
          },
          {
            "unknown error",
            "Không thể cập nhật giá bán do sản phẩm đang trong CTKM. Vui lòng đồng bộ lại khi CTKM trên sàn kết thúc."
          },
          {
            "Product edit was blocked by tag,context:",
            "Sản phẩm đang tham gia chương trình khuyến mãi. Vui lòng đồng bộ lại khi chương trình kết thúc."
          },
          {
            "ITEM_NOT_FOUND;",
            "Không tìm thấy seller sku trên Lazada, Vui lòng thực hiện đồng bộ lại"
          },
          {
            "negative sellable on channel inventory,entityId=5020227315,id=45,846,446",
            "Tồn kho của sản phẩm đang nhỏ hơn tổng số lượng đã được đặt trên Lazada. Vui lòng cập nhật lại tồn kho và đồng bộ lại."
          },
          {
            "promotion price must less than origin price,serverIP:33.1.112.169(IC_F_IC_DOMAIN_PRICE_007)",
            "Giá khuyến mại không được phép lớn hơn giá bán hoặc nhỏ hơn 50% giá bán. Vui lòng cập nhật lại giá khuyến mại của sản phẩm."
          },
          {
            "Unkown error key ERROR_MODEL_ORIGINAL_PRICE_EDITING_LOCK",
            "Sản phẩm đang tham gia chương trình khuyến mãi. Vui lòng đồng bộ lại khi chương trình kết thúc."
          },
          {
            "Lỗi kết nối với Tiki (InternalServerError)",
            "Có lỗi trong quá trình đồng bộ với sàn Tiki. Vui lòng đồng bộ lại sau."
          },
          {
            "Giá bán: {\"error\":{\"message\":\"Gi\u00e1 b\u00e1n ph\u1ea3i nh\u1ecf h\u01a1n gi\u00e1 ni\u00eam y\u1ebft\",\"status_code\":400}}",
            "Giá bán phải nhỏ hơn giá niêm yết của sản phẩm và không nhỏ hơn quá 10% giá niêm yết."
          },
          {
            "Concurrent product edit is not allowed,please try again,productId:1156620581,serverIP:33.1.39.180(IC_F_IC_SERVICE_EDIT_002",
            "Sản phẩm đang được cập nhật. Vui lòng đồng bộ lại sau."
          },
          {
            "invalid productPackage, package [weight] value must greater than 0,serverIP:11.13.248.124(IC_F_DOMAIN_PACKAGE_00_01_001)",
            "Khối lượng sản phẩm trên Lazada phải lớn hơn 0."
          },
          {
            "invalid general image count limit failed,serverIP:33.1.38.108(IC_F_DOMAIN_IMAGE_00_01_001)",
            "Số lượng ảnh sản phẩm nhiều hơn giới hạn cho phép. Vui lòng liên hệ Lazada để được trợ giúp."
          },
          {
            "Invalid general image count limit failed,serverIP:11.24.35.178(IC_F_DOMAIN_IMAGE_00_01_001)",
            "Lỗi khi cập nhật ảnh sản phẩm lên Lazada. Vui lòng liên hệ Lazada để được hỗ trợ."
          },
          {
            "The system encounters exception, please try again later",
            "Có lỗi trên hệ thống Lazada. Vui lòng thử đồng bộ lại sau."
          },
          {
            "Invalid Price: 0.0000",
            "Giá bán không được thấp hơn 1000, Vui lòng cập nhật lại giá bán."
          },
          {
            "stock should less then :999999",
            "Tồn kho phải nhỏ hơn 999,999"
          },
          {
            "Total stock must be more than reserved stock",
            "Số lượng tồn kho phải lớn hơn tồn kho dự trữ để tham gia CTKM."
          },
          {
            "Need to set location_id for your stock. Please double check",
            "Demo chưa hỗ trợ các gian hàng sử dụng chức năng bán đa kho trên Shopee. Vui lòng tắt tính năng này trên Shopee và thực hiện đồng bộ lại."
          },
          {
            "The price of the most expensive sku divided by the price of the cheapest sku limit:5",
            "Giá các hàng phân loại chênh lệch nhau quá 5 lần. Vui lòng kiểm tra và cập nhật lại giá bán phù hợp cho các phân loại hàng hóa."
          },
          {
            "Original_price should bigger then wholesale price",
            "Giá bán của sản phẩm nên lớn hơn giá sỉ"
          },
          {
            "cannot save to db, please try again",
            "Có lỗi trong quá trình đồng bộ với sàn Tiki. Vui lòng đồng bộ lại sau."
          },
          {
            "stock count is invalid",
            "Tồn kho phải nhỏ hơn 999,999."
          },
          {
            "Need to set location_id for your stock. Please double check.",
            "Demo chưa hỗ trợ các gian hàng sử dụng chức năng bán đa kho trên Shopee. Vui lòng tắt tính năng này trên Shopee và thực hiện đồng bộ lại."
          },
          {
            "The price of the most expensive sku divided by the price of the cheapest sku limit:5.",
            "Giá các hàng phân loại chênh lệch nhau quá 5 lần. Vui lòng kiểm tra và cập nhật lại giá bán phù hợp cho các phân loại hàng hóa."
          },
          {
            "Invalid Price: 0.0000.",
            "Giá bán không được thấp hơn 1000, Vui lòng cập nhật lại giá bán."
          },
          {
            "stock count is invalid; detail:sku id:1729430419376211992, less reserve stock limit,current reseve stock:215",
            "Sản phẩm sku id:1729430419376211992 đang tham gia CTKM. Vui lòng cập nhật số lượng tồn lớn hơn số lượng 215 tham gia CTKM."
          },
          {
            "timeout of 30000ms exceeded",
            "Có lỗi trong quá trình đồng bộ với sàn Lazada. Vui lòng đồng bộ lại sau."
          },
          {
            "UNEXPECT_EXCEPTION",
            "Có lỗi trong quá trình đồng bộ với sàn Lazada. Vui lòng đồng bộ lại sau."
          },
          {
            "The request has failed due to RPC timeout",
            "Có lỗi trong quá trình đồng bộ với sàn Lazada. Vui lòng đồng bộ lại sau."
          },
          {
            "promotion_match",
            "Không thể cập nhật giá bán do sản phẩm đang trong CTKM. Vui lòng đồng bộ lại khi CTKM trên sàn kết thúc."
          },
          {
            "server internal error validation: [Rule Type: price.range",
            "Giá bán không hợp lệ. Giá phải lớn hơn đ1,000 và nhỏ hơn đ120,000,000."
          },
          {
            "server internal error validation: [Rule Type: price.gap.limit",
            "Giá các hàng phân loại chênh lệch nhau quá 5 lần. Vui lòng kiểm tra và cập nhật lại giá bán phù hợp cho các phân loại hàng hóa."
          },
          {
            "Can't edit this item. item status can not support editing.; server internal error external error: Item is uneditable, source: item",
            "Trạng thái hàng hóa hiện tại không cho phép chỉnh sửa."
          },
          {
            "price edit forbid",
            "Không thể cập nhật giá bán do sản phẩm đang trong CTKM. Vui lòng đồng bộ lại khi CTKM trên sàn kết thúc."
          },
          {
            "Wholesale price can't more than original price",
            "Giá hàng hóa không được nhỏ hơn hoặc bằng giá sỉ, Vui lòng cập nhật lại giá."
          },
          {
            "Wholesale price is less than ratio limit",
            "Giá hàng hóa không được lớn hơn 10% giá sỉ, Vui lòng cập nhật lại giá."
          },
          {
            "negative sellable on channel inventory",
            "Tồn kho của sản phẩm đang nhỏ hơn tổng số lượng đã được khách hàng đặt hoặc đang tham gia CTKM trên Lazada. Vui lòng cập nhật lại tồn kho và đồng bộ lại."
          },
          {
            "CHANNEL_INV_NEGATIVE_SELLABLE",
            "Tồn kho của sản phẩm đang nhỏ hơn tổng số lượng đã được khách hàng đặt hoặc đang tham gia CTKM trên Lazada. Vui lòng cập nhật lại tồn kho và đồng bộ lại."
          },
          {
            "E006: Unexpected internal error",
            "Do warehouse address chưa có data, setting trên seller center"
          },
          {
            "E0901: Limit service request speed in server side temporarily",
            "Tạm thời giới hạn tốc độ yêu cầu dịch vụ ở phía máy chủ."
          },
          {
            "Failed to update product : total stock must more than reserved stock",
            "Số lượng tồn kho phải lớn hơn tồn kho dự trữ để tham gia CTKM."
          },
          {
            "All model price should be the same when the item has been set wholesales",
            "Giá của các phân loại hàng phải bằng nhau khi sản phẩm thiết lập bán giá sỉ"
          },
          {
            "fulfillment channel is not allowed",
            "Thông tin vận chuyển của hàng hóa không hợp lệ. Liên hệ tổng đài để được hỗ trợ."
          },
          {
            "product price exceed limit",
            "Gía sản phẩm phải lớn hơn 0 nhỏ hơn 180,000,000"
          },
          {
            "internal error",
            "Không thể cập nhật giá bán cho sản phẩm tặng kèm."
          },
        };
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Forwarder;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;
using ServiceStack;
using ServiceStack.Logging;

namespace Demo.OmniChannel.ChannelClient.Common
{
    public class RequestHelper
    {

        private static ILog _log = LogManager.GetLogger(typeof(RequestHelper));

        public static Dictionary<string, object> CreateParam(JObject data)
        {
            var param = new Dictionary<string, object>();
            foreach (var item in data)
            {
                if (item.Value.Type != JTokenType.Null)
                {
                    param.Add(item.Key, item.Value.Type == JTokenType.Float ? item.Value.ToString().Replace(",", ".") : item.Value);
                }
            }
            return param;
        }

        public static async Task<IRestResponse<T>> PostRequestWithFormUrlencoded<T>(string endpoint,
            Dictionary<string, string> param, Dictionary<string, string> header = null, string clientId = null, string clientSecret = null) where T : class
        {
            var builder = new UriBuilder(endpoint);
            var client = new RestClient(builder.ToString());
            if (!string.IsNullOrEmpty(clientId) && !string.IsNullOrEmpty(clientSecret))
            {
                client.Authenticator = new HttpBasicAuthenticator(clientId, clientSecret);
            }
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            if (header != null)
            {
                foreach (var entry in header)
                {
                    request.AddHeader(entry.Key, entry.Value);
                }
            }
            if (param != null)
            {
                foreach (var p in param)
                {
                    request.AddParameter(p.Key, p.Value);
                }
            }
            IRestResponse<T> response = null;
            try
            {
                response = await client.ExecuteTaskAsync<T>(request);
                WriteLogRequestError(client, request, response);
                if (response != null && response.Data == null && !string.IsNullOrEmpty(response.Content) && response.Content.IsValidJson<T>())
                {
                    response.Data = JsonConvert.DeserializeObject<T>(response.Content);
                }
            }
            catch (Exception ex)
            {
                WriteLogException(endpoint, ex);
                throw;
            }

            return response;
        }

        public static async Task<IRestResponse<T>> PostRequest<T, T1>(string endpoint, T1 body,
                Dictionary<string, string> header = null, bool isRetryGateWayTimeOut = false,
            Dictionary<string, object> param = null,
            bool isCallGrpc = false) where T : class where T1 : class

        {
            var builder = new UriBuilder(endpoint);

            var client = new RestClient(builder.ToString());
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-type", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(body), ParameterType.RequestBody);
            client.AddHandler("application/json", new DynamicJsonDeserializer());

            if (header != null && header.Any())
            {
                foreach (var entry in header)
                {
                    request.AddHeader(entry.Key, entry.Value);
                }
            }
            if (param != null)
            {
                foreach (var entry in param)
                {
                    request.AddParameter(entry.Key, entry.Value, ParameterType.QueryString);
                }
            }
            IRestResponse<T> response;
            try
            {
                if (isRetryGateWayTimeOut)
                {
                    response = await ExecuteWithRetryGatewayTimeOut<T>(client, request, endpoint);
                }
                else if (isCallGrpc)
                {
                    var newParam = param?.ToDictionary(pair => pair.Key, pair => pair.Value.ToString());

                    response = await ForwarderApiCallWork<T>(Guid.NewGuid().ToString(), endpoint,
                              JsonConvert.SerializeObject(body) ?? string.Empty, Method.POST.ToString(), header.ToSafeJson() ?? string.Empty,
                                JsonConvert.SerializeObject(newParam) ?? string.Empty);
                }
                else
                {
                    response = await client.ExecuteTaskAsync<T>(request);
                }
                WriteLogRequestError(client, request, response);
                if (response != null && response.Data == null && !string.IsNullOrEmpty(response.Content) && response.Content.IsValidJson<T>())
                {
                    response.Data = JsonConvert.DeserializeObject<T>(response.Content);
                }
            }
            catch (Exception ex)
            {
                WriteLogException(endpoint, ex);
                throw;
            }

            return response;
        }

        public static async Task<IRestResponse<T>> PostImage<T>(string endpoint, string fieldName, byte[] data, string fileName,
            Dictionary<string, string> header = null, 
            Dictionary<string, object> param = null,
            bool isRetryGateWayTimeOut = false,
            bool isCallGrpc = false) where T : class

        {
            var builder = new UriBuilder(endpoint);

            var client = new RestClient(builder.ToString());
            var request = new RestRequest(Method.POST);

            if (header != null && header.Any())
            {
                foreach (var entry in header)
                {
                    request.AddHeader(entry.Key, entry.Value);
                }
            }
            if (param != null)
            {
                foreach (var entry in param)
                {
                    request.AddParameter(entry.Key, entry.Value, ParameterType.QueryString);
                }
            }

            request.AddFile(fieldName, data, fileName);

            IRestResponse<T> response;
            try
            {
                if (isRetryGateWayTimeOut)
                {
                    response = await ExecuteWithRetryGatewayTimeOut<T>(client, request, endpoint);
                }
                else if (isCallGrpc)
                {
                    var newParam = param?.ToDictionary(pair => pair.Key, pair => pair.Value.ToString());

                    response = await ForwarderApiCallWork<T>(Guid.NewGuid().ToString(), endpoint,
                               string.Empty, Method.POST.ToString(), header.ToSafeJson() ?? string.Empty,
                                JsonConvert.SerializeObject(newParam) ?? string.Empty);
                }
                else
                {
                    response = await client.ExecuteTaskAsync<T>(request);
                }
                WriteLogRequestError(client, request, response);
                if (response != null && response.Data == null && !string.IsNullOrEmpty(response.Content) && response.Content.IsValidJson<T>())
                {
                    response.Data = JsonConvert.DeserializeObject<T>(response.Content);
                }
            }
            catch (Exception ex)
            {
                WriteLogException(endpoint, ex);
                throw;
            }

            return response;
        }

        public static async Task<IRestResponse<T>> PutRequest<T, T1>(string endpoint, T1 body,
            Dictionary<string, string> header = null, bool isRetryGateWayTimeOut = false,
            Dictionary<string, object> param = null) where T : class where T1 : class
        {
            var builder = new UriBuilder(endpoint);

            var client = new RestClient(builder.ToString());
            //ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            var request = new RestRequest(Method.PUT);
            request.AddHeader("Content-type", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(body), ParameterType.RequestBody);

            if (header != null)
            {
                foreach (var entry in header)
                {
                    request.AddHeader(entry.Key, entry.Value);
                }
            }
            if (param != null)
            {
                foreach (var entry in param)
                {
                    request.AddParameter(entry.Key, entry.Value, ParameterType.QueryString);
                }
            }
            IRestResponse<T> response = null;
            try
            {
                if (isRetryGateWayTimeOut)
                {
                    response = await ExecuteWithRetryGatewayTimeOut<T>(client, request, endpoint);
                }
                else
                {
                    response = await client.ExecuteTaskAsync<T>(request);
                }
                WriteLogRequestError(client, request, response);
                if (response != null && response.Data == null && !string.IsNullOrEmpty(response.Content) && response.Content.IsValidJson<T>())
                {
                    response.Data = JsonConvert.DeserializeObject<T>(response.Content);
                }
            }
            catch (Exception ex)
            {
                WriteLogException(endpoint, ex);
                throw;
            }

            return response;
        }

        static async Task<IRestResponse<T>> ExecuteWithRetryGatewayTimeOut<T>(RestClient client, RestRequest request, string endpoint, CancellationToken ct = default) where T : class
        {
            var retryCount = 0;
            var result = await client.ExecuteTaskAsync<T>(request, ct);

            while (result?.StatusCode == HttpStatusCode.GatewayTimeout &&
                   retryCount < SelfAppConfig.RetryGateWayTimeOut.Total)
            {
                _log.Warn(new KvShopeeGatewayTimeOutException($"Call request to Shopee: GatewayTimeout (has retry). Endpoint: {endpoint}"));
                retryCount++;
                await Task.Delay(SelfAppConfig.RetryGateWayTimeOut.Delay);
                result = await client.ExecuteTaskAsync<T>(request, ct);
            }

            return result;
        }

        public static async Task<IRestResponse<T>> GetRequestWithDuplicateKeyParams<T>(string endpoint, List<BaseParametersDuplicateKey<string, string>> param,
            Dictionary<string, string> header = null, bool useJsonConverter = false, bool isCallGrpc = false)
        {
            var builder = new UriBuilder(endpoint);
            var client = new RestClient(builder.ToString());
            client.AddHandler("application/json", new DynamicJsonDeserializer());
            var request = new RestRequest(Method.GET);

            if (header != null)
            {
                foreach (var entry in header)
                {
                    request.AddHeader(entry.Key, entry.Value);
                }
            }


            if (param != null)
            {
                foreach (var entry in param)
                {
                    request.AddParameter(entry.Key, entry.Value, ParameterType.GetOrPost);
                }
            }

            IRestResponse<T> response = null;
            try
            {
                if (isCallGrpc)
                {
                    response = await ForwarderApiCallWork<T>(Guid.NewGuid().ToString(), endpoint,
                              string.Empty, Method.GET.ToString(), header.ToSafeJson() ?? string.Empty,
                                JsonConvert.SerializeObject(param) ?? string.Empty, default, true);
                }
                else
                {
                    response = await client.ExecuteTaskAsync<T>(request);
                }

                WriteLogRequestError(client, request, response);
                if (response != null && response.Data == null && !string.IsNullOrEmpty(response.Content) && response.Content.IsValidJson<T>())
                {
                    response.Data = useJsonConverter
                     ? JsonConvert.DeserializeObject<T>(response.Content)
                     : response.Content.FromJson<T>();
                }

            }
            catch (Exception ex)
            {
                WriteLogException(endpoint, ex);
                throw;
            }

            return response;
        }

        public static async Task<IRestResponse<T>> GetRequest<T>(string endpoint, Dictionary<string, object> param,
            Dictionary<string, string> header = null, bool useJsonConverter = false, bool isCallGrpc = false)
        {
            var builder = new UriBuilder(endpoint);
            //ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            var client = new RestClient(builder.ToString());
            client.AddHandler("application/json", new DynamicJsonDeserializer());
            var request = new RestRequest(Method.GET);

            if (header != null)
            {
                foreach (var entry in header)
                {
                    request.AddHeader(entry.Key, entry.Value);
                }
            }

            if (param != null)
            {
                foreach (var entry in param)
                {
                    request.AddParameter(entry.Key, entry.Value, ParameterType.GetOrPost);
                }
            }

            IRestResponse<T> response = null;
            try
            {
                if (isCallGrpc)
                {
                    var newParam = param?.ToDictionary(pair => pair.Key, pair => pair.Value.ToString());

                    response = await ForwarderApiCallWork<T>(Guid.NewGuid().ToString(), endpoint,
                              string.Empty, Method.GET.ToString(), header.ToSafeJson() ?? string.Empty,
                                JsonConvert.SerializeObject(newParam) ?? string.Empty);
                }
                else
                {
                    response = await client.ExecuteTaskAsync<T>(request);
                }

                WriteLogRequestError(client, request, response);
                if (response != null && response.Data == null && !string.IsNullOrEmpty(response.Content) && response.Content.IsValidJson<T>())
                {
                    response.Data = useJsonConverter
                     ? JsonConvert.DeserializeObject<T>(response.Content)
                     : response.Content.FromJson<T>();
                }

            }
            catch (Exception ex)
            {
                WriteLogException(endpoint, ex);
                throw;
            }

            return response;
        }

        public static async Task<IRestResponse<T>> GetRequestToEcommerce<T>(LogObjectMicrosoftExtension logInfo, string endpoint, Dictionary<string, object> param,
            Dictionary<string, string> header = null, bool useJsonConverter = false)
        {
            try
            {
                var builder = new UriBuilder(endpoint);
                var client = new RestClient(builder.ToString());
                client.AddHandler("application/json", new DynamicJsonDeserializerResponse(logInfo));
                var request = new RestRequest(Method.GET);

                if (header != null)
                {
                    foreach (var entry in header)
                    {
                        request.AddHeader(entry.Key, entry.Value);
                    }
                }

                if (param != null)
                {
                    foreach (var entry in param)
                    {
                        request.AddParameter(entry.Key, entry.Value, ParameterType.GetOrPost);
                    }
                }

                IRestResponse<T> response = null;

                response = await client.ExecuteTaskAsync<T>(request);
                WriteLogRequestError(client, request, response);
                if (response != null && response.Data == null && !string.IsNullOrEmpty(response.Content) && response.Content.IsValidJson<T>())
                {
                    response.Data = useJsonConverter
                   ? JsonConvert.DeserializeObject<T>(response.Content)
                   : response.Content.FromJson<T>();
                }


                return response;
            }
            catch (Exception ex)
            {
                logInfo.LogError(ex);
                throw;
            }
        }

        public static async Task<IRestResponse<T>> PostRequestToEcommerce<T, T1>(
            LogObjectMicrosoftExtension logInfo,
            string endpoint, T1 body,
            Dictionary<string, string> header = null,
            bool isRetryGateWayTimeOut = false, CancellationToken ct = default) where T : class where T1 : class
        {
            var builder = new UriBuilder(endpoint);

            var client = new RestClient(builder.ToString());
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-type", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(body), ParameterType.RequestBody);
            client.AddHandler("application/json", new DynamicJsonDeserializerResponse(logInfo));


            if (header != null)
            {
                foreach (var entry in header)
                {
                    request.AddHeader(entry.Key, entry.Value);
                }
            }
            IRestResponse<T> response = null;
            try
            {
                if (isRetryGateWayTimeOut)
                {
                    response = await ExecuteWithRetryGatewayTimeOut<T>(client, request, endpoint, ct);
                }
                else
                {
                    response = await client.ExecuteTaskAsync<T>(request, ct);
                }
                WriteLogRequestError(client, request, response);
                if (response != null && response.Data == null && !string.IsNullOrEmpty(response.Content) && response.Content.IsValidJson<T>())
                {
                    response.Data = JsonConvert.DeserializeObject<T>(response.Content);
                }
            }
            catch (Exception ex)
            {
                logInfo.LogError(ex);
                throw;
            }

            return response;
        }

        public static async Task<IRestResponse<T>> ForwarderApiCallWork<T>(LogObjectMicrosoftExtension logInfo,
            string endpoint, string body, string method, Dictionary<string, string> header, CancellationToken ct = default)
        {
            var logGrpc = logInfo.Clone($"{logInfo.Action}_GRPCResponse");
            var channel = SingletonChannelPool.Instance.OpenChannel(SelfAppConfig.ForwarderApiFeature.Endpoint);
            try
            {
                var client = new ForwarderApiCall.ForwarderApiCallClient(channel);
                var getOrdersGrpcRequest = new Request
                {
                    LogId = logInfo.Id.ToString(),
                    Endpoint = endpoint,
                    Method = method,
                    Header = JsonConvert.SerializeObject(header),
                    Body = body,
                };
                var responseGrpc = await client.ForwarderAsync(getOrdersGrpcRequest, cancellationToken: ct);
                logGrpc.RequestObject = getOrdersGrpcRequest.ToSafeJson();
                logGrpc.ResponseObject = responseGrpc.Content.ToSafeJson();
                logGrpc.LogInfo(true);

                Enum.TryParse(responseGrpc.StatusCode, out HttpStatusCode statusCode);
                return new RestResponse<T>
                {
                    StatusCode = statusCode,
                    Content = responseGrpc.Content,
                    ErrorException = !string.IsNullOrEmpty(responseGrpc.ErrorException)
                        ? new Exception(responseGrpc.ErrorException)
                        : null,
                    Data = !string.IsNullOrEmpty(responseGrpc.Content)
                        ? JsonConvert.DeserializeObject<T>(responseGrpc.Content)
                        : default
                };
            }
            catch (Exception ex)
            {
                logGrpc.LogError(ex);
                throw;
            }
            finally
            {
                SingletonChannelPool.Instance.CloseChannel(channel, SelfAppConfig.ForwarderApiFeature.Endpoint);
            }
        }


        public static async Task<IRestResponse<T>> ForwarderApiCallWork<T>(string logInfo, string endpoint, string body, string method, string header, string parameters, CancellationToken ct = default, bool hasSameKeyInParam = false)
        {
            var channel = SingletonChannelPool.Instance.OpenChannel(SelfAppConfig.ForwarderApiFeature.Endpoint);
            try
            {
                var client = new ForwarderApiCall.ForwarderApiCallClient(channel);
                var getOrdersGrpcRequest = new Request
                {
                    LogId = logInfo,
                    Endpoint = endpoint,
                    Method = method,
                    Header = header,
                    Body = body,
                    Params = parameters,
                    HasSameKeyInParam = hasSameKeyInParam
                };
                var responseGrpc = await client.ForwarderAsync(getOrdersGrpcRequest, cancellationToken: ct);
                Enum.TryParse(responseGrpc.StatusCode, out HttpStatusCode statusCode);
                return new RestResponse<T>
                {
                    StatusCode = statusCode,
                    Content = responseGrpc.Content,
                    ErrorException = !string.IsNullOrEmpty(responseGrpc.ErrorException)
                        ? new Exception(responseGrpc.ErrorException)
                        : null,
                    Data = !string.IsNullOrEmpty(responseGrpc.Content)
                        ? JsonConvert.DeserializeObject<T>(responseGrpc.Content)
                        : default
                };
            }
            finally
            {
                SingletonChannelPool.Instance.CloseChannel(channel, SelfAppConfig.ForwarderApiFeature.Endpoint);
            }
        }

        private static void WriteLogRequestError<T>(RestClient client, RestRequest request, IRestResponse<T> response)
        {
            if (response == null)
            {
                _log.Error($"{KeywordCommonSearch.CallApiError} response is null BaseUrl: {client.BaseUrl}");
                return;
            }

            if ((response.StatusCode != HttpStatusCode.Accepted && response.StatusCode != HttpStatusCode.OK && response.StatusCode != HttpStatusCode.Created)
                || (response.Data == null && !string.IsNullOrWhiteSpace(response.ErrorMessage))
                || (response.Data is BaseShopeeResponse baseShopeeResponse && !string.IsNullOrEmpty(baseShopeeResponse.Error)))
            {
                try
                {
                    StringBuilder bld = new StringBuilder();
                    var msgLog = $"{KeywordCommonSearch.CallApiError} Request data: BaseUrl: {client.BaseUrl} \n Method: {request.Method} \n Default Parameters: {client.DefaultParameters.ToJson()}";
                    bld.Append(msgLog);
                    var parameters = request.Parameters?.Where(x => x.Name != "Authorization").ToList();
                    if (parameters?.Count > 0)
                    {
                        var detailParams = new StringBuilder();
                        foreach (var item in parameters)
                        {
                            detailParams.Append($"\"{item.Name}\":\"{item.Value?.ToString()}\"");

                        }

                        bld.Append($" \n<br> Parameters: {detailParams}");
                    }

                    bld.Append($"\n <<<======>>> Response data: StatusCode: {response.StatusCode.ToString()}");
                    if (response.Data != null) bld.Append($" \n Data: {response.Data.ToJson()}");
                    if (!string.IsNullOrEmpty(response.ErrorMessage)) bld.Append($" \n ErrorMessage: {response.ErrorMessage} Content: {response.Content}");
                    _log.Warn(bld.ToString());
                }
                catch (Exception ex)
                {
                    _log.Error($"{KeywordCommonSearch.CallApiError} Request save log error BaseUrl: {client.BaseUrl} - Message: {ex.Message}", ex);
                }
            }
        }

        private static void WriteLogRequestError<T>(RestClient client, RestRequest request, IRestResponse<T> response, LogObjectMicrosoftExtension loggerExtension)
        {
            if (loggerExtension == null)
            {
                WriteLogRequestError<T>(client, request, response);
                return;
            }
            try
            {
                if (response == null)
                {
                    loggerExtension.LogWarning($"{KeywordCommonSearch.CallApiError} response is null BaseUrl: {client.BaseUrl}");
                    return;
                }

                if ((response.StatusCode != HttpStatusCode.Accepted && response.StatusCode != HttpStatusCode.OK && response.StatusCode != HttpStatusCode.Created)
                    || (response.Data == null && !string.IsNullOrWhiteSpace(response.ErrorMessage))
                    || (response.Data is BaseShopeeResponse baseShopeeResponse && !string.IsNullOrEmpty(baseShopeeResponse.Error)))
                {

                    var msgLog = $"{KeywordCommonSearch.CallApiError} Request data: BaseUrl: {client.BaseUrl} \n Method: {request.Method} \n Default Parameters: {client.DefaultParameters.ToJson()}";
                    var parameters = request.Parameters?.Where(x => x.Name != "Authorization").ToList();
                    if (parameters?.Count > 0)
                    {
                        var detailParams = string.Empty;
                        foreach (var item in parameters)
                        {
                            detailParams += $"\"{item.Name}\":\"{item.Value.ToString()}\"";
                        }
                        msgLog += $" \n<br> Parameters: {detailParams}";
                    }

                    msgLog += $"\n <<<======>>> Response data: StatusCode: {response.StatusCode.ToString()}";
                    if (response.Data != null) msgLog += $" \n Data: {response.Data.ToJson()}";
                    if (!string.IsNullOrEmpty(response.ErrorMessage)) msgLog += $" \n ErrorMessage: {response.ErrorMessage} Content: {response.Content}";
                    var ex = new TikiException(msgLog);
                    loggerExtension.LogError(ex);

                }
            }
            catch (Exception ex)
            {
                loggerExtension.LogError(ex);
            }
        }

        private static void WriteLogException(string endpoint, Exception exception)
        {
            _log.Error($"{KeywordCommonSearch.CallApiError} exception BaseUrl: {endpoint} - Message: {exception.Message}", exception);
        }
    }
}
﻿using Demo.OmniChannel.ChannelClient.ResponseDTO.Tiktok;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Models;
using System.Collections.Generic;
using System.Linq;

namespace Demo.OmniChannel.ChannelClient.Common
{
    public static class TiktokFeeHelper
    {
        #region V2
        public static List<FeeDetail> OnConvertFeeJsonV2(TiktokOrderDetail tiktokOrder, List<TiktokSettlement> settlementList)
        {
            List<FeeDetail> resultFeeDetail = new List<FeeDetail>();
            if (settlementList == null || settlementList.Count == 0)
            {
                return resultFeeDetail;
            }
            settlementList = settlementList.Where(item => item.SettStatus != TiktokPaymentStatus.NotPay).ToList();
            foreach (var settlementItem in settlementList)
            {
                ConvertTransactionFeeToFeeJsonV2(settlementItem, resultFeeDetail);
            }
            if (resultFeeDetail.Count > 0)
            {
                OnCalOtherFeeV2(tiktokOrder, settlementList, resultFeeDetail);
            }
            resultFeeDetail = resultFeeDetail.GroupBy(item => item.Name).Select(item => new FeeDetail
            {
                Name = item.Key,
                Value = item.Sum(x => x.Value)
            }).ToList();

            return resultFeeDetail;
        }


        private static void OnCalOtherFeeV2(TiktokOrderDetail tiktokOrder, List<TiktokSettlement> settlementList, List<FeeDetail> resultFeeDetail)
        {
            if (settlementList != null && settlementList.Count > 0)
            {
                var settlementAmountLst = settlementList.Where(item => item.SettlementInfo != null
                    && !string.IsNullOrEmpty(item.SettlementInfo.SettlementAmount)).Select(item => item.SettlementInfo.SettlementAmount);
                var settlementAmountValue = settlementAmountLst.Sum(value => Helpers.ToFloat(value, (float)0));

                var shippingFeeAmountLst = settlementList.Where(item => item.SettlementInfo != null
                && !string.IsNullOrEmpty(item.SettlementInfo.ShippingFee)).Select(item => item.SettlementInfo.ShippingFee);
                var shippingFeeAmountValue = shippingFeeAmountLst.Sum(value => Helpers.ToFloat(value, (float)0));

                var sumFeeDetail = resultFeeDetail.Sum(item => item.Value);
                if (tiktokOrder.PaymentInfo != null)
                {
                    var otherFeeValue = tiktokOrder.PaymentInfo.SubTotal + tiktokOrder.PaymentInfo.PlatformDiscount
                        + tiktokOrder.PaymentInfo.ShippingFee - settlementAmountValue - sumFeeDetail - shippingFeeAmountValue;
                    if (otherFeeValue != 0)
                    {
                        resultFeeDetail.Add(new FeeDetail
                        {
                            Name = "TiktokOtherFee",
                            Value = otherFeeValue
                        });
                    }
                }
            }
        }

        public static void ConvertTransactionFeeToFeeJsonV2(TiktokSettlement settlementItem, List<FeeDetail> resultFeeDetail)
        {
            if (settlementItem == null || settlementItem.SettlementInfo == null)
            {
                return;
            }
            var settlementInfo = settlementItem.SettlementInfo;
            if (StringExtension.IsValidStringNumber(settlementInfo.PaymentFee))
            {
                resultFeeDetail.Add(new FeeDetail
                {
                    Name = "TiktokPaymentFee",
                    Value = Helpers.ToFloat(settlementInfo.PaymentFee, (float)0)
                });
            }
            if (StringExtension.IsValidStringNumber(settlementInfo.PlatformCommission))
            {
                resultFeeDetail.Add(new FeeDetail
                {
                    Name = "TiktokPlatformCommission",
                    Value = Helpers.ToFloat(settlementInfo.PlatformCommission, (float)0)
                });
            }
            if (StringExtension.IsValidStringNumber(settlementInfo.FlatFee))
            {
                resultFeeDetail.Add(new FeeDetail
                {
                    Name = "TiktokFlatFee",
                    Value = Helpers.ToFloat(settlementInfo.FlatFee, (float)0)
                });
            }
            if (StringExtension.IsValidStringNumber(settlementInfo.SalesFee))
            {
                resultFeeDetail.Add(new FeeDetail
                {
                    Name = "TiktokSalesFee",
                    Value = Helpers.ToFloat(settlementInfo.SalesFee, (float)0)
                });
            }
            if (StringExtension.IsValidStringNumber(settlementInfo.AffiliateCommission))
            {
                resultFeeDetail.Add(new FeeDetail
                {
                    Name = "TiktokAffiliateCommission",
                    Value = Helpers.ToFloat(settlementInfo.AffiliateCommission, (float)0)
                });
            }
            if (StringExtension.IsValidStringNumber(settlementInfo.Vat))
            {
                resultFeeDetail.Add(new FeeDetail
                {
                    Name = "TiktokVat",
                    Value = Helpers.ToFloat(settlementInfo.Vat, (float)0)
                });
            }
            if (StringExtension.IsValidStringNumber(settlementInfo.Refund))
            {
                resultFeeDetail.Add(new FeeDetail
                {
                    Name = "TiktokRefund",
                    Value = Helpers.ToFloat(settlementInfo.Refund, 0)
                });
            }
        }

        #endregion



        public static List<FeeDetail> OnConvertFeeJson(TiktokOrderDetail tiktokOrder, List<TiktokSettlement> settlementList)
        {
            List<FeeDetail> resultFeeDetail = new List<FeeDetail>();
            if (settlementList == null || settlementList.Count == 0)
            {
                return resultFeeDetail;
            }
            settlementList = settlementList.Where(item => item.SettStatus != TiktokPaymentStatus.NotPay).ToList();
            foreach (var settlementItem in settlementList)
            {
                ConvertTransactionFeeToFeeJson(settlementItem, resultFeeDetail);
            }
            if (resultFeeDetail.Count > 0)
            {
                OnCalOtherFee(tiktokOrder, settlementList, resultFeeDetail);
            }
            resultFeeDetail = resultFeeDetail.GroupBy(item => item.Name).Select(item => new FeeDetail
            {
                Name = item.Key,
                Value = item.Sum(x => x.Value)
            }).ToList();

            return resultFeeDetail;
        }

        private static void OnCalOtherFee(TiktokOrderDetail tiktokOrder, List<TiktokSettlement> settlementList, List<FeeDetail> resultFeeDetail)
        {
            if (settlementList != null && settlementList.Count > 0)
            {
                var settlementAmountLst = settlementList.Where(item => item.SettlementInfo != null
                    && !string.IsNullOrEmpty(item.SettlementInfo.SettlementAmount)).Select(item => item.SettlementInfo.SettlementAmount);
                var settlementAmountValue = settlementAmountLst.Sum(value => Helpers.ToFloat(value, (float)0));
                var sumFeeDetail = resultFeeDetail.Sum(item => item.Value);

                var refundLst = settlementList.Where(item => item.SettlementInfo != null
                    && !string.IsNullOrEmpty(item.SettlementInfo.Refund)).Select(item => item.SettlementInfo.Refund);
                var refundValue = refundLst.Sum(value => Helpers.ToFloat(value, (float)0));

                if (tiktokOrder.PaymentInfo != null)
                {
                    var otherFeeValue = tiktokOrder.PaymentInfo.SubTotal + tiktokOrder.PaymentInfo.PlatformDiscount -
                        tiktokOrder.PaymentInfo.ShippingFeesellerDiscount - sumFeeDetail - settlementAmountValue - refundValue;
                    if (otherFeeValue != 0)
                    {
                        resultFeeDetail.Add(new FeeDetail
                        {
                            Name = "TiktokOtherFee",
                            Value = otherFeeValue
                        });
                    }
                }
            }
        }

        public static void ConvertTransactionFeeToShippingFee(List<FeeDetail> resultFeeDetail, float shippingFee)
        {
            resultFeeDetail.Add(new FeeDetail
            {
                Name = "TiktokShippingFee",
                Value = shippingFee
            });
        }

        public static void ConvertTransactionFeeToFeeJson(TiktokSettlement settlementItem, List<FeeDetail> resultFeeDetail)
        {
            if (settlementItem == null || settlementItem.SettlementInfo == null)
            {
                return;
            }
            var settlementInfo = settlementItem.SettlementInfo;
            if (StringExtension.IsValidStringNumber(settlementInfo.PaymentFee))
            {
                resultFeeDetail.Add(new FeeDetail
                {
                    Name = "TiktokPaymentFee",
                    Value = Helpers.ToFloat(settlementInfo.PaymentFee, (float)0)
                });
            }
            if (StringExtension.IsValidStringNumber(settlementInfo.PlatformCommission))
            {
                resultFeeDetail.Add(new FeeDetail
                {
                    Name = "TiktokPlatformCommission",
                    Value = Helpers.ToFloat(settlementInfo.PlatformCommission, (float)0)
                });
            }
            if (StringExtension.IsValidStringNumber(settlementInfo.FlatFee))
            {
                resultFeeDetail.Add(new FeeDetail
                {
                    Name = "TiktokFlatFee",
                    Value = Helpers.ToFloat(settlementInfo.FlatFee, (float)0)
                });
            }
            if (StringExtension.IsValidStringNumber(settlementInfo.SalesFee))
            {
                resultFeeDetail.Add(new FeeDetail
                {
                    Name = "TiktokSalesFee",
                    Value = Helpers.ToFloat(settlementInfo.SalesFee, (float)0)
                });
            }
            if (StringExtension.IsValidStringNumber(settlementInfo.AffiliateCommission))
            {
                resultFeeDetail.Add(new FeeDetail
                {
                    Name = "TiktokAffiliateCommission",
                    Value = Helpers.ToFloat(settlementInfo.AffiliateCommission, (float)0)
                });
            }
            if (StringExtension.IsValidStringNumber(settlementInfo.Vat))
            {
                resultFeeDetail.Add(new FeeDetail
                {
                    Name = "TiktokVat",
                    Value = Helpers.ToFloat(settlementInfo.Vat, (float)0)
                });
            }
            if (StringExtension.IsValidStringNumber(settlementInfo.ShippingFeeAdjustment))
            {
                resultFeeDetail.Add(new FeeDetail
                {
                    Name = "TiktokShippingFeeAdjustment",
                    Value = Helpers.ToFloat(settlementInfo.ShippingFeeAdjustment, (float)0)
                });
            }
            if (StringExtension.IsValidStringNumber(settlementInfo.ShippingFee))
            {
                resultFeeDetail.Add(new FeeDetail
                {
                    Name = "TiktokShippingFee",
                    Value = Helpers.ToFloat(settlementInfo.ShippingFee, (float)0)
                });
            }
            if (StringExtension.IsValidStringNumber(settlementInfo.Refund))
            {
                resultFeeDetail.Add(new FeeDetail
                {
                    Name = "TiktokRefund",
                    Value = Helpers.ToFloat(settlementInfo.Refund, 0)
                });
            }
        }

    }
}

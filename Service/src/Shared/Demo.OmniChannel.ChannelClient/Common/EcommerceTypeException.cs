﻿namespace Demo.OmniChannel.ChannelClient.Common
{
    public static class EcommerceTypeException
    {
        public const string EmptyAccessToken = "{0}EmptyAccessToken";

        public const string SyncOnHand = "{0}SyncOnHand";
        public const string SyncMultiOnHand = "{0}SyncMultiOnHand";
        public const string SyncMultiOnHandSyncItem = "{0}SyncMultiOnHand-SyncItem";
        public const string SyncMultiOnHandSyncBatch = "{0}SyncMultiOnHand-SyncBatch";
        public const string SyncMultiOnHandWithGroupParrent = "{0}SyncMultiOnHandWithGroupParrent";
        public const string SyncPrice = "{0}SyncPrice";
        public const string SyncMultiPrice = "{0}SyncMultiPrice";
        public const string SyncUpdatePriceStockHelper = "{0}SyncUpdatePriceStockHelper";
        public const string SyncMultiPriceSyncItem = "{0}SyncMultiPrice-SyncItem";
        public const string SyncMultiPriceSyncBatch = "{0}SyncMultiPrice-SyncBatch";
        public const string SyncMultiPriceWithGroupParrent = "{0}SyncMultiPriceWithGroupParrent";
        public const string GetProductDetail = "{0}GetProductDetail";
        public const string GetOrders = "{0}GetOrders";
        public const string GetOrderDetail = "{0}GetOrderDetail";
        public const string GetInvoiceDetail = "{0}GetInvoiceDetail";
        public const string RefreshAccessToken = "{0}RefreshAccessToken";
        public const string GetOrderStatus = "{0}GetOrderStatus";
        public const string GetListProduct = "{0}GetListProduct";
        public const string GetListProductPage = "{0}GetListProductPage";
        public const string GetListDeletedProduct = "{0}GetListDeletedProduct";
        public const string OrderDiscountDetail = "{0}OrderDiscountDetail";
        public const string OrderDetail = "{0}OrderDetail";
        public const string Logistics = "{0}Logistics";
        public const string GetCustomerByOrderId = "{0}GetCustomerByOrderId";
        public const string MappingInvoiceLzdLog = "{0}MappingInvoiceLzdLog";
        public const string UpdateOnHand = "{0}UpdateOnHand";
        public const string UpdateMultiOnhand = "{0}UpdateMultiOnhand";
        public const string GetEscrowDetail = "{0}GetEscrowDetail";
        public const string GetInCome = "{0}GetInCome";
        public const string GetLogistics = "{0}GetLogistics";
        public const string GetOrderById = "{0}GetOrderById";
        public const string GetFinanceTransactions = "{0}GetFinanceTransactions";
        public const string GetOrderTrace = "{0}GetOrderTrace";
    }

    public static class Ecommerce
    {
        public const string TikiV2 = "TikiV2";
        public const string Tiki = "Tiki";
        public const string Shopee = "Shopee";
        public const string Sendo = "Sendo";
        public const string Lazada = "Lazada";
        public const string TikTok = "TikTok";
    }
}

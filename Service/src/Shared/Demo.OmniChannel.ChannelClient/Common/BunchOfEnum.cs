﻿using Demo.OmniChannel.Sdk.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Demo.OmniChannel.ChannelClient.Common
{
    public enum EcommerceSellQtyFormula
    {
        [Description("ecommSellQtyFormula1")]
        OnHand = 1,
        [Description("ecommSellQtyFormula2")]
        SubOrder,
        [Description("ecommSellQtyFormula3")]
        AddSupplierOrder,
        [Description("ecommSellQtyFormula4")]
        SubOrderAddSupplierOrder
    }
    public enum ChannelTypeEnum
    {
        [Description("DHLZD")]
        Lazada = 1,
        [Description("DHSPE")]
        Shopee = 2,
        [Description("DHTIKI")]
        Tiki = 3,
        [Description("DHSDO")]
        Sendo = 4,
        [Description("DHSTIKTOK")]
        Tiktok = 5
    }

    public enum PulicOrderMessageSource
    {
        [Description("WEBHOOK")]
        Webhook = 1,
        [Description("JOB")]
        Job = 2,
        [Description("SyncProduct")]
        SyncProduct = 1,
    }

    internal enum ChannelStatus
    {
        Pending = 1,
        Reject = 2,
        Approved = 3
    }

    public enum Delivery
    {
        [Description("commonDelivery")]
        Normal = 0,
        [Description("fastDelivery")]
        Quick = 1,
        [Description("deliveryInDay")]
        InDays = 2
    }

    public enum ChannelProductType
    {
        Normal = 1,
        Variation = 2
    }
    public enum DeliveryStatus
    {
        [Description("Chờ xử lý")]
        Pending = 1,
        [Description("Đang giao hàng")]
        Delivering = 2,
        [Description("Giao thành công")]
        Delivered = 3,
        [Description("Đang chuyển hoàn")]
        Returning = 4,
        [Description("Đã chuyển hoàn")]
        Returned = 5,
        [Description("Đã hủy")]
        Void = 6,
        [Description("Đang lấy hàng")]
        InPickup = 7,
        PickupRetry = 8,
        [Description("Đã lấy hàng")]
        Pickuped = 9,
        DeliveringRetry = 10,
        ReturnPending = 11,
        ReturnRetry = 12,
        WaitingReturn = 13
    }
    public enum OrderState
    {
        [Description("Phiếu tạm")]
        Pending = 0,
        [Description("billTemp")]
        Draft = 1,
        [Description("Đang giao hàng")]
        Ongoing = 2,
        [Description("Hoàn thành")]
        Finalized = 3,
        [Description("Đã hủy")]
        Void = 4,
        [Description("Đã xác nhận")]
        Verified = 5
    }
    public enum InvoiceState
    {
        [Description("Hoàn thành")]
        Issued = 1,
        [Description("Hủy")]
        Void = 2,
        [Description("Đang xử lý")]
        Pending = 3,
        [Description("Không giao được")]
        Failed = 5
    }

    public enum ShopeeSurchargeType
    {
        [Description("Trợ giá Shopee")]
        Subsidize = 1,
        [Description("Thu lệch vận chuyển")]
        FeeShip = 2
    }

    public enum TiktokSurchargeType
    {
        [Description("Thu lệch vận chuyển")]
        FeeShip = 2
    }

    public class ChannelTypeHelper
    {
        public static string GenerateSurchargeCode(byte channelType, int retailerId, byte surchargeType)
        {
            var code = string.Empty;
            var prefix = "TH";
            if (surchargeType == (byte)ShopeeSurchargeType.FeeShip) prefix = "TL";
            switch (channelType)
            {
                case (byte)Sdk.Common.ChannelType.Lazada:
                    {
                        code = $"{prefix}LZD_{retailerId}";
                        break;
                    }
                case (byte)Sdk.Common.ChannelType.Shopee:
                    {
                        code = $"{prefix}SPE_{retailerId}";
                        break;
                    }
                case (byte)Sdk.Common.ChannelType.Sendo:
                    {
                        code = $"{prefix}SDO_{retailerId}";
                        break;
                    }
                case (byte)Sdk.Common.ChannelType.Tiki:
                    {
                        code = $"{prefix}TIKI_{retailerId}";
                        break;
                    }
                case (byte)Sdk.Common.ChannelType.Tiktok:
                    {
                        code = $"{prefix}TTS_{retailerId}";
                        break;
                    }
            }

            return code;
        }

        public static string GetOrderPrefix(byte channelType)
        {
            switch (channelType)
            {
                case (byte)ChannelTypeEnum.Lazada:
                    return "DHLZD";
                case (byte)ChannelTypeEnum.Shopee:
                    return "DHSPE";
                case (byte)ChannelTypeEnum.Tiki:
                    return "DHTIKI";
                case (byte)ChannelTypeEnum.Sendo:
                    return "DHSDO";
                case (byte)ChannelTypeEnum.Tiktok:
                    return "DHTTS";
            }
            return null;
        }

        public static string GetInvoicePrefix(byte channelType)
        {
            switch (channelType)
            {
                case (byte)ChannelTypeEnum.Lazada:
                    return "HDLZD";
                case (byte)ChannelTypeEnum.Shopee:
                    return "HDSPE";
                case (byte)ChannelTypeEnum.Tiki:
                    return "HDTIKI";
                case (byte)ChannelTypeEnum.Sendo:
                    return "HDSDO";
                case (byte)ChannelTypeEnum.Tiktok:
                    return "HDTTS";
            }
            return null;
        }

        public static string GetCustomerPrefix(byte channelType)
        {
            switch (channelType)
            {
                case (byte)ChannelTypeEnum.Lazada:
                    return "KHLZD";
                case (byte)ChannelTypeEnum.Shopee:
                    return "KHSPE";
                case (byte)ChannelTypeEnum.Tiki:
                    return "KHTIKI";
                case (byte)ChannelTypeEnum.Sendo:
                    return "KHSDO";
                case (byte)ChannelTypeEnum.Tiktok:
                    return "KHTTS";
            }
            return null;
        }

        public static string ConvertInvoiceCode(string orderSn, byte channelType)
        {
            var channelInvoicePrefix = GetInvoicePrefix(channelType);
            if (channelType == (byte)ChannelTypeEnum.Lazada)
            {
                return $"{channelInvoicePrefix}_{orderSn}.1";
            }
            return $"{channelInvoicePrefix}_{orderSn}";
        }

        public static string DeSufixInvoiceCode(string invoiceCode, string stopAt = ".")
        {
            if (!String.IsNullOrWhiteSpace(invoiceCode))
            {
                int charLocation = invoiceCode.IndexOf(stopAt, StringComparison.Ordinal);

                if (charLocation > 0)
                {
                    return invoiceCode.Substring(0, charLocation);
                }
            }
            return invoiceCode;
        }

        /// <summary>
        /// Input : "Lazada,Shopee,Tiki" 
        /// Output : [1,2,3] in enum ChannelType
        /// </summary>
        /// <param name="channelTypes"></param>
        /// <returns></returns>
        public static List<int> GetChannelTypes(string channelTypes)
        {
            try
            {
                if (string.IsNullOrEmpty(channelTypes)) return new List<int>();
                var channelTypesKey = channelTypes.Split(',');
                return channelTypesKey.Select(type => (int)Enum.Parse(typeof(ChannelType), type)).ToList();
            }
            catch (Exception)
            {
                return new List<int>();
            }
        }

    }

    public enum ShopeeWebhookEventType
    {
        TestCallback = 0,
        OrderStatusUpdate = 3,
        TrackingNoPush = 4,
        PromotionUpdate = 9,

    }
    public enum SendoOrderStatus
    {
        [Description("Mới")] New = 2,
        [Description("Đang xử lý")] Proccessing = 3,
        [Description("Đang vận chuyển")] Shipping = 6,
        [Description("Đã giao hàng")] Pod = 7,
        [Description("Đã hoàn tất")] Completed = 8,
        [Description("Đóng")] Closed = 10,
        [Description("Yêu cầu hoãn")] Delaying = 11,
        [Description("Đang hoãn")] Delay = 12,
        [Description("Hủy")] Cancelled = 13,
        [Description("Yêu cầu tách")] Splitting = 14,
        [Description("Chờ tách")] Splitted = 15,
        [Description("Chờ gộp")] Merging = 19,
        [Description("Đang đổi trả")] Returning = 21,
        [Description("Đổi trả thành công")] Returned = 22,
        [Description("Chờ sendo xử lý")] WaitingSendo = 23,
    }

    public enum WarningType
    {
        [Description("Không hỗ trợ")] NoSupport = 1,
        [Description("Thuộc tính lỗi")] PropertiesError = 2,
    }

    public enum PlatformType
    {
        Lazada = 1,
        Shopee = 2,
        Tiki = 3,
        Sendo = 4,
        Tiktok = 5
    }
    public enum DiffReturn
    {
        EqualZero = 0,
        LessThanZero = -1,
        GreaterThanZero = 1,
        NotYetPayment = 2
    }
}
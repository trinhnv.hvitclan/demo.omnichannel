﻿using ServiceStack;
using ServiceStack.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Demo.OmniChannel.ChannelClient.Common
{
    internal class LogObject
    {
        public LogObject(ILog logger, Guid id)
        {
            Id = id;
            Timer = Stopwatch.StartNew();
            StartTime = DateTime.Now.ToString("o");
            Logger = logger;
            LogStages = new List<LogStage>();
        }
        public Guid Id { get; set; }
        public string Action { get; set; }
        public long Duration { get; set; }
        public int? RetailerId { get; set; }
        public int? BranchId { get; set; }
        public string Description { get; set; }
        public long? OmniChannelId { get; set; }
        public object RequestObject { get; set; }
        public object ResponseObject { get; set; }
        public int? TotalItem { get; set; }
        private Stopwatch Timer { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        private ILog Logger { get; set; }
        public List<LogStage> LogStages { get; set; }

        public void LogInfo(bool isStopTimer = false)
        {
            if (isStopTimer)
            {
                Timer.Stop();
            }
            EndTime = DateTime.Now.ToString("o");
            Duration = Timer.ElapsedMilliseconds;

            if (RequestObject != null && !(RequestObject is string))
            {
                RequestObject = RequestObject.ToSafeJson();
            }

            if (ResponseObject != null && !(ResponseObject is string))
            {
                ResponseObject = ResponseObject.ToSafeJson();
            }

            Logger.Info(this.ToSafeJson());
        }

        public void LogError(Exception ex = null, string startMessage = "")
        {
            Timer.Stop();
            EndTime = DateTime.Now.ToString("o");
            Duration = Timer.ElapsedMilliseconds;

            if (RequestObject != null && !(RequestObject is string))
            {
                RequestObject = RequestObject.ToSafeJson();
            }

            if (ResponseObject != null && !(ResponseObject is string))
            {
                ResponseObject = ResponseObject.ToSafeJson();
            }

            if (ex != null)
            {
                Description = string.IsNullOrEmpty(Description) ? ex.Message : Description;
                Logger.Error($"{startMessage}: {this.ToSafeJson()}", ex);
            }
            else
            {
                Logger.Error($"{startMessage}: {this.ToSafeJson()}");
            }
        }

        public void LogWarning()
        {
            Timer.Stop();
            EndTime = DateTime.Now.ToString("o");
            Duration = Timer.ElapsedMilliseconds;

            if (RequestObject != null && !(RequestObject is string))
            {
                RequestObject = RequestObject.ToSafeJson();
            }

            if (ResponseObject != null && !(ResponseObject is string))
            {
                ResponseObject = ResponseObject.ToSafeJson();
            }
            Logger.Warn(this.ToSafeJson());
        }
    }

    internal class LogStage
    {
        public string Name { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public long Duration { get; set; }
        private Stopwatch Timer { get; set; }
        public string Description { get; set; }
        public LogStage(string name)
        {
            Name = name;
            Timer = Stopwatch.StartNew();
            StartTime = DateTime.Now.ToString("o");
        }

        public void EndStage()
        {
            EndTime = DateTime.Now.ToString("o");
            Timer.Stop();
            Duration = Timer.ElapsedMilliseconds;
        }
    }
}
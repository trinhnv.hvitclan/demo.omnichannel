﻿using Microsoft.Extensions.Logging;
using System;

namespace Demo.OmniChannel.ChannelClient.Common
{
    public class KvTrackingOrderLogInfo : LogObjectMicrosoftExtension
    {
        public KvTrackingOrderLogInfo(ILogger logger, Guid id) : base(logger, id)
        {
        }

        public long? ReceivedOrderFirstTime { get; set; }
        public long? OrderCreatedInChanelTime { get; set; }
        public long? OrderCreatedInKv { get; set; }
        public long FirstReceivedOrderPeriodTime { get; set; }
        public long CompletedOrderInKvPeriodTime { get; set; }
        public bool IsCreatedOrderSuccess { get; set; }

    }
}

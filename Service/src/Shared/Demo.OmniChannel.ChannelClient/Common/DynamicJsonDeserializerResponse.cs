﻿using System;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Deserializers;

namespace Demo.OmniChannel.ChannelClient.Common
{
    public class DynamicJsonDeserializerResponse : IDeserializer
    {
        private readonly LogObjectMicrosoftExtension _loggerExtension;
        public DynamicJsonDeserializerResponse(LogObjectMicrosoftExtension loggerExtension)
        {
            _loggerExtension = loggerExtension;
        }    

        public T Deserialize<T>(IRestResponse response)
        {
            try
            {
                var settingFomart = new JsonSerializerSettings
                {
                    DateFormatHandling = DateFormatHandling.MicrosoftDateFormat,
                    DateTimeZoneHandling = DateTimeZoneHandling.Local
                };

                return JsonConvert.DeserializeObject<T>(response.Content, settingFomart);
            }
            catch (Exception ex)
            {
                _loggerExtension.Action = "DynamicJsonDeserializerResponse";
                _loggerExtension.LogError(ex);
                throw;
            }
        }

        public string RootElement { get; set; }
        public string Namespace { get; set; }
        public string DateFormat { get; set; }
    }
}
﻿using System;

namespace Demo.OmniChannel.ChannelClient.Common
{
    internal static class DataRequestHelper
    {
        public static DateTime GetOrderLastSyncDateTime(int syncOrderFormula)
        {
            switch (syncOrderFormula)
            {
                case -1:
                    return DateTime.Now;
                case 0:
                    return DateTime.Today;
                default:
                    return DateTime.Now.AddDays(-syncOrderFormula);

            }
        }

        public static long ParseItemIdToLong(string itemId)
        {
            try
            {
                return long.Parse(itemId);
            }
            catch
            {
                return 0;
            }
        }
    }
}

﻿using Grpc.Core;
using System;
using System.Collections.Concurrent;

namespace Demo.OmniChannel.ChannelClient.Common
{
    public class ChannelPool
    {
        private readonly ConcurrentBag<Channel> _objects;
        private readonly Func<Channel> _objectGenerator;

        public ChannelPool(Func<Channel> objectGenerator)
        {
            _objectGenerator = objectGenerator ?? throw new ArgumentNullException(nameof(objectGenerator));
            _objects = new ConcurrentBag<Channel>();
        }

        public Channel GetChannel()
        {
            if (_objects.TryTake(out var item))
            {
                if (item == null || item.State == ChannelState.Shutdown)
                {
                    item = _objectGenerator();
                }
            }
            else
            {
                item = _objectGenerator();
            }
            return item;
        }

        public void PutObject(Channel item)
        {
            _objects.Add(item);
        }
    
    }

    public sealed class SingletonChannelPool
    {
        private GrpcChannelInitialize _grpcChannelInitialize;
        private ChannelPool _channelPool;
        public static SingletonChannelPool Instance => Nested.instance;
        private readonly object _objLock = new object();

        private void EnsureData(string endpoint)
        {
            if(_grpcChannelInitialize != null)
            {
                return;
            }
            lock (_objLock)
            {
                if (_grpcChannelInitialize == null)
                {
                    _grpcChannelInitialize = new GrpcChannelInitialize(endpoint);
                }
            }
            if (_channelPool != null) return;
            lock (_objLock)
            {
                if (_channelPool == null)
                {
                    _channelPool = new ChannelPool(() => _grpcChannelInitialize.CreateChannel(endpoint));
                }
            }
        }


        public Channel OpenChannel(string endpoint)
        {
            EnsureData(endpoint);
            return _channelPool.GetChannel();
        }


        public void CloseChannel(Channel item, string endpoint)
        {
            EnsureData(endpoint);
            _channelPool.PutObject(item);
        }

        private static class Nested
        {
            
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested()
            {
            }
            internal static readonly SingletonChannelPool instance = new SingletonChannelPool();
        }
    }


}

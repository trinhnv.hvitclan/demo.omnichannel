﻿using Serilog.Events;

namespace Demo.OmniChannel.ChannelClient.Common
{
    public class SubLoggerConfiguration
    {
        public LogLevelConfiguration Information { get; set; }
        public LogLevelConfiguration Error { get; set; }
        public LogLevelConfiguration Warning { get; set; }

    }

    public class LogLevelConfiguration
    {
        public LogEventLevel Level { get; set; }
        public string PathFormat { get; set; }
    }
}

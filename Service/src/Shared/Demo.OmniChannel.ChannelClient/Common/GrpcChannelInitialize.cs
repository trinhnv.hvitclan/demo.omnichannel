﻿using Grpc.Core;
using ServiceStack.Logging;
using System;

namespace Demo.OmniChannel.ChannelClient.Common
{
    public class GrpcChannelInitialize
    {
        private Channel _channel { get; set; }
        private readonly ILog _logger;


        public GrpcChannelInitialize(string endpoint)
        {
            _logger = LogManager.GetLogger(typeof(GrpcChannelInitialize));
            _channel = CreateGRPCChannel(endpoint);
        }

        public Channel CreateGRPCChannel(string endpoint)
        {
            if (string.IsNullOrEmpty(endpoint))
            {
                return null;
            }
            var connectionFactory = new Channel(endpoint, ChannelCredentials.Insecure);
            connectionFactory.ConnectAsync();
            return connectionFactory;
        }

        public void CheckGRPCChannelConnection(string endpoint)
        {
            try
            {
                if (_channel != null && _channel.State != ChannelState.Shutdown && _channel.State != ChannelState.Idle)
                {
                    return;
                }
                _channel = CreateGRPCChannel(endpoint);
            }
            catch (System.Exception ex)
            {
                _logger.Error($"Can't create GRPCChannelConnection... {ex.Message} {ex.StackTrace}", ex);
            }
        }

        public Channel CreateChannel(string endpoint)
        {
            CheckGRPCChannelConnection(endpoint);
            if (_channel != null && _channel.State != ChannelState.Shutdown && _channel.State != ChannelState.Idle)
            {
                return _channel;
            }
            return null;
        }
    }
}

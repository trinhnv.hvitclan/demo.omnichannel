﻿using ServiceStack.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Demo.OmniChannel.ChannelClient.Common
{
    public class SelfAppConfig
    {
        private static IAppSettings _settings;
        public SelfAppConfig(IAppSettings settings)
        {
            _settings = settings;
        }
        #region Shopee
        public static int ShopeePartnerId => _settings.Get<int>("ShopeePartnerId");
        public static string ShopeeSiteFormat => _settings.GetString("ShopeeSiteFormat");
        public static string ShopeeCallBack => _settings.GetString("ShopeeCallBack");
        public static string ShopeeApiEndpoint => _settings.GetString("ShopeeApiEndpoint");
        public static string ShopeeToken => _settings.GetString("ShopeeToken");
        public static List<string> ShopeeExpiredTokenMsg => _settings.Get<List<string>>("ShopeeExpiredTokenMsg");
        public static List<string> ShopeeSellerNeedRegisterMsg => _settings.Get<List<string>>("ShopeeSellerNeedRegisterMsg");

        public static string ShopeeApiEndpointV2 => _settings.GetString("ShopeeApiEndpointV2");
        public static string WebHookKvOnlineApiEndpointV2 => _settings.Get("WebHookKvOnlineApiEndpointV2", "https://webhook.kiotonline.com");

        public static string ShopeeLanguageRequestV2 => _settings.Get("ShopeeLanguageRequestV2", "vi");

        public static ShopeeFeature Shopee => _settings.Get<ShopeeFeature>("Shopee");

        #endregion


        #region Lazada
        public static string LazadaClientAppId => _settings.GetString("LazadaClientAppId");
        public static string LazadaApiSercetKey => _settings.GetString("LazadaApiSercetKey");
        public static string LazadaHostApi => _settings.GetString("LazadaHostApi");
        public static string LazadaHostAuthApi => _settings.GetString("LazadaHostAuthApi");
        public static int LazadaProductPageSize => _settings.Get<int>("LazadaProductPageSize");
        public static int LazadaFinanceTransactionPageSize => _settings.Get<int>("LazadaFinanceTransactionPageSize");
        public static int LazadaFinanceTransactionLimitOffset => _settings.Get<int>("LazadaFinanceTransactionLimitOffset", 100000);

        public static int LazadaOrderPageSize => _settings.Get<int>("LazadaOrderPageSize");
        public static int LazadaTimeOutRequest => _settings.Get<int>("LazadaTimeOutRequest", 20000);
        public static int LazadaReadWriteTimeoutRequest => _settings.Get<int>("LazadaReadWriteTimeoutRequest", 60000);
        public static List<string> LazadaProductIgnoreStatus => _settings.Get<List<string>>("LazadaProductIgnoreStatus", new List<string> { "deleted" });
        public static int LazadaRequestGetProductMaximum => _settings.Get<int>("LazadaRequestGetProductMaximum");
        public static int LazadaTaskCallMaxPerThread => _settings.Get<int>("LazadaTaskCallMaxPerThread",10);
        public static int LazadaFinanceTransactionMaximumRequests => _settings.Get<int>("LazadaFinanceTransactionMaximumRequests");
        public static bool LazadaEnableLogGetTransactionDetail => _settings.Get<bool>("LazadaEnableLogGetTransactionDetail", true);


        #endregion

        #region Tiki
        public static string TikiApiEndpointV21 => _settings.GetString("TikiApiEndpointV21");
        public static string TikiApiEndpoint => _settings.GetString("TikiApiEndpoint");
        public static string TikiApiEndpointV2 => _settings.GetString("TikiApiEndpointV2");
        public static string TikiOAuthURL => _settings.GetString("TikiOAuthURL");
        public static string TikiClientAppId => _settings.GetString("TikiClientAppId");
        public static string TikiApiSercetKey => _settings.GetString("TikiApiSercetKey");
        public static string TikiCallBack => _settings.Get<string>("TikiCallBack", "https://tiki.Demo.vn");
        public static int TikiProductPageSize => _settings.Get<int>("TikiProductPageSize", 100);
        public static int TikiOrderPageSize => _settings.Get<int>("TikiOrderPageSize", 50);
        public static int FirstSyncStart => _settings.Get<int>("FirstSyncStart ", 30);
        public static int SyncStart => _settings.Get<int>("SyncStart", 15);
        public static bool IsWriteLogRequestTiki => _settings.Get<bool>("IsWriteLogRequestTiki");
        #endregion

        #region Sendo

        public static string SendoApiEndpoint => _settings.GetString("SendoApiEndpoint");
        public static int SendoProductPageSize => _settings.Get<int>("SendoProductPageSize", 50);
        public static int SendoOrderPageSize => _settings.Get<int>("SendoOrderPageSize", 50);
        public static bool IsWriteLogRequestSendo => _settings.Get<bool>("IsWriteLogRequestSendo");
        public static List<long> LogRequestSendoByChannels => _settings.Get<List<long>>("LogRequestSendoByChannels");
        public static bool IsRetryRequestPriceAndStockSendo => _settings.Get<bool>("IsRetryRequestPriceAndStockSendo");
        public static int MaxRetryRequestPriceAndStockSendo => _settings.Get<int>("MaxRetryRequestPriceAndStockSendo");

        public static int WaitRetryRequestPriceAndStockSendo =>
            _settings.Get<int>("WaitRetryRequestPriceAndStockSendo", 2);

        #endregion

        #region Order
        public static bool IsStringItemId = _settings?.Get("IsStringItemId", false) ?? false;
        public static string DefaultLanguage => _settings.GetString("DefaultLanguage");
        public static UseSyncOrderV2Feature UseSyncOrderV2Feature => _settings.Get<UseSyncOrderV2Feature>("UseSyncOrderV2Feature");
        public static UseTikiV21Feature UseTikiV21Feature => _settings.Get<UseTikiV21Feature>("UseTikiV21Feature");
        public static UseShopeeOptimizeV2Feature UseShopeeOptimizeV2Feature => _settings.Get<UseShopeeOptimizeV2Feature>("UseShopeeOptimizeV2Feature");

        public static UseTikTokReturnConfirmFeature UseTikTokReturnConfirmFeature => _settings.Get<UseTikTokReturnConfirmFeature>("UseTikTokReturnConfirmFeature");
        public static UseDiscountShopeeFeature UseDiscountShopeeFeature => _settings.Get<UseDiscountShopeeFeature>("UseDiscountShopeeFeature");

        public static UseOptimizeStockService UseOptimizeStockService => _settings.Get<UseOptimizeStockService>("UseOptimizeStockService");
        public static WriteLogOptimizeStockService UseMultiMappingSku => _settings.Get<WriteLogOptimizeStockService>("UseMultiMappingSKU");
        public static bool CheckActiveFeature(BaseConfig config, int retailerId)
        {
            return config.Enable && (config.IncludeRetail.Count == 0 ||
                                     config.IncludeRetail.Contains(retailerId));
        }
        public static bool CheckActiveFeatureByPlatform(BaseConfig config, int retailerId, byte platform)
        {
            if (config.IncludePlatform.Count > 0)
            {
                return config.Enable &&
                       config.IncludePlatform.Any(t =>
                           t.Type == platform && (t.IncludeRetail.Count == 0 || t.IncludeRetail.Contains(retailerId)));
            }

            return config.Enable && (config.IncludeRetail.Count == 0 ||
                                     config.IncludeRetail.Contains(retailerId));
        }
        public static int ChannelRetry => _settings.Get<int>("ChannelRetry", 3);
        public static int LazadaGetEmptyRetry => _settings.Get("LazadaGetEmptyRetry", 3);
        public static RetryGateWayTimeOutConfig RetryGateWayTimeOut => _settings.Get<RetryGateWayTimeOutConfig>("RetryGateWayTimeOut");
        public static int ChannelRetryInterval => _settings.Get<int>("ChannelRetryInterval", 3);
        public static int ChannelTimeoutSecond => _settings.Get("ChannelTimeoutSecond", 5);

        public static int ShopeeGetOrderDetailPageSize => _settings.Get("ShopeeGetOrderDetailPageSize", 30);
        public static ForwarderApiFeature ForwarderApiFeature =>
            _settings.Get<ForwarderApiFeature>("ForwarderApiFeature") ?? new ForwarderApiFeature();

        public static bool UseCallApiMappingGolang => _settings.Get("UseCallApiMappingGolang", true);
        #endregion

        #region Tiktok
        public static ShopeeMigrationV2ApiFeature ShopeeMigrationV2ApiFeature => _settings.Get<ShopeeMigrationV2ApiFeature>("ShopeeMigrationV2ApiFeature");

        public static TiktokFeature Tiktok => _settings.Get<TiktokFeature>("Tiktok");
        #endregion
    }

    public class RetryGateWayTimeOutConfig
    {
        public int Total { get; set; }
        public int Delay { get; set; }
    }
    public class UseSyncOrderV2Feature : BaseConfig { }
    public class UseTikiV21Feature : BaseConfig { }
    public class UseShopeeOptimizeV2Feature : BaseConfig
    {
        public string V2WebhookEndPoint { get; set; }
    }

    public class UseDiscountShopeeFeature : BaseConfig
    {
    }

    public class UseTikTokReturnConfirmFeature : BaseConfig
    {
        public List<string> CancelUserLst { get; set; } = new List<string>();
        public List<string> CancelReasonLst { get; set; } = new List<string>();

    }
    public class UseOptimizeStockService : BaseConfig { }
    public class WriteLogOptimizeStockService : BaseConfig { }
    public class ForwarderApiFeature
    {
        /// <summary>
        /// Bật tính năng năng ForwarderApi
        /// </summary>
        public bool Enable { get; set; } = false;
        /// <summary>
        /// Endpoint Dc2
        /// </summary>
        public string Endpoint { get; set; }
        /// <summary>
        /// True: Sẽ mặc định gọi ForwarderApi, False khi nào timeout sẽ gọi sang ForwarderApi
        /// </summary>
        public bool DefaultForwarder { get; set; } = false;
    }
    public class TiktokFeature
    {
        public string AuthEndPoint { get; set; }
        public string Endpoint { get; set; }
        public int ProductPageSize { get; set; }
        public string EditProductUrl { get; set; }
        public int ProductDetailThread { get; set; } = 50;
        public int ProductListThread { get; set; } = 10;
        public List<string> MsgProductDelete { get; set; }
        public MappingBusinessV2 MappingBusinessV2 { get; set; }
}

    public class ShopeeMigrationV2ApiFeature : BaseConfig
    {
    }

    public class TiktokOptimizeProductApiFeature : BaseConfig
    {
    }

    public class MappingV2Feature : BaseConfig
    {
        public string TopicMappingRequestName { get; set; }
        public string TopicRemoveMappingRequestName { get; set; }
    }
    public class OnHandPriceV2 : BaseConfig
    {
        public bool IsOptimizeMapping { get; set; }

        private List<int> _includeV2;

        public List<int> IncludeV2
        {
            get => _includeV2 ?? new List<int>();
            set => _includeV2 = value;
        }
    }

    public class SyncInvoiceError 
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public List<string> ErrorScan { get; set; }
    }

    public class SyncOrderError 
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public List<string> ErrorScan { get; set; }
    }

    public class IgnoreFeeTypeLst
    {
        public List<string> FeeLst { get; set; }
        public List<string> FeePaymentLst { get; set; }

    }

    public class FeeLzdDefConfig
    {
        public List<FeeLzdItem> FeeLzdDefLst { get; set; }
    }

    public class FeeLzdItem
    {
        public string Label { get; set; }
        public string Value { get; set; }
    }

    public class UseMultiMappingSKU : BaseConfig { }

    public class MappingBusinessV2 : BaseConfig { }

    public sealed class SelfAppConfigSingleton
    {
        #region Constructor

        public static SelfAppConfigSingleton Instance => Nested.instance;

        private static class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested()
            {
            }

            // ReSharper disable once InconsistentNaming
            internal static readonly SelfAppConfigSingleton instance = new SelfAppConfigSingleton();
        }

        public SelfAppConfigSingleton()
        {

        }

        private FeeLzdDefConfig _feeLzdDefConfig;
        #endregion
        public void InitAppSettings(IAppSettings appSettings)
        {
            _feeLzdDefConfig = appSettings?.Get<FeeLzdDefConfig>("FeeLzdDefConfig");
        }

        public FeeLzdDefConfig GetFeeLzdDefConfig()
        {
            return _feeLzdDefConfig;
        }
    }

    public class ShopeeFeature
    {
        public int TimeDelayPushProduct { get; set; } //Miliseconds
        public List<string> ErrorRetryPushProduct { get; set; }
        public int ProductDetailThread { get; set; } = 50;
        public int ProductListThread { get; set; } = 10;
    }
    public class FeeTikiV21Feature : BaseConfig 
    {
        public string TimeLimit { get; set; }
        public decimal TotalSellerFee { get; set; }
    }
    public class OptimizeApiProductChannelFeature : BaseConfig
    {
    }
    public class DeactivateChannelFeature : BaseConfig {}

    public class CreateInvoiceV2Feature : BaseConfig
    {
        public string TopicCreateInvoiceShopee { get; set; }
        public string TopicCreateInvoiceTiktok { get; set; }
    }
    public class TikTokAuthConfigFeature : BaseConfig {}
    public class SyncOrderErrorV2Feature : BaseConfig {}

}
﻿using Microsoft.Extensions.Logging;
using Serilog.Context;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Demo.OmniChannel.ChannelClient.Common
{
    public class LogObjectMicrosoftExtension
    {
        public LogObjectMicrosoftExtension(ILogger logger, Guid id)
        {
            Id = id;
            Timer = Stopwatch.StartNew();
            TimerTotalTime = Stopwatch.StartNew();
            StartTime = DateTime.Now.ToString("o");
            Logger = logger;
        }

        public List<Guid> ParentIds { get; set; }
        public Guid Id { get; set; }
        public string Action { get; set; }

        private Stopwatch Timer { get; set; }
        private Stopwatch TimerTotalTime { get; set; }
        public string StartTime { get; set; }
        public long? TotalTime { get; set; }
        public long Duration { get; set; }
        public int? RetailerId { get; set; }
        public int? BranchId { get; set; }
        public string RetailerCode { get; set; }
        public string Description { get; set; }
        public long? OmniChannelId { get; set; }
        public object RequestObject { get; set; }
        public object ResponseObject { get; set; }
        public long? TotalItem { get; set; }
        private ILogger Logger { get; set; }
        public string CustomerCentricType { get; set; }
        public byte ChannelType { get; set; }
        public string ChannelTypeCode { get; set; }
        public string ShopId { get; set; }
        public string OrderId { get; set; }
        public string EcommerceRequest { get; set; }
        public long GroupId { get; set; }
        public int? WarningType { get; set; }
        public void ResetTimer()
        {
            Timer.Reset();
            Timer.Start();
        }

        public void LogInfo(bool isLogTotalTime = false)
        {
            if (isLogTotalTime)
            {
                TimerTotalTime.Stop();
                TotalTime = TimerTotalTime.ElapsedMilliseconds;
                Duration = TimerTotalTime.ElapsedMilliseconds;
            }
            else
            {
                Duration = Timer.ElapsedMilliseconds;
            }

            if (RequestObject != null && !(RequestObject is string))
            {
                RequestObject = RequestObject.ToSafeJson();
            }

            if (ResponseObject != null && !(ResponseObject is string))
            {
                ResponseObject = ResponseObject.ToSafeJson();
            }

            using (LogContext.PushProperty(nameof(Action), Action))
            using (LogContext.PushProperty(nameof(RetailerId), RetailerId))
            {
                if (!string.IsNullOrEmpty(EcommerceRequest))
                {
                    LogContext.PushProperty(nameof(EcommerceRequest), EcommerceRequest);
                }
                Logger.LogInformation(this.ToSafeJson());
            }
        }

        public void LogInfo(string message, bool isLogTotalTime = false)
        {
            Timer.Stop();
            RequestObject = message;
            if (isLogTotalTime)
            {
                TimerTotalTime.Stop();
                TotalTime = TimerTotalTime.ElapsedMilliseconds;
                Duration = TimerTotalTime.ElapsedMilliseconds;
            }
            else
            {
                Duration = Timer.ElapsedMilliseconds;
            }

            if (RequestObject != null && !(RequestObject is string))
            {
                RequestObject = RequestObject.ToSafeJson();
            }

            if (ResponseObject != null && !(ResponseObject is string))
            {
                ResponseObject = ResponseObject.ToSafeJson();
            }

            using (LogContext.PushProperty(nameof(Action), Action))
            using (LogContext.PushProperty(nameof(RetailerId), RetailerId))
            {
                if (!string.IsNullOrEmpty(EcommerceRequest))
                {
                    LogContext.PushProperty(nameof(EcommerceRequest), EcommerceRequest);
                }
                Logger.LogInformation(this.ToSafeJson());
            }
        }

        public void LogError(Exception ex, bool isLogTotalTime = false, bool isValidate = false)
        {
            Timer.Stop();
            TimerTotalTime.Stop();
            if (isLogTotalTime)
            {
                TotalTime = Timer.ElapsedMilliseconds;
            }
            else
            {
                Duration = Timer.ElapsedMilliseconds;
            }

            if (RequestObject != null && !(RequestObject is string))
            {
                RequestObject = RequestObject.ToSafeJson();
            }

            if (ResponseObject != null && !(ResponseObject is string))
            {
                ResponseObject = ResponseObject.ToSafeJson();
            }

            using (LogContext.PushProperty(nameof(Action), Action))
            using (LogContext.PushProperty(nameof(RetailerId), RetailerId))
            {
                LogContext.PushProperty(nameof(EcommerceRequest), !string.IsNullOrEmpty(EcommerceRequest) ? EcommerceRequest : "Demo");
                Logger.LogError(ex, this.ToJson());
            }
        }

        public void LogWarning(string message)
        {
            Timer.Stop();
            TimerTotalTime.Stop();
            Duration = Timer.ElapsedMilliseconds;

            if (RequestObject != null && !(RequestObject is string))
            {
                RequestObject = RequestObject.ToSafeJson();
            }

            if (ResponseObject != null && !(ResponseObject is string))
            {
                ResponseObject = ResponseObject.ToSafeJson();
            }

            Description = message;

            using (LogContext.PushProperty(nameof(Action), Action))
            using (LogContext.PushProperty(nameof(RetailerId), RetailerId))
            {
                LogContext.PushProperty(nameof(EcommerceRequest), !string.IsNullOrEmpty(EcommerceRequest) ? EcommerceRequest : "Demo");
                Logger.LogWarning(this.ToJson());
            }
        }

        public void LogErrorOrWarning(Exception ex, int max, int current)
        {
            if (max == current)
            {
                LogError(ex);
            }
            else
            {
                LogWarning(ex.Message);
            }
        }

        public LogObjectMicrosoftExtension Clone(string action)
        {
            var logger = new LogObjectMicrosoftExtension(Logger, Id)
            {
                Action = action,
                OmniChannelId = OmniChannelId,
                RetailerId = RetailerId,
                BranchId = BranchId,
                ChannelType = ChannelType
            };
            return logger;
        }

        public void Update(string orderId, byte channelType, string channelTypeCode, long groupId)
        {
            OrderId = orderId;
            ChannelType = channelType;
            ChannelTypeCode = channelTypeCode;
            GroupId = groupId;
        }
    }
}

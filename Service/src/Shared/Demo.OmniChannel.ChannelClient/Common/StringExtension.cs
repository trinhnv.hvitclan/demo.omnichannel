﻿using System;
using Newtonsoft.Json;

namespace Demo.OmniChannel.ChannelClient.Common
{
    public static class StringExtension
    {
        /// <summary>
        /// Check input string is valid to deserialize to JSON
        /// </summary>
        /// <param name="str"> Input string</param>
        /// <returns></returns>
        public static bool IsValidJson<T>(this String str)
        {
            if (string.IsNullOrWhiteSpace(str)) return false;
            if ((str.StartsWith("{") && str.EndsWith("}")) || //For object
                (str.StartsWith("[") && str.EndsWith("]"))) //For array
            {
                try
                {
                    JsonConvert.DeserializeObject<T>(str);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }

            return false;
        }

        public static bool IsValidStringNumber(string s)
        {
            if (!string.IsNullOrWhiteSpace(s) && s != "0")
            {
                return true;
            }
            return false;
        }
    }
}

﻿
using System.Collections.Generic;
using Demo.OmniChannel.ChannelClient.Models;

namespace Demo.OmniChannel.ChannelClient.Common
{
    public class BaseConfig
    {
        public bool Enable { get; set; }

        private List<int> _includeRetail;
        
        private List<PlatformConfig> _includePlatform;

        public List<int> IncludeRetail
        {
            get => _includeRetail ?? new List<int>();
            set => _includeRetail = value;
        }
        
        public List<PlatformConfig> IncludePlatform
        {
            get => _includePlatform ?? new List<PlatformConfig>();
            set => _includePlatform = value;
        }

        private List<int> _excludeZone;

        public List<int> ExcludeZone
        {
            get => _excludeZone ?? new List<int>();
            set => _excludeZone = value;
        }

        private List<int> _excludeRetailer;

        public List<int> ExcludeRetailer
        {
            get => _excludeRetailer ?? new List<int>();
            set => _excludeRetailer = value;
        }

        private List<int> _includeZone;

        public List<int> IncludeZone
        {
            get => _includeZone ?? new List<int>();
            set => _includeZone = value;
        }

        public bool IsValid(int groupId, int retailerId)
        {
            if (!Enable)
                return false;
            if (ExcludeZone.Contains(groupId))
                return false;
            if (ExcludeRetailer.Contains(retailerId))
                return false;
            if (IncludeZone.Count > 0 && !IncludeZone.Contains(groupId))
                return false;
            if (IncludeZone.Count > 0 && IncludeZone.Contains(groupId))
                return true;
            return IncludeRetail.Count <= 0 || IncludeRetail.Contains(retailerId);
        }

        private List<int> _includeGroup;

        public List<int> IncludeGroup
        {
            get => _includeGroup ?? new List<int>();
            set => _includeGroup = value;
        }
    }
}
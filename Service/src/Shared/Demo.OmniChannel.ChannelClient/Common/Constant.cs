﻿using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.Common
{
    public static class ShopeeProductStatus
    {
        public const string Nomal = "NORMAL";
        public const string Banned = "BANNED";
        public const string Deleted = "DELETED";
        public const string Unlist = "UNLIST";
    }

    public static class ShopeeProfileMessage
    {
        public const string NoPermission = "No permission. Please inform seller to complete the Seller Registration on Shopee Seller Center first, then this shop can call for this API";
    }

    public class ShopeeMessage
    {
        protected ShopeeMessage()
        {

        }
        public static readonly string ShopeeSurchargeMessage = "Thu lệch vận chuyển";

    }

    public class ShopeeStatus
    {
        public const string Unpaid = "UNPAID";
        public const string ReadyToShip = "READY_TO_SHIP";
        public const string ToConfirmReceive = "TO_CONFIRM_RECEIVE";
        public const string RetryShip = "RETRY_SHIP";
        public const string Shipped = "SHIPPED";
        public const string Completed = "COMPLETED";
        public const string InCancel = "IN_CANCEL";
        public const string Cancelled = "CANCELLED";
        public const string ToReturn = "TO_RETURN";
        public const string InvoicePending = "INVOICE_PENDING";
        public const string Processed = "PROCESSED";
    }

    public class ShopeeDeliveryStatus
    {
        public const string LogisticNotStart = "LOGISTICS_NOT_START";
        public const string LogisticRequestCreated = "LOGISTICS_REQUEST_CREATED";
        public const string LogisticPickupDone = "LOGISTICS_PICKUP_DONE";
        public const string LogisticPickupRetry = "LOGISTICS_PICKUP_RETRY";
        public const string LogisticPickupFailed = "LOGISTICS_PICKUP_FAILED";
        public const string LogisticRequestCanceled = "LOGISTICS_REQUEST_CANCELED";
        public const string LogisticCodRejected = "LOGISTICS_COD_REJECTED";
        public const string LogisticReady = "LOGISTICS_READY";
        public const string LogisticInvalid = "LOGISTICS_INVALID";
        public const string LogisticLost = "LOGISTICS_LOST";
        public const string LogisticsDeliveryDone = "LOGISTICS_DELIVERY_DONE";
        public const string LogisticsDeliveryFailed = "LOGISTICS_DELIVERY_FAILED";
        public const string LogisticsUnknown = "LOGISTICS_UNKNOWN";
    }
    public class LazadaStatus
    {
        public const string Unpaid = "unpaid";
        public const string Pending = "pending";
        public const string ReadyToShip = "ready_to_ship";
        public const string Shipped = "shipped";
        public const string Failed = "failed";
        public const string Delivered = "delivered";
        public const string Cancelled = "canceled";
        public const string Returned3PL = "INFO_ST_DOMESTIC_RETURN_WITH_LAST_MILE_3PL"; //TMDT-55
        public const string BackToShipper = "INFO_ST_DOMESTIC_BACK_TO_SHIPPER"; //TMDT-70
        public const string Repacked = "repacked";
        public const string Packed = "packed";
        public const string ReadyToShipPending = "ready_to_ship_pending";
        public const string FailedDelivery = "failed_delivery";
        public const string ShippedBack = "shipped_back";
        public const string ShippedBackSuccess = "shipped_back_success";
        public const string ShippedBackFailed = "shipped_back_failed";
        public const string PackageScrapped = "package_scrapped";
        public const string LostBy3PL = "lost_by_3pl";
        public const string DamagedBy3PL = "damaged_by_3pl";
    }

    public static class ShopeeLogicsticName
    {
        public const string VeryQuick = "Hỏa Tốc";
        public const string Quick = "Nhanh";
        public const string Thrifty = "Tiết kiệm";
    }

    public class LazadaShippingType
    {
        public const string Dropshipping = "dropshipping";
    }

    public class LazadaShipmentProvider
    {
        public const string SellerDelivery = "Seller Delivery";
    }

    public class TikiStatus
    {
        public const string Queueing = "queueing";
        public const string Canceled = "canceled";
        public const string Processing = "processing";
        public const string FinishedPacking = "finished_packing";
        public const string WaitingPayment = "waiting_payment";
        public const string SuccessfulDelivery = "successful_delivery";
        public const string HandoverToPartner = "handover_to_partner";
        public const string Closed = "closed";
        public const string Packaging = "packaging";
        public const string Picking = "picking";
        public const string Shipping = "shipping";
        public const string Paid = "paid";
        public const string Delivered = "delivered";
        public const string ReadyToShip = "ready_to_ship";
        public const string Returned = "returned";
        public const string Complete = "complete";
        public const string SellerConfirmed = "seller_confirmed";
        public const string SellerCanceled = "seller_canceled";
    }

    public class TikiSellerStatus
    {
        public const string Draft = "draft";
        public const string Waiting = "waiting";
        public const string SellerSupplementing = "seller_supplementing";
        public const string KamRejected = "kam_rejected";
        public const string Completed = "completed";
    }

    public class SendoOrderStatusName
    {
        public const string New = "New";
        public const string Proccessing = "Proccessing";
        public const string Shipping = "Shipping";
        public const string Pod = "POD";
        public const string Completed = "Completed";
        public const string Closed = "Closed";
        public const string Cancelled = "Cancelled";
        public const string Returning = "returning";
        public const string Returned = "Returned";
    }

    public class WarehouseCode
    {
        public const string DropShipping = "dropshipping";
    }

    public class SendoProductStatus
    {
        public const int Draft = 0;
        public const int Submitted = 1; //Chờ duyệt
        public const int Approved = 2;  //Đã duyệt
        public const int Rejected = 3;  //Từ chối
        public const int Cancelled = 4; //Đã hủy
        public const int Deleted = 5;   //Đã xóa
    }

    public static class ShopeeShipingCarrier
    {
        public const string SellerShipping = "Người bán tự vận chuyển";
    }

    #region Tiktok
    public static class TiktokProductStatus
    {
        //1-draft、2-pending、3-failed、4-live、5-seller_deactivated、6-platform_deactivated、7-freeze 、8-deleted
        public const int Live = 4; //4-live
        public const int Deleted = 8;
    }

    public static class TiktokErrorCode
    {
        public const string RefreshTokenExpired = "36004005";
        public const string TokenExpired = "105001";
        public const string SellerIsInactivated = "12019004";
        public const string AuthorizationIsCancelled = "36004006";

    }
    public class TiktokOrderStatus
    {
        public const int AwaitingCollection = 112;
        public const int Unpaid = 100;
        public const int AwaitingShipment = 111;
        public const int InTransit = 121;
        public const int Delivered = 122;
        public const int Completed = 130;
        public const int Cancelled = 140;
    }

    public static class TiktokPaymentStatus
    {
        public const int NotPay = 1;
    }

    public class TiktokCancelMessage
    {
        public const string CanncelReason = "Giao gói hàng thất bại";
        public const string CanncelUser = "SYSTEM";
        public const string CanncelReasonEn = "Package delivery failed";
    }

    public static class TiktokMessage
    {
        public const string TiktokSurchargeMessage = "Thu lệch vận chuyển";
    }

    public static class TikTokReverseType
    {
        public const int CANCEL = 1;
        public const int REFUND_ONLY = 2;
        public const int RETURN_AND_REFUND = 3;
        public const int REQUEST_CANCEL = 4;
    }

    public class TiktokPaymentMethod
    {
        public const string Cod = "CASH_ON_DELIVERY";
    }
    public static class TiktokReverseOrderStatus
    {
        public const int AFTERSALE_APPLYING = 1;
        public const int AFTERSALE_REJECT_APPLICATION = 2;
        public const int AFTERSALE_RETURNING = 3;
        public const int AFTERSALE_BUYER_SHIPPED = 4;
        public const int AFTERSALE_SELLER_REJECT_RECEIVE = 5;
        public const int AFTERSALE_SUCCESS = 50;
        public const int CANCEL_SUCCESS = 51;
        public const int CLOSED = 99;
        public const int COMPLETE = 100;
    }
    #endregion

    #region Lazada
    public static class LazadaDetailType
    {
        public const string ReadyTo = "ready_to";
    }

    public static class LazadaErrorCode
    {
        public const string IllegalAccessToken = "IllegalAccessToken";
        public const string IllegalRefreshToken = "IllegalRefreshToken";
    }

    #endregion

}
﻿using System;
using System.Net;
using Demo.OmniChannel.ChannelClient.Impls;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Deserializers;
using ServiceStack.Logging;

namespace Demo.OmniChannel.ChannelClient.Common
{
    public class DynamicJsonDeserializer : IDeserializer
    {
        private ILog Log => LogManager.GetLogger(typeof(DynamicJsonDeserializer));

        public T Deserialize<T>(IRestResponse response)
        {
            try
            {
                var settingFomart = new JsonSerializerSettings
                {
                    DateFormatHandling = DateFormatHandling.MicrosoftDateFormat,
                    DateTimeZoneHandling = DateTimeZoneHandling.Local
                };
                return JsonConvert.DeserializeObject<T>(response.Content, settingFomart);
            }
            catch (Exception ex)
            {
                var getOrdersLog = new LogObject(Log, Guid.NewGuid())
                {
                    Action = "DynamicJsonDeserializer",
                    RequestObject = $"Status: {(int)HttpStatusCode.InternalServerError}",
                    ResponseObject = ex.Message
                };
                getOrdersLog.LogError(ex);
                throw;
            }
        }

        public string RootElement { get; set; }
        public string Namespace { get; set; }
        public string DateFormat { get; set; }
    }
}
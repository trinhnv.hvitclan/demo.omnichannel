﻿using Serilog;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Demo.OmniChannel.ChannelClient.Common
{
    public class SeriLogObject
    {
        public SeriLogObject(Guid id)
        {
            Id = id;
            Timer = Stopwatch.StartNew();
            StartTime = DateTime.Now.ToString("o");
        }
        public Guid Id { get; set; }
        public string Action { get; set; }
        public long Duration { get; set; }
        public int? RetailerId { get; set; }
        public int? BranchId { get; set; }
        public string Description { get; set; }
        public long? OmniChannelId { get; set; }
        public object RequestObject { get; set; }
        public object ResponseObject { get; set; }
        public int? TotalItem { get; set; }
        private Stopwatch Timer { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public List<SeriLogStage> SeriLogStages { get; set; } = new List<SeriLogStage>();

        public void LogInfo()
        {
            Timer.Stop();
            EndTime = DateTime.Now.ToString("o");
            Duration = Timer.ElapsedMilliseconds;

            if (RequestObject != null && !(RequestObject is string))
            {
                RequestObject = RequestObject.ToSafeJson();
            }

            if (ResponseObject != null && !(ResponseObject is string))
            {
                ResponseObject = ResponseObject.ToSafeJson();
            }

            Log.Information(this.ToSafeJson());
        }

        public void LogWarning()
        {
            Timer.Stop();
            EndTime = DateTime.Now.ToString("o");
            Duration = Timer.ElapsedMilliseconds;

            if (RequestObject != null && !(RequestObject is string))
            {
                RequestObject = RequestObject.ToSafeJson();
            }

            if (ResponseObject != null && !(ResponseObject is string))
            {
                ResponseObject = ResponseObject.ToSafeJson();
            }

            Log.Warning(this.ToSafeJson());
        }

        public void LogError(Exception ex = null)
        {
            Timer.Stop();
            EndTime = DateTime.Now.ToString("o");
            Duration = Timer.ElapsedMilliseconds;

            if (RequestObject != null && !(RequestObject is string))
            {
                RequestObject = RequestObject.ToSafeJson();
            }

            if (ResponseObject != null && !(ResponseObject is string))
            {
                ResponseObject = ResponseObject.ToSafeJson();
            }

            if (ex != null)
            {
                Description = string.IsNullOrEmpty(Description) ? ex.StackTrace : Description;
                Log.Error(ex,this.ToSafeJson());
            }
            else
            {
                Log.Error(this.ToSafeJson());
            }
        }
    }

    public class SeriLogStage
    {
        public string Name { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public long Duration { get; set; }
        private Stopwatch Timer { get; set; }
        public string Description { get; set; }
        public SeriLogStage(string name)
        {
            Name = name;
            Timer = Stopwatch.StartNew();
            StartTime = DateTime.Now.ToString("o");
        }

        public void EndStage()
        {
            EndTime = DateTime.Now.ToString("o");
            Timer.Stop();
            Duration = Timer.ElapsedMilliseconds;
        }
    }
}

﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.RequestDTO;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.ShareKernel.Dtos;
using Demo.OmniChannel.ShareKernel.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProductDetailResponse = Demo.OmniChannel.ChannelClient.RequestDTO.ProductDetailResponse;

namespace Demo.OmniChannel.ChannelClient.Interfaces
{
    public interface IBaseClient
    {
        /// <summary>
        /// Get thông tin đơn vị hành chính
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        Task<List<Models.Logistics>> GetLogisticsByShopId(int retailerId, long channelId, ChannelAuth auth, Guid logId, Platform platform);
        Task<RefreshTokenResponse> RefreshAccessToken(string refreshToken, long channelId, Guid logId,
            ChannelAuth auth, Platform platform);

        Task<ShopInfoResponseV2> GetShopInfo(ChannelAuth auth, KvInternalContext context, bool isOnlyGetInfo = false,
            Platform platform = null);

        Task<ProductListResponse> GetListProduct(ChannelAuth auth, long channelId, Guid logId, bool isFirstSync,
            DateTime? updateTimeFrom = null, KvInternalContext kvContext = null, Platform platform = null);

        Task<(bool, string)> SyncOnHand(
            int retailerId, string kvProductSku, long channelId, Guid logId,
            ChannelAuth auth, string itemId, string itemSku, double onHand, byte productType,
            LogObjectMicrosoftExtension loggerExtension, string parentProductId = null, Platform platform = null);

        Task<(bool, string)> SyncMultiOnHand(
            int retailerId, long channelId, Guid logId, ChannelAuth auth, List<MultiProductItem> productItems, 
            LogObjectMicrosoftExtension loggerExtension, byte productType, Platform platform);

        Task<(bool, string)> SyncPrice(int retailerId, string kvProductSku, long channelId, Guid logId,
            ChannelAuth auth, string itemId, string itemSku, decimal? basePrice
            , byte productType, string parentItemId, LogObjectMicrosoftExtension loggerExtension,
            decimal? salePrice = null, DateTime? startSaleDateTime = null, DateTime? endSaleDateTime = null,
            Platform platform = null);

        Task<(bool, string, int?)> SyncMultiPrice(int retailerId, long channelId, Guid logId, ChannelAuth auth,
            List<MultiProductItem> productItems, byte productType, LogObjectMicrosoftExtension loggerExtension,
            Platform platform);

        Task<ProductDetailResponse> GetProductDetail(ChannelAuth auth, long channelId, Guid logId, object productId,
            object parentProductId, string productSku, Platform platform);

        Task<(bool, List<CreateOrderRequest>)> GetOrders(ChannelAuth auth, OrderRequest request,
            KvInternalContext context, LogObjectMicrosoftExtension logInfo, Platform platform);
        Task<OrderDetailGetResponse> GetOrderDetail(int retailerId, long channelId, ChannelAuth auth,
            CreateOrderRequest request, LogObjectMicrosoftExtension logInfo, Platform platform,
            OmniChannelSettingObject settings = null);

        Task<(bool, bool, bool, List<KvInvoice>)> GetInvoiceDetail(int retailerId, long channelId, Guid logId,
            ChannelAuth auth, CreateInvoiceRequest request, LogObjectMicrosoftExtension loggerExtension,
            Platform platform, OmniChannelSettingObject settings);

        Task<(byte?, string, string)> GetLogisticsStatus(int retailerId, long channelId, LogObjectMicrosoftExtension logInfo, string orderId,
            ChannelAuth auth, bool isGetOrderStatus = false, Platform platform = null);

        Task<(byte?, byte?, byte?)> GetOrderStatus(int retailerId, long channelId, Guid logId, string orderId,
            ChannelAuth auth, LogObjectMicrosoftExtension loggerExtension, bool isGetDeliveryStatus = false,
            Platform platform = null, OmniChannelSettingObject settings = null);
        Task<ShopInfoResponseV2> GetShopStatus(string accessToken);
        Task EnableUpdateProduct(string accessToken);
        Task<TokenResponse> CreateToken(string code, ChannelAuth auth, Platform platform);

        Task<CustomerResponse> GetCustomerByOrderId(int retailerId, long channelId, string orderId, ChannelAuth auth,
            Guid logId);

        Task<CustomerResponse> GetCustomerByOrderId(int retailerId, long channelId, string orderId, ChannelAuth auth,
            Guid logId, LogObjectMicrosoftExtension loggerExtension, Platform platform);

        Task<List<KvInvoice>> GetInvoiceDetailByOrderId(int retailerId, Guid logId, ChannelAuth auth, string orderId,
            long channelId, LogObjectMicrosoftExtension loggerExtension, Platform platform);

        Task<List<ShopeeAttribute>> GetAttributeByShopId(int retailerId, long channelId, ChannelAuth auth, Guid logId,
            long categoryId, Platform platform);

        Task<ShopeeProductResponse> PushProductToChannel(int retailerId, long channelId, ChannelAuth auth, Guid logId,
            ShopeeProduct req, Platform platform, string kvProductAttributeStr);

        Task<CategoriesResp> GetCategories(int retailerId, long channelId, ChannelAuth auth, Guid logId, Platform platform);

        Task<ShopeeUploadImgResponse> UploadImg(ChannelAuth auth, int retailerId, long shopId, List<Demo.OmniChannel.ShareKernel.Dtos.Image> images,
            Guid logId, Platform platform);

        Task<ProductListResponse> GetListDeletedProduct(ChannelAuth auth, long channelId, Guid logId, bool isFirstSync,
            KvInternalContext kvContext = null, DateTime? updateTimeFrom = null);

        Task<List<OrderDetail>> GetListOrderDetail(ChannelAuth auth,
            string[] orderSnList, LogObjectMicrosoftExtension logInfo, Platform platform);

        Task<KvOrder> GenListKvOrder(long shopId, int? saleChannelId, int branchId, long userId, int retailerId,
            Models.OrderDetail shopeeOrder, OmniChannelSettingObject settings, LogObjectMicrosoftExtension logInfo,
            Platform platform);

        List<KvInvoice> GenKvInvoices(CreateInvoiceRequest request);

        Task<(bool, List<Transaction>)> GetListTransaction(ChannelAuth auth, DateTime createFrom, DateTime createTo,
            LogObjectMicrosoftExtension log, Platform platform);

        Task<(bool ,List<FinanceTransaction>)> GetFinanceTransactions(string token, long channelId, string orderSn,
            DateTime? startTime, DateTime? endTime, LogObjectMicrosoftExtension logInfo, bool isBreakIfMaximum);

        Task<SellerWareHouseV2Response> GetTikiWarehouse(string accessToken);

    }
}

﻿using Demo.OmniChannel.ChannelClient.Models;

namespace Demo.OmniChannel.ChannelClient.Interfaces.NewBase
{
    public interface ITikiBusinessClient 
    {
        KvOrder CalFeeKvOrder(KvOrder kvOrder, TikiOrderV2 tikiOrder);
    }
}

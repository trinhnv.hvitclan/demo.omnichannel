﻿using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ShareKernel.Models;

namespace Demo.OmniChannel.ChannelClient.Interfaces.NewBase
{
    public interface IShopeeBusinessClient : IChannelBusinessClient
    {
        byte? ConvertOrderDetailToKvOrderStatus(string shopeeStatus, ShopeeTrackingInfoV2Response trackingInfo = null);
        byte? ConvertOrderDetailToKvInvoiceStatus(string shopeeStatus, string logisticsStatus);
        byte? ConvertTrackingInfoToKvDeliveryStatus(string logisticsStatus);
        string GetKvOrderDescription(OrderDetailV2 order);
        KvOrder ConvertPaymentEscrowToKvOrder(KvOrder kvOrder, ShopeePaymentEscrowDetailResponse paymentEscrowDetail,
            int retailerId, int groupId);

        KvOrder GenKvOrderFromOrderDetailV2(OrderDetailV2 shopeeOrder, ShopeeTrackingInfoV2Response trackingInfo,
            OmniChannelSettingObject settings, int branchId, long userId, int? saleChannelId);

        KvOrder GenKvOrderV2WithLogistics(OrderDetailV2 shopeeOrder, ShopeePaymentEscrowDetailResponse paymentEscrowDetail,
            ShopeeTrackingInfoV2Response trackingInfo, OmniChannelSettingObject settings,
            int branchId, long userId, int retailerId, int groupId);
    }
}

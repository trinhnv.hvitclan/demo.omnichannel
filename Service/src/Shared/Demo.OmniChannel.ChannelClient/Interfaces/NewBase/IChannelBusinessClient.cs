﻿using Demo.OmniChannel.ChannelClient.Models;

namespace Demo.OmniChannel.ChannelClient.Interfaces.NewBase
{
    public interface IChannelBusinessClient
    {
        KvOrder GenListKvOrder(int branchId, long userId, int retailerId, int groupId, OmniChannelSettingObject settings, string orderRawInfo);
    }
}

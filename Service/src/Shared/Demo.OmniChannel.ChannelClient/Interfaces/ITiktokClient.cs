﻿using Demo.OmniChannel.ChannelClient.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.RequestDTO;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Tiktok;

namespace Demo.OmniChannel.ChannelClient.Interfaces
{
    public interface ITiktokClient : IBaseClient
    {
        Task<(bool, string, string)> SyncOnHandWithGroupParrent(ChannelAuth auth,
          string parentItemId, List<MultiProductItem> productItems,
          LogObjectMicrosoftExtension logger,
          Platform platform);

        Task<TiktokShippingInfo> GetShippingInfo(ChannelAuth auth, Platform platform, string orderId);

        Task<TiktokReverseOrderListResponse> GetReverseOrderList(ChannelAuth auth, Platform platform, string orderId);
    }
}

﻿using Demo.OmniChannel.ChannelClient.RequestDTO;
using Demo.OmniChannel.ChannelClient.ResponseDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.OmniChannel.ChannelClient.Interfaces
{
    public interface ISendoClient
    {
        /// <summary>
        /// Get thông tin đơn vị hành chính từ Sendo
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        Task<List<SendoLocation>> GetSendoLocation(string accessToken);
    }
}

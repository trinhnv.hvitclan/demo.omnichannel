﻿using System;
using Demo.OmniChannel.ChannelClient.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.RequestDTO;
using Demo.OmniChannel.ShareKernel.Models;

namespace Demo.OmniChannel.ChannelClient.Interfaces
{
    public interface IShopeeClientV2: IBaseClient
    {
        Task<ShopeeModelListReponseV2> GetModelList(long itemId, long channelId, Guid logId,
              ChannelAuth auth, Platform platform);

        Task<(bool, string, string)> SyncPriceWithGroupParrent(ChannelAuth auth,
            string parentItemId, List<MultiProductItem> productItems,
            LogObjectMicrosoftExtension logger,
            Platform platform);

        Task<(bool, string, string)> SyncOnHandWithGroupParrent(ChannelAuth auth,
          string parentItemId, List<MultiProductItem> productItems,
          LogObjectMicrosoftExtension logger,
          Platform platform);

        Task<List<ItemListBase>> GetItemListBaseWithPageSize(ChannelAuth auth,
           List<long> itemLst,
           Guid logId,
           Platform platform, int pageSize = 30);
    }
}

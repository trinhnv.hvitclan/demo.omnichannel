﻿using ServiceStack.Configuration;

namespace Demo.OmniChannel.ChannelClient.Interfaces
{
    public interface IChannelClient
    {
        IBaseClient GetClient(byte channelType, IAppSettings settings);
        IBaseClient GetClientV2(byte channelType, IAppSettings settings);
        IBaseClient CreateClient(byte channelType, int groupId, long retailerId);
    }
}

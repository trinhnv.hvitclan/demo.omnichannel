﻿using Demo.OmniChannel.ChannelClient.Models;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Lazada
{
    public class GetOrderTraceResponse : BaseResponse
    {
        [DataMember(Name = "result")]
        public LazadaOrderTrace Result { get; set; }
    }

    public class LazadaOrderTrace
    {
        [DataMember(Name = "success")]
        public string Success { get; set; }
        [DataMember(Name = "module")]
        public List<LazadaModule> Module { get; set; }
    }

    public class LazadaModule
    {
        [DataMember(Name = "warehouse_detail_info")]
        public string WarehouseDetailInfo { get; set; }

        [DataMember(Name = "package_detail_info_list")]
        public List<LazadaPackageDetailInfoList> PackageDetailInfoList { get; set; }
    }

    public class LazadaPackageDetailInfoList
    {
        [DataMember(Name = "logistic_detail_info_list")]
        public List<LazadaLogisticDetailInfoList> LogisticDetailInfoList { get; set; }
    }

    public class LazadaLogisticDetailInfoList
    {
        [DataMember(Name = "detail_type")]
        public string DetailType { get; set; }
        [DataMember(Name = "status_code")]
        public string StatusCode { get; set; }
        [DataMember(Name = "event_time")]
        public long EventTime { get; set; }
    }
}

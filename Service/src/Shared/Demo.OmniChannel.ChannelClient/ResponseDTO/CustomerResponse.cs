﻿namespace Demo.OmniChannel.ChannelClient.RequestDTO
{
    public class CustomerResponse
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
    }
}
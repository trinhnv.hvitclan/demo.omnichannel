﻿using Demo.OmniChannel.ShareKernel.Dtos;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Tiktok
{
    public class GetOrderSettlementsResponse
    {
        [JsonProperty(PropertyName = "settlement_list")]
        public List<TiktokSettlement> SettlementList { get; set; }
    }
}

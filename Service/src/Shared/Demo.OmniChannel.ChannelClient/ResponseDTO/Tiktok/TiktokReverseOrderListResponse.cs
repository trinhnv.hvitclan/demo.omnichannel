﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Tiktok
{
    public class TiktokReverseOrderListResponse
    {
        [JsonProperty(PropertyName = "more")]
        public bool More { get; set; }

        [JsonProperty(PropertyName = "reverse_list")]
        public List<TiktokReverseInfo> ReverseList { get; set; }
    }
    public class TiktokReverseInfo
    {
        [JsonProperty(PropertyName = "order_id")]
        public string OrderId { get; set; }

        [JsonProperty(PropertyName = "reverse_type")]
        public int ReverseType { get; set; }

        [JsonProperty(PropertyName = "reverse_order_id")]
        public string ReverseOrderId { get;set; }

        [JsonProperty(PropertyName = "reverse_status_value")]
        public int ReverseStatusValue { get;set;}

        [JsonProperty(PropertyName = "reverse_update_time")]
        public long ReverseUpdate_time { get; set; }
    }
}

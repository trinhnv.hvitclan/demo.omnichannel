﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Tiktok
{
    public class TiktokShippingInfo
    {
        [JsonProperty(PropertyName = "tracking_info_list")]
        public List<TiktokTrackingInfoList> TrackingInfoList { get; set; }
    }

    public class TiktokTrackingInfoList
    {
        [JsonProperty(PropertyName = "tracking_info")]
        public List<TiktokTrackingInfo> TrackingInfo { get; set; }
    }

    public class TiktokTrackingInfo
    {
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "update_time")]
        public long UpdateTime { get; set; }
    }
}

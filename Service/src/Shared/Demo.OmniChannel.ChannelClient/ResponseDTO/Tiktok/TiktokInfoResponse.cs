﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee
{
    public class TiktokShopListReponse
    {
        [JsonProperty(PropertyName = "shop_list")]
        public List<TiktokInfoResponse> ShopList { get; set; }
    }
    public class TiktokInfoResponse
    {
        [JsonProperty(PropertyName = "region")]
        public string Region { get; set; }

        [JsonProperty(PropertyName = "shop_id")]
        public string ShopId { get; set; }

        [JsonProperty(PropertyName = "shop_name")]
        public string ShopName { get; set; }

        [JsonProperty(PropertyName = "type")]
        public int Type { get; set; }
    }
}
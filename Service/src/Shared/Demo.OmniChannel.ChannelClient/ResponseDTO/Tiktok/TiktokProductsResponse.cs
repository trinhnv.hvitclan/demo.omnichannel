﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee
{
    public class TiktokProductsResponse
    {
        [JsonProperty(PropertyName = "products")]
        public List<TiktokProductResponse> Products { get; set; }

        [JsonProperty(PropertyName = "total")]
        public int Total { get; set; }
    }
    public class TiktokProductResponse
    {
        [JsonProperty(PropertyName = "create_time")]
        public long CreateTime { get; set; }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "sale_regions")]
        public string[] SaleTegions { get; set; }

        [JsonProperty(PropertyName = "status")]
        public int status { get; set; }

        [JsonProperty(PropertyName = "update_time")]
        public long UpdateTime { get; set; }

        [JsonProperty(PropertyName = "skus")]
        public List<TiktokSkus> Skus { get; set; }
    }
    public class TiktokSkus
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "seller_sku")]
        public string SellerSku { get; set; }
    }
   
}
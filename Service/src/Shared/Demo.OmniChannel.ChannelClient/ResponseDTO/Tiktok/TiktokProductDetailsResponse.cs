﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee
{
    public class TiktokProductDetailsResponse
    {
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "product_id")]
        public string ProductId { get; set; }

        [JsonProperty(PropertyName = "product_name")]
        public string ProductName { get; set; }

        [JsonProperty(PropertyName = "product_status")]
        public int ProductStatus { get; set; }

        [JsonProperty(PropertyName = "skus")]
        public List<TiktokProductDetailsSku> Skus { get; set; }

        [JsonProperty(PropertyName = "images")]
        public List<TiktokImage> Images { get; set; }
    }

    public class TiktokProductDetailsSku
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "seller_sku")]
        public string SellerSku { get; set; }

        [JsonProperty(PropertyName = "sales_attributes")]
        public List<TiktokSalesAttributes> SalesAttributes { get; set; }
    }

    public class TiktokSalesAttributes
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "value_id")]
        public string ValueId { get; set; }

        [JsonProperty(PropertyName = "value_name")]
        public string ValueName { get; set; }

        [JsonProperty(PropertyName = "sku_img")]
        public TiktokImage SkuImg { get; set; }
    }

    public class TiktokImage
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "thumb_url_list")]
        public List<string> ThumbUrlList { get; set; }

        [JsonProperty(PropertyName = "url_list")]
        public List<string> UrlList { get; set; }
    }
}
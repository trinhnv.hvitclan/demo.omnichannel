﻿using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee
{
    public class TiktokUpdateStockResponse
    {
        public List<TiktokFailed> FailedSkus { get; set; }
    }

    public class TiktokFailed
    {
        public string FailedWareHouseIds { get; set; }
        public string Id { get; set; }
    }
}
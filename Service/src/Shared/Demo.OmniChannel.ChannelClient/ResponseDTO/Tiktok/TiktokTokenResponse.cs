﻿using Newtonsoft.Json;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Tiktok
{
    public class TiktokTokenResponse 
    {
        [JsonProperty(PropertyName = "access_token")]
        public string AccessToken { get; set; }

        [JsonProperty(PropertyName = "access_token_expire_in")]
        public long AccessTokenExpireIn { get; set; }

        [JsonProperty(PropertyName = "refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty(PropertyName = "refresh_token_expire_in")]
        public long RefreshTokenExpireIn { get; set; }

        [JsonProperty(PropertyName = "open_id")]
        public string OpenId { get; set; }

        [JsonProperty(PropertyName = "seller_name")]
        public string SellerName { get; set; }
    }
}

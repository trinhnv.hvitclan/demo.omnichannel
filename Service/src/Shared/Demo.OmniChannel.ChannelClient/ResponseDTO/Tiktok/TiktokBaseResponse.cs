﻿using Newtonsoft.Json;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Tiktok
{
    public class TiktokBaseResponse<T>
    {
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "request_id")]
        public string RequestId { get; set; }

        [JsonProperty(PropertyName = "data")]
        public T Response { get; set; }

        public bool IsError()
        {
            return !string.IsNullOrEmpty(Code) && !Code.Equals("0");
        }
    }
}

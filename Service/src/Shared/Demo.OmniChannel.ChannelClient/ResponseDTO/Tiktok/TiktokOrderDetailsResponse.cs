﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Tiktok
{
    public class TiktokOrderDetailsResponse
    {
        [JsonProperty(PropertyName = "order_list")]
        public List<TiktokOrderDetail> OrderDetailLst { get; set; }
    }

    public class TiktokOrderDetailResponse
    {
        public TiktokOrderDetail OrderDetail { get; set; }
    }

    public class TiktokOrderDetail
    {
        [JsonProperty(PropertyName = "buyer_message")]
        public string BuyerMessage { get; set; }

        [JsonProperty(PropertyName = "buyer_uid")]
        public string BuyerUid { get; set; }

        [JsonProperty(PropertyName = "cancel_order_sla")]
        public string CancelOrderSla { get; set; }

        [JsonProperty(PropertyName = "cancel_reason")]
        public string CancelReason { get; set; }

        [JsonProperty(PropertyName = "cancel_user")]
        public string CancelUser { get; set; }

        [JsonProperty(PropertyName = "create_time")]
        public long CreateTime { get; set; }

        [JsonProperty(PropertyName = "delivery_option")]
        public string DeliveryOption { get; set; }

        [JsonProperty(PropertyName = "ext_status")]
        public string ExtStatus { get; set; }

        [JsonProperty(PropertyName = "fulfillment_type")]
        public string FulfillmentType { get; set; }

        [JsonProperty(PropertyName = "item_list")]
        public List<TiktokItemList> ItemList { get; set; }

        [JsonProperty(PropertyName = "order_id")]
        public string OrderId { get; set; }

        [JsonProperty(PropertyName = "order_status")]
        public int OrderStatus { get; set; }

        [JsonProperty(PropertyName = "order_status_old")]
        public string OrderStatusOld { get; set; }

        [JsonProperty(PropertyName = "paid_time")]
        public long PaidTime { get; set; }

        [JsonProperty(PropertyName = "payment_info")]
        public TiktokPaymentInfo PaymentInfo { get; set; }

        [JsonProperty(PropertyName = "payment_method")]
        public string PaymentMethod { get; set; }

        [JsonProperty(PropertyName = "receiver_address_updated")]
        public string ReceiverAddressUpdated { get; set; }

        [JsonProperty(PropertyName = "recipient_address")]
        public TiktokRecipientAddress RecipientAddress { get; set; }

        [JsonProperty(PropertyName = "rts_sla")]
        public string RtsSla { get; set; }

        [JsonProperty(PropertyName = "rts_time")]
        public long RtsTime { get; set; }

        [JsonProperty(PropertyName = "seller_note")]
        public string SellerNote { get; set; }

        [JsonProperty(PropertyName = "shipping_provider")]
        public string ShippingProvider { get; set; }

        [JsonProperty(PropertyName = "shipping_provider_id")]
        public string ShippingProviderId { get; set; }

        [JsonProperty(PropertyName = "split_or_combine_tag")]
        public string SplitOrCombineTag { get; set; }

        [JsonProperty(PropertyName = "tracking_number")]
        public string TrackingNumber { get; set; }

        [JsonProperty(PropertyName = "tts_sla")]
        public string TtsSla { get; set; }

        [JsonProperty(PropertyName = "update_time")]
        public string UpdateTime { get; set; }

        [JsonProperty(PropertyName = "settlementList")]
        public List<TiktokSettlement> SettlementList { get; set; }       
    }

    public class TiktokItemList
    {
        [JsonProperty(PropertyName = "product_id")]
        public long ProductId { get; set; }

        [JsonProperty(PropertyName = "product_name")]
        public string ProductName { get; set; }

        [JsonProperty(PropertyName = "quantity")]
        public int Quantity { get; set; }

        [JsonProperty(PropertyName = "seller_sku")]
        public string SellerSku { get; set; }

        [JsonProperty(PropertyName = "sku_cancel_reason")]
        public string SkuCancelReason { get; set; }

        [JsonProperty(PropertyName = "sku_cancel_user")]
        public string SkuCancelUser { get; set; }

        [JsonProperty(PropertyName = "sku_display_status")]
        public string SkuDisplayStatus { get; set; }

        [JsonProperty(PropertyName = "sku_ext_status")]
        public string SkuExtStatus { get; set; }

        [JsonProperty(PropertyName = "sku_id")]
        public long SkuId { get; set; }

        [JsonProperty(PropertyName = "sku_image")]
        public string SkuImage { get; set; }

        [JsonProperty(PropertyName = "sku_name")]
        public string SkuName { get; set; }

        [JsonProperty(PropertyName = "sku_original_price")]
        public long SkuOriginalPrice { get; set; }

        [JsonProperty(PropertyName = "sku_platform_discount")]
        public string SkuPlatformDiscount { get; set; }

        [JsonProperty(PropertyName = "sku_rts_time")]
        public string SkuTtsTime { get; set; }

        [JsonProperty(PropertyName = "sku_sale_price")]
        public string SkuSalePrice { get; set; }

        [JsonProperty(PropertyName = "sku_type")]
        public string SkuType { get; set; }
    }

    public class TiktokPaymentInfo
    {
        [JsonProperty(PropertyName = "currency")]
        public string Currency { get; set; }

        [JsonProperty(PropertyName = "original_shipping_fee")]
        public long OriginalShippingFee { get; set; }

        [JsonProperty(PropertyName = "original_total_product_price")]
        public long OriginalTotalProductPrice { get; set; }

        [JsonProperty(PropertyName = "platform_discount")]
        public long PlatformDiscount { get; set; }

        [JsonProperty(PropertyName = "seller_discount")]
        public long SellerDiscount { get; set; }

        [JsonProperty(PropertyName = "shipping_fee")]
        public long ShippingFee { get; set; }

        [JsonProperty(PropertyName = "shipping_fee_platform_discount")]
        public long ShippingFeePlatformDiscount { get; set; }

        [JsonProperty(PropertyName = "shipping_fee_seller_discount")]
        public long ShippingFeesellerDiscount { get; set; }

        [JsonProperty(PropertyName = "sub_total")]
        public long SubTotal { get; set; }

        [JsonProperty(PropertyName = "taxes")]
        public long Taxes { get; set; }

        [JsonProperty(PropertyName = "total_amount")]
        public long TotalAmount { get; set; }
    }

    public class TiktokRecipientAddress
    {
        [JsonProperty(PropertyName = "address_detail")]
        public string AddressDetail { get; set; }

        [JsonProperty(PropertyName = "city")]
        public string City { get; set; }

        [JsonProperty(PropertyName = "district")]
        public string District { get; set; }

        [JsonProperty(PropertyName = "full_address")]
        public string FullAddress { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "phone")]
        public string Phone { get; set; }

        [JsonProperty(PropertyName = "region")]
        public string Region { get; set; }

        [JsonProperty(PropertyName = "region_code")]
        public string RegionCode { get; set; }

        [JsonProperty(PropertyName = "state")]
        public string State { get; set; }

        [JsonProperty(PropertyName = "town")]
        public string Town { get; set; }

        [JsonProperty(PropertyName = "zipcode")]
        public string Zipcode { get; set; }
    }

    public class TiktokSettlement
    {
        [JsonProperty(PropertyName = "sku_id")]
        public string SkuId { get; set; }


        [JsonProperty(PropertyName = "sku_name")]
        public string SkuName { get; set; }


        [JsonProperty(PropertyName = "product_name")]
        public string ProductName { get; set; }


        [JsonProperty(PropertyName = "sett_status")]
        public int SettStatus { get; set; }


        [JsonProperty(PropertyName = "unique_key")]
        public string UniqueKey { get; set; }


        [JsonProperty(PropertyName = "settlement_info")]
        public TiktokSettlementInfo SettlementInfo { get; set; }
    }

    public class TiktokSettlementInfo
    {

        [JsonProperty(PropertyName = "settlement_time")]
        public long SettlementTime { get; set; }


        [JsonProperty(PropertyName = "currency")]
        public string Currency { get; set; }


        [JsonProperty(PropertyName = "user_pay")]
        public string UserPay { get; set; }


        [JsonProperty(PropertyName = "platform_promotion")]
        public string PlatformPromotion { get; set; }


        [JsonProperty(PropertyName = "shipping_fee_subsidy")]
        public string ShippingFeeSubsidy { get; set; }


        [JsonProperty(PropertyName = "refund")]
        public string Refund { get; set; }


        [JsonProperty(PropertyName = "payment_fee")]
        public string PaymentFee { get; set; }


        [JsonProperty(PropertyName = "platform_commission")]
        public string PlatformCommission { get; set; }


        [JsonProperty(PropertyName = "flat_fee")]
        public string FlatFee { get; set; }


        [JsonProperty(PropertyName = "sales_fee")]
        public string SalesFee { get; set; }


        [JsonProperty(PropertyName = "affiliate_commission")]
        public string AffiliateCommission { get; set; }


        [JsonProperty(PropertyName = "vat")]
        public string Vat { get; set; }


        [JsonProperty(PropertyName = "shipping_fee")]
        public string ShippingFee { get; set; }


        [JsonProperty(PropertyName = "shipping_fee_adjustment")]
        public string ShippingFeeAdjustment { get; set; }


        [JsonProperty(PropertyName = "charge_back")]
        public string ChargeBack { get; set; }


        [JsonProperty(PropertyName = "customer_service_compensation")]
        public string CustomerServiceCompensation { get; set; }


        [JsonProperty(PropertyName = "promotion_adjustment")]
        public string PromotionAdjustment { get; set; }


        [JsonProperty(PropertyName = "other_adjustment")]
        public string OtherAdjustment { get; set; }


        [JsonProperty(PropertyName = "settlement_amount")]
        public string SettlementAmount { get; set; }

    }
}

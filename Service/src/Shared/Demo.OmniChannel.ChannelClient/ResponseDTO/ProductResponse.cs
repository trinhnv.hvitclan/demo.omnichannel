﻿using System.Collections.Generic;
using System.Linq;

namespace Demo.OmniChannel.ChannelClient.RequestDTO
{
    public class ProductListResponse
    {
        public int Total { get; set; }
        public List<Product> Data { get; set; }
        public List<string> RemovedProductIds { get; set; }
        public List<VariationProduct> VariationProductActives { get; set; }
        public List<string> ParentChangeIds { get; set; }
        public bool TokenIsExpiredOrInvalid { get; set; }
        public ProductListResponse()
        {
            Data = new List<Product>();
            RemovedProductIds = new List<string>();
            ParentChangeIds = new List<string>();
            VariationProductActives = new List<VariationProduct>();
        }
    }

    public class Product
    {
        public string ItemId { get; set; }
        public string ItemSku { get; set; }
        public string ItemName { get; set; }
        public List<string> ItemImages { get; set; }
        public byte Type { get; set; }
        public string ParentItemId { get; set; }
        public string Status { get; set; }
        public int OnHand { get; set; }
        public string ErrorMessage { get; set; }
        public decimal BasePrice { get; set; }
        public decimal? SalePrice { get; set; }
        public long? SuperId { get; set; } // Tiki only
    }
    public class MultiProductResponse
    {
        public string RequestId { get; set; }
        public List<Product> Product { get; set; }
        public string ErrorMessage { get; set; }
        public bool IsException { get; set; }
    }

    public class ProductDetailResponse
    {
        public string ItemId { get; set; }
        public List<ProductDetailSkus> ProductDetails { get; set; }
    }

    public class ProductDetailSkus
    {
        public string ItemId { get; set; }
        public string Sku { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public List<string> Images { get; set; }
        public string Description { get; set; }
        public List<object> Variations { get; set; }
    }

    public class VariationProduct
    {
        public List<string> VariationProductActiveIds { get; set; }
        public string ParentId { get; set; }

        public VariationProduct(long parentId, List<long> variationProductActiveIds)
        {
            ParentId = parentId.ToString();
            VariationProductActiveIds = variationProductActiveIds.Select(item => item.ToString()).ToList();
        }

        public VariationProduct()
        {
        }
    }
}
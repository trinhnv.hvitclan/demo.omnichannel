﻿using Newtonsoft.Json;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee
{
    public class BaseShopeeV2Response<T>
    {
        [JsonProperty(PropertyName = "request_id")]
        public string RequestId { get; set; }


        [JsonProperty(PropertyName = "error")]
        public string Error { get; set; }


        [JsonProperty(PropertyName = "message")]
        public string Msg { get; set; }


        [JsonProperty(PropertyName = "warning")]
        public string WarningLst { get; set; }


        [JsonProperty(PropertyName = "response")]
        public T Response { get; set; }
    }
}

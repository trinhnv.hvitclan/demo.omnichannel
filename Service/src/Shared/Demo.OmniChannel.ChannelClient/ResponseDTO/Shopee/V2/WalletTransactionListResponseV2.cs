﻿using Demo.OmniChannel.ChannelClient.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee
{
    public class WalletTransactionListResponseV2
    {
        [JsonProperty(PropertyName = "request_id")]
        public string RequestId { get; set; }
        [JsonProperty(PropertyName = "error")]
        public string Error { get; set; }
        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }
        [JsonProperty(PropertyName = "response")]
        public TransactionListReponse Response { get; set; }
    }

    public class TransactionListReponse
    {
        [JsonProperty(PropertyName = "more")]
        public bool HasMore { get; set; }
        [JsonProperty(PropertyName = "transaction_list")]
        public List<Transaction> TransactionList { get; set; }
    }

    public class PayOrder
    {
        [JsonProperty(PropertyName = "ordersn")]
        public string OrderSn { get; set; }
        [JsonProperty(PropertyName = "shop_name")]
        public string ShopName { get; set; }
    }
}

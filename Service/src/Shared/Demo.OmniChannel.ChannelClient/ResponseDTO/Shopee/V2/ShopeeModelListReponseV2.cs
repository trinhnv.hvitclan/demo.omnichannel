﻿using Demo.OmniChannel.ShareKernel.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee
{
    public class ShopeeModelListReponseV2 : ShopeeBaseReponseV2
    {
        #pragma warning restore CS0108 // Member hides inherited member; missing new keyword
        [JsonProperty(PropertyName = "response")]
        public ModelListResponse Response { get; set; }
        public long ItemId { get; set; }
    }

    public class ModelListResponse
    {
        [JsonProperty(PropertyName = "tier_variation")]
        public List<TierVariation> TierVariations { get; set; }

        [JsonProperty(PropertyName = "model")]
        public List<Model> Model { get; set; }
    }
    public class TierVariation
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "option_list")]
        public List<TierVariationOption> OptionList { get; set; }
    }

    public class TierVariationOption
    {
        [JsonProperty(PropertyName = "option")]
        public string Option { get; set; }

        [JsonProperty(PropertyName = "image")]
        public ShopeeImage Image { get; set; }
    }
    public class Model
    {
        [JsonProperty(PropertyName = "model_id")]
        public long ModelId { get; set; }

        [JsonProperty(PropertyName = "tier_index")]
        public List<int> TierIndex { get; set; }

        [JsonProperty(PropertyName = "model_sku")]
        public string ModelSku { get; set; }

        [JsonProperty("price_info")]
        public List<PriceInfo> PriceInfo { get; set; }

        [JsonProperty("stock_info_v2")]
        public StockInfoV2 StockInfoV2 { get; set; }
    }

    public class ShopeeImage
    {
        [JsonProperty(PropertyName = "image_id")]
        public string ImageId { get; set; }

        [JsonProperty(PropertyName = "image_url")]
        public string ImageUrl { get; set; }
    }
}

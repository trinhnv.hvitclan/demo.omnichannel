﻿using Newtonsoft.Json;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee
{
    public class ShopeeRefreshTokenReponseV2 : ShopeeBaseReponseV2
    {
        [JsonProperty(PropertyName = "request_id")]
        #pragma warning disable CS0108 // Member hides inherited member; missing new keyword
        public string RequestId { get; set; }
        #pragma warning restore CS0108 // Member hides inherited member; missing new keyword

        [JsonProperty(PropertyName = "access_token")]
        public string AccessToken { get; set; }

        [JsonProperty(PropertyName = "expire_in")]
        public int ExpireIn { get; set; }

        [JsonProperty(PropertyName = "refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty(PropertyName = "shop_id")]
        public long ShopId { get; set; }
    }
}

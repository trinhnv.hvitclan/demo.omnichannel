﻿using Demo.OmniChannel.ChannelClient.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee.V2
{
    public class ShopeeLogisticsGetChannelListReponseV2 : ShopeeBaseReponseV2
    {
        [JsonProperty(PropertyName = "response")]
        public LogisticsChannelList Response { get; set; }
    }

    public class LogisticsChannelList
    {
        [JsonProperty(PropertyName = "logistics_channel_list")]
        public List<LogisticChannelModelResponseV2> LogisticsChannel { get; set; }
    }

    public class LogisticChannelModelResponseV2
    {
        [JsonProperty(PropertyName = "logistics_channel_id")]
        public long LogisticId { get; set; }

        [JsonProperty(PropertyName = "logistics_channel_name")]
        public string LogisticName { get; set; }

        [JsonProperty(PropertyName = "cod_enabled")]
        public bool HasCod { get; set; }

        [JsonProperty(PropertyName = "enabled")]
        public bool Enabled { get; set; }

        [JsonProperty(PropertyName = "fee_type")]
        public string FeeType { get; set; }

        [JsonProperty(PropertyName = "sizes")]
        public List<ShopeeSizes> Sizes { get; set; }

        [JsonProperty(PropertyName = "weight_limit")]
        public WeightLimit WeightLimit { get; set; }

        [JsonProperty(PropertyName = "item_max_dimension")]
        public ItemMaxDimension ItemMaxDimension { get; set; }

        [JsonProperty(PropertyName = "logistics_description")]
        public string LogisticsDescription { get; set; }

        [JsonProperty(PropertyName = "force_enabled")]
        public bool ForceEnabled { get; set; }

        [JsonProperty(PropertyName = "mask_channel_id")]
        public long MaskChannelId { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee
{
    public class ShopUpdateStockResponseV2 : ShopeeBaseReponseV2
    {
        [JsonProperty(PropertyName = "response")]
        public ShopUpdateStockResponseList Response { get; set; }
    }

    public class ShopUpdateStockResponseList
    {
        [JsonProperty(PropertyName = "failure_list")]
        public List<ShopeeUpdateStockFailed> FailureList { get; set; }

        [JsonProperty(PropertyName = "success_list")]
        public List<ShopUpdateStockSuccess> SuccessList { get; set; }
    }
    public class ShopeeUpdateStockFailed
    {
        [JsonProperty(PropertyName = "model_id")]
        public long ModelId { get; set; }

        [JsonProperty(PropertyName = "failed_reason")]
        public string FailedReason { get; set; }
    }
    public class ShopUpdateStockSuccess
    {
        [JsonProperty(PropertyName = "model_id")]
        public long ModelId { get; set; }

        [JsonProperty(PropertyName = "stock")]
        public int Stock { get; set; }
    }
}
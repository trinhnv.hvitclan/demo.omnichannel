﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee
{
    public class ShopUpdatePriceResponseV2 : ShopeeBaseReponseV2
    {
        [JsonProperty(PropertyName = "response")]
        public ShopUpdatePriceResponseList Response { get; set; }
    }

    public class ShopUpdatePriceResponseList
    {
        [JsonProperty(PropertyName = "failure_list")]
        public List<ShopeeUpdatePriceFailed> FailureList { get; set; }

        [JsonProperty(PropertyName = "success_list")]
        public List<ShopUpdatePriceSuccess> SuccessList { get; set; }
    }
    public class ShopeeUpdatePriceFailed
    {
        [JsonProperty(PropertyName = "model_id")]
        public long ModelId { get; set; }

        [JsonProperty(PropertyName = "failed_reason")]
        public string FailedReason { get; set; }
    }
    public class ShopUpdatePriceSuccess
    {
        [JsonProperty(PropertyName = "model_id")]
        public long ModelId { get; set; }

        [JsonProperty(PropertyName = "original_price")]
        public float OriginalPrice { get; set; }
    }
}
﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee.V2
{
    public class ShopeeBrandResponseV2 : ShopeeBaseReponseV2
    {
        [JsonProperty(PropertyName = "response")]
        public ShopeeModelListBrandResponseV2 Response { get; set; }
    }

    public class ShopeeModelListBrandResponseV2
    {
        [JsonProperty(PropertyName = "brand_list")]
        public List<ShopeeModelBrandResponseV2> BrandList { get; set; }
        [JsonProperty(PropertyName = "has_next_page")]
        public bool HasNextPage { get; set; }
        [JsonProperty(PropertyName = "next_offset")]
        public int NextOffSet { get; set; }
        [JsonProperty(PropertyName = "is_mandatory")]
        public bool IsMandatory { get; set; }
        [JsonProperty(PropertyName = "input_type")]
        public string InputType { get; set; }
    }

    public class ShopeeModelBrandResponseV2
    {
        [JsonProperty(PropertyName = "original_brand_name")]
        public string BrandName { get; set; }
        [JsonProperty(PropertyName = "brand_id")]
        public int BrandId { get; set; }
        [JsonProperty(PropertyName = "display_brand_name")]
        public string DisplayName { get; set; }
    }
}

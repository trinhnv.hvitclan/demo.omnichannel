﻿namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee
{
    public class ShopInfoResponseV2
    {
        public long AuthId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string Email { get; set; }
        public string IdentityKey { get; set; }
        public byte Status { get; set; }
        public byte Active { get; set; }
    }
}
﻿using Newtonsoft.Json;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee
{
    public class ShopeeBaseReponseV2
    {
        [JsonProperty(PropertyName = "request_id")]
        public string RequestId { get; set; }
        public string Request { get; set; }
        [JsonProperty(PropertyName = "error")]
        public string Error { get; set; }
        [JsonProperty(PropertyName = "message")]
        public string Msg { get; set; }
        [JsonProperty(PropertyName = "warning")]
        public string Warning { get; set; }
    }
}
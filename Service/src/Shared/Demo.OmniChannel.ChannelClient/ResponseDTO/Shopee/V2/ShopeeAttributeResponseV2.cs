﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee.V2
{
    public class ShopeeAttributeResponseV2 : ShopeeBaseReponseV2
    {
        [JsonProperty(PropertyName = "response")]
        public ShopeeModelListAttributeResponseV2 Response { get; set; }
    }

    public class ShopeeModelListAttributeResponseV2
    {
        [JsonProperty(PropertyName = "attribute_list")]
        public List<ShopeeModelAttributeResponseV2> AttributeList { get; set; }
    }

    public class ShopeeModelAttributeResponseV2
    {
        [JsonProperty(PropertyName = "attribute_id")]
        public long AttributeId { get; set; }

        [JsonProperty(PropertyName = "display_attribute_name")]
        public string AttributeName { get; set; }

        [JsonProperty(PropertyName = "is_mandatory")]
        public bool IsMandatory { get; set; }

        [JsonProperty(PropertyName = "input_validation_type")]
        public string AttributeType { get; set; }

        [JsonProperty(PropertyName = "input_type")]
        public string InputType { get; set; }

        [JsonProperty(PropertyName = "attribute_value_list")]
        public List<ShopeeAttributeValueV2> Values { get; set; }

        [JsonProperty(PropertyName = "format_type")]
        public string FormatType { get; set; }

        [JsonProperty(PropertyName = "attribute_unit")]
        public List<string> UnitList { get; set; }
    }

    public class ShopeeAttributeValueV2
    {
        [JsonProperty(PropertyName = "value_id")]
        public int ValueId { get; set; }
        [JsonProperty(PropertyName = "original_value_name")]
        public string OriginalValue { get; set; }

        [JsonProperty(PropertyName = "display_value_name")]
        public string TranslateValue { get; set; }
    }
}

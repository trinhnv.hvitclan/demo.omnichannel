﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee.V2
{
    public class ShopeeeUploadImageReponseV2 : ShopeeBaseReponseV2
    {
        [JsonProperty(PropertyName = "response")]
        public ShopeeModelResponseV2 Response { get; set; }
    }

    public class ShopeeModelResponseV2
    {
        [JsonProperty(PropertyName = "image_info")]
        public ShopeeImageInfoResponse ImageInfo { get; set; }
    }

    public class ShopeeImageInfoResponse
    {
        [JsonProperty(PropertyName = "image_id")]
        public string ImageId { get; set; }
        [JsonProperty(PropertyName = "image_url_list")]
        public List<ShopeeImageUrlListResponse> ImageUrlList { get; set; }
    }

    public class ShopeeImageUrlListResponse
    {
        [JsonProperty(PropertyName = "image_url_region")]
        public string ImageUrlRegion { get; set; }
        [JsonProperty(PropertyName = "image_url")]
        public string ImageUrl { get; set; }
    }
}

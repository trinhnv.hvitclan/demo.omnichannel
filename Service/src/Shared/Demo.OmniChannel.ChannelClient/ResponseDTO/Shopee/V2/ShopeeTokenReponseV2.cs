﻿using Newtonsoft.Json;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee
{
    public class ShopeeTokenReponseV2
    {
        [JsonProperty(PropertyName = "request_id")]
        public string RequestId { get; set; }

        [JsonProperty(PropertyName = "access_token")]
        public string AccessToken { get; set; }

        [JsonProperty(PropertyName = "expire_in")]
        public long ExpireIn { get; set; }

        [JsonProperty(PropertyName = "refresh_token")]
        public string RefreshToken { get; set; }
    }
}

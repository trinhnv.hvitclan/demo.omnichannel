﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee.V2
{
    public class ShopeeCategoryResponseV2 : ShopeeBaseReponseV2
    {
        [JsonProperty(PropertyName = "response")]
        public ShopeeModelCategoryResponseV2 Response { get; set; }
    }

    public class ShopeeModelCategoryResponseV2
    {
        [JsonProperty(PropertyName = "category_list")]
        public List<ShopeeModelListCategoryResponseV2> CategoryList { get; set; }
    }

    public class ShopeeModelListCategoryResponseV2
    {
        [JsonProperty(PropertyName = "category_id")]
        public long CategoryId { get; set; }
        [JsonProperty(PropertyName = "parent_category_id")]
        public long ParentId { get; set; }
        [JsonProperty(PropertyName = "display_category_name")]
        public string CategoryName { get; set; }
        [JsonProperty(PropertyName = "original_category_name")]
        public string OriginalCategoryName { get; set; }
        [JsonProperty(PropertyName = "has_children")]
        public bool HasChildren { get; set; }
    }
}

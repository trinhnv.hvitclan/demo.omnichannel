﻿
namespace Demo.OmniChannel.ChannelClient.ResponseDTO
{
    public class SendoLocation
    {
        public int DistrictId { get; set; }
        public string FullName { get; set; }
    }
}

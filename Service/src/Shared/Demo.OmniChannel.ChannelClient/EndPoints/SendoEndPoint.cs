﻿namespace Demo.OmniChannel.ChannelClient.EndPoints
{
    public class SendoEndpoint
    {
        public const string Login = "login";
        public const string GetProducts = "product/search";
        public const string GetProductDetail = "product";
        public const string UpdatePriceAndStock = "product/update-by-field-mask";
        public const string GetOrders = "salesorder/search";
        public const string GetOrderDetail = "salesorder";
        public const string GetProvince = "api/address/region";
        public const string GetDistrict = "api/address/district";
        public const string GetWard = "api/address/ward";
    }
}
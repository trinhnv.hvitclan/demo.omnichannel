﻿namespace Demo.OmniChannel.ChannelClient.EndPoints
{
    public class TikiEndPointV2
    {
        public const string GetSellerInfo = "sellers/me";
        public const string UpdateCanUpdateProduct = "sellers/me/updateCanUpdateProduct";
        public const string GetProducts = "products";
        public const string GetOrders = "orders";
        public const string UpdateProduct = "products/updateSku";
        public const string CreateToken = "sc/oauth2/token";
        public const string GetSellerWarehouse = "sellers/me/warehouses";
    }
}
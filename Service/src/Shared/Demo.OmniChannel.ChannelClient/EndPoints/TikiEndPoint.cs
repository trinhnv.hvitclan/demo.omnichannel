﻿namespace Demo.OmniChannel.ChannelClient.EndPoints
{
    public class TikiEndpoint
    {
        public const string GetSellerInfo = "sellers/me";
        public const string UpdateCanUpdateProduct = "sellers/me/updateCanUpdateProduct";
        public const string GetProducts = "products";
        public const string GetOrders = "orders";
        public const string UpdateProduct = "products/updateSku";
        public const string CreateToken = "sc/oauth2/token";
        public const string GetHiddenSkus = "getHiddenSkus";
    }

    public class TikiInclude
    {
        public const string StatusHistories = "status_histories";
        public const string Inventory = "inventory";
        public const string Attributes = "attributes";
        public const string Images = "images";

    }
}
﻿namespace Demo.OmniChannel.ChannelClient.EndPoints
{
    public class LazadaEndpoint
    {
        public const string CreateToken = "/auth/token/create";
        public const string RefeshToken = "/auth/token/refresh";
        public const string UpdatePriceAndStock = "/product/price_quantity/update";
        public const string GetOrders = "/orders/get";
        public const string GetOrderId = "/order/get";
        public const string GetOrderItem = "/order/items/get";
        public const string GetSellerProfile = "/seller/get";
        public const string GetProducts = "/products/get";
        public const string GetProductDetail = "/product/item/get";
        public const string GetFinanceTransactions = "/finance/transaction/details/get";

        #region Logictics
        public const string GetOrderTrace = "/logistic/order/trace";
        #endregion

    }
}
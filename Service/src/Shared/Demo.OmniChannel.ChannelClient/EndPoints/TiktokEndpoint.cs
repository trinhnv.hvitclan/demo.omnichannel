﻿namespace Demo.OmniChannel.ChannelClient.EndPoints
{
    public static class TiktokEndpoint
    {
        #region endpoint auth
        public const string AuthPartner = "oauth/authorize";
        public const string GetToken = "api/token/getAccessToken";
        public const string RefreshToken = "api/token/refreshToken";
        #endregion

        #region endpoint api
        public const string GetProducts = "/api/products/search";
        public const string GetProductDetail = "/api/products/details";
        public const string GetAuthorizedShop = "/api/shop/get_authorized_shop";
        public const string GetOrdersDetail = "/api/orders/detail/query";
        public const string GetOrderSettlements = "/api/finance/order/settlements";
        public const string UpdateStock = "/api/products/stocks";
        public const string UpdatePrice = "/api/products/prices";
        public const string GetShippingInfo = "/api/logistics/ship/get";
        public const string GetReverseOrderList = "/api/reverse/reverse_order/list";

        public const string Webhook = "api/webhooktiktok";
        #endregion
    }
}
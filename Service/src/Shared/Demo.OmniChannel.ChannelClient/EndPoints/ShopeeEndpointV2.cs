﻿namespace Demo.OmniChannel.ChannelClient.EndPoints
{
    public static class  ShopeeEndpointV2
    {
        #region Shop
        public const string GetShopInfo = "shop/get_shop_info";
        #endregion

        #region Product
        public const string GetModelList = "product/get_model_list";
        public const string UpdatePrice = "product/update_price";
        public const string UpdateStock = "product/update_stock";
        public const string PushProductToChannel = "product/add_item";
        public const string DeleteProductById = "product/delete_item";
        public const string AddProductModel = "product/add_model";
        public const string InitTierVariation = "product/init_tier_variation";
        public const string UploadImage = "media_space/upload_image";
        public const string GetCategories = "product/get_category";
        public const string GetAttributes = "product/get_attributes";
        public const string GetBrands = "product/get_brand_list";
        #endregion

        #region Token
        public const string AuthPartner = "shop/auth_partner";
        public const string GetToken = "auth/token/get";
        public const string RefreshToken = "auth/access_token/get";
        #endregion


        public const string GetOrderDetail = "order/get_order_detail";
        public const string GetTrackingInfo = "logistics/get_tracking_info";
        public const string GetPaymentEscrowDetail = "payment/get_escrow_detail";
        public const string GetItemList = "product/get_item_list";
        public const string GetItemBaseInfo = "product/get_item_base_info";

        public const string GetWalletTransactionList = "payment/get_wallet_transaction_list";
        public const string GetLogisticsChannelList = "logistics/get_channel_list";
        public const string ShopeeWebhook = "api/webhookshopee";

    }
}
﻿using System;
using System.Collections.Generic;

namespace Demo.OmniChannel.ShareKernel.Exceptions
{
    public class KvException : Exception
    {
        public KvException(string msg) : base(msg) { }

        public KvException(string msg, Exception exception) : base(msg, exception)
        {
        }
    }

    public class OmniValidateOrderException : Exception
    {
        public string ErrorCode { get; set; }
        public OmniValidateOrderException(string msg) : base(msg) { }

        public OmniValidateOrderException(string errorCode, string msg) : base(msg)
        {
            ErrorCode = errorCode;
        }
    }

    public class OmniException : Exception
    {
        public string ErrorCode { get; set; }
        public OmniException(string msg) : base(msg) { }
        
        public OmniException(string errorCode, string msg) : base(msg)
        {
            ErrorCode = errorCode;
        }
    }

    public class KvValidateException : Exception
    {
        public KvValidateException(string msg) : base(msg) { }
    }

    public class KvValidateLazadaException : Exception
    {
        public KvValidateLazadaException(string msg) : base(msg) { }
    }

    public class KvValidateSendoException : Exception
    {
        public KvValidateSendoException(string msg) : base(msg) { }
    }

    public class KvValidateTikiException : Exception
    {
        public KvValidateTikiException(string msg) : base(msg) { }
    }

    public class KvValidateShopeeException : Exception
    {
        public KvValidateShopeeException(string msg) : base(msg) { }
    }

    public class KvValidateBranchException : Exception
    {
        public KvValidateBranchException(string msg) : base(msg) { }
    }

    public class KvOmniChannelAuthException : Exception
    {
        public KvOmniChannelAuthException(string msg) : base(msg)
        {
        }
    }

    public class KvOmniChannelDataConflictException : Exception
    {
        public KvOmniChannelDataConflictException(string msg) : base(msg)
        {
        }
    }

    public class KvShopeeItemNameException : Exception
    {
        public KvShopeeItemNameException(string msg) : base(msg)
        {
        }

        public KvShopeeItemNameException(string msg, Exception ex): base(msg, ex)
        {
        }
    }

    public class KvTiktokItemNameException : Exception
    {
        public KvTiktokItemNameException(string msg) : base(msg)
        {
        }

        public KvTiktokItemNameException(string msg, Exception ex) : base(msg, ex)
        {
        }
    }

    public class KvTiktokException : Exception
    {
        public KvTiktokException(string msg) : base(msg)
        {
        }

        public KvTiktokException(string msg, Exception ex) : base(msg, ex)
        {
        }
    }

    public class KvShopeeGatewayTimeOutException : Exception
    {
        public KvShopeeGatewayTimeOutException(string msg) : base(msg) { }
    }

    /// <summary>
    /// Message queue exception
    /// </summary>
    public class KvMqException : Exception
    {
        /// <summary>
        /// Message queue exception
        /// </summary>
        public KvMqException(string msg, Exception exception) : base(msg, exception)
        {
        }
    }

    public class ExceptionContextModel
    {
        public Guid LogId { get; set; }
        public long ShopId { get; set; }
        public int RetailerId { get; set; }
        public long ChannelId { get; set; }
        public int BranchId { get; set; }
        public List<object> OrderIds { get; set; }
        public List<object> ProductIds { get; set; }
        public object Auth { get; set; }
        public object Response { get; set; }
    }

    public class ShopeeException : Exception
    {
        public ShopeeException(string msg) : base(msg)
        {
        }

        public ShopeeException(string msg, Exception ex) : base(msg, ex)
        {
        }
    }

    public class ShopeeNeedCompleteRegisterException : Exception
    {
        public ShopeeNeedCompleteRegisterException(string msg) : base(msg)
        {
        }

        public ShopeeNeedCompleteRegisterException(string msg, Exception ex) : base(msg, ex)
        {
        }
    }

    public class TiktokException : Exception
    {
        public TiktokException(string msg) : base(msg)
        {
        }
    }

    public class LazadaException : Exception
    {
        public LazadaException(string msg) : base(msg)
        {
        }
    }

    public class TikiException : Exception
    {
        public TikiException(string msg) : base(msg)
        {
        }
    }

    public class SendoException : Exception
    {
        public SendoException(string msg) : base(msg)
        {
        }
    }

    public class ShopeePushProductException : ShopeeException
    {
        public ShopeePushProductException(string msg) : base(msg)
        {
        }
    }
}
﻿namespace Demo.OmniChannel.ShareKernel.Exceptions
{
    public static class KVMessage
    {
        public const string NotFound = "Dữ liệu này không còn tồn tại trên hệ thống. Vui lòng kiểm tra lại";
        public const string GlobalDeleteSuccess = "Xóa dữ liệu thành công";
        public const string GlobalErrorSummary = "Có lỗi trong quá trình cập nhật dữ liệu.";
        public const string LazadaEmptyBasePriceBook = @"Vui lòng nhập Bảng giá bán trước khi lưu.";
        public const string LazadaEmptyBranch = @"Vui lòng chọn chi nhánh trước khi lưu";
        public const string EcommNotEnableSellAllowOrder = "Chưa bật tính năng đặt hàng";
        public const string EcommNotEnableUseOrderSupplier = "Chưa bật tính năng đặt hàng nhà cung cấp";
        public const string OmniChannelReAuthenAnotherShopFail = "Tài khoản kết nối lại không phải tài khoản {0}. Xin vui lòng thử lại..";

        public const string ShopeeEmptyBasePriceBook = "Vui lòng nhập Bảng giá bán trước khi lưu.";
        public const string ShopeeEmptyBranch = "Vui lòng chọn chi nhánh trước khi lưu";


        public const string SendoRequireInput = "Vui lòng nhập {0}.";
        public const string SendoLimitLenght = "{0} không được vượt quá {1} ký tự.";
        public const string SendoChannelName = "Tên shop.";
        public const string SendoShopkey = "Mã shop.";
        public const string SendoSecretKey = "Mã bảo mật.";

    }
}

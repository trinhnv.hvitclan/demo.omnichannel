﻿using Demo.OmniChannel.ShareKernel.EventBus;
using Newtonsoft.Json;
using System;

namespace Demo.OmniChannel.ShareKernel.EventLogMessage
{
    public class OrderTrackingEvent : IntegrationEvent
    {
        public OrderTrackingEvent(Guid logId) : base(logId)
        {
        }

        public OrderTrackingEvent()
        {
        }
        [JsonProperty(PropertyName = "track_data")]
        public TrackModelEvent TrackModelEvent { get; set; }

    }
    public class TrackModelEvent
    {
        [JsonProperty(PropertyName = "timestamp")]
        public long OrderTimestamp { get; set; }

        [JsonProperty(PropertyName = "order_id_sp")]
        public string OrderSn { get; set; }

        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [JsonProperty(PropertyName = "source")]
        public string Source { get; set; }

        [JsonProperty(PropertyName = "status")]
        public int Status { get; set; }

        [JsonProperty(PropertyName = "errmsg")]
        public string ErrorMessage { get; set; }

        [JsonProperty(PropertyName = "shop_id")]
        public string ShopId { get; set; }

        [JsonProperty(PropertyName = "process_type")]
        public int ProcessType { get; set; }
    }
}

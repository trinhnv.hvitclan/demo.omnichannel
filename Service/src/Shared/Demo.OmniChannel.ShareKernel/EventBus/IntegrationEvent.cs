﻿using System;
using Newtonsoft.Json;

namespace Demo.OmniChannel.ShareKernel.EventBus
{
    public class IntegrationEvent
    {
        public IntegrationEvent()
        {
            Id = Guid.NewGuid();
            CreatedDate = DateTime.UtcNow;
        }

        public IntegrationEvent(Guid logId)
        {
            Id = logId;
            CreatedDate = DateTime.UtcNow;
        }

        [JsonConstructor]
        public IntegrationEvent(
            Guid id,
            DateTime createdDate)
        {
            Id = id;
            CreatedDate = createdDate;
        }

        [JsonProperty]
        public Guid Id { get; private set; }

        [JsonProperty]
        public DateTime CreatedDate { get; private set; }

        [JsonProperty]
        public virtual int RetailerId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Demo.OmniChannel.ShareKernel.Auth
{
    public class ExecutionContext
    {
        public string Id { get; set; }
        public int BranchId { get; set; }
        public SessionUser User { get; set; }
        public IList<string> Permissions { get; set; }
        public IDictionary<string, IList<int>> PermissionMap { get; set; }
        public IList<int> AuthorizedBranchIds { get; set; }
        public int RetailerId { get; set; }
        public string RetailerCode { get; set; }
        public KvGroup Group { get; set; }
        public int GroupId { get; set; }
        public string ShopId { get; set; }
        public long ChannelId { get; set; }
        public Guid LogId { get; set; }
        public long? UserId
        {
            get
            {
                return User?.Id;
            }
        }

        public ExecutionContext(){}

        public ExecutionContext(
            int retailerId,
            string retailerCode,
            int groupId)
        {
            RetailerId = retailerId;
            RetailerCode = retailerCode;
            GroupId = groupId;
        }
    }

    public class KvGroup
    {
        public int Id { get; set; }
    }
}

﻿namespace Demo.OmniChannel.ShareKernel.Dto
{
    public class PosParameterDto
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public int RetailerId { get; set; }
        public bool isActive { get; set; }
        public long CreatedDate { get; set; }
        public long? ExpiredDate { get; set; }
        public int? ParameterType { get; set; }
        public long? StartTrialDate { get; set; }
        public int? BlockUnit { get; set; }
    }
}

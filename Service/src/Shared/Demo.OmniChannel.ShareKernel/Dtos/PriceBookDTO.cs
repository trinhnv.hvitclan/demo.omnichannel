﻿namespace Demo.OmniChannel.ShareKernel.Dto
{
    public class PriceBookDto
    {
        public decimal Price { get; set; }
        public long PriceBookId { get; set; }
    }
}

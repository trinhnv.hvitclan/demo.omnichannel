﻿
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Demo.OmniChannel.ShareKernel.Dtos
{
    public class ShopeeUploadImgReq
    {
        [JsonProperty(PropertyName = "images")]
        public List<string> Images { get; set; }
        [JsonProperty(PropertyName = "partner_id")]
        public long PartnerId { get; set; }
        [JsonProperty(PropertyName = "shopid")]
        public long ShopId { get; set; }
        [JsonProperty(PropertyName = "timestamp")]
        public long TimeStamp { get; set; }
    }
}

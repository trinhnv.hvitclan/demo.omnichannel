﻿using System;

namespace Demo.OmniChannel.ShareKernel.Dto
{
    public class ProductDto
    {
        public long Id { get; set; }
        public Decimal BasePrice { get; set; }
        public bool? isDeleted { get; set; }
        public int RetailerId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public byte[] Revision { get; set; }
        public long? MasterProductId { get; set; }
        public string Code { get; set; }
        public string MasterCode { get; set; }
        public string Name { get; set; }
        public long CategoryId { get; set; }
        public string Description { get; set; }
        public bool AllowsSale { get; set; }
        public byte? ProductType { get; set; }
        public bool HasVariants { get; set; }
        public string Unit { get; set; }
        public double ConversionValue { get; set; }
        public bool? IsLotSerialControl { get; set; }
        public bool? IsRewardPoint { get; set; }
        public string FullName { get; set; }
        public bool? IsRelateToOmniChannel { get; set; }
        public bool isActive { get; set; }
        public double OnHand { get; set; }
        public int BranchId { get; set; }
        public double? Weight { get; set; }
    }
}

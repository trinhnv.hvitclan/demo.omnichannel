﻿
namespace Demo.OmniChannel.ShareKernel.Dto
{
    public class DeliveryInfoDto
    {
        public long DeliveryInfoId { get; set; }
        public bool? IsCurrent { get; set; }
        public byte Status { get; set; }
        public bool? UseDefaultPartner { get; set; }
        public long? DeliveryPackageId { get; set; }
        public string DeliveryCode { get; set; }
        public double? Weight { get; set; }
        public double? Length { get; set; }
        public double? Width { get; set; }
        public double? Height { get; set; }
        public string Receiver { get; set; }
        public string ContactNumber { get; set; }
        public string Address { get; set; }
        public int? LocationId { get; set; }
        public string LocationName { get; set; }
        public string WardName { get; set; }
        public string Comments { get; set; }
        public bool? UsingCod { get; set; }
        public long? WardId { get; set; }
        public double? ConvertedWeight { get; set; }
        public byte? Type { get; set; }
        public long? PartnerId { get; set; }
        public string PartnerCode { get; set; }
        public string PartnerName { get; set; }
    }
}

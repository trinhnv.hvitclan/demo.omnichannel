﻿namespace Demo.OmniChannel.ShareKernel.Dto
{
    public class KvRetailerDto
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string Website { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Code { get; set; }
        public string LogoUrl { get; set; }
        public bool IsActive { get; set; }
        public bool IsAdminActive { get; set; }
        public int GroupId { get; set; }
        public int IndustryId { get; set; }
        public int ContractType { get; set; }
        public long ContractDate { get; set; }
        public long? ExpiryDate { get; set; }
        public int? MaximumSaleChannels { get; set; }
    }
}

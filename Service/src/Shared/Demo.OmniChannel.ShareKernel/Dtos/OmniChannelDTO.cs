﻿using System;
using System.Collections.Generic;

namespace Demo.OmniChannel.ShareKernel.Dto
{
    public class OmniChannelDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long CreateBy { get; set; }
        public long? ModifiedBy { get; set; }
        public byte Type { get; set; }
        public byte Status { get; set; }
        public int BranchId { get; set; }
        public int RetailerId { get; set; }
        public long? PriceBookId { get; set; }
        public long? BasePriceBookId { get; set; }
        public byte SyncOnHandFormula { get; set; }
        public int? SyncOrderFormula { get; set; }
        public bool IsApplyAllFormula { get; set; }
        public bool IsAutoMappingProduct { get; set; }
        public bool? IsAutoDeleteMapping { get; set; }
        public bool? IsDeleted { get; set; }
        public string IdentityKey { get; set; } // Nhận biết shop (Lazada: SellerId. Shopee: ShopId)
        public DateTime? RegisterDate { get; set; }
        public string ExtraKey { get; set; } // Key nhận biết thêm về shop của sàn (Sendo: ShopId)
        public long PlatformId { get; set; }
        public OmniChannelSettingDto OmniChannelSettings { get; set; }
        public OmniChannelAuthDto OmniChannelAuth { get; set; }
        public List<OmniChannelScheduleDto> OmniChannelSchedules { get; set; }
        public List<OmniChannelWareHouseDto> OmniChannelWareHouses { get; set; }
        public long TotalProductConnected { get; set; }
    }
}
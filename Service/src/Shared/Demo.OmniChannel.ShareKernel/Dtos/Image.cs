﻿using Newtonsoft.Json;

namespace Demo.OmniChannel.ShareKernel.Dtos
{
    public class Image
    {
        [JsonProperty(PropertyName = "url")]
        public string Url { get; set; }
        public string ImageId { get; set; }
        public Image() { }

        public Image(string url)
        {
            Url = url;
        }
    }
}

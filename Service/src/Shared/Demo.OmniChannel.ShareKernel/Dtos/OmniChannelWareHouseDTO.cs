﻿using System;

namespace Demo.OmniChannel.ShareKernel.Dto
{
    public class OmniChannelWareHouseDto
    {
        public long Id { get; set; }
        public long WareHouseId { get; set; }
        public long OmniChannelId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long CreateBy { get; set; }
        public long? ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public long RetailerId { get; set; }
        public string StreetInfo { get; set; }
    }
}

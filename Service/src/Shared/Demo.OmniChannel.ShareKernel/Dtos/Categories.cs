﻿
using Newtonsoft.Json;

namespace Demo.OmniChannel.ShareKernel.Dtos
{
    public class Categories
    {
        [JsonProperty(PropertyName = "category_id")]
        public long CategoryId { get; set; }
        [JsonProperty(PropertyName = "parent_id")]
        public long ParentId { get; set; }
        [JsonProperty(PropertyName = "category_name")]
        public string CategoryName { get; set; }
        [JsonProperty(PropertyName = "has_children")]
        public bool HasChildren { get; set; }
        public string DisplayName { get; set; }
    }
}

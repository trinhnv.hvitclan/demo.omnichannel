﻿using System.Collections.Generic;

namespace Demo.OmniChannel.ShareKernel.Dto
{
    public class ProductAndProductAttributeDto
    {
        public ProductDto Product { get; set; }
        public List<ProductAttributeDto> ProductAttributes { get; set; }
    }
}

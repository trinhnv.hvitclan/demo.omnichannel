﻿using System.Collections.Generic;

namespace Demo.OmniChannel.ShareKernel.Dtos
{
    public class CustomerDTO
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ContactNumber { get; set; }
        public string HashLastestNumber { get; set; }
        public string HashNumber { get; set; }
        public string SearchNumber { get; set; }
        public string Avatar { get; set; }
        public List<CustomerSocialDTO> CustomerSocials { get; set; }
    }
    public class CustomerSocialDTO
    {
        public int RetailerId { get; set; }

        public int? BranchId { get; set; }

        public long CustomerId { get; set; }

        public long? UidFacebook { get; set; }

        public long? PsidFacebook { get; set; }

        public long? PageIdFacebook { get; set; }
    }

}

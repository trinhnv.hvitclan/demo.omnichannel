﻿using Newtonsoft.Json;

namespace Demo.OmniChannel.ShareKernel.Dtos
{
    public class Logistics
    {
        [JsonProperty(PropertyName = "logistic_id")]
        public long LogisticId { get; set; }
        [JsonProperty(PropertyName = "enabled")]
        public bool Enable { get; set; }
        [JsonProperty(PropertyName = "size_id")]
        public long SizeId { get; set; }
        [JsonProperty(PropertyName = "shipping_fee")]
        public float ShippingFee { get; set; }
        [JsonProperty(PropertyName = "is_free")]
        public bool IsFree { get; set; }
    }
}

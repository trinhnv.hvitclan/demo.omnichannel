﻿namespace Demo.OmniChannel.ShareKernel.Dto
{
    public class OmniChannelSettingDto
    {
        /// <summary>
        /// Xác nhận vận đơn chuyển hoàn
        /// </summary>
        public bool IsConfirmReturning { get; set; }

        /// <summary>
        /// Xác nhận tự động chọn hàng lô date khi tạo hóa đơn
        /// </summary>
        public bool IsAutoSyncBatchExpire { get; set; }


        /// <summary>
        /// Đơn hàng đồng bộ về theo hai loại (tạo, tạo hoặc cập nhập)
        /// </summary>
        public int SyncOrderFormulaType { get; set; }

        /// <summary>
        /// Ngày bật đồng bộ đơn theo option (tạo, tạo hoặc cập nhập)
        /// </summary>
        public string SyncOrderFormulaDateTime { get; set; }
        /// <summary>
        /// Tự động tạo hàng hóa tại KiotOnline với thông tin tương tự trên sàn và liên kết
        /// </summary>
        public bool IsAutoCreatingProduct { get; set; }

        /// <summary>
        /// https://citigo.atlassian.net/browse/KOL-11934
        /// Nếu là true thì phân bổ
        /// Nếu là false thì không phân bổ, tất cả hàng hóa có tồn như nhau khi đồng bộ
        /// </summary>
        public bool? DistributeMultiMapping { get; set; }
        
        /// <summary>
        /// Đã gửi thông báo hết hạn kết nối chưa
        /// </summary>
        public bool? IsSentExpirationNotification { get; set; }

        /// <summary>
        /// Thời gian bán cho sàn 
        /// "Thời gian đơn tạo trên sàn" & "Thời gian được xác nhận sẵn sàng giao"
        /// </summary>
        public int SyncOrderSaleTimeType { get; set; }
        /// <summary>
        ///  Tự động sao chép hàng hóa khi có hàng hóa mới tạo trên sàn
        /// </summary>
        public bool IsAutoCopyProduct { get; set; }
    }
}

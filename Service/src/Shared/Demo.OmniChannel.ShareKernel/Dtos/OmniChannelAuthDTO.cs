﻿using System;

namespace Demo.OmniChannel.ShareKernel.Dto
{
    public class OmniChannelAuthDto
    {
        public long Id { get; set; }
        public long ShopId { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public int ExpiresIn { get; set; }
        public int RefreshExpiresIn { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long CreateBy { get; set; }
        public long? ModifiedBy { get; set; }
        public long? OmniChannelId { get; set; }
    }
}
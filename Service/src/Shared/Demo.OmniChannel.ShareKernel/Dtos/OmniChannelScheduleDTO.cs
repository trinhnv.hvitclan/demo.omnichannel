﻿using System;

namespace Demo.OmniChannel.ShareKernel.Dto
{
    public class OmniChannelScheduleDto
    {
        public long Id { get; set; }
        public DateTime? NextRun { get; set; }
        public DateTime? LastSync { get; set; }
        public bool IsRunning { get; set; }
        public int Type { get; set; }
        public long OmniChannelId { get; set; }
    }
}
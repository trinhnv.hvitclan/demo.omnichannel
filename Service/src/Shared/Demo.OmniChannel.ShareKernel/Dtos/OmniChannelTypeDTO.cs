﻿namespace Demo.OmniChannel.ShareKernel.Dto
{
    public class OmniChannelTypeDto
    {
        public long Id { get; set; }
        public byte Type { get; set; }
        public string IdentityKey { get; set; }
    }
}

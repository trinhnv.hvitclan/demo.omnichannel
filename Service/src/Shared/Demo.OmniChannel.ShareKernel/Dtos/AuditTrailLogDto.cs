﻿
using System;

namespace Demo.OmniChannel.ShareKernel.Dto
{
    public class AuditTrailLogDto
    {
        public int FunctionId { get; set; }
        public int Action { get; set; }
        public string Content { get; set; }
        public string UserName { get; set; }
        public string IpSource { get; set; }
        public string ClientInfo { get; set; }
        public bool IsMultiCode { get; set; }
        public string KeyWords { get; set; }
        public int? RefId { get; set; }
        public string LotId { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public int BranchId { get; set; }
    }
}

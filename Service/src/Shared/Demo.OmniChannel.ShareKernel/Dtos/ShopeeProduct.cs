﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Demo.OmniChannel.ShareKernel.Dtos
{
    public class ShopeeProduct
    {
        [JsonProperty(PropertyName = "category_id")]
        public long CategoryId { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "item_sku")]
        public string ItemSku { get; set; }
        [JsonProperty(PropertyName = "variation_sku")]
        public string VariationSku { get; set; }
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
        [JsonProperty(PropertyName = "price")]
        public float Price { get; set; }
        [JsonProperty(PropertyName = "stock")]
        public int Stock { get; set; }
        [JsonProperty(PropertyName = "variations")]
        public List<ShopeeProduct> Variations { get; set; }
        [JsonProperty(PropertyName = "attributes")]
        public List<Attribute> Attributes { get; set; }
        [JsonProperty(PropertyName = "images")]
        public List<Image> Images { get; set; }
        [JsonProperty(PropertyName = "logistics")]
        public List<Logistics> Logistics { get; set; }
        [JsonProperty(PropertyName = "weight")]
        public float Weight { get; set; }
        [JsonProperty(PropertyName = "package_width")]
        public int? Width { get; set; }
        [JsonProperty(PropertyName = "package_height")]
        public int? Height { get; set; }
        [JsonProperty(PropertyName = "package_length")]
        public int? Length { get; set; }
        [JsonProperty(PropertyName = "partner_id")]
        public long PartnerId { get; set; }
        [JsonProperty(PropertyName = "shopid")]
        public long ShopId { get; set; }
        [JsonProperty(PropertyName = "timestamp")]
        public long TimeStamp { get; set; }
        public Brand Brand { get; set; }
        public long Id { get; set; }
    }
}

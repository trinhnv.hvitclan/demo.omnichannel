﻿
using Newtonsoft.Json;

namespace Demo.OmniChannel.ShareKernel.Dtos
{
    public class Attribute
    {
        [JsonProperty(PropertyName = "attributes_id")]
        public long AttributeId { get; set; }
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }
        public int ValueId { get; set; }
        public string ValueUnit { get; set; }
    }

    public class Brand
    {
        [JsonProperty(PropertyName = "brand_id")]
        public int BrandId { get; set; }
    }
}

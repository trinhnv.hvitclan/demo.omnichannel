﻿using System;
using System.Collections.Generic;

namespace Demo.OmniChannel.ShareKernel.Dtos
{
    public class OrderDTO
    {
        public long Id { get; set; }
        public long? CustomerId { get; set; }
        public long? CashierId { get; set; }
        public DateTime PurchaseDate { get; set; }
        public string Code { get; set; }
        public string PaymentType { get; set; }
        public int BranchId { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int RetailerId { get; set; }
        public decimal? Discount { get; set; }
        public long? SoldById { get; set; }
        public int? TableId { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public long? ModifiedBy { get; set; }
        public double? DiscountRatio { get; set; }
        public string Extra { get; set; }
        public DateTime? EndPurchaseDate { get; set; }
        public string BookingTitle { get; set; }
        public decimal Total { get; set; }
        public decimal TotalPayment { get; set; }
        public byte? UsingCod { get; set; }
        public decimal? Surcharge { get; set; }
        public long? Point { get; set; }
        public string Uuid { get; set; }
        public int? SaleChannelId { get; set; }
        public bool? IsFavourite { get; set; }
        public bool? FromFbPos { get; set; }
        public List<OrderDetailDTO> OrderDetails { get; set; }
    }
    public class OrderDetailDTO
    {
        public long ProductId { get; set; }
        public double Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
        public double? DiscountRatio { get; set; }
        public string Note { get; set; }
        public long OrderId { get; set; }
    }
}

﻿
namespace Demo.OmniChannel.ShareKernel.Dto
{
    public class ProductImageDto
    {
        public string Url { get; set; }
    }
}

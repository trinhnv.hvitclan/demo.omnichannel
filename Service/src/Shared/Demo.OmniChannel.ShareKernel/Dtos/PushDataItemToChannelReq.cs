﻿using System.Collections.Generic;
using Demo.OmniChannel.ShareKernel.Common;

namespace Demo.OmniChannel.ShareKernel.Dtos
{
    public class PushDataItemToChannelReq
    {
        public ProductDataPushChannel PushDataItemToChannel { get; set; }
        public List<ChannelType> ChannelTypes { get; set; }
    }
}

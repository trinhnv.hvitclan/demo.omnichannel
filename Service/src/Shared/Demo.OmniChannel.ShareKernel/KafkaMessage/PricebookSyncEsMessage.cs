﻿using System.Collections.Generic;

namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class PriceBookSyncEsMessage<T> : BaseRedisMessage where T : class
    {
        public List<T> Products { get; set; }
        public long PriceBookId { get; set; }
    }
}
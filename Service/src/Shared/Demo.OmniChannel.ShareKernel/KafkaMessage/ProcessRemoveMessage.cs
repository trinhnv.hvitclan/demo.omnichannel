﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class ProcessRemoveMessage<T> : BaseRedisMessage where T : class
    {
        public List<long> ProductIds { get; set; }
    }
}

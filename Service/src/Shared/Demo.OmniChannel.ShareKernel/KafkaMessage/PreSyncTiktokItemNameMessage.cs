﻿using System;
using System.Collections.Generic;

namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class SyncGroupTiktokItemNameMessage
    {
        public Guid LogId { get; set; }
        public int RetailerId { get; set; }
        public long ChannelId { get; set; }
        public string KvEntities { get; set; }
        public List<string> ItemIds { get; set; }
    }
}
﻿using System;

namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class SyncErrorCreateOrder : BaseKafkaModel { }
    public class ShopeeSyncErrorCreateOrder : SyncErrorCreateOrder
    {
        public string OrderSn { get; set; }
        public Guid LogId { get; set; }
        public int BranchId { get; set; }
        public int RetailerId { get; set; }
        public int ChannelId { get; set; }
    }

    public class TiktokSyncErrorCreateOrder : SyncErrorCreateOrder
    {
        public string OrderId { get; set; }
        public Guid LogId { get; set; }
        public int BranchId { get; set; }
        public int RetailerId { get; set; }
        public int ChannelId { get; set; }
    }
    public class BaseSyncErrorInvoiceMessage : BaseKafkaModel
    {
        public string Invoice { get; set; }
        public string KvEntities { get; set; }
        public bool CurrentSaleTime { get; set; }
	    public long ChannelId { get; set; }
        public byte ChannelType { get; set; }
        public string ChannelName { get; set; }
        public string PriceBookName { get; set; }
        public string SalePriceBookName { get; set; }
        public int RetailerId { get; set; }
        public string RetailerCode { get; set; }
        public bool IsIgnoreAuditTrail { get; set; }
        public int BranchId { get; set; }
        public DateTime? SentTime { get; set; }
        public Guid LogId { get; set; }
        public long PlatformId { get; set; }
    }
    public class ShopeeSyncErrorCreateInvoice : BaseSyncErrorInvoiceMessage { }
    public class TiktokSyncErrorCreateInvoice : BaseSyncErrorInvoiceMessage { }
}
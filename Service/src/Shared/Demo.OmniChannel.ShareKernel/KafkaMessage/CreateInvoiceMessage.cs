﻿namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class ShopeeCreateInvoiceMessage : CreateInvoiceMessage { }
    public class LazadaCreateInvoiceMessage : CreateInvoiceMessage { }
    public class TikiCreateInvoiceMessage : CreateInvoiceMessage { }
    public class SendoCreateInvoiceMessage : CreateInvoiceMessage { }
    public class CreateInvoiceMessage : BaseMessage
    {
        public string OrderId { get; set; }
        public string Order { get; set; }
        public string KvEntities { get; set; }
        public long PriceBookId { get; set; }
        public string KvOrder { get; set; }
        public long KvOrderId { get; set; }
        public int RetryCount { get; set; }
        public bool IsDbException { get; set; }
        public string InvoiceCode { get; set; }
    }
}
﻿using System;

namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class SyncTiktokItemNameMessage
    {
        public Guid LogId { get; set; }
        public string GroupId { get; set; }
        public int RetailerId { get; set; }
        public long ChannelId { get; set; }
        public string KvEntities { get; set; }
        public string ItemId { get; set; }
    }
}
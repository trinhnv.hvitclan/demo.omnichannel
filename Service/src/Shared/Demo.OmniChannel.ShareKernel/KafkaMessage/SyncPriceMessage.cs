﻿using System;
using System.Collections.Generic;

namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class LazadaSyncPriceMessage : SyncPriceMessage { }
    public class ShopeeSyncPriceMessage : SyncPriceMessage { }
    public class ShopeeSyncPriceMessageV2 : SyncPriceMessage { }
    public class TikiSyncPriceMessage : SyncPriceMessage { }
    public class SendoSyncPriceMessage : SyncPriceMessage { }
    public class TiktokSyncPriceMessage : SyncPriceMessage { }
    public class SyncPriceMessage : BaseMessage
    {
        public string KvEntities { get; set; }
        public string ItemSku { get; set; }
        public string ItemId { get; set; }
        public string ParentItemId { get; set; }
        public byte ItemType { get; set; }
        public decimal? BasePrice { get; set; }
        public decimal SalePrice { get; set; }
        public DateTime? StartSaleTime { get; set; }
        public DateTime? EndSaleTime { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long KvProductId { get; set; }
        public List<MultiProductItem> MultiProducts { get; set; }
        public bool IsMessageFromTrackingServices { get; set; }
        public string Source { get; set; }
    }
}
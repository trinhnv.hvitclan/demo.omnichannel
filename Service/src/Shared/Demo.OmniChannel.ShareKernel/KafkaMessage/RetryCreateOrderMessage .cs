﻿using System;

namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class RetryCreateOrder : BaseKafkaModel
    {

    }
    public class ShopeeRetryCreateOrder : RetryCreateOrder
    {
        public string KvOrder { get; set; }
        public Guid LogId { get; set; }
        public int BranchId { get; set; }
        public int RetailerId { get; set; }
        public int ChannelId { get; set; }
    }

    public class TiktokRetryCreateOrder : RetryCreateOrder
    {
        public string KvOrder { get; set; }
        public Guid LogId { get; set; }
        public int BranchId { get; set; }
        public int RetailerId { get; set; }
        public int ChannelId { get; set; }
    }
}
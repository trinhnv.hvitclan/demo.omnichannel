﻿using System;
using System.Collections.Generic;

namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class CurrentProductMessage :BaseRetryMessage
    {
        public List<PriceDto> Products { get; set; }
    }

    public class ShopeeCurrentProductMessage : CurrentProductMessage
    {

    }
    public class LazadaCurrentProductMessage : CurrentProductMessage
    {

    }

    public class TikiCurrentProductMessage : CurrentProductMessage
    {

    }
    public class SendoCurrentProductMessage : CurrentProductMessage
    {

    }

    public class TiktokCurrentProductMessage : CurrentProductMessage
    {

    }
    public class PriceDto
    {
        public long KvProductId { get; set; }
        public long? KvMasterProductId { get; set; }
        public byte[] Revision { get; set; }
        public decimal? BasePrice { get; set; }
    }
}
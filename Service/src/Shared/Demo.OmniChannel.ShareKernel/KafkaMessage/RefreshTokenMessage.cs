﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class RefreshTokenMessage
    {
        public Guid LogId { get; set; }
        public long ChannelId { get; set; }
        public string ChannelJson { get; set; }
        public int RetryCount { get; set; }
    }
}

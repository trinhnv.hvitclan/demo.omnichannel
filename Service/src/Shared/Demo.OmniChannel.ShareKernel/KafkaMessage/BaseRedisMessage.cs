﻿using System;

namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class BaseRedisMessage
    {
        public string EventName { get; set; }
        public int BranchId { get; set; }
        public int GroupId { get; set; }
        public string Ids { get; set; }
        public byte Type { get; set; }
        public byte Advice { get; set; }
        public DateTime? SentTime { get; set; }
        public virtual string ConnectionString { get; set; }
        public virtual int RetailerId { get; set; }
        public Guid LogId { get; set; }
    }

    public class BaseRetryMessage
    {
        public long ChannelId { get; set; }
        public string ChannelName { get; set; }
        public byte ChannelType { get; set; }
        public long BasePriceBookId { get; set; }
        public long PriceBookId { get; set; }
        public string ConnectStringName { get; set; }
        public int RetailerId { get; set; }
        public Guid LogId { get; set; }
        public int BranchId { get; set; }
        public long KvProductId { get; set; }
    }
}
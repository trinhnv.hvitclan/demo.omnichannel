﻿using System;

namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class LazadaSyncFeeTransactionMessage : SyncFeeTransactionMessage { }


    public class SyncFeeTransactionMessage : BaseMessage
    {
        public int GroupId { get; set; }
        public string ShopId { get; set; }
        public DateTime? LastSync { get; set; }
        public string KvEntities { get; set; }
        public string OrderSn { get; set; }
        public virtual bool IsBreakIfMaximum { get; set; } = true;
        public DateTime? TimeStartToGet { get; set; }
    }

    public class LazadaSyncFeeTimeoutTransactionMessage : SyncFeeTransactionMessage
    {
        public override bool IsBreakIfMaximum { get; set; } = false;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Demo.OmniChannel.ShareKernel.Common;

namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class SyncModifiedProductMessage : SyncProductMessage
    {
        public DateTime? LastSyncDateTime { get; set; }
        public long ChannelScheduleId { get; set; }
        public ScheduleDto ScheduleDto { get; set; }
        public List<Guid> ParentLogIds { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;

namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class CurrentProductBranchMessage : BaseRetryMessage
    {
        public byte SyncOnHandFormula { get; set; }
        public List<ProductBranchDto> Products { get; set; }
    }

    public class ShopeeCurrentProductBranchMessage : CurrentProductBranchMessage
    {
    }

    public class LazadaCurrentProductBranchMessage : CurrentProductBranchMessage
    {
    }

    public class TikiCurrentProductBranchMessage : CurrentProductBranchMessage
    {
    }
    public class SendoCurrentProductBranchMessage : CurrentProductBranchMessage
    {
    }

    public class TiktokCurrentProductBranchMessage : CurrentProductBranchMessage
    {
    }

    public class ProductBranchDto
    {
        public long KvProductId { get; set; }
        public int BranchId { get; set; }
        public byte[] Revision { get; set; }
        public double? OnHand { get; set; }
        public double? BasePrice { get; set; }
    }
}
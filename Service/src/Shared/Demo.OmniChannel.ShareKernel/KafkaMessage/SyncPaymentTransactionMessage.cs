﻿
using Demo.OmniChannel.ShareKernel.Common;
using System;
using System.Collections.Generic;

namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class ShopeeSyncPaymentTransactionMessage : SyncPaymentTransactionMessage { }
    public class LazadaSyncPaymentTransactionMessage : SyncPaymentTransactionMessage
    {
        public List<string> OrderSnLst { get; set; } 
    }

    public class LazadaProcessFeeTransactionMessage : SyncPaymentTransactionMessage
    {
        public string OrderSn { get; set; }
    }

    public class TikiSyncPaymentTransactionMessage : SyncPaymentTransactionMessage { }
    public class SendoSyncPaymentTransactionMessage : SyncPaymentTransactionMessage { }
    public class SyncPaymentTransactionMessage : BaseMessage
    {
        public int GroupId { get; set; }
        public string ShopId { get; set; }
        public long ScheduleId { get; set; }
        public DateTime? LastSync { get; set; }
        public string KvEntities { get; set; }
        public ScheduleDto ScheduleDto { get; set; }
    }

    public class TiktokSyncPaymentTransactionMessage : SyncPaymentTransactionMessage 
    {
        public string OrderId { get; set; }
        public string DataSettlements { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;

namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class LazadaSyncOnHandMessage : SyncOnHandMessage { }
    public class ShopeeSyncOnHandMessage : SyncOnHandMessage { }
    public class ShopeeSyncOnHandMessageV2 : SyncOnHandMessage { }
    public class TikiSyncOnHandMessage : SyncOnHandMessage { }
    public class SendoSyncOnHandMessage : SyncOnHandMessage { }
    public class TiktokSyncOnHandMessage : SyncOnHandMessage { }
    public class SyncOnHandMessage : BaseMessage
    {
        public string KvEntities { get; set; }
        public string ItemSku { get; set; }
        public string ItemId { get; set; }
        public string ParentItemId { get; set; }
        public byte ItemType { get; set; }
        public byte SyncOnHandFormula { get; set; }
        public double OnHand { get; set; }
        public long KvProductId { get; set; }
        public List<MultiProductItem> MultiProducts { get; set; }
    }

    public class MultiProductItem
    {
        public string ItemSku { get; set; }
        public string ItemId { get; set; }
        public string ParentItemId { get; set; }
        public byte ItemType { get; set; }
        public double OnHand { get; set; }
        public decimal? Price { get; set; }
        public decimal? SalePrice { get; set; }
        public DateTime? StartSaleDate { get; set; }
        public DateTime? EndSaleDate { get; set; }
        public string KvProductSku { get; set; }
        public byte[] Revision { get; set; }
        public long KvProductId { get; set; }
        public string ProductName { get; set; }
    }
}
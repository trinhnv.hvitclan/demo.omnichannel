﻿using System;

namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class ShopeeCreateOrderMessageV2 : CreateOrderMessageV2 { }
    public class CreateOrderMessageV2 : BaseMessage
    {
        public string OrderId { get; set; }
        public string Order { get; set; }
        public string KvEntities { get; set; }
        public int GroupId { get; set; }
        public long PriceBookId { get; set; }
        public bool IsFirstSync { get; set; }
        public string OrderStatus { get; set; }
        public int RetryCount { get; set; }
        public bool IsDbException { get; set; }
        public string SourceQueue { get; set; }
        public string UpdatedTime { get; set; }
        public string KvOrder { get; set; }
        public DateTime? FirstReceivedOrderDate { get; set; }
        public byte? PublicEventOrderSource { get; set; } // webhook, job

    }
}
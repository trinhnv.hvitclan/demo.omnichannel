﻿using System;
using Demo.OmniChannel.ShareKernel.Common;

namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class ShopeeSyncOrderMessage : SyncOrdersMessage
    { }
    public class LazadaSyncOrderMessage : SyncOrdersMessage
    { }
    public class TikiSyncOrderMessage : SyncOrdersMessage
    { }
    public class SendoSyncOrderMessage: SyncOrdersMessage
    { }
    public class SyncOrdersMessage : BaseMessage
    {
        public string KvEntities { get; set; }
        public DateTime? LastSync { get; set; }
        public int GroupId { get; set; }
        public long BasePriceBookId { get; set; }
        public ScheduleDto ScheduleDto { get; set; }
        public bool IsFirstSync { get; set; }
        public int SyncOrderFormula { get; set; }
    }
}
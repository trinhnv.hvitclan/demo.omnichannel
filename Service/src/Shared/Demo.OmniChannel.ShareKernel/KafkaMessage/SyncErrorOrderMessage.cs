﻿namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class SyncErrorOrderMessage : BaseMessage
    {
        public string Order { get; set; }
        public string KvEntities { get; set; }
        public string ChannelJson { get; set; }
    }

    public class LazadaSyncErrorOrderMessage : SyncErrorOrderMessage
    {
    }

    public class ShopeeSyncErrorOrderMessage : SyncErrorOrderMessage
    {

    }

    public class TikiSyncErrorOrderMessage : SyncErrorOrderMessage
    {
    }

    public class SendoSyncErrorOrderMessage : SyncErrorOrderMessage
    {
    }
    public class TiktokSyncErrorOrderMessage : SyncErrorOrderMessage
    {
    }

    public class SyncErrorInvoiceMessage : BaseMessage
    {
        public string Invoice { get; set; }
        public string KvEntities { get; set; }
        public bool CurrentSaleTime { get; set; }
    }

    public class LazadaSyncErrorInvoiceMessage : SyncErrorInvoiceMessage
    {
    }

    public class ShopeeSyncErrorInvoiceMessage : SyncErrorInvoiceMessage
    {
    }

    public class TikiSyncErrorInvoiceMessage : SyncErrorInvoiceMessage
    {
    }

    public class SendoSyncErrorInvoiceMessage : SyncErrorInvoiceMessage
    {
    }
    public class TiktokSyncErrorInvoiceMessage : SyncErrorInvoiceMessage
    {
    }
}
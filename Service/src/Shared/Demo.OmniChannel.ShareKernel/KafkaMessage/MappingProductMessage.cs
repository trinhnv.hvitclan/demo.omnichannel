﻿using Demo.OmniChannel.ShareKernel.Auth;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    [DataContract]
    public class KafkaMessage
    {
        [DataMember(Name = "topicName")]
        public string TopicName { get; set; }
        [DataMember(Name = "key")]
        public string Key { get; set; }
        [DataMember(Name = "jsonContent")]
        public dynamic JsonContent { get; set; }
        public KafkaMessage(string topicName, string key, dynamic jsonContent)
        {
            TopicName = topicName;
            Key = key;
            JsonContent = jsonContent;
        }
    }

    [DataContract]
    public class MappingRequest : BaseMessageV2
    {

        [DataMember(Name = "kvEntities")]
        public string KvEntities { get; set; }
        [DataMember(Name = "isFirstMapping")]
        public bool IsFirstMapping { get; set; }
        [DataMember(Name = "itemIds")]
        public List<string> ItemIds { get; set; }
        [DataMember(Name = "connectStringName")]
        public string ConnectStringName { get; set; }
        [DataMember(Name = "productMappings")]
        public List<ProductMappingPre> ProductMappings { get; set; }
        [DataMember(Name = "isAddNew")]
        public bool IsAddNew { get; set; }
        [DataMember(Name = "messageFrom")]
        public string MessageFrom { get; set; }
    }

    public class MappingProductMessage : BaseMessage
    {
        public string KvEntities { get; set; }
        public bool IsFirstMapping { get; set; }
        public List<string> ItemIds { get; set; }
    }

    public class CurrentPageMappingMessage : MappingProductMessage
    {
        public int CurrentPage { get; set; }
        public bool IsSyncOrder { get; set; }
    }

    [DataContract]
    public class RemoveProductMappingMessage
    {
        [DataMember(Name = "retailerId")]
        public int RetailerId { get; set; }
        [DataMember(Name = "branchId")]
        public int BranchId { get; set; }
        [DataMember(Name = "connectStringName")]
        public string ConnectStringName { get; set; }
        [DataMember(Name = "channelIds")]
        public List<long> ChannelIds { get; set; }
        [DataMember(Name = "kvProductIds")]
        public List<long> KvProductIds { get; set; }
        [DataMember(Name = "channelProductIds")]
        public List<string> ChannelProductIds { get; set; }
        [DataMember(Name = "channelProductParentIds")]
        public List<string> ChannelProductParentIds { get; set; }
        [DataMember(Name = "isDeleteKvProduct")]
        public bool IsDeleteKvProduct { get; set; }
        [DataMember(Name = "logId")]
        public Guid LogId { get; set; }
        [DataMember(Name = "messageFrom")]
        public string MessageFrom { get; set; }
    }
    public class CreateProductMappingMessage
    {
        public int RetailerId { get; set; }
        public int BranchId { get; set; }
        public long ChannelId { get; set; }
        public string ConnectStringName { get; set; }
        public List<ProductMappingPre> ProductMappings { get; set; }
        public bool IsAddNew { get; set; }
        public Guid LogId { get; set; }
        public string MessageFrom { get; set; }
    }

    [DataContract]
    public class ProductMappingPre
    {
        [DataMember(Name = "productChannelObjectIdKey")]
        public string ProductChannelObjectIdKey { get; set; }
        [DataMember(Name = "productKvId")]
        public long ProductKvId { get; set; }
        [DataMember(Name = "productKvSku")]
        public string ProductKvSku { get; set; }
        [DataMember(Name = "productKvFullName")]
        public string ProductKvFullName { get; set; }
        [DataMember(Name = "retailerId")]
        public int RetailerId { get; set; }
        [DataMember(Name = "channelId")]
        public long ChannelId { get; set; }
        [DataMember(Name = "branchId")]
        public int BranchId { get; set; }
        [DataMember(Name = "channelProductKey")]
        public string ChannelProductKey { get; set; }
        [DataMember(Name = "productChannelId")]
        public string ProductChannelId { get; set; }
        [DataMember(Name = "parentProductChannelId")]
        public string ParentProductChannelId { get; set; }
        [DataMember(Name = "productChannelSku")]
        public string ProductChannelSku { get; set; }
        [DataMember(Name = "productChannelName")]
        public string ProductChannelName { get; set; }
        [DataMember(Name = "productChannelType")]
        public byte ProductChannelType { get; set; }
        [DataMember(Name = "inActiveBranchIds")]
        public List<int> InActiveBranchIds { get; set; }
    }

    public class UpdateProductFlagRelateMessage
    {
        public ExecutionContext ExecutionContext { get; set; }
        public List<long> ProductIds { get; set; }
        public bool IsRelate { get; set; }
        public int ReTryAttempt { get; set; }
        public Guid LogId { get; set; }
    }
}
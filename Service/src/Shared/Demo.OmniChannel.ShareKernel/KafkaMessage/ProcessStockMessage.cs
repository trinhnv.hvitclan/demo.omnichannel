﻿
using System.Collections.Generic;

namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class ProcessStockMessage<T> : BaseRedisMessage where T : class
    {
        public List<T> Products { get; set; }
    }
}
﻿
using System.Collections.Generic;

namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class SyncComboProductMessage
    {
        public string KvEntities { get; set; }
        public long ParentProductId { get; set; }
        public int RetailerId { get; set; }
        public string Formulas { get; set; }
    }
}
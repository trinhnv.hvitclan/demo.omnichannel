﻿namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class ShopeeCreateOrderMessage : CreateOrderMessage { }
    public class LazadaCreateOrderMessage : CreateOrderMessage { }
    public class TikiCreateOrderMessage : CreateOrderMessage { }
    public class SendoCreateOrderMessage : CreateOrderMessage { }
    public class CreateOrderMessage :BaseMessage
    {
        public string OrderId { get; set; }
        public string Order { get; set; }
        public string KvEntities { get; set; }
        public int GroupId { get; set; }
        public long PriceBookId { get; set; }
        public bool IsFirstSync { get; set; }
        public string OrderStatus { get; set; }
        public int RetryCount { get; set; }
        public bool IsDbException { get; set; }
        public string SourceQueue { get; set; }
        public string UpdatedTime { get; set; }

    }
}
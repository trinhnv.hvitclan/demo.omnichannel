﻿using System;

namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class GetProductBranchByChannelMessage
    {
        public long ChannelId { get; set; }
        public string ChannelName { get; set; }
        public int BranchId { get; set; }
        public int RetailerId { get; set; }
        public byte ChannelType { get; set; }
        public long BasePriceBookId { get; set; }
        public long PriceBookId { get; set; }
        public byte SyncOnHandFormula { get; set; }
        public Guid LogId { get; set; }
    }
}
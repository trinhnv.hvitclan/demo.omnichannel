﻿using System;
using System.Runtime.Serialization;

namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class BaseMessage
    {
        public long ChannelId { get; set; }
        public byte ChannelType { get; set; }
        public string ChannelName { get; set; }
        public string PriceBookName { get; set; }
        public string SalePriceBookName { get; set; }
        public int RetailerId { get; set; }
        public string RetailerCode { get; set; }
        public bool IsIgnoreAuditTrail { get; set; }
        public int BranchId { get; set; }
        public DateTime? SentTime { get; set; }
        public Guid LogId { get; set; }
        public long PlatformId { get; set; }
    }

    [DataContract]
    [KnownType(typeof(MappingRequest))]
    public class BaseMessageV2
    {
        [DataMember(Name = "channelId")]
        public long ChannelId { get; set; }
        [DataMember(Name = "channelType")]
        public byte ChannelType { get; set; }
        [DataMember(Name = "channelName")]
        public string ChannelName { get; set; }
        [DataMember(Name = "priceBookName")]
        public string PriceBookName { get; set; }
        [DataMember(Name = "salePriceBookName")]
        public string SalePriceBookName { get; set; }
        [DataMember(Name = "retailerId")]
        public int RetailerId { get; set; }
        [DataMember(Name = "retailerCode")]
        public string RetailerCode { get; set; }
        [DataMember(Name = "isIgnoreAuditTrail")]
        public bool IsIgnoreAuditTrail { get; set; }
        [DataMember(Name = "branchId")]
        public int BranchId { get; set; }
        [DataMember(Name = "sentTime")]
        public DateTime? SentTime { get; set; }
        [DataMember(Name = "logId")]
        public Guid LogId { get; set; }
        [DataMember(Name = "platformId")]
        public long PlatformId { get; set; }
    }

    public class BaseKafkaModel { }
   

    public class SyncOrderWithAutoCreateProduct : BaseKafkaModel
    {
        public string KvOrder { get; set; }
        public string KvEntities { get; set; }
        public Guid LogId { get; set; }
        public int BranchId { get; set; }
        public int RetailerId { get; set; }
        public long ChannelId { get; set; }
        public string OrderId { get; set; }
    }
  
}
﻿using System;

namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class SyncProductMessage
    {
        public long ChannelId { get; set; }
        public byte ChannelType { get; set; }
        public int RetailerId { get; set; }
        public string RetailerCode { get; set; }
        public int BranchId { get; set; }
        public string KvEntities { get; set; }
        public bool IsUpdateChannel { get; set; }
        public bool IsIgnoreAuditTrail { get; set; }
        public DateTime? SendTime { get; set; }
        public Guid LogId { get; set; }
        public int RetryCount { get; set; } = 0;
    }
}
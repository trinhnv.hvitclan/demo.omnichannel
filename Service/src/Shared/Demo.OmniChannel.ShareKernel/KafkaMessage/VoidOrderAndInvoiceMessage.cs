﻿namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class VoidOrderAndInvoiceMessage : BaseMessage
    {
        public long OrderId { get; set; }
        public string KvEntities { get; set; }
        public string Prefix { get; set; }

    }
}

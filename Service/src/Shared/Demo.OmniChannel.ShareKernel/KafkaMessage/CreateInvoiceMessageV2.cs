﻿namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class ShopeeCreateInvoiceMessageV2 : CreateInvoiceMessageV2 { }
    public class CreateInvoiceMessageV2 : BaseMessage
    {
        public string OrderId { get; set; }
        public string Order { get; set; }
        public string KvEntities { get; set; }
        public long PriceBookId { get; set; }
        public string KvOrder { get; set; }
        public long KvOrderId { get; set; }
        public int RetryCount { get; set; }
        public bool IsDbException { get; set; }
        public string InvoiceCode { get; set; }

        public string OrderSn { get; set; }
        public long Timestamps { get; set; }
        public string CodeSource { get; set; }
        public string Source { get; set; }
        public string ShopId { get; set; }
        public int ProcessType { get; set; } // 1 Order; 2 Invoice
    }
    public class TiktokCreateInvoiceMessageV2 : CreateInvoiceMessageV2 { }
}
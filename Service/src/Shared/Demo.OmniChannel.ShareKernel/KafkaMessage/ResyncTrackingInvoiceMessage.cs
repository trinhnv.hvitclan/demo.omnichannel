﻿namespace Demo.OmniChannel.ShareKernel.KafkaMessage
{
    public class ResyncTrackingInvoiceMessage : BaseMessage
    {
        public string InvoiceCode { get; set; }
        public string KvEntities { get; set; }
    }
}

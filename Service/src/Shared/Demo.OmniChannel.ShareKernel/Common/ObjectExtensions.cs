﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Demo.OmniChannel.ShareKernel.Common
{
    public static class ObjectExtensions
    {
        public static T ToObject<T>(this Object fromObject)
        {
            return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(fromObject));
        }

        public static List<T> ToObjectList<T>(this Object fromObject)
        {
            return JsonConvert.DeserializeObject<List<T>>(JsonConvert.SerializeObject(fromObject));
        }
    }
}
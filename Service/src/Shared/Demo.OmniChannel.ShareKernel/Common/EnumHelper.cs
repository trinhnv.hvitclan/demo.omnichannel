﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Resources;
using Demo.OmniChannel.Resource;

namespace Demo.OmniChannel.ShareKernel.Common
{
    public static class EnumHelper
    {
        public static int GetValueByName<TEnum>(string name)
        {
            return Enum.GetValues(typeof(TEnum)).Cast<TEnum>()
                .Where(x => name.ToLower().Contains(x.ToString().ToLower()))
                .Select(x => (int)Enum.Parse(typeof(TEnum), x.ToString())).FirstOrDefault();
        }

        public static string GetNameByType<TEnum>(byte type)
        {
            return Enum.GetName(typeof(TEnum), type);
        }
       
        public static string ToDescription(Enum value)
        {
            try
            {
                FieldInfo fi = value.GetType().GetField(value.ToString());
                DescriptionAttribute[] attributes = fi.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];

                if (attributes != null && attributes.Any())
                {
                    var val = ReadResourceValue(KvMessage.ResourceManager, attributes[0].Description);
                    return val ?? attributes[0].Description;
                }

                return value.ToString();
            }
            catch
            {
                return string.Empty;
            }
        }

        public static T EnumTypeTo<T>(object value) where T : struct
        {
            T result;
            Enum.TryParse(value?.ToString(), out result);
            return result;
        }

        public static string AddSpaceBetweenProductNameAddAttribute(string fullName) => fullName;

        public static string RelaceProductName(string fullName, string unitName)
        {
            try
            {
                if (string.IsNullOrEmpty(fullName) || string.IsNullOrEmpty(unitName) || fullName == unitName)
                    return fullName;
                unitName = "(" + unitName + ")";
                int startIndex = fullName.LastIndexOf(unitName);
                return startIndex == -1 || startIndex + unitName.Length != fullName.Length ? fullName : fullName.Remove(startIndex, unitName.Length);
            }
            catch
            {
                return fullName;
            }
        }

        public static string ReadResourceValue(ResourceManager resourceManager, string key)
        {
            try
            {
                var resourceValue = resourceManager.GetString(key);
                key = string.IsNullOrEmpty(resourceValue) ? key : resourceValue;
            }
            catch
            {
                //ignore
            }
            return key;
        }

        public static string ToDescriptionName(Enum value)
        {
            try
            {
                string result = value.ToString();
                FieldInfo field = value.GetType().GetField(value.ToString());
                if (field == null)
                {
                    return string.Empty;
                }

                DescriptionAttribute[] array = (DescriptionAttribute[])field.GetCustomAttributes(typeof(DescriptionAttribute), inherit: false);
                if (array.Length == 0)
                {
                    return result;
                }

                string description = array[0].Description;
                if (!string.IsNullOrEmpty(description))
                {
                    result = description;
                }

                return result;
            }
            catch
            {
                return string.Empty;
            }
        }

        public static bool IsDefinedInEnum(this Enum value, Type enumType)
        {
            if (value.GetType() != enumType)
                return false;

            return Enum.IsDefined(enumType, value);
        }
    }
}
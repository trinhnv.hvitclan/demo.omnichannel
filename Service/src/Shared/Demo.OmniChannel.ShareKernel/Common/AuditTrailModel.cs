﻿using System.Collections.Generic;

namespace Demo.OmniChannel.ShareKernel.Common
{
    public class ProductMappingAuditTrail
    {
        public byte? ChannelType { get; set; }
        public string ChannelName { get; set; }
        public List<ProductMappingItemAuditTrail> Items { get; set; }
    }

    public class ProductMappingItemAuditTrail
    {
        public string ProductKvSku { get; set; }
        public string ParentProductChannelId { get; set; }
        public string ProductChannelId { get; set; }
        public string ProductChannelSku { get; set; }
    }

    public class CreateProductAuditTrail
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public double Quantity { get; set; }
        public decimal BasePrice { get; set; }
        public bool IsExpired { get; set; }
        public bool AllowsSale { get; set; }
        public bool? IsRewardPoint { get; set; }
        public double OnHand { get; set; }
        public decimal Price { get; set; }
    }
    public class ProductAutoMappingAuditTrail
    {
        public string ProductKvSku { get; set; }
        public string CommonParentProductChannelId { get; set; }
        public string CommonProductChannelId { get; set; }
        public string ProductChannelSku { get; set; }
    }
}

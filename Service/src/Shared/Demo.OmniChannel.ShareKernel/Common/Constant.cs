﻿using System.Collections.Generic;

namespace Demo.OmniChannel.ShareKernel.Common
{
    public static class FeeList
    {
        public const string LazadaPaymentFee = "Payment Fee";
        public const string LazadaCommission = "Commission";
        public const string LazadaOrdersLazadaFees = "Orders-Lazada Fees";
        public const string LazadaOrdersMarketingFees = "Orders-Marketing Fees";
        public const string LazadaOrdersClaims = "Orders-Claims";
        public const string LazadaOrdersLogistics = "Orders-Logistics";
        public const string LazadaItemPriceCredit = "Item Price Credit";
        public const string LazadaPaymentFeeRefund = "Payment fee refund - correction for SOD-COD";
        public const string LazadaPaymentFeeSod = "SOD - COD charge";

        public static readonly string ShopeeFeeIgnoreWhenNoFinalFee = "TotalShippingFee";

        public static List<string> InitialShoppeFee()
        {
            return new List<string>
            {
                "TotalShippingFee",
                "SellerTransactionFee",
                "ServiceFee",
                "CommissionFee"
            };
        }

        public static List<string> InitialSendoFee()
        {
            return new List<string>
            {
                "FinalShippingFee",
                "SellerTransactionFee",
                "ServiceFee",
                "CommissionFee"
            };
        }

        public static List<string> InitialLazadaFee()
        {
            return new List<string>
            {
                LazadaPaymentFee,
                LazadaCommission,
                LazadaOrdersLazadaFees,
                LazadaOrdersMarketingFees,
                LazadaOrdersClaims,
                LazadaOrdersLogistics
            };
        }
    }
    public static class ChanelCodeName
    {
        public const string Dhlzd = "DHLZD";
        public const string Dhspe = "DHSPE";
        public const string Dhtiki = "DHTIKI";
        public const string Dhsdo = "DHSDO";
    }

    public static class ProcessOrderStatus
    {
        public const int Processed = 1;
    }

    public static class ProcessFlowType
    {
        public const int Order = 1;
        public const int Invoice = 2;
    }

    public static class ProcessOrderSource
    {
        public const string WebHook = "webhook";
    }

    public static class RegionCode
    {
        public const string VN = "VN";
    }

    public static class RoutingKey
    {
        public const string WebhookShopee = "shopee.order";
        public const string ChannelActive = "omni.channel.activate";
        public const string ChannelDeActive = "omni.channel.deactivate";
        public const string AddNewProduct = "sync-new-omni-products";
        public const string MessageErrorNotification = "notification-error-orders";
        public const string SyncPrice = "omni.product.sync-price-{0}";
        public const string MessageErrorProductNotification = "notification-error-products";
        public const string OrderTrackingEvent = "#.order-tracking-event.#";

        public const string OmniSyncPrice = "OmniSyncPriceEvent";
        public const string OmniSyncOnHand = "OmniSyncOnhandEvent";
        public const string TiktokSyncProductFromOrder = "TiktokSyncProductFromErrorOrder";
        public const string OmniPushChannelExpired = "#.schedule.channel.expired.#";
    }

    public static class CategoryName
    {
        public static readonly string Orther = "Khác";
    }

    public static class SourceQueue
    {
        public const string ComparingResync = "omni-resync-comparing";
    }

    public static class ErrorCode
    {
        public const string ErrorBussiness = "errorBussiness";
    }
}
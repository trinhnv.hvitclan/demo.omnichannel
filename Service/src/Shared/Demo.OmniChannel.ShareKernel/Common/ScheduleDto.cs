﻿using System;

namespace Demo.OmniChannel.ShareKernel.Common
{
    public class ScheduleDto
    {
        public long ChannelId { get; set; }
        public byte ChannelType { get; set; }
        public int ChannelBranch { get; set; }
        public int RetailerId { get; set; }
        public long BasePriceBookId { get; set; }
        public long Id { get; set; }
        public DateTime? NextRun { get; set; }
        public DateTime? LastSync { get; set; }
        public bool IsRunning { get; set; }
        public byte Type { get; set; }
        public int? SyncOrderFormula { get; set; }
    }
}
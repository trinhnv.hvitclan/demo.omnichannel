﻿namespace Demo.OmniChannel.ShareKernel.Common
{
    public class KvConstant
    {
        public const string ChannelAuthKey = "cache:channel:auth_{0}";
        public const string ProductMappingCacheKey = "products:{0}:{1}_{2}";
        public const string ListProductMappingCacheKey = "listmappingproducts:{0}_{1}_{2}";// retailerId_channelId_prodcutKVid
        public const double Tolerance = .000001;
        public const string ExecutionContextCacheKey = "cache:retailer:context_{0}";
        public const string LocationCacheKey = "cache:location:{0}";
        public const string WardCacheKey = "cache:ward:{0}:{1}";
        public const string RetailerCodeCacheKey = "cache:retailer:retailercode_{0}";
        public const string RetailerIdCacheKey = "cache:retailer:retailerId_{0}";
        public const string KvGroupCacheKey = "cache:kvgroups:group_{0}";
        public const string KvUserLangCacheKey = "cache:kvUser:lang_l_{0}_{1}";
        public const string LockProductStockCache = "lock:stock:lock_{0}_{1}";
        public const string AcquireLockProductStockCache = "acquirelock:stock:lock_{0}_{1}";
        public const string LockProductPriceCache = "lock:price:lock_{0}_{1}";
        public const string AcquireLockProductPriceCache = "acquirelock:price:lock_{0}_{1}";
        public const string ChannelsByRetailerKey = "cache:channels:retailerId_{0}";
        public const string RetailersByChannelsKey = "cache:channels:omni:retailers";
        public const string ChannelByIdKey = "cache:channel:channelId_{0}";
        public const string ChannelScheduleByIdKey = @"{0}:{1}:{2}";
        public const string LockAddMappingKey = "lock:lockAddMappingChannel_{0}";
        public const string LockAddChannelKey = "lock:lockAddChannel_{0}_{1}";
        public const string LockSetCacheChannelKey = "lock:lockSetCacheChannel_{0}_{1}";
        public const string LockSetCacheAuthKey = "lock:lockSetCacheAuth_{0}_{1}";
        public const string IdentityCacheSetKey = "cache:channels:webhookvalidate:identityKey_{0}_{1}";
        public const string KvPosSettingKey = "cache:kv_settings:i_{0}";
        public const string LockGetAccessTokenChannelId = "lock:lockGetAccessTokenChannelId_{0}";
        public const string CacheSendoLocation = "cache:sendo:location_{0}";
        public const string SkipResyncPriceFail = "cache:SkipResyncPriceFail";
        public const string LockWaitingDeleteMappingCache = "cache:waitingDeleteMapping:channelId_{0}-kvProductId_{1}";
        public const string ChannelPlatformKey = "cache:platformId_{0}";
        public const string FeeLZDPlatformKey = "cache:waitingLZDFee-{0}"; // ChannelId
        public const string RefreshTokenKeyByChannelId = "cache:channel:refreshToken:channelId_{0}";

        public const string LockProcessOrderShopeeV2 = "lock:lockProcessOrderShopeeV2-{0}-{1}";
        public const string LockProcessOrderTiktok = "lock:lockProcessOrderTiktok-{0}-{1}";

        public const string GroupIdWithActiveRetailerKey = "cache:kvgroups_active_retailers:group_{0}";
        public const string MigrateSaleChannelWithRetailerKey = "cache:migratesalechannel:group_{0}";

        public const string IsRunningGetProductKey = "cache:isrunninggetproduct:channel_{0}";
        public const string TiktokCallBackSubcribeKey = "cache:callback:state_{0}";


        /// <summary>
        /// {0}: ChannelId
        /// {1}: BranchId
        /// {2}: ProductId
        /// </summary>
        public const string KvOnHandRevision = "productrevision:kvOnHandRevision_{0}_{1}_{2}";
        public const string KvPriceRevision = "productrevision:price:kvPriceRevision_{0}_{1}";
        public const string LockMappingChannelId = "lock:productProcessingMappingChannelId_{0}";
        public const string LockMappingChannelIdV2 = "lock:productProcessingMappingChannelIdV2_{0}";

        public const string LockCreateInvoice = "lock:createInvoice_{0}_{1}_{2}";
        /// <summary>
        /// {0}: Customer phone number
        /// </summary>
        public const string LockAddCustomer = "lock:addCustomer_{0}";

        /// <summary>
        /// {0}: lock process invoice by retailerid_orderid
        /// </summary>
        public const string LockProcessInvoice = "lock:processInvoice_{0}_{1}";
        public const string LockProcessInvoiceV2 = "lock:processInvoiceV2_{0}_{1}";

        /// <summary>
        /// {0}: Channel id, {1}: Group id (Guid)
        /// </summary>
        public const string GroupChannelItemNameCacheKey = "cache:groupChannelItemName:c-{0}_g-{1}";

        /// <summary>
        /// {0}: Channel Id, {1}: Channel product id
        /// </summary>
        public const string ChannelItemNameCacheKey = "cache:channelItemNameCacheKey:c-{0}_cp-{1}";

        /// <summary>
        /// {0}: Schedule id
        /// </summary>
        public const string ScheduleDtoCacheKey = "urn:scheduledto:{0}";

        public const string KvMasterEntities = "KvMasterEntities";

        public const string CachePlatform = "cache:platform:Id_{0}";
        public const string CachePlatformCurrentType = "cache:platform_current_{0}_app_support_{1}";
        public const string CacheKvAuToken = "cache:kvaa:retailerId_{0}";

        /// <summary>
        /// {0}: Order id
        /// </summary>
        public const string LockAutoCreateProductWithOrderId = "lock:autoCreateProductOrderId_{0}";
        public const string LockAutoCreateProductByChannel = "lock:autoCreateProductByChannel_{0}";
        public const string LockCreateOrderFromFBPos = "lock:lockCreateOrderFromFBPos_{0}_{1}";

        public const string CacheTiktokSyncProductOrder = "urn:tiktokSyncProductErrorOrder:{0}";
    }

    public class ShopeeStatus
    {
        public const string Unpaid = "UNPAID";
        public const string ReadyToShip = "READY_TO_SHIP";
        public const string Completed = "COMPLETED";
      
    }

    public class ShopeePromotionType
    {
        public const string BundleDeal = "bundle_deal";
    }

    public class LazadaStatus
    {
        public const string Unpaid = "unpaid";
        public const string Pending = "pending";
        public const string ReadyToShip = "ready_to_ship";
        public const string Repacked = "repacked";
        public const string Packed = "packed";
        public const string ReadyToShipPending = "ready_to_ship_pending";
    }
    public static class EsEventName
    {
        public const string UpdateStock = "UpdateStock";
        public const string AddPriceBook = "AddPricebook";
        public const string UpdatePriceBook = "UpdatePricebook";
        public const string UpdateStockBatchExpire = "UpdateStockBatchExpire";
        public const string UpdateStockSerial = "UpdateStockSerial";
        public const string Add = "Add";
        public const string Delete = "Delete";
        public const string Update = "Update";
        public const string UpdatePrice = "UpdatePrice";
        public const string DeletePriceBook = "DeletePricebook";
    }

    public class TikiSellerStatus
    {
        public const string Completed = "completed";
    }

    public class CacheScheduleIndex
    {
        public static string ScheduleEvent(byte channelType, byte scheduleType) =>
            $"urn:scheduledto>channelType:{channelType}>scheduleType:{scheduleType}";
    }

    public class KvOrderValidateMessage
    {
        public const string OrderExistedVi = "{0} đã tồn tại";
        public const string OrderExistedEn = "{0} is already existed";
    }

    public class KeywordCommonSearch
    {
        public const string CallApiError = "CALL API HAVE ERROR";
        public const string CallLazadaError = "CALL LAZADA SYSTEM HAVE ERROR";
        public const string CallShopeeError = "CALL SHOPEE SYSTEM HAVE ERROR";
        public const string CallTikiError = "CALL TIKI SYSTEM HAVE ERROR";
        public const string CallSendoError = "CALL SENDO SYSTEM HAVE ERROR";
    }

    public class RedisMqStatus
    {
        public const string Stopped = "Stopped";
        public const string Stopping = "Stopping";
        public const string Disposed = "Disposed";
        public const string Started = "Started";
    }

    public class DbExceptionMessages
    {
        public const string ConnectionTimeout = "timeout expired";
        public const string TransactionDeadlocked= "deadlock";
        public const string DatabaseException = "Có lỗi cập nhật dữ liệu";
    }


    public enum ContractType
    {
        Trial = 0,
        Basic = 1,
        Advance = 2
    }
}
﻿namespace Demo.OmniChannel.ShareKernel.Common
{
    public class KvChannelConfig
    {
        public string Name { get; set; }
        public byte Type { get; set; }
        public string ApiEndpoint { get; set; }
        public long PartnerId { get; set; }
        public string Token { get; set; }
        public string SecretKey { get; set; }
        public string ClientAppId { get; set; }
    }
}
﻿using System.ComponentModel;

namespace Demo.OmniChannel.ShareKernel.Common
{
    public enum ChannelType
    {
        Lazada = 1,
        Shopee = 2,
        Tiki = 3,
        Sendo = 4,
        Tiktok = 5
    }

    public enum ErrorTypeSync
    {
        Order = 1,
        Invoice = 2,
        Approved = 3,
        NeedRegister = 4
    }
    public enum ChannelStatusType
    {
        Pending = 1,
        Reject = 2,
        Approved = 3,
        NeedRegister = 4
    }
    public enum ScheduleType
    {
        SyncModifiedProduct = 1,
        SyncPrice = 2,
        SyncOnhand = 3,
        SyncOrder = 4,
        SyncPaymentTransaction = 5,
        SyncFeeTransaction = 6,
        SyncErrorInvoice = 7,
        SyncProductFromErrorOrder = 15
    }
    public enum SyncOnHandFormula
    {
        [Description("ecommSellQtyFormula1")]
        OnHand = 1,
        [Description("ecommSellQtyFormula2")]
        SubOrder = 2,
        [Description("ecommSellQtyFormula3")]
        AddSupplierOrder = 3,
        [Description("ecommSellQtyFormula4")]
        SubOrderAddSupplierOrder = 4,
    }

    public enum KafkaTopicType
    {
        SyncProduct = 1,
        MappingProduct = 2,
        SyncEsMessage = 3,
        SyncModifiedProduct = 4,
        SyncOnHand = 5,
        SyncPrice = 6,
        SyncOrder = 7,
        CreateOrder = 8,
        CreateInvoice = 9,
        SyncErrorOrder = 10,
        SyncEsDc2Message = 11,
        CurrentPageMappingProduct = 12,
        SyncErrorInvoice = 13,
        PriceBookSyncEsMessage = 14,
        PriceBookSyncEsDc2Message = 15,
        TestMultiThread = 16,
        DLQCreateOrder = 17,
        RetryCreateOrder = 18,
        CreateOrderWithAutoCreateProduct = 19,
        MappingRequest = 20,
        RemoveMappingRequest = 21,
        DLQCreateInvoice = 22,
    }

    public enum ChannelProductType
    {
        Normal = 1,
        Variation = 2
    }

    public enum FunctionType
    {
        PosParameter = 1,
        ShopeeInteration = 48,
        TiktokInteration = 69,
        LazadaInteration = 34,
        MappingSalesChannel = 55,
        TikiInteration = 56,
        SendoInteration = 208,
        OrderFBPOS = 9
    }

    public enum AuditTrailAction
    {
        Create = 1,
        Update = 2,
        Delete = 3,
        Reject = 4,
        ProductIntergate = 9,
        OrderIntergate = 10,
        InvoiceIntergate = 11,
        LicenseService = 12
    }
    public class KvSettingKey
    {
        public const string SellAllowOrder = "SellAllowOrder";
        public const string UseOrderSupplier = "UseOrderSupplier";
    }
    public enum SettingTypes
    {
        Attribute = 1,
        Feature = 2,//to do remove feature setting on cpanel 
        Settings = 3,
        System = 4,
        SettingExtra = 5
    }
    public enum GroupSettings
    {
        TransactionsManagement = 1,
        ProductManagement = 2,
        PartnerManagement = 3,
        TimekeepingManagement = 4
    }
    public enum ProductType
    {
        [Description("productType_Combo")]
        Manufactured = 1,
        [Description("productType_Commodity")]
        Purchased = 2,
        [Description("productType_Service")]
        Service = 3
    }

    public class SyncErrorType
    {
        public const string OnHand = "OnHand";
        public const string Price = "Price";
    }

    public enum TrackingAdvice
    {
        Add,
        Remove,
        Update
    }
    public enum EsEventType
    {
        All = 0,
        Product = 1,
        //Customer = 2,
        //Invoice = 3,
        //Order = 4
    }

    public enum SyncOrderFormula
    {
        [Description("syncOrderFormulaNow")]
        Now = -1,
        [Description("syncOrderFormulaToday")]
        Today = 0,
        [Description("syncOrderFormulaLast7Day")]
        Last7Day = 7,
        [Description("syncOrderFormulaLast15Day")]
        Last15Day = 15,
        [Description("syncOrderFormulaLast30Day")]
        Last30Day = 30
    }

    public enum SendoOrderStatus
    {
        [Description("Mới")] New = 2,
        [Description("Đang xử lý")] Proccessing = 3,
        [Description("Đang vận chuyển")] Shipping = 6,
        [Description("Đã giao hàng")] Pod = 7,
        [Description("Đã hoàn tất")] Completed = 8,
        [Description("Đóng")] Closed = 10,
        [Description("Yêu cầu hoãn")] Delaying = 11,
        [Description("Đang hoãn")] Delay = 12,
        [Description("Hủy")] Cancelled = 13,
        [Description("Yêu cầu tách")] Splitting = 14,
        [Description("Chờ tách")] Splitted = 15,
        [Description("Chờ gộp")] Merging = 19,
        [Description("Đang đổi trả")] Returning = 21,
        [Description("Đổi trả thành công")] Returned = 22,
        [Description("Chờ sendo xử lý")] WaitingSendo = 23,
    }

    public enum SendoDeliveryStatus
    {
        [Description("Trả hàng cho người bán")] ReturnSeller = 8,
        [Description("Người bán đã nhận lại hàng")] ReturnSellerReceived = 10,
    }

    public class ActionPromoUpdate
    {
        public const string AddInPromo = "add_in_promo";
        public const string RemoveFromPromo = "removed_from_promo";
        public const string PromoTimeUpdated = "promo_time_updated";
    }

    public enum SyncOrderFormulaType
    {
        [Description("Tạo")]
        Create = 1,
        [Description("Tạo/cập nhật")]
        CreatedUpdate = 2
    }

    public enum LogType
    {
        [Description("Info")]
        Info = 0,
        [Description("Warning")]
        Warning = 1,
        [Description("Error")]
        Error = 2
    }

    public enum SyncOrderSaleTimeType
    {
        [Description("Thời gian tạo đơn trên sàn")]
        TimeCreateOrder = 0,
        [Description("Thời gian xác nhận đơn trên sàn")]
        TimeReadyToShip = 1,
    }

    public enum SyncOrderAutoCreateProduct
    {
        [Description("Chờ liên kết thủ công")]
        Manual = 0,
        [Description("Tự động tạo trên Demo và liên kết")]
        Auto = 1,
    }

    #region Shopee
    public enum ShopeeTrackingStatus
    {
        [Description("Người gửi đang chuẩn bị hàng")]
        ORDER_CREATED,
    }

    #endregion

    public enum SettingCustomer
    {
        [Description("Shopee")]
        IsShopeeSyncCustomer,

        [Description("Lazada")]
        IsLazadaSyncCustomer,

        [Description("Tiki")]
        IsTikiSyncCustomer,

        [Description("Sendo")]
        IsSendoSyncCustomer,

        [Description("Tiktok")]
        IsTiktokShopSyncCustomer,
    }
    public enum WarrantyTimeType
    {
        [Description("Day")]
        Day = 1,
        [Description("Month")]
        Month = 2,
        [Description("Year")]
        Year = 3,
    }

    public enum WarrantyType
    {
        [Description("ManufacturerWarranty")]
        ManufacturerWarranty = 1,
        [Description("SellerWarranty")]
        SellerWarranty,
        [Description("Maintenance")]
        Maintenance
    }

    public enum WarrantyOrderStatus
    {
        [Description("warrantyOrder_Pending")]
        Pending = 0,
        [Description("warrantyOrder_billTemp")]
        Draft = 1,
        [Description("warrantyOrder_delivering")]
        Ongoing = 2,
        [Description("warrantyOrder_completeOrder")]
        Finalized = 3,
        [Description("warrantyOrder_cancelled")]
        Void = 4
    }
}
﻿using System.Collections.Generic;

namespace Demo.OmniChannel.ShareKernel.Common
{
    public class KvSqlConnectString
    {
        public string KvChannelEntities { get; set; }
        public string KvMasterEntities { get; set; }
        public List<KvEntity> KvEntitiesDC1 { get; set; } = new List<KvEntity>();
        public List<KvEntity> KvEntitiesDC2 { get; set; } = new List<KvEntity>();
        public List<KvEntity> KvEntitiesDC1p { get; set; } = new List<KvEntity>();
    }

    public class KvEntity
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
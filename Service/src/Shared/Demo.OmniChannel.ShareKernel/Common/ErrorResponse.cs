﻿namespace Demo.OmniChannel.ShareKernel.Common
{
    public class ErrorResponse
    {
        public string RequestId { get; set; }
        public string Message { get; set; }
    }
}
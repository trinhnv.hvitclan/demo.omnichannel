﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Demo.OmniChannel.ShareKernel.Models;

namespace Demo.OmniChannel.ShareKernel.Common
{
    public static class Helpers
    {
        private static readonly CultureInfo _defaultCultureInfo = new CultureInfo("en-US");
        public static List<FeeDetail> ConvertFeeToList<TEntity>(TEntity entity, List<string> fees)
        {
            var feeList = new List<FeeDetail>();
            if (entity == null) return new List<FeeDetail>();
            var properties = entity.GetType().GetProperties();
            foreach (var pi in properties)
            {
                var feeName = pi.Name;
                var feeValueObject = pi.GetValue(entity, null);
                var feeValue = (float)0;
                if (!fees.Contains(feeName)) continue;
                if (feeValueObject != null)
                    feeValue = (float)feeValueObject;

                var feeItem = new FeeDetail()
                {
                    Name = feeName,
                    Value = feeValue
                };

                feeList.Add(feeItem);
            }

            return feeList;
        }
        public static long GetMilisecondsDiff(DateTime endTime, DateTime startTime)
        {
            long totalEndTimeMs = (Int64)(endTime.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
            long totalStartTimeMs = (Int64)(startTime.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
            return totalEndTimeMs - totalStartTimeMs;
        }

        public static string GenLockProductKey(long retailerId, int branchId, long productId)
        {
            return string.Format(KvConstant.LockCreateInvoice, retailerId, branchId, productId);
        }

        public static float ToFloat(object val, float defValue)
        {
            var input = Convert.ToString(val, _defaultCultureInfo);
            return float.TryParse(input, NumberStyles.Any, _defaultCultureInfo, out var parsedResult) ? parsedResult : defValue;
        }

        public static decimal ToDecimal(object val, decimal defValue)
        {
            var input = Convert.ToString(val, _defaultCultureInfo);
            return decimal.TryParse(input, NumberStyles.Any, _defaultCultureInfo, out var parsedResult) ? parsedResult : defValue;
        }

        public static long ToLong(object val, long defValue)
        {
            var input = Convert.ToString(val, _defaultCultureInfo);
            return long.TryParse(input, NumberStyles.Any, _defaultCultureInfo, out var parsedResult) ? parsedResult : defValue;
        }

        public static DateTime GmtToVietNamTimeZone(DateTime dateTime)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(dateTime,
                TimeZoneInfo.FindSystemTimeZoneById("Asia/Ho_Chi_Minh"));
        }
    }
}
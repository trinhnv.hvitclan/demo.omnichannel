﻿
using System;

namespace Demo.OmniChannel.ShareKernel.Common
{
    public static class SyncHelper
    {
        public static double? CalculateOnHandFormula(byte syncOnHandFormula, double onHand, double reserved, double onOrder)
        {
            double? retVal = null;
            switch (syncOnHandFormula)
            {
                case (byte)SyncOnHandFormula.OnHand:
                    {
                        retVal = onHand;
                        break;
                    }

                case (byte)SyncOnHandFormula.SubOrder:
                    {
                        retVal = onHand - reserved > 0
                            ? onHand - reserved
                            : 0;
                        break;
                    }

                case (byte)SyncOnHandFormula.AddSupplierOrder:
                    {
                        retVal = onOrder + onHand > 0
                            ? onOrder + onHand
                            : 0;
                        break;
                    }

                case (byte)SyncOnHandFormula.SubOrderAddSupplierOrder:
                    {
                        retVal = (onHand + onOrder) - reserved > 0
                            ? (onHand + onOrder) - reserved
                            : 0;
                        break;
                    }
            }

            retVal = retVal < 0 ? 0 : retVal;
            return retVal;
        }

        public static DateTime? CalExpireWarranty(int timeType, int numberTime, DateTime purchaseDate)
        {
            var today = purchaseDate;

            if (timeType == (int)WarrantyTimeType.Month)
            {
                return today.AddMonths(numberTime);
            }
            else if (timeType == (int)WarrantyTimeType.Day)
            {
                return today.AddDays(numberTime);
            }
            else if (timeType == (int)WarrantyTimeType.Year)
            {
                return today.AddYears(numberTime);
            }

            return null;
        }
    }
}
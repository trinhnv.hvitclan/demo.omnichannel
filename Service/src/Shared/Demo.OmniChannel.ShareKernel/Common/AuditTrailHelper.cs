﻿using Demo.OmniChannel.ShareKernel.Models;
using System;
using System.Linq;
using System.Text;

namespace Demo.OmniChannel.ShareKernel.Common
{
    public class AuditTrailHelper
    {
        public static int GetAuditTrailFunctionType(byte? channelType)
        {
            var type = channelType != null ? (ChannelType)channelType : ChannelType.Shopee;
            FunctionType retVal;
            switch (type)
            {
                case ChannelType.Lazada:
                    retVal = FunctionType.LazadaInteration;
                    break;
                case ChannelType.Shopee:
                    retVal = FunctionType.ShopeeInteration;
                    break;
                case ChannelType.Tiktok:
                    retVal = FunctionType.TiktokInteration;
                    break;
                case ChannelType.Tiki:
                    retVal = FunctionType.TikiInteration;
                    break;
                case ChannelType.Sendo:
                    retVal = FunctionType.SendoInteration;
                    break;
                default:
                    retVal = FunctionType.ShopeeInteration;
                    break;

            }
            return (int)retVal;
        }

        public static StringBuilder GetMappingProductContent(ProductMappingAuditTrail req, bool isDeleteMapping, bool isGetTitle = true)
        {
            var logContent = new StringBuilder();

            if (req == null || req.Items?.Any() != true) return logContent;

            if (isGetTitle) logContent.Append(isDeleteMapping ? "Hủy bỏ liên kết hàng hóa: <br/>" : "Thêm mới liên kết hàng hóa: <br/>");

            var channelTypeText = req.ChannelType != null ? Enum.GetName(typeof(ChannelType), req.ChannelType) : string.Empty;
            logContent.Append($"{channelTypeText} shop: {req.ChannelName} <br/>");

            var editProductUrlParam = GetEditProductUrlParam(req.ChannelType);  // Khi hiển thị dùng key này để replace đúng url

            foreach (var item in req.Items)
            {
                var idInUrl = req.ChannelType == (byte)ChannelType.Tiki ? item.ProductChannelId : item.ParentProductChannelId;
                logContent.Append($"- [ProductCode]{item.ProductKvSku}[/ProductCode] liên kết với <a href='{editProductUrlParam}{idInUrl}' target='_blank'>{item.ProductChannelId}</a> - {item.ProductChannelSku}<br/>");
            }

            return logContent;
        }
        public static StringBuilder GetAutoMappingProductContent(ProductAutoMappingAuditTrail item,string channelName)
        {
            var logContent = new StringBuilder();
            if (item == null) return logContent;
            var editProductUrlParam = GetEditProductUrlParam(null);  // Khi hiển thị dùng key này để replace đúng url
            logContent.Append("Liên kết hàng hóa khi có đơn hàng mới có hàng hóa chưa liên kết: <br/>");
            logContent.Append($"Shopee shop: {channelName} <br/>");
            logContent.Append($"- [ProductCode]{item.ProductKvSku}[/ProductCode] liên kết với <a href='{editProductUrlParam}{item.CommonParentProductChannelId}' target='_blank'>{item.CommonProductChannelId}</a> - {item.ProductChannelSku}<br/>");
            return logContent;
        }
        public static StringBuilder GetCreateProductContent(CreateProductAuditTrail req)
        {
            var logContent = new StringBuilder();
            if (req == null) return logContent;

            logContent.Append("Thêm mới hàng hóa khi có đơn hàng mới có hàng hóa chưa liên kết: <br/>");
            var information = $"[ProductCode]{req.Code} [/ProductCode](mã tạo tự động), tên: {req.Name}, nhóm hàng: khác, ";
            logContent.Append(information);

            var price = $"đơn giá: {req.BasePrice.ToString("#,##0")}, Tồn kho: {req.OnHand}, Quản lý theo lô, ";
            logContent.Append(price);
            var extend = $"Hạn sử dụng: {(req.IsExpired ? "Có" : "Không")}. " +
                $"Được bán trực tiếp: {(req.AllowsSale ? "Có" : "Không")}, " +
                $"Tích điểm: {((req.IsRewardPoint ?? false) ? "Có" : "Không")} <br/>";
            logContent.Append(extend);
            logContent.Append($"Giá vốn: {req.Price.ToString("#,##0")} Áp dụng Toàn hệ thống");

            return logContent;
        }

        private static string GetEditProductUrlParam(byte? channelType)
        {
            var type = channelType != null ? (ChannelType)channelType : ChannelType.Shopee;
            switch (type)
            {
                case ChannelType.Lazada:
                    return "LazadaEditProductUrlParam";
                case ChannelType.Tiki:
                    return "TikiEditProductUrlParam";
                case ChannelType.Shopee:
                    return "ShopeeEditProductUrlParam";
                case ChannelType.Sendo:
                    return "SendoEditProductUrlParam";
                case ChannelType.Tiktok:
                    return "TiktokEditProductUrlParam";
                default:
                    return "ShopeeEditProductUrlParam";
            }
        }
    }
}
﻿namespace Demo.OmniChannel.ShareKernel.Common
{
    public class ShardConnect
    {
        public int Shard { get; set; }
        public string Prefix { get; set; }
    }
}
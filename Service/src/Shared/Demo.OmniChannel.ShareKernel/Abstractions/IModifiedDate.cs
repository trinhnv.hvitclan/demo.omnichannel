﻿using System;

namespace Demo.OmniChannel.ShareKernel.Abstractions
{
    public interface IModifiedDate
    {
        DateTime? ModifiedDate { get; set; }
    }
}
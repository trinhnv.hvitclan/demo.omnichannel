﻿namespace Demo.OmniChannel.ShareKernel.Abstractions
{
    public interface IAppId
    {
        long OmniChannelId { get; set; }
    }
}
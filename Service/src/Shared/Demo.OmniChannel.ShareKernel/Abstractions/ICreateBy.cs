﻿namespace Demo.OmniChannel.ShareKernel.Abstractions
{
    public interface ICreateBy
    {
        long CreateBy { get; set; }
    }
}
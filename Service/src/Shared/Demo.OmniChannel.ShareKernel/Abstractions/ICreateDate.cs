﻿using System;

namespace Demo.OmniChannel.ShareKernel.Abstractions
{
    public interface ICreateDate
    {
        DateTime CreatedDate { get; set; }
    }
}
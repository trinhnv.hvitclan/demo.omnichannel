﻿namespace Demo.OmniChannel.ShareKernel.Abstractions
{
    public interface IBranch
    {
        int BranchId { get; set; }
    }
}
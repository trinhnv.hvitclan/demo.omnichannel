﻿namespace Demo.OmniChannel.ShareKernel.Abstractions
{
    public interface IModifiedBy
    {
        long? ModifiedBy { get; set; }
    }
}
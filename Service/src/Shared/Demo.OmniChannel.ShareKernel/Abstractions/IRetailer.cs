﻿namespace Demo.OmniChannel.ShareKernel.Abstractions
{
    public interface IRetailer
    {
        int RetailerId { get; set; }
    }
}
﻿namespace Demo.OmniChannel.ShareKernel.Abstractions
{
    public interface IDeleted
    {
        bool? IsDeleted { get; set; }
    }
}
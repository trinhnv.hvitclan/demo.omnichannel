﻿namespace Demo.OmniChannel.ShareKernel.Abstractions
{
    public interface IEntityId
    {
        long Id { get; set; }
    }
}
﻿using Newtonsoft.Json;

namespace Demo.OmniChannel.ShareKernel.RedisStreamMessage
{
    public class TiktokDeadLetterQueueCreateOrder : DeadLetterQueueCreateOrder
    {
        [JsonProperty(PropertyName = "order")]
        public object Order { get; set; }

        [JsonProperty(PropertyName = "logistics")]
        public TiktokLogistics Logistics { get; set; }

        [JsonProperty(PropertyName = "reverse")]
        public TiktokReverse Reverse { get; set; }
    }
}

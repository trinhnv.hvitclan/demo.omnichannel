﻿using Newtonsoft.Json;

namespace Demo.OmniChannel.ShareKernel.RedisStreamMessage
{
    public class TiktokCreateOrderStreamMessage : CreateOrderMessage
    {
        [JsonProperty(PropertyName = "order")]
        public object Order { get; set; }
        [JsonProperty(PropertyName = "logistics")]
        public TiktokLogistics Logistics { get; set; }
        [JsonProperty(PropertyName = "reverse")]
        public TiktokReverse Reverse { get; set; }
    }

    public class TiktokLogistics
    {
        [JsonProperty(PropertyName = "shippingInfo")]
        public object TitokShippingInfo { get; set; }
    }
    public class TiktokReverse
    {
        [JsonProperty(PropertyName = "reverseInfo")]
        public object TiktokReverseInfo { get; set; }
    }
}

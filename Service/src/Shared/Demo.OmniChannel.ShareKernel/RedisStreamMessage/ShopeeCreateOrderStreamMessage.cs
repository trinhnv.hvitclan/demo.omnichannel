﻿using Demo.OmniChannel.ShareKernel.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Demo.OmniChannel.ShareKernel.RedisStreamMessage
{
    #region Shopee

    public class ShopeeRetryErrorOrderStreamMessage : CreateOrderMessage
    {
        public int RetailerId { get; set; }
        public int BranchId { get; set; }
        public int ChannelId { get; set; }
        public string KvOrder { get; set; }
    }

    public class ShopeeSyncErrorOrderStreamMessage : CreateOrderMessage
    {
        public int RetailerId { get; set; }
        public int BranchId { get; set; }
        public int ChannelId { get; set; }
        public string KvOrder { get; set; }
    }


    public class ShopeeCreateOrderKafkaMessageV2 : CreateOrderMessage
    {
        [JsonProperty(PropertyName = "logId")]
        public override Guid LogId { get; set; }


        [JsonProperty(PropertyName = "order")]
        public OrderDetailV2 Order { get; set; }


        [JsonProperty(PropertyName = "tracking")]
        public ShopeeTrackingInfoV2Response Tracking { get; set; }


        [JsonProperty(PropertyName = "escrowDetails")]
        public ShopeePaymentEscrowDetailResponse PaymentEscrowDetail { get; set; }

    }

    public class Income
    {
        [JsonProperty(PropertyName = "order_income")]
        public OrderIncome OrderIncome { get; set; }
    }

    public class Order
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is mandatory")]
        [JsonProperty(PropertyName = "ordersn")]
        public string OrderSn { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is mandatory")]
        [JsonProperty(PropertyName = "recipient_address")]
        public RecipientAddress RecipientAddress { get; set; }

        [JsonProperty(PropertyName = "message_to_seller")]
        public string MessageToSeller { get; set; }

        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is mandatory")]
        [JsonProperty(PropertyName = "create_time")]
        public long CreateTime { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is mandatory")]
        [JsonProperty(PropertyName = "items")]
        public List<OrderDetailItem> OrderDetailItem { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is mandatory")]
        [JsonProperty(PropertyName = "order_status")]
        public string OrderStatus { get; set; }

        [JsonProperty(PropertyName = "tracking_no")]
        public string TrackingNo { get; set; }

        [JsonProperty(PropertyName = "buyer_username")]
        public string BuyerUsername { get; set; }
    }

    public class OrderDetailItem
    {
        [JsonProperty(PropertyName = "item_sku")]
        public string ItemSku { get; set; }

        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }

        [JsonProperty(PropertyName = "variation_sku")]
        public string VariationSku { get; set; }

        [JsonProperty(PropertyName = "variation_id")]
        public long VariationId { get; set; }

        [JsonProperty(PropertyName = "variation_name")]
        public string VariationName { get; set; }

        [JsonProperty(PropertyName = "variation_original_price")]
        public float VariationOriginalPrice { get; set; }

        [JsonProperty(PropertyName = "item_name")]
        public string ItemName { get; set; }

        [JsonProperty(PropertyName = "variation_quantity_purchased")]
        public int VariationQuantityPurchased { get; set; }

        [JsonProperty(PropertyName = "variation_discounted_price")]
        public float? VariationDiscountedPrice { get; set; }

        [JsonProperty(PropertyName = "promotion_type")]
        public string PromotionType { get; set; }

        [JsonProperty(PropertyName = "add_on_deal_id")]
        public long AddOnDealId { get; set; }

    }

    public class OrderIncome
    {
        [Description("Phí vận chuyển (không tính trợ giá)")]
        public float TotalShippingFee => -(FinalShippingFee + BuyerPaidShippingFee);

        [JsonProperty(PropertyName = "final_shipping_fee"), Description("Phí vận chuyển thực tế")]
        public float FinalShippingFee { get; set; }

        [JsonProperty(PropertyName = "commission_fee"), Description("Phí cố định")]
        public float CommissionFee { get; set; }

        [JsonProperty(PropertyName = "service_fee"), Description("Phí dịch vụ")]
        public float ServiceFee { get; set; }

        [JsonProperty(PropertyName = "seller_transaction_fee"), Description("Phí thanh toán")]
        public float SellerTransactionFee { get; set; }

        [JsonProperty(PropertyName = "buyer_paid_shipping_fee"), Description("phí vận chuyển ng mua trả")]
        public float BuyerPaidShippingFee { get; set; }
        [JsonProperty(PropertyName = "seller_coin_cash_back"), Description("Giá trị giảm giá hoàn xu")]
        public float SellerCoinCashBack { get; set; }
    }

    public class MyIncome
    {
        [JsonProperty(PropertyName = "ordersn")]
        public string OrderSn { get; set; }
        [JsonProperty(PropertyName = "income_details")]
        public IncomeDetail IncomeDetail { get; set; }
        [JsonProperty(PropertyName = "items")]
        public List<ItemDetail> Items { get; set; }
        [JsonProperty(PropertyName = "activity")]
        public List<ActivityDetail> ActivityDetails { get; set; }
    }

    public class ItemDetail
    {
        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }

        [JsonProperty(PropertyName = "item_sku")]
        public string ItemSku { get; set; }

        [JsonProperty(PropertyName = "variation_sku")]
        public string VariationSku { get; set; }

        [JsonProperty(PropertyName = "variation_id")]
        public long VariationId { get; set; }

        [JsonProperty(PropertyName = "discounted_price")]
        public float DiscountedPrice { get; set; }

        [JsonProperty(PropertyName = "quantity_purchased")]
        public int QuantityPurchased { get; set; }

        [JsonProperty(PropertyName = "add_on_deal_id")]
        public long AddOnDealId { get; set; }
    }

    public class IncomeDetail
    {
        [JsonProperty(PropertyName = "voucher_seller"), Description("Giảm giá")]
        public float VoucherSeller { get; set; }
        [JsonProperty(PropertyName = "seller_rebate")]
        public float SellerRebate { get; set; }
    }

    public class ActivityDetail
    {
        [JsonProperty(PropertyName = "discounted_price")]
        public float DiscountedPrice { get; set; }
        [JsonProperty(PropertyName = "original_price")]
        public float OriginalPrice { get; set; }
        [JsonProperty(PropertyName = "items")]
        public List<ActivityItem> ActivityItems { get; set; }
        [JsonProperty(PropertyName = "activity_type")]
        public string ActivityType { get; set; }
    }

    public class ActivityItem
    {
        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }
        [JsonProperty(PropertyName = "variation_id")]
        public long VariationId { get; set; }
        [JsonProperty(PropertyName = "original_price")]
        public float OriginalPrice { get; set; }
        [JsonProperty(PropertyName = "quantity_purchased")]
        public int QuantityPurchased { get; set; }
    }

    public class BankAccount
    {
    }


    public class TrackingInfo
    {
        [JsonProperty(PropertyName = "ctime")]
        public int Ctime { get; set; }
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
    }

    public class Tracking
    {
        [JsonProperty(PropertyName = "tracking_number")]
        public string TrackingNumber { get; set; }
        [JsonProperty(PropertyName = "tracking_info")]
        public List<TrackingInfo> TrackingInfos { get; set; }
        [JsonProperty(PropertyName = "logistics_status")]
        public string LogisticsStatus { get; set; }



    }

    public class RecipientAddress
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is mandatory")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is mandatory")]
        [JsonProperty(PropertyName = "phone")]
        public string Phone { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is mandatory")]
        [JsonProperty(PropertyName = "city")]
        public string City { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is mandatory")]
        [JsonProperty(PropertyName = "district")]
        public string District { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is mandatory")]
        [JsonProperty(PropertyName = "state")]
        public string State { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is mandatory")]
        [JsonProperty(PropertyName = "full_address")]
        public string FullAddress { get; set; }
    }

    #endregion
}

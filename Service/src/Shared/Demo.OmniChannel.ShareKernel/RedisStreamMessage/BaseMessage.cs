﻿using Newtonsoft.Json;
using System;

namespace Demo.OmniChannel.ShareKernel.RedisStreamMessage
{
    public class BaseMessage
    {
        public string Id { get; set; }
        public virtual Guid LogId { get; set; }
    }

    #region Base 
    public abstract class CreateOrderMessage : BaseMessage
    {

        [JsonProperty(PropertyName = "orderId")]
        public string OrderSn { get; set; }

        [JsonProperty(PropertyName = "webhookTimestamp")]
        public long? Timestamps { get; set; }

        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [JsonProperty(PropertyName = "source")]
        public string Source { get; set; }

        [JsonProperty(PropertyName = "shopId")]
        public string ShopId { get; set; }

        [JsonProperty(PropertyName = "retryProcessService")]
        public bool RetryProcessService { get; set; }

        [JsonProperty(PropertyName = "logId")]
        public override Guid LogId { get; set; }

    }

    public class BaseMessageInvoice
    {
        public long ChannelId { get; set; }
        public byte ChannelType { get; set; }
        public string ChannelName { get; set; }
        public string PriceBookName { get; set; }
        public string SalePriceBookName { get; set; }
        public int RetailerId { get; set; }
        public string RetailerCode { get; set; }
        public bool IsIgnoreAuditTrail { get; set; }
        public int BranchId { get; set; }
        public DateTime? SentTime { get; set; }
        public Guid LogId { get; set; }
        public long PlatformId { get; set; }
    }

    public class CreateInvoiceMessage : BaseMessageInvoice
    {
        public string OrderId { get; set; }
        public string Order { get; set; }
        public string KvEntities { get; set; }
        public long PriceBookId { get; set; }
        public string KvOrder { get; set; }
        public long KvOrderId { get; set; }
        public int RetryCount { get; set; }
        public bool IsDbException { get; set; }
        public string InvoiceCode { get; set; }

        public string OrderSn { get; set; }
        public long Timestamps { get; set; }
        public string CodeSource { get; set; }
        public string Source { get; set; }
        public string ShopId { get; set; }
        public int ProcessType { get; set; }
    }
    #endregion
}

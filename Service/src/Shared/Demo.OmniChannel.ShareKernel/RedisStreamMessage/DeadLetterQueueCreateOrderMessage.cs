﻿using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.ShareKernel.Models;
using Newtonsoft.Json;
using System;

namespace Demo.OmniChannel.ShareKernel.RedisStreamMessage
{
    public class DeadLetterQueueCreateOrder : BaseKafkaModel
    {
        [JsonProperty(PropertyName = "shopId")]
        public long ShopId { get; set; }
        [JsonProperty(PropertyName = "RetryCount")]
        public int RetryCount { get; set; }

        [JsonProperty(PropertyName = "logId")]
        public Guid LogId { get; set; }
        [JsonProperty(PropertyName = "webhookTimestamp")]
        public string Timestamps { get; set; }
        [JsonProperty(PropertyName = "retryProcessService")]
        public bool RetryProcessService { get; set; }
    }
    public class ShopeeDeadLetterQueueCreateOrder : DeadLetterQueueCreateOrder
    {
        [JsonProperty(PropertyName = "order")]
        public Order Order { get; set; }

        [JsonProperty(PropertyName = "income")]
        public Income Income { get; set; }

        [JsonProperty(PropertyName = "myIncome")]
        public MyIncome MyIncome { get; set; }

        [JsonProperty(PropertyName = "tracking")]
        public Tracking Tracking { get; set; }
    }

    public class ShopeeDeadLetterQueueCreateOrderV2 : DeadLetterQueueCreateOrder
    {

        [JsonProperty(PropertyName = "order")]
        public OrderDetailV2 Order { get; set; }


        [JsonProperty(PropertyName = "tracking")]
        public ShopeeTrackingInfoV2Response Tracking { get; set; }


        [JsonProperty(PropertyName = "escrowDetails")]
        public ShopeePaymentEscrowDetailResponse PaymentEscrowDetail { get; set; }
    }
}
﻿using Demo.OmniChannel.ShareKernel.KafkaMessage;
using System;

namespace Demo.OmniChannel.ShareKernel.RedisStreamMessage
{
    public class BaseDeadLetterQueueCreateInvoice : BaseKafkaModel
    {
        public long ChannelId { get; set; }
        public byte ChannelType { get; set; }
        public string ChannelName { get; set; }
        public string PriceBookName { get; set; }
        public string SalePriceBookName { get; set; }
        public int RetailerId { get; set; }
        public string RetailerCode { get; set; }
        public bool IsIgnoreAuditTrail { get; set; }
        public int BranchId { get; set; }
        public DateTime? SentTime { get; set; }
        public Guid LogId { get; set; }
        public long PlatformId { get; set; }
    }

    public class DeadLetterQueueCreateInvoiceMessage : BaseDeadLetterQueueCreateInvoice
    {
        public string OrderId { get; set; }
        public string Order { get; set; }
        public string KvEntities { get; set; }
        public long PriceBookId { get; set; }
        public string KvOrder { get; set; }
        public long KvOrderId { get; set; }
        public int RetryCount { get; set; }
        public bool IsDbException { get; set; }
        public string InvoiceCode { get; set; }
    }
}
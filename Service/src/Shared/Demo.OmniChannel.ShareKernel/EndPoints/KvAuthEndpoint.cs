﻿namespace Demo.OmniChannel.ShareKernel.EndPoints
{
    public class KvAuthEndpoint
    {
        //Token
        public const string GetToken = "/token";

        //warranty
        public const string GetWarranty = "/api/warranty";
    }
}

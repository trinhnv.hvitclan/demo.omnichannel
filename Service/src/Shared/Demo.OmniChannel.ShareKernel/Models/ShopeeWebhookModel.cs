﻿using Newtonsoft.Json;

namespace Demo.OmniChannel.ShareKernel.Models
{
    public class ShopeeWebhookModel
    {
        [JsonProperty(PropertyName = "shop_id")]
        public long ShopId { get; set; }
        [JsonProperty(PropertyName = "code")]
        public long Code { get; set; }
        [JsonProperty(PropertyName = "data")]
        public DataBody Data { get; set; }
        [JsonProperty(PropertyName = "timestamp")]
        public long Timestamp { get; set; }
    }

    public class DataBody
    {
        [JsonProperty(PropertyName = "ordersn")]
        public string Ordersn { get; set; }
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
        [JsonProperty(PropertyName = "update_time")]
        public long UpdateTime { get; set; }
    }
}

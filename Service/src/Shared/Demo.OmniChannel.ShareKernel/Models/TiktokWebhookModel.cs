﻿using Newtonsoft.Json;

namespace Demo.OmniChannel.ShareKernel.Models
{
    public class TiktokWebhookModel
    {
        [JsonProperty(PropertyName = "shop_id")]
        public string ShopId { get; set; }

        [JsonProperty(PropertyName = "type")]
        public int Type { get; set; } = 1;

        [JsonProperty(PropertyName = "timestamp")]
        public long Timestamp { get; set; }

        [JsonProperty(PropertyName = "data")]
        public DataOrder Data { get; set; }
    }

    public class DataOrder
    {
        [JsonProperty(PropertyName = "order_id")]
        public string Ordersn { get; set; }

        [JsonProperty(PropertyName = "order_status")]
        public string OrderStatus { get; set; }

        [JsonProperty(PropertyName = "update_time")]
        public long UpdateTime { get; set; }
    }
}

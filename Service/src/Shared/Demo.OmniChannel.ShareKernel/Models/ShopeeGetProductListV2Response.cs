﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ShareKernel.Models
{
    public class ShopeeGetProductListV2Response
    {
        [JsonProperty("item")]
        public List<ItemGetProducts> Items { get; set; }

        [JsonProperty("total_count")]
        public int TotalCount { get; set; }

        [JsonProperty("has_next_page")]
        public bool HasNextPage { get; set; }
    }

    public class ItemGetProducts
    {
        [JsonProperty("item_id")]
        public long ItemId { get; set; }

        [JsonProperty("item_status")]
        public string ItemStatus { get; set; }

        [JsonProperty("update_time")]
        public int UpdateTime { get; set; }
    }
}

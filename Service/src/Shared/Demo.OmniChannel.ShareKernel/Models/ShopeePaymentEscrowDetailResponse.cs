﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ShareKernel.Models
{
    public class ShopeePaymentEscrowDetailResponse
    {
        [JsonProperty("order_sn")]
        public string OrderSn { get; set; }

        [JsonProperty("buyer_user_name")]
        public string BuyerUserName { get; set; }

        [JsonProperty("return_order_sn_list")]
        public List<string> ReturnOrderSnList { get; set; }

        [JsonProperty("order_income")]
        public OrderIncome OrderIncome { get; set; }
    }

    public class Item
    {
        [JsonProperty("item_id")]
        public long ItemId { get; set; }

        [JsonProperty("item_name")]
        public string ItemName { get; set; }

        [JsonProperty("item_sku")]
        public string ItemSku { get; set; }

        [JsonProperty("model_id")]
        public long ModelId { get; set; }

        [JsonProperty("model_name")]
        public string ModelName { get; set; }

        [JsonProperty("model_sku")]
        public string ModelSku { get; set; }

        [JsonProperty("original_price")]
        public long OriginalPrice { get; set; }

        [JsonProperty("seller_discount")]
        public long SellerDiscount { get; set; }
        [JsonProperty("shopee_discount")]
        public long ShopeeDiscount { get; set; }

        [JsonProperty("discounted_price")]
        public long DiscountedPrice { get; set; }

        [JsonProperty("discount_from_coin")]
        public long DiscountFromCoin { get; set; }

        [JsonProperty("discount_from_voucher_shopee")]
        public long DiscountFromVoucherShopee { get; set; }

        [JsonProperty("discount_from_voucher_seller")]
        public long DiscountFromVoucherSeller { get; set; }

        [JsonProperty("activity_type")]
        public string ActivityType { get; set; }

        [JsonProperty("is_main_item")]
        public bool IsMainItem { get; set; }

        [JsonProperty("activity_id")]
        public long ActivityId { get; set; }

        [JsonProperty("quantity_purchased")]
        public int QuantityPurchased { get; set; }
    }

    public class OrderIncome
    {
        [JsonProperty("escrow_amount")]
        public long EscrowAmount { get; set; }

        [JsonProperty("buyer_total_amount")]
        public long BuyerTotalAmount { get; set; }

        [JsonProperty("original_price")]
        public long OriginalPrice { get; set; }

        [JsonProperty("seller_discount")]
        public long SellerDiscount { get; set; }

        [JsonProperty("shopee_discount")]
        public long ShopeeDiscount { get; set; }

        [JsonProperty("voucher_from_seller")]
        public long VoucherFromSeller { get; set; }

        [JsonProperty("voucher_from_shopee")]
        public long VoucherFromShopee { get; set; }

        [JsonProperty("coins")]
        public long Coins { get; set; }

        [JsonProperty("buyer_paid_shipping_fee")]
        public long BuyerPaidShippingFee { get; set; }

        [JsonProperty("buyer_transaction_fee")]
        public long BuyerTransactionFee { get; set; }

        [JsonProperty("cross_border_tax")]
        public long CrossBorderTax { get; set; }

        [JsonProperty("payment_promotion")]
        public long PaymentPromotion { get; set; }

        [JsonProperty("commission_fee")]
        public long CommissionFee { get; set; }

        [JsonProperty("service_fee")]
        public long ServiceFee { get; set; }

        [JsonProperty("seller_transaction_fee")]
        public long SellerTransactionFee { get; set; }

        [JsonProperty("seller_lost_compensation")]
        public long SellerLostCompensation { get; set; }

        [JsonProperty("seller_coin_cash_back")]
        public long SellerCoinCashBack { get; set; }

        [JsonProperty("escrow_tax")]
        public long EscrowTax { get; set; }

        [JsonProperty("final_shipping_fee")]
        public long FinalShippingFee { get; set; }

        [JsonProperty("actual_shipping_fee")]
        public long ActualShippingFee { get; set; }

        [JsonProperty("shopee_shipping_rebate")]
        public long ShopeeShippingRebate { get; set; }

        [JsonProperty("shipping_fee_discount_from_3pl")]
        public long ShippingFeeDiscountFrom3pl { get; set; }

        [JsonProperty("seller_shipping_discount")]
        public long SellerShippingDiscount { get; set; }

        [JsonProperty("estimated_shipping_fee")]
        public double EstimatedShippingFee { get; set; }

        [JsonProperty("seller_voucher_code")]
        public List<object> SellerVoucherCode { get; set; }

        [JsonProperty("drc_adjustable_refund")]
        public long DrcAdjustableRefund { get; set; }

        [JsonProperty("cost_of_goods_sold")]
        public long CostOfGoodsSold { get; set; }

        [JsonProperty("original_cost_of_goods_sold")]
        public long OriginalCostOfGoodsSold { get; set; }

        [JsonProperty("original_shopee_discount")]
        public long OriginalShopeeDiscount { get; set; }

        [JsonProperty("seller_return_refund")]
        public double SellerReturnRefund { get; set; }

        [JsonProperty("reverse_shipping_fee")]
        public long ReverseShippingFee { get; set; }

        [JsonProperty("items")]
        public List<Item> Items { get; set; }
    }
}

﻿using Newtonsoft.Json;

namespace Demo.OmniChannel.ShareKernel.Models
{
    public class ShopeeProductResponse
    {
        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }
        [JsonProperty(PropertyName = "msg")]
        public string Message { get; set; }
        [JsonProperty(PropertyName = "message")]
        public string MessageV2 { get; set; }
        [JsonProperty(PropertyName = "item")]
        public ProductItemResponse Item { get; set; }
        [JsonProperty(PropertyName = "request_id")]
        public string RequestId { get; set; }
        [JsonProperty(PropertyName = "error")]
        public string Error { get; set; }
        [JsonProperty(PropertyName = "shop_id")]
        public string ShopId { get; set; }
        [JsonProperty(PropertyName = "response")]
        public ShopeeProductModelReponseV2 Response { get; set; }
    }

    public class ShopeeProductModelReponseV2
    {
        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }
    }
}

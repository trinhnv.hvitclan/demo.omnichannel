﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Demo.OmniChannel.ShareKernel.Models
{
    public class ProductItemResponse
    {
        [JsonProperty(PropertyName = "shopid")]
        public long ShopId { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "variation_id")]
        public long VariationId { get; set; }
        [JsonProperty(PropertyName = "variation_sku")]
        public string VariationSku { get; set; }
        [JsonProperty(PropertyName = "item_sku")]
        public string ItemSku { get; set; }
        [JsonProperty(PropertyName = "images")]
        public List<string> Images { get; set; }
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
        [JsonProperty(PropertyName = "price")]
        public float Price { get; set; }
        [JsonProperty(PropertyName = "stock")]
        public int Stock { get; set; }
        [JsonProperty(PropertyName = "create_time")]
        public int CreateTime { get; set; }
        [JsonProperty(PropertyName = "update_time")]
        public int UpdateTime { get; set; }
        [JsonProperty(PropertyName = "variations")]
        public List<ProductItemResponse> Variations { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ShareKernel.Models
{
    public class ShopeeTrackingInfoV2Response
    {
        [JsonProperty("order_sn")]
        public string OrderSn { get; set; }

        [JsonProperty("package_number")]
        public string PackageNumber { get; set; }

        [JsonProperty("logistics_status")]
        public string LogisticsStatus { get; set; }

        [JsonProperty("tracking_info")]
        public List<TrackingInfo> TrackingInfo { get; set; }

        [JsonProperty("tracking_number")]
        public string TrackingNumber { get; set; }
    }

    public class TrackingInfo
    {
        [JsonProperty("update_time")]
        public long UpdateTime { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("logistics_status")]
        public string LogisticsStatus { get; set; }
    }
}

﻿using Newtonsoft.Json;

namespace Demo.OmniChannel.ShareKernel.Models
{
    public class ImgUpload
    {
        [JsonProperty(PropertyName = "image_url")]
        public string ImageUrl { get; set; }
        [JsonProperty(PropertyName = "shopee_image_url")]
        public string ShopeeImageUrl { get; set; }
        [JsonProperty(PropertyName = "error_desc")]
        public string ErrorDesc { get; set; }
    }
}

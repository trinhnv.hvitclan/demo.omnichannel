﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Demo.OmniChannel.ShareKernel.Models
{
    public class ShopeeUploadImgResponse
    {
        [JsonProperty(PropertyName = "images")]
        public List<ImgUpload> Images { get; set; }
        [JsonProperty(PropertyName = "request_id")]
        public string RequestId { get; set; }
        [JsonProperty(PropertyName = "shopid")]
        public string ShopId { get; set; }
        [JsonProperty(PropertyName = "error")]
        public string Error { get; set; }
        [JsonProperty(PropertyName = "msg")]
        public string Msg { get; set; }
    }
}

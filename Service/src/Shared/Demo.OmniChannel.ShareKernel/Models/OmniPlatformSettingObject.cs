﻿using System.Collections.Generic;
using System.Reflection;

namespace Demo.OmniChannel.ChannelClient.Models
{
    public class OmniPlatformSettingObject
    {
        public Dictionary<string, string> Data { get; set; }

        public bool IsShopeeSyncCustomer
        {
            get
            {
                bool.TryParse(GetPropValue(MethodBase.GetCurrentMethod().Name, true), out var boolParse);
                return boolParse;
            }
            set => SetPropValue(MethodBase.GetCurrentMethod().Name, value);
        }
        public bool IsLazadaSyncCustomer
        {
            get
            {
                bool.TryParse(GetPropValue(MethodBase.GetCurrentMethod().Name, true), out var boolParse);
                return boolParse;
            }
            set => SetPropValue(MethodBase.GetCurrentMethod().Name, value);
        }
        public bool IsTikiSyncCustomer
        {
            get
            {
                bool.TryParse(GetPropValue(MethodBase.GetCurrentMethod().Name, true), out var boolParse);
                return boolParse;
            }
            set => SetPropValue(MethodBase.GetCurrentMethod().Name, value);
        }
        public bool IsSendoSyncCustomer
        {
            get
            {
                bool.TryParse(GetPropValue(MethodBase.GetCurrentMethod().Name, true), out var boolParse);
                return boolParse;
            }
            set => SetPropValue(MethodBase.GetCurrentMethod().Name, value);
        }
        public bool IsTiktokShopSyncCustomer
        {
            get
            {
                bool.TryParse(GetPropValue(MethodBase.GetCurrentMethod().Name, true), out var boolParse);
                return boolParse;
            }
            set => SetPropValue(MethodBase.GetCurrentMethod().Name, value);
        }

        public string GetPropValue(string propName, object def)
        {
            propName = propName.Replace("get_", "").Replace("set_", "");
            var val = def?.ToString();
            if (Data != null && Data.ContainsKey(propName))
            {
                val = Data[propName];
            }
            return val;
        }
        public void SetPropValue(string propName, object value)
        {
            propName = propName.Replace("get_", "").Replace("set_", "");
            if (Data.ContainsKey(propName))
            {
                Data[propName] = value.ToString();
            }
            else
            {
                Data.Add(propName, value.ToString());
            }
        }
    }
}

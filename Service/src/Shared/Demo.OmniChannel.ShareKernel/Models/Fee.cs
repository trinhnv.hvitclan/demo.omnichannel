﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Demo.OmniChannel.ShareKernel.Models
{
   
    public class Fee
    {
        [Description("Danh sách các phí")]
        public List<FeeDetail> FeeList { get; set; }
        public int ChannelType { get; set; }
        public float TotalFee()
        {
            var totalFee = FeeList.Aggregate((float) 0, (current, fee) => current + fee.Value);
            return totalFee > 0 ? totalFee : 0;
        }
    }

    public class FeeDetail
    {
        [Description("Tên phí")]
        public string Name { get; set; }

        [Description("Giá trị")]
        public float Value { get; set; }
    }
}

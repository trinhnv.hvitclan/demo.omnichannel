﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Demo.OmniChannel.ShareKernel.Models
{

    public class ShopeeGetProductBaseInfoResponse
    {
        [JsonProperty("item_list")]
        public List<ItemListBase> ItemList { get; set; }
    }
    public class ItemListBase
    {
        [JsonProperty("item_id")]
        public long ItemId { get; set; }

        [JsonProperty("item_name")]
        public string ItemName { get; set; }

        [JsonProperty("item_sku")]
        public string ItemSku { get; set; }

        [JsonProperty("item_status")]
        public string ItemStatus { get; set; }

        [JsonProperty("has_model")]
        public bool HasModel { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("price_info")]
        public List<PriceInfo> PriceInfo { get; set; }

        [JsonProperty("stock_info_v2")]
        public StockInfoV2 StockInfoV2 { get; set; }

        [JsonProperty("image")]
        public Image Images { get; set; }
    }

    public class Image
    {
        [JsonProperty("image_url_list")]
        public List<string> ImageUrlList { get; set; }
    }

    public class PriceInfo
    {
        [JsonProperty("current_price")]
        public float CurrentPrice { get; set; }
    }
    public class StockInfoV2
    {
        [JsonProperty("seller_stock")]
        public List<StockInfoV2SellerStock> SellerStocks { get; set; }
    }

    public class StockInfoV2SellerStock
    {
        [JsonProperty("location_id")]
        public string LocationId { get; set; }

        [JsonProperty("stock")]
        public int Stock { get; set; }
    }

}

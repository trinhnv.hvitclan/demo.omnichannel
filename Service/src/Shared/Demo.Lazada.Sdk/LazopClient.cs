﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Demo.Lazada.Sdk.Models;
using Demo.Lazada.Sdk.Utils;
using RestSharp;

namespace Demo.Lazada.Sdk
{
    public class LazopClient : ILazopClient
    {
        internal string serverUrl;
        internal string appKey;
        internal string appSecret;
        internal string signMethod = Constants.SIGN_METHOD_SHA256;
        internal string sdkVersion = "lazop-sdk-net-20180508";
        internal string logLevel = Constants.LOG_LEVEL_ERROR;

        internal DateTime dt1970 = new DateTime(1970, 1, 1, 0, 0, 0, 0);
        internal IDictionary<string, string> customrParameters; // set client custom params

        public LazopClient(string serverUrl, string appKey, string appSecret)
        {
            this.appKey = appKey;
            this.appSecret = appSecret;
            this.serverUrl = serverUrl;
        }
        public Task<LazopResponse> ExecuteAsync(LazopRequest request)
        {
            throw new System.NotImplementedException();
        }

        public async Task<IRestResponse<LazopResponse>> ExecuteAsync(LazopRequest request, string accessToken, DateTime? timestamp = null)
        {
            LazopDictionary txtParams = new LazopDictionary(request.GetParameters());
            txtParams.Add(Constants.APP_KEY, appKey);
            txtParams.Add(Constants.TIMESTAMP,
                timestamp != null ? GetTimestamp(timestamp.Value) : GetTimestamp(DateTime.UtcNow));
            txtParams.Add(Constants.ACCESS_TOKEN, accessToken);
            txtParams.Add(Constants.PARTNER_ID, sdkVersion);
            txtParams.AddAll(this.customrParameters);
            txtParams.Add(Constants.SIGN_METHOD, this.signMethod);
            txtParams.Add(Constants.SIGN, LazopUtils.SignRequest(request.GetApiName(), txtParams, appSecret, this.signMethod));
            string realServerUrl = GetServerUrl(this.serverUrl, request.GetApiName(), accessToken);
            var client = new RestClient($"{realServerUrl}?{BuildQuery(txtParams)}");
            var restReq = new RestRequest
            {
                Method = request.GetHttpMethod().Equals(nameof(Method.POST)) ? Method.POST : Method.GET
            };
            if (request.GetHttpMethod().Equals("POST"))
            {
                restReq.AddHeader("content-type", "application/x-www-form-urlencoded");
            }
            var response = await client.ExecuteTaskAsync<LazopResponse>(restReq);
            return response;
        }

        public LazapRequestInfo GetLazapRequestInfo(LazopRequest request, string accessToken, DateTime? timestamp = null)
        {
            LazopDictionary txtParams = new LazopDictionary(request.GetParameters());
            txtParams.Add(Constants.APP_KEY, appKey);
            txtParams.Add(Constants.TIMESTAMP,
                timestamp != null ? GetTimestamp(timestamp.Value) : GetTimestamp(DateTime.UtcNow));
            txtParams.Add(Constants.ACCESS_TOKEN, accessToken);
            txtParams.Add(Constants.PARTNER_ID, sdkVersion);
            txtParams.AddAll(this.customrParameters);
            txtParams.Add(Constants.SIGN_METHOD, this.signMethod);
            txtParams.Add(Constants.SIGN, LazopUtils.SignRequest(request.GetApiName(), txtParams, appSecret, this.signMethod));
            string realServerUrl = GetServerUrl(this.serverUrl, request.GetApiName(), accessToken);
            var query = BuildQuery(txtParams);
            return new LazapRequestInfo
            {
                Method = request.GetHttpMethod(),
                Endpoint = realServerUrl,
                Params = query,
                DicParamns = txtParams
            };
        }

        private long GetTimestamp(DateTime dateTime)
        {
            return (dateTime.Ticks - dt1970.Ticks) / 10000;
        }
        internal virtual string GetServerUrl(string serverUrl, string apiName, string session)
        {
            if (string.IsNullOrEmpty(apiName))
            {
                return serverUrl;
            }
            bool hasPrepend = serverUrl.EndsWith("/");
            if (hasPrepend)
            {
                return serverUrl + apiName.Substring(1);
            }
            else
            {
                return serverUrl + apiName;
            }
        }
        public static string BuildQuery(IDictionary<string, string> parameters)
        {
            if (parameters == null || parameters.Count == 0)
            {
                return null;
            }

            StringBuilder query = new StringBuilder();
            bool hasParam = false;

            foreach (KeyValuePair<string, string> kv in parameters)
            {
                string name = kv.Key;
                string value = kv.Value;
                // ignore key or value is empty
                if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(value))
                {
                    if (hasParam)
                    {
                        query.Append("&");
                    }

                    query.Append(name);
                    query.Append("=");
                    query.Append(HttpUtility.UrlEncode(value, Encoding.UTF8));
                    hasParam = true;
                }
            }

            return query.ToString();
        }
    }
}
﻿using System.Collections.Generic;

namespace KiotViet.Lazada.Sdk.FastJson
{
    public sealed class DatasetSchema
    {
        public List<string> Info;//{ get; set; }
        public string Name;//{ get; set; }
    }
}
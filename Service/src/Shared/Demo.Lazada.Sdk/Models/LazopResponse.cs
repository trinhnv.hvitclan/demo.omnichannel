﻿using System.Runtime.Serialization;

namespace Demo.Lazada.Sdk.Models
{
    [DataContract]
    public class LazopResponse
    {
        /// <summary>
        /// error type for error response.
        /// </summary>
        [DataMember(Name = "type")]
        public string Type { get; set; }

        /// <summary>
        /// error code for error response, zero means successful response.
        /// </summary>
        [DataMember(Name = "code")]
        public string Code { get; set; }

        /// <summary>
        /// error message for error response.
        /// </summary>
        [DataMember(Name = "message")]
        public string Message { get; set; }


        /// <summary>
        /// request id for api request.
        /// </summary>
        [DataMember(Name = "request_id")]
        public string RequestId { get; set; }

        /// <summary>
        /// api response
        /// </summary>
        [DataMember(Name = "body")]
        public string Body { get; set; }

        [DataMember(Name = "access_token")]
        public string AccessToken { get; set; }

        [DataMember(Name = "refresh_token")]
        public string RefreshToken { get; set; }

        [DataMember(Name = "account")]
        public string Account { get; set; }

        [DataMember(Name = "refresh_expires_in")]
        public int RefreshExpiresIn { get; set; }

        [DataMember(Name = "expires_in")]
        public int ExpiresIn { get; set; }

        public bool IsError()
        {
            return Code != null && !Code.Equals("0");
        }
    }
}
﻿using System.Collections.Generic;

namespace Demo.Lazada.Sdk.Models
{
    public class LazapRequestInfo
    {
        public string LogId { get; set; }
        public string Endpoint { get; set; }
        public string Method { get; set; }
        public string Header { get; set; }
        public string Body { get; set; }
        public string Params { get; set; }
        public Dictionary<string, string> DicParamns { get; set; }
    }
}

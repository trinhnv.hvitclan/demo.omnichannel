﻿using System;
using System.Threading.Tasks;
using Demo.Lazada.Sdk.Models;
using RestSharp;

namespace Demo.Lazada.Sdk
{
    public interface ILazopClient
    {
        /// <summary>
        /// Execute api request without access token.
        /// </summary>
        /// <param name="request">common api requst</param>
        /// <returns>common response</returns>
        Task<LazopResponse> ExecuteAsync(LazopRequest request);

        /// <summary>
        /// Execute api request with access token.
        /// </summary>
        /// <param name="request">common api requst</param>
        /// <param name="session">user access token</param>
        /// <returns>common respons</returns>
        Task<IRestResponse<LazopResponse>> ExecuteAsync(LazopRequest request, string accessToken, DateTime? timestamp = null);
    }
}
﻿using Demo.OmniChannel.Domain.Model;

namespace Demo.OmniChannel.Domain.ResultModel
{
    public class PriceBookDetailDTO : PriceBookDetail
    {
        public string PriceBookName { get; set; }
    }
}
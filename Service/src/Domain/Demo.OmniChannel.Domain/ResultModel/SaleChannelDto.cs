﻿namespace Demo.OmniChannel.Domain.ResultModel
{
    public class SaleChannelDto
    {
        public SaleChannelDto(int saleChannelId)
        {
            SaleChannelId = saleChannelId;
        }

        public long SaleChannelId { get; set; }
    }
}

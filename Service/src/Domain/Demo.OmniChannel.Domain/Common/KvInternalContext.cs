﻿using ServiceStack.DataAnnotations;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Demo.OmniChannel.Domain.Common
{
    public class KvInternalContext
    {
        public string BaseUrl { get; set; }
        public string OriginalUrl { get; set; }
        public string RetailerCode { get; set; }
        public string Token { get; set; }
        public int BranchId { get; set; }
        public int RetailerId { get; set; }
        public long UserId { get; set; }
        public string Language { get; set; }
        public KvGroup Group { get; set; }
        public Guid LogId { get; set; }
    }

    public class KvGroup
    {
        public int Id { get; set; }
        public string ConnectionString { get; set; }
        public bool? IsFreePremium { get; set; }

        [Ignore]
        [NotMapped]
        public string ConnectionStringName { get; set; }
    }
}
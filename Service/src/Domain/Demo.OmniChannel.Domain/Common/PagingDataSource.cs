﻿using System;
using System.Collections.Generic;

namespace Demo.OmniChannel.Domain.Common
{
    public class PagingDataSource<T>
    {
        public long Total { get; set; }
        public IList<T> Data { get; set; }

        public object Filter { get; set; }

        private DateTime? _timestamp;
        public DateTime Timestamp
        {
            get => (this._timestamp ?? DateTime.Now);
            set => this._timestamp = value;
        }

        public string TimestampStr { get; set; }
    }
    public class HeaderGridDataSource<T> : PagingDataSource<T> where T : class
    {
        public double? TotalValue { get; set; }
    }
}
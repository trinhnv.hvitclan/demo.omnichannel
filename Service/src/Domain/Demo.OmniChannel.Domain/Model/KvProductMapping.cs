﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo.OmniChannel.Domain.Model
{
    public class KvProductMapping
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Fullname { get; set; }
        public List<int> InActiveBranchIds { get; set; }

        public KvProductMapping()
        {
            InActiveBranchIds = new List<int>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Demo.OmniChannel.ShareKernel.Abstractions;
using Demo.OmniChannel.ShareKernel.Common;
using ServiceStack.DataAnnotations;

namespace Demo.OmniChannel.Domain.Model
{
    public class OmniChannel : IEntityId, ICreateBy, ICreateDate, IModifiedDate, IModifiedBy, IBranch, IDeleted
    {
        [PrimaryKey]
        [AutoIncrement]
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long CreateBy { get; set; }
        public long? ModifiedBy { get; set; }
        public byte Type { get; set; }
        public byte Status { get; set; }

        [Ignore]
        public bool IsSyncOrder {
            get
            {
                return OmniChannelSchedules?.Any(x => x.Type == (byte)ScheduleType.SyncOrder) ?? false;
            }
        }
        [Ignore]
        public bool IsSyncOnHand {
            get
            {
                return OmniChannelSchedules?.Any(x => x.Type == (byte)ScheduleType.SyncOnhand) ?? false;
            }
        }
        [Ignore]
        public bool IsSyncPrice {
            get
            {
                return OmniChannelSchedules?.Any(x => x.Type == (byte)ScheduleType.SyncPrice) ?? false;
            }
        }

        public int BranchId { get; set; }
        public int RetailerId { get; set; }
        public long? PriceBookId { get; set; }
        public long? BasePriceBookId { get; set; }
        public byte SyncOnHandFormula { get; set; }
        public int? SyncOrderFormula { get; set; }
        public bool IsApplyAllFormula { get; set; }
        public bool IsAutoMappingProduct { get; set; }
        public bool? IsAutoDeleteMapping { get; set; }
        public bool? IsDeleted { get; set; }
        public string IdentityKey { get; set; } // Nhận biết shop (Lazada: SellerId. Shopee: ShopId)
        public DateTime? RegisterDate { get; set; }
        public string ExtraKey { get; set; } // Key nhận biết thêm về shop của sàn (Sendo: ShopId)
        public long PlatformId { get; set; }
        [Reference]
        public OmniChannelAuth OmniChannelAuth { get; set; }
        [Reference]
        public List<OmniChannelSchedule> OmniChannelSchedules { get; set; }
        [Reference]
        public List<OmniChannelSetting> OmniChannelSettings { get; set; }
        [Reference]
        public List<OmniChannelWareHouse> OmniChannelWareHouses { get; set; }

        public OmniChannel() { }

        /// <summary>
        /// Tạo mới kênh bán
        /// </summary>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <param name="isActive"></param>
        /// <param name="status"></param>
        /// <param name="type"></param>
        /// <param name="branchId"></param>
        /// <param name="retailerId"></param>
        /// <param name="priceBookId"></param>
        /// <param name="basePriceBookId"></param>
        /// <param name="syncOnHandFormula"></param>
        /// <param name="syncOrderFormula"></param>
        /// <param name="isApplyAllFormula"></param>
        /// <param name="isAutoMappingProduct"></param>
        /// <param name="isAutoDeleteMapping"></param>
        /// <param name="identityKey"></param>
        /// <param name="extraKey"></param>
        /// <param name="platformId"></param>
        public OmniChannel(
            string name,
            string email,
            bool isActive,
            byte status,
            byte type,
            int branchId,
            int retailerId,
            long? priceBookId,
            long? basePriceBookId,
            byte syncOnHandFormula,
            int? syncOrderFormula,
            bool isApplyAllFormula,
            bool isAutoMappingProduct,
            bool? isAutoDeleteMapping,
            string identityKey,
            string extraKey,
            long platformId
        )
        {
            Name = name;
            Email = email;
            IsActive = isActive;
            Status = status;
            Type = type;
            BranchId = branchId;
            RetailerId = retailerId;
            PriceBookId = priceBookId;
            BasePriceBookId = basePriceBookId;
            SyncOnHandFormula = syncOnHandFormula;
            SyncOrderFormula = syncOrderFormula;
            IsApplyAllFormula = isApplyAllFormula;
            IsAutoMappingProduct = isAutoMappingProduct;
            IsAutoDeleteMapping = isAutoDeleteMapping;
            IdentityKey = identityKey;
            ExtraKey = extraKey;
            PlatformId = platformId;
            RegisterDate = DateTime.Now;
        }

        /// <summary>
        /// Cập nhập kênh bán
        /// </summary>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <param name="isActive"></param>
        /// <param name="status"></param>
        /// <param name="branchId"></param>
        /// <param name="priceBookId"></param>
        /// <param name="basePriceBookId"></param>
        /// <param name="syncOnHandFormula"></param>
        /// <param name="syncOrderFormula"></param>
        /// <param name="isApplyAllFormula"></param>
        /// <param name="isAutoMappingProduct"></param>
        /// <param name="isAutoDeleteMapping"></param>
        /// <param name="identityKey"></param>
        /// <param name="extraKey"></param>
        /// <param name="isDeleted"></param>
        /// <param name="platformId"></param>
        /// <param name="registerDate"></param>
        public void Update(
            string name,
            string email,
            bool isActive,
            byte status,
            int branchId,
            long? priceBookId,
            long? basePriceBookId,
            byte syncOnHandFormula,
            int? syncOrderFormula,
            bool isApplyAllFormula,
            bool isAutoMappingProduct,
            bool? isAutoDeleteMapping,
            string identityKey,
            string extraKey,
            bool? isDeleted,
            long platformId,
            DateTime? registerDate
            )
        {
            Name = name;
            Email = email;
            IsActive = isActive;
            Status = status;
            BranchId = branchId;
            PriceBookId = priceBookId;
            BasePriceBookId = basePriceBookId;
            SyncOnHandFormula = syncOnHandFormula;
            SyncOrderFormula = syncOrderFormula;
            IsApplyAllFormula = isApplyAllFormula;
            IsAutoMappingProduct = isAutoMappingProduct;
            IsAutoDeleteMapping = isAutoDeleteMapping;
            IdentityKey = identityKey;
            ExtraKey = extraKey;
            IsDeleted = isDeleted;
            PlatformId = platformId;
            RegisterDate = registerDate;
        }
    }
}
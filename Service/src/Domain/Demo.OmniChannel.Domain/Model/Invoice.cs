﻿using System;
using System.Collections.Generic;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ShareKernel.Common;
using ServiceStack.DataAnnotations;

namespace Demo.OmniChannel.Domain.Model
{
    public class Invoice
    {
        public long Id { get; set; }
        public long? OrderId { get; set; }
        public DateTime PurchaseDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long CreatedBy { get; set; }
        public long? ModifiedBy { get; set; }
        public int RetailerId { get; set; }
        public string Code { get; set; }
        public decimal? Discount { get; set; }
        public double? DiscountRatio { get; set; }
        public byte Status { get; set; }
        public int BranchId { get; set; }
        public string Description { get; set; }
        public long? CustomerId { get; set; }
        public long SoldById { get; set; }
        public int? TableId { get; set; }
        public long? PriceBookId { get; set; }
        public DateTime? EndPurchaseDate { get; set; }
        public string BookingTitle { get; set; }
        public decimal Total { get; set; }
        public decimal TotalPayment { get; set; }
        public decimal TotalPaymentOld { get; set; }
        public decimal TotalOther { get; set; }
        public decimal Debt { get; set; }
        public long? ReturnId { get; set; }
        public byte? UsingCod { get; set; }
        public decimal? Surcharge { get; set; }
        public long? Point { get; set; }
        public string Uuid { get; set; }
        public decimal? NewInvoiceTotal { get; set; }
        public decimal? MoneyPerPoint { get; set; }
        public DateTime? EntryDate { get; set; }
        public int? ActiveCustomerGroupId { get; set; }
        public bool? IsAllReturned { get; set; }
        public int? SaleChannelId { get; set; }
        public decimal? PayingAmount { get; set; }
        public decimal? DiscountByPromotion { get; set; }
        public long? DocumentId { get; set; }
        public bool? IsFavourite { get; set; }
        public string DocumentType { get; set; }
        public bool? NationalSyncStatus { get; set; }
        public byte? UsingPrescription { get; set; }
        [Ignore]
        public string StatusValue => EnumHelper.ToDescription((InvoiceState) Status);
        
        [Reference]
        public List<InvoiceDetail> InvoiceDetails { get; set; }
        [Reference]
        public List<DeliveryInfo> DeliveryInfos { get; set; }
        [Reference]
        public Customer Customer { get; set; }
        [Reference]
        public List<Payment> Payments { get; set; }
        [Reference]
        public Order Order { get; set; }
        [Reference]
        public Retailer Retailer { get; set; }

    }

}
﻿using System;

namespace Demo.OmniChannel.Domain.Model
{
    public class Attribute
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int RetailerId { get; set; }
        public DateTime CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
    }
}

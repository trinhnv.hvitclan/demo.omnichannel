﻿using ServiceStack.DataAnnotations;

namespace Demo.OmniChannel.Domain.Model
{
    public class ProductFormula
    {
        public long ProductId { get; set; }
        public long MaterialId { get; set; }
        public string MaterialCode { get; set; }
        public double Quantity { get; set; }
        public long RetailerId { get; set; }
        public int? IsWarranty { get; set; }
    }
}
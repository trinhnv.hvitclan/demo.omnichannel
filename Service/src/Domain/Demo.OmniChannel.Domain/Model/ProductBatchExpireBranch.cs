﻿using System;

namespace Demo.OmniChannel.Domain.Model
{
    public class ProductBatchExpireBranch
    {
        public long ProductBatchExpireId { get; set; }
        public int BranchId { get; set; }
        public int RetailerId { get; set; }
        public double OnHand { get; set; }
        public byte Status { get; set; }
        public long? DocumentId { get; set; }
        public int? DocumentType { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public byte[] Revision { get; set; }
    }

    public class ProductBatchExpireBranchDto
    {
        public long ProductBatchExpireId { get; set; }
        public int BranchId { get; set; }
        public int RetailerId { get; set; }
        public double OnHand { get; set; }
        public byte Status { get; set; }
        public long? DocumentId { get; set; }
        public int? DocumentType { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public byte[] Revision { get; set; }
    }
}

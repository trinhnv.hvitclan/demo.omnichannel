﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Demo.OmniChannel.Domain.Model
{
    public class BaseEvent
    {
        public long EventId { get; set; }
        public string EventName { get; set; }
        public string OrderCode { get; set; }
        public long OrderId { get; set; }
        public string InvoiceCode { get; set; }
        public long InvoiceId { get; set; }
        public int RetailerId { get; set; }
        public string RetailerCode { get; set; }
        public long UserId { get; set; }
    }

    public class BaseEventDelete : BaseEvent
    {
        public int BranchId { get; set; }
    }
    public class KolOrderMappingDelete : BaseEventDelete
    {
        public void CopyFrom(string orderCode, long orderId, int retailerId, long eventId, string retailerCode, long userId, int branchId)
        {
            EventId = eventId;
            EventName = "DELETE";
            OrderCode = orderCode;
            OrderId = orderId;
            RetailerId = retailerId;
            RetailerCode = retailerCode;
            UserId = userId;
            BranchId = branchId;
        }
    }

    public class KolInvoiceMappingDelete : BaseEventDelete
    {
        public void CopyFrom(string invoiceCode, long invoiceId, int retailerId, long eventId, string retailerCode, long userId, int branchId)
        {
            EventId = eventId;
            EventName = "DELETE";
            InvoiceCode = invoiceCode;
            RetailerId = retailerId;
            RetailerCode = retailerCode;
            UserId = userId;
            BranchId = branchId;
        }
    }

    public class BaseMapping : BaseEvent
    {
        public int BranchId { get; set; }
        public int Status { get; set; }
        public long CreatedBy { get; set; }
        public string StatusValue { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string BuyerUsername { get; set; }
        public string CustomerCode { get; set; }
        public long? CustomerId { get; set; }
        public string CustomerPhone { get; set; }
        public string PaymentMethod { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal TotalPayment { get; set; }
        public int UsingShipping { get; set; }
        public string TrackingCode { get; set; }
        public string ShippingCarrier { get; set; }
        public byte ShippingStatus { get; set; }
        public DateTime? ShipByDate { get; set; }
        public List<KolDetailMapping> Items { get; set; }
    }
    public class BaseEventCustomer
    {
        public long EventId { get; set; }
        public string EventName { get; set; }
        public string RetailerCode { get; set; }
        public int RetailerId { get; set; }
        public int BranchId { get; set; }
        public long UserId { get; set; }
    }
    public class KolOrderMapping : BaseMapping
    {

        public void CopyFrom(Order order, string eventName, long eventId, string retailerCode, long userId)
        {
            var paymentMethod = string.Empty;
            if (order.Payments != null)
            {
                var methods = order.Payments.Where(a => a.Status != (int)PaymentStatus.Void)
                    .Select(payment => payment.Method)
                    .Where(method =>
                        !string.IsNullOrEmpty(method)).Distinct()
                    .Select(PaymentType.GetMethod);
                paymentMethod = string.Join(",", methods);
            }
            var deliveryInfo = order.DeliveryInfos.FirstOrDefault(x => x.IsCurrent == true);
            RetailerCode = retailerCode;
            UserId = userId;
            EventId = eventId;
            EventName = eventName;
            OrderCode = order.Code;
            OrderId = order.Id;
            RetailerId = order.RetailerId;
            BranchId = order.BranchId;
            Status = order.Status;
            StatusValue = order.StatusValue;
            CreatedBy = order.CreatedBy;
            CreatedAt = order.CreatedDate;
            UpdatedAt = order.ModifiedDate;
            BuyerUsername = order.Customer?.Name;
            CustomerCode = order.Customer?.Code;
            CustomerId = order.Customer?.Id;
            CustomerPhone = order.Customer?.ContactNumber;
            PaymentMethod = paymentMethod;
            TotalPrice = order.Total;
            TotalPayment = order.TotalPayment;
            UsingShipping = deliveryInfo != null ? 1 : 0;
            TrackingCode = deliveryInfo?.DeliveryCode;
            ShippingCarrier = deliveryInfo?.PartnerDelivery?.Name;
            ShippingStatus = deliveryInfo?.Status ?? 1;
            ShipByDate = deliveryInfo?.CompletedDate;
            Items = new List<KolDetailMapping>();
            if (order.OrderDetails.Count <= 0) return;
            foreach (var orderOrderDetail in order.OrderDetails)
            {
                var item = new KolDetailMapping
                {
                    ProductId = orderOrderDetail.ProductId,
                    ProductCode = orderOrderDetail.Product?.Code,
                    ProductName = orderOrderDetail.Product?.Name,
                    ProductUnit = orderOrderDetail.Product?.Unit,
                    Quantity = orderOrderDetail.Quantity,
                    Price = orderOrderDetail.Price,
                    Discount = orderOrderDetail.Discount,
                    Images = orderOrderDetail.Product?.ProductImages?.Select(it => new KolProductImage() { Image = it.Image }).ToList(),
                    Attributes = orderOrderDetail.Product?.ProductAttributes?.Select(it => new KolProductAttribute() { AttributeId = it.AttributeId, Value = it.Value, Id = it.Id }).ToList(),
                };
                Items.Add(item);
            }
        }

    }

    public class KolInvoiceMapping : BaseMapping
    {
        public void CopyFrom(Invoice invoice, string eventName, long eventId, string retailerCode, long userId)
        {
            var paymentMethod = string.Empty;
            if (invoice.Payments != null)
            {
                var methods = invoice.Payments.Where(a => a.Status != (int) PaymentStatus.Void)
                    .Select(payment => payment.Method)
                    .Where(method =>
                        !string.IsNullOrEmpty(method)).Distinct().Select(PaymentType.GetMethod);
                paymentMethod = string.Join(",", methods);
            }
            var deliveryInfo = invoice.DeliveryInfos.FirstOrDefault(x => x.IsCurrent == true);
            OrderCode = invoice.Order?.Code;
            OrderId = invoice.Order?.Id ?? 0;
            RetailerCode = retailerCode;
            CreatedBy = invoice.CreatedBy;
            UserId = userId;
            EventId = eventId;
            EventName = eventName;
            InvoiceCode = invoice.Code;
            InvoiceId = invoice.Id;
            RetailerId = invoice.RetailerId;
            BranchId = invoice.BranchId;
            Status = invoice.Status;
            StatusValue = invoice.StatusValue;
            CreatedAt = invoice.CreatedDate;
            UpdatedAt = invoice.ModifiedDate;
            BuyerUsername = invoice.Customer?.Name;
            CustomerCode = invoice.Customer?.Code;
            CustomerId = invoice.Customer?.Id;
            CustomerPhone = invoice.Customer?.ContactNumber;
            PaymentMethod = paymentMethod;
            TotalPrice = invoice.Total;
            TotalPayment = invoice.TotalPayment;
            UsingShipping = deliveryInfo != null ? 1 : 0;
            TrackingCode = deliveryInfo?.DeliveryCode;
            ShippingStatus = deliveryInfo?.Status ?? 1;
            ShippingCarrier = deliveryInfo?.PartnerDelivery?.Name;
            ShipByDate = deliveryInfo?.CompletedDate;
            Items = new List<KolDetailMapping>();
            if (invoice.InvoiceDetails.Count <= 0) return;
            foreach (var invoiceDetail in invoice.InvoiceDetails)
            {
                var item = new KolDetailMapping
                {
                    ProductId = invoiceDetail.ProductId,
                    ProductCode = invoiceDetail.Product?.Code,
                    ProductName = invoiceDetail.Product?.Name,
                    ProductUnit = invoiceDetail.Product?.Unit,
                    Quantity = invoiceDetail.Quantity,
                    Price = invoiceDetail.Price,
                    Discount = invoiceDetail.Discount ?? 0,
                    Images = invoiceDetail.Product?.ProductImages?.Select(it => new KolProductImage() { Image = it.Image }).ToList(),
                    Attributes = invoiceDetail.Product?.ProductAttributes?.Select(it => new KolProductAttribute() { AttributeId = it.AttributeId, Value = it.Value, Id = it.Id }).ToList(),
                };
                Items.Add(item);
            }
        }

    }
    public class KolCustomerMapping : BaseEventCustomer
    {
        public string BuyerUsername { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerPhone { get; set; }
        public long CustomerId { get; set; }

        public void CopyFrom(Customer customer, string eventName, long eventId, string retailerCode)
        {
            EventId = eventId;
            EventName = eventName;
            RetailerCode = retailerCode;
            RetailerId = customer.RetailerId;
            BuyerUsername = customer.Name;
            CustomerCode = customer.Code;
            CustomerPhone = customer.ContactNumber;
            CustomerId = customer.Id;
        }

    }

    public class KolDetailMapping
    {
        public long ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string ProductUnit { get; set; }
        public decimal Price { get; set; }
        public double Quantity { get; set; }
        public decimal Discount { get; set; }
        public List<KolProductImage> Images { get; set; }
        public List<KolProductAttribute> Attributes { get; set; }
    }

    public class KolProductImage
    {
        public string Image { get; set; }
    }

    public class KolProductAttribute
    {
        public long Id { get; set; }
        public long AttributeId { get; set; }
        public string Value { get; set; }
    }

    public class PaymentType
    {
        protected PaymentType()
        {
            MethodType = new Dictionary<string, string>()
            {
                {"Cash", "Tiền mặt"},
                {"Debit", "Ghi nợ"},
                {"Transfer", "Chuyển khoản"},
                {"Card", "Thẻ"},
                {"Direct", "Trực tiếp"},
                {"Point", "Điểm quy đổi tiền"},
                {"Voucher", "Voucher"}
            };
        }
        public static Dictionary<string, string> MethodType { get; set; }

        public static string GetMethod(string method)
        {
            try
            {
                MethodType.TryGetValue(method, out var result);
                return result;
            }
            catch (Exception)
            {
                return method;
            }
        }
    }
}

﻿using System;
using ServiceStack.DataAnnotations;

namespace Demo.OmniChannel.Domain.Model
{
    public class Payment
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public decimal Amount { get; set; }
        public decimal AmountOriginal { get; set; }
        public string Method { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int RetailerId { get; set; }
        public int BranchId { get; set; }
        public long? OrderId { get; set; }
        public long? InvoiceId { get; set; }
        public long? ReturnId { get; set; }
        public string Description { get; set; }
        public long? CustomerId { get; set; }
        public DateTime TransDate { get; set; }
        public long? UserId { get; set; }
        public byte? Status { get; set; }
        public byte System { get; set; }
        public int? AccountId { get; set; }
        public long? UsePoint { get; set; }
        public long? VoucherId { get; set; }
        public long? DocumentId { get; set; }
        public string DocumentCode { get; set; }
    }

    public enum PaymentStatus
    {
        [Description("paymentStatus_Paid")] Paid,
        [Description("paymentStatus_Void")] Void,
    }
}

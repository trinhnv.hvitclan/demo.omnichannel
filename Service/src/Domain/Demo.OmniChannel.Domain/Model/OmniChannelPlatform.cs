﻿using System;
using Demo.OmniChannel.ShareKernel.Abstractions;
using ServiceStack.DataAnnotations;

namespace Demo.OmniChannel.Domain.Model
{
    public class OmniChannelPlatform  : IEntityId
    {
        [AutoIncrement]
        [PrimaryKey]
        public long Id { get; set; }
        public string Name { get; set; }
        public string Data { get; set; }
        public bool IsCurrent { get; set; }
        public int PlatformType { get; set; }
        public int SupportMaxVersion { get; set; }
        public int AppSupport { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long CreateBy { get; set; }
        public long? ModifiedBy { get; set; }
    }
}
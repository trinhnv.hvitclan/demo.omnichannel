﻿namespace Demo.OmniChannel.Domain.Model
{
    public class User
    {
        public long Id { get; set; }
        public bool IsActive { get; set; }
        public bool IsAdmin { get; set; }
        public string UserName { get; set; }
        public int RetailerId { get; set; }
        public string GivenName { get; set; }
    }
}
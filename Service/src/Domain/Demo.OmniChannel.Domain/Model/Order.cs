﻿using System;
using System.Collections.Generic;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ShareKernel.Common;
using ServiceStack.DataAnnotations;

namespace Demo.OmniChannel.Domain.Model
{
    public class Order
    {
        public long Id { get; set; }
        public long? CustomerId { get; set; }
        public long? CashierId { get; set; }
        public DateTime PurchaseDate { get; set; }
        public string Code { get; set; }
        public string PaymentType { get; set; }
        public int BranchId { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int RetailerId { get; set; }
        public decimal? Discount { get; set; }
        public long? SoldById { get; set; }
        public int? TableId { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public long? ModifiedBy { get; set; }
        public double? DiscountRatio { get; set; }
        public string Extra { get; set; }
        public DateTime? EndPurchaseDate { get; set; }
        public string BookingTitle { get; set; }
        public decimal Total { get; set; }
        public decimal TotalPayment { get; set; }
        public byte? UsingCod { get; set; }
        public decimal? Surcharge { get; set; }
        public long? Point { get; set; }
        public string Uuid { get; set; }
        public int? SaleChannelId { get; set; }
        public bool? IsFavourite { get; set; }
        [Reference]
        public List<OrderDetail> OrderDetails { get; set; }
        [Reference]
        public List<DeliveryInfo> DeliveryInfos { get; set; }
        [Reference]
        public Customer Customer { get; set; }
        [Reference]
        public Retailer Retailer { get; set; }
        [Reference]
        public List<Payment> Payments { get; set; }
        [Ignore]
        public string StatusValue => EnumHelper.ToDescription((OrderState)Status);
    }

    public class OrderOptimize : Order
    {
        public string OrderDetail { get; set; }
    }
}
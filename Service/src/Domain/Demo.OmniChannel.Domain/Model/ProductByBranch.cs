﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Demo.OmniChannel.Domain.Model
{
    public class ProductByBranch
    {
        public long Id { get; set; }
        public byte? ProductType { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string CategoryNameTree { get; set; }
        public bool isActive { get; set; }
        public bool HasVariants { get; set; }
        public int VariantCount { get; set; }
        public bool AllowsSale { get; set; }
        public bool isDeleted { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal BasePrice { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public decimal Cost { get; set; }
        public decimal LatestPurchasePrice { get; set; }
        public double OnHand { get; set; }
        public double OnOrder { get; set; }
        public double OnHandCompareMin { get; set; }
        public double OnHandCompareMax { get; set; }
        public double CompareOnHand { get; set; }
        public decimal CompareCost { get; set; }
        public decimal CompareBasePrice { get; set; }
        public string CompareCode { get; set; }
        public string CompareBarcode { get; set; }
        public long? CompareTradeMarkId { get; set; }
        public string CompareName { get; set; }
        public string CompareUnit { get; set; }
        public double? Weight { get; set; }
        public double Reserved { get; set; }
        public double ActualReserved { get; set; }
        public double MinQuantity { get; set; }
        public double MaxQuantity { get; set; }
        public string Image { get; set; }
        public IEnumerable<string> Images { get; set; }
        public long CustomId { get; set; }
        public decimal CustomValue { get; set; }
        public long? MasterProductId { get; set; }
        public int? BranchId { get; set; }
        public long? MasterUnitId { get; set; }
        public string Unit { get; set; }
        public double ConversionValue { get; set; }
        public string OrderTemplate { get; set; }
        public IEnumerable<ProductAttribute> ProductAttributes { get; set; }
        public string ProductAttribute { get; set; }
        public IEnumerable<ProductImage> ProductImages { get; set; }
        public IEnumerable<ProductFormula> Formulas { get; set; }
        public IEnumerable<ProductSerial> ProductSerials { get; set; }
        public string ProductSerial { get; set; }
        public int? IsFavourite { get; set; }
        public object CustomValue1 { get; set; }
        public object CustomValue2 { get; set; }
        public object CustomValue3 { get; set; }
        public object CustomValue4 { get; set; }
        public bool IsLotSerialControl { get; set; }
        public bool? IsRewardPoint { get; set; }
        public int? RewardPoint { get; set; }
        public int? CompareRewardPoint { get; set; }
        public string ModifiedDateStr { get; set; }
        public string MasterCode { get; set; }
        private string _attributedName;
        public IEnumerable<PriceBookDetail> ListPriceBookDetail { get; set; }
        public IEnumerable<PriceBookDetail> ListUnitPriceBookDetail { get; set; }
        public IEnumerable<ProductUnit> ListProductUnit { get; set; }
        public string ProductUnit { get; set; }
        public string ProductShelvesStr { get; set; }

        [IgnoreDataMember]
        public string AttributedName
        {
            get
            {
                if (_attributedName == null)
                {
                    _attributedName = FullName;
                }
                return _attributedName;
            }
        }

        public int? ProductFormulaHistoryId { get; set; }
        public byte[] Revision { get; set; }
        public bool? IsBatchExpireControl { get; set; }
        public bool? IsExpire { get; set; }
        public int? IsWarranty { get; set; }

        public int? GlobalMedicineId { get; set; }
        public string MedicineCode { get; set; }
        public string RegistrationNo { get; set; }
        public string ActiveElement { get; set; }
        public string Content { get; set; }
        public string PackagingSize { get; set; }
        public string ShortName { get; set; }
        public string GlobalManufacturerName { get; set; }
        public string GlobalManufacturerCountryName { get; set; }
        public int? ManufacturerId { get; set; }
        public int? GlobalManufacturerCountryId { get; set; }
        public int? GlobalManufacturerId { get; set; }
        public bool? IsMedicineProduct { get; set; }
        public string ManufacturerName { get; set; }
        public string RouteOfAdministration { get; set; }
        public bool? IsProductMedicineNational { get; set; }

        public int FormulaCount { get; set; }
        public string Barcode { get; set; }
        public long? TradeMarkId { get; set; }
        public string TradeMarkName { get; set; }
        //oldProductMedicine
        public double? OldWeight { get; set; }
        public string OldShortName { get; set; }
        public string OldRouteOfAdministration { get; set; }
        public string OldGlobalManufacturerName { get; set; }
        public string OldMedicineManufacturerName { get; set; }
        public string OldGlobalManufacturerCountryName { get; set; }
        public string OldPackagingSize { get; set; }
        public bool? IsRelateToOmniChannel { get; set; }
        public int? GlobalRoaId { get; set; }
        public int? RetailerRoaId { get; set; }
        public DateTime CreatedDate { get; set; }
        public double? StockoutForecast { get; set; }
        public DateTime? StockoutForecastLastUpdate { get; set; }
        public IEnumerable<ProductBatchExpireDto> ProductBatchExpires { get; set; }
        public IEnumerable<ProductShelves> ProductShelves { get; set; }

    }
}

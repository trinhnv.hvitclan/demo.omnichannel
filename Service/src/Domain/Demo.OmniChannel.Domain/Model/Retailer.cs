﻿namespace Demo.OmniChannel.Domain.Model
{
    public class Retailer
    {
        public int Id { get; set; }
        public string Code { get; set; }
    }
}

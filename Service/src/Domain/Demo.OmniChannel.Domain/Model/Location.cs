﻿namespace Demo.OmniChannel.Domain.Model
{
    public class Location
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string NormalName { get; set; }
    }
}
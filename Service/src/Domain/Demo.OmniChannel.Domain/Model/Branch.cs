﻿namespace Demo.OmniChannel.Domain.Model
{
    public class Branch
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
        public int RetailerId { get; set; }
        public bool IsActive { get; set; }
        public bool? LimitAccess { get; set; }

    }
}
﻿using System;

namespace Demo.OmniChannel.Domain.Model
{
    public class PartnerDelivery
    {
        public long Id { get; set; }
        public int RetailerId { get; set; }
        public byte Type { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ContactNumber { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Comments { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public decimal? Debt { get; set; }
        public long? ModifiedBy { get; set; }
        public string Uuid { get; set; }
        public int? LocationId { get; set; }
        public string LocationName { get; set; }
        public string WardName { get; set; }
        public bool? isActive { get; set; }
        public bool? isDeleted { get; set; }
        public bool? IsOmniChannel { get; set; }
        public string SearchNumber { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using Demo.OmniChannel.Domain.ResultModel;
using Demo.OmniChannel.ShareKernel.Abstractions;
using ServiceStack.DataAnnotations;
using Newtonsoft.Json;

namespace Demo.OmniChannel.Domain.Model
{
    public class Product : IRetailer, ICreateDate, IModifiedDate
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string MasterCode { get; set; }
        public byte? ProductType { get; set; }
        public string FullName { get; set; }
        public bool IsActive { get; set; }
        public bool? isDeleted { get; set; }
        public bool AllowsSale { get; set; }
        public int RetailerId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public decimal BasePrice { get; set; }
        public string Description { get; set; }
        public bool? IsLotSerialControl { get; set; }
        public bool? IsBatchExpireControl { get; set; }
        public long? MasterUnitId { get; set; }
        public long? MasterProductId { get; set; }
        [Ignore]
        [JsonIgnore]
        public byte[] Revision { get; set; }
        public int? IsWarranty { get; set; }
        public double ConversionValue { get; set; }
        public bool HasVariants { get; set; }
        public string Name { get; set; }
        public string Unit { get; set; }
        [Reference]
        public List<ProductImage> ProductImages { get; set; }
        [Reference]
        public List<ProductAttribute> ProductAttributes { get; set; }
        public bool? IsRelateToOmniChannel { get; set; }


        public bool IsHasVariantsAndSingleUnit()
        {
            // Hàng đa thuộc tính nhưng chỉ có 1 đơn vị tính
            if (HasVariants && !MasterUnitId.HasValue)
            {
                return true;
            }
            return false;
        }
        public string OrderTemplate { get; set; }
    }
    public class ProductSubscribe : Product
    {
        public List<ProductBranch> ProductBranch { get; set; }
        public List<ProductAttribute> ProductAttribute { get; set; }
        public List<ProductUnit> ProductUnit { get; set; }
        public List<PriceBookDetailDTO> ProductPriceBookDetail { get; set; }
        public decimal Price { get; set; }
        public int BranchId { get; set; }
    }
    public class ProductBranchDTO : Product
    {
        public bool IsActiveBranch { get; set; }
        public int BranchId { get; set; }
    }

    public class ProductUnit
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Unit { get; set; }
        public float BasePrice { get; set; }
        public float ConversionValue { get; set; }
        public double Conversion { get; set; }
        public long MasterUnitId { get; set; }
    }

    public class ProductForChannelMapping
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public decimal BasePrice { get; set; }
        public double? OnHand { get; set; }
        public double? OnOrder { get; set; }
        public double? Reserved { get; set; }
        public string Unit { get; set; }
        public string AttributeValue { get; set; }
        public byte? ProductType { get; set; }
        public DateTime CreatedDate { get; set; }
        public long? SuperId { get; set; }
        public int BranchId { get; set; }
    }
}
﻿using Demo.OmniChannel.ShareKernel.Abstractions;
using System;

namespace Demo.OmniChannel.Domain.Model
{
    public class KvPosSetting : ICreateDate
    {
        public long Id { get; set; }
        public int? IndustryId { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public bool IsDisplay { get; set; }
        public bool IsDefault { get; set; }
        public bool IsFeature { get; set; }
        public string Type { get; set; }
        public int? Order { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }
    }
}

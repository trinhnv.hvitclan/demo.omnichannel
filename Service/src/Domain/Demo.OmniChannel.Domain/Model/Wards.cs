﻿namespace Demo.OmniChannel.Domain.Model
{
    public class Wards
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int ParentId { get; set; }
        public string NormalName { get; set; }
    }
}
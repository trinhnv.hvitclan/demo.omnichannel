﻿using Demo.OmniChannel.ShareKernel.Abstractions;
using System;

namespace Demo.OmniChannel.Domain.Model
{
    public class Category : IRetailer, ICreateDate, IModifiedDate
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public int RetailerId { get; set; }
        public int? ParentId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? isDeleted { get; set; }

    }
}

﻿namespace Demo.OmniChannel.Domain.Model
{
    public class KvGroup
    {
        public int Id { get; set; }
        public string ConnectionString { get; set; }
    }
}
﻿using Demo.OmniChannel.ShareKernel.Abstractions;
using ServiceStack.DataAnnotations;
using System;

namespace Demo.OmniChannel.Domain.Model
{
    public class OmniChannelWareHouse : IEntityId, IAppId, ICreateBy, ICreateDate, IModifiedDate, IModifiedBy, IDeleted
    {
        [PrimaryKey]
        [AutoIncrement]
        public long Id { get; set; }
        public long WareHouseId { get; set; }
        public long OmniChannelId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long CreateBy { get; set; }
        public long? ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public long RetailerId { get; set; }
        public string StreetInfo { get; set; }
        [Ignore]
        public OmniChannel OmniChannel { get; set; }

    }
}

﻿using System;

namespace Demo.OmniChannel.Domain.Model
{
    public class ProductImage
    {
        public long Id { get; set; }
        public int RetailerId { get; set; }
        public long ProductId { get; set; }
        public string Image { get; set; }
        public string ImageId { get; set; }
        public long? RootProductId { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}

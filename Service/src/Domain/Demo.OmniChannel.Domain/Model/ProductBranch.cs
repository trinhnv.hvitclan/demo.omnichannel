﻿using System;

namespace Demo.OmniChannel.Domain.Model
{
    public class ProductBranch
    {
        public long ProductId { get; set; }
        public double OnHand { get; set; }
        public double Reserved { get; set; }
        public double OnOrder { get; set; }
        public int BranchId { get; set; }
        public int RetailerId { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public byte[] Revision { get; set; }
        public decimal LatestPurchasePrice { get; set; }
        public decimal Cost { get; set; }
    }

}
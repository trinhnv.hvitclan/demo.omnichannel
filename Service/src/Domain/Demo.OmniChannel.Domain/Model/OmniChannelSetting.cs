﻿using System;
using Demo.OmniChannel.ShareKernel.Abstractions;
using ServiceStack.DataAnnotations;

namespace Demo.OmniChannel.Domain.Model
{
    public class OmniChannelSetting : IEntityId, ICreateDate, IModifiedDate
    {
        [PrimaryKey]
        [AutoIncrement]
        public long Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public bool Status { get; set; }
        public byte ChannelType { get; set; }
        public long OmniChannelId { get; set; }
        public int BranchId { get; set; }
        public int RetailerId { get; set; }
        public string ShopId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        [Ignore]
        public OmniChannel OmniChannel { get; set; }

        public OmniChannelSetting() { }
        public OmniChannelSetting(
            string name,
            string value,
            bool status,
            byte channelType,
            long omniChannelId,
            int branchId,
            int retailId,
            string shopId
        )
        {
            Name = name;
            Value = value;
            Status = status;
            ChannelType = channelType;
            OmniChannelId = omniChannelId;
            BranchId = branchId;
            RetailerId = retailId;
            ShopId = shopId;
        }
        public void Update(string value)
        {
            Value = value;
        }
    }
}

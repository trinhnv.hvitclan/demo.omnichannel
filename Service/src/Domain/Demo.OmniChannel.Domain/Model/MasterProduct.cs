﻿namespace Demo.OmniChannel.Domain.Model
{
    public class MasterProduct
    {
        public long MasterProductId { get; set; }
        public long ProductId { get; set; }
        public double ConversionValue { get; set; }
    }
}

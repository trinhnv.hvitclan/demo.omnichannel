﻿using Demo.OmniChannel.ShareKernel.Abstractions;
using ServiceStack.DataAnnotations;
using System;

namespace Demo.OmniChannel.Domain.Model
{
    public class OmniPlatformSetting : IEntityId, ICreateBy, ICreateDate, IModifiedDate, IModifiedBy, IRetailer
    {
        [PrimaryKey]
        [AutoIncrement]
        public long Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long CreateBy { get; set; }
        public long? ModifiedBy { get; set; }
        public int RetailerId { get; set; }

        public OmniPlatformSetting() { }

        public OmniPlatformSetting(
          string name,
          string value,
          int retailId
      )
        {
            Name = name;
            Value = value;
            RetailerId = retailId;
        }

        public void Update(string value)
        {
            Value = value;
        }
    }
}

﻿using System;

namespace Demo.OmniChannel.Domain.Model
{
    public class SaleChannel
    {
        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public int? Position { get; set; }
        public int RetailerId { get; set; }
        public string Img { get; set; }
        public bool? IsNotDelete { get; set; }
        public long? OmniChannelId { get; set; }
    }
    public class SaleChannelDataSourceDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string Img { get; set; }

    }
}
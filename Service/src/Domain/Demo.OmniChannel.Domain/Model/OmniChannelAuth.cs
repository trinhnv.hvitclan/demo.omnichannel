﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Demo.OmniChannel.ShareKernel.Abstractions;
using ServiceStack.DataAnnotations;

namespace Demo.OmniChannel.Domain.Model
{
    public class OmniChannelAuth : IEntityId, ICreateDate, IModifiedDate, ICreateBy, IModifiedBy
    {
        [AutoIncrement]
        [PrimaryKey]
        public long Id { get; set; }
        public long ShopId { get; set; }
        [MaxLength(1000)]
        public string AccessToken { get; set; }
        [MaxLength(1000)]
        public string RefreshToken { get; set; }
        public int ExpiresIn { get; set; }
        public int RefreshExpiresIn { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long CreateBy { get; set; }
        public long? ModifiedBy { get; set; }
        public long? OmniChannelId { get; set; }
        [Ignore]
        public OmniChannel OmniChannel { get; set; }
        [Ignore]
        [NotMapped]
        public string AccessTokenDecode { get; set; }
    }
}
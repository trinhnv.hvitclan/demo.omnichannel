﻿using System;
using System.Collections.Generic;
using System.Linq;
using Demo.OmniChannel.ShareKernel.Abstractions;
using ServiceStack.DataAnnotations;

namespace Demo.OmniChannel.Domain.Model
{
    public class PriceBook : IRetailer
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public bool IsGlobal { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool? isDeleted { get; set; }
        [Reference]
        public List<PriceBookBranch> PriceBookBranches { get; set; }
        public int RetailerId { get; set; }

        public (bool, string) IsValidPriceBook(int branchId)
        {
            if (!IsGlobal && PriceBookBranches != null && !PriceBookBranches.Any(x => x.BranchId == branchId))
            {
                return (false, $"Bảng giá bán {Name} không áp dụng cho chi nhánh");
            }
            else if (!IsActive)
            {
                return (false, $"Bảng giá bán {Name} không được bật");
            }
            else if (isDeleted.GetValueOrDefault())
            {
                return (false, $"Bảng giá bán {Name} đã bị xóa");
            }
            else if (StartDate >= DateTime.Now || EndDate <= DateTime.Now)
            {
                return (false, $"Bảng giá bán {Name} đã hết hạn");
            }
            return (true, "");
        }
    }


   

}
﻿namespace Demo.OmniChannel.Domain.Model
{
    public class PriceBookBranch
    {
        public long Id { get; set; }
        public long PriceBookId { get; set; }
        public int BranchId { get; set; }
    }
}
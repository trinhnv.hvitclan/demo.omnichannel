﻿using ServiceStack.DataAnnotations;

namespace Demo.OmniChannel.Domain.Model
{
    public class OrderDetail
    {
        public long ProductId { get; set; }
        //public string ProductCode { get; set; }
        //public string ProductName { get; set; }
        public double Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
        public double? DiscountRatio { get; set; }
        public string Note { get; set; }
        public long OrderId { get; set; }
        [Reference]
        public Product Product { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo.OmniChannel.Domain.Model
{
    public class ProductSerial
    {
        public long Id { get; set; }
        public long ProductId { get; set; }
        public string SerialNumber { get; set; }
        public int Status { get; set; }
        public int BranchId { get; set; }
        public int RetailerId { get; set; }
        public double? Quantity { get; set; }
        public long DocumentId { get; set; }
        public int DocumentType { get; set; }
    }
}

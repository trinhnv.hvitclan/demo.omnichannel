﻿using System;
using ServiceStack.DataAnnotations;

namespace Demo.OmniChannel.Domain.Model
{
    public class DeliveryInfo
    {
        public long Id { get; set; }
        public long? InvoiceId { get; set; }
        public long? OrderId { get; set; }
        public int RetailerId { get; set; }
        [References(typeof(DeliveryPackage))]
        public long? DeliveyPackageId { get; set; }
        [References(typeof(PartnerDelivery))]
        public long? DeliveryBy { get; set; }
        public string DeliveryCode { get; set; }
        public string ServiceType { get; set; }
        public string ServiceAdd { get; set; }
        public decimal? Price { get; set; }
        public decimal? PriceCodPayment { get; set; }
        public DateTime? ExpectedDelivery { get; set; }
        public string ExpectedDeliveryText { get; set; }
        public bool? UseDefaultPartner { get; set; }
        public byte Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public string ServiceTypeText { get; set; }
        public bool? IsCurrent { get; set; }
        public bool? IsVoidByCarrier { get; set; }
        public string SortCode { get; set; }
        public string DebtControlCode { get; set; }
        public string StatusNote { get; set; }
        public decimal? TotalPrice { get; set; }
        public long? BranchTakingAddressId { get; set; }
        public string BranchTakingAddressStr { get; set; }
        public decimal? OriginalCod { get; set; }
        public DateTime? CompletedDate { get; set; }
        [Reference]
        public DeliveryPackage DeliveryPackage { get; set; }
        [Reference]
        public PartnerDelivery PartnerDelivery { get; set; }
        public string FeeJson { get; set; }

        //[Ignore]
        //public PartnerDelivery PartnerDelivery { get; set; }
    }
}
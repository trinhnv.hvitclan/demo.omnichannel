﻿using System;
using System.Globalization;

namespace Demo.OmniChannel.Domain.Model
{
    public class ProductBatchExpire
    {
        public long Id { get; set; }
        public long ProductId { get; set; }
        public int RetailerId { get; set; }
        public string BatchName { get; set; }
        public DateTime ExpireDate { get; set; }
        public byte DisplayType { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public byte[] Revision { get; set; }
        public string FullName { get; set; }
        public string FullNameVirgule { get; set; }
        public int? BatchExpireId { get; set; }
    }
    public class ProductBatchExpireDto
    {
        public long Id { get; set; }
        public string FullNameVirgule { get; set; }
        public string BatchName { get; set; }
        public DateTime ExpireDate { get; set; }
        public long ProductId { get; set; }
        public bool? IsBatchExpireControl { get; set; }
        public double OnHand { get; set; }
        public byte Status { get; set; }
        public int BranchId { get; set; }
        public int RetailerId { get; set; }
        public bool? IsExpire => DateTime.ParseExact(ExpireDate.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture) < DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        public bool? IsAboutToExpire => DateTime.ParseExact(ExpireDate.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture) >= DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture) && DateTime.ParseExact(ExpireDate.AddMonths(-3).ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture) < DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture);
    }
}

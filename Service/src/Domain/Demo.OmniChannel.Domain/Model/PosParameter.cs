﻿using System;

namespace Demo.OmniChannel.Domain.Model
{
    public class PosParameter
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public int RetailerId { get; set; }
        public bool isActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public int? ParameterType { get; set; }
        public DateTime? StartTrialDate { get; set; }
        public int? BlockUnit { get; set; }
    }
}
﻿using System;
using ServiceStack.DataAnnotations;

namespace Demo.OmniChannel.Domain.Model
{
    public class ProductAttribute
    {
        public long Id { get; set; }
        public int RetailerId { get; set; }
        public long AttributeId { get; set; }
        public long ProductId { get; set; }
        public string Value { get; set; }
        public DateTime CreatedDate { get; set; }
        public byte? IsDeleted { get; set; }
        public DateTime? ModifiedDate { get; set; }
        [Reference]
        public Attribute Attribute { get; set; }
    }
}

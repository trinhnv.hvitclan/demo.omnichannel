﻿using System;
using Demo.OmniChannel.ShareKernel.Abstractions;
using ServiceStack.DataAnnotations;

namespace Demo.OmniChannel.Domain.Model
{
    public class EventKol : IRetailer
    {
        [PrimaryKey]
        [AutoIncrement]
        public long Id { get; set; }
        public long EventId { get; set; }
        public int Status { get; set; }
        public int RetailerId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Action { get; set; }
        public long DocumentId { get; set; }
        public int DocumentType { get; set; }
        public string DocumentCode { get; set; }
    }

}

﻿namespace Demo.OmniChannel.Domain.Model
{
    public class ProductShelves
    {
        public long Id { get; set; }
        public int RetailerId { get; set; }
        public long ShelvesId { get; set; }
        public long ProductId { get; set; }
    }
}

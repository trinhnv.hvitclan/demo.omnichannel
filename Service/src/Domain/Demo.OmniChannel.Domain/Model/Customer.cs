﻿using ServiceStack.DataAnnotations;

namespace Demo.OmniChannel.Domain.Model
{
    public class Customer
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ContactNumber { get; set; }
        public string HashLastestNumber { get; set; }
        public string HashNumber { get; set; }
        public int RetailerId { get; set; }
        public int? BranchId { get; set; }
        public bool? IsDeleted { get; set; }
        public string SearchNumber { get; set; }

        [Reference]
        public Retailer Retailer { get; set; }
    }
}
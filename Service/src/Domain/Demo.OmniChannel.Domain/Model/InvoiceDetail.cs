﻿using System;
using ServiceStack.DataAnnotations;

namespace Demo.OmniChannel.Domain.Model
{
    public class InvoiceDetail
    {
        public long Id { get; set; }
        public long InvoiceId { get; set; }
        public long ProductId { get; set; }
        public double Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal? Discount { get; set; }
        public decimal? AllocationDiscount { get; set; }
        public double? DiscountRatio { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Note { get; set; }
        public string SerialNumbers { get; set; }
        public bool? IsMaster { get; set; }
        public int RetailerId { get; set; }
        [Reference]
        public Product Product { get; set; }
    }
}
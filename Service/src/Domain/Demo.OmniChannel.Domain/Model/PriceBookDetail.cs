﻿namespace Demo.OmniChannel.Domain.Model
{
    public class PriceBookDetail
    {
        public long Id { get; set; }
        public int RetailerId { get; set; }
        public long PriceBookId { get; set; }
        public long ProductId { get; set; }
        public decimal Price { get; set; }
    }
}
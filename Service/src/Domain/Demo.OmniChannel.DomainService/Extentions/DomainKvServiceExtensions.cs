﻿using Demo.OmniChannel.DomainService.Interfaces;
using Demo.OmniChannel.DomainService.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Demo.OmniChannel.DomainService.Extentions
{
    public static class DomainKvServiceExtensions
    {
        public static void AddDomainService(this IServiceCollection service)
        {
            service.AddTransient<ICreateOrderDomainService, CreateOrderDomainService>();
            service.AddTransient<ICreateInvoiceDomainService, CreateInvoiceDomainService>();
        }
    }
}

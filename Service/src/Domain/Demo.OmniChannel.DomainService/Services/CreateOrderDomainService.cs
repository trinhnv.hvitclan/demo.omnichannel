﻿using Demo.OmniChannel.DomainService.Interfaces;
using Demo.OmniChannel.Infrastructure.DemoGuaranteeClient.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Repository.Impls;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.DemoGuaranteeClient;
using Microsoft.Extensions.Options;
using Demo.OmniChannelCore.Api.Sdk.Models;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Demo.OmniChannel.ShareKernel.RedisStreamMessage;
using Demo.OmniChannel.ChannelClient.Common;
using System.Text;
using Demo.OmniChannel.Utilities;

namespace Demo.OmniChannel.DomainService.Services
{
    public class CreateOrderDomainService : ICreateOrderDomainService
    {
        private readonly IGuaranteeClient _guaranteeClient;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly GuaranteeClientConfiguration _crmClientConfiguration;
        private readonly ISurChargeBranchInternalClient _surChargeBranchInternalClient;
        private readonly ISurChargeInternalClient _surChargeInternalClient;
        public CreateOrderDomainService(
            IGuaranteeClient guaranteeClient,
            IDbConnectionFactory dbConnectionFactory,
            IOptions<GuaranteeClientConfiguration> crmClientConfiguration,
            ISurChargeBranchInternalClient surChargeBranchInternalClient,
            ISurChargeInternalClient surChargeInternalClient)
        {
            _guaranteeClient = guaranteeClient;
            _dbConnectionFactory = dbConnectionFactory;
            _crmClientConfiguration = crmClientConfiguration.Value;
            _surChargeBranchInternalClient = surChargeBranchInternalClient;
            _surChargeInternalClient = surChargeInternalClient;
        }
        #region Public method
        public async Task<List<ChannelClient.Models.InvoiceWarranties>> GetInvoiceWarranties(KvInternalContext context,
           List<MongoDb.OrderDetail> orderDetails,
           DateTime purchaseDate)
        {
            if (_crmClientConfiguration.IncludeRetailerIds.Any() && !_crmClientConfiguration.IncludeRetailerIds.Contains(context.RetailerId))
                return new List<ChannelClient.Models.InvoiceWarranties>();

            var productIds = orderDetails.Where(x => !string.IsNullOrEmpty(x.Uuid)).Select(x => x.ProductId).ToList();
            if (productIds == null || !productIds.Any()) return new List<ChannelClient.Models.InvoiceWarranties>();

            var getProducts = await GetProducts(context, productIds);

            var productCombo = getProducts.Where(x => x.ProductType == (byte)ProductType.Manufactured).ToList();

            var productFormula = await GetProductFormula(context, productCombo.Select(x => x.Id).ToList());

            if (getProducts.All(x => x.IsWarranty == null || x.IsWarranty == 0)
                && productFormula.All(x => x.IsWarranty == null || x.IsWarranty == 0))
            {
                return new List<ChannelClient.Models.InvoiceWarranties>();
            }

            productIds.AddRange(productFormula.Select(x => x.MaterialId));

            var warrantys = await _guaranteeClient.GetWarrantyListByProductIds(
                new ShareKernel.Auth.ExecutionContext(context.RetailerId, context.RetailerCode,
                context.Group?.Id ?? 0), productIds);
            if (warrantys == null || !warrantys.Any()) return new List<ChannelClient.Models.InvoiceWarranties>();

            var invoiceWarrantys = new List<ChannelClient.Models.InvoiceWarranties>();

            orderDetails.ForEach(detail =>
            {
                var getproductCombo = productFormula.Where(pf => pf.ProductId == detail.ProductId);
                var warrantysByProduct = warrantys.Where(w => w.ProductId == detail.ProductId
                 || getproductCombo.Select(x => x.MaterialId).Contains(w.ProductId)).ToList();

                var invoiceWarranty = warrantysByProduct?.Select(x =>
                {
                    return new ChannelClient.Models.InvoiceWarranties
                    {
                        Description = x.Description,
                        NumberTime = x.NumberTime,
                        TimeType = x.TimeType,
                        ProductId = x.ProductId,
                        ProductCode = getproductCombo.FirstOrDefault(p => p.MaterialId == x.ProductId)?.MaterialCode,
                        ExpireDate = SyncHelper.CalExpireWarranty(x.TimeType, x.NumberTime, purchaseDate),
                        InvoiceDetailUuid = detail.Uuid,
                        WarrantyType = x.WarrantyType,
                        Status = (int)WarrantyOrderStatus.Finalized
                    };
                }).ToList();
                invoiceWarrantys.AddRange(invoiceWarranty);
            });

            return invoiceWarrantys;
        }

        public async Task<List<InvoiceSurCharges>> GetListInvoiceSurchargeAsync(
          List<ChannelClient.Models.SurCharge> surCharges,
          OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext,
          CreateInvoiceMessage request)
        {
            var lstInvoiceSurcharge = new List<InvoiceSurCharges>();
            if (surCharges != null && surCharges.Count > 0)
            {
                foreach (var itemSurCharge in surCharges)
                {
                    var codeByGen = ChannelTypeHelper.GenerateSurchargeCode(request.ChannelType, request.RetailerId,
                        itemSurCharge.Type ?? 0);
                    var surcharge =
                        await _surChargeInternalClient.GetSurchargeByCode(coreContext, codeByGen);

                    if (surcharge != null && !surcharge.isActive)
                    {
                        continue;
                    }

                    if (surcharge == null)
                    {
                        var surchargeTmp = new SurCharge
                        {
                            Code = codeByGen,
                            Name = itemSurCharge.Name,
                            Value = itemSurCharge.Value,
                            RetailerId = request.RetailerId,
                            isActive = true,
                            isAuto = false,
                            ForAllBranch = true,
                            isReturnAuto = true
                        };
                        surcharge = await _surChargeInternalClient.CreateSurCharge(coreContext, surchargeTmp);
                    }

                    var isApproveSurcharge = await CheckSurchargeActiveWithBranch(coreContext, surcharge, request.BranchId);

                    if (isApproveSurcharge)
                    {
                        //create surcharge for Order
                        var invoiceSurcharge = new InvoiceSurCharges
                        {
                            SurchargeId = surcharge?.Id ?? 0,
                            Price = itemSurCharge.Value ?? 0,
                            RetailerId = request.RetailerId,
                            Name = surcharge?.Name,
                            SurValue = itemSurCharge.Value
                        };

                        lstInvoiceSurcharge.Add(invoiceSurcharge);
                    }
                }
            }

            return lstInvoiceSurcharge;
        }
        #endregion


        #region Private method
        private async Task<List<ProductBranchDTO>> GetProducts(KvInternalContext context, List<long> productIds)
        {
            List<ProductBranchDTO> kvProducts;
            using (var db = await _dbConnectionFactory.OpenAsync(context.Group.ConnectionStringName.ToLower()))
            {
                var productRepository = new ProductRepository(db);
                kvProducts = await productRepository.GetProductByIds(context.RetailerId, context.BranchId,
                   productIds, isLogMonitor: false);
            }
            return kvProducts;
        }

        private async Task<List<Domain.Model.ProductFormula>> GetProductFormula(KvInternalContext context,
            List<long> productIds)
        {
            if (!productIds.Any()) return new List<Domain.Model.ProductFormula>();

            List<Domain.Model.ProductFormula> productFormulas;
            using (var db = await _dbConnectionFactory.OpenAsync(context.Group.ConnectionStringName.ToLower()))
            {
                var productFormulaRepository = new ProductFormulaRepository(db);
                productFormulas = await productFormulaRepository.GetByProductIds(context.RetailerId, productIds);
            }
            return productFormulas;
        }

        private async Task<bool> CheckSurchargeActiveWithBranch(
            OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext, 
            SurCharge surcharge, int branchId)
        {
            if (surcharge.ForAllBranch) return true;
            var surchargeByBranch =
                await _surChargeBranchInternalClient.GetSurChargeBranchById(coreContext, surcharge.Id);
            var isApproveSurcharge = surchargeByBranch?.Any(b => b.BranchId == branchId) ?? false;
            return isApproveSurcharge;
        }
        #endregion
    }
}

﻿using Demo.OmniChannel.Domain.Common;
using Demo.OmniChannel.ShareKernel.RedisStreamMessage;
using Demo.OmniChannelCore.Api.Sdk.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.DomainService.Interfaces
{
    public interface ICreateOrderDomainService
    {
        Task<List<ChannelClient.Models.InvoiceWarranties>> GetInvoiceWarranties(KvInternalContext context,
            List<MongoDb.OrderDetail> orderDetails, DateTime purchaseDate);

        Task<List<InvoiceSurCharges>> GetListInvoiceSurchargeAsync(List<ChannelClient.Models.SurCharge> surCharges,
          OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext, CreateInvoiceMessage request);
    }

}

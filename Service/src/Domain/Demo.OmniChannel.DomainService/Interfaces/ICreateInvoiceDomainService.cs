﻿using Demo.OmniChannel.ChannelClient.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.DomainService.Interfaces
{
    public interface ICreateInvoiceDomainService
    {
        (bool, string, string) ValidateAutoSyncBatchExpire(List<InvoiceDetail> before,
            List<InvoiceDetail> after);

        Task<List<OmniChannelCore.Api.Sdk.Models.InvoiceWarranties>> GetInvoiceWarranties(
          OmniChannelCore.Api.Sdk.Common.KvInternalContext context,
          List<OmniChannelCore.Api.Sdk.Models.ProductBrachDto> productData,
          List<MongoDb.InvoiceDetail> invoiceDetails,
          DateTime purchaseDate);

        List<MongoDb.InvoiceDetail> SplitOrderDetails(
            List<Domain.Model.ProductBranchDTO> productData,
            List<MongoDb.InvoiceDetail> invoiceDetails);
    }
}

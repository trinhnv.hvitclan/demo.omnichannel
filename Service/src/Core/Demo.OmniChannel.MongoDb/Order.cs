﻿using Demo.OmniChannel.ShareKernel.Abstractions;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Demo.OmniChannel.MongoDb
{
    [BsonIgnoreExtraElements]
    public class Order : BaseEntity, ICreateDate, IModifiedDate
    {
        public string Uuid { get; set; }
        public string Code { get; set; }
        public DateTime PurchaseDate { get; set; }
        public int BranchId { get; set; }
        public string BranchName { get; set; }
        public long SoldById { get; set; }
        public string SoldByName { get; set; }
        public long CustomerId { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerAddress { get; set; }
        public string Extra { get; set; }
        public decimal Total { get; set; }
        public decimal TotalPayment { get; set; }
        public decimal Discount { get; set; }
        public double DiscountRatio { get; set; }
        public int Status { get; set; }
        public string StatusValue { get; set; }
        public int RetailerId { get; set; }
        public string Description { get; set; }
        public bool UsingCod { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public InvoiceDelivery OrderDelivery { get; set; }
        public int SaleChannelId { get; set; }
        public List<OrderDetail> OrderDetails { get; set; }
        public bool IsSyncSuccess { get; set; }
        public byte? NewStatus { get; set; }
        public string ChannelStatus { get; set; }
        public string ChannelOrder { get; set; }
        public string ChannelDiscount { get; set; }
        public string ErrorMessage { get; set; }
        public string OrderId { get; set; }
        public long ChannelId { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? ChannelCreatedDate { get; set; }
        public decimal? Surcharge { get; set; }
        public bool IsBatchExpireControl { get; set; }
        public bool IsLotSerialControl { get; set; }
        public string KvOrder { get; set; }

        private List<InvoiceSurCharges> _orderSurCharges;
        public List<InvoiceSurCharges> OrderSurCharges
        {
            get => _orderSurCharges;
            set { _orderSurCharges = value?.Where(x => x != null).ToList(); }
        }
    }
}
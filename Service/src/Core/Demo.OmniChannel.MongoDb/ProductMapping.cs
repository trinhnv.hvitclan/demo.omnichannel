﻿using System;
using Demo.OmniChannel.ShareKernel.Abstractions;
using MongoDB.Bson.Serialization.Attributes;

namespace Demo.OmniChannel.MongoDb
{
    [BsonIgnoreExtraElements]
    public class ProductMapping : BaseEntity, IRetailer, ICreateDate, IModifiedDate
    {
        public ProductMapping() { }
        public ProductMapping(int retailerId, long channelId, long productKvId, string productKvSku,
            string productKvFullName, string productChannelSku, string productChannelName, byte productChannelType,
            string commonProductChannelId, string commonParentProductChannelId)
        {
            RetailerId = retailerId;
            ChannelId = channelId;
            ProductKvId = productKvId;
            ProductKvSku = productKvSku;
            ProductKvFullName = productKvFullName;
            ProductChannelSku = productChannelSku;
            ProductChannelName = productChannelName;
            ProductChannelType = productChannelType;
            CommonProductChannelId = commonProductChannelId;
            CommonParentProductChannelId = commonParentProductChannelId;
        }
        public long ProductKvId { get; set; }
        public string ProductKvSku { get; set; }
        public string ProductKvFullName { get; set; }
        public int RetailerId { get; set; }
        public string RetailerCode { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        /// <summary>
        /// Chỉ dùng cho các thao tác giao tiếp với mongodb
        /// </summary>
        public long? ProductChannelId { get; set; }
        /// <summary>
        /// Chỉ dùng cho các thao tác giao tiếp với mongodb
        /// </summary>
        public long? ParentProductChannelId { get; set; }
        public string ProductChannelSku { get; set; }
        public string ProductChannelName { get; set; }
        public byte ProductChannelType { get; set; }
        public long ChannelId { get; set; }
        public double LatestOnHand { get; set; }
        public decimal LatestBasePrice { get; set; }
        public decimal LatestSalePrice { get; set; }
        /// <summary>
        /// Chỉ dùng cho các thao tác giao tiếp với mongodb
        /// </summary>
        public string StrProductChannelId { get; set; }
        /// <summary>
        /// Chỉ dùng cho các thao tác giao tiếp với mongodb
        /// </summary>
        public string StrParentProductChannelId { get; set; }

        /// <summary>
        /// Dùng cho truy xuất itemId trên memory (không dùng với mongodb)
        /// </summary>
        [BsonIgnore]
        public string CommonProductChannelId
        {
            get
            {
                if (ProductChannelId.GetValueOrDefault() > 0)
                {
                    return ProductChannelId.ToString();
                }
                else
                {
                    return StrProductChannelId;
                }
            }
            set
            {
                try
                {
                    StrProductChannelId = value;
                    ProductChannelId = long.Parse(value);
                }
                catch
                {
                    ProductChannelId = 0;
                }
            }
        }
        /// <summary>
        /// Dùng cho truy xuất ParentItemId trên memory (không dùng với mongodb)
        /// </summary>
        [BsonIgnore]
        public string CommonParentProductChannelId
        {
            get
            {
                if (ParentProductChannelId.GetValueOrDefault() > 0)
                {
                    return ParentProductChannelId.ToString();
                }
                else
                {
                    return StrParentProductChannelId;
                }
            }
            set
            {
                try
                {
                    StrParentProductChannelId = value;
                    ParentProductChannelId = long.Parse(value);
                }
                catch
                {
                    ParentProductChannelId = 0;
                }
            }
        }
        public long? SuperId { get; set; } // Tiki only
    }

}
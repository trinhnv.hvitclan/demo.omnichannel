﻿using Demo.OmniChannel.ShareKernel.Abstractions;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Demo.OmniChannel.MongoDb
{
    [BsonIgnoreExtraElements]
    public class LazadaFeePaymentTransactionHistory : BaseEntity, ICreateDate, IModifiedDate
    {
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string OrderNo { get; set; }
        public string TransactionDate { get; set; }
        public string Amount { get; set; }
        public string PaidStatus { get; set; }
        public string ShippingProvider { get; set; }
        public string WHTIncludedInAmount { get; set; }
        public string LazadaSKU { get; set; }
        public string FeeType { get; set; }
        public string TransactionType { get; set; }
        public string OrderItemNo { get; set; }
        public string OrderItemStatus { get; set; }
        public string Reference { get; set; }
        public string FeeName { get; set; }
        public string ShippingSpeed { get; set; }
        public string WHTAmount { get; set; }
        public string TransactionNumber { get; set; }
        public string SellerSku { get; set; }
        public string Statement { get; set; }
        public string Details { get; set; }
        public string VATInAmount { get; set; }
        public string ShipmentType { get; set; }
    }
}
﻿using Demo.OmniChannel.ShareKernel.Abstractions;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace Demo.OmniChannel.MongoDb
{
    [BsonIgnoreExtraElements]
    public class Product : BaseEntity, IRetailer, IBranch, ICreateDate, IModifiedDate
    {
        /// <summary>
        /// Chỉ dùng cho các thao tác giao tiếp với mongodb
        /// </summary>
        public long? ItemId { get; set; }
        /// <summary>
        /// Chỉ dùng cho các thao tác giao tiếp với mongodb
        /// </summary>
        public long? ParentItemId { get; set; }
        public string ItemSku { get; set; }
        public string ItemName { get; set; }
        public List<string> ItemImages { get; set; }
        public byte Type { get; set; }
        public int RetailerId { get; set; }
        public string RetailerCode { get; set; }
        public int BranchId { get; set; }
        public byte ChannelType { get; set; }
        public long ChannelId { get; set; }
        public bool IsConnected { get; set; }
        public string Status { get; set; }
        public int? SyncStatusCode { get; set; }
        [BsonElement]
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime CreatedDate { get; set; }
        [BsonElement]
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? ModifiedDate { get; set; }
        public List<SyncError> SyncErrors { get; set; }
        public string Keyword
        {
            get
            {
                return CommonItemId + ";$#" + ItemSku + ";$#" + ItemName + ";$#" + CommonParentItemId;
            }
            set
            {
                _ = CommonItemId + ";$#" + ItemSku + ";$#" + ItemName + ";$#" + CommonParentItemId;

            }
        }
        /// <summary>
        /// Chỉ dùng riêng cho Tiki - tác vụ link sản phẩm đến sàn
        /// </summary>
        public long? SuperId { get; set; }

        public Product()
        {
            SyncErrors = new List<SyncError>();
        }
        /// <summary>
        /// Chỉ dùng cho các thao tác giao tiếp với mongodb
        /// </summary>
        public string StrItemId { get; set; }
        /// <summary>
        /// Chỉ dùng cho các thao tác giao tiếp với mongodb
        /// </summary>
        public string StrParentItemId { get; set; }
        /// <summary>
        /// Dùng cho truy xuất ItemId trên memory (không dùng với mongodb)
        /// </summary>
        [BsonIgnore]
        public string CommonItemId
        {
            get
            {
                if (ItemId.GetValueOrDefault() > 0)
                {
                    return ItemId.ToString();
                }
                else
                {
                    return StrItemId;
                }
            }
            set
            {
                try
                {
                    StrItemId = value;
                    ItemId = long.Parse(value);
                }
                catch
                {
                    ItemId = 0;
                }
            }
        }
        /// <summary>
        /// Dùng cho truy xuất ParentItemId trên memory (không dùng với mongodb)
        /// </summary>
        [BsonIgnore]
        public string CommonParentItemId
        {
            get
            {
                if (ParentItemId.GetValueOrDefault() > 0)
                {
                    return ParentItemId.ToString();
                }
                else
                {
                    return StrParentItemId;
                }
            }
            set
            {
                try
                {
                    StrParentItemId = value;
                    ParentItemId = long.Parse(value);
                }
                catch
                {
                    ParentItemId = 0;
                }
            }
        }

    }

    public class SyncError
    {
        public string Type { get; set; }
        public string Message { get; set; }
    }

}
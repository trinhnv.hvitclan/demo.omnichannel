﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Demo.OmniChannel.MongoDb
{
    [BsonIgnoreExtraElements]
    public class RetryRefreshToken : BaseEntity
    {
        public int RetailerId { get; set; }
        public int BranchId { get; set; }
        public long ChannelId { get; set; }
        public byte ChannelType { get; set; }
        public long AuthId { get; set; }
        public string ShopId { get; set; }
        public string ChannelName { get; set; }
        public string RefreshToken { get; set; }
        public string AccessToken { get; set; }
        public int RetryCount { get; set; }
        public bool IsActivate { get; set; }
        public long PlatformId { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}

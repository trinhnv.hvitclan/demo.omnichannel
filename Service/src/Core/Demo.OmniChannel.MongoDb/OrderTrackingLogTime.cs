﻿using MongoDB.Bson.Serialization.Attributes;

namespace Demo.OmniChannel.MongoDb
{
    [BsonIgnoreExtraElements]
    public class OrderTrackingLogTime : BaseEntity
    {
        public string OrderSn { get; set; }
        public byte ChannelType { get; set; }
        public long? KvReceivedTime { get; set; }
        public int KvReceivedFromSource { get; set; } // Webhook or job
        public int RetailerId { get; set; }
        public string OrderStatus { get; set; }
    }
}

﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Demo.OmniChannel.MongoDb
{
    [BsonIgnoreExtraElements]
    public class RetryOrder : BaseEntity
    {
        public string OrderId { get; set; }
        public int RetryCount { get; set; }
        public string KvEntities { get; set; }
        public int GroupId { get; set; } //Chưa dùng đến
        public long PriceBookId { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public int RetailerId { get; set; }
        public int BranchId { get; set; }
        public long ChannelId { get; set; }
        public byte ChannelType { get; set; }
        public bool IsFirstSync { get; set; }
        public string OrderStatus { get; set; }
        public string Order { get; set; }
        public bool IsDbException { get; set; }
        public long PlatformId { get; set; }
    }
}
﻿using Demo.OmniChannel.ShareKernel.Abstractions;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Demo.OmniChannel.MongoDb
{
    [BsonIgnoreExtraElements]
    public class Invoice : BaseEntity, ICreateDate, IModifiedDate
    {
        public string Uuid { get; set; }
        public string Code { get; set; }
        public DateTime PurchaseDate { get; set; }
        public string BranchName { get; set; }
        public long SoldById { get; set; }
        public int? SaleChannelId { get; set; }
        public int RetailerId { get; set; }
        public int BranchId { get; set; }
        public string SoldByName { get; set; }
        public long? CustomerId { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public long? OrderId { get; set; }
        public string OrderCode { get; set; }
        public decimal Total { get; set; }
        public decimal TotalPayment { get; set; }
        public decimal? Discount { get; set; }
        public double? DiscountRatio { get; set; }
        public byte Status { get; set; }
        public string StatusValue { get; set; }
        public string Description { get; set; }
        public long PriceBookId { get; set; }
        public bool UsingCod { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public byte? DeliveryStatus { get; set; }
        public bool IsSyncSuccess { get; set; }
        public byte? NewStatus { get; set; }
        public string ChannelStatus { get; set; }
        public string ChannelOrder { get; set; }
        public string ChannelDiscount { get; set; }
        public string ErrorMessage { get; set; }
        public string ChannelOrderId { get; set; }
        public long ChannelId { get; set; }
        public long PlatformId { get; set; }
        public decimal? Surcharge { get; set; }
        public decimal? SumDeliveryPrices { get; set; }
        public DateTime? DeliveryInfoes { get; set; }
        public InvoiceDelivery DeliveryDetail { get; set; }
        public List<InvoiceDetail> InvoiceDetails { get; set; }
        private List<InvoiceSurCharges> _invoiceSurCharges;
        public List<InvoiceSurCharges> InvoiceSurCharges
        {
            get => _invoiceSurCharges;
            set { _invoiceSurCharges = value?.Where(x => x != null).ToList(); }
        }
    }
}
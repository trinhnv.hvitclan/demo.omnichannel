﻿using Demo.OmniChannel.ShareKernel.Abstractions;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Demo.OmniChannel.MongoDb
{
    [BsonIgnoreExtraElements]
    public class SendoLocation : BaseEntity, ICreateDate, IModifiedDate
    {
        public int DistrictId { get; set; }
        public string FullName { get; set; }
        [BsonElement]
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime CreatedDate { get; set; }
        [BsonElement]
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? ModifiedDate { get; set; }
    }
}

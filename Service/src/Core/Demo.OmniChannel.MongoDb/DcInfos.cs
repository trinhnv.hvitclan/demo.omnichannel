﻿using Demo.OmniChannel.MongoDb.Common;
using System.Collections.Generic;

namespace Demo.OmniChannel.MongoDb
{
    [BsonCollection("dc_infos")]
    public class DcInfos: BaseEntity
    {
        public string Name { get; set; }
        public bool All { get; set; }
        public List<int> RetailerIds { get; set; }
        public List<int> GroupIds { get; set; }
        public List<int> RetailerIdsIgnore { get; set; }
    }
}

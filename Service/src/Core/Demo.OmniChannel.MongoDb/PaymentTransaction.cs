﻿using Demo.OmniChannel.ShareKernel.Abstractions;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Demo.OmniChannel.MongoDb
{
    [BsonIgnoreExtraElements]
    public class PaymentTransaction : BaseEntity, IRetailer, ICreateDate, IModifiedDate
    {
        public int RetailerId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public decimal Amount { get; set; }
        public bool IsPaymentAfterInvoiceCompleted { get; set; } = false;
        public string OrderSn { get; set; }
        public int ChannelType { get; set; } //Shopee;LZD;TIKI;SENDO
        public long TransactionId { get; set; }

    }

    public class PayOrder
    {
        public string OrderSn { get; set; }
        public string ShopName { get; set; }
    }
    public class PaymentChannelInvoices
    {
        public byte? ChannelType { get; set; }
        public decimal? Amount { get; set; }
        public string OrderSn { get; set; }
        public int RetailerId { get; set; }
        public string InvoiceCode { get; set; }
        public bool IsPaymentAfterInvoiceCompleted { get; set; }

    }
}

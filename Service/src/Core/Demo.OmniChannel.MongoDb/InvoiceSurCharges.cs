﻿using System;

namespace Demo.OmniChannel.MongoDb
{
    public class InvoiceSurCharges
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public long? InvoiceId { get; set; }

        public long? OrderId { get; set; }

        public int SurchargeId { get; set; }

        public Decimal? SurValue { get; set; }

        public double? SurValueRatio { get; set; }

        public Decimal Price { get; set; }

        public DateTime CreatedDate { get; set; }

        public long? ReturnId { get; set; }

        public int RetailerId { get; set; }
    }
}
﻿using Demo.OmniChannel.ShareKernel.Abstractions;
using System;
using System.Collections.Generic;

namespace Demo.OmniChannel.MongoDb
{
    public class ShopeePaymentTransactionHistory : BaseEntity, IRetailer, ICreateDate, IModifiedDate
    {
        public long TransactionId { get; set; }
        public string Status { get; set; }
        public string WalletType { get; set; }
        public string TransactionType { get; set; }
        public decimal Amount { get; set; }
        public decimal CurrentBalance { get; set; }
        public int CreateTime { get; set; }
        public string OrderSn { get; set; }
        public string RefundSn { get; set; }
        public string WithdrawalType { get; set; }
        public decimal TransactionFee { get; set; }
        public string Description { get; set; }
        public string BuyerName { get; set; }
        public List<PayOrder> PayOrderList { get; set; }
        public float WithdrawId { get; set; }
        public string Reason { get; set; }
        public float RootWithdrawalId { get; set; }
        public int RetailerId { get; set; }
        public int ChannelType { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}

﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Demo.OmniChannel.MongoDb
{
    [BsonIgnoreExtraElements]
    public class RetryInvoice : BaseEntity
    {
        public string OrderId { get; set; }
        public int RetryCount { get; set; }
        public string KvEntities { get; set; }
        public int GroupId { get; set; }
        public long PriceBookId { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public int RetailerId { get; set; }
        public int BranchId { get; set; }
        public long ChannelId { get; set; }
        public byte ChannelType { get; set; }
        public string OrderStatus { get; set; }
        public string Order { get; set; }
        public string KvOrder { get; set; }
        public long KvOrderId { get; set; }
        public bool IsDbException { get; set; }
        public string Code { get; set; }
    }
}
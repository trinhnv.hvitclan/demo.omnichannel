﻿using MongoDB.Bson.Serialization.Attributes;

namespace Demo.OmniChannel.MongoDb
{
    [BsonIgnoreExtraElements]
    public class InvoiceDetail
    {
        public double ReturnQuantity;
        public long ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public double Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal? Discount { get; set; }
        public double? DiscountRatio { get; set; }
        public bool? UsePoint { get; set; }
        public decimal SubTotal { get; set; }
        public string Note { get; set; }
        public string SerialNumbers { get; set; }
        public long? ProductChannelId { get; set; }
        public string ProductChannelSku { get; set; }
        public string ProductChannelName { get; set; }
        public long? ParentChannelProductId { get; set; }
        public float DiscountPrice { get; set; }
        public bool UseProductBatchExpire { get; set; }
        public bool UseProductSerial { get; set; }
        public long? ProductBatchExpireId { get; set; }

        public string StrProductChannelId { get; set; }
        public string StrParentChannelProductId { get; set; }
        public string Uuid { get; set; }
        public bool UseWarranty { get; set; }

        [BsonIgnore]
        public string CommonProductChannelId
        {
            get
            {
                if (ProductChannelId.GetValueOrDefault() > 0)
                {
                    return ProductChannelId.ToString();
                }
                else
                {
                    return StrProductChannelId;
                }
            }
            set
            {
                try
                {
                    StrProductChannelId = value;
                    ProductChannelId = long.Parse(value);
                }
                catch
                {
                    ProductChannelId = 0;
                }
            }
        }

        [BsonIgnore]
        public string CommonParentChannelProductId
        {
            get
            {
                if (ParentChannelProductId.GetValueOrDefault() > 0)
                {
                    return ParentChannelProductId.ToString();
                }
                else
                {
                    return StrParentChannelProductId;
                }
            }
            set
            {
                try
                {
                    StrParentChannelProductId = value;
                    ParentChannelProductId = long.Parse(value);
                }
                catch
                {
                    ParentChannelProductId = 0;
                }
            }
        }
    }
}
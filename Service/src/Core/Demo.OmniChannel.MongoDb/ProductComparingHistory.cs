﻿using Demo.OmniChannel.ShareKernel.Abstractions;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Demo.OmniChannel.MongoDb
{
    [BsonIgnoreExtraElements]
    public class ProductComparingHistory :  BaseEntity, IRetailer, ICreateDate
    {
        public long ChannelId { get; set; }

        public int RetailerId { get; set; }

        public string ItemId { get; set; }

        public string ItemSku { get; set; }

        public byte ItemType { get; set; }

        public string ParrentId { get; set; }
        /// <summary>
        /// Giá kv
        /// </summary>
        public decimal? PriceKv { get; set; }

        /// <summary>
        /// Giá khuyến mại Kv
        /// </summary>
        public decimal? SalePriceKv { get; set; }

        /// <summary>
        /// Id sản phẩm Kv
        /// </summary>
        public long KvProductId { get; set; }

        /// <summary>
        /// Ngày bắt đầu khuyến mại Kv
        /// </summary>
        public DateTime? StartSaleDateKv { get; set; }

        /// <summary>
        /// Ngày kết thúc khuyến mại Kv
        /// </summary>
        public DateTime? EndSaleDateKv { get; set; }


        /// <summary>
        /// Ngày kết thúc khuyến trên sàn
        /// </summary>
        public DateTime? SpecialFromTime { get; set; }


        /// <summary>
        /// Ngày bắt đầu khuyến mại trên sàn
        /// </summary>
        public DateTime? SpecialToTime { get; set; }

        /// <summary>
        /// Revision
        /// </summary>
        public byte[] Revision { get; set; }

        /// <summary>
        /// Giá trên sàn
        /// </summary>
        public decimal? Price { get; set; }

        /// <summary>
        /// Giá khuyến mại trên sàn
        /// </summary>
        public decimal? SpecialPrice { get; set; }

        /// <summary>
        /// Message lỗi
        /// </summary>
        public string ErrorMessage { get; set; }


        /// <summary>
        /// Giá hoặc tồn
        /// </summary>
        public byte Type { get; set; }

        /// <summary>
        /// Ngày tạo
        /// </summary>
        public DateTime CreatedDate { get; set; }
    }
}

﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Demo.OmniChannel.MongoDb
{
    public class BaseEntity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
    }
}
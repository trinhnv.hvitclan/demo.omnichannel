﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmmiChannel.MongoDb;

namespace Demo.OmmiChannel.MongoService.Interface
{
    public interface IProductMongoService
    {
        Task BatchAddProductAsync(List<Product> products);
        Task GetAllProduct();
        //Task GetAllByChannelIdAsync(long channelId, int top, int skip);
    }
}
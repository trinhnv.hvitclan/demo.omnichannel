﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmmiChannel.MongoDb;
using Demo.OmmiChannel.MongoRepository.Interface;
using Demo.OmmiChannel.MongoService.Interface;

namespace Demo.OmmiChannel.MongoService.Impls
{
    public class ProductMongoService : IProductMongoService

    {
        private IProductMongoRepository _productMongoRepository;

        public ProductMongoService(IProductMongoRepository productMongoRepository)
        {
            _productMongoRepository = productMongoRepository;
        }

        public async Task BatchAddProductAsync(List<Product> products)
        {
            await _productMongoRepository.BatchAddAsync(products);
        }

        public async Task GetAllProduct()
        {
            await _productMongoRepository.GetAll();
        }

        //public async Task GetAllByChannelIdAsync(long channelId, int top, int skip)
        //{
        //    await _productMongoRepository.GetAllByChannelId(channelId, top, skip);
        //}
    }
}
﻿using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.ShareKernel.Common;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.MongoService.Interface
{
    public interface IProductMongoService : IMongoRepository<Product>
    {
        Task<Product> GetProductById(long retailerId, long channelId, string itemId, bool isStringItemId);
        Task<List<Product>> Get(long retailerId, IList<long> channelIds, IList<string> itemIds, bool? isConnected = null, bool? isStringItemId = null);
        Task<PagingDataSource<Product>> GetAllByChannelId(long retailerId, long channelId,  int top, int skip, bool isStringItemId, string channelTerm = null, List<string> itemIds = null, bool? isConnected = null, bool? isSyncSuccess = null);
        Task<PagingDataSource<Product>> GetAllByListChannelId(long retailerId, List<long> channelIdLst, int top, int skip, bool isStringItemId, List<string> itemSkuLst, string channelTerm = null, List<string> itemIds = null, bool? isConnected = null, bool? isSyncSuccess = null);
        Task<List<Product>> Get(long retailerId, List<long> channelIds = null, string itemSku = null, bool? isConnected = null);
        Task<List<Product>> GetProductNotConnnectBySku(long retailerId, List<long> channelIds = null, string sku = null);
        Task<long> CountProductByChannelId(long retailerId, long channelId, int branchId, bool? isConnected = null);
        Task<List<Product>> GetByChannel(long channelId, int branchId);
        Task<List<Product>> GetBySearch(long retailerId, long channelId, string term, int? skip, int? top);
        Task<List<Product>> GetBySearchPaging(long retailerId, long channelId, string term, int skip, int top);
        Task<UpdateResult> UpdateProductConnect(string id, bool isConnected = true);
        Task<UpdateResult> UpdateProductsConnect(List<string> ids, bool isConnected = true);
        Task<UpdateResult> UpdateProductConnect(long retailerId, long channelId, string itemId, bool isConnected, bool isStringItemId);
        Task<Product> GetByProductChannelId(long retailerId, long channelId, string productChannelid, bool isStringItemId);
        Task<List<Product>> GetByProductChannelIds(long retailerId, long channelId, List<string> productChannelIds, bool isStringItemId);
        Task<UpdateResult> UpdateBranch(long retailerId, long channelId, long branchId);
        Task<bool> RemoveByChannelId(int retailerId, long channelId);
        Task<bool> RemoveAllSyncError(long channelId, IList<string> errorTypes);
        Task<List<Product>> GetProductSyncError(long retailerId, List<long> channelIds, string errorType = null, List<string> itemIds = null);
        Task<UpdateResult> UpdateProductSyncSuccess(long retailerId, long channelId, string itemId, bool isStringItemId);
        Task<long> GetQuantityOfProductSyncError(long retailerId, List<long> channelIds);
        Task<bool> RemoveByItemIds(long retailerId, long channelId, List<string> itemIds, bool isStringItemId);
        Task<List<Product>> GetProductSameKvSku(long channelId, List<string> skus = null, bool? isConnected = null);
        Task UpdateProductName(long retailId, long channelId, string itemId, string itemName, List<Variations> variations);
        Task UpdateProducts(long retailerId, long channelId, byte channelType, List<ChannelClient.RequestDTO.Product> products, bool isStringItemId);
        Task<List<Product>> GetByItemIds(long retailerId, long channelId, List<string> itemIds, bool isStringItemId);
        Task<UpdateResult> UpdateFailedMessageAndStatusCode(List<string> ids, int? statusCode, string message, string syncType);
        Task<List<Product>> GetProductByStatusCode(int statusCode, long channelId, DateTime fromDate, DateTime toDate);
        Task<bool> RemoveByParentItemIds(long retailerId, long channelId, List<string> itemIds);
        Task<List<Product>> GetProductByParentIds(long retailerId, IList<long> channelIds, IList<string> parentIds, bool? isConnected = null);
        Task<List<Product>> GetProductConnectByChannelIds(long retailerId, List<long> channelIds = null);
        Task<bool> RemoveAllSyncErrorByChannelIds(List<long> channelIds, IList<string> errorTypes);
        Task<List<Product>> GetProductByTrackKey(long retailerId, long channelId, string trackKey, bool? isConnected = null, int? limit = 100);
        Task<List<Product>> GetByChanneId(int retailerId, long channelId);
        Task<List<Product>> GetByStrItemId(long retailerId, long channelId, List<string> itemIds);
    }
}
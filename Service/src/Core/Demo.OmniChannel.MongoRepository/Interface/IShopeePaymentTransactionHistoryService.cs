﻿using Demo.OmniChannel.MongoDb;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.MongoService.Interface
{
    public interface IShopeePaymentTransactionHistoryService : IMongoRepository<ShopeePaymentTransactionHistory>
    {
        Task<(long, long, List<ShopeePaymentTransactionHistory>)> AddOrUpdate(long retailerId, List<ShopeePaymentTransactionHistory> paymentTransactions);
        Task<(long, long, List<ShopeePaymentTransactionHistory>)> AddOrUpdateWithSession(IClientSessionHandle session, long retailerId, List<ShopeePaymentTransactionHistory> paymentTransactions);
        Task<ShopeePaymentTransactionHistory> GetByTransactionId(int transactionId);
        Task<List<ShopeePaymentTransactionHistory>> GetByTransactionIds(List<long> transactionIds);
        Task<List<ShopeePaymentTransactionHistory>> GetPaymentChannelInvoices(long retailerId, List<string> orderSnList);
        Task<List<ShopeePaymentTransactionHistory>> GetPaymentChannelInvoices(IClientSessionHandle session, long retailerId, List<string> orderSnList);

    }
}

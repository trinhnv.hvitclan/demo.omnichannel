﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.MongoDb;

namespace Demo.OmniChannel.MongoService.Interface
{
    public interface IRetryInvoiceMongoService : IMongoRepository<RetryInvoice>
    {
        Task<List<RetryInvoice>> GetByChannelType(long channelType, int retryCount, int top, int skip);
        Task<RetryInvoice> GetByOrderId(string orderId, long channelId, int branchId, int retailerId);
        Task<long> TotalByChannelType(long channelType, int retryCount);
        Task<bool> RemoveByChannelId(int retailerId, long channelId);
        Task<long> RemoveOldErrorInvoiceByChannelType(long channelType, int retryCount, DateTime createAfter);

        Task<bool> RemoveMultiByIds(List<string> ids, long channelIds,
            int retailerIds);
    }
}
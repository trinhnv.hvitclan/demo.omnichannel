﻿using Demo.OmniChannel.MongoDb;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.MongoService.Interface
{
    public interface ISendoLocationMongoService : IMongoRepository<SendoLocation>
    {
        Task<SendoLocation> GetByDistrictId(int districtId);
        Task<List<SendoLocation>> GetByDistrictIds(List<int> districtIds);
        /// <summary>
        /// Thêm hoặc cập nhật khu vực hành chính lấy từ Sendo
        /// </summary>
        /// <param name="sendoLocations"></param>
        /// <returns>(số item thêm mới, số item cập nhật)</returns>
        Task<(long, long)> AddOrUpdate(List<SendoLocation> sendoLocations);
    }
}

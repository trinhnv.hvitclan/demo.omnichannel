﻿using Demo.OmniChannel.MongoDb;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.MongoService.Interface
{
    public interface ILazadaFeePaymentTransactionHistoryService : IMongoRepository<LazadaFeePaymentTransactionHistory>
    {
        Task AddSessionBulkAsync(IClientSessionHandle session, List<LazadaFeePaymentTransactionHistory> insertedOrderSnLst, int pageSize = 200);
        Task<List<LazadaFeePaymentTransactionHistory>> GetListByOrderSn(List<string> orderSnList);
        Task<List<LazadaFeePaymentTransactionHistory>> GetSessionListByOrderSn(IClientSessionHandle session, List<string> orderSnList);
        Task UpdateSessionBulkAsync(IClientSessionHandle session, List<LazadaFeePaymentTransactionHistory> updateLst, int pageSize = 200);

    }
}
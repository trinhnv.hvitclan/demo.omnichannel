﻿using Demo.OmniChannel.MongoDb;
using System.Threading.Tasks;

namespace Demo.OmniChannel.MongoService.Interface
{
    public interface IOrderTrackingLogTimeMongoService: IMongoRepository<OrderTrackingLogTime>
    {
        Task<bool> CheckExistedByOrderId(string orderSn, int retailerId);
    }
}

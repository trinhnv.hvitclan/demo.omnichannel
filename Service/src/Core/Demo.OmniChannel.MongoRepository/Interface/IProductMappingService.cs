﻿using Demo.OmniChannel.MongoDb;
using ServiceStack.Caching;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.MongoService.Interface
{
    public interface IProductMappingService : IMongoRepository<ProductMapping>
    {
        Task<List<ProductMapping>> Get(long retailerId, IList<long> channelIds, IList<long> kvProductIds);
        Task<List<ProductMapping>> Get(long retailerId, long? channelId, List<string> kvSkus = null,
            List<string> channelSkus = null, List<string> strProductChannelId = null);
        Task<List<ProductMapping>> GetByChannels(long retailerId, List<long> channelIds, List<string> channelSkus = null,
            string kvProductSearchTerm = null, List<string> strProductChannelId = null);
        Task<List<ProductMapping>> GetBySearchKvProduct(long retailerId, long channelId, string kvTerm);
        Task<List<ProductMapping>> Get(IList<long> channelIds);
        Task<ProductMapping> GetByProductId(long retailerId, long channelId, long kvProductId, string channelProductId, bool isStringItemId);
        Task<ProductMapping> Remove(long retailerId, long channelId, long kvProductId, string channelProductId, bool isStringItemId);
        Task<ProductMapping> RemoveByProductChannelId(long retailerId, long channelId, string channelProductId, bool isStringItemId);
        Task<List<ProductMapping>> RemoveByChannelId(long channelId);
        Task<ProductMapping> GetByKvProductSku(long retailerId, long channelId, string sku);
        Task AddOrUpdate(long retailerId, ProductMapping productMapping);
        Task AddOrUpdate(long retailerId, List<ProductMapping> listProductMapping);
        Task<List<ProductMapping>> DeleteByKvProductId(long retailerId, List<long> kvProductIds, List<long> channelIds = null);
        Task<ProductMapping> GetByKvSingleProductId(long retailerId, long channelId, long productId);
        Task<List<ProductMapping>> GetListByKvSingleProductId(long retailerId, long channelId, long productId);
        Task<List<ProductMapping>> GetListActualNameByKvSingleProductId(long retailerId, long branchId, long channelId, long productId, bool isStringItemId);
        Task<List<ProductMapping>> GetByKvSingleProductIdChannelIds(long retailerId, List<long> channelIds, long productId);
        Task<List<ProductMapping>> GetByChannelProductIds(long retailerId, long channelId, List<string> channelProductIds, List<long> kvProductIds = null, bool isFromSendo = false, bool isStringItemId = false);
        Task<List<ProductMapping>> GetByChannelProductSkus(long retailerId, long channelId, List<string> productSkus);
        Task<List<ProductMapping>> GetByKvProductId(long retailerId, long productId);
        Task<List<ProductMapping>> GetByKvProductIds(long retailerId, List<long> productIds);
        Task<List<ProductMapping>> GetByChannelId(long channelId);
        Task<List<ProductMapping>> GetByRetailerChannelId(long retailerId, long channelId);
        Task<List<ProductMapping>> RemoveByChannelProductId(int retailerId, long channelId, List<string> channelProductIds, bool isStringItemId);
        Task<List<ProductMapping>> GetByProductChannelIds(long retailerId, long channelId, List<string> productChannelIds, bool isStringItemId);
        Task<List<ProductMapping>> GetMappingForDelete(long retailerId, long channelId, List<ChannelClient.RequestDTO.Product> products, bool isStringItemId);
        Task UpdateMappings(long retailerId, long channelId, List<ChannelClient.RequestDTO.Product> products, bool isStringItemId, ICacheClient cacheClient = null);
        Task<List<ProductMapping>> GetByChannelProductParentIds(long retailerId, long channelId, List<string> channelParentProductIds, List<long> kvProductIds = null);
        Task RemoveByChannelIds(List<long> channelIds);
    }
}
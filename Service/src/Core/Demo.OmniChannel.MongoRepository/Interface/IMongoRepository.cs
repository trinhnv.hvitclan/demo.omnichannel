﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.MongoService.Interface
{
    public interface IMongoRepository<TEntity> where TEntity : class
    {
        Task<TEntity> AddSync(TEntity obj);
        Task<TEntity> GetByIdAsync(string id);
        Task<IEnumerable<TEntity>> GetAll();
        Task<TEntity> UpdateAsync(string id, TEntity obj);
        Task<bool> RemoveAsync(string id);
        Task<bool> Remove(FilterDefinition<TEntity> filter);
        Task BatchAddAsync(List<TEntity> l);
        Task DeleteManyAsync(FilterDefinition<TEntity> filter);
        Task<long> CountAsync(FilterDefinition<TEntity> filter);

        Task<IClientSessionHandle> GetSessionHandle();
    }
}
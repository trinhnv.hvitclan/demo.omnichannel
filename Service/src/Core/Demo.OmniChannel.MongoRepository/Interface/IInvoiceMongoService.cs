﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.ShareKernel.Common;

namespace Demo.OmniChannel.MongoService.Interface
{
    public interface IInvoiceMongoService : IMongoRepository<Invoice>
    {
        Task<Invoice> GetByInvoiceId(string orderId, int branchId, long channelId, string invoiceCode);
        Task<List<Invoice>> GetByOrderId(string orderId, int branchId, long channelId, string invoiceCode = null);
        Task<PagingDataSource<Invoice>> GetByChannelId(int retailerId, int top, int skip, string invoiceCode, string searchTerm, List<long> channelIds = null,
            List<int> branchIds = null, string keySearchProduct = null);

        Task<Invoice> UpdateMessageById(string id, string errorMessage);
        Task<long> GetTotalInvoiceError(int retailerId, List<long> channelIds);
        Task<bool> RemoveByChannelId(int retailerId,long channelId);
        Task<bool> RemoveMultiInvoicesByIds(List<string> orderIds, long channelIds, int retailerIds);
        Task<List<Invoice>> GetInvoiceByErrorMessage(List<string> errorMessages,
            DateTime fromDate, DateTime toDate, List<int> includeRetailerIds, List<int> excludeRetailerIds);
        Task<List<Invoice>> GetByChannelIds(int retailerId, List<long> channelIds);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.ShareKernel.Common;

namespace Demo.OmniChannel.MongoService.Interface
{
    public interface IOrderMongoService : IMongoRepository<Order>
    {
        Task<Order> GetByOrderId(string orderId, int branchId, long channelId);
        Task<PagingDataSource<Order>> GetByChannelId(int retailerId, int top, int skip, string orderCode, string searchTerm, List<long> channelIds = null,
            List<int> branchIds = null, string keySearchProduct = null);
        Task<List<Order>> GetByChannelId(int retailerId, long channelId, DateTime createBefore);
        Task<Order> UpdateMessageById(string id, string errorMessage);
        Task<long> GetTotalOrderError(int retailerId, List<long> channelIds);
        Task<bool> RemoveByChannelId(int retailerId, long channelId);
        Task<bool> RemoveMultiOrdersByIds(List<string> orderIds, long channelIds, int retailerIds);
        Task<List<Order>> GetByChannelIds(List<int> retailerIds, List<long> channelIds);
        Task<List<Order>> GetOrderByErrorMessage(List<string> errorMessages, DateTime fromDate, DateTime toDate, List<int> includeRetailerIds, List<int> excludeRetailerIds);
    }
}
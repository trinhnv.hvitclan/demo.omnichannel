﻿using Demo.OmniChannel.MongoDb;

namespace Demo.OmniChannel.MongoService.Interface
{
    public interface IProductComparingHistoryService : IMongoRepository<ProductComparingHistory>
    {

    }
}

﻿using Demo.OmniChannel.MongoDb;

namespace Demo.OmniChannel.MongoService.Interface
{
    public interface IDcInfosService : IMongoRepository<DcInfos>
    {
    }
}

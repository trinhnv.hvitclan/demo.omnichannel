﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.MongoDb;

namespace Demo.OmniChannel.MongoService.Interface
{
    public interface IRetryOrderMongoService : IMongoRepository<RetryOrder>
    {
        Task<List<RetryOrder>> GetByChannelType(long channelType, int retryCount, int top, int skip);
        Task<RetryOrder> GetByOrderId(string orderId, long channelId, int branchId, int retailerId);
        Task<long> TotalByChannelType(long channelType, int retryCount);
        Task<bool> RemoveByChannelId(int retailerId, long channelId);
        Task<long> RemoveOldErrorOrderByChannelType(long channelType, int retryCount, DateTime createAfter);

        Task<bool> RemoveMultiByIds(List<string> ids, long channelIds,
            int retailerIds);

        Task<List<RetryOrder>> GetByOrderByIds(List<string> orderIds, long channelId, int branchId, int retailerId);

        Task BatchUpdateAsync(List<RetryOrder> l, int retailerId, long channelId, int branchId);


    }
}
﻿using Demo.OmniChannel.MongoDb;
using System.Threading.Tasks;

namespace Demo.OmniChannel.MongoService.Interface
{
    public interface IRetryRefreshTokenMongoService : IMongoRepository<RetryRefreshToken>
    {
        Task<RetryRefreshToken> AddWithoutExist(RetryRefreshToken retryRefreshToken);
    }
}

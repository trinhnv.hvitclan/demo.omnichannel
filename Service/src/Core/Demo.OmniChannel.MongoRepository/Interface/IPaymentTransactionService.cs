﻿using Demo.OmniChannel.MongoDb;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.MongoService.Interface
{
    public interface IPaymentTransactionService : IMongoRepository<PaymentTransaction>
    {
        Task<List<PaymentTransaction>> GetByListOrderSn(long retailerId, List<string> orderSnLst);
        Task<List<PaymentChannelInvoices>> GetPaymentChannelInvoices(long retailerId, List<string> orderSnLst);
        Task<(long, long)> AddOrUpdate(long retailerId, List<PaymentTransaction> paymentTransactions);
        Task AddOrUpdate(long retailerId, PaymentTransaction paymentTransaction);
        Task<bool> AddOrUpdateUnitOfWork(IClientSessionHandle session, List<PaymentTransaction> newPaymentTransactions, List<PaymentTransaction> updatePaymentTransactions);
        Task<List<PaymentTransaction>> GetSessionByListOrderSn(IClientSessionHandle session, long retailerId, List<string> orderSnLst);
    }
}

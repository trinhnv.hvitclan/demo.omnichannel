﻿using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace Demo.OmniChannel.MongoService.Impls
{
    public class RetryRefreshTokenMongoService : MongoRepository<RetryRefreshToken>, IRetryRefreshTokenMongoService
    {
        public RetryRefreshTokenMongoService(IMongoDatabase database) : base(database)
        {
        }

        public async Task<RetryRefreshToken> AddWithoutExist(RetryRefreshToken retryRefreshToken) {
            var filter = Builders<RetryRefreshToken>.Filter.Eq(x => x.RetailerId, retryRefreshToken.RetailerId);
            filter = filter & Builders<RetryRefreshToken>.Filter.Eq(x => x.ChannelId, retryRefreshToken.ChannelId);
            filter = filter & Builders<RetryRefreshToken>.Filter.Eq(x => x.IsActivate, true);
            RetryRefreshToken data = await DbSet.Find(filter).FirstOrDefaultAsync();
            if (data == null)
            {
                return await AddSync(retryRefreshToken);
            }

            return data;
        }
    }
}

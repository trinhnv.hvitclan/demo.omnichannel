﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.MongoService.Impls
{
    public class PaymentTransactionService : MongoRepository<PaymentTransaction>, IPaymentTransactionService
    {

        public PaymentTransactionService(IMongoDatabase database) : base(database)
        {
        }

        public async Task<List<PaymentTransaction>> GetSessionByListOrderSn(IClientSessionHandle session, long retailerId, List<string> orderSnLst)
        {
            var filterPayment = Builders<PaymentTransaction>.Filter.Eq(t => t.RetailerId, retailerId);
            if (orderSnLst.Any())
            {
                filterPayment = filterPayment & Builders<PaymentTransaction>.Filter.In(x => x.OrderSn, orderSnLst);
            }
            return await DbSet.Find(session, filterPayment).ToListAsync();
        }

        public async Task<List<PaymentTransaction>> GetByListOrderSn(long retailerId, List<string> orderSnLst)
        {
            var filterPayment = Builders<PaymentTransaction>.Filter.Eq(t => t.RetailerId, retailerId);
            if (orderSnLst.Any())
            {
                filterPayment = filterPayment & Builders<PaymentTransaction>.Filter.In(x => x.OrderSn, orderSnLst);
            }
            return await DbSet.Find(filterPayment).ToListAsync();
        }

        public async Task<List<PaymentChannelInvoices>> GetPaymentChannelInvoices(long retailerId, List<string> orderSnLst)
        {
            orderSnLst = orderSnLst.Select(item => ChannelTypeHelper.DeSufixInvoiceCode(item)).ToList();
            List<PaymentChannelInvoices> paymentChannelInvoices = new List<PaymentChannelInvoices>();
            var resultMongo = await GetByListOrderSn(retailerId, orderSnLst);
            if (resultMongo.Any())
            {
                paymentChannelInvoices = resultMongo.Select(item => new PaymentChannelInvoices()
                {
                    Amount = item.Amount,
                    ChannelType = (byte?)item.ChannelType,
                    OrderSn = item.OrderSn,
                    RetailerId = item.RetailerId,
                    InvoiceCode = ChannelTypeHelper.ConvertInvoiceCode(item.OrderSn, (byte)item.ChannelType),
                    IsPaymentAfterInvoiceCompleted = item.IsPaymentAfterInvoiceCompleted
                }).ToList();
            }
            return paymentChannelInvoices;
        }

        public async Task<(long, long)> AddOrUpdate(long retailerId, List<PaymentTransaction> paymentTransactions)
        {
            var paymentTrasactionIds = paymentTransactions.Select(t => t.TransactionId).ToList();
            var existPaymentTransaction = await GetByTransactionIds(paymentTrasactionIds);

            var newPaymentTransaction = new List<PaymentTransaction>();
            var bulkUpdateTransactions = new List<WriteModel<PaymentTransaction>>();

            foreach (var item in paymentTransactions)
            {
                var existed = existPaymentTransaction.FirstOrDefault(t => t.TransactionId == item.TransactionId && t.RetailerId == item.RetailerId);
                if (existed != null)
                {
                    if (item.Amount == existed.Amount)
                    {
                        continue;
                    }

                    var updatePayment = Builders<PaymentTransaction>.Update
                          .Set(t => t.Amount, item.Amount)
                          .Set(x => x.RetailerId, item.RetailerId)
                          .Set(x => x.TransactionId, item.TransactionId)
                          .Set(x => x.IsPaymentAfterInvoiceCompleted, item.IsPaymentAfterInvoiceCompleted)
                          .Set(x => x.ChannelType, item.ChannelType)
                          .Set(x => x.ModifiedDate, DateTime.Now);

                    var filterPayment =
                        Builders<PaymentTransaction>.Filter.Eq(t => t.TransactionId, item.TransactionId);
                    filterPayment &= Builders<PaymentTransaction>.Filter.Eq(x => x.RetailerId, item.RetailerId);

                    var upsertOne = new UpdateOneModel<PaymentTransaction>(filterPayment, updatePayment) { IsUpsert = true };
                    bulkUpdateTransactions.Add(upsertOne);
                }
                else
                {
                    newPaymentTransaction.Add(item);
                }
            }

            if (newPaymentTransaction.Any())
            {
                await BatchAddAsync(newPaymentTransaction);
            }

            if (bulkUpdateTransactions.Any())
            {
                await DbSet.BulkWriteAsync(bulkUpdateTransactions);
            }

            return (newPaymentTransaction.Count, bulkUpdateTransactions.Count);
        }

        public async Task AddOrUpdate(long retailerId, PaymentTransaction paymentTransaction)
        {
            var existed = await GetByOrderSnAsync(retailerId, paymentTransaction.OrderSn);
            if (existed != null && existed.Amount != paymentTransaction.Amount)
            {
                var bulkUpdateTransactions = new List<WriteModel<PaymentTransaction>>();
                var updatePayment = Builders<PaymentTransaction>.Update
                          .Set(t => t.Amount, paymentTransaction.Amount)
                          .Set(x => x.ModifiedDate, DateTime.Now);
                var filterPayment =
                Builders<PaymentTransaction>.Filter.Eq(t => t.Id, existed.Id);
                var upsertOne = new UpdateOneModel<PaymentTransaction>(filterPayment, updatePayment) { IsUpsert = true };
                bulkUpdateTransactions.Add(upsertOne);
                await DbSet.BulkWriteAsync(bulkUpdateTransactions);

            }
            else if (existed == null)
            {
                await AddSync(paymentTransaction);
            }
        }

        private async Task<List<PaymentTransaction>> GetByTransactionIds(List<long> transactionIds)
        {
            var filter = Builders<PaymentTransaction>.Filter.In(t => t.TransactionId, transactionIds);

            return await DbSet.Find(filter).ToListAsync();
        }

        private async Task<PaymentTransaction> GetByOrderSnAsync(long retailerId, string orderSn)
        {
            var filter = Builders<PaymentTransaction>.Filter.Eq(t => t.RetailerId, retailerId);
            filter &= Builders<PaymentTransaction>.Filter.Eq(x => x.OrderSn, orderSn);
            return await DbSet.Find(filter).FirstOrDefaultAsync();
        }
        private List<WriteModel<PaymentTransaction>> GetBulkWriteModelPaymentTransaction(List<PaymentTransaction> updatePaymentTransactions)
        {
            var bulkUpdateTransactions = new List<WriteModel<PaymentTransaction>>();
            foreach (var item in updatePaymentTransactions)
            {
                var updatePayment = Builders<PaymentTransaction>.Update
                      .Set(t => t.Amount, item.Amount)
                      .Set(x => x.RetailerId, item.RetailerId)
                      .Set(x => x.TransactionId, item.TransactionId)
                      .Set(x => x.IsPaymentAfterInvoiceCompleted, item.IsPaymentAfterInvoiceCompleted)
                      .Set(x => x.ChannelType, item.ChannelType)
                      .Set(x => x.ModifiedDate, DateTime.Now);

                var filterPayment =
                    Builders<PaymentTransaction>.Filter.Eq(t => t.TransactionId, item.TransactionId);
                filterPayment &= Builders<PaymentTransaction>.Filter.Eq(x => x.RetailerId, item.RetailerId);

                var upsertOne = new UpdateOneModel<PaymentTransaction>(filterPayment, updatePayment) { IsUpsert = true };
                bulkUpdateTransactions.Add(upsertOne);
            }
            return bulkUpdateTransactions;
        }

        public async Task<bool> AddOrUpdateUnitOfWork(IClientSessionHandle session, List<PaymentTransaction> newPaymentTransactions, List<PaymentTransaction> updatePaymentTransactions)
        {
            if (newPaymentTransactions.Count > 0 || updatePaymentTransactions.Count > 0)
            {
                // OpenTransaction
                try
                {
                    if (updatePaymentTransactions.Count > 0)
                    {
                        var bulkUpdateTransactions = GetBulkWriteModelPaymentTransaction(updatePaymentTransactions);
                        await DbSet.BulkWriteAsync(session, bulkUpdateTransactions);
                    }
                    if (newPaymentTransactions.Count > 0)
                    {
                        await DbSet.InsertManyAsync(session, newPaymentTransactions);
                    }
                }
                catch
                {
                    await session.AbortTransactionAsync();
                    throw;
                }
            }
            return true;
        }
    }
}

﻿using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.ShareKernel.Common;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Demo.OmniChannel.MongoService.Impls
{
    public class InvoiceMongoService : MongoRepository<Invoice>, IInvoiceMongoService
    {
        public InvoiceMongoService(IMongoDatabase database) : base(database)
        {
        }

        public async Task<Invoice> GetByInvoiceId(string orderId, int branchId, long channelId, string invoiceCode)
        {
            var filter = Builders<Invoice>.Filter.Eq(x => x.ChannelId, channelId);
            filter = filter & Builders<Invoice>.Filter.Eq(x => x.BranchId, branchId);
            filter = filter & Builders<Invoice>.Filter.Eq(x => x.ChannelOrderId, orderId);
            if (!string.IsNullOrEmpty(invoiceCode))
            {
                filter = filter & Builders<Invoice>.Filter.Eq(x => x.Code, invoiceCode);
            }

            return await DbSet.Find(filter).FirstOrDefaultAsync();
        }

        public async Task<List<Invoice>> GetByOrderId(string orderId, int branchId, long channelId, string invoiceCode = null)
        {
            var filter = Builders<Invoice>.Filter.Eq(x => x.ChannelId, channelId);
            filter = filter & Builders<Invoice>.Filter.Eq(x => x.BranchId, branchId);
            filter = filter & Builders<Invoice>.Filter.Eq(x => x.ChannelOrderId, orderId);

            if (!string.IsNullOrEmpty(invoiceCode))
            {
                filter = filter & Builders<Invoice>.Filter.Eq(x => x.Code, invoiceCode);
            }

            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task<PagingDataSource<Invoice>> GetByChannelId(int retailerId, int top, int skip, string invoiceCode, string searchTerm, List<long> channelIds = null,
            List<int> branchIds = null, string keySearchProduct = null)
        {
            var filter = Builders<Invoice>.Filter.Eq(x => x.RetailerId, retailerId);
            if (channelIds != null && channelIds.Any())
            {
                filter = filter & Builders<Invoice>.Filter.In(x => x.ChannelId, channelIds);
            }
            if (branchIds != null && branchIds.Any())
            {
                filter = filter & Builders<Invoice>.Filter.In(x => x.BranchId, branchIds);
            }

            if (!string.IsNullOrEmpty(invoiceCode))
            {
                filter = filter & Builders<Invoice>.Filter.Regex(x => x.Code, new BsonRegularExpression(invoiceCode.Replace(".", "[.]"), "i"));
            }
            if (!string.IsNullOrEmpty(searchTerm))
            {
                filter = filter & (Builders<Invoice>.Filter.Regex(x => x.CustomerCode, new BsonRegularExpression(searchTerm, "i"))
                                   | Builders<Invoice>.Filter.Regex(x => x.CustomerName, new BsonRegularExpression(searchTerm, "i"))
                                   | Builders<Invoice>.Filter.Regex(x => x.CustomerPhone, new BsonRegularExpression(searchTerm, "i")));
            }
            if (!string.IsNullOrEmpty(keySearchProduct))
            {

                Regex rgx = new Regex(@"(['^$.|?*+()+\[\]\\\\])");
                keySearchProduct = rgx.Replace(keySearchProduct, "\\$1");
                var bsonRegexExpression = new Regex(keySearchProduct, RegexOptions.IgnoreCase);

                filter = filter & (
                    Builders<Invoice>.Filter.Regex(x => x.ErrorMessage, new BsonRegularExpression(bsonRegexExpression))
                    | Builders<Invoice>.Filter.Regex("InvoiceDetails.ProductChannelSku", new BsonRegularExpression(keySearchProduct, "i"))
                    | Builders<Invoice>.Filter.Regex("InvoiceDetails.ProductChannelId", new BsonRegularExpression(keySearchProduct, "i"))
                    | Builders<Invoice>.Filter.Regex("InvoiceDetails.ProductChannelName", new BsonRegularExpression(bsonRegexExpression))
                    | Builders<Invoice>.Filter.Regex("InvoiceDetails.ProductName", new BsonRegularExpression(keySearchProduct, "i"))
                    | Builders<Invoice>.Filter.Regex("InvoiceDetails.ProductCode", new BsonRegularExpression(keySearchProduct, "i")));
            }
            var result = new PagingDataSource<Invoice>
            {
                Total = await DbSet.CountDocumentsAsync(filter),
                Data = await DbSet.Find(filter).SortByDescending(x => x.CreatedDate).Skip(skip).Limit(top).ToListAsync()
            };
            return result;
        }

        public async Task<Invoice> UpdateMessageById(string id, string errorMessage)
        {
            var filter = Builders<Invoice>.Filter.Eq(x => x.Id, id);
            var update = Builders<Invoice>.Update.Set(x => x.ErrorMessage, errorMessage);
            await DbSet.UpdateOneAsync(filter, update);
            return await DbSet.Find(filter).FirstOrDefaultAsync();
        }

        public async Task<long> GetTotalInvoiceError(int retailerId, List<long> channelIds)
        {
            var filterInvoice = Builders<Invoice>.Filter.Eq(x => x.RetailerId, retailerId);
            if (channelIds != null && channelIds.Any())
            {
                filterInvoice &= Builders<Invoice>.Filter.In(x => x.ChannelId, channelIds);
            }
            var totalInvoice = await DbSet.CountDocumentsAsync(filterInvoice);
            return totalInvoice;
        }

        public async Task<bool> RemoveByChannelId(int retailerId, long channelId)
        {
            var filter = Builders<Invoice>.Filter.Eq(x => x.ChannelId, channelId);
            if (retailerId != 0)
            {
                filter &= Builders<Invoice>.Filter.Eq(x => x.RetailerId, retailerId);
            }
            return (await DbSet.DeleteManyAsync(filter)).IsAcknowledged;
        }

        public async Task<bool> RemoveMultiInvoicesByIds(List<string> ids, long channelIds,
            int retailerIds)
        {
            var filter = Builders<Invoice>.Filter.Eq(x => x.RetailerId, retailerIds);
            filter = filter & Builders<Invoice>.Filter.Eq(x => x.ChannelId, channelIds);
            filter = filter & Builders<Invoice>.Filter.In(x => x.ChannelOrderId, ids);
            await DeleteManyAsync(filter);
            return true;
        }


        public async Task<List<Invoice>> GetInvoiceByErrorMessage(List<string> errorMessages,
            DateTime fromDate, DateTime toDate, List<int> includeRetailerIds, List<int> excludeRetailerIds)
        {
            var filter = Builders<Invoice>.Filter.Gte(x => x.ModifiedDate, fromDate);
            filter &= Builders<Invoice>.Filter.Lt(x => x.ModifiedDate, toDate);
            foreach (var errorMessageItem in errorMessages)
            {
                filter &= Builders<Invoice>.Filter.Regex(x => x.ErrorMessage, new BsonRegularExpression($"^((?!{errorMessageItem}).)*$", "i"));
            }
            if (includeRetailerIds?.Any() == true) filter = filter & Builders<Invoice>.Filter.In(x => x.RetailerId, includeRetailerIds);
            if (excludeRetailerIds?.Any() == true) filter = filter & Builders<Invoice>.Filter.Nin(x => x.RetailerId, excludeRetailerIds);

            return await DbSet.Find(filter).ToListAsync();
        }
        public async Task<List<Invoice>> GetByChannelIds(int retailerId, List<long> channelIds)
        {
            var filterInvoice = Builders<Invoice>.Filter.Eq(x => x.RetailerId, retailerId);
            filterInvoice &= Builders<Invoice>.Filter.In(x => x.ChannelId, channelIds);
            return await DbSet.Find(filterInvoice).ToListAsync();
        }
    }
}
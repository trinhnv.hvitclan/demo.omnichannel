﻿using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.MongoService.Impls
{
    public class RetryOrderMongoService : MongoRepository<RetryOrder>, IRetryOrderMongoService
    {

        public RetryOrderMongoService(IMongoDatabase database) : base(database)
        {
        }
        public async Task<List<RetryOrder>> GetByChannelType(long channelType, int retryCount, int top, int skip)
        {
            var filter = Builders<RetryOrder>.Filter.Eq(x => x.ChannelType, channelType);
            filter = filter & Builders<RetryOrder>.Filter.Lte(x => x.RetryCount, retryCount);
            return await DbSet.Find(filter).Skip(skip).Limit(top).ToListAsync();
        }

        public async Task<RetryOrder> GetByOrderId(string orderId, long channelId, int branchId, int retailerId)
        {
            var filter = Builders<RetryOrder>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<RetryOrder>.Filter.Eq(x => x.BranchId, branchId);
            filter = filter & Builders<RetryOrder>.Filter.Eq(x => x.ChannelId, channelId);
            filter = filter & Builders<RetryOrder>.Filter.Eq(x => x.OrderId, orderId);
            return await DbSet.Find(filter).FirstOrDefaultAsync();
        }

        public async Task<long> TotalByChannelType(long channelType, int retryCount)
        {
            var filter = Builders<RetryOrder>.Filter.Eq(x => x.ChannelType, channelType);
            filter = filter & Builders<RetryOrder>.Filter.Lte(x => x.RetryCount, retryCount);
            return await DbSet.CountDocumentsAsync(filter);
        }

        public async Task<bool> RemoveByChannelId(int retailerId, long channelId)
        {
            var filter = Builders<RetryOrder>.Filter.Eq(x => x.ChannelId, channelId);
            if (retailerId != 0)
            {
                filter &= Builders<RetryOrder>.Filter.Eq(x => x.RetailerId, retailerId);
            }
            return (await DbSet.DeleteManyAsync(filter)).IsAcknowledged;
        }

        public async Task<long> RemoveOldErrorOrderByChannelType(long channelType, int retryCount, DateTime createAfter)
        {
            var filter = Builders<RetryOrder>.Filter.Eq(x => x.ChannelType, channelType);
            filter = filter & Builders<RetryOrder>.Filter.Gt(x => x.RetryCount, retryCount);
            filter = filter & Builders<RetryOrder>.Filter.Lte(x => x.CreatedDate, createAfter);
            return (await DbSet.DeleteManyAsync(filter)).DeletedCount;
        }

        public async Task<bool> RemoveMultiByIds(List<string> ids, long channelIds,
            int retailerIds)
        {
            var filter = Builders<RetryOrder>.Filter.Eq(x => x.RetailerId, retailerIds);
            filter = filter & Builders<RetryOrder>.Filter.Eq(x => x.ChannelId, channelIds);
            filter = filter & Builders<RetryOrder>.Filter.In(x => x.OrderId, ids);
            await DeleteManyAsync(filter);
            return true;
        }

        public async Task<List<RetryOrder>> GetByOrderByIds(List<string> orderIds, long channelId, int branchId, int retailerId)
        {
            var filter = Builders<RetryOrder>.Filter.Eq(x => x.RetailerId, retailerId);
            filter &= Builders<RetryOrder>.Filter.Eq(x => x.BranchId, branchId);
            filter &= Builders<RetryOrder>.Filter.Eq(x => x.ChannelId, channelId);
            filter &= Builders<RetryOrder>.Filter.In(x => x.OrderId, orderIds);
            var orderRetryList = await DbSet.Find(filter).ToListAsync();
            return orderRetryList;
        }


        public async Task BatchUpdateAsync(List<RetryOrder> retryList, int retailerId, long channelId, int branchId)
        {
            //var requests = new List<UpdateOneModel<RetryOrder>>(retryList.Count);
            var newItem = new List<RetryOrder>();
            var bulkOps = new List<WriteModel<RetryOrder>>();
            foreach (var retryOrder in retryList)
            {
                var filter = new FilterDefinitionBuilder<RetryOrder>()
                    .Where(o => o.RetailerId == retailerId && o.ChannelId == channelId && o.BranchId == branchId);
                var count = await DbSet.CountDocumentsAsync(filter);
                if (count > 0)
                {
                    var update = new UpdateDefinitionBuilder<RetryOrder>().Set(o => o, retryOrder);
                    var upsertOne = new UpdateOneModel<RetryOrder>(filter, update) { IsUpsert = true };
                    bulkOps.Add(upsertOne);
                }
                else
                {
                    newItem.Add(retryOrder);
                }
            }

            if (newItem.Any())
            {
                await BatchAddAsync(newItem);
            }
            if (bulkOps.Any())
            {
                await DbSet.BulkWriteAsync(bulkOps);
            }
        }

    }
}
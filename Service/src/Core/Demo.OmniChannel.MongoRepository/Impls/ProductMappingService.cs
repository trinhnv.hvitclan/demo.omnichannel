﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.ShareKernel.Common;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;//NOSONAR
using ServiceStack.Caching;

namespace Demo.OmniChannel.MongoService.Impls
{
    public class ProductMappingService : MongoRepository<ProductMapping>, IProductMappingService
    {
        public ProductMappingService(IMongoDatabase database) : base(database)
        {
        }

        public async Task<List<ProductMapping>> Get(long retailerId, IList<long> channelIds, IList<long> kvProductIds)
        {
            var filter = Builders<ProductMapping>.Filter.Eq(x => x.RetailerId, retailerId);

            if (channelIds?.Any() == true)
            {
                filter &= Builders<ProductMapping>.Filter.In(x => x.ChannelId, channelIds);
            }

            if (kvProductIds?.Any() == true)
            {
                filter &= Builders<ProductMapping>.Filter.In(x => x.ProductKvId, kvProductIds);
            }

            var result = await DbSet.Find(filter).ToListAsync();

            return result;
        }

        public async Task<List<ProductMapping>> Get(IList<long> channelIds)
        {
            var result = new List<ProductMapping>();

            if (channelIds?.Any() != true) return result;

            var filter = Builders<ProductMapping>.Filter.In(x => x.ChannelId, channelIds);

            result = await DbSet.Find(filter).ToListAsync();

            return result;
        }

        public async Task<List<ProductMapping>> Get(long retailerId, long? channelId,
            List<string> kvSkus = null,
            List<string> channelSkus = null,
            List<string> strProductChannelId = null)
        {
            var filter = Builders<ProductMapping>.Filter.Eq(x => x.RetailerId, retailerId);
            if (channelId.HasValue)
            {
                filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.ChannelId, channelId);
            }
            if (kvSkus != null && kvSkus.Any())
            {
                filter = filter & Builders<ProductMapping>.Filter.In(x => x.ProductKvSku, kvSkus);
            }
            if (channelSkus != null && channelSkus.Any())
            {
                filter = filter & Builders<ProductMapping>.Filter.In(x => x.ProductChannelSku, channelSkus);
            }
            if(strProductChannelId != null && strProductChannelId.Any())
            {
                filter = filter & Builders<ProductMapping>.Filter.In(x => x.StrProductChannelId, strProductChannelId);
            }

            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task<List<ProductMapping>> GetByChannels(long retailerId, List<long> channelIds,
            List<string> channelSkus = null,
            string kvProductSearchTerm = null,
            List<string> strProductChannelId = null)
        {
            var filter = Builders<ProductMapping>.Filter.Eq(x => x.RetailerId, retailerId);
            if (channelIds != null && channelIds.Any())
            {
                filter = filter & Builders<ProductMapping>.Filter.In(x => x.ChannelId, channelIds);
            }
            if (channelSkus != null && channelSkus.Any())
            {
                filter = filter & Builders<ProductMapping>.Filter.In(x => x.ProductChannelSku, channelSkus);
            }
            if (!string.IsNullOrEmpty(kvProductSearchTerm))
            {
                filter = filter & (Builders<ProductMapping>.Filter.Regex(x => x.ProductKvSku, new BsonRegularExpression(kvProductSearchTerm, "i"))
                    | Builders<ProductMapping>.Filter.Regex(x => x.ProductKvFullName, new BsonRegularExpression(kvProductSearchTerm, "i")));
            }
            if (strProductChannelId != null && strProductChannelId.Any())
            {
                filter = filter & Builders<ProductMapping>.Filter.In(x => x.StrProductChannelId, strProductChannelId);
            }
            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task<ProductMapping> GetByProductId(long retailerId, long channelId, long kvProductId, string channelProductId, bool isStringItemId)
        {
            var filter = Builders<ProductMapping>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.ChannelId, channelId);
            filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.ProductKvId, kvProductId);
            if (SelfAppConfig.IsStringItemId || isStringItemId)
            {
                filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.StrProductChannelId, channelProductId);
            }
            else
            {
                filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.ProductChannelId, ParseStringToLong(channelProductId));
            }
            return await DbSet.Find(filter).FirstOrDefaultAsync();
        }

        public async Task<ProductMapping> Remove(long retailerId, long channelId, long kvProductId, string channelProductId, bool isStringItemId)
        {
            var filter = Builders<ProductMapping>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.ChannelId, channelId);
            filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.ProductKvId, kvProductId);
            if (SelfAppConfig.IsStringItemId || isStringItemId)
            {
                filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.StrProductChannelId, channelProductId);
            }
            else
            {
                filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.ProductChannelId, ParseStringToLong(channelProductId));
            }
            var map = await DbSet.Find(filter).FirstOrDefaultAsync();
            await DbSet.DeleteManyAsync(filter);
            return map;
        }

        public async Task<List<ProductMapping>> RemoveByChannelId(long channelId)
        {
            var filter = Builders<ProductMapping>.Filter.Eq(x => x.ChannelId, channelId);
            var mappings = await DbSet.Find(filter).ToListAsync();
            await DbSet.DeleteManyAsync(filter);
            return mappings;
        }

        public async Task<ProductMapping> RemoveByProductChannelId(long retailerId, long channelId, string channelProductId, bool isStringItemId)
        {
            var filter = Builders<ProductMapping>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.ChannelId, channelId);
            if (SelfAppConfig.IsStringItemId || isStringItemId)
            {
                filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.StrProductChannelId, channelProductId);
            }
            else
            {
                filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.ProductChannelId, ParseStringToLong(channelProductId));
            }
            var map = DbSet.Find(filter).FirstOrDefault();
            await DbSet.DeleteManyAsync(filter);
            return map;
        }

        public async Task<ProductMapping> GetByKvProductSku(long retailerId, long channelId, string sku)
        {
            var filter = Builders<ProductMapping>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.ChannelId, channelId)
                     & Builders<ProductMapping>.Filter.Eq(x => x.ProductKvSku, sku);
            return await DbSet.Find(filter).FirstOrDefaultAsync();
        }

        public async Task<List<ProductMapping>> GetBySearchKvProduct(long retailerId, long channelId, string kvTerm)
        {
            var filter = Builders<ProductMapping>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.ChannelId, channelId);
            filter = filter & (Builders<ProductMapping>.Filter.Regex(x => x.ProductKvSku, new BsonRegularExpression(kvTerm, "i"))
                             | Builders<ProductMapping>.Filter.Regex(x => x.ProductKvFullName, new BsonRegularExpression(kvTerm, "i")));
            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task AddOrUpdate(long retailerId, ProductMapping productMapping)
        {
            var listMappingCurrent = await Get(retailerId, channelId: null);
            var mappingsExisted = listMappingCurrent.Any(x => x.ChannelId == productMapping.ChannelId && ((productMapping.ProductChannelId.GetValueOrDefault() > 0 && x.ProductChannelId == productMapping.ProductChannelId) || x.StrProductChannelId == productMapping.StrProductChannelId));
            if (mappingsExisted)
            {
                var updateMapping = Builders<ProductMapping>.Update
                                                        .Set(x => x.RetailerId, retailerId)
                                                        .Set(x => x.ChannelId, productMapping.ChannelId)
                                                        .Set(x => x.ProductChannelId, productMapping.ProductChannelId)
                                                        .Set(x => x.ProductKvId, productMapping.ProductKvId)
                                                        .Set(x => x.ProductChannelSku, productMapping.ProductChannelSku)
                                                        .Set(x => x.ProductChannelName, productMapping.ProductChannelName)
                                                        .Set(x => x.ProductChannelType, productMapping.ProductChannelType)
                                                        .Set(x => x.ParentProductChannelId, productMapping.ParentProductChannelId)
                                                        .Set(x => x.RetailerCode, productMapping.RetailerCode)
                                                        .Set(x => x.ProductKvSku, productMapping.ProductKvSku)
                                                        .Set(x => x.ProductKvFullName, productMapping.ProductKvFullName)
                                                        .Set(x => x.LatestBasePrice, productMapping.LatestBasePrice)
                                                        .Set(x => x.LatestSalePrice, productMapping.LatestSalePrice)
                                                        .Set(x => x.LatestOnHand, productMapping.LatestOnHand)
                                                        .Set(x => x.StrProductChannelId, productMapping.StrProductChannelId)
                                                        .Set(x => x.StrParentProductChannelId, productMapping.StrParentProductChannelId)
                                                        .Set(x => x.ModifiedDate, DateTime.Now);

                var filterMapping = Builders<ProductMapping>.Filter.Eq(x => x.RetailerId, retailerId);
                filterMapping &= Builders<ProductMapping>.Filter.Eq(x => x.ChannelId, productMapping.ChannelId);

                if (productMapping.ProductChannelId > 0)
                {
                    filterMapping &= (Builders<ProductMapping>.Filter.Eq(x => x.ProductChannelId, productMapping.ProductChannelId) & Builders<ProductMapping>.Filter.Eq(x => x.ProductKvId, productMapping.ProductKvId));
                }
                else
                {
                    filterMapping &= (Builders<ProductMapping>.Filter.Eq(x => x.StrProductChannelId, productMapping.StrProductChannelId) & Builders<ProductMapping>.Filter.Eq(x => x.ProductKvId, productMapping.ProductKvId));
                }

                var bulkOps = new List<WriteModel<ProductMapping>>();
                var upsertOne = new UpdateOneModel<ProductMapping>(filterMapping, updateMapping) { IsUpsert = true };
                bulkOps.Add(upsertOne);
                await DbSet.BulkWriteAsync(bulkOps);
            }
            else
            {
                await AddSync(productMapping);
            }
        }


        public async Task AddOrUpdate(long retailerId, List<ProductMapping> productMappings)
        {
            var listMappingCurrent = await Get(retailerId, channelId: null);

            var newMappings = new List<ProductMapping>();
            var bulkOps = new List<WriteModel<ProductMapping>>();
            foreach (var item in productMappings)
            {
                var mappingsExisted = listMappingCurrent.Any(x => x.ChannelId == item.ChannelId && ((item.ProductChannelId.GetValueOrDefault() > 0 && x.ProductChannelId == item.ProductChannelId) || x.StrProductChannelId == item.StrProductChannelId));
                if (mappingsExisted)
                {
                    if (item.CreatedDate == default(DateTime))
                    {
                        item.CreatedDate = DateTime.Now;
                    }
                    var updateMapping = Builders<ProductMapping>.Update
                                                        .Set(x => x.RetailerId, retailerId)
                                                        .Set(x => x.ChannelId, item.ChannelId)
                                                        .Set(x => x.ProductChannelId, item.ProductChannelId)
                                                        .Set(x => x.ProductKvId, item.ProductKvId)
                                                        .Set(x => x.ProductChannelSku, item.ProductChannelSku)
                                                        .Set(x => x.ProductChannelName, item.ProductChannelName)
                                                        .Set(x => x.ProductChannelType, item.ProductChannelType)
                                                        .Set(x => x.ParentProductChannelId, item.ParentProductChannelId)
                                                        .Set(x => x.RetailerCode, item.RetailerCode)
                                                        .Set(x => x.ProductKvSku, item.ProductKvSku)
                                                        .Set(x => x.ProductKvFullName, item.ProductKvFullName)
                                                        .Set(x => x.LatestBasePrice, item.LatestBasePrice)
                                                        .Set(x => x.LatestSalePrice, item.LatestSalePrice)
                                                        .Set(x => x.LatestOnHand, item.LatestOnHand)
                                                        .Set(x => x.CreatedDate, item.CreatedDate)
                                                        .Set(x => x.StrProductChannelId, item.StrProductChannelId)
                                                        .Set(x => x.StrParentProductChannelId, item.StrParentProductChannelId)
                                                        .Set(x => x.ModifiedDate, DateTime.Now);

                    var filterMapping = Builders<ProductMapping>.Filter.Eq(x => x.RetailerId, retailerId);
                    filterMapping &= Builders<ProductMapping>.Filter.Eq(x => x.ChannelId, item.ChannelId);
                    if (item.ProductChannelId > 0)
                    {
                        filterMapping &= (Builders<ProductMapping>.Filter.Eq(x => x.ProductChannelId, item.ProductChannelId) & Builders<ProductMapping>.Filter.Eq(x => x.ProductKvId, item.ProductKvId));
                    }
                    else
                    {
                        filterMapping &= (Builders<ProductMapping>.Filter.Eq(x => x.StrProductChannelId, item.StrProductChannelId) & Builders<ProductMapping>.Filter.Eq(x => x.ProductKvId, item.ProductKvId));
                    }

                    var upsertOne = new UpdateOneModel<ProductMapping>(filterMapping, updateMapping) { IsUpsert = true };
                    bulkOps.Add(upsertOne);
                }
                else
                {
                    newMappings.Add(item);
                }
            }
            if (newMappings.Any())
            {
                await BatchAddAsync(newMappings);
            }
            if (bulkOps.Any())
            {
                await DbSet.BulkWriteAsync(bulkOps);
            }
        }

        public async Task<List<ProductMapping>> DeleteByKvProductId(long retailerId, List<long> kvProductIds, List<long> channelIds = null)
        {
            var filter = Builders<ProductMapping>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<ProductMapping>.Filter.In(x => x.ProductKvId, kvProductIds);
            if (channelIds != null && channelIds.Any())
            {
                filter = filter & Builders<ProductMapping>.Filter.In(x => x.ChannelId, channelIds);
            }
            var listMappingDeleted = await DbSet.Find(filter).ToListAsync();
            await DbSet.DeleteManyAsync(filter);
            return listMappingDeleted;
        }

        public async Task<ProductMapping> GetByKvSingleProductId(long retailerId, long channelId, long productId)
        {
            var filter = Builders<ProductMapping>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.ChannelId, channelId);
            filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.ProductKvId, productId);
            return await DbSet.Find(filter).FirstOrDefaultAsync();
        }

        public async Task<List<ProductMapping>> GetListByKvSingleProductId(long retailerId, long channelId, long productId)
        {
            var filter = Builders<ProductMapping>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.ChannelId, channelId);
            filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.ProductKvId, productId);
            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task<List<ProductMapping>> GetListActualNameByKvSingleProductId(long retailerId, long branchId,
            long channelId, long productId, bool isStringItemId)
        {
            var productQuery = Database.GetCollection<Product>(nameof(Product)).AsQueryable();
            var query = isStringItemId
                ? DbSet.AsQueryable().Join(productQuery, pm => pm.StrProductChannelId, p => p.StrItemId,
                    (productMapping, product) => new { productMapping, product })
                : DbSet.AsQueryable().Join(productQuery, pm => pm.ProductChannelId, p => p.ItemId,
                    (productMapping, product) => new { productMapping, product });
            query = query.Where(p => p.productMapping.RetailerId == retailerId &&
                                  p.productMapping.ChannelId == channelId &&
                                  p.productMapping.ProductKvId == productId &&
                                  p.product.RetailerId == retailerId &&
                                  p.product.ChannelId == channelId &&
                                  p.product.BranchId == branchId);

            var data = await query.Select(x => new ProductMapping
            {
                ProductKvId = x.productMapping.ProductKvId,
                ProductKvSku = x.productMapping.ProductKvSku,
                ProductKvFullName = x.productMapping.ProductKvFullName,
                RetailerId = x.productMapping.RetailerId,
                RetailerCode = x.productMapping.RetailerCode,
                CreatedDate = x.productMapping.CreatedDate,
                ModifiedDate = x.productMapping.ModifiedDate,
                ProductChannelId = x.productMapping.ProductChannelId,
                ParentProductChannelId = x.productMapping.ParentProductChannelId,
                ProductChannelSku = x.productMapping.ProductChannelSku,
                ProductChannelName = x.product.ItemName,
                ProductChannelType = x.productMapping.ProductChannelType,
                ChannelId = x.productMapping.ChannelId,
                LatestOnHand = x.productMapping.LatestOnHand,
                LatestBasePrice = x.productMapping.LatestBasePrice,
                LatestSalePrice = x.productMapping.LatestSalePrice,
                StrProductChannelId = x.productMapping.StrProductChannelId,
                StrParentProductChannelId = x.productMapping.StrParentProductChannelId
            }).ToListAsync();
            return data;
        }

        public async Task<List<ProductMapping>> GetByKvSingleProductIdChannelIds(long retailerId, List<long> channelIds,
            long productId)
        {
            var filter = Builders<ProductMapping>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.ProductKvId, productId);
            filter = filter & Builders<ProductMapping>.Filter.In(x => x.ChannelId, channelIds);
            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task<List<ProductMapping>> GetByChannelProductIds(long retailerId, long channelId, List<string> channelProductIds, List<long> kvProductIds = null, bool isFromSendo = false, bool isStringItemId = false)
        {
            var filter = Builders<ProductMapping>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.ChannelId, channelId);
            if (channelProductIds != null && channelProductIds.Any())
            {
                if (SelfAppConfig.IsStringItemId || isStringItemId)
                {
                    filter = filter & Builders<ProductMapping>.Filter.In(x => x.StrProductChannelId, channelProductIds);
                }
                else
                {
                    filter = filter & Builders<ProductMapping>.Filter.In(x => x.ProductChannelId, ParseStringToLong(channelProductIds));
                }
            }
            if (kvProductIds?.Any() == true)
            {
                filter = filter & Builders<ProductMapping>.Filter.In(x => x.ProductKvId, kvProductIds);
            }

            if (isFromSendo)
            {
                var result = await DbSet.Find(filter).ToListAsync();
                return result.OrderBy(x => x.StrParentProductChannelId).ToList();
            }
            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task<List<ProductMapping>> GetByChannelProductSkus(long retailerId, long channelId, List<string> productSkus)
        {
            var filter = Builders<ProductMapping>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.ChannelId, channelId);
            filter = filter & Builders<ProductMapping>.Filter.In(x => x.ProductChannelSku, productSkus);
            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task<List<ProductMapping>> GetByKvProductId(long retailerId, long kvProductId)
        {
            var filter = Builders<ProductMapping>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.ProductKvId, kvProductId);
            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task<List<ProductMapping>> GetByKvProductIds(long retailerId, List<long> productIds)
        {
            var filter = Builders<ProductMapping>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<ProductMapping>.Filter.In(x => x.ProductKvId, productIds);
            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task<List<ProductMapping>> GetByChannelId(long channelId)
        {
            return await DbSet.Find(Builders<ProductMapping>.Filter.Eq(x => x.ChannelId, channelId)).ToListAsync();
        }

        public async Task<List<ProductMapping>> GetByRetailerChannelId(long retailerId, long channelId)
        {
            var filter = Builders<ProductMapping>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.ChannelId, channelId);
            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task<List<ProductMapping>> RemoveByChannelProductId(int retailerId, long channelId, List<string> channelProductIds, bool isStringItemId)
        {
            var filter = Builders<ProductMapping>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.ChannelId, channelId);
            if (SelfAppConfig.IsStringItemId || isStringItemId)
            {
                filter = filter & Builders<ProductMapping>.Filter.In(x => x.StrProductChannelId, channelProductIds);
            }
            else
            {
                filter = filter & Builders<ProductMapping>.Filter.In(x => x.ProductChannelId, ParseStringToLong(channelProductIds));
            }
            var listMappingDeleted = await DbSet.Find(filter).ToListAsync();
            await DbSet.DeleteManyAsync(filter);
            return listMappingDeleted;
        }

        public async Task<List<ProductMapping>> GetByProductChannelIds(long retailerId, long channelId, List<string> productChannelIds, bool isStringItemId)
        {
            var filter = Builders<ProductMapping>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.ChannelId, channelId);
            if (SelfAppConfig.IsStringItemId || isStringItemId)
            {
                filter = filter & Builders<ProductMapping>.Filter.In(x => x.StrProductChannelId, productChannelIds);
            }
            else
            {
                filter = filter & Builders<ProductMapping>.Filter.In(x => x.ProductChannelId, ParseStringToLong(productChannelIds));
            }
            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task<List<ProductMapping>> GetMappingForDelete(long retailerId, long channelId, List<ChannelClient.RequestDTO.Product> products, bool isStringItemId)
        {
            var channelProductIdDeletes = new List<ProductMapping>();

            if (products?.Any() != true) return channelProductIdDeletes;
            var mappingExistedLst = await GetByProductChannelIds(retailerId, channelId, products.Select(x => x.ItemId).ToList(), isStringItemId: isStringItemId);

            foreach (var item in products)
            {
                // #TMDT-23 Đề xuất ngắt kết nối khi đổi mã SKU; tự động đẩy lại đồng bộ trùng mã SKU lại cho hàng hóa
                // Điều kiện delete: SKU sàn thay đổi, Mã hàng KV cũ != SKU sàn mới
                var mappingExisted = mappingExistedLst.FirstOrDefault(x => x.CommonProductChannelId == item.ItemId);
                if (mappingExisted != null &&
                    mappingExisted.ProductChannelSku != item.ItemSku &&
                    mappingExisted.ProductKvSku != item.ItemSku &&
                    mappingExisted.ProductChannelId.HasValue)
                {
                    channelProductIdDeletes.Add(mappingExisted);
                }
            }

            return channelProductIdDeletes;
        }

        public async Task UpdateMappings(long retailerId, long channelId, List<ChannelClient.RequestDTO.Product> products, bool isStringItemId, ICacheClient cacheClient = null)
        {
            if (products?.Any() != true) return;
            var writeModelMappings = new List<WriteModel<ProductMapping>>();
            var mappingsExisted = await GetByProductChannelIds(retailerId, channelId, products.Select(x => x.ItemId).ToList(), isStringItemId: isStringItemId);
            var updatedMappingProducts = new List<ProductMapping>();

            foreach (var item in products)
            {
                var mappingExisted = mappingsExisted.FirstOrDefault(x => x.CommonProductChannelId == item.ItemId);
                if (mappingExisted != null && (mappingExisted.ProductChannelSku != item.ItemSku || mappingExisted.ProductChannelName != item.ItemName))
                {

                    var filterMapping = Builders<ProductMapping>.Filter.Eq(x => x.RetailerId, retailerId);
                    filterMapping = filterMapping & Builders<ProductMapping>.Filter.Eq(x => x.ChannelId, channelId);
                    if (isStringItemId)
                    {
                        filterMapping = filterMapping & Builders<ProductMapping>.Filter.Eq(x => x.StrProductChannelId, item.ItemId);
                    }
                    else
                    {
                        filterMapping = filterMapping & Builders<ProductMapping>.Filter.Eq(x => x.ProductChannelId, ParseStringToLong(item.ItemId));
                    }
                    var updateMapping = Builders<ProductMapping>.Update.Set(x => x.ProductChannelSku, item.ItemSku)
                                                         .Set(x => x.ModifiedDate, DateTime.Now);

                    var upsertMapping = new UpdateOneModel<ProductMapping>(filterMapping, updateMapping) { IsUpsert = false };
                    writeModelMappings.Add(upsertMapping);
                    updatedMappingProducts.Add(mappingExisted);
                }
            }

            if (writeModelMappings != null && writeModelMappings.Any())
            {
                await DbSet.BulkWriteAsync(writeModelMappings);
            }
            if (!(cacheClient is null))
            {
                ClearUpdatedProductMappingsCache(cacheClient, retailerId, channelId, updatedMappingProducts);
            }
        }

        private void ClearUpdatedProductMappingsCache(ICacheClient cacheClient, long retailerId, long channelId, List<ProductMapping> productMappings)
        {
            var mappingKeyList = productMappings.Select(x => string.Format(KvConstant.ListProductMappingCacheKey,
                                                                            retailerId, channelId, x.ProductKvId))
                                                .Distinct().ToList();
            cacheClient.RemoveAll(mappingKeyList);
        }

        public async Task<List<ProductMapping>> GetByChannelProductParentIds(long retailerId, long channelId, List<string> channelParentProductIds, List<long> kvProductIds = null)
        {
            var filter = Builders<ProductMapping>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<ProductMapping>.Filter.Eq(x => x.ChannelId, channelId);
            if (channelParentProductIds != null && channelParentProductIds.Any())
            {
                filter = filter & Builders<ProductMapping>.Filter.In(x => x.ParentProductChannelId, ParseStringToLong(channelParentProductIds));
            }
            if (kvProductIds?.Any() == true)
            {
                filter = filter & Builders<ProductMapping>.Filter.In(x => x.ProductKvId, kvProductIds);
            }

            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task RemoveByChannelIds(List<long> channelIds)
        {
            var filter = Builders<ProductMapping>.Filter.In(x => x.ChannelId, channelIds);
            await DbSet.DeleteManyAsync(filter);
        }
    }
}
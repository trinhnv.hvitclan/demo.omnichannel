﻿using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using MongoDB.Driver;

namespace Demo.OmniChannel.MongoService.Impls
{
    public class ProductComparingHistoryService : MongoRepository<ProductComparingHistory>, IProductComparingHistoryService
    {
        public ProductComparingHistoryService(IMongoDatabase database) : base(database)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.ShareKernel.Common;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Demo.OmniChannel.MongoService.Impls
{
    public class SendoLocationMongoService : MongoRepository<SendoLocation>, ISendoLocationMongoService
    {
        public SendoLocationMongoService(IMongoDatabase database) : base(database)
        {
        }

        public async Task<(long, long)> AddOrUpdate(List<SendoLocation> sendoLocations)
        {
            var inDistrictIds = sendoLocations.Select(x => x.DistrictId).ToList();
            var existLocations = await GetByDistrictIds(inDistrictIds);

            var newLocation = new List<SendoLocation>();
            var bulkUpdateLocation = new List<WriteModel<SendoLocation>>();

            foreach (var item in sendoLocations)
            {
                var isExisted = existLocations.Any(x => x.DistrictId == item.DistrictId);
                if (isExisted)
                {
                    var updateMapping = Builders<SendoLocation>.Update
                                                        .Set(x => x.FullName, item.FullName)
                                                        .Set(x => x.ModifiedDate, DateTime.Now);

                    var filterMapping = Builders<SendoLocation>.Filter.Eq(x => x.DistrictId, item.DistrictId);

                    var upsertOne = new UpdateOneModel<SendoLocation>(filterMapping, updateMapping) { IsUpsert = true };
                    bulkUpdateLocation.Add(upsertOne);
                }
                else
                {
                    newLocation.Add(item);
                }
            }

            if (newLocation.Any())
            {
                await BatchAddAsync(newLocation);
            }

            if (bulkUpdateLocation.Any())
            {
                await DbSet.BulkWriteAsync(bulkUpdateLocation);
            }

            return (newLocation.Count, bulkUpdateLocation.Count);
        }

        public async Task<SendoLocation> GetByDistrictId(int districtId)
        {
            var filter = Builders<SendoLocation>.Filter.Eq(x => x.DistrictId, districtId);

            return await DbSet.Find(filter).FirstOrDefaultAsync();
        }

        public async Task<List<SendoLocation>> GetByDistrictIds(List<int> districtIds)
        {
            var filter = Builders<SendoLocation>.Filter.In(x => x.DistrictId, districtIds);

            return await DbSet.Find(filter).ToListAsync();
        }
    }
}
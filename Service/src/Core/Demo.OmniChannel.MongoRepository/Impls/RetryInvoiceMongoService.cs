﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using MongoDB.Driver;

namespace Demo.OmniChannel.MongoService.Impls
{
    public class RetryInvoiceMongoService : MongoRepository<RetryInvoice>, IRetryInvoiceMongoService
    {
        public RetryInvoiceMongoService(IMongoDatabase database) : base(database)
        {
        }

        public async Task<List<RetryInvoice>> GetByChannelType(long channelType, int retryCount, int top, int skip)
        {
            var filter = Builders<RetryInvoice>.Filter.Eq(x => x.ChannelType, channelType);
            filter = filter & Builders<RetryInvoice>.Filter.Lte(x => x.RetryCount, retryCount);
            return await DbSet.Find(filter).Skip(skip).Limit(top).ToListAsync();
        }

        public async Task<RetryInvoice> GetByOrderId(string orderId, long channelId, int branchId, int retailerId)
        {
            var filter = Builders<RetryInvoice>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<RetryInvoice>.Filter.Eq(x => x.BranchId, branchId);
            filter = filter & Builders<RetryInvoice>.Filter.Eq(x => x.ChannelId, channelId);
            filter = filter & Builders<RetryInvoice>.Filter.Eq(x => x.OrderId, orderId);
            return await DbSet.Find(filter).FirstOrDefaultAsync();
        }

        public async Task<long> TotalByChannelType(long channelType, int retryCount)
        {
            var filter = Builders<RetryInvoice>.Filter.Eq(x => x.ChannelType, channelType);
            filter = filter & Builders<RetryInvoice>.Filter.Lte(x => x.RetryCount, retryCount);
            return await DbSet.CountDocumentsAsync(filter);
        }

        public async Task<bool> RemoveByChannelId(int retailerId, long channelId)
        {
            var filter = Builders<RetryInvoice>.Filter.Eq(x => x.ChannelId, channelId);
            if (retailerId != 0)
            {
                filter &= Builders<RetryInvoice>.Filter.Eq(x => x.RetailerId, retailerId);
            }
            return (await DbSet.DeleteManyAsync(filter)).IsAcknowledged;
        }

        public async Task<long> RemoveOldErrorInvoiceByChannelType(long channelType, int retryCount, DateTime createAfter)
        {
            var filter = Builders<RetryInvoice>.Filter.Eq(x => x.ChannelType, channelType);
            filter = filter & Builders<RetryInvoice>.Filter.Gt(x => x.RetryCount, retryCount);
            filter = filter & Builders<RetryInvoice>.Filter.Lte(x => x.CreatedDate, createAfter);
            return (await DbSet.DeleteManyAsync(filter)).DeletedCount;
        }
        public async Task<bool> RemoveMultiByIds(List<string> ids, long channelIds,
            int retailerIds)
        {
            var filter = Builders<RetryInvoice>.Filter.Eq(x => x.RetailerId, retailerIds);
            filter = filter & Builders<RetryInvoice>.Filter.Eq(x => x.ChannelId, channelIds);
            filter = filter & Builders<RetryInvoice>.Filter.In(x => x.OrderId, ids);
            await DeleteManyAsync(filter);
            return true;
        }
    }
}
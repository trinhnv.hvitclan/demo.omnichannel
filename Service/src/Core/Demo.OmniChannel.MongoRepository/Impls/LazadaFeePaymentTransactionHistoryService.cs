﻿using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.ShareKernel.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.MongoService.Impls
{
    public class LazadaFeePaymentTransactionHistoryService : MongoRepository<LazadaFeePaymentTransactionHistory>, ILazadaFeePaymentTransactionHistoryService
    {
        public LazadaFeePaymentTransactionHistoryService(IMongoDatabase database) : base(database)
        {
        }

        public async Task<List<LazadaFeePaymentTransactionHistory>> GetSessionListByOrderSn(IClientSessionHandle session, List<string> orderSnList)
        {
            if (orderSnList.Any())
            {
                var filter = Builders<LazadaFeePaymentTransactionHistory>.Filter.In(x => x.OrderNo, orderSnList);
                return await DbSet.Find(session, filter).ToListAsync();

            }
            return new List<LazadaFeePaymentTransactionHistory>();
        }
        public async Task<List<LazadaFeePaymentTransactionHistory>> GetListByOrderSn(List<string> orderSnList)
        {
            if (orderSnList.Any())
            {
                var filter = Builders<LazadaFeePaymentTransactionHistory>.Filter.In(x => x.OrderNo, orderSnList);
                return await DbSet.Find(filter).ToListAsync();
            }
            return new List<LazadaFeePaymentTransactionHistory>();

        }
        public async Task AddSessionBulkAsync(IClientSessionHandle session, List<LazadaFeePaymentTransactionHistory> insertedOrderSnLst, int pageSize = 200)
        {
            int totalCount = insertedOrderSnLst.Count;
            var totalPages = (int)Math.Ceiling(totalCount / (decimal)pageSize);
            for (int page = 1; page <= totalPages; page++)
            {
                var insertedOrderSnLstPaging = insertedOrderSnLst.Skip((page - 1) * pageSize).Take(pageSize);
                await DbSet.InsertManyAsync(session, insertedOrderSnLstPaging);
            }

        }

        public async Task UpdateSessionBulkAsync(IClientSessionHandle session, List<LazadaFeePaymentTransactionHistory> updateLst, int pageSize = 200)
        {
            if (updateLst.Count > 0)
            {
                int totalCount = updateLst.Count;
                var totalPages = (int)Math.Ceiling(totalCount / (decimal)pageSize);
                for (int page = 1; page <= totalPages; page++)
                {
                    var updateLstPaging = updateLst.Skip((page - 1) * pageSize).Take(pageSize);
                    var bulkUpdateTransactions = new List<WriteModel<LazadaFeePaymentTransactionHistory>>();
                    foreach (var updateItem in updateLstPaging)
                    {
                        var updatePayment = Builders<LazadaFeePaymentTransactionHistory>.Update
                            .Set(t => t.Amount, updateItem.Amount)
                            .Set(x => x.ModifiedDate, DateTime.Now);
                        var filterUpdate =
                         Builders<LazadaFeePaymentTransactionHistory>.Filter.Eq(t => t.Id, updateItem.Id);
                        var upsertOne = new UpdateOneModel<LazadaFeePaymentTransactionHistory>(filterUpdate, updatePayment) { IsUpsert = true };
                        bulkUpdateTransactions.Add(upsertOne);
                    }
                    if (bulkUpdateTransactions.Any())
                    {
                        await DbSet.BulkWriteAsync(session, bulkUpdateTransactions);
                    }
                }
            }
        }
    }
}

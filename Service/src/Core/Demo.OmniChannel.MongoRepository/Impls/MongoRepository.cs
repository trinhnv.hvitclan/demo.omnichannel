﻿using Demo.OmniChannel.MongoDb.Common;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.ShareKernel.Abstractions;
using MongoDB.Driver;
using ServiceStack.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.MongoService.Impls
{
    public abstract class MongoRepository<TEntity> : IMongoRepository<TEntity> where TEntity : class

    {
        protected readonly IMongoDatabase Database;
        protected readonly IMongoCollection<TEntity> DbSet;
        public ILog Log { get; set; }
        protected MongoRepository(IMongoDatabase database)
        {
            Database = database;
            var collectionName = GetCollectionNameByAttribute();
            DbSet = String.IsNullOrEmpty(collectionName) ? database.GetCollection<TEntity>(typeof(TEntity).Name) : database.GetCollection<TEntity>(collectionName);
        }

        private static string GetCollectionNameByAttribute()
        {
            try
            {
                var attribute = typeof(TEntity).GetCustomAttributes(typeof(BsonCollectionAttribute), true)?.FirstOrDefault();
                if (attribute == null) return null;
                return (attribute as BsonCollectionAttribute).CollectionName;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public virtual async Task<TEntity> AddSync(TEntity obj)
        {
            if (obj is ICreateDate createdOn)
            {
                var created = createdOn.CreatedDate;
                if (created == default(DateTime))
                {
                    createdOn.CreatedDate = DateTime.Now;
                }
            }
            if (obj is IModifiedDate modifiedOn)
            {
                modifiedOn.ModifiedDate = DateTime.Now;
            }
            await DbSet.InsertOneAsync(obj);

            return obj;
        }

        public virtual async Task<TEntity> GetByIdAsync(string id)
        {
            var data = await DbSet.Find(FilterId(id)).SingleOrDefaultAsync();
            return data;
        }

        public virtual async Task<IEnumerable<TEntity>> GetAll()
        {
            var all = await DbSet.FindAsync(Builders<TEntity>.Filter.Empty);
            return all.ToList();
        }

        public virtual async Task<TEntity> UpdateAsync(string id, TEntity obj)
        {
            if (obj is IModifiedDate modifiedOn)
            {
                modifiedOn.ModifiedDate = DateTime.Now;
            }

            await DbSet.ReplaceOneAsync(FilterId(id), obj);
            return obj;
        }

        public virtual async Task<bool> RemoveAsync(string id)
        {
            var result = await DbSet.DeleteOneAsync(FilterId(id));
            return result.IsAcknowledged;
        }

        public virtual async Task<bool> Remove(FilterDefinition<TEntity> filter)
        {
            var result = await DbSet.DeleteOneAsync(filter);
            return result.IsAcknowledged;
        }

        public async Task BatchAddAsync(List<TEntity> l)
        {
            foreach (var t in l)
            {
                if (!(t is ICreateDate createdOn)) continue;
                var created = createdOn.CreatedDate;
                if (created == default(DateTime))
                {
                    createdOn.CreatedDate = DateTime.Now;
                }
            }
            var insertManyAsync = DbSet?.InsertManyAsync(l);
            if (insertManyAsync != null) await insertManyAsync;
        }

        public async Task BatchAddAsync(IClientSessionHandle session, List<TEntity> l)
        {
            foreach (var t in l)
            {
                if (!(t is ICreateDate createdOn)) continue;
                var created = createdOn.CreatedDate;
                if (created == default(DateTime))
                {
                    createdOn.CreatedDate = DateTime.Now;
                }
            }
            var insertManyAsync = DbSet?.InsertManyAsync(session, l);
            if (insertManyAsync != null) await insertManyAsync;
        }

        public async Task DeleteManyAsync(FilterDefinition<TEntity> filter)
        {
            var deliveryManyAsync = DbSet?.DeleteManyAsync(filter);
            if (deliveryManyAsync != null) await deliveryManyAsync;
        }

        public async Task<long> CountAsync(FilterDefinition<TEntity> filter)
        {
            return await DbSet.CountDocumentsAsync(filter);
        }


        private static FilterDefinition<TEntity> FilterId(string key)
        {
            return Builders<TEntity>.Filter.Eq("Id", key);
        }

        protected static long? ParseStringToLong(string itemId, long defaultValue = 0)
        {
            try
            {
                return long.Parse(itemId);
            }
            catch
            {
                return defaultValue;
            }
        }

        protected static List<long?> ParseStringToLong(List<string> itemIds, long defaultValue = 0)
        {
            var ids = new List<long?>();
            foreach (var item in itemIds)
            {
                try
                {
                    ids.Add(long.Parse(item));
                }
                catch
                {
                    ids.Add(defaultValue);
                }
            }
            return ids;
        }

        public async Task<IClientSessionHandle> GetSessionHandle()
        {
            return await Database.Client.StartSessionAsync();
        }
    }
}
﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.ShareKernel.Common;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Demo.OmniChannel.MongoService.Impls
{
    public class ProductMongoService : MongoRepository<Product>, IProductMongoService
    {
        public ProductMongoService(IMongoDatabase database) : base(database)
        {
        }

        public async Task<Product> GetProductById(long retailerId, long channelId, string itemId, bool isStringItemId = false)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
            filter &= Builders<Product>.Filter.Eq(x => x.ChannelId, channelId);
            if (SelfAppConfig.IsStringItemId || isStringItemId)
            {
                filter &= Builders<Product>.Filter.Eq(x => x.StrItemId, itemId);
            }
            else
            {
                filter &= Builders<Product>.Filter.Eq(x => x.ItemId, ParseStringToLong(itemId));
            }
            return await DbSet.Find(filter).FirstOrDefaultAsync();
        }

        public async Task<List<Product>> Get(long retailerId, IList<long> channelIds, IList<string> itemIds, bool? isConnected = null, bool? isStringItemId = null)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);

            if (channelIds?.Any() == true)
            {
                filter &= Builders<Product>.Filter.In(x => x.ChannelId, channelIds);
            }

            if (itemIds?.Any() == true)
            {
                if (isStringItemId.HasValue)
                {
                    if (isStringItemId == true)
                    {
                        filter &= (Builders<Product>.Filter.In(x => x.StrItemId, itemIds) | Builders<Product>.Filter.In(x => x.StrParentItemId, itemIds));
                    }
                    else
                    {
                        filter &= (Builders<Product>.Filter.In(x => x.ItemId, ParseStringToLong(itemIds.ToList())) | Builders<Product>.Filter.In(x => x.ParentItemId, ParseStringToLong(itemIds.ToList())));
                    }
                }
                else
                {
                    filter &= (Builders<Product>.Filter.In(x => x.StrItemId, itemIds) | Builders<Product>.Filter.In(x => x.StrParentItemId, itemIds) | Builders<Product>.Filter.In(x => x.ItemId, ParseStringToLong(itemIds.ToList())) | Builders<Product>.Filter.In(x => x.ParentItemId, ParseStringToLong(itemIds.ToList())));
                }
            }

            if (isConnected.HasValue)
            {
                filter &= Builders<Product>.Filter.Eq(x => x.IsConnected, isConnected.Value);
            }

            var result = await DbSet.Find(filter).ToListAsync();

            return result;
        }

        public async Task<List<Product>> Get(long retailerId, List<long> channelIds = null, string itemSku = null, bool? isConnected = null)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
            if (channelIds != null && channelIds.Any())
            {
                filter = filter & Builders<Product>.Filter.In(x => x.ChannelId, channelIds);
            }
            if (!string.IsNullOrEmpty(itemSku))
            {
                filter = filter & Builders<Product>.Filter.Regex(x => x.ItemSku, new BsonRegularExpression(itemSku, "i"));
            }
            if (isConnected.HasValue)
            {
                filter = filter & Builders<Product>.Filter.Eq(x => x.IsConnected, isConnected);
            }
            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task<List<Product>> GetProductNotConnnectBySku(long retailerId, List<long> channelIds = null, string sku = null)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
            if (channelIds != null && channelIds.Any())
            {
                filter = filter & Builders<Product>.Filter.In(x => x.ChannelId, channelIds);
            }
            if (!string.IsNullOrEmpty(sku))
            {
                filter = filter & Builders<Product>.Filter.Eq(x => x.ItemSku, sku);
            }
            filter = filter & Builders<Product>.Filter.Eq(x => x.IsConnected, false);
            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task<PagingDataSource<Product>> GetAllByListChannelId(long retailerId, List<long> channelIdLst, int top, int skip, bool isStringItemId, List<string> itemSkuLst, string channelTerm = null, List<string> itemIds = null, bool? isConnected = null, bool? isSyncSuccess = null)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<Product>.Filter.In(x => x.ChannelId, channelIdLst);
            if (isConnected.HasValue)
            {
                filter = filter & Builders<Product>.Filter.Eq(x => x.IsConnected, isConnected.GetValueOrDefault());
            }
            if (isSyncSuccess.HasValue && isSyncSuccess.Value)
            {
                filter = filter & (Builders<Product>.Filter.Eq(x => x.SyncErrors, null) | Builders<Product>.Filter.Size(x => x.SyncErrors, 0));
            }
            else if (isSyncSuccess.HasValue && !isSyncSuccess.Value)
            {
                filter = filter & Builders<Product>.Filter.Ne(x => x.SyncErrors, null) & Builders<Product>.Filter.SizeGt(x => x.SyncErrors, 0);
            }
            if (itemIds != null && itemIds.Count > 0)
            {
                if (SelfAppConfig.IsStringItemId || isStringItemId)
                {
                    filter = filter & Builders<Product>.Filter.In(x => x.StrItemId, itemIds);
                }
                else
                {
                    filter = filter & Builders<Product>.Filter.In(x => x.ItemId, ParseStringToLong(itemIds));
                }
            }

            if (itemSkuLst != null && itemSkuLst.Count > 0)
            {
                filter = filter & (Builders<Product>.Filter.In(x => x.ItemId, ParseStringToLong(itemSkuLst))
                    | Builders<Product>.Filter.In(x => x.ItemId, ParseStringToLong(itemSkuLst))
                    | Builders<Product>.Filter.In(x => x.ItemSku, itemSkuLst));
            }

            if (!string.IsNullOrEmpty(channelTerm))
            {
                Regex rg = new Regex(@"(['^$.|?*+()+\[\]\\\\])");

                channelTerm = rg.Replace(channelTerm, "\\$1");
                var bsonRegexExpression = new Regex(channelTerm, RegexOptions.IgnoreCase);

                filter = filter & Builders<Product>.Filter.Regex(x => x.Keyword, new BsonRegularExpression(bsonRegexExpression));
            }
            var resultData = new PagingDataSource<Product>();
            resultData.Total = await DbSet.CountDocumentsAsync(filter);
            resultData.Data = await DbSet.Find(filter, new FindOptions { Collation = new Collation("vi") }).SortBy(x => x.ItemName).ThenBy(x => x.ItemSku).Skip(skip).Limit(top).ToListAsync();
            return resultData;
        }

        public async Task<PagingDataSource<Product>> GetAllByChannelId(long retailerId, long channelId,  int top, int skip, bool isStringItemId, string channelTerm = null, List<string> itemIds = null, bool? isConnected = null, bool? isSyncSuccess = null)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<Product>.Filter.Eq(x => x.ChannelId, channelId);
            if (isConnected.HasValue)
            {
                filter = filter & Builders<Product>.Filter.Eq(x => x.IsConnected, isConnected.GetValueOrDefault());
            }

            if (isSyncSuccess.HasValue)
            {
                if (isSyncSuccess.Value == true)
                {
                    filter = filter & (Builders<Product>.Filter.Eq(x => x.SyncErrors, null) | Builders<Product>.Filter.Size(x => x.SyncErrors, 0));
                }
                else
                {
                    filter = filter & Builders<Product>.Filter.Ne(x => x.SyncErrors, null) & Builders<Product>.Filter.SizeGt(x => x.SyncErrors, 0);
                }
            }

            if (itemIds != null && itemIds.Any())
            {
                if (SelfAppConfig.IsStringItemId || isStringItemId)
                {
                    filter = filter & Builders<Product>.Filter.In(x => x.StrItemId, itemIds);
                }
                else
                {
                    filter = filter & Builders<Product>.Filter.In(x => x.ItemId, ParseStringToLong(itemIds));
                }
            }

            if (!string.IsNullOrEmpty(channelTerm))
            {
                Regex rgx = new Regex(@"(['^$.|?*+()+\[\]\\\\])");

                channelTerm = rgx.Replace(channelTerm, "\\$1");
                var bsonRegexExpression = new Regex(channelTerm, RegexOptions.IgnoreCase);

                filter = filter & Builders<Product>.Filter.Regex(x => x.Keyword, new BsonRegularExpression(bsonRegexExpression));
            }
            var result = new PagingDataSource<Product>();
            result.Total = await DbSet.CountDocumentsAsync(filter);
            result.Data = await DbSet.Find(filter, new FindOptions { Collation = new Collation("vi") }).SortBy(x => x.ItemName).ThenBy(x => x.ItemSku).Skip(skip).Limit(top).ToListAsync();
            return result;
        }

        public async Task<long> CountProductByChannelId(long retailerId, long channelId, int branchId, bool? isConnected = null)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<Product>.Filter.Eq(x => x.ChannelId, channelId);
            filter = filter & Builders<Product>.Filter.Eq(x => x.BranchId, branchId);
            if (isConnected.HasValue)
            {
                filter = filter & Builders<Product>.Filter.Eq(x => x.IsConnected, isConnected.GetValueOrDefault());
            }
            return await DbSet.CountDocumentsAsync(filter);
        }

        public async Task<List<Product>> GetByChannel(long channelId, int branchId)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.ChannelId, channelId);
            filter = filter & Builders<Product>.Filter.Eq(x => x.BranchId, branchId);
            filter = filter & Builders<Product>.Filter.Eq(x => x.IsConnected, true);
            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task<UpdateResult> UpdateProductConnect(string id, bool isConnected = true)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.Id, id);
            var update = Builders<Product>.Update.Set(x => x.IsConnected, isConnected)
                .Set(y => y.ModifiedDate, DateTime.Now);
            return await DbSet.UpdateOneAsync(filter, update);
        }

        public async Task<UpdateResult> UpdateProductsConnect(List<string> ids, bool isConnected = true)
        {
            var filter = Builders<Product>.Filter.In(x => x.Id, ids);
            var update = Builders<Product>.Update.Set(x => x.IsConnected, isConnected)
                .Set(y => y.ModifiedDate, DateTime.Now);
            return await DbSet.UpdateManyAsync(filter, update);
        }

        public async Task<UpdateResult> UpdateProductConnect(long retailerId, long channelId, string itemId, bool isConnected, bool isStringItemId = false)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<Product>.Filter.Eq(x => x.ChannelId, channelId);
            if (SelfAppConfig.IsStringItemId || isStringItemId)
            {
                filter = filter & Builders<Product>.Filter.Eq(x => x.StrItemId, itemId);
            }
            else
            {
                filter = filter & Builders<Product>.Filter.Eq(x => x.ItemId, ParseStringToLong(itemId));
            }
            var update = Builders<Product>.Update.Set(x => x.IsConnected, isConnected)
                .Set(y => y.ModifiedDate, DateTime.Now);
            return await DbSet.UpdateManyAsync(filter, update);
        }

        public async Task<List<Product>> GetBySearch(long retailerId, long channelId, string term, int? skip, int? top)
        {
            if (skip.HasValue && top.HasValue)
            {
                return await GetBySearchPaging(retailerId, channelId, term, skip.Value, top.Value);
            }
            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<Product>.Filter.Eq(x => x.ChannelId, channelId);
            if (!string.IsNullOrEmpty(term))
            {
                filter = filter & Builders<Product>.Filter.Regex(x => x.Keyword, new BsonRegularExpression(term, "i"));
            }
            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task<Product> GetByProductChannelId(long retailerId, long channelId, string productChannelid, bool isStringItemId = false)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<Product>.Filter.Eq(x => x.ChannelId, channelId);
            if (SelfAppConfig.IsStringItemId || isStringItemId)
            {
                filter = filter & Builders<Product>.Filter.Eq(x => x.StrItemId, productChannelid);
            }
            else
            {
                filter = filter & Builders<Product>.Filter.Eq(x => x.ItemId, ParseStringToLong(productChannelid));
            }

            return await DbSet.Find(filter).FirstOrDefaultAsync();
        }

        public async Task<List<Product>> GetByProductChannelIds(long retailerId, long channelId, List<string> productChannelIds, bool isStringItemId = false)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<Product>.Filter.Eq(x => x.ChannelId, channelId);
            if (SelfAppConfig.IsStringItemId || isStringItemId)
            {
                filter = filter & Builders<Product>.Filter.In(x => x.StrItemId, productChannelIds);
            }
            else
            {
                filter = filter & Builders<Product>.Filter.In(x => x.ItemId, ParseStringToLong(productChannelIds));
            }
            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task<bool> RemoveByChannelId(int retailerId, long channelId)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.ChannelId, channelId);
            if (retailerId != 0)
            {
                filter = filter & Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
            }
            var result = await DbSet.DeleteManyAsync(filter);

            return result.IsAcknowledged;
        }

        public async Task<bool> RemoveAllSyncError(long channelId, IList<string> errorTypes)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.ChannelId, channelId);

            if (errorTypes?.Any() == true)
            {
                filter &= Builders<Product>.Filter.ElemMatch(x => x.SyncErrors, x => errorTypes.Contains(x.Type));

                var products = await DbSet.Find(filter).ToListAsync();

                foreach (var product in products)
                {
                    product.SyncErrors = product.SyncErrors?.Where(p => !errorTypes.Contains(p.Type)).ToList() ?? new List<SyncError>();
                    var update = Builders<Product>.Update
                        .Set(x => x.SyncErrors, product.SyncErrors)
                        .Set(y => y.ModifiedDate, DateTime.Now);

                    await DbSet.UpdateOneAsync(Builders<Product>.Filter.Eq(x => x.Id, product.Id), update);
                }

                return true;
            }
            else
            {
                var update = Builders<Product>.Update
                    .Set(x => x.SyncErrors, new List<SyncError>())
                    .Set(y => y.ModifiedDate, DateTime.Now);
                var result = await DbSet.UpdateManyAsync(filter, update);

                return result.IsAcknowledged;
            }
        }

        public async Task<UpdateResult> UpdateBranch(long retailerId, long channelId, long branchId)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<Product>.Filter.Eq(x => x.ChannelId, channelId);

            var update = Builders<Product>.Update.Set(x => x.BranchId, branchId)
                .Set(y => y.ModifiedDate, DateTime.Now);
            return await DbSet.UpdateManyAsync(filter, update);
        }

        public async Task<List<Product>> GetProductSyncError(long retailerId, List<long> channelIds, string errorType = null,List<string> itemIds = null)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
            if (channelIds != null && channelIds.Any())
            {
                filter = filter & Builders<Product>.Filter.In(x => x.ChannelId, channelIds);
            }
            if (itemIds != null && itemIds.Any())
            {
                filter = filter & Builders<Product>.Filter.In(x => x.ItemId,ParseStringToLong(itemIds));
            }
            filter = filter & Builders<Product>.Filter.Ne(x => x.SyncErrors, null) & Builders<Product>.Filter.SizeGt(x => x.SyncErrors, 0);
            if (!string.IsNullOrEmpty(errorType))
            {
                filter &= Builders<Product>.Filter.ElemMatch(x => x.SyncErrors, x => x.Type == errorType);
            }
            var result = await DbSet.Find(filter).ToListAsync();
            return result;
        }
        public async Task<UpdateResult> UpdateProductSyncSuccess(long retailerId, long channelId, string itemId, bool isStringItemId = false)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<Product>.Filter.Eq(x => x.ChannelId, channelId);
            if (SelfAppConfig.IsStringItemId || isStringItemId)
            {
                filter = filter & Builders<Product>.Filter.Eq(x => x.StrItemId, itemId);
            }
            else
            {
                filter = filter & Builders<Product>.Filter.Eq(x => x.ItemId, ParseStringToLong(itemId));
            }
            var update = Builders<Product>.Update
                .Set(x => x.SyncErrors, new List<SyncError>())
                .Set(y => y.ModifiedDate, DateTime.Now);
            return await DbSet.UpdateManyAsync(filter, update);
        }

        public async Task<long> GetQuantityOfProductSyncError(long retailerId, List<long> channelIds)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
            if (channelIds != null && channelIds.Any())
            {
                filter = filter & Builders<Product>.Filter.In(x => x.ChannelId, channelIds);
            }
            filter = filter & Builders<Product>.Filter.Ne(x => x.SyncErrors, null) & Builders<Product>.Filter.SizeGt(x => x.SyncErrors, 0);
            return await DbSet.CountDocumentsAsync(filter);
        }

        public async Task<bool> RemoveByItemIds(long retailerId, long channelId, List<string> itemIds, bool isStringItemId = false)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
            filter &= Builders<Product>.Filter.Eq(x => x.ChannelId, channelId);
            if (SelfAppConfig.IsStringItemId || isStringItemId)
            {
                filter = filter & Builders<Product>.Filter.In(x => x.StrItemId, itemIds);
            }
            else
            {
                filter = filter & Builders<Product>.Filter.In(x => x.ItemId, ParseStringToLong(itemIds));
            }

            var result = await DbSet.DeleteManyAsync(filter);

            return result.IsAcknowledged;
        }

        public async Task<List<Product>> GetProductSameKvSku(long channelId, List<string> skus = null, bool? isConnected = null)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.ChannelId, channelId);
            filter = filter & Builders<Product>.Filter.In(x => x.ItemSku, skus);
            if (isConnected.HasValue)
            {
                filter = filter & Builders<Product>.Filter.Eq(x => x.IsConnected, isConnected);
            }
            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task UpdateProductName(long retailId, long channelId, string itemId, string itemName, List<Variations> variations)
        {
            await HandleParent();
            await HandleVariation();

            async Task HandleParent()
            {
                var parentFilter = GetCommonFilter();

                parentFilter = parentFilter & (Builders<Product>.Filter.Eq(x => x.StrItemId, itemId) | Builders<Product>.Filter.Eq(x => x.ItemId, ParseStringToLong(itemId)));
                var parent = await DbSet.Find(parentFilter).FirstOrDefaultAsync();
                if (parent == null || parent.ItemName == itemName) return;

                var parentKeyWork = parent.CommonItemId + ";$#" + parent.ItemSku + ";$#" + itemName + ";$#" + parent.CommonParentItemId;
                var parentUpdate = Builders<Product>.Update.Set(x => x.ItemName, itemName).Set(x => x.Keyword, parentKeyWork);
                await DbSet.UpdateOneAsync(Builders<Product>.Filter.Eq(x => x.Id, parent.Id), parentUpdate);
            }

            async Task HandleVariation()
            {
                if (variations?.Any() != true) return;

                var variationFilter = GetCommonFilter();

                variationFilter = variationFilter & (Builders<Product>.Filter.Eq(x => x.StrParentItemId, itemId) | Builders<Product>.Filter.Eq(x => x.ParentItemId, ParseStringToLong(itemId)));

                var variationInDbs = await DbSet.Find(variationFilter).ToListAsync();
                if (variationInDbs?.Any() != true) return;

                foreach (var variationInDb in variationInDbs)
                {
                    var variation = variations.FirstOrDefault(x => x.VariationId == variationInDb.ItemId);
                    if (variation == null || string.IsNullOrEmpty(variation.Name)) continue;

                    var separateText = " - ";
                    var name = $"{itemName}{separateText}{string.Join(separateText, variation.Name.Split(','))}";
                    if (variationInDb.ItemName == name) continue;

                    var keyWork = variationInDb.CommonItemId + ";$#" + variationInDb.ItemSku + ";$#" + name + ";$#" + variationInDb.CommonParentItemId;
                    var update = Builders<Product>.Update.Set(x => x.ItemName, name).Set(x => x.Keyword, keyWork);
                    await DbSet.UpdateOneAsync(Builders<Product>.Filter.Eq(x => x.Id, variationInDb.Id), update);
                }
            }

            FilterDefinition<Product> GetCommonFilter()
            {
                var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailId);
                filter &= Builders<Product>.Filter.Eq(x => x.ChannelId, channelId);

                return filter;
            }
        }

        public async Task UpdateProducts(long retailerId, long channelId, byte channelType, List<ChannelClient.RequestDTO.Product> products, bool isStringItemId)
        {
            if (products?.Any() != true) return;
            var writeModelProducts = new List<WriteModel<Product>>();

            List<Product> productInDbs = null;
            if (channelType == (byte)ChannelType.Shopee)
            {
                var itemIdUpdateds = products.Where(x => string.IsNullOrEmpty(x.ItemName)).Select(x => x.ItemId).ToList();
                if (itemIdUpdateds?.Any() == true) productInDbs = await GetByProductChannelIds(retailerId, channelId, itemIdUpdateds, isStringItemId: isStringItemId);
            }

            foreach (var item in products)
            {
                var filterProd = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
                filterProd = filterProd & Builders<Product>.Filter.Eq(x => x.ChannelId, channelId);
                if (isStringItemId)
                {
                    filterProd = filterProd & Builders<Product>.Filter.Eq(x => x.StrItemId, item.ItemId);
                }
                else
                {
                    filterProd = filterProd & Builders<Product>.Filter.Eq(x => x.ItemId, ParseStringToLong(item.ItemId));
                }

                // Is Shopee and item name is empty --> set old name in db
                if (channelType == (byte)ChannelType.Shopee)
                {
                    var productInDb = productInDbs?.FirstOrDefault(x => x.ItemId == ParseStringToLong(item.ItemId));
                    if (productInDb != null && string.IsNullOrEmpty(item.ItemName)) item.ItemName = productInDb.ItemName;
                }


                var keywork = item.ItemId + ";$#" + item.ItemSku + ";$#" + item.ItemName + ";$#" + item.ParentItemId;
                var updateProduct = Builders<Product>.Update.Set(x => x.ItemName, item.ItemName)
                                                     .Set(x => x.ItemSku, item.ItemSku)
                                                     .Set(x => x.Type, item.Type)
                                                     .Set(x => x.ModifiedDate, DateTime.Now)
                                                     .Set(x => x.Status, item.Status)
                                                     .Set(x => x.Keyword, keywork)
                                                     .Set(x => x.ItemImages, item.ItemImages);
                if (isStringItemId)
                {
                    updateProduct.Set(x => x.StrParentItemId, item.ParentItemId);
                }
                else
                {
                    updateProduct.Set(x => x.StrParentItemId, item.ParentItemId);
                    updateProduct.Set(x => x.ParentItemId, ParseStringToLong(item.ParentItemId));
                }
                var upsertProduct = new UpdateOneModel<Product>(filterProd, updateProduct) { IsUpsert = false };
                writeModelProducts.Add(upsertProduct);
            }

            if (writeModelProducts != null && writeModelProducts.Any())
            {
                await DbSet.BulkWriteAsync(writeModelProducts);
            }
        }

        public async Task<List<Product>> GetByItemIds(long retailerId, long channelId, List<string> itemIds, bool isStringItemId)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
            filter &= Builders<Product>.Filter.Eq(x => x.ChannelId, channelId);
            if (SelfAppConfig.IsStringItemId || isStringItemId)
            {
                filter = filter & Builders<Product>.Filter.In(x => x.StrItemId, itemIds);
            }
            else
            {
                filter = filter & Builders<Product>.Filter.In(x => x.ItemId, ParseStringToLong(itemIds));
            }


            return await DbSet.Find(filter).ToListAsync();
        }
        public async Task<UpdateResult> UpdateFailedMessageAndStatusCode(List<string> ids, int? statusCode, string message, string syncType)
        {
            var filter = Builders<Product>.Filter.In(x => x.Id, ids);
            var update = Builders<Product>.Update
                .Set(x => x.SyncErrors, new List<SyncError>() { new SyncError() { Message = message, Type = syncType } })
                .Set(x => x.SyncStatusCode, statusCode)
                .Set(y => y.ModifiedDate, DateTime.Now);
            return await DbSet.UpdateManyAsync(filter, update);
        }

        public async Task<List<Product>> GetProductByStatusCode(int statusCode, long channelId, DateTime fromDate, DateTime toDate)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.ChannelId, channelId);
            filter = filter & Builders<Product>.Filter.Gte(x => x.ModifiedDate, fromDate) &
                     Builders<Product>.Filter.Lte(x => x.ModifiedDate, toDate);
            if (statusCode > 0)
            {
                filter = filter & Builders<Product>.Filter.Eq(x => x.SyncStatusCode, statusCode);
            }

            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task<bool> RemoveByParentItemIds(long retailerId, long channelId, List<string> parentIds)
        {
            var listParentItemId = ParseStringToLong(parentIds);
            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
            filter &= Builders<Product>.Filter.Eq(x => x.ChannelId, channelId);

            filter = filter & Builders<Product>.Filter.In(x => x.ParentItemId, listParentItemId);

            var result = await DbSet.DeleteManyAsync(filter);

            return result.IsAcknowledged;
        }

        public async Task<List<Product>> GetProductByParentIds(long retailerId, IList<long> channelIds, IList<string> parentIds, bool? isConnected = null)
        {
            if (parentIds?.Any() != true || channelIds?.Any() != true) return new List<Product>();

            var listParentItemId = ParseStringToLong(parentIds.ToList());

            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);

            filter &= Builders<Product>.Filter.In(x => x.ChannelId, channelIds);

            filter &= (Builders<Product>.Filter.In(x => x.StrItemId, parentIds) |
                           Builders<Product>.Filter.In(x => x.StrParentItemId, parentIds) |
                           Builders<Product>.Filter.In(x => x.ItemId, listParentItemId) |
                           Builders<Product>.Filter.In(x => x.ParentItemId, listParentItemId));
            if (isConnected != null)
            {
                filter = filter & Builders<Product>.Filter.Eq(x => x.IsConnected, isConnected);
            }
            var result = await DbSet.Find(filter).ToListAsync();

            return result;
        }

        public async Task<List<Product>> GetProductConnectByChannelIds(long retailerId, List<long> channelIds = null)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
            if (channelIds != null && channelIds.Any())
            {
                filter = filter & Builders<Product>.Filter.In(x => x.ChannelId, channelIds);
            }
            filter = filter & Builders<Product>.Filter.Eq(x => x.IsConnected, true);
            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task<bool> RemoveAllSyncErrorByChannelIds(List<long> channelIds, IList<string> errorTypes)
        {
            var filter = Builders<Product>.Filter.In(x => x.ChannelId, channelIds);

            if (errorTypes?.Any() == true)
            {
                filter &= Builders<Product>.Filter.ElemMatch(x => x.SyncErrors, x => errorTypes.Contains(x.Type));

                var products = await DbSet.Find(filter).ToListAsync();

                foreach (var product in products)
                {
                    product.SyncErrors = product.SyncErrors.Where(p => !errorTypes.Contains(p.Type)).ToList() ?? new List<SyncError>();
                    var update = Builders<Product>.Update
                        .Set(x => x.SyncErrors, product.SyncErrors)
                        .Set(y => y.ModifiedDate, DateTime.Now);

                    await DbSet.UpdateOneAsync(Builders<Product>.Filter.Eq(x => x.Id, product.Id), update);
                }

                return true;
            }
            else
            {
                var update = Builders<Product>.Update
                    .Set(x => x.SyncErrors, new List<SyncError>())
                    .Set(y => y.ModifiedDate, DateTime.Now);
                var result = await DbSet.UpdateManyAsync(filter, update);

                return result.IsAcknowledged;
            }
        }

        public async Task<List<Product>> GetBySearchPaging(long retailerId, long channelId, string term, int skip, int top)
        {
            var defaultLimit = 50;
            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<Product>.Filter.Eq(x => x.ChannelId, channelId);
            if (!string.IsNullOrEmpty(term))
            {
                filter = filter & Builders<Product>.Filter.Regex(x => x.Keyword, new BsonRegularExpression(Regex.Escape(term), "i"));
            }
            top = top > 0 ? top : defaultLimit;
            return await DbSet.Find(filter).Skip(skip).Limit(top).ToListAsync();
        }

        public async Task<List<Product>> GetProductByTrackKey(long retailerId, long channelId, string trackKey, bool? isConnected = null, int? limit = 100)
        {
            if (string.IsNullOrEmpty(trackKey))
            {
                return new List<Product>();
            }

            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
            filter &= Builders<Product>.Filter.Eq(x => x.ChannelId, channelId);
            filter &= Builders<Product>.Filter.Eq(x => x.ItemSku, trackKey) |
                      Builders<Product>.Filter.Eq(x => x.StrItemId, trackKey) |
                      Builders<Product>.Filter.Eq(x => x.StrParentItemId, trackKey) |
                      Builders<Product>.Filter.Eq(x => x.ItemId, ParseStringToLong(trackKey, -1)) |
                      Builders<Product>.Filter.Eq(x => x.ParentItemId, ParseStringToLong(trackKey, -1));
            if (isConnected != null)
            {
                filter &= Builders<Product>.Filter.Eq(x => x.IsConnected, isConnected);
            }

            var result = await DbSet.Find(filter).ToListAsync();

            return await DbSet.Find(filter).Limit(limit).ToListAsync();
        }

        public async Task<List<Product>> GetByChanneId(int retailerId, long channelId)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<Product>.Filter.Eq(x => x.ChannelId, channelId);
           
            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task<List<Product>> GetByStrItemId(long retailerId, long channelId, List<string> itemIds)
        {
            var filter = Builders<Product>.Filter.Eq(x => x.RetailerId, retailerId);
            filter &= Builders<Product>.Filter.Eq(x => x.ChannelId, channelId);
            filter &= filter & Builders<Product>.Filter.In(x => x.ItemId, ParseStringToLong(itemIds));

            filter &= filter & Builders<Product>.Filter.In(x => x.StrItemId, itemIds);

            return await DbSet.Find(filter).ToListAsync();
        }
    }

}
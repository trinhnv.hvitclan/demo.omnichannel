﻿using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace Demo.OmniChannel.MongoService.Impls
{
    public class OrderTrackingLogTimeMongoService : MongoRepository<OrderTrackingLogTime>, IOrderTrackingLogTimeMongoService
    {
        public OrderTrackingLogTimeMongoService(IMongoDatabase database) : base(database)
        {
        }

        public async Task<bool> CheckExistedByOrderId(string orderSn, int retailerId)
        {
            var filter = Builders<OrderTrackingLogTime>.Filter.Eq(x => x.OrderSn, orderSn);
            if (retailerId > 0)
            {
                filter = filter & Builders<OrderTrackingLogTime>.Filter.Eq(x => x.RetailerId, retailerId);
            }
            var countTheDocument = await DbSet.CountDocumentsAsync(filter);
            if(countTheDocument > 0)
            {
                return true;
            }
            return false;
        }
    }
}

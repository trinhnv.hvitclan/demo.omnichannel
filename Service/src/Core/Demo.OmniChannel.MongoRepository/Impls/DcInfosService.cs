﻿using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using MongoDB.Driver;

namespace Demo.OmniChannel.MongoService.Impls
{
    public class DcInfosService : MongoRepository<DcInfos>, IDcInfosService
    {
        public DcInfosService(IMongoDatabase database) : base(database)
        {
        }
    }
}

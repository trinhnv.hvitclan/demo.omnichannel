﻿using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.MongoService.Impls
{
    public class ShopeePaymentTransactionHistoryService : MongoRepository<ShopeePaymentTransactionHistory>, IShopeePaymentTransactionHistoryService
    {
        public ShopeePaymentTransactionHistoryService(IMongoDatabase database) : base(database)
        {
        }

        public async Task<(long, long, List<ShopeePaymentTransactionHistory>)> AddOrUpdate(long retailerId, List<ShopeePaymentTransactionHistory> paymentTransactions)
        {
            paymentTransactions = paymentTransactions.Where(item => !string.IsNullOrEmpty(item.OrderSn)).ToList();
            List<ShopeePaymentTransactionHistory> result = new List<ShopeePaymentTransactionHistory>();
            var paymentTrasactionIds = paymentTransactions.Select(t => t.TransactionId).ToList();
            var existPaymentTransaction = await GetByTransactionIds(paymentTrasactionIds);

            var newPaymentTransaction = new List<ShopeePaymentTransactionHistory>();
            var bulkUpdateTransactions = new List<WriteModel<ShopeePaymentTransactionHistory>>();

            foreach (var item in paymentTransactions)
            {
                var existed = existPaymentTransaction.FirstOrDefault(t => t.TransactionId == item.TransactionId && t.RetailerId == item.RetailerId);
                if (existed != null)
                {
                    if (item.Amount == existed.Amount)
                    {
                        continue;
                    }
                    result.Add(existed);
                    var updatePayment = Builders<ShopeePaymentTransactionHistory>.Update
                        .Set(t => t.Status, item.Status)
                        .Set(t => t.WalletType, item.WalletType)
                        .Set(t => t.TransactionType, item.TransactionType)
                        .Set(t => t.Amount, item.Amount)
                        .Set(t => t.CurrentBalance, item.CurrentBalance)
                        .Set(t => t.CreateTime, item.CreateTime)
                        .Set(t => t.OrderSn, item.OrderSn)
                        .Set(t => t.RefundSn, item.RefundSn)
                        .Set(t => t.WithdrawalType, item.WithdrawalType)
                        .Set(t => t.TransactionFee, item.TransactionFee)
                        .Set(t => t.Description, item.Description)
                        .Set(t => t.BuyerName, item.BuyerName)
                        .Set(t => t.PayOrderList, item.PayOrderList)
                        .Set(t => t.WithdrawId, item.WithdrawId)
                        .Set(t => t.Reason, item.Reason)
                        .Set(t => t.RootWithdrawalId, item.RootWithdrawalId)
                        .Set(x => x.RetailerId, retailerId)
                        .Set(x => x.ModifiedDate, DateTime.Now);

                    var filterPayment =
                        Builders<ShopeePaymentTransactionHistory>.Filter.Eq(t => t.TransactionId, item.TransactionId);
                    filterPayment &= Builders<ShopeePaymentTransactionHistory>.Filter.Eq(x => x.RetailerId, item.RetailerId);

                    var upsertOne = new UpdateOneModel<ShopeePaymentTransactionHistory>(filterPayment, updatePayment) { IsUpsert = true };
                    bulkUpdateTransactions.Add(upsertOne);
                }
                else
                {
                    newPaymentTransaction.Add(item);
                }
            }

            if (newPaymentTransaction.Any())
            {
                await BatchAddAsync(newPaymentTransaction);
                result.AddRange(newPaymentTransaction);
            }

            if (bulkUpdateTransactions.Any())
            {
                await DbSet.BulkWriteAsync(bulkUpdateTransactions);
            }

            return (newPaymentTransaction.Count, bulkUpdateTransactions.Count, result);
        }

        public async Task<(long, long, List<ShopeePaymentTransactionHistory>)> AddOrUpdateWithSession(IClientSessionHandle session, long retailerId, List<ShopeePaymentTransactionHistory> paymentTransactions)
        {
            paymentTransactions = paymentTransactions.Where(item => !string.IsNullOrEmpty(item.OrderSn)).ToList();
            List<ShopeePaymentTransactionHistory> result = new List<ShopeePaymentTransactionHistory>();
            var paymentTrasactionIds = paymentTransactions.Select(t => t.TransactionId).ToList();
            var existPaymentTransaction = await GetByTransactionIds(session, paymentTrasactionIds);
            var newPaymentTransaction = new List<ShopeePaymentTransactionHistory>();
            var bulkUpdateTransactions = new List<WriteModel<ShopeePaymentTransactionHistory>>();
            foreach (var item in paymentTransactions)
            {
                var existed = existPaymentTransaction.FirstOrDefault(t => t.TransactionId == item.TransactionId && t.RetailerId == item.RetailerId);
                if (existed != null)
                {
                    if (item.Amount == existed.Amount)
                    {
                        continue;
                    }
                    result.Add(existed);
                    var updatePayment = Builders<ShopeePaymentTransactionHistory>.Update
                        .Set(t => t.Status, item.Status)
                        .Set(t => t.WalletType, item.WalletType)
                        .Set(t => t.TransactionType, item.TransactionType)
                        .Set(t => t.Amount, item.Amount)
                        .Set(t => t.CurrentBalance, item.CurrentBalance)
                        .Set(t => t.CreateTime, item.CreateTime)
                        .Set(t => t.OrderSn, item.OrderSn)
                        .Set(t => t.RefundSn, item.RefundSn)
                        .Set(t => t.WithdrawalType, item.WithdrawalType)
                        .Set(t => t.TransactionFee, item.TransactionFee)
                        .Set(t => t.Description, item.Description)
                        .Set(t => t.BuyerName, item.BuyerName)
                        .Set(t => t.PayOrderList, item.PayOrderList)
                        .Set(t => t.WithdrawId, item.WithdrawId)
                        .Set(t => t.Reason, item.Reason)
                        .Set(t => t.RootWithdrawalId, item.RootWithdrawalId)
                        .Set(x => x.RetailerId, retailerId)
                        .Set(x => x.ModifiedDate, DateTime.Now);
                    var filterPayment =
                        Builders<ShopeePaymentTransactionHistory>.Filter.Eq(t => t.TransactionId, item.TransactionId);
                    filterPayment &= Builders<ShopeePaymentTransactionHistory>.Filter.Eq(x => x.RetailerId, item.RetailerId);
                    var upsertOne = new UpdateOneModel<ShopeePaymentTransactionHistory>(filterPayment, updatePayment) { IsUpsert = true };
                    bulkUpdateTransactions.Add(upsertOne);
                }
                else
                {
                    newPaymentTransaction.Add(item);
                }
            }

            if (newPaymentTransaction.Any())
            {
                await BatchAddAsync(session, newPaymentTransaction);
                result.AddRange(newPaymentTransaction);
            }

            if (bulkUpdateTransactions.Any())
            {
                await DbSet.BulkWriteAsync(session, bulkUpdateTransactions);
            }
            return (newPaymentTransaction.Count, bulkUpdateTransactions.Count, result);
        }

        public async Task<ShopeePaymentTransactionHistory> GetByTransactionId(int transactionId)
        {
            var filter = Builders<ShopeePaymentTransactionHistory>.Filter.Eq(t => t.TransactionId, transactionId);

            return await DbSet.Find(filter).FirstOrDefaultAsync();
        }

        public async Task<List<ShopeePaymentTransactionHistory>> GetByTransactionIds(List<long> transactionIds)
        {
            var filter = Builders<ShopeePaymentTransactionHistory>.Filter.In(t => t.TransactionId, transactionIds);

            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task<List<ShopeePaymentTransactionHistory>> GetByTransactionIds(IClientSessionHandle session, List<long> transactionIds)
        {
            var filter = Builders<ShopeePaymentTransactionHistory>.Filter.In(t => t.TransactionId, transactionIds);

            return await DbSet.Find(session, filter).ToListAsync();
        }

        public async Task<List<ShopeePaymentTransactionHistory>> GetPaymentChannelInvoices(long retailerId, List<string> orderSnList)
        {
            var filterPayment = Builders<ShopeePaymentTransactionHistory>.Filter.Eq(t => t.RetailerId, retailerId);
            if (orderSnList.Any())
            {
                filterPayment = filterPayment & Builders<ShopeePaymentTransactionHistory>.Filter.In(x => x.OrderSn, orderSnList);
            }
            var resultMongo = await DbSet.Find(filterPayment).ToListAsync();
            return resultMongo;
        }

        public async Task<List<ShopeePaymentTransactionHistory>> GetPaymentChannelInvoices(IClientSessionHandle session, long retailerId, List<string> orderSnList)
        {
            var filterPayment = Builders<ShopeePaymentTransactionHistory>.Filter.Eq(t => t.RetailerId, retailerId);
            if (orderSnList.Any())
            {
                filterPayment = filterPayment & Builders<ShopeePaymentTransactionHistory>.Filter.In(x => x.OrderSn, orderSnList);
            }
            var resultMongo = await DbSet.Find(session,filterPayment).ToListAsync();
            return resultMongo;
        }
    }
}

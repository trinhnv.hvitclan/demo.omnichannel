﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.ShareKernel.Common;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Demo.OmniChannel.MongoService.Impls
{
    public class OrderMongoService : MongoRepository<Order>, IOrderMongoService
    {

        public OrderMongoService(IMongoDatabase database) : base(database)
        {
        }

        public async Task<Order> GetByOrderId(string orderId, int branchId, long channelId)
        {
            var filter = Builders<Order>.Filter.Eq(x => x.ChannelId, channelId);
            filter = filter & Builders<Order>.Filter.Eq(x => x.BranchId, branchId);
            filter = filter & Builders<Order>.Filter.Eq(x => x.OrderId, orderId);
            return await DbSet.Find(filter).FirstOrDefaultAsync();
        }

        public async Task<PagingDataSource<Order>> GetByChannelId(int retailerId, int top, int skip, string orderCode, string searchTerm, List<long> channelIds = null,
            List<int> branchIds = null, string keySearchProduct = null)
        {
            var filter = Builders<Order>.Filter.Eq(x => x.RetailerId, retailerId);
            if (channelIds != null && channelIds.Any())
            {
                filter = filter & Builders<Order>.Filter.In(x => x.ChannelId, channelIds);
            }
            if (branchIds != null && branchIds.Any())
            {
                filter = filter & Builders<Order>.Filter.In(x => x.BranchId, branchIds);
            }

            if (!string.IsNullOrEmpty(orderCode))
            {
                filter = filter & Builders<Order>.Filter.Regex(x => x.Code, new BsonRegularExpression(orderCode, "i"));
            }
            if (!string.IsNullOrEmpty(searchTerm))
            {
                filter = filter &
                    (Builders<Order>.Filter.Regex(x => x.CustomerCode, new BsonRegularExpression(searchTerm, "i"))
                                   | Builders<Order>.Filter.Regex(x => x.CustomerName, new BsonRegularExpression(searchTerm, "i"))
                                   | Builders<Order>.Filter.Regex(x => x.CustomerPhone, new BsonRegularExpression(searchTerm, "i")));
            }
            if (!string.IsNullOrEmpty(keySearchProduct))
            {
                Regex rgx = new Regex(@"(['^$.|?*+()+\[\]\\\\])");
                keySearchProduct = rgx.Replace(keySearchProduct, "\\$1");
                var bsonRegexExpression = new Regex(keySearchProduct, RegexOptions.IgnoreCase);

                filter = filter &
                    (Builders<Order>.Filter.Regex(x => x.ErrorMessage, new BsonRegularExpression(bsonRegexExpression))
                    | Builders<Order>.Filter.Regex("OrderDetails.ProductChannelId", new BsonRegularExpression(keySearchProduct, "i"))
                    | Builders<Order>.Filter.Regex("OrderDetails.ProductChannelSku", new BsonRegularExpression(keySearchProduct, "i"))
                    | Builders<Order>.Filter.Regex("OrderDetails.ProductChannelName", new BsonRegularExpression(bsonRegexExpression))
                    | Builders<Order>.Filter.Regex("OrderDetails.ProductName", new BsonRegularExpression(keySearchProduct, "i"))
                    | Builders<Order>.Filter.Regex("OrderDetails.ProductCode", new BsonRegularExpression(keySearchProduct, "i")));
            }
            var result = new PagingDataSource<Order>
            {
                Total = await DbSet.CountDocumentsAsync(filter),
                Data = await DbSet.Find(filter).SortByDescending(x => x.CreatedDate).Skip(skip).Limit(top).ToListAsync()
            };
            return result;
        }

        public async Task<Order> UpdateMessageById(string id, string errorMessage)
        {
            var filter = Builders<Order>.Filter.Eq(x => x.Id, id);
            var update = Builders<Order>.Update.Set(x => x.ErrorMessage, errorMessage);
            await DbSet.UpdateOneAsync(filter, update);
            return await DbSet.Find(filter).FirstOrDefaultAsync();
        }

        public async Task<long> GetTotalOrderError(int retailerId, List<long> channelIds)
        {
            var filterOrder = Builders<Order>.Filter.Eq(x => x.RetailerId, retailerId);
            if (channelIds != null && channelIds.Any())
            {
                filterOrder &= Builders<Order>.Filter.In(x => x.ChannelId, channelIds);
            }
            var totalOrder = await DbSet.CountDocumentsAsync(filterOrder);
            return totalOrder;
        }

        public async Task<bool> RemoveByChannelId(int retailerId, long channelId)
        {
            var filter = Builders<Order>.Filter.Eq(x => x.ChannelId, channelId);
            if (retailerId != 0)
            {
                filter &= Builders<Order>.Filter.Eq(x => x.RetailerId, retailerId);
            }
            return (await DbSet.DeleteManyAsync(filter)).IsAcknowledged;
        }

        public async Task<bool> RemoveMultiOrdersByIds(List<string> orderIds, long channelIds, int retailerIds)
        {
            var filter = Builders<Order>.Filter.Eq(x => x.RetailerId, retailerIds);
            filter = filter & Builders<Order>.Filter.Eq(x => x.ChannelId, channelIds);
            filter = filter & Builders<Order>.Filter.In(x => x.OrderId, orderIds);
            await DeleteManyAsync(filter);
            return true;
        }
        public async Task<List<Order>> GetByChannelIds(List<int> retailerIds, List<long> channelIds)
        {
            var filter = Builders<Order>.Filter.In(x => x.RetailerId, retailerIds);
            filter = filter & Builders<Order>.Filter.In(x => x.ChannelId, channelIds);

            return await DbSet.Find(filter).ToListAsync();
        }

        public async Task<List<Order>> GetByChannelId(int retailerId, long channelId, DateTime createBefore)
        {
            var filter = Builders<Order>.Filter.Eq(x => x.RetailerId, retailerId);
            filter = filter & Builders<Order>.Filter.Eq(x => x.ChannelId, channelId);
            filter = filter & Builders<Order>.Filter.Gte(x => x.CreatedDate, createBefore);

            return await DbSet.Find(filter).ToListAsync();
        }
        public async Task<List<Order>> GetOrderByErrorMessage(List<string> errorMessages, DateTime fromDate, DateTime toDate, List<int> includeRetailerIds, List<int> excludeRetailerIds)
        {
            var filter = Builders<Order>.Filter.Gte(x => x.CreatedDate, fromDate);
            filter &= Builders<Order>.Filter.Lt(x => x.CreatedDate, toDate);
            foreach (var errorMessageItem in errorMessages)
            {
                filter &= Builders<Order>.Filter.Regex(x => x.ErrorMessage, new BsonRegularExpression($"^((?!{errorMessageItem}).)*$", "i"));
            }
            if (includeRetailerIds?.Any() == true) filter = filter & Builders<Order>.Filter.In(x => x.RetailerId, includeRetailerIds);
            if (excludeRetailerIds?.Any() == true) filter = filter & Builders<Order>.Filter.Nin(x => x.RetailerId, excludeRetailerIds);

            return await DbSet.Find(filter).ToListAsync();
        }
    }
}
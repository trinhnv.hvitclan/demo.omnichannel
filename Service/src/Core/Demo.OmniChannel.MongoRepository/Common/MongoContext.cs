﻿using MongoDB.Driver;

namespace Demo.OmniChannel.MongoService.Common
{
    public interface IMongoContext
    {
        IMongoDatabase Database { get; }
    }

    public  class MongoContext : IMongoContext
    {
        public static IMongoClient Client;
        public MongoContext(MongoDbSettings connectionSetting)
        {
            if (Client == null)
            {
                Client = new MongoClient(connectionSetting.ConnectionString);
            }
            Database = Client.GetDatabase(connectionSetting.DatabaseName);
        }

        public IMongoDatabase Database { get; }
    }
}

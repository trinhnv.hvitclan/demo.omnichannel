﻿using Demo.OmniChannel.MongoDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Demo.OmniChannel.MongoService.Common
{
    public class MongoDbHelper
    {
        public static List<string> GetChannelItemIds(List<ProductMapping> mappings)
        {
            var listIds = mappings.Where(x => string.IsNullOrEmpty(x.StrProductChannelId) && x.ProductChannelId > 0).Select(x => x.ProductChannelId.ToString()).ToList();
            listIds = listIds.Union(mappings.Where(x => !string.IsNullOrEmpty(x.StrProductChannelId)).Select(x => x.StrProductChannelId.ToString())).ToList();
            return listIds;
        }
    }
}

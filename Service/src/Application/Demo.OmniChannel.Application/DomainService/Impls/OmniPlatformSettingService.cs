﻿using Demo.Audit.Model.Message;
using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.Domain.Common;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Dto;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using System;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Services.Impls
{
    public class OmniPlatformSettingService : BaseService<Domain.Model.OmniPlatformSetting>, IOmniPlatformSettingService
    {
        public readonly IOmniPlatformSettingRepository _omniPlatformSettingRepository;

        private readonly IAuditTrailInternalClient _auditTrailInternalClient;

        private readonly KvInternalContext _internalContext;

        public OmniPlatformSettingService(
            IOmniPlatformSettingRepository OmniPlatformSettingRepository,
            IAuditTrailInternalClient auditTrailInternalClient,
            KvInternalContext internalContext)
        {
            _omniPlatformSettingRepository = OmniPlatformSettingRepository;
            _auditTrailInternalClient = auditTrailInternalClient;
            _internalContext = internalContext;
        }
        public async Task<CreateUpdateSettingCustomer> CreateOrUpdateAsync(CreateUpdateSettingCustomer settingDto)
        {
            var existSettings = await _omniPlatformSettingRepository.GetByRetailerAsync(_internalContext.RetailerId);

            await _omniPlatformSettingRepository.CreateOrUpdateAsync(settingDto, _internalContext.RetailerId);

            await AddAuditLog(settingDto, existSettings);

            return settingDto;
        }

        #region private method
        private async Task AddAuditLog(CreateUpdateSettingCustomer settingDto, OmniPlatformSettingDto existSettings)
        {
            var content = RenderContent(settingDto, existSettings);

            var context = new OmniChannelCore.Api.Sdk.Common.KvInternalContext(Guid.NewGuid(),
                _internalContext.BaseUrl, _internalContext.RetailerCode, _internalContext.BranchId,
                _internalContext.RetailerId, _internalContext.UserId, _internalContext?.Group?.Id ?? 0);

            var log = new AuditTrailLog
            {
                FunctionId = (int)FunctionType.PosParameter,
                Action = (int)AuditTrailAction.Update,
                Content = content.ToString(),
                CreatedDate = DateTime.Now,
                BranchId = _internalContext.BranchId
            };
            await _auditTrailInternalClient.AddLogAsync(context, log);
        }

        private string RenderContent(CreateUpdateSettingCustomer settingDto,
            OmniPlatformSettingDto existSettings)
        {
            Enum.TryParse(settingDto.KeyUpdate, out SettingCustomer settingEnumUpdate);

            var settingItemListDto = settingDto.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public)
               .ToDictionary(prop => prop.Name, prop => prop.GetValue(settingDto, null)?.ToString())
               .ToList();

            var settingUpdate = settingItemListDto.FirstOrDefault(x => x.Key == settingEnumUpdate.ToString());

            var settingOld = existSettings.Data[settingDto.KeyUpdate];

            var content = new StringBuilder($"Cập nhật thiết lập đồng bộ thông tin khách hàng " +
                $"{EnumHelper.ToDescription(settingEnumUpdate)} : {(settingOld == "True" ? "Có" : "Không")}" +
                $" --> {(settingUpdate.Value == "True" ? "Có" : "Không")}");

            return content.ToString();
        }

        #endregion
    }
}

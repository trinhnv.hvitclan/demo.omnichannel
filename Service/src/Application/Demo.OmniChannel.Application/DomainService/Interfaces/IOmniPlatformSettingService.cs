﻿using Demo.OmniChannel.Api.ServiceModel;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Services.Interfaces
{
    public interface IOmniPlatformSettingService
    {
        Task<CreateUpdateSettingCustomer> CreateOrUpdateAsync(CreateUpdateSettingCustomer settingDto);
    }
}

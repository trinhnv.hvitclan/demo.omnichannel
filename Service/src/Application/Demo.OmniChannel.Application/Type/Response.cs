﻿
namespace Demo.OmniChannel.Application.Type
{
    public class Response<T> where T : class
    {
        public T Result { get; set; }
        public string Message { get; set; }
        public object Errors { get; set; }
    }
}

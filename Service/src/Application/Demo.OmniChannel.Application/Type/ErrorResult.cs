﻿namespace Demo.OmniChannel.Application.Type
{
    public class ErrorResult
    {
        public string Message { get; set; }
        public string Code { get; set; }
    }
}

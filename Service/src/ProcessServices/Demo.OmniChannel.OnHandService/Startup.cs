﻿using Demo.OmniChannel.Business;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.DCControl.ConfigureService;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Infrastructure.EventBus.Extentions;
using Demo.OmniChannel.Infrastructure.IoC;
using Demo.OmniChannel.MongoService.Common;
using Demo.OmniChannel.MongoService.Impls;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.OnHandService.Impls;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Services.Impls;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.Services.LogginConfiguration;
using Demo.OmniChannel.ShareKernel.Auth;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Impls;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.Messaging.Redis;
using ServiceStack.OrmLite;
using System;
using System.Linq;
using System.Reflection;

namespace Demo.OmniChannel.OnHandService
{
    public class Startup
    {
        private IConfiguration _configuration { get; }

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            #region Register ServiceStack License
            Licensing.RegisterLicense(_configuration.GetSection("servicestack:license").Value);
            #endregion

            services.AddSingleton<IAppSettings>(x => new NetCoreAppSettings(_configuration));
            #region Redis
            var redisConfig = new KvRedisConfig();
            _configuration.GetSection("redis:cache").Bind(redisConfig);
            services.AddSingleton(sc => KvRedisPoolManager.GetClientsManager(redisConfig));
            services.AddSingleton<ICacheClient, KvCacheClient>();
            services.AddSingleton<IKvLockRedis, KvLockRedis>();

            var redisMqConfig = new KvRedisConfig();
            _configuration.GetSection("redis:message").Bind(redisMqConfig);
            services.AddSingleton(redisMqConfig);
            var redisFactory = KvRedisPoolManager.GetClientsManager(redisMqConfig);
            var mqHost = new RedisMqServer(redisFactory, retryCount: 2);
            services.AddSingleton<IMessageService>(mqHost);
            services.AddSingleton<IMessageFactory>(c => new RedisMessageFactory(redisFactory));
            #endregion

            #region Register kafka
            var kafka = new Kafka();
            _configuration.GetSection("Kafka").Bind(kafka);
            KafkaClient.KafkaClient.Instance.SetKafkaProducerConfig(kafka, false);
            KafkaClient.KafkaClient.Instance.InitProducer(false);
            #endregion

            #region Register DatabaseConnectionString
            var kvSql = new KvSqlConnectString();
            _configuration.GetSection("SqlConnectStrings").Bind(kvSql);
            var connectFactory = new OrmLiteConnectionFactory(kvSql.KvChannelEntities, SqlServer2016Dialect.Provider);
            connectFactory.RegisterConnection(nameof(kvSql.KvMasterEntities), kvSql.KvMasterEntities, SqlServer2016Dialect.Provider);
            foreach (var item in kvSql.KvEntitiesDC1)
            {
                connectFactory.RegisterConnection(item.Name.ToLower(), item.Value, SqlServer2016Dialect.Provider);
            }
            connectFactory.RegisterConnection(nameof(kvSql.KvMasterEntities), kvSql.KvMasterEntities, SqlServer2016Dialect.Provider);
            OrmLiteConfig.DialectProvider.GetStringConverter().UseUnicode = true;
            services.AddSingleton(kvSql);
            services.AddSingleton<IDbConnectionFactory>(c => connectFactory);
            #endregion

            #region Register Mongo

            services.Configure<MongoDbSettings>(_configuration.GetSection("mongodb"));
            services.UsingMongoDb(_configuration);
            services.AddTransient<IProductMappingService, ProductMappingService>();
            services.AddTransient<IProductMongoService, ProductMongoService>();
            #endregion

            #region Register Channel Third Party
            services.AddSingleton(c => new ChannelClient.Impls.ChannelClient());
            #endregion

            services.AddScoped<IExecutionContextService, ExecutionContextService>();

            services.AddScoped<IAuditTrailInternalClient>(c => new AuditTrailInternalClient(c.GetService<IAppSettings>()));
            //services.AddSingleton<IHostedService, SyncOnHandService>();

            services.AddScoped<IOmniChannelAuthService, OmniChannelAuthService>();
            services.AddScoped<IOmniChannelSettingService, OmniChannelSettingService>();
            services.AddScoped<IOmniChannelPlatformService, OmniChannelPlatformService>();
            services.AddScoped<IOmniChannelWareHouseService, OmniChannelWareHouseService>();

            services.AddScoped<IOnHandBusiness, OnHandBusiness>();
            services.AddScoped<IChannelBusiness, ChannelBusiness>();
            services.AddScoped<IMappingBusiness, MappingBusiness>();
            services.AddScoped<IMicrosoftLoggingContextExtension, MicrosoftLoggingContextExtension>();

            services.AddConfigEventBus(_configuration);
            services.AddEventBus();

            //services.AddCronJob<MonitorJob>(c =>
            //{
            //    c.TimeZoneInfo = TimeZoneInfo.Local;
            //    c.CronExpression = @"* * * * *";
            //});

            if (redisMqConfig.EventMessages.Any())
            {
                var lsName = redisMqConfig.EventMessages.Where(t => t.IsActive).Select(v => v.Name);
                var types = typeof(BaseService<>).GetTypeInfo().Assembly.DefinedTypes
                    .Where(p => p.GetTypeInfo().IsAssignableFrom(p.AsType()) && p.IsClass && lsName.Contains(p.Name)).Select(p => p.AsType());
                try
                {
                    foreach (var type in types)
                    {
                        services.AddTransient(typeof(IHostedService), type);
                    }
                }
                catch (Exception e)
                {
                    Log.Logger.Error($"Register {nameof(IHostedService)} error: {e.Message}", e);
                    throw;
                }

            }

            services.AddScoped(c =>
            {
                var eventContextSvc = c.GetRequiredService<IExecutionContextService>();
                
                if (eventContextSvc.Context == null)
                {
                    return new ExecutionContext();
                }

                var context = new ExecutionContext
                {
                    Id = eventContextSvc.Context.Id,
                    RetailerCode = eventContextSvc.Context.RetailerCode,
                    BranchId = eventContextSvc.Context.BranchId,
                    User = eventContextSvc.Context.User,
                    RetailerId = eventContextSvc.Context.RetailerId,
                    GroupId = eventContextSvc.Context.GroupId,
                    Group = eventContextSvc.Context.Group
                };
                return context;
            });

            SetEncryptPassPhrase();

            services.RegisterApplicationDcControl(new ApplicationDcInputOptions
            {
                ServiceName = "OnHandService",
                AppSettings = new NetCoreAppSettings(_configuration)
            });
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime lifetime, IMessageService messageService)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            //app.UseMiddleware<RequestLogContextMiddleware>();

            lifetime.ApplicationStarted.Register(messageService.Start);
        }

        private void SetEncryptPassPhrase()
        {
            CryptoHelper.PassPhrase = _configuration.GetSection("EncryptPassPhrase").Value;

            if (string.IsNullOrWhiteSpace(CryptoHelper.PassPhrase))
            {
                throw new KvException("Setting EncryptPassPhrase invalid. Check setting file, please.");
            }
        }
    }
}
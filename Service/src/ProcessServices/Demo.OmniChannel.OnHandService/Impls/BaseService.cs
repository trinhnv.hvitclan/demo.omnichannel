﻿using Demo.Audit.Model.Message;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ChannelType = Demo.OmniChannel.ShareKernel.Common.ChannelType;
using MultiProductItem = Demo.OmniChannel.ShareKernel.KafkaMessage.MultiProductItem;

namespace Demo.OmniChannel.OnHandService.Impls
{
    public abstract class BaseService<T> : IHostedService where T : class
    {
        protected readonly ILogger Logger;
        protected IAppSettings Settings;
        protected ICacheClient CacheClient;
        protected readonly IProductMappingService ProductMappingService;
        protected readonly IMessageService MessageService;
        protected readonly IAuditTrailInternalClient _auditTrailInternalClient;
        protected readonly IProductMongoService _productMongoService;

        protected BaseService(ILogger logger)
        {
            Logger = logger;
        }
        protected BaseService(
          ILogger logger,
          ICacheClient cacheClient,
          IProductMappingService productMappingService,
          IMessageService _messageService,
          IAuditTrailInternalClient auditTrailInternalClient,
          IProductMongoService productMongoService
          )
        {
            Logger = logger;
            CacheClient = cacheClient;
            ProductMappingService = productMappingService;
            MessageService = _messageService;
            _auditTrailInternalClient = auditTrailInternalClient;
            ProductMappingService = productMappingService;
            _productMongoService = productMongoService;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Logger.LogInformation($"----------------------------------------- Start {nameof(T)} Service -----------------------------------------------");
            return Task.CompletedTask;
        }
        protected abstract Task ProcessMessage(T data);
        public Task StopAsync(CancellationToken cancellationToken)
        {
            Logger.LogInformation($"----------------------------------------- Stop {nameof(T)} Service -------------------------------------------------");
            return Task.CompletedTask;
        }
        protected int GetSyncMultiPageSize(byte channelType)
        {
            int? pageSize = 20;
            switch (channelType)
            {
                case (byte)ChannelType.Lazada:
                    {
                        pageSize = Settings?.Get<int>("SyncMultiLazadaPageSize");
                        if (pageSize == null || pageSize == 0)
                        {
                            pageSize = 20;
                        }
                        break;
                    }

                case (byte)ChannelType.Shopee:
                    {
                        pageSize = Settings?.Get<int>("SyncMultiShopeePageSize");
                        if (pageSize == null || pageSize == 0)
                        {
                            pageSize = 50;
                        }
                        break;
                    }
                case (byte)ChannelType.Tiki:
                    {
                        pageSize = Settings?.Get<int>("SyncMultiTikiPageSize");
                        if (pageSize == null || pageSize == 0)
                        {
                            pageSize = 50;
                        }
                        break;
                    }
            }
            return pageSize.Value;
        }

        protected async Task<(List<MultiProductItem>, StringBuilder)> GenerateProducts(
          SyncOnHandMessage data,
          MultiProductItem productItem,
          Domain.Model.OmniChannel channel,
          OmniChannelSettingObject omniChannelSettingObject
        )
        {
            var allocateLog = new StringBuilder();
            var listProductsAfterAllocate = new List<MultiProductItem>();
            var listProductMapping = await GetChannelProducts(data.RetailerId, data.BranchId, data.ChannelId,
                productItem.KvProductId, allocateLog, channel);
            if (listProductMapping.Any())
            {
                listProductsAfterAllocate = AllocateStock(listProductMapping, productItem, allocateLog,
                    omniChannelSettingObject);
            }

            return (listProductsAfterAllocate, allocateLog);
        }

        protected List<string> GetProductRemoveError(byte channelType)
        {
            var lstErrorRemoveMsg = new List<string>();
            switch (channelType)
            {
                case (byte)ChannelType.Lazada:
                    {
                        lstErrorRemoveMsg = Settings.Get<List<string>>("LazadaProductRemoveErrorMsg");
                        if (lstErrorRemoveMsg == null || !lstErrorRemoveMsg.Any())
                        {
                            lstErrorRemoveMsg = new List<string>() { "SELLER_SKU_NOT_FOUND" };
                        }

                        break;
                    }

                case (byte)ChannelType.Shopee:
                    {
                        lstErrorRemoveMsg = Settings.Get<List<string>>("ShopeeProductRemoveErrorMsg");
                        if (lstErrorRemoveMsg == null || !lstErrorRemoveMsg.Any())
                        {
                            lstErrorRemoveMsg = new List<string>()
                            {"You provided an invalid variation id", "Can't edit deleted/invalid items"};
                        }

                        break;
                    }
            }

            return lstErrorRemoveMsg;
        }

        private async Task<List<ProductMapping>> GetChannelProducts(long retailerId, long branchId, long channelId,
           long productId, StringBuilder allocateLog, Domain.Model.OmniChannel channel)
        {
            List<ProductMapping> dataInCache = null;
            if (CacheClient != null)
            {
                dataInCache = CacheClient.Get<List<ProductMapping>>(string.Format(
                    ShareKernel.Common.KvConstant.ListProductMappingCacheKey,
                    retailerId, channelId, productId));
            }

            if (dataInCache != null && dataInCache.Any())
            {
                return dataInCache;
            }

            var timer = System.Diagnostics.Stopwatch.StartNew();
            var retVal =
                await ProductMappingService.GetListActualNameByKvSingleProductId(retailerId, branchId, channelId,
                    productId, ConvertHelper.CheckUseStringItemId(channel?.Type));
            timer.Stop();
            var longLevel = timer.ElapsedMilliseconds / 100;
            allocateLog.Append($"Join operation takes {timer.ElapsedMilliseconds} ms >{longLevel}");

            if (retVal is null)
            {
                return new List<ProductMapping>();
            }

            CacheClient?.Set(string.Format(ShareKernel.Common.KvConstant.ListProductMappingCacheKey, retailerId, channelId, productId), retVal,
                DateTime.Now.AddDays(30));
            return retVal;
        }

        private List<MultiProductItem> AllocateStock(
          List<ProductMapping> listProductMapping,
          MultiProductItem productItem,
          StringBuilder allocateLog,
          OmniChannelSettingObject omniChannelSettingObject
        )
        {
            var listAllocate = (from pro in listProductMapping
                                orderby pro.ProductChannelName, pro.ProductChannelSku
                                select new MultiProductItem
                                {
                                    ItemId = pro.CommonProductChannelId,
                                    ItemSku = pro.ProductChannelSku,
                                    KvProductSku = pro.ProductKvSku,
                                    KvProductId = pro.ProductKvId,
                                    ParentItemId = pro.CommonParentProductChannelId,
                                    OnHand = 0,
                                    Revision = productItem.Revision,
                                    ItemType = pro.ProductChannelType,
                                    ProductName = pro.ProductKvFullName,
                                }).ToList();
            allocateLog.Append(
                $"- Allocate onHand for productId: {productItem.KvProductId} with onHand {productItem.OnHand} ");
            Distribute(listAllocate, allocateLog, productItem.OnHand, omniChannelSettingObject);
            return listAllocate;
        }
        private void Distribute(List<MultiProductItem> listProduct, StringBuilder allocateLog, double allStock, OmniChannelSettingObject omniChannelSettingObject)
        {
            var distributeMultiMapping = false;
            if (omniChannelSettingObject?.DistributeMultiMapping != null)
            {
                distributeMultiMapping = omniChannelSettingObject.DistributeMultiMapping;
            }
            var productLength = listProduct.Count;
            for (int x = 0; x < productLength; x++)
            {
                var normalStock = (int)Math.Floor(allStock / listProduct.Count);
                if (distributeMultiMapping)
                {
                    // San đều ra, chia phần dư còn lại cho các SKU đầu tiên
                    listProduct[x].OnHand = normalStock + (x >= allStock % productLength ? 0 : 1);
                }
                else
                {
                    listProduct[x].OnHand = allStock;
                }
                allocateLog.Append($"- ItemId:{listProduct[x].ItemId}, onHand:{listProduct[x].OnHand}, distributeMultiMappingSetting:{distributeMultiMapping}");
            }
        }

        protected void PushMessageRemoveProductMapping(ShareKernel.Auth.ExecutionContext context,
          string connectionStringName, SyncOnHandMessage data, Product pro)
        {
            var configMappingV2Feature = Settings?.Get<MappingV2Feature>("MappingV2Feature");
            if (configMappingV2Feature != null && configMappingV2Feature.IsValid(context.Group?.Id ?? 0, data.RetailerId))
            {
                var removeMsg = new RemoveProductMappingMessage
                {
                    RetailerId = context.RetailerId,
                    BranchId = context.BranchId,
                    ChannelIds = new List<long> { data.ChannelId },
                    ChannelProductIds = new List<string> { pro.CommonItemId },
                    ConnectStringName = connectionStringName,
                    IsDeleteKvProduct = false,
                    LogId = data.LogId,
                    MessageFrom = "SyncOnhandService"
                };
                var kafkaMessage = new KafkaMessage(configMappingV2Feature.TopicRemoveMappingRequestName,
                    $"{removeMsg.RetailerId}_{removeMsg.BranchId}_{data.ChannelId}", removeMsg);
                KafkaClient.KafkaClient.Instance.PublishMessage(kafkaMessage.TopicName, kafkaMessage.Key, JsonConvert.SerializeObject(kafkaMessage), isV2: true);
            }
            else
            {
                using (var mq = MessageService.CreateMessageProducer())
                {
                    var removeMsg = new RemoveProductMappingMessage
                    {
                        RetailerId = context.RetailerId,
                        BranchId = context.BranchId,
                        ChannelIds = new List<long> { data.ChannelId },
                        ChannelProductIds = new List<string> { pro.CommonItemId },
                        ConnectStringName = connectionStringName,
                        IsDeleteKvProduct = false,
                        LogId = data.LogId
                    };
                    mq.Publish(removeMsg);
                }
            }
        }

        /// <summary>
        /// Lưu cache theo Revision (Luồng resync product sẽ check nếu đồng bộ rồi sẽ không đồng bộ lại)
        /// </summary>
        /// <param name="listProducts"></param>
        /// <param name="channelId"></param>
        /// <param name="branchId"></param>
        protected void SaveCacheProductsSyncSuccess(List<MultiProductItem> listProducts,
            long channelId, int branchId)
        {
            foreach (var kvProduct in listProducts)
            {
                CacheClient.Set(
                    ShareKernel.Common.KvConstant.KvOnHandRevision.FormatWith(channelId, branchId,
                        kvProduct.KvProductId),
                    new ProductBranchDto()
                    {
                        KvProductId = kvProduct.KvProductId,
                        BranchId = branchId,
                        Revision = kvProduct.Revision
                    }, TimeSpan.FromDays(Settings?.Get<int>("RevisionProductBranchExpired", 1) ?? 1));
            }
        }

        protected async Task HandleAuditMessage(Domain.Model.OmniChannel channel, 
            SyncOnHandMessage data, 
            ShareKernel.Auth.ExecutionContext executionContext,
            List<string> logItemSuccess, 
            List<string> logItemFail)
        {
            var coreContext = executionContext.ConvertTo<OmniChannelCore.Api.Sdk.Common.KvInternalContext>();
            coreContext.UserId = executionContext.User?.Id ?? 0;
            var log = new AuditTrailLog
            {
                FunctionId = ShareKernel.Common.AuditTrailHelper.GetAuditTrailFunctionType(channel.Type),
                Action = (int)ShareKernel.Common.AuditTrailAction.ProductIntergate,
                CreatedDate = DateTime.Now,
                BranchId = data.BranchId
            };
            var name = ShareKernel.Common.EnumHelper.GetNameByType<ShareKernel.Common.ChannelType>(channel.Type);
            var contentLog =
                new StringBuilder(
                    $"Đồng bộ thông tin số lượng hàng hóa sẵn bán: {name} shop: {channel.Email ?? channel.Name}, Số lượng sẵn bán: = {ShareKernel.Common.EnumHelper.ToDescription((ShareKernel.Common.SyncOnHandFormula)channel.SyncOnHandFormula)} </br>");

            contentLog.Append($"{string.Join("", logItemSuccess)}{string.Join("", logItemFail)}");
            log.Content = contentLog.ToString();
            // KOL-1036
            if (coreContext.BranchId != log.BranchId)
            {
                var loggerObj = new LogObjectMicrosoftExtension(Logger, data.LogId)
                {
                    Action = "SyncOnhandCheckAuditLog",
                    ChannelType = data.ChannelType,
                    RetailerId = data.RetailerId,
                    BranchId = data.BranchId
                };
                loggerObj.LogWarning($"CoreContext: {coreContext.ToSafeJson()} | AuditLog: {log.ToSafeJson()}");
            }
            await _auditTrailInternalClient.AddLogAsync(coreContext, log);
        }

        protected async Task ProcessResponseData(Domain.Model.OmniChannel channel,
            ChannelClient.RequestDTO.MultiProductResponse multiProductResponse,
            List<string> batchItemIds, List<MultiProductItem> listProducts,
            SyncOnHandMessage data, ShareKernel.Auth.ExecutionContext context,
            string connectionStringName,
            bool isSuccess,
            string errorMessage)
        {
            var logItemSuccess = new List<string>();
            var logItemFail = new List<string>();

            var channelProducts =
                 await _productMongoService.GetByProductChannelIds(data.RetailerId, data.ChannelId,
                     batchItemIds, ConvertHelper.CheckUseStringItemId(channel?.Type));

            if (!isSuccess)
            {
                if (channelProducts != null && channelProducts.Any())
                {
                    await _productMongoService.UpdateFailedMessageAndStatusCode(channelProducts.Select(p => p.Id).ToList(), 0,
                        errorMessage, ShareKernel.Common.SyncErrorType.OnHand);
                    foreach (var channelProductItem in channelProducts)
                    {
                        var map = listProducts.FirstOrDefault(p => p.ItemId == channelProductItem.StrItemId);
                        if (map != null)
                        {
                            PushLishMessageErrorNotification(channel, map, errorMessage, data);
                        }
                    }
                }
                logItemFail.Add($"- {errorMessage}");
                await HandleAuditMessage(channel, data, context, logItemSuccess, logItemFail);
                return;
            }

            foreach (var item in multiProductResponse?.Product)
            {
                var map = listProducts.FirstOrDefault(x => x.ItemId == item.ItemId);
                var pro = channelProducts.FirstOrDefault(x => x.CommonItemId == item.ItemId);

                if (pro != null && !string.IsNullOrEmpty(item.ErrorMessage))
                {
                    List<string> lstErrorRemoveMsg = GetProductRemoveError(data.ChannelType);
                    lstErrorRemoveMsg = lstErrorRemoveMsg.Select(p => p.Trim().ToLower()).ToList();
                    if (lstErrorRemoveMsg.Contains(item.ErrorMessage.Trim().ToLower()))
                    {

                        await _productMongoService.RemoveAsync(pro.Id);
                        PushMessageRemoveProductMapping(context, connectionStringName, data, pro);
                    }
                    else
                    {
                        var err = pro.SyncErrors.FirstOrDefault(x => x.Type == ShareKernel.Common.SyncErrorType.OnHand);
                        if (err != null)
                        {
                            err.Message = StringHelper.ReplaceHexadecimalSymbols(item.ErrorMessage);
                        }
                        else
                        {
                            pro.SyncErrors.Add(new MongoDb.SyncError
                            {
                                Type = ShareKernel.Common.SyncErrorType.OnHand,
                                Message = StringHelper.ReplaceHexadecimalSymbols(item.ErrorMessage)
                            });
                        }

                        await _productMongoService.UpdateAsync(pro.Id, pro);
                        
                    }
                    PushLishMessageErrorNotification(channel, map, item.ErrorMessage, data);
                    logItemFail.Add(
                        $"- [ProductCode]{map?.KvProductSku}[/ProductCode] KHÔNG thành công: {item.ErrorMessage} <br/>");
                }
                else if (pro != null)
                {
                    pro.SyncErrors.RemoveAll(x => x.Type == ShareKernel.Common.SyncErrorType.OnHand);
                    await _productMongoService.UpdateAsync(pro.Id, pro);
                    logItemSuccess.Add(
                        $"- [ProductCode]{map?.KvProductSku}[/ProductCode] thành công: SL sẵn bán: {item.OnHand} <br/>");


                }
            }

            SaveCacheProductsSyncSuccess(listProducts, data.ChannelId, data.BranchId);

            await HandleAuditMessage(channel, data, context, logItemSuccess, logItemFail);
        }

        protected virtual void PushLishMessageErrorNotification(Domain.Model.OmniChannel channel, MultiProductItem item, string errorMessage, SyncOnHandMessage data)
        {

        }
    }
}
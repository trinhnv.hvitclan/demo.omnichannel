﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Microsoft.Extensions.Logging;
using ServiceStack.Caching;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.OnHandService.Impls
{
    public class SendoCurrentProductBranchService : BaseCurrentProductBranchService<SendoCurrentProductBranchMessage>
    {
        public SendoCurrentProductBranchService(ICacheClient cacheClient,
            IMessageService mqService,
            KvRedisConfig mqRedisConfig,
            IOnHandBusiness onHandBusiness,
            IMappingBusiness mappingBusiness,
            ILogger<LazadaCurrentProductBranchService> logger) : base(cacheClient,
            mqService,
            mqRedisConfig,
            onHandBusiness,
            mappingBusiness,
            logger)
        {
        }
    }
}
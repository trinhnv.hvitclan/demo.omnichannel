﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.OnHandService.Impls
{
    public class ShopeeSyncOnHandService : SyncOnHandService<ShopeeSyncOnHandMessage>
    {
        public ShopeeSyncOnHandService(IProductMongoService productMongoService,
            IProductMappingService productMappingService,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IAppSettings settings,
            IDbConnectionFactory dbConnectionFactory,
            IAuditTrailInternalClient auditTrailInternalClient,
            IMessageService mqService,
            KvRedisConfig mqRedisConfig,
            IOmniChannelAuthService channelAuthService,
            IOnHandBusiness onHandBusiness,
            IChannelBusiness channelBusiness,
            IOmniChannelPlatformService omniChannelPlatformService,
            IOmniChannelSettingService omniChannelSettingService,
            ILogger<ShopeeSyncOnHandService> logger) : base(productMongoService,
            productMappingService,
            channelClient,
            cacheClient,
            settings,
            dbConnectionFactory,
            auditTrailInternalClient,
            mqService,
            mqRedisConfig,
            channelAuthService,
            onHandBusiness,
            channelBusiness,
            omniChannelPlatformService,
            omniChannelSettingService,
            logger)
        {
            ChannelType = ChannelType.Shopee;
        }
    }
}
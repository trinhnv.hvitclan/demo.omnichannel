﻿using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Microsoft.Extensions.Logging;
using ServiceStack.Configuration;
using ServiceStack.Messaging;
using System.Linq;
using Demo.OmniChannel.Utilities;
using System;
using System.Threading.Tasks;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ChannelClient.Models;
using System.Collections.Generic;
using System.Text;
using ServiceStack;
using MultiProductItem = Demo.OmniChannel.ShareKernel.KafkaMessage.MultiProductItem;
using Platform = Demo.OmniChannel.ChannelClient.Models.Platform;
using Demo.OmniChannel.ChannelClient.Common;
using Newtonsoft.Json;
using Demo.OmniChannel.ChannelClient.RequestDTO;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.ShareKernel.Common;
using ServiceStack.Caching;
using Demo.OmniChannel.Infrastructure.Common;
using ServiceStack.Data;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Demo.OmniChannel.ShareKernel.Auth;

namespace Demo.OmniChannel.OnHandService.Impls
{
    public class ShopeeSyncOnHandServiceV2 : BaseService<ShopeeSyncOnHandMessageV2>
    {
        private readonly ICacheClient _cacheClient;
        private readonly IChannelBusiness _channelBusiness;
        private readonly IOmniChannelAuthService _channelAuthService;
        private readonly IOmniChannelPlatformService _omniChannelPlatformService;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly IOmniChannelSettingService _omniChannelSettingService;

        public ShopeeSyncOnHandServiceV2(
         ICacheClient cacheClient,
         ILogger<ShopeeSyncOnHandServiceV2> logger,
         KvRedisConfig mqRedisConfig,
         IMessageService messageService,
         IAppSettings settings,
         IChannelBusiness channelBusiness,
         IOmniChannelAuthService channelAuthService,
         IOmniChannelPlatformService omniChannelPlatformService,
         IProductMongoService productMongoService,
         IProductMappingService productMappingService,
         IAuditTrailInternalClient auditTrailInternalClient,
         IDbConnectionFactory dbConnectionFactory,
         IOmniChannelSettingService omniChannelSettingService
        ) : base(logger, cacheClient, productMappingService, messageService,
            auditTrailInternalClient, productMongoService)
        {
            var threadSize = mqRedisConfig?.EventMessages.FirstOrDefault(x => !string.IsNullOrEmpty(x.Name) &&
                                                                         x.Name.Contains(GetType()
                                                                             .Name))?.ThreadSize ?? 10;
            messageService.RegisterHandler<ShopeeSyncOnHandMessageV2>(m =>
            {
                try
                {
                    m.Options = (int)MessageOption.None;
                    var task = Task.Run(async () => await ProcessMessage(m.GetBody()));
                    task.GetAwaiter().GetResult();
                    return Task.CompletedTask;
                }
                catch (Exception e)
                {
                    ExceptionHelper.WriteLogExceptionMq(typeof(ShopeeSyncOnHandMessageV2), m.Body, e);
                    return string.Empty;
                }
            }, threadSize);

            Settings = settings;
            _cacheClient = cacheClient;
            _channelBusiness = channelBusiness;
            _channelAuthService = channelAuthService;
            _omniChannelPlatformService = omniChannelPlatformService;
            _dbConnectionFactory = dbConnectionFactory;
            _omniChannelSettingService = omniChannelSettingService;
        }

        protected override async Task ProcessMessage(ShopeeSyncOnHandMessageV2 data)
        {
            var syncOnHandLog = new LogObjectMicrosoftExtension(Logger, data.LogId)
            {
                Action = "ShopeeMultiSyncOnHandV2",
                RetailerCode = data.RetailerCode,
                RetailerId = data.RetailerId,
                BranchId = data.BranchId,
                OmniChannelId = data.ChannelId,
                RequestObject =
               $"Id: {data.ItemId} Sku:{data.ItemSku} OnHand:{data.OnHand} MultiProduct:{data.MultiProducts.ToSafeJson()}",
                ChannelType = data.ChannelType,
                ChannelTypeCode = ((ChannelTypeEnum)data.ChannelType).ToString()
            };

            var channel = await _channelBusiness.GetChannel(data.ChannelId, data.RetailerId, data.LogId);
            if (channel == null)
            {
                syncOnHandLog.LogError(new Exception("Channel is null"));
                return;
            }
            if (data.OnHand < 0) data.OnHand = 0;

            var auth = await _channelAuthService.GetChannelAuth(channel, data.LogId);
            var platform = await _omniChannelPlatformService.GetById(channel.PlatformId);
            if (auth == null)
            {
                syncOnHandLog.LogError(new Exception("Token is Expired"));
                return;
            }

            var authKey = new ChannelAuth
            {
                AccessToken = auth.AccessToken,
                RefreshToken = auth.RefreshToken,
                ShopId = auth.ShopId
            };
            var response = await SyncOnHand(data, authKey, channel, platform, syncOnHandLog);

            syncOnHandLog.ResponseObject = response;
            syncOnHandLog.LogInfo();
        }

        #region Private method
        private async Task<string> SyncOnHand(SyncOnHandMessage data, ChannelAuth authKey,
           Domain.Model.OmniChannel channel, Platform platform, LogObjectMicrosoftExtension log)
        {
            var allocateLog = new StringBuilder();
            var products = new List<MultiProductItem>();

            var groupKvProductId = data.MultiProducts.GroupBy(x => x.KvProductId).Select(x => new MultiProductItem
            {
                KvProductId = x.Key,
                OnHand = x.First().OnHand,
                Revision = x.First().Revision
            });
            var settings = await _omniChannelSettingService.GetChannelSettings(data.ChannelId, data.RetailerId);

            foreach (var productItem in groupKvProductId)
            {
                var (productsMappings, stringBuilder) = await GenerateProducts(data, productItem, channel, settings);
                products.AddRange(productsMappings);
                allocateLog.Append(stringBuilder.ToString());
            }

            var parrent = products.GroupBy(x => x.ParentItemId).Select(x => new { Parrent = x.Key, Child = x.ToList() }).ToList();

            foreach (var item in parrent)
            {
                await SyncMultiOnHand(data, item.Parrent, item.Child, authKey, channel, log, platform);
            }
            return allocateLog.ToString();
        }

        private async Task SyncMultiOnHand(SyncOnHandMessage data,
          string parrentItemId,
          List<MultiProductItem> listProducts,
          ChannelAuth authKey,
          Domain.Model.OmniChannel channel,
          LogObjectMicrosoftExtension syncOnHandLog,
          Platform platform)
        {
            var client = new ChannelClient.Impls.ShopeeClientV2(Settings);
            var connectionStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);
            var context = await ContextHelper.GetExecutionContext(_cacheClient,
                              _dbConnectionFactory, data.RetailerId, connectionStringName, data.BranchId,
                              Settings?.Get<int>("ExecutionContext"));

            var productsWithGroupParrent = listProducts.ConvertAll(x => x.ConvertTo<ChannelClient.RequestDTO.MultiProductItem>());

            var (isSuccess, message, errorMessage) =
                await client.SyncOnHandWithGroupParrent(authKey, parrentItemId, productsWithGroupParrent, syncOnHandLog, platform);

            var multiProductResponse = !string.IsNullOrEmpty(message)
                               ? JsonConvert.DeserializeObject<MultiProductResponse>(message)
                               : new MultiProductResponse();

            await ProcessResponseData(channel, multiProductResponse,
               productsWithGroupParrent.Select(x => x.ItemId).ToList(),
               listProducts, data, context, connectionStringName, isSuccess, errorMessage);
        }
        #endregion
    }
}
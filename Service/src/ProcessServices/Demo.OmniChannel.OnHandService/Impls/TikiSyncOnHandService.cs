﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.OnHandService.Impls
{
    public class TikiSyncOnHandService : SyncOnHandService<TikiSyncOnHandMessage>
    {
        private readonly IIntegrationEventService _integrationEventService;
        public TikiSyncOnHandService(
            IIntegrationEventService integrationEventService,
            IProductMongoService productMongoService,
            IProductMappingService productMappingService,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IAppSettings settings,
            IDbConnectionFactory dbConnectionFactory,
            IAuditTrailInternalClient auditTrailInternalClient,
            IMessageService mqService,
            KvRedisConfig mqRedisConfig,
            IOmniChannelAuthService channelAuthService,
            IOnHandBusiness onHandBusiness,
            IChannelBusiness channelBusiness,
            IOmniChannelPlatformService omniChannelPlatformService,
            IOmniChannelSettingService omniChannelSettingService,
            ILogger<TikiSyncOnHandService> logger) : base(productMongoService,
            productMappingService,
            channelClient,
            cacheClient,
            settings,
            dbConnectionFactory,
            auditTrailInternalClient,
            mqService,
            mqRedisConfig,
            channelAuthService,
            onHandBusiness,
            channelBusiness,
            omniChannelPlatformService,
            omniChannelSettingService,
            logger)
        {
            ChannelType = ChannelType.Tiki;
            _integrationEventService = integrationEventService;
        }

        protected override void PushLishMessageErrorNotification(Domain.Model.OmniChannel channel, MultiProductItem item, string errorMessage, SyncOnHandMessage data)
        {
            var logger = new LogObjectMicrosoftExtension(Logger, data.LogId)
            {
                Action = "PushErrorProductNotificationMesage",
                RetailerCode = data.RetailerCode,
                RetailerId = data.RetailerId,
                BranchId = data.BranchId,
                OmniChannelId = data.ChannelId,
            };
            var obj = new Infrastructure.EventBus.Event.ErrorProductNotificationMesage(data.LogId)
            {
                RetailerId = channel.RetailerId,
                BranchId = channel.BranchId,
                ChannelId = channel.Id,
                ErrorType = (int)Demo.OmniChannel.Sdk.Common.SyncTabError.Product,
                ItemSku = item.KvProductSku,
                ItemName = item.ProductName,
                ErrorMessage = errorMessage,
                Type = channel.Type
            };
            _integrationEventService.AddEventWithRoutingKeyAsync(obj, RoutingKey.MessageErrorProductNotification);
            logger.RequestObject = obj.ToJson();
            logger.LogInfo(true);
        }
    }
}
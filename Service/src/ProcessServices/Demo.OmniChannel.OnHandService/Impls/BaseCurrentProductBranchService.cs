﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.OnHandService.Impls
{
    public class BaseCurrentProductBranchService<T> : BaseService<T> where T : CurrentProductBranchMessage
    {
        private readonly ICacheClient _cacheClient;
        private readonly IOnHandBusiness _onHandBusiness;
        private readonly IMappingBusiness _mappingBusiness;
        private readonly ILogger _logger;
        public BaseCurrentProductBranchService(
            ICacheClient cacheClient,
            IMessageService mqService,
            KvRedisConfig mqRedisConfig,
            IOnHandBusiness onHandBusiness,
            IMappingBusiness mappingBusiness,
            ILogger logger
            ) : base(logger)
        {
            var threadSize = mqRedisConfig?.EventMessages.FirstOrDefault(x => !string.IsNullOrEmpty(x.Name) &&
                                                                              x.Name.Contains(this.GetType()
                                                                                  .Name))?.ThreadSize ?? 10;
            mqService.RegisterHandler<T>(m =>
            {
                try
                {
                    m.Options = (int)MessageOption.None;
                    var task = Task.Run(async () => await ProcessMessage(m.GetBody()));
                    task.Wait();
                    return Task.CompletedTask;
                }
                catch (Exception e)
                {
                    ExceptionHelper.WriteLogExceptionMq(typeof(T), m.Body, e);
                    return string.Empty;
                }
            }, threadSize);
            _cacheClient = cacheClient;
            _onHandBusiness = onHandBusiness;
            _mappingBusiness = mappingBusiness;
            _logger = logger;
        }

        protected override async Task ProcessMessage(T data)
        {
            var logMicrosoftExtension = new LogObjectMicrosoftExtension(_logger, data.LogId)
            {
                Action = $"{nameof(BaseCurrentProductBranchService<T>)}",
                OmniChannelId = data.ChannelId,
                RetailerId = data.RetailerId,
                BranchId = data.BranchId,
                ChannelType = data.ChannelType,
                ChannelTypeCode = ((ChannelTypeEnum)data.ChannelType).ToString()
            };

            try
            {
                if (data.Products == null || !data.Products.Any())
                {
                    logMicrosoftExtension.LogInfo();
                    return;
                }

                var kvProductIds = data.Products.Select(p => p.KvProductId).ToList();
                var productMaps = await _mappingBusiness.GetMappingsByProductIds(data, kvProductIds);
                if (productMaps == null || !productMaps.Any())
                {
                    logMicrosoftExtension.LogInfo();
                    return;
                }

                var channel = new Domain.Model.OmniChannel
                {
                    Id = data.ChannelId,
                    Name = data.ChannelName,
                    Type = data.ChannelType,
                    RetailerId = data.RetailerId,
                    BranchId = data.BranchId,
                    PriceBookId = data.PriceBookId,
                    BasePriceBookId = data.BasePriceBookId,
                    SyncOnHandFormula = data.SyncOnHandFormula
                };
                var mapIds = productMaps.Select(p => p.ProductKvId).ToList();
                var kvProductWithMaps = data.Products.Where(p => mapIds.Contains(p.KvProductId)).ToList();
                var lstRedisKey = mapIds
                    .Select(p => KvConstant.KvOnHandRevision.FormatWith(data.ChannelId, data.BranchId, p)).ToList();
                var currentSyncOnHands = ((IKvCacheClient)_cacheClient).GetValues<ProductBranchDto>(lstRedisKey);
                var reSyncOnhands = new List<ProductMapping>();
                foreach (var item in kvProductWithMaps)
                {
                    var currentProduct = currentSyncOnHands.FirstOrDefault(x => x.KvProductId == item.KvProductId);
                    var currentMapping = productMaps.FirstOrDefault(x => x.ProductKvId == item.KvProductId);
                    if (currentMapping == null)
                    {
                        continue;
                    }

                    if (currentProduct == null || currentProduct.BranchId != data.BranchId)
                    {
                        reSyncOnhands.Add(currentMapping);
                        continue;
                    }

                    if (System.Collections.StructuralComparisons.StructuralComparer.Compare(item.Revision,
                            currentProduct.Revision) > 0)
                    {
                        reSyncOnhands.Add(currentMapping);
                    }
                }

                if (!reSyncOnhands.Any())
                {
                    return;
                }

                await _onHandBusiness.SyncMultiOnHand(data.ConnectStringName,
                    channel,
                    reSyncOnhands.Select(p => p.CommonProductChannelId).ToList(),
                    reSyncOnhands.Select(p => p.ProductKvId).ToList(),
                    false,
                    Guid.NewGuid(),
                    null,
                    true,
                    GetSyncMultiPageSize(data.ChannelType),
                    reSyncOnhands,
                    false,
                    logMicrosoftExtension);

                logMicrosoftExtension.TotalItem = reSyncOnhands.Count;
                logMicrosoftExtension.LogInfo(true);
            }
            catch (Exception ex)
            {
                logMicrosoftExtension.LogError(ex);
                throw;
            }
        }
    }
}
﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.Services.LogginConfiguration;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.OnHandService.Impls
{
    public class LazadaSyncOnHandService : SyncOnHandService<LazadaSyncOnHandMessage>
    {
        public LazadaSyncOnHandService(IProductMongoService productMongoService,
            IProductMappingService productMappingService,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IAppSettings settings,
            IDbConnectionFactory dbConnectionFactory,
            IAuditTrailInternalClient auditTrailInternalClient,
            IMessageService mqService,
            KvRedisConfig mqRedisConfig,
            IOmniChannelAuthService channelAuthService,
            IOnHandBusiness onHandBusiness,
            IChannelBusiness channelBusiness,
            ILogger<LazadaSyncOnHandService> logger,
            IOmniChannelSettingService omniChannelSettingService,
            IOmniChannelPlatformService omniChannelPlatformService) : base(productMongoService,
            productMappingService,
            channelClient,
            cacheClient,
            settings,
            dbConnectionFactory,
            auditTrailInternalClient,
            mqService,
            mqRedisConfig,
            channelAuthService,
            onHandBusiness,
            channelBusiness,
            omniChannelPlatformService,
            omniChannelSettingService,
            logger
            )
        {
            ChannelType = ChannelType.Lazada;
        }
    }
}
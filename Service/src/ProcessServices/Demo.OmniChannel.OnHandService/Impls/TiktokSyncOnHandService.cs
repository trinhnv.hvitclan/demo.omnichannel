﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.RequestDTO;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MultiProductItem = Demo.OmniChannel.ShareKernel.KafkaMessage.MultiProductItem;
using Platform = Demo.OmniChannel.ChannelClient.Models.Platform;

namespace Demo.OmniChannel.OnHandService.Impls
{
    public class TiktokSyncOnHandService : BaseService<TiktokSyncOnHandMessage>
    {
        private readonly ICacheClient _cacheClient;
        private readonly IChannelBusiness _channelBusiness;
        private readonly IOmniChannelAuthService _channelAuthService;
        private readonly IOmniChannelPlatformService _omniChannelPlatformService;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly IOmniChannelSettingService _omniChannelSettingService;
        private readonly IIntegrationEventService _integrationEventService;

        public TiktokSyncOnHandService(
         ICacheClient cacheClient,
         ILogger<TiktokSyncOnHandService> logger,
         KvRedisConfig mqRedisConfig,
         IMessageService messageService,
         IAppSettings settings,
         IChannelBusiness channelBusiness,
         IOmniChannelAuthService channelAuthService,
         IOmniChannelPlatformService omniChannelPlatformService,
         IProductMongoService productMongoService,
         IProductMappingService productMappingService,
         IDbConnectionFactory dbConnectionFactory,
         IAuditTrailInternalClient auditTrailInternalClient,
         IOmniChannelSettingService omniChannelSettingService,
         IIntegrationEventService integrationEventService
        ) : base(logger, cacheClient, productMappingService, messageService,
            auditTrailInternalClient, productMongoService)
        {
            var threadSize = mqRedisConfig?.EventMessages.FirstOrDefault(x => !string.IsNullOrEmpty(x.Name) &&
                                                                         x.Name.Contains(GetType()
                                                                             .Name))?.ThreadSize ?? 10;
            messageService.RegisterHandler<TiktokSyncOnHandMessage>(m =>
            {
                try
                {
                    m.Options = (int)MessageOption.None;
                    var task = Task.Run(async () => await ProcessMessage(m.GetBody()));
                    task.GetAwaiter().GetResult();
                    return Task.CompletedTask;
                }
                catch (Exception e)
                {
                    ExceptionHelper.WriteLogExceptionMq(typeof(TiktokSyncOnHandMessage), m.Body, e);
                    return string.Empty;
                }
            }, threadSize);

            Settings = settings;
            _cacheClient = cacheClient;
            _channelBusiness = channelBusiness;
            _channelAuthService = channelAuthService;
            _omniChannelPlatformService = omniChannelPlatformService;
            _dbConnectionFactory = dbConnectionFactory;
            _omniChannelSettingService = omniChannelSettingService;
            _integrationEventService = integrationEventService;
        }

        protected override async Task ProcessMessage(TiktokSyncOnHandMessage data)
        {
            var syncOnHandLog = new LogObjectMicrosoftExtension(Logger, data.LogId)
            {
                Action = "TiktokMultiSyncOnHand",
                RetailerCode = data.RetailerCode,
                RetailerId = data.RetailerId,
                BranchId = data.BranchId,
                OmniChannelId = data.ChannelId,
                RequestObject =
              $"Id: {data.ItemId} Sku:{data.ItemSku} OnHand:{data.OnHand} MultiProduct:{data.MultiProducts.ToSafeJson()}",
                ChannelType = data.ChannelType,
                ChannelTypeCode = ((ChannelTypeEnum)data.ChannelType).ToString()
            };

            var channel = await _channelBusiness.GetChannel(data.ChannelId, data.RetailerId, data.LogId);
            if (channel == null)
            {
                syncOnHandLog.LogError(new Exception("Channel is null"));
                return;
            }
            if (data.OnHand < 0) data.OnHand = 0;

            var auth = await _channelAuthService.GetChannelAuth(channel, data.LogId);
            var platform = await _omniChannelPlatformService.GetById(channel.PlatformId);
            if (auth == null)
            {
                syncOnHandLog.LogError(new Exception("Token is Expired"));
                return;
            }

            var authKey = new ChannelAuth
            {
                AccessToken = auth.AccessToken,
                RefreshToken = auth.RefreshToken,
                ShopId = auth.ShopId
            };
            var response = await SyncOnHand(data, authKey, channel, platform, syncOnHandLog);
            syncOnHandLog.ResponseObject = response;
            syncOnHandLog.LogInfo();
        }

        #region Private method
        private async Task<string> SyncOnHand(SyncOnHandMessage data, ChannelAuth authKey,
           Domain.Model.OmniChannel channel, Platform platform, LogObjectMicrosoftExtension log)
        {
            var allocateLog = new StringBuilder();
            var products = new List<MultiProductItem>();

            var groupKvProductId = data.MultiProducts.GroupBy(x => x.KvProductId).Select(x => new MultiProductItem
            {
                KvProductId = x.Key,
                OnHand = x.First().OnHand,
                Revision = x.First().Revision
            });
            var settings = await _omniChannelSettingService.GetChannelSettings(data.ChannelId, data.RetailerId);

            foreach (var productItem in groupKvProductId)
            {
                var (productsMappings, stringBuilder) = await GenerateProducts(data, productItem, channel, settings);
                products.AddRange(productsMappings);
                allocateLog.Append(stringBuilder.ToString());
            }

            foreach (var item in products)
            {
                await SyncMultiOnHand(data, item.ParentItemId, new List<MultiProductItem> { item }, authKey, channel, log, platform);
            }

            return allocateLog.ToString();
        }


        protected override void PushLishMessageErrorNotification(Domain.Model.OmniChannel channel, MultiProductItem item, string errorMessage, SyncOnHandMessage data)
        {
            var logger = new LogObjectMicrosoftExtension(Logger, data.LogId)
            {
                Action = "PushErrorProductNotificationMesage",
                RetailerCode = data.RetailerCode,
                RetailerId = data.RetailerId,
                BranchId = data.BranchId,
                OmniChannelId = data.ChannelId,
            };
            var obj = new Infrastructure.EventBus.Event.ErrorProductNotificationMesage(data.LogId)
            {
                RetailerId = channel.RetailerId,
                BranchId = channel.BranchId,
                ChannelId = channel.Id,
                ErrorType = (int)Sdk.Common.SyncTabError.Product,
                ItemSku = item.KvProductSku,
                ItemName = item.ProductName,
                ErrorMessage = errorMessage,
                Type = channel.Type
            };
            _integrationEventService.AddEventWithRoutingKeyAsync(obj, RoutingKey.MessageErrorProductNotification);
            logger.RequestObject = obj.ToJson();
            logger.LogInfo(true);
        }

        private async Task SyncMultiOnHand(SyncOnHandMessage data,
          string parrentItemId,
          List<MultiProductItem> listProducts,
          ChannelAuth authKey,
          Domain.Model.OmniChannel channel,
          LogObjectMicrosoftExtension syncOnHandLog,
          Platform platform)
        {
            var client = new ChannelClient.Impls.TikTokClient(Settings);
            var connectionStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);
            var context = await ContextHelper.GetExecutionContext(_cacheClient,
                              _dbConnectionFactory, data.RetailerId, connectionStringName, data.BranchId,
                              Settings?.Get<int>("ExecutionContext"));

            var productsWithGroupParrent = listProducts.ConvertAll(x => x.ConvertTo<ChannelClient.RequestDTO.MultiProductItem>());

            var (isSuccess, message, errorMessage) =
                await client.SyncOnHandWithGroupParrent(authKey, parrentItemId, productsWithGroupParrent, syncOnHandLog, platform);

            var multiProductResponse = !string.IsNullOrEmpty(message)
                               ? JsonConvert.DeserializeObject<MultiProductResponse>(message)
                               : new MultiProductResponse();

            await ProcessResponseData(channel, multiProductResponse,
              productsWithGroupParrent.Select(x => x.ItemId).ToList(),
              listProducts, data, context, connectionStringName, isSuccess, errorMessage);

            var delProductIds = multiProductResponse.Product.Where(x => SelfAppConfig.Tiktok.MsgProductDelete.Any(msg => msg == x.ErrorMessage))
             .Select(x => x.ItemId).ToList();
            if (delProductIds.Any()) await DeleteProductWithErrorMessage(channel, delProductIds, connectionStringName);
        }

        private async Task DeleteProductWithErrorMessage(Domain.Model.OmniChannel channel,
            List<string> delProductIds,
            string connectStringName
            )
        {
            await _productMongoService.RemoveByItemIds(channel.RetailerId, channel.Id, delProductIds,
                isStringItemId: ConvertHelper.CheckUseStringItemId(channel?.Type));

            var removeMsg = new RemoveProductMappingMessage
            {
                RetailerId = channel.RetailerId,
                BranchId = channel.BranchId,
                ChannelIds = new List<long> { channel.Id },
                ChannelProductIds = delProductIds,
                ConnectStringName = connectStringName,
                IsDeleteKvProduct = false,
                LogId = Guid.NewGuid(),
                MessageFrom = "TiktokSyncOnhand"
            };
            var kafkaConfig = Settings.Get<Kafka>("Kafka");
            var topicName = kafkaConfig.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.RemoveMappingRequest && x.ChannelType == channel.Type)?.Name;
            KafkaClient.KafkaClient.Instance.PublishMessage($"{topicName}",
                $"{removeMsg.RetailerId}_{removeMsg.BranchId}_{channel.Id}", JsonConvert.SerializeObject(removeMsg), isV2: true);
        }
        #endregion
    }
}
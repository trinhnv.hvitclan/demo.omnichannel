﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Microsoft.Extensions.Logging;
using ServiceStack.Caching;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.OnHandService.Impls
{
    public class TiktokCurrentProductBranchService : BaseCurrentProductBranchService<TiktokCurrentProductBranchMessage>
    {
        public TiktokCurrentProductBranchService(ICacheClient cacheClient,
            IMessageService mqService,
            KvRedisConfig mqRedisConfig,
            IOnHandBusiness onHandBusiness,
            IMappingBusiness mappingBusiness,
            ILogger<TiktokCurrentProductBranchService> logger) : base(
            cacheClient,
            mqService,
            mqRedisConfig,
            onHandBusiness,
            mappingBusiness,
            logger)
        {
        }
    }
}
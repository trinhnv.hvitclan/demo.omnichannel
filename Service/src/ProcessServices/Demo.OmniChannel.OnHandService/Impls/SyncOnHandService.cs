﻿using Demo.Audit.Model.Message;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.RequestDTO;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.Services.LogginConfiguration;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ServiceStack.LicenseUtils;
using ChannelType = Demo.OmniChannel.ShareKernel.Common.ChannelType;
using MultiProductItem = Demo.OmniChannel.ShareKernel.KafkaMessage.MultiProductItem;
using Platform = Demo.OmniChannel.ChannelClient.Models.Platform;

namespace Demo.OmniChannel.OnHandService.Impls
{
    public abstract class SyncOnHandService<T> : BaseService<T> where T : SyncOnHandMessage
    {
        private readonly ChannelClient.Impls.ChannelClient _channelClient;
        private readonly ICacheClient _cacheClient;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        protected ChannelType ChannelType;
        private readonly IOmniChannelAuthService _channelAuthService;
        private readonly IOnHandBusiness _onHandBusiness;
        private readonly IChannelBusiness _channelBusiness;
        private readonly IOmniChannelPlatformService _omniChannelPlatformService;
        private readonly IOmniChannelSettingService _omniChannelSettingService;

        public SyncOnHandService(IProductMongoService productMongoService,
            IProductMappingService productMappingService,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IAppSettings settings,
            IDbConnectionFactory dbConnectionFactory,
            IAuditTrailInternalClient auditTrailInternalClient,
            IMessageService mqService,
            KvRedisConfig mqRedisConfig,
            IOmniChannelAuthService channelAuthService,
            IOnHandBusiness onHandBusiness,
            IChannelBusiness channelBusiness,
            IOmniChannelPlatformService omniChannelPlatformService,
            IOmniChannelSettingService omniChannelSettingService,
            ILogger logger) : base(logger, cacheClient, 
                productMappingService, mqService, auditTrailInternalClient,
                productMongoService)
        {
            var threadSize = mqRedisConfig?.EventMessages.FirstOrDefault(x => !string.IsNullOrEmpty(x.Name) &&
                                                                              x.Name.Contains(this.GetType()
                                                                                  .Name))?.ThreadSize ?? 10;
            mqService.RegisterHandler<T>(m =>
            {
                try
                {
                    m.Options = (int)MessageOption.None;
                    var task = Task.Run(async () => await ProcessMessage(m.GetBody()));
                    task.Wait();
                    return Task.CompletedTask;
                }
                catch (Exception e)
                {
                    ExceptionHelper.WriteLogExceptionMq(typeof(T), m.Body, e);
                    return string.Empty;
                }
            }, threadSize);
            _channelClient = channelClient;
            _cacheClient = cacheClient;
            Settings = settings;
            _dbConnectionFactory = dbConnectionFactory;
            _channelAuthService = channelAuthService;
            _onHandBusiness = onHandBusiness;
            _channelBusiness = channelBusiness;
            _omniChannelPlatformService = omniChannelPlatformService;
            _omniChannelSettingService = omniChannelSettingService;
        }

        protected override async Task ProcessMessage(T data)
        {
            var action = data.MultiProducts != null && data.MultiProducts.Any()
                ? ExceptionType.MultiSyncOnHand
                : ExceptionType.SyncOnHand;
            var syncOnHandLog = new LogObjectMicrosoftExtension(Logger, data.LogId)
            {
                Action = action,
                RetailerCode = data.RetailerCode,
                RetailerId = data.RetailerId,
                BranchId = data.BranchId,
                OmniChannelId = data.ChannelId,
                RequestObject =
                    $"Id: {data.ItemId} Sku:{data.ItemSku} OnHand:{data.OnHand} " +
                    $"MultiProduct:{data.MultiProducts.ToSafeJson()}",
                ChannelType = data.ChannelType,
            };
            try
            {
                var channel = await _channelBusiness.GetChannel(data.ChannelId, data.RetailerId, data.LogId);
                if (channel == null) return;
                if (data.OnHand < 0) data.OnHand = 0;

                var client = _channelClient.GetClient((byte)ChannelType, Settings);
                var auth = await _channelAuthService.GetChannelAuth(channel, data.LogId);
                var platform = await _omniChannelPlatformService.GetById(channel.PlatformId);
                if (auth == null)
                {
                    syncOnHandLog.Action = ExceptionType.TokenExpired;
                    syncOnHandLog.Description = "Token is Expired";
                    syncOnHandLog.LogInfo(true);
                    return;
                }

                var authKey = new ChannelAuth
                {
                    AccessToken = auth.AccessToken,
                    RefreshToken = auth.RefreshToken,
                    ShopId = auth.ShopId
                };
                await SyncOnHand(data, client, authKey, channel, syncOnHandLog, platform);
                syncOnHandLog.LogInfo();
            }
            catch (Exception ex)
            {
                syncOnHandLog.LogError(ex);
                throw;
            }
        }

        private async Task<(double?, byte[])> GetKvProductInfo(SyncOnHandMessage data, Domain.Model.OmniChannel channel,
            DateTime? messageSentTime, LogObjectMicrosoftExtension log)
        {
            var syncOnHandLog = log.Clone(ExceptionType.MappingProduct);
            try
            {
                var connectStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);
                using var db = _dbConnectionFactory.OpenDbConnection(connectStringName.ToLower());
                var productBranchRepository = new ProductBranchRepository(db);
                var productRepository = new ProductRepository(db);
                var kvProduct = await productRepository.GetByIdAsync(data.KvProductId);
                if (kvProduct.ProductType != (byte)ProductType.Purchased)
                {
                    syncOnHandLog.Description = "Product isn't Purchased type";
                    syncOnHandLog.LogInfo();
                    return (null, null);
                }

                if (kvProduct.isDeleted.GetValueOrDefault())
                {
                    syncOnHandLog.Description = "Product is deleted";
                    syncOnHandLog.LogInfo();
                    return (null, null);
                }

                var productBranchInDb = await _onHandBusiness.GetProductBranchesWithRetry(
                    productBranchRepository, channel.RetailerId, channel.BranchId,
                    new List<long> { data.KvProductId }, messageSentTime);
                var productBranch = productBranchInDb.FirstOrDefault(x => x.BranchId == data.BranchId);
                if (productBranch == null)
                {
                    syncOnHandLog.Description = "Empty product branch --> return 0";
                    syncOnHandLog.LogInfo();
                    return (0, null);
                }

                if (productBranchInDb.Any(x => x.IsActive == false))
                {
                    syncOnHandLog.Description = "product is inactive";
                    syncOnHandLog.LogInfo();
                    return (null, null);
                }

                var onHand = SyncHelper.CalculateOnHandFormula(channel.SyncOnHandFormula, productBranch.OnHand,
                    productBranch.Reserved, productBranch.OnOrder);

                var productBranches = new List<ProductBranch>
                {
                    productBranch
                };

                var onHandAfterCalculate = new Dictionary<long, ProductBranchDto>
                {
                    {
                        data.KvProductId, new ProductBranchDto
                        {
                            OnHand = onHand
                        }
                    }
                };

                LogTraceProductBranch(channel.SyncOnHandFormula, productBranches, onHandAfterCalculate,
                    messageSentTime, this.GetType().ToString(), syncOnHandLog);

                if (onHand == null)
                {
                    syncOnHandLog.Description = "onHand less then zero";
                    syncOnHandLog.LogInfo();
                    return (null, null);
                }

                return (Math.Floor(onHand.GetValueOrDefault()),
                    productBranch.Revision);
            }
            catch (Exception ex)
            {
                syncOnHandLog.Action = "GetKvProductInfoError";
                syncOnHandLog.LogError(ex);
                throw;
            }
        }

        private void LogTraceProductBranch(byte syncOnHandFormula,
            List<ProductBranch> productBranches,
            Dictionary<long, ProductBranchDto> onhandAfterCalculate,
            DateTime? messageSentTime,
            string locationCall,
            LogObjectMicrosoftExtension log)
        {
            var onhandAfterCalculateText = new StringBuilder();
            if (onhandAfterCalculate != null)
            {
                foreach (var d in onhandAfterCalculate)
                {
                    onhandAfterCalculateText.Append($"Id= {d.Key} Value= {d.Value?.ToSafeJson()}. ");
                }
            }

            var syncOnHandLog = log.Clone(ExceptionType.SyncOnHandTraceProductBranch);
            syncOnHandLog.RequestObject = new
            {
                LocationCall = locationCall,
                MessageSentTime = messageSentTime,
                ProductBranchs = productBranches,
                SyncOnHandFormula = syncOnHandFormula,
                OnhandAfterCalculate = onhandAfterCalculateText
            };

            syncOnHandLog.LogInfo();
        }

        private async Task SyncMultiOnHand(SyncOnHandMessage data, List<MultiProductItem> listProducts,
            IBaseClient client, ChannelAuth authKey,
            Domain.Model.OmniChannel channel, LogObjectMicrosoftExtension syncOnHandLog, byte itemType,
            Platform platform)
        {
            var pageSize = GetSyncMultiPageSize(data.ChannelType);
            var totalProduct = listProducts.Count / pageSize;
            var totalPage = listProducts.Count % pageSize == 0 ? totalProduct : totalProduct + 1;
            List<string> logItemSuccess = new List<string>();
            List<string> logItemFail = new List<string>();
            var connectionStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);
            var allocateLog = syncOnHandLog.ResponseObject;
            var context = await ContextHelper.GetExecutionContext(_cacheClient,
                _dbConnectionFactory, data.RetailerId, connectionStringName, data.BranchId,
                Settings?.Get<int>("ExecutionContext"));

            var coreContext = context.ConvertTo<OmniChannelCore.Api.Sdk.Common.KvInternalContext>();
            coreContext.UserId = context.User?.Id ?? 0;
            for (var i = 0; i < totalPage; i++)
            {
                bool isSuccess;
                string message;
                var currentPage = i * pageSize;
                var batchItem = listProducts.Skip(currentPage).Take(pageSize).ToList();
                var batchItemIds = batchItem.Select(p => p.ItemId).ToList();
                (isSuccess, message) =
                    await client.SyncMultiOnHand(data.RetailerId, data.ChannelId, data.LogId, authKey,
                        batchItem.ConvertAll(x => x.ConvertTo<ChannelClient.RequestDTO.MultiProductItem>()),
                        syncOnHandLog,
                        itemType, platform);
                var multiProductResponse = !string.IsNullOrEmpty(message)
                    ? JsonConvert.DeserializeObject<MultiProductResponse>(message)
                    : new MultiProductResponse();

                if (isSuccess)
                {
                    foreach (var kvProduct in listProducts)
                    {
                        _cacheClient.Set(
                            KvConstant.KvOnHandRevision.FormatWith(data.ChannelId, data.BranchId,
                                kvProduct.KvProductId),
                            new ProductBranchDto()
                            {
                                KvProductId = kvProduct.KvProductId,
                                BranchId = data.BranchId,
                                Revision = kvProduct.Revision
                            },
                            TimeSpan.FromDays(Settings?.Get<int>("RevisionProductBranchExpired", 1) ?? 1));
                    }
                }

                var channelProducts =
                    await _productMongoService.GetByProductChannelIds(data.RetailerId, data.ChannelId,
                        batchItemIds, ConvertHelper.CheckUseStringItemId(channel?.Type));

                var (logSuccess, logFail) = await ProcessResponseData(channel,multiProductResponse, listProducts,
                    channelProducts,
                    data, syncOnHandLog, context, connectionStringName, itemType);
                logItemSuccess.AddRange(logSuccess);
                logItemFail.AddRange(logFail);

                syncOnHandLog.ResponseObject = allocateLog + ".Respone Client: " + multiProductResponse?.ToSafeJson();
                if (multiProductResponse != null) syncOnHandLog.TotalItem = multiProductResponse.Product?.Count ?? 0;

                syncOnHandLog.LogInfo(true);
            }

            #region Write Audit Logs

            if (!data.IsIgnoreAuditTrail)
            {
                var log = new AuditTrailLog
                {
                    FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channel.Type),
                    Action = (int)AuditTrailAction.ProductIntergate,
                    CreatedDate = DateTime.Now,
                    BranchId = data.BranchId
                };
                var name = EnumHelper.GetNameByType<ShareKernel.Common.ChannelType>(channel.Type);
                var contentLog =
                    new StringBuilder(
                        $"Đồng bộ thông tin số lượng hàng hóa sẵn bán: {name} shop: {channel.Email ?? channel.Name}, Số lượng sẵn bán: = {EnumHelper.ToDescription((SyncOnHandFormula)channel.SyncOnHandFormula)} </br>");

                contentLog.Append($"{string.Join("", logItemSuccess)}{string.Join("", logItemFail)}");
                log.Content = contentLog.ToString();
                // KOL-1036
                if (coreContext.BranchId != log.BranchId)
                {
                    var loggerObj = new LogObjectMicrosoftExtension(Logger, data.LogId)
                    {
                        Action = "SyncOnhandCheckAuditLog",
                        ChannelType = data.ChannelType,
                        RetailerId = data.RetailerId,
                        BranchId = data.BranchId
                    };
                    loggerObj.LogWarning($"CoreContext: {coreContext.ToSafeJson()} | AuditLog: {log.ToSafeJson()}");
                }
                await _auditTrailInternalClient.AddLogAsync(coreContext, log);
            }
            else
            {
                syncOnHandLog.RequestObject = null;
                syncOnHandLog.ResponseObject = null;
                syncOnHandLog.LogWarning("Ignore audit log - IgnoreAuditTrail");
            }

            #endregion
        }

        private async Task<(List<string>, List<string>)> ProcessResponseData(
             Domain.Model.OmniChannel channel,
            MultiProductResponse multiProductResponse,
            List<MultiProductItem> listProducts,
            List<MongoDb.Product> channelProducts,
            SyncOnHandMessage data,
            LogObjectMicrosoftExtension syncOnHandLog,
            ShareKernel.Auth.ExecutionContext context,
            string connectionStringName,
            byte itemType)
        {
            var logItemSuccess = new List<string>();
            var logItemFail = new List<string>();

            if (multiProductResponse.IsException)
            {
                foreach (var product in channelProducts)
                {
                    var map = listProducts.FirstOrDefault(p => p.ItemId == product.StrItemId);
                    if (map != null)
                    {
                        PushLishMessageErrorNotification(channel, map, multiProductResponse.ErrorMessage, data);
                    }
                    logItemFail.Add(
                        $"- [ProductCode]{product?.ItemSku}[/ProductCode] KHÔNG thành công: {multiProductResponse.ErrorMessage} <br/>");
                }
                return (logItemSuccess, logItemFail);
            }

            foreach (var item in multiProductResponse?.Product ?? new List<ChannelClient.RequestDTO.Product>())
            {
                var map = listProducts.FirstOrDefault(x => x.ItemId == item.ItemId);
                var pro = channelProducts.FirstOrDefault(x => x.CommonItemId == item.ItemId);
                if (itemType == (byte)ShareKernel.Common.ChannelProductType.Normal)
                {
                    map ??= listProducts.FirstOrDefault(x => x.ItemId == item.ParentItemId);

                    pro ??= channelProducts.FirstOrDefault(x => x.CommonItemId == item.ParentItemId);
                }

                if (pro != null && !string.IsNullOrEmpty(item.ErrorMessage))
                {
                    List<string> lstErrorRemoveMsg = GetProductRemoveError(data.ChannelType);
                    lstErrorRemoveMsg = lstErrorRemoveMsg.Select(p => p.Trim().ToLower()).ToList();
                    if (lstErrorRemoveMsg.Contains(item.ErrorMessage.Trim().ToLower()))
                    {
                        syncOnHandLog.Action = ExceptionType.RemoveMappingBySyncOnHand;
                        syncOnHandLog.RequestObject = $"ItemId: {item.ItemId} ItemSku:{item.ItemSku}";
                        syncOnHandLog.Description =
                            $"Remove mapping by OnhandService (SyncOnhand response: {item.ErrorMessage})";
                        syncOnHandLog.LogInfo();

                        await _productMongoService.RemoveAsync(pro.Id);
                        PushMessageRemoveProductMapping(context, connectionStringName, data, pro);
                    }
                    else
                    {
                        var err = pro.SyncErrors?.FirstOrDefault(x => x.Type == SyncErrorType.OnHand);
                        if (err != null)
                        {
                            err.Message = StringHelper.ReplaceHexadecimalSymbols(item.ErrorMessage);
                        }
                        else
                        {
                            pro.SyncErrors.Add(new SyncError
                            {
                                Type = SyncErrorType.OnHand,
                                Message = StringHelper.ReplaceHexadecimalSymbols(item.ErrorMessage)
                            });
                        }

                        await _productMongoService.UpdateAsync(pro.Id, pro);
                        PushLishMessageErrorNotification(channel, map, err.Message, data);
                    }

                    logItemFail.Add(
                        $"- [ProductCode]{map?.KvProductSku}[/ProductCode] KHÔNG thành công: {item.ErrorMessage} <br/>");
                }
                else if (pro != null)
                {
                    pro.SyncErrors.RemoveAll(x => x.Type == SyncErrorType.OnHand);
                    await _productMongoService.UpdateAsync(pro.Id, pro);
                    logItemSuccess.Add(
                        $"- [ProductCode]{map?.KvProductSku}[/ProductCode] thành công: SL sẵn bán: {item.OnHand} <br/>");
                }
            }

            return (logItemSuccess, logItemFail);
        }

        private async Task SyncOnHand(SyncOnHandMessage data, IBaseClient client, ChannelAuth authKey,
            Domain.Model.OmniChannel channel, LogObjectMicrosoftExtension syncOnHandLog, Platform platform)
        {
            var products = new List<MultiProductItem>();
            var allocateLog = new StringBuilder();
            if (data.MultiProducts == null)
            {
                var (kvOnHand, revision) = await GetKvProductInfo(data, channel, data.SentTime, syncOnHandLog);
                if (kvOnHand == null) return;
                data.MultiProducts = new List<MultiProductItem>
                {
                    new MultiProductItem
                    {
                        ItemId = data.ItemId,
                        ItemSku = data.ItemSku,
                        ParentItemId = data.ParentItemId,
                        KvProductId = data.KvProductId,
                        OnHand = kvOnHand.GetValueOrDefault(),
                        Revision = revision
                    }
                };
            }

            var groupKvProductId = data.MultiProducts.GroupBy(x => x.KvProductId).Select(x => new MultiProductItem
            {
                KvProductId = x.Key,
                OnHand = x.First().OnHand,
                Revision = x.First().Revision
            });
            var settings = await _omniChannelSettingService.GetChannelSettings(data.ChannelId, data.RetailerId);

            foreach (var productItem in groupKvProductId)
            {
                var (productsMappings, stringBuilder) = await GenerateProducts(data, productItem, channel, settings);
                products.AddRange(productsMappings);
                allocateLog.Append(stringBuilder.ToString());
            }

            syncOnHandLog.Action = ExceptionType.SyncOnHandUseMultiMappingSku;
            syncOnHandLog.ResponseObject = allocateLog.ToString();
            syncOnHandLog.LogInfo();
            var productsVariation = products.Where(x => x.ItemType == (byte)ShareKernel.Common.ChannelProductType.Variation).ToList();
            if (productsVariation.Any())
            {
                await SyncMultiOnHand(data, productsVariation, client, authKey, channel, syncOnHandLog,
                    (byte)ShareKernel.Common.ChannelProductType.Variation, platform);
            }
            var productsNormal = products.Where(x => x.ItemType == (byte)ShareKernel.Common.ChannelProductType.Normal).ToList();

            if (productsNormal.Any())
            {
                await SyncMultiOnHand(data, productsNormal, client, authKey, channel, syncOnHandLog,
                    (byte)ShareKernel.Common.ChannelProductType.Normal, platform);
            }
        }
      
    }
}
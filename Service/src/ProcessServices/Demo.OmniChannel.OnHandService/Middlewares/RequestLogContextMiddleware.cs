﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Demo.OmniChannel.ShareKernel.Auth;
using Microsoft.AspNetCore.Http;
using Serilog.Context;
using ServiceStack;

namespace Demo.OmniChannel.OnHandService.Middlewares
{
    public class RequestLogContextMiddleware
    {
        private readonly RequestDelegate _next;

        public RequestLogContextMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public Task Invoke(HttpContext context)
        {
            var session = HostContext.Resolve<ExecutionContext>();
            if (session.User != null)
            {
                using (LogContext.PushProperty("TenantCode", session.RetailerCode))
                using (LogContext.PushProperty("BranchId", session.BranchId))
                using (LogContext.PushProperty("RetailId", session.RetailerId))
                using (LogContext.PushProperty("UserId", session.User.Id))
                using (LogContext.PushProperty("GroupId", session.GroupId))
                using (LogContext.PushProperty("UserName", session.User.UserName))
                using (LogContext.PushProperty("IsAdmin", session.User.IsAdmin))
                {
                    return _next.Invoke(context);
                }
            }

            return _next.Invoke(context);
        }
    }
}

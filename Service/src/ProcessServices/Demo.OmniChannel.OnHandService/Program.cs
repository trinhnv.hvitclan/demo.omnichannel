﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.WindowsServices;
using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Serilog.Exceptions;
using ServiceStack.Logging.Serilog;
using ServiceStack.Logging;

namespace Demo.OmniChannel.OnHandService
{
    class Program
    {
        public static readonly string Namespace = typeof(Program).Namespace;
        public static readonly string AppName =
            Namespace.Substring(Namespace.LastIndexOf('.', Namespace.LastIndexOf('.') - 1) + 1);

        static void Main(string[] args)
        {
            var configuration = GetConfiguration();
            Log.Logger = CreateSerilogLogger(configuration);
            LogManager.LogFactory = new SerilogFactory();
            var host = CreateWebHostBuilder(configuration, args);
            try
            {
                Log.Information("OnHand task starting console...");
                host.Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "OnHand Service corrupted", AppName);
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static IConfiguration GetConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            return builder.Build();
        }

        private static ILogger CreateSerilogLogger(IConfiguration configuration)
        {
            return new LoggerConfiguration()//NOSONAR
                .Enrich.FromLogContext()
                .Enrich.WithProperty("ApplicationContext", AppName)
                .Enrich.WithExceptionDetails()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
        }

        public static IWebHostBuilder CreateWebHostBuilder(IConfiguration configuration, string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                   .ConfigureAppConfiguration(x => x.AddConfiguration(configuration))
                   .UseSerilog()
                   .UseUrls(Environment.GetEnvironmentVariable("ASPNETCORE_URLS") ?? "http://localhost:5002/")
                   .UseStartup<Startup>();
    }
}

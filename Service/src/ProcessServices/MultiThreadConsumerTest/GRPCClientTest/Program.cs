﻿// See https://aka.ms/new-console-template for more information



using Grpc.Core;
using KiotViet.OmniChannel.OrderGRPCServer;

Channel channel = new Grpc.Core.Channel("localhost:50001", ChannelCredentials.Insecure);
var client = new HealthCheck.HealthCheckClient(channel);
StatusRequest statusRequest = new StatusRequest();
var reply = await client.StatusAsync(statusRequest);
Console.WriteLine($"CreateShopeeOrderV2Async: {reply}");


Console.WriteLine("Press any key to exit...");
Console.ReadKey();

Console.WriteLine("Press any key to exit...");
Console.ReadKey();

﻿using Demo.OmniChannel.Infrastructure.EventBus.Abstractions;
using Demo.OmniChannel.Infrastructure.EventBus.Event;
using Microsoft.Extensions.Hosting;
using System.Threading;
using System.Threading.Tasks;

namespace Demo.OmniChannel.IntegrationService.Consumer
{
    public class EventBusConsumerService : BackgroundService
    {
        private readonly IEventBus _eventBus;

        public EventBusConsumerService(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _eventBus.Subscribe<TiktokSyncProductFromErrorOrder, IIntegrationEventHandler<TiktokSyncProductFromErrorOrder>>();
            return Task.CompletedTask;
        }
    }
}

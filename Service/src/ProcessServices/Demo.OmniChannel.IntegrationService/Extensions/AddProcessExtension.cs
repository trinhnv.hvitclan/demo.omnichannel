﻿using Demo.OmniChannel.IntegrationService.BackgroundProcess.Type;
using Microsoft.Extensions.DependencyInjection;

namespace Demo.OmniChannel.IntegrationService.Extensions
{
    public static class AddProcessExtension
    {
        public static void AddProcess(this IServiceCollection services)
        {
            services.AddScoped<TiktokSyncProductFromOrderProcess>();
        }
    }
}

﻿using Demo.OmniChannel.Infrastructure.EventBus.Abstractions;
using Demo.OmniChannel.Infrastructure.EventBus.Event;
using Demo.OmniChannel.IntegrationService.EventHandlers;
using Microsoft.Extensions.DependencyInjection;

namespace Demo.OmniChannel.IntegrationService.Extensions
{
    public static class AddEventHandlersExtension
    {
        public static void AddEventHandlers(this IServiceCollection services)
        {
            services.AddScoped<IIntegrationEventHandler<TiktokSyncProductFromErrorOrder>, TiktokSyncProductEventHandler>();
        }
    }
}

﻿using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Models;
using System.Collections.Generic;
using System.Linq;

namespace Demo.OmniChannel.IntegrationService.Common
{
    public static class LazadaFeeHelper
    {

        public static float ConvertFeeLazadaToRealNumber(float input)
        {
            // Phi > 0 vaf < 0
            return 0 - input;
        }

        public static bool ValidateFee(LazadaFeePaymentTransactionHistory financeTransactionByOrder, List<string> igroneFeeTypeLst)
        {
            return ValidateFee(financeTransactionByOrder.Amount, financeTransactionByOrder.FeeName, financeTransactionByOrder.FeeType, igroneFeeTypeLst);
        }

        public static bool ValidateFee(string amount, string feeName, string feeType, List<string> igroneFeeTypeLst)
        {
            if (string.IsNullOrEmpty(amount) || string.IsNullOrEmpty(feeName) || string.IsNullOrEmpty(feeType))
            {
                return false;
            }
            if (igroneFeeTypeLst.Any(item => item.Equals(feeType)))
            {
                return false;
            }
            return true;
        }

        public static FeeDetail ConvertTransactionFeeToFeeJson(LazadaFeePaymentTransactionHistory financeTransactionByOrder, List<string> ignoreList)
        {
            if (!ValidateFee(financeTransactionByOrder.Amount, financeTransactionByOrder.FeeName, financeTransactionByOrder.FeeType, ignoreList))
            {
                return new FeeDetail();
            }
            return new FeeDetail
            {
                Name = financeTransactionByOrder.FeeType,
                Value = Helpers.ToFloat(financeTransactionByOrder.Amount, (float)0)
            };
        }

        public static FeeDetail ConvertToFeeJson(LazadaFeePaymentTransactionHistory financeTransactionByOrder, List<string> ignoreList)
        {
            return ConvertTransactionFeeToFeeJson(financeTransactionByOrder, ignoreList);
        }
    }
}

﻿using Demo.OmniChannel.Infrastructure.EventBus.Abstractions;
using Demo.OmniChannel.Infrastructure.EventBus.Event;
using Demo.OmniChannel.IntegrationService.BackgroundProcess.Type;
using System.Threading.Tasks;

namespace Demo.OmniChannel.IntegrationService.EventHandlers
{
    public class TiktokSyncProductEventHandler : IIntegrationEventHandler<TiktokSyncProductFromErrorOrder>
    {
        private readonly TiktokSyncProductFromOrderProcess TiktokSyncProductFromOrderProcess;

        public TiktokSyncProductEventHandler(
            TiktokSyncProductFromOrderProcess tiktokSyncProductFromOrderProcess)
        {
            TiktokSyncProductFromOrderProcess = tiktokSyncProductFromOrderProcess;
        }
        public async Task Handle(TiktokSyncProductFromErrorOrder @event)
        {
            await TiktokSyncProductFromOrderProcess.HandleSyncProductFromOrder(@event);
        }
    }
}

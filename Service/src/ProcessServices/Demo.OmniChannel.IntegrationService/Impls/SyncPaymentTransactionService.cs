﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.Services.LogginConfiguration;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.IntegrationService.Impls
{
    public class SyncPaymentTransactionService<T> : BaseService<T> where T : SyncPaymentTransactionMessage
    {
        private readonly IShopeePaymentTransactionHistoryService _shopeePaymentTransactionHistoryService;
        private readonly IScheduleService _scheduleService;
        private readonly IOmniChannelPlatformService _omniChannelPlatformService;
        private readonly IPaymentTransactionService _paymentTransactionService;
        private readonly IOmniChannelAuthService _channelAuthService;
        private readonly IChannelBusiness _channelBusiness;
        private readonly IDbConnectionFactory _dbConnectionFactory;

        public SyncPaymentTransactionService(
            IAppSettings settings,
            ILogger logger,
            IChannelClient channelClient,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IMessageService mqService,
            KvRedisConfig mqRedisConfig,
            IShopeePaymentTransactionHistoryService shopeePaymentTransactionHistoryService,
            IScheduleService scheduleService,
            IOmniChannelPlatformService omniChannelPlatformService,
            IPaymentTransactionService paymentTransactionService,
            IOmniChannelAuthService channelAuthService,
            IChannelBusiness channelBusiness
          ) : base(
            settings,
            logger,
            channelClient,
            cacheClient,
            dbConnectionFactory,
            mqService,
            mqRedisConfig
            )
        {
            ThreadSize = mqRedisConfig?.EventMessages
                .FirstOrDefault(t => !string.IsNullOrEmpty(t.Name) && t.Name.Contains(GetType().Name))?.ThreadSize ?? 10;

            MessageService.RegisterHandler<T>(m =>
            {
                try
                {
                    m.Options = (int)MessageOption.None;
                    var task = Task.Run(async () => await ProcessMessage(m.GetBody()));
                    task.GetAwaiter().GetResult();
                    return Task.CompletedTask;
                }
                catch (Exception e)
                {
                    ExceptionHelper.WriteLogExceptionMq(typeof(T), m.Body, e);
                    return string.Empty;
                }
            }, ThreadSize);

            _shopeePaymentTransactionHistoryService = shopeePaymentTransactionHistoryService;
            _scheduleService = scheduleService;
            _omniChannelPlatformService = omniChannelPlatformService;
            _paymentTransactionService = paymentTransactionService;
            _channelAuthService = channelAuthService;
            _channelBusiness = channelBusiness;
            _dbConnectionFactory = dbConnectionFactory;
        }

        protected override async Task ProcessMessage(T data)
        {
            var loggerExtension = new LogObjectMicrosoftExtension(Logger, data.LogId)
            {
                Action = ExceptionType.SyncPaymentTransaction,
                RetailerId = data.RetailerId,
                BranchId = data.BranchId,
                OmniChannelId = data.ChannelId,
                ChannelType = data.ChannelType,
                ChannelTypeCode = ((ChannelTypeEnum)data.ChannelType).ToString(),
                GroupId = data.GroupId
            };

            try
            {
                var shopeePaymentMaxGetApiByDate = Settings.Get("ShopeePaymentMaxGetApiByDate", 14);
                var createFrom = data.LastSync ?? DateTime.UtcNow;
                var createTo = DateTime.UtcNow;
                var diffDate = createTo - createFrom;
                if (diffDate.TotalDays > shopeePaymentMaxGetApiByDate)
                {
                    createTo = createFrom.AddDays(shopeePaymentMaxGetApiByDate);
                }

                var client = ChannelClient.CreateClient(data.ChannelType, data.GroupId, data.RetailerId);

                var plaftform = await _omniChannelPlatformService.GetById(data.PlatformId);
                var channel = await _channelBusiness.GetChannel(data.ChannelId, data.RetailerId, Guid.NewGuid());

                var auth = await _channelAuthService.GetChannelAuth(channel, Guid.NewGuid());
                if (auth == null)
                {
                    loggerExtension.LogWarning("Get ChannelAuth is null");
                    return;
                }
                var channelAuth = new ChannelAuth { ShopId = auth.ShopId, AccessToken = auth.AccessToken };

                var (isSuccess, transactions) =
                    await client.GetListTransaction(channelAuth, createFrom, createTo, loggerExtension,
                        plaftform);

                if (!isSuccess)
                {
                    loggerExtension.LogWarning("Error while update payment");
                    return;
                }

                if (!transactions.Any())
                {
                    loggerExtension.Description = "No payments to update";
                    loggerExtension.LogInfo();
                    await UpdateLastSyncCache(data.ScheduleDto, createTo);
                    return;
                }

                var transactionsDomainList = transactions.Select(t => new ShopeePaymentTransactionHistory
                {
                    Amount = t.Amount,
                    BuyerName = t.BuyerName,
                    CreateTime = t.CreateTime,
                    CurrentBalance = t.CurrentBalance,
                    Description = t.Description,
                    OrderSn = t.OrderSn,
                    PayOrderList = t.PayOrderList?.Select(x => new MongoDb.PayOrder
                    {
                        OrderSn = x.OrderSn,
                        ShopName = x.ShopName
                    }).ToList(),
                    Reason = t.Reason,
                    RefundSn = t.RefundSn,
                    RetailerId = data.RetailerId,
                    RootWithdrawalId = t.RootWithdrawalId,
                    Status = t.Status,
                    TransactionFee = t.TransactionFee,
                    TransactionId = t.TransactionId,
                    TransactionType = t.TransactionType,
                    WalletType = t.WalletType,
                    WithdrawId = t.WithdrawId,
                    WithdrawalType = t.WithdrawalType,
                    ChannelType = data.ChannelType
                }).ToList();
                await HandleTransaction(data, transactionsDomainList, loggerExtension);
                await UpdateLastSyncCache(data.ScheduleDto, createTo);
            }
            catch (Exception e)
            {
                loggerExtension.LogError(e, true);
            }
        }

        private async Task HandleTransaction(T data, List<ShopeePaymentTransactionHistory> transactionsDomainList, LogObjectMicrosoftExtension loggerExtension)
        {
            using (var session = await _shopeePaymentTransactionHistoryService.GetSessionHandle())
            {
                try
                {
                    session.StartTransaction();
                    var (createCount, updateCount, result) = await _shopeePaymentTransactionHistoryService.AddOrUpdateWithSession(session, data.RetailerId, transactionsDomainList);
                    if (createCount > 0 || updateCount > 0)
                    {
                        loggerExtension.ResponseObject = result.ToSafeJson();
                        await CalAmountOfPayment(session, data.RetailerId, result);
                    }
                    await session.CommitTransactionAsync();
                    loggerExtension.Description = $"Success";
                    loggerExtension.LogInfo();
                }
                catch (Exception ex)
                {
                    await session.AbortTransactionAsync();
                    loggerExtension.LogError(ex);
                    throw;
                }
            }
        }

        private async Task  UpdateLastSyncCache(ScheduleDto dto, DateTime? lastSync = null)
        {
            var scheduleHelper = new ScheduleHelper(_scheduleService);

            if (lastSync != null)
            {
                dto.LastSync = lastSync;
                scheduleHelper.ReleaseSchedule(dto);

                using (var db = _dbConnectionFactory.OpenDbConnection())
                {
                    var kvChannelRepository = new OmniChannelScheduleRepository(db);
                    await kvChannelRepository.UpdateExecutePlan(dto.Id, lastSync, null);
                }

                return;
            }
            scheduleHelper.UnlockSchedule(dto);
        }

        private async Task CalAmountOfPayment(IClientSessionHandle session, int retailerId, List<ShopeePaymentTransactionHistory> transactionsDomainList)
        {
            var (newPaymentTransactionLst, oldPaymentTransactionLst) = await GetListInsertOrUpdate(session, retailerId, transactionsDomainList);
            await _paymentTransactionService.AddOrUpdateUnitOfWork(session, newPaymentTransactionLst, oldPaymentTransactionLst);
        }

        private async Task<(List<PaymentTransaction> newPaymentTransactionLst, List<PaymentTransaction> oldPaymentTransactionLst)> GetListInsertOrUpdate(IClientSessionHandle session, int retailerId, List<ShopeePaymentTransactionHistory> transactionsDomainList)
        {
            List<PaymentTransaction> newPaymentTransactionLst = new List<PaymentTransaction>();
            var orderSnList = transactionsDomainList.Select(item => item.OrderSn).Distinct().ToList();
            var oldPaymentTransactionLst = await _paymentTransactionService.GetSessionByListOrderSn(session, retailerId, orderSnList);
            var orderSnInfoLst = await _shopeePaymentTransactionHistoryService.GetPaymentChannelInvoices(session, retailerId, orderSnList);
            foreach (var orderSn in orderSnList)
            {
                var paymentLst = orderSnInfoLst.Where(item => item.OrderSn == orderSn && item.Amount != 0).ToList();
                if (paymentLst.Count > 0)
                {
                    var transactionId = paymentLst.FirstOrDefault().TransactionId;
                    var paymentAmount = paymentLst.Sum(item => item.Amount);
                    var paymentInfo = oldPaymentTransactionLst.FirstOrDefault(item => item.OrderSn == orderSn);
                    if (paymentInfo == null)
                    {
                        newPaymentTransactionLst.Add(new PaymentTransaction()
                        {
                            Amount = paymentAmount,
                            RetailerId = retailerId,
                            OrderSn = orderSn,
                            ChannelType = (int)ChannelTypeEnum.Shopee,
                            CreatedDate = DateTime.Now,
                            ModifiedDate = DateTime.Now,
                            TransactionId = transactionId
                        });
                    }
                    else
                    {
                        paymentInfo.Amount = paymentAmount;
                        paymentInfo.ModifiedDate = DateTime.Now;
                    }
                }
            }
            return (newPaymentTransactionLst, oldPaymentTransactionLst);
        }
    }
}

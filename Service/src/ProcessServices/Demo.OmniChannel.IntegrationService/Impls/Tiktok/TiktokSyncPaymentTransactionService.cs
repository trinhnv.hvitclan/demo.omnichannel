﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Impls;
using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Tiktok;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.Services.LogginConfiguration;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.IntegrationService.Impls.Tiktok
{
    public class TiktokSyncPaymentTransactionService : BaseService<TiktokSyncPaymentTransactionMessage>
    {
        private readonly IChannelBusiness _channelBusiness;
        private readonly IOmniChannelAuthService _channelAuthService;
        private readonly IOmniChannelPlatformService _omniChannelPlatformService;
        private readonly IPaymentTransactionService _paymentTransactionService;

        public TiktokSyncPaymentTransactionService(IAppSettings settings,
            ILogger<TiktokSyncPaymentTransactionService> logger,
            IChannelClient channelClient,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IMessageService mqService,
            KvRedisConfig mqRedisConfig,
            IChannelBusiness channelBusiness,
            IOmniChannelAuthService channelAuthService,
            IOmniChannelPlatformService omniChannelPlatformService,
            IPaymentTransactionService paymentTransactionService)
            : base(settings, logger, channelClient, cacheClient, dbConnectionFactory, mqService, mqRedisConfig)
        {
            ThreadSize = mqRedisConfig?.EventMessages
               .FirstOrDefault(t => !string.IsNullOrEmpty(t.Name) && t.Name.Contains(this.GetType().Name))?.ThreadSize ?? 10;

            MessageService.RegisterHandler<TiktokSyncPaymentTransactionMessage>(m =>
            {
                try
                {
                    m.Options = (int)MessageOption.None;
                    var task = Task.Run(async () => await ProcessMessage(m.GetBody()));
                    task.GetAwaiter().GetResult();
                    return Task.CompletedTask;
                }
                catch (Exception e)
                {
                    ExceptionHelper.WriteLogExceptionMq(typeof(TiktokSyncPaymentTransactionMessage), m.Body, e);
                    return string.Empty;
                }
            }, ThreadSize);
            _channelBusiness = channelBusiness;
            _channelAuthService = channelAuthService;
            _omniChannelPlatformService = omniChannelPlatformService;
            _paymentTransactionService = paymentTransactionService;
        }

        protected override async Task ProcessMessage(TiktokSyncPaymentTransactionMessage data)
        {
            var loggerExtension = new LogObjectMicrosoftExtension(Logger, data.LogId)
            {
                Action = ExceptionType.TiktokFinanceTransaction,
                RetailerId = data.RetailerId,
                RequestObject = data.DataSettlements,
                OmniChannelId = data.ChannelId
            };
            try
            {
                var settlements = await ValidateAndGetDataSettlements(data, loggerExtension);
                if (settlements == null || settlements.Count <= 0)
                {
                    loggerExtension.LogWarning("settlementList null or empty!!");
                    return;
                }
                var amount = settlements.Where(item => item.SettlementInfo != null && !string.IsNullOrEmpty(item.SettlementInfo.SettlementAmount))
                    .Sum(item => Helpers.ToDecimal(item.SettlementInfo.SettlementAmount, 0));
                var transaction = new PaymentTransaction()
                {
                    Amount = amount,
                    CreatedDate = DateTime.Now,
                    RetailerId = data.RetailerId,
                    OrderSn = data.OrderId,
                    TransactionId = Helpers.ToLong(data.OrderId, (Int64)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds),
                    ChannelType = (int)ShareKernel.Common.ChannelType.Tiktok
                };
                await _paymentTransactionService.AddOrUpdate(data.RetailerId, transaction);
                loggerExtension.Description = $"Update TiktokPaymentTransaction with orderId : {data.OrderId} and amount : {amount}";
                loggerExtension.LogInfo(true);
            }
            catch (Exception ex)
            {
                loggerExtension.LogError(ex);
            }

        }

        private async Task<List<TiktokSettlement>> ValidateAndGetDataSettlements(TiktokSyncPaymentTransactionMessage data, LogObjectMicrosoftExtension loggerExtension)
        {
            List<TiktokSettlement> settlements = new List<TiktokSettlement>();
            if (string.IsNullOrEmpty(data.DataSettlements))
            {
                loggerExtension.LogWarning("Input dataSettlements is null or empty!! We will call API to get the data");
                using (var db = DbConnectionFactory.OpenDbConnection())
                {
                    var channel = await _channelBusiness.GetChannel(data.ChannelId, data.RetailerId, loggerExtension.Id);
                    var channelAuth = await _channelAuthService.GetChannelAuth(channel, loggerExtension.Id);
                    if (channelAuth != null && !string.IsNullOrEmpty(channelAuth.AccessToken))
                    {
                        var auth = new ChannelAuth
                        {
                            Id = channelAuth.Id,
                            AccessToken = channelAuth.AccessToken,
                            ShopId = channelAuth.ShopId
                        };
                        var client = new TikTokClient(Settings);
                        var platform = await _omniChannelPlatformService.GetById(channel.PlatformId);
                        settlements = await client.GetOrderSettlements(data.ChannelId, auth, data.OrderId, platform, loggerExtension);
                    }
                }
            }
            else
            {
                settlements = data.DataSettlements.FromJson<List<TiktokSettlement>>();
                if (settlements == null)
                {
                    return new List<TiktokSettlement>();
                }
            }
            return settlements.Where(item => item.SettStatus != TiktokPaymentStatus.NotPay).ToList();
        }
    }
}

﻿using System.Threading;
using System.Threading.Tasks;
using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Sdk.Common;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.IntegrationService.Impls
{
    public abstract class BaseService<T> : IHostedService where T : class
    {
        protected IAppSettings Settings;
        protected int ThreadSize { get; set; }
        protected readonly ILogger Logger;
        protected IMessageFactory MessageFactory;
        protected IChannelClient ChannelClient;
        protected ICacheClient CacheClient;
        protected IDbConnectionFactory DbConnectionFactory;
        protected IMessageService MessageService;
        protected ChannelType ChannelType;
        protected KvRedisConfig MqRedisConfig;

        protected BaseService(
            IAppSettings settings,
            ILogger logger,
            IChannelClient channelClient,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IMessageService mqService,
            KvRedisConfig mqRedisConfig
            )
        {
            Logger = logger;
            Settings = settings;
            ChannelClient = channelClient;
            CacheClient = cacheClient;
            DbConnectionFactory = dbConnectionFactory;
            MessageService = mqService;
            MqRedisConfig = mqRedisConfig;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Logger.LogInformation($"----------------------------------------- Start Subscribe {typeof(T).Name} -----------------------------------------------");
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Logger.LogInformation($"----------------------------------------- Stop Subscribe {typeof(T).Name} -----------------------------------------------");
            return Task.CompletedTask;
        }

        protected abstract Task ProcessMessage(T data);

    }
}

﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.IntegrationService.Impls.Lazada
{
    public class TrackingInvoiceService : BaseService<ResyncTrackingInvoiceMessage>
    {
        private readonly IInvoiceInternalClient _invoiceInternalClient;
        public TrackingInvoiceService(
            IAppSettings settings,
            ILogger<ShopeeSyncPaymentTransactionService> logger,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IMessageService mqService,
            KvRedisConfig mqRedisConfig,
            IInvoiceInternalClient invoiceInternalClient
        ) : base(settings, logger, channelClient, cacheClient, dbConnectionFactory, mqService, mqRedisConfig)
        {
            ThreadSize = mqRedisConfig?.EventMessages
                .FirstOrDefault(t => !string.IsNullOrEmpty(t.Name) && t.Name.Contains(this.GetType().Name))?.ThreadSize ?? 10;
            MessageService.RegisterHandler<ResyncTrackingInvoiceMessage>(m =>
            {
                try
                {
                    m.Options = (int)MessageOption.None;
                    var task = Task.Run(async () => await ProcessMessage(m.GetBody()));
                    task.GetAwaiter().GetResult();
                    return Task.CompletedTask;
                }
                catch (Exception e)
                {
                    ExceptionHelper.WriteLogExceptionMq(typeof(ResyncTrackingInvoiceMessage), m.Body, e);
                    return string.Empty;
                }
            }, ThreadSize);
            _invoiceInternalClient = invoiceInternalClient;
        }
        protected override async Task ProcessMessage(ResyncTrackingInvoiceMessage data)
        {
            var loggerExtension = new LogObjectMicrosoftExtension(Logger, data.LogId)
            {
                Action = "ResyncTrackingInvoiceMessage",
                RetailerId = data.RetailerId,
                RequestObject = $"ResyncTrackingInvoiceMessage details RetailerId: {data.RetailerId}, InvoiceCode : {data.InvoiceCode}"
            };
            try
            {
                if (!string.IsNullOrEmpty(data.InvoiceCode))
                {
                    var connectStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);
                    var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, data.RetailerId, connectStringName, data.BranchId, Settings?.Get<int>("ExecutionContext"));
                    var coreContext = context.ConvertTo<Demo.OmniChannelCore.Api.Sdk.Common.KvInternalContext>();
                    coreContext.UserId = context.User?.Id ?? 0;
                    coreContext.LogId = loggerExtension.Id;
                    await _invoiceInternalClient.UpdateWithTracking(coreContext, data.InvoiceCode, coreContext.RetailerId);
                    loggerExtension.LogInfo(true);
                }
            }
            catch (Exception ex)
            {
                loggerExtension.LogError(ex);
            }
        }

    }
}

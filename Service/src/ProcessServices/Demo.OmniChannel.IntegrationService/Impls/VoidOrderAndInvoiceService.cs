﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.IntegrationService.Impls.Lazada
{
    public class VoidOrderAndInvoiceService : BaseService<VoidOrderAndInvoiceMessage>
    {
        private readonly IOrderInternalClient _orderInternalClient;
        public VoidOrderAndInvoiceService(
            IAppSettings settings,
            ILogger<ShopeeSyncPaymentTransactionService> logger,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IMessageService mqService,
            KvRedisConfig mqRedisConfig,
            IOrderInternalClient orderInternalClient
        ) : base(settings, logger, channelClient, cacheClient, dbConnectionFactory, mqService, mqRedisConfig)
        {
            ThreadSize = mqRedisConfig?.EventMessages
                .FirstOrDefault(t => !string.IsNullOrEmpty(t.Name) && t.Name.Contains(this.GetType().Name))?.ThreadSize ?? 10;
            MessageService.RegisterHandler<VoidOrderAndInvoiceMessage>(m =>
            {
                try
                {
                    m.Options = (int)MessageOption.None;
                    var task = Task.Run(async () => await ProcessMessage(m.GetBody()));
                    task.GetAwaiter().GetResult();
                    return Task.CompletedTask;
                }
                catch (Exception e)
                {
                    ExceptionHelper.WriteLogExceptionMq(typeof(LazadaSyncFeeTransactionMessage), m.Body, e);
                    return string.Empty;
                }
            }, ThreadSize);
            _orderInternalClient = orderInternalClient;
        }
        protected override async Task ProcessMessage(VoidOrderAndInvoiceMessage data)
        {
            var loggerExtension = new LogObjectMicrosoftExtension(Logger, data.LogId)
            {
                Action = "VoidOrderAndInvoiceMessage",
                RetailerId = data.RetailerId,
                RequestObject = $"VoidOrderAndInvoiceMessage details RetailerId: {data.RetailerId}, OrderId : {data.OrderId}"
            };
            try
            {
                var connectStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);
                var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, data.RetailerId, connectStringName, data.BranchId, Settings?.Get<int>("ExecutionContext"));
                var coreContext = context.ConvertTo<Demo.OmniChannelCore.Api.Sdk.Common.KvInternalContext>();
                coreContext.UserId = context.User?.Id ?? 0;
                coreContext.LogId = loggerExtension.Id;
                await _orderInternalClient.VoidOrderAndInvoiceList(coreContext, new System.Collections.Generic.List<long> { data.OrderId }, data.Prefix);
                loggerExtension.LogInfo(true);
            }
            catch (Exception ex)
            {
                loggerExtension.LogError(ex);
            }
        }

    }
}

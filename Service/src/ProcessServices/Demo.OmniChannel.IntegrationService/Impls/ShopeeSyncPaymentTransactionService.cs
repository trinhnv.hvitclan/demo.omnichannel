﻿using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Microsoft.Extensions.Logging;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.Business.Interfaces;

namespace Demo.OmniChannel.IntegrationService.Impls
{
    public class ShopeeSyncPaymentTransactionService : SyncPaymentTransactionService<ShopeeSyncPaymentTransactionMessage>
    {
        public ShopeeSyncPaymentTransactionService(IAppSettings settings,
            ILogger<ShopeeSyncPaymentTransactionService> logger,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IMessageService mqService,
            KvRedisConfig mqRedisConfig,
            IShopeePaymentTransactionHistoryService shopeePaymentTransactionHistoryService,
            IScheduleService scheduleService,
            IOmniChannelPlatformService omniChannelPlatformService,
            IPaymentTransactionService paymentTransactionService,
            IOmniChannelAuthService channelAuthService,
            IChannelBusiness channelBusiness
            ) : base(
            settings,
            logger,
            channelClient, 
            cacheClient, 
            dbConnectionFactory, 
            mqService,
            mqRedisConfig,
            shopeePaymentTransactionHistoryService,
            scheduleService,
            omniChannelPlatformService,
            paymentTransactionService,
            channelAuthService,
            channelBusiness)
        {
            ChannelType = ChannelType.Shopee;
        }
    }
}

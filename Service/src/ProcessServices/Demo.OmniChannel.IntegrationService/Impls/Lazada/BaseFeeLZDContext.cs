﻿using Demo.OmniChannel.Repository.Impls;

namespace Demo.OmniChannel.IntegrationService.Impls.Lazada
{
    public class BaseFeeLzdContext
    {
        private readonly OrderRepository _orderRepository;
        private readonly DeliveryInfoRepository _deliveryInfoRepository;
        private readonly InvoiceRepository _invoiceRepository;
        public BaseFeeLzdContext(OrderRepository orderRepository, DeliveryInfoRepository deliveryInfoRepository, InvoiceRepository invoiceRepository)
        {
            _orderRepository = orderRepository;
            _deliveryInfoRepository = deliveryInfoRepository;
            _invoiceRepository = invoiceRepository;
        }

        public OrderRepository GetOrderRepository()
        {
            return _orderRepository;
        }

        public DeliveryInfoRepository GetDeliveryInfoRepository()
        {
            return _deliveryInfoRepository;
        }

        public InvoiceRepository GetInvoiceRepository()
        {
            return _invoiceRepository;
        }
    }
}

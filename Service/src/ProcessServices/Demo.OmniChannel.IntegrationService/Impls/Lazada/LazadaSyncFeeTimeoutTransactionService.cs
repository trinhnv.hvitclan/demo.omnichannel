﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.Services.LogginConfiguration;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.IntegrationService.Impls.Lazada
{
    public class LazadaSyncFeeTimeoutTransactionService : BaseService<LazadaSyncFeeTimeoutTransactionMessage>
    {
        private readonly IOmniChannelAuthService _channelAuthService;
        private readonly IChannelBusiness _channelBusiness;
        private readonly ILazadaFeePaymentTransactionHistoryService _lazadaFeePaymentTransactionHistory;

        public LazadaSyncFeeTimeoutTransactionService(
            IAppSettings settings,
            ILogger<ShopeeSyncPaymentTransactionService> logger,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IMessageService mqService,
            KvRedisConfig mqRedisConfig,
            IOmniChannelAuthService channelAuthService,
            IChannelBusiness channelBusiness,
            ILazadaFeePaymentTransactionHistoryService lazadaFeePaymentTransactionHistory
        ) : base(settings, logger, channelClient, cacheClient, dbConnectionFactory, mqService, mqRedisConfig)
        {
            ThreadSize = mqRedisConfig?.EventMessages
                .FirstOrDefault(t => !string.IsNullOrEmpty(t.Name) && t.Name.Contains(this.GetType().Name))?.ThreadSize ?? 10;
            MessageService.RegisterHandler<LazadaSyncFeeTimeoutTransactionMessage>(m =>
            {
                try
                {
                    m.Options = (int)MessageOption.None;
                    var task = Task.Run(async () => await ProcessMessage(m.GetBody()));
                    task.GetAwaiter().GetResult();
                    return Task.CompletedTask;
                }
                catch (Exception e)
                {
                    ExceptionHelper.WriteLogExceptionMq(typeof(LazadaSyncFeeTimeoutTransactionMessage), m.Body, e);
                    return string.Empty;
                }
            }, ThreadSize);
            _channelAuthService = channelAuthService;
            _channelBusiness = channelBusiness;
            _lazadaFeePaymentTransactionHistory = lazadaFeePaymentTransactionHistory;
        }

        protected override async Task ProcessMessage(LazadaSyncFeeTimeoutTransactionMessage data)
        {
            var loggerExtension = new LogObjectMicrosoftExtension(Logger, data.LogId)
            {
                Action = ExceptionType.SyncLZDTimeoutFinanceTransaction,
                RetailerId = data.RetailerId,
                BranchId = data.BranchId,
                OmniChannelId = data.ChannelId,
                ChannelType = data.ChannelType,
                ChannelTypeCode = ((ChannelTypeEnum)data.ChannelType).ToString(),
                RequestObject = $"LazadaSyncFeeTransactionMessage details {data.ToSafeJson()}"
            };
            try
            {
                var connectStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);
                using (var db = DbConnectionFactory.OpenDbConnection())
                {
                    var scheduleRepo = new OmniChannelScheduleRepository(db);
                    var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, data.RetailerId, connectStringName, data.BranchId, Settings?.Get<int>("ExecutionContext"));
                    var coreContext = context.ConvertTo<Demo.OmniChannelCore.Api.Sdk.Common.KvInternalContext>();
                    coreContext.UserId = context.User?.Id ?? 0;
                    coreContext.LogId = loggerExtension.Id;
                    var channel = await _channelBusiness.GetChannel(data.ChannelId, data.RetailerId, loggerExtension.Id);
                    var auth = await _channelAuthService.GetChannelAuth(channel, loggerExtension.Id);
                    if (auth == null)
                    {
                        loggerExtension.LogError(new KvOmniChannelAuthException($"[LogId = {data.LogId:N}] GetAuthInfo Channel:{data.ChannelId} RetailerId:{data.RetailerId} Empty ChannelAuth"));
                        return;
                    }
                    DateTime startTime = DateTime.UtcNow.AddDays(-1).ToUniversalTime(); // 1 Ngày với những bản ghi mới
                    DateTime endTime = DateTime.UtcNow.ToUniversalTime();
                    var scheduleFeeInfo = (await scheduleRepo.WhereAsync(item => item.OmniChannelId == data.ChannelId && item.Type == (int)ScheduleType.SyncFeeTransaction)).FirstOrDefault();
                    if (scheduleFeeInfo != null && scheduleFeeInfo.LastSync.HasValue)
                    {
                        startTime = scheduleFeeInfo.LastSync.Value;
                    }
                    var client = ChannelClient.GetClient(data.ChannelType, Settings);

                    await ProcessPageSizeTimeoutTransaction(data, coreContext, client, auth, startTime, endTime, scheduleRepo, scheduleFeeInfo, loggerExtension);

                }
                loggerExtension.LogInfo(true);
            }
            catch (Exception ex)
            {
                loggerExtension.LogError(ex);
            }
            finally
            {
                RemoveRunningCache(data.ChannelId, data.OrderSn);
            }
        }



        private async Task ProcessPageSizeTimeoutTransaction(LazadaSyncFeeTimeoutTransactionMessage data,
            Demo.OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext,
            Demo.OmniChannel.ChannelClient.Interfaces.IBaseClient client,
            ChannelClient.Models.TokenResponse auth,
            DateTime startTime,
            DateTime endTime,
            OmniChannelScheduleRepository scheduleRepo,
            OmniChannelSchedule scheduleFeeInfo,
            LogObjectMicrosoftExtension loggerExtension)
        {
            while (startTime < endTime)
            {
                var nextStartTime = startTime.AddDays(1);
                if (nextStartTime > endTime)
                {
                    nextStartTime = endTime;
                }
                var (isValid, feeTransactions) = await client.GetFinanceTransactions(auth.AccessToken, data.ChannelId,
                    data.OrderSn, startTime, nextStartTime, loggerExtension, data.IsBreakIfMaximum);

                if (feeTransactions.Any())
                {
                    feeTransactions = feeTransactions.Where(item => !string.IsNullOrEmpty(item.OrderNo)).ToList();

                    var listOrderSn = feeTransactions.Select(item => item.OrderNo).Distinct().ToList();
                    // Lưu vào mongo
                    var pageSize = Settings.Get<int>("LazadaProcessTransactionPageSize", 200);
                    int totalCount = feeTransactions.Count;
                    var totalPages = (int)Math.Ceiling((decimal)totalCount / pageSize);
                    for (int page = 1; page <= totalPages; page++)
                    {
                        var invoicePaging = listOrderSn.Page(page, pageSize).ToList();
                        var pageSizeFeeTransactions = feeTransactions.Where(item => invoicePaging.Contains(item.OrderNo)).ToList();
                        await ProcessSaveFinanceToDb(pageSizeFeeTransactions);
                        // Bắn message lên để xử lý
                        PushSyncFinanceMQ(data, coreContext, invoicePaging);
                    }
                }
                await AddOrUpdateSchedule(scheduleRepo, scheduleFeeInfo, (int)data.ChannelId, nextStartTime);
                startTime = nextStartTime;
                loggerExtension.Description = $"EndTime: {nextStartTime}; Count : {feeTransactions.Count}; isValid : {isValid}; startTime : {startTime.ToSafeJson()}";
                loggerExtension.LogInfo(true);
            }
        }

        private void PushSyncFinanceMQ(LazadaSyncFeeTimeoutTransactionMessage data, Demo.OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext, List<string> feeOrderIdTransactions)
        {
            PushMQToSyncFee(data, coreContext, feeOrderIdTransactions);
            PushMqToSyncPaymentTransaction(coreContext, feeOrderIdTransactions);
        }

        private void PushMqToSyncPaymentTransaction(Demo.OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext, List<string> financeTransactionOrderSnLst, int pageSize = 200)
        {
            int totalCount = financeTransactionOrderSnLst.Count;
            var totalPages = (int)Math.Ceiling(totalCount / (decimal)pageSize);
            using (var mq = MessageService.CreateMessageProducer())
            {
                for (int page = 1; page <= totalPages; page++)
                {
                    var financeTransactionOrderSnPaging = financeTransactionOrderSnLst.Page(page, pageSize).ToList();
                    mq.Publish(new LazadaSyncPaymentTransactionMessage
                    {
                        OrderSnLst = financeTransactionOrderSnPaging,
                        ChannelType = (byte)Demo.OmniChannel.ShareKernel.Common.ChannelType.Lazada,
                        RetailerId = coreContext.RetailerId,
                        BranchId = coreContext.BranchId
                    });
                }
            }
        }

        private void PushMQToSyncFee(LazadaSyncFeeTimeoutTransactionMessage data, Demo.OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext, List<string> feeTransactions, int pageSize = 200)
        {

            using (var mq = MessageService.CreateMessageProducer())
            {
                foreach (var item in feeTransactions)
                {
                    mq.Publish(new LazadaProcessFeeTransactionMessage
                    {
                        OrderSn = item,
                        ChannelType = (byte)Demo.OmniChannel.ShareKernel.Common.ChannelType.Lazada,
                        RetailerId = coreContext.RetailerId,
                        BranchId = coreContext.BranchId,
                        KvEntities = data.KvEntities
                    });
                }
            }
        }

        private async Task ProcessSaveFinanceToDb(List<FinanceTransaction> feeTransactions)
        {
            var orderSnLst = feeTransactions.Select(item => item.OrderNo).Distinct().ToList();
            using (var session = await _lazadaFeePaymentTransactionHistory.GetSessionHandle())
            {
                try
                {
                    session.StartTransaction();
                    // 
                    var updateLst = new List<LazadaFeePaymentTransactionHistory>();
                    var existedFinanceLst = await _lazadaFeePaymentTransactionHistory.GetSessionListByOrderSn(session, orderSnLst);
                    var existedOrderSnLst = existedFinanceLst.Select(item => item.OrderNo).Distinct().ToList();
                    var insertedOrderSnLst = feeTransactions.Where(item => !existedOrderSnLst.Contains(item.OrderNo))
                        .Select(item => ConvertFromFinanceTransaction(item)).ToList();
                    if (insertedOrderSnLst.Count > 0)
                    {
                        insertedOrderSnLst = insertedOrderSnLst.GroupBy(item => new { item.OrderNo, item.TransactionNumber, item.FeeType, item.OrderItemNo })
                            .Select(x => x.First()).ToList();
                    }
                    var updatedFinanceLst = feeTransactions.Where(item => existedOrderSnLst.Contains(item.OrderNo)).ToList();
                    if (updatedFinanceLst.Any())
                    {
                        foreach (var item in updatedFinanceLst)
                        {
                            var existedItem = existedFinanceLst.FirstOrDefault(x => x.OrderNo == item.OrderNo && x.TransactionNumber == item.TransactionNumber && x.OrderItemNo == item.OrderItemNo && x.FeeType == item.FeeType);
                            if (existedItem != null)
                            {
                                if (existedItem.Amount == item.Amount)
                                {
                                    continue;
                                }
                                existedItem.Amount = item.Amount;
                                updateLst.Add(existedItem);
                            }
                            else
                            {
                                insertedOrderSnLst.Add(ConvertFromFinanceTransaction(item));

                            }
                        }
                    }
                    await _lazadaFeePaymentTransactionHistory.AddSessionBulkAsync(session, insertedOrderSnLst);
                    await _lazadaFeePaymentTransactionHistory.UpdateSessionBulkAsync(session, updateLst);
                    await session.CommitTransactionAsync();
                }
                catch
                {
                    await session.AbortTransactionAsync();
                    throw;
                }
            }
        }

        private LazadaFeePaymentTransactionHistory ConvertFromFinanceTransaction(FinanceTransaction item)
        {
            return new LazadaFeePaymentTransactionHistory()
            {
                OrderNo = item.OrderNo,
                TransactionDate = item.TransactionDate,
                Amount = item.Amount,
                PaidStatus = item.PaidStatus,
                ShippingProvider = item.ShippingProvider,
                WHTIncludedInAmount = item.WHTIncludedInAmount,
                LazadaSKU = item.LazadaSKU,
                TransactionType = item.TransactionType,
                OrderItemNo = item.OrderItemNo,
                OrderItemStatus = item.OrderItemStatus,
                Reference = item.Reference,
                FeeName = item.FeeName,
                ShippingSpeed = item.ShippingSpeed,
                WHTAmount = item.WHTAmount,
                TransactionNumber = item.TransactionNumber,
                SellerSku = item.SellerSku,
                Statement = item.Statement,
                Details = item.Details,
                VATInAmount = item.VATInAmount,
                ShipmentType = item.ShipmentType,
                CreatedDate = DateTime.Now,
                ModifiedDate = DateTime.Now,
                FeeType = item.FeeType
            };
        }

        private void RemoveRunningCache(long channelId, string orderSn)
        {
            if (string.IsNullOrEmpty(orderSn))
            {
                CacheClient.Remove(string.Format(KvConstant.FeeLZDPlatformKey, channelId));
            }
        }

        private async Task<bool> AddOrUpdateSchedule(OmniChannelScheduleRepository scheduleRepo, Demo.OmniChannel.Domain.Model.OmniChannelSchedule scheduleFeeInfo, int omniChannelId, DateTime lastSync)
        {
            if (scheduleFeeInfo == null)
            {
                // Insert
                scheduleFeeInfo = new Domain.Model.OmniChannelSchedule()
                {
                    IsRunning = false,
                    LastSync = lastSync,
                    OmniChannelId = omniChannelId,
                    Type = (int)ScheduleType.SyncFeeTransaction
                };
            }
            scheduleFeeInfo.LastSync = lastSync;
            return await scheduleRepo.SaveAsync(scheduleFeeInfo);
        }


    }
}

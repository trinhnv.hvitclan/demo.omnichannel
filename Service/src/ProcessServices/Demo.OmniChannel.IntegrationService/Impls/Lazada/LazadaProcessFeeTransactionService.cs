﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.IntegrationService.Common;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Services.LogginConfiguration;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.ShareKernel.Models;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.IntegrationService.Impls.Lazada
{
    public class LazadaProcessFeeTransactionService : BaseService<LazadaProcessFeeTransactionMessage>
    {
        private readonly ILazadaFeePaymentTransactionHistoryService _lazadaFeePaymentTransactionHistoryService;
        private readonly IDeliveryInfoInternalClient _deliveryInfoInternalClient;
        private readonly List<string> _feeIgnoreList;

        public LazadaProcessFeeTransactionService(
          IAppSettings settings,
          ILogger<ShopeeSyncPaymentTransactionService> logger,
          ChannelClient.Impls.ChannelClient channelClient,
          ICacheClient cacheClient,
          IDbConnectionFactory dbConnectionFactory,
          IMessageService mqService,
          KvRedisConfig mqRedisConfig,
          ILazadaFeePaymentTransactionHistoryService lazadaFeePaymentTransactionHistoryService,
          IDeliveryInfoInternalClient deliveryInfoInternalClient
          ) : base(settings, logger, channelClient, cacheClient, dbConnectionFactory, mqService, mqRedisConfig)
        {

            ThreadSize = mqRedisConfig?.EventMessages
                .FirstOrDefault(t => !string.IsNullOrEmpty(t.Name) && t.Name.Contains(this.GetType().Name))?.ThreadSize ?? 10;

            MessageService.RegisterHandler<LazadaProcessFeeTransactionMessage>(m =>
            {
                try
                {
                    m.Options = (int)MessageOption.None;
                    var task = Task.Run(async () => await ProcessMessage(m.GetBody()));
                    task.GetAwaiter().GetResult();
                    return Task.CompletedTask;
                }
                catch (Exception e)
                {
                    ExceptionHelper.WriteLogExceptionMq(typeof(LazadaSyncPaymentTransactionMessage), m.Body, e);
                    return string.Empty;
                }
            }, ThreadSize);
            _lazadaFeePaymentTransactionHistoryService = lazadaFeePaymentTransactionHistoryService;
            _deliveryInfoInternalClient = deliveryInfoInternalClient;
            _feeIgnoreList = Settings.Get<IgnoreFeeTypeLst>("IgnoreFeeTypeLst").FeeLst;
        }


        protected override async Task ProcessMessage(LazadaProcessFeeTransactionMessage data)
        {
            var loggerExtension = new LogObjectMicrosoftExtension(Logger, data.LogId)
            {
                Action = ExceptionType.LazadaProcessFeeTransaction,
                RetailerId = data.RetailerId,
                BranchId = data.BranchId,
                ChannelType = data.ChannelType,
                RequestObject = $"LazadaProcessFeeTransactionMessage details {data.OrderSn}"
            };
            try
            {
                if (!string.IsNullOrEmpty(data.OrderSn))
                {
                    var connectStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);
                    var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, data.RetailerId, connectStringName, data.BranchId, Settings?.Get<int>("ExecutionContext"));
                    var coreContext = context.ConvertTo<Demo.OmniChannelCore.Api.Sdk.Common.KvInternalContext>();
                    coreContext.UserId = context.User?.Id ?? 0;
                    coreContext.LogId = loggerExtension.Id;
                    var feeTransactionLst = await _lazadaFeePaymentTransactionHistoryService.GetListByOrderSn(new List<string>() { data.OrderSn });
                    await ProcessFinanceTransaction(data, coreContext, feeTransactionLst);
                    loggerExtension.LogInfo(true);
                }
            }
            catch (Exception ex)
            {
                loggerExtension.LogError(ex);
            }
        }

        private bool ValidateTransactionLst(List<LazadaFeePaymentTransactionHistory> feeTransactions)
        {
            if (feeTransactions == null)
            {
                return false;
            }
            if (feeTransactions.Count == 0)
            {
                return false;
            }
            return true;
        }

        private async Task ProcessFinanceTransaction(LazadaProcessFeeTransactionMessage data, Demo.OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext, List<LazadaFeePaymentTransactionHistory> feeTransactions)
        {
            if (ValidateTransactionLst(feeTransactions))
            {
                await ProcessValidatedFinanceTransaction(data, coreContext, feeTransactions);
            }
        }

        private async Task<bool> ProcessValidatedFinanceTransaction(LazadaProcessFeeTransactionMessage data, Demo.OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext, List<LazadaFeePaymentTransactionHistory> feeTransactions)
        {
            OrderRepository _orderRepository;
            DeliveryInfoRepository _deliveryInfoRepository;
            InvoiceRepository _invoiceRepository;
            var connectStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);
            using (var dbRetailer = await DbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                _orderRepository = new OrderRepository(dbRetailer);
                _invoiceRepository = new InvoiceRepository(dbRetailer);
                _deliveryInfoRepository = new DeliveryInfoRepository(dbRetailer);
                var baseFeeLZDContext = new BaseFeeLzdContext(_orderRepository, _deliveryInfoRepository, _invoiceRepository);
                var financeTransactionOrderSnLst = feeTransactions.Select(item => item.OrderNo).Distinct().ToList();
                var codePrefix = ChannelTypeHelper.GetOrderPrefix((byte)ChannelTypeEnum.Lazada);
                var orderWithFrefixLst = financeTransactionOrderSnLst.Select(item => $"{codePrefix}_{item}").ToList();
                var orderInfoLst = await baseFeeLZDContext.GetOrderRepository().GetOrdersByCodeListAsync(orderWithFrefixLst, coreContext.RetailerId, Settings.Get<int>("LazadaFeeGetOrderByCodePageSize", 50));
                var orderIdLst = orderInfoLst.Select(item => (long?)item.Id).ToList();
                var invoiceInfoLst = await baseFeeLZDContext.GetInvoiceRepository().GetByLstOrderId(orderIdLst, coreContext.RetailerId, Settings.Get<int>("LazadaFeeGetOrderByCodePageSize", 50));
                var invoiceIdLst = invoiceInfoLst.Select(item => (long?)item.Id).ToList();
                var deliveryUpdateLst = new List<Demo.OmniChannel.Domain.Model.DeliveryInfo>();
                var deliveryOrderInfoLst = await baseFeeLZDContext.GetDeliveryInfoRepository().GetDeliveryInfoByListOrderId(orderIdLst, coreContext.RetailerId, Settings.Get<int>("LazadaFeeGetOrderByCodePageSize", 50));
                var deliveryInvoiceInfoLst = await baseFeeLZDContext.GetDeliveryInfoRepository().GetDeliveryInfoByListInvoices(invoiceIdLst, coreContext.RetailerId, Settings.Get<int>("LazadaFeeGetOrderByCodePageSize", 50));
                var mappingDelivery = orderInfoLst.ToDictionary(x => x.Code, y => y.Id);
                foreach (var financeTransactionOrderSn in financeTransactionOrderSnLst)
                {
                    long orderId = 0;
                    if (mappingDelivery.TryGetValue($"{codePrefix}_{financeTransactionOrderSn}", out orderId))
                    {
                        var deliveryOrderInfo = deliveryOrderInfoLst.FirstOrDefault(item => item.OrderId == orderId);
                        var invoiceInfo = invoiceInfoLst.FirstOrDefault(item => item.OrderId == orderId);
                        if (invoiceInfo != null && invoiceInfo.Status == (byte)InvoiceState.Issued)
                        {
                            continue;
                        }
                        if (deliveryOrderInfo != null)
                        {
                            var financeTransactionByOrderLst = feeTransactions.Where(item => item.OrderNo == financeTransactionOrderSn).ToList();
                            var feeList = OnConvertFeeJson(financeTransactionByOrderLst);
                            ModifiedFee(deliveryOrderInfo, feeList);
                            deliveryUpdateLst.Add(deliveryOrderInfo);
                            if (invoiceInfo != null && deliveryInvoiceInfoLst != null && deliveryInvoiceInfoLst.Count > 0)
                            {
                                var deliveryInvoiceInfo = deliveryInvoiceInfoLst.FirstOrDefault(item => item.InvoiceId == invoiceInfo.Id);
                                if (deliveryInvoiceInfo != null)
                                {
                                    deliveryInvoiceInfo.FeeJson = deliveryOrderInfo.FeeJson;
                                    deliveryInvoiceInfo.Price = deliveryOrderInfo.Price;
                                    deliveryInvoiceInfo.TotalPrice = deliveryOrderInfo.TotalPrice;
                                    deliveryUpdateLst.Add(deliveryInvoiceInfo);
                                }
                            }
                        }
                    }
                }
                if (deliveryUpdateLst.Count > 0)
                {
                    await UpdateDeliveryAsync(coreContext, deliveryUpdateLst);
                }
                return true;
            }
        }

        private async Task UpdateDeliveryAsync(Demo.OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext, List<Demo.OmniChannel.Domain.Model.DeliveryInfo> deliveryUpdateLst)
        {
            var coreUpdateDeliveryLst = deliveryUpdateLst.Select(item => ConvertToDeliveryInfoCoreModel(item)).ToList();
            foreach (var item in coreUpdateDeliveryLst)
            {
                await _deliveryInfoInternalClient.UpdateDeliveryPriceAndFeeJson(coreContext, item);
            }
        }

        private Demo.OmniChannelCore.Api.Sdk.Models.DeliveryInfo ConvertToDeliveryInfoCoreModel(Demo.OmniChannel.Domain.Model.DeliveryInfo deliveryInfo)
        {
            return new Demo.OmniChannelCore.Api.Sdk.Models.DeliveryInfo
            {
                Status = deliveryInfo.Status,
                OriginalCod = deliveryInfo.OriginalCod,
                BranchTakingAddressStr = deliveryInfo.BranchTakingAddressStr,
                BranchTakingAddressId = deliveryInfo.BranchTakingAddressId,
                TotalPrice = deliveryInfo.TotalPrice,
                StatusNote = deliveryInfo.StatusNote,
                DebtControlCode = deliveryInfo.DebtControlCode,
                SortCode = deliveryInfo.SortCode,
                IsVoidByCarrier = deliveryInfo.IsVoidByCarrier,
                IsCurrent = deliveryInfo.IsCurrent,
                ServiceTypeText = deliveryInfo.ServiceTypeText,
                ModifiedBy = deliveryInfo.ModifiedBy,
                ModifiedDate = DateTime.Now,
                CreatedBy = deliveryInfo.CreatedBy,
                CreatedDate = deliveryInfo.CreatedDate,
                UseDefaultPartner = deliveryInfo.UseDefaultPartner,
                ExpectedDeliveryText = deliveryInfo.ExpectedDeliveryText,
                ExpectedDelivery = deliveryInfo.ExpectedDelivery,
                PriceCodPayment = deliveryInfo.PriceCodPayment,
                Price = deliveryInfo.Price,
                ServiceAdd = deliveryInfo.ServiceAdd,
                ServiceType = deliveryInfo.ServiceType,
                DeliveryCode = deliveryInfo.DeliveryCode,
                DeliveryBy = deliveryInfo.DeliveryBy,
                DeliveyPackageId = deliveryInfo.DeliveyPackageId,
                RetailerId = deliveryInfo.RetailerId,
                OrderId = deliveryInfo.OrderId,
                InvoiceId = deliveryInfo.InvoiceId,
                Id = deliveryInfo.Id,
                FeeJson = deliveryInfo.FeeJson
            };
        }


        private void ModifiedFee(Demo.OmniChannel.Domain.Model.DeliveryInfo deliveryInfo, List<FeeDetail> newFeeLst)
        {
            if (newFeeLst.Count == 0)
            {
                return;
            }
            // Convert Fee cũ về object FeeDetail
            newFeeLst = newFeeLst.GroupBy(item => item.Name).Select(item => new FeeDetail
            {
                Name = item.Key,
                Value = LazadaFeeHelper.ConvertFeeLazadaToRealNumber(item.Sum(x => x.Value))
            }).ToList();
            ConvertToFeeJson(deliveryInfo, newFeeLst);
        }



        private void ConvertToFeeJson(Demo.OmniChannel.Domain.Model.DeliveryInfo deliveryInfo, List<FeeDetail> newFeeLst)
        {
            var feeDetail = new Fee { FeeList = newFeeLst, ChannelType = (byte)ChannelTypeEnum.Lazada };
            deliveryInfo.FeeJson = feeDetail.ToSafeJson();
            deliveryInfo.Price = (decimal?)feeDetail.TotalFee();
            if (deliveryInfo.Price.HasValue)
            {
                deliveryInfo.Price = decimal.Round(deliveryInfo.Price.Value, 2);
            }
            deliveryInfo.TotalPrice = deliveryInfo.Price;
        }

        private List<FeeDetail> OnConvertFeeJson(List<LazadaFeePaymentTransactionHistory> financeTransactionByOrderLst)
        {
            List<FeeDetail> resultFeeDetail = new List<FeeDetail>();
            if (financeTransactionByOrderLst == null || financeTransactionByOrderLst.Count == 0)
            {
                return resultFeeDetail;
            }
            resultFeeDetail = financeTransactionByOrderLst
                .Where(item => LazadaFeeHelper.ValidateFee(item, _feeIgnoreList))
                .Select(item => LazadaFeeHelper.ConvertTransactionFeeToFeeJson(item, _feeIgnoreList))
                .ToList();

            resultFeeDetail = resultFeeDetail.GroupBy(item => item.Name).Select(item => new FeeDetail
            {
                Name = item.Key,
                Value = item.Sum(x => x.Value)
            }).ToList();
            return resultFeeDetail;
        }
    }
}

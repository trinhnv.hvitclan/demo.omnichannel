﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.IntegrationService.Common;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Services.LogginConfiguration;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.IntegrationService.Impls.Lazada
{
    public class LazadaSyncPaymentTransactionService : BaseService<LazadaSyncPaymentTransactionMessage>
    {
        private readonly ILazadaFeePaymentTransactionHistoryService _lazadaFeePaymentTransactionHistoryService;
        private readonly IPaymentTransactionService _paymentTransactionService;
        private readonly List<string> _feeIgnoreList;

        public LazadaSyncPaymentTransactionService(
            IAppSettings settings,
            ILogger<ShopeeSyncPaymentTransactionService> logger,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IMessageService mqService,
            KvRedisConfig mqRedisConfig,
            IPaymentTransactionService paymentTransactionService,
            ILazadaFeePaymentTransactionHistoryService lazadaFeePaymentTransactionHistoryService
            ) : base(settings, logger, channelClient, cacheClient, dbConnectionFactory, mqService, mqRedisConfig)
        {

            ThreadSize = mqRedisConfig?.EventMessages
                .FirstOrDefault(t => !string.IsNullOrEmpty(t.Name) && t.Name.Contains(this.GetType().Name))?.ThreadSize ?? 10;

            MessageService.RegisterHandler<LazadaSyncPaymentTransactionMessage>(m =>
            {
                try
                {
                    m.Options = (int)MessageOption.None;
                    var task = Task.Run(async () => await ProcessMessage(m.GetBody()));
                    task.GetAwaiter().GetResult();
                    return Task.CompletedTask;
                }
                catch (Exception e)
                {
                    ExceptionHelper.WriteLogExceptionMq(typeof(LazadaSyncPaymentTransactionMessage), m.Body, e);
                    return string.Empty;
                }
            }, ThreadSize);
            _lazadaFeePaymentTransactionHistoryService = lazadaFeePaymentTransactionHistoryService;
            _paymentTransactionService = paymentTransactionService;
            _feeIgnoreList = Settings.Get<IgnoreFeeTypeLst>("IgnoreFeeTypeLst").FeePaymentLst;

        }
        protected async override Task ProcessMessage(LazadaSyncPaymentTransactionMessage data)
        {
            // Log
            var loggerExtension = new LogObjectMicrosoftExtension(Logger, data.LogId)
            {
                Action = ExceptionType.SyncLZDPaymentTransaction,
                RetailerId = data.RetailerId,
                BranchId = data.BranchId,
                ChannelType = data.ChannelType,
                RequestObject = $"LazadaSyncPaymentTransactionMessage details {string.Join(",", data.OrderSnLst)}"
            };
            try
            {
                if (data.OrderSnLst != null && data.OrderSnLst.Count > 0)
                {
                    var feeTransactionLst = await _lazadaFeePaymentTransactionHistoryService.GetListByOrderSn(data.OrderSnLst);
                    var feeTransactionByTypeLst = feeTransactionLst.Where(item => !string.IsNullOrEmpty(item.FeeName) && item.FeeName.Equals(FeeList.LazadaItemPriceCredit)).ToList();
                    if (feeTransactionByTypeLst.Count > 0)
                    {
                        // List need update
                        var paymentTransactionFromDbLst = await _paymentTransactionService.GetByListOrderSn(data.RetailerId, data.OrderSnLst);
                        await OnProcessUpdatePayment(data.RetailerId, paymentTransactionFromDbLst, feeTransactionByTypeLst, feeTransactionLst);
                    }
                }
                loggerExtension.LogInfo(true);
            }
            catch (Exception ex)
            {
                loggerExtension.LogError(ex);
            }
        }

        private async Task OnProcessUpdatePayment(long retailerId, List<PaymentTransaction> paymentTransactionFromDbLst, List<LazadaFeePaymentTransactionHistory> feeTransactionByTypeLst, List<LazadaFeePaymentTransactionHistory> feeTransactionLst)
        {
            List<PaymentTransaction> insertOrUpdateLst = new List<PaymentTransaction>();
            foreach (var paymentTransactionFromDbInfo in paymentTransactionFromDbLst)
            {
                // Tính update những transaction cũ
                var paymentLst = feeTransactionByTypeLst
                    .Where(item => item.OrderNo == paymentTransactionFromDbInfo.OrderSn).ToList();
                if (paymentLst.Count <= 0) continue;
                var sumAmount = paymentLst.Sum(item => Helpers.ToDecimal(item.Amount, (decimal)0));
                //Tính phí và Trừ phí
                var feeAmount = feeTransactionLst
                    .Where(fee => fee.OrderNo == paymentTransactionFromDbInfo.OrderSn && LazadaFeeHelper.ValidateFee(fee, _feeIgnoreList))
                    .Select(t => LazadaFeeHelper.ConvertToFeeJson(t, _feeIgnoreList))
                    .Sum(t => t.Value);
                sumAmount = sumAmount + (decimal)feeAmount;
                if (paymentTransactionFromDbInfo.Amount != sumAmount)
                {
                    paymentTransactionFromDbInfo.Amount = sumAmount;
                    paymentTransactionFromDbInfo.ModifiedDate = DateTime.Now;
                    insertOrUpdateLst.Add(paymentTransactionFromDbInfo);
                }
            }

            // Transaction mới
            var orderUpdateLst = paymentTransactionFromDbLst.Select(item => item.OrderSn).Distinct().ToList();
            var insertTransactionHistoryLst = feeTransactionByTypeLst.Where(item => !orderUpdateLst.Contains(item.OrderNo))
                .GroupBy(item => item.OrderNo)
                .Select(item => new PaymentTransaction()
                {
                    Amount = item.Sum(x => Helpers.ToDecimal(x.Amount, (decimal)0)),
                    CreatedDate = DateTime.Now,
                    RetailerId = (int)retailerId,
                    OrderSn = item.Key,
                    TransactionId = Helpers.ToLong(item.Key, (Int64)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds),
                    ChannelType = (int)ShareKernel.Common.ChannelType.Lazada
                }).ToList();
            //Trừ phí
            insertTransactionHistoryLst = insertTransactionHistoryLst
                .Select(t =>
                {
                    var totalFee = feeTransactionLst
                        .Where(fee => LazadaFeeHelper.ValidateFee(fee, _feeIgnoreList) && fee.OrderNo == t.OrderSn)
                        .Select(t => LazadaFeeHelper.ConvertTransactionFeeToFeeJson(t, _feeIgnoreList))
                        .Sum(t => t.Value);
                    t.Amount = t.Amount + (decimal)totalFee;
                    return t;
                }).ToList();
            insertOrUpdateLst.AddRange(insertTransactionHistoryLst);
            if (insertOrUpdateLst.Count > 0)
            {
                await _paymentTransactionService.AddOrUpdate(retailerId, insertOrUpdateLst);
            }
        }
    }
}

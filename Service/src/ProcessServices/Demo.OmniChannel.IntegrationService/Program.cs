﻿using System;
using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Exceptions;

namespace Demo.OmniChannel.IntegrationService
{
    public static class Program
    {
        public static readonly string Namespace = typeof(Program).Namespace;
        public static readonly string AppName =
            Namespace.Substring(Namespace.LastIndexOf('.', Namespace.LastIndexOf('.') - 1) + 1);

        public static void Main(string[] args)
        {
            var configuration = GetConfiguration();
            Log.Logger = CreateSerilogLogger(configuration);
            var host = CreateWebHostBuilder(configuration, args);
            try
            {
                Log.Information("IntegrationService task starting console...");
                host.Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "IntegrationService corrupted", AppName);
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static IConfiguration GetConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            return builder.Build();
        }

        private static ILogger CreateSerilogLogger(IConfiguration configuration)
        {
            return new LoggerConfiguration()//NOSONAR
                .Enrich.FromLogContext()
                .Enrich.WithProperty("ApplicationContext", AppName)
                .Enrich.WithExceptionDetails()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
        }


    

        public static IWebHostBuilder CreateWebHostBuilder(IConfiguration configuration, string[] args) =>
            WebHost.CreateDefaultBuilder(args)//NOSONAR
                .ConfigureAppConfiguration(x => x.AddConfiguration(configuration))
                .ConfigureLogging(logging =>
                {
                    logging.AddSerilog();
                })
                .UseUrls(Environment.GetEnvironmentVariable("ASPNETCORE_URLS") ?? "http://localhost:5004/")
                .UseStartup<Startup>();
    }
}

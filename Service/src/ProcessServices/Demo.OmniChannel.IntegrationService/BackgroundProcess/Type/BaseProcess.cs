﻿using Microsoft.Extensions.Logging;
using ServiceStack.Caching;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.IntegrationService.BackgroundProcess.Type
{
    public abstract class BaseProcess
    {
        protected readonly ILogger Logger;
        protected IAppSettings Settings;
        protected ICacheClient CacheClient;

        protected BaseProcess(
          ILogger logger,
          IAppSettings settings,
          ICacheClient cacheClient
          )
        {
            Logger = logger;
            Settings = settings;
            CacheClient = cacheClient;
        }
    }
}

﻿using Demo.OmniChannel.Infrastructure.EventBus.Event;
using Demo.OmniChannel.MongoService.Interface;
using System.Threading.Tasks;
using System;
using Microsoft.Extensions.Logging;
using Demo.OmniChannel.ChannelClient.Common;
using System.Collections.Generic;
using Demo.OmniChannel.ChannelClient.Impls;
using ServiceStack.Configuration;
using System.Linq;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Business.Interfaces;
using Newtonsoft.Json;
using ServiceStack;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee;
using ServiceStack.Caching;

namespace Demo.OmniChannel.IntegrationService.BackgroundProcess.Type
{
    public class TiktokSyncProductFromOrderProcess : BaseProcess
    {
        private readonly IOrderMongoService _orderMongoService;
        private readonly IProductMongoService _productMongoService;
        private readonly IOmniChannelAuthService _channelAuthService;
        private readonly IChannelBusiness _channelBusiness;
        private readonly IOmniChannelPlatformService _omniChannelPlatformService;

        public TiktokSyncProductFromOrderProcess(
            ILogger<TiktokSyncProductFromOrderProcess> logger,
            IAppSettings settings,
            IOrderMongoService orderMongoService,
            IProductMongoService productMongoService,
            IChannelBusiness channelBusiness,
            IOmniChannelAuthService omniChannelAuthService,
            IOmniChannelPlatformService omniChannelPlatformService,
            ICacheClient cacheClient)
            : base(logger, settings, cacheClient)
        {
            _orderMongoService = orderMongoService;
            _productMongoService = productMongoService;
            _channelBusiness = channelBusiness;
            _channelAuthService = omniChannelAuthService;
            _omniChannelPlatformService = omniChannelPlatformService;
        }

        /// <summary>
        /// Handle sync Product
        /// </summary>
        /// <param name="event"></param>
        /// <returns></returns>
        public async Task HandleSyncProductFromOrder(TiktokSyncProductFromErrorOrder @event)
        {
            var currenDate = DateTime.Now.AddMinutes(-10);
            var loggerExtension = new LogObjectMicrosoftExtension(Logger, @event.Id)
            {
                Action = "TiktokSyncProductFromOrder",
                RetailerId = @event.RetailerId,
            };

            var listProduct = new List<MongoDb.Product>();

            var lastsync = GetLastSync(@event.ChannelId);

            var orderError = await _orderMongoService.GetByChannelId(@event.RetailerId,
                @event.ChannelId, lastsync);

            if (orderError == null || !orderError.Any())
            {
                ResetLastsync(@event.ChannelId, currenDate);
                return;
            }

            var products = await GetProducts(orderError, @event.RetailerId, @event.ChannelId);

            var channel = await _channelBusiness.GetChannel(@event.ChannelId, @event.RetailerId, @event.Id);

            var platform = await _omniChannelPlatformService.GetById(channel?.PlatformId ?? 0);

            var channelAuth = await _channelAuthService.GetChannelAuth(
                new Domain.Model.OmniChannel { RetailerId = @event.RetailerId, Id = @event.ChannelId },
                @event.Id);

            if (channel == null || channelAuth == null)
            {
                ResetLastsync(@event.ChannelId, currenDate);
                return;
            }

            foreach (var order in orderError)
            {
                var orderDetail = order.OrderDetails;
                if (orderDetail == null) continue;
                var genProduct = await GenProducts(order, products, listProduct,
                    platform, channelAuth, channel);
                listProduct.AddRange(genProduct);
            }

            if (listProduct.Any())
            {
                await _productMongoService.BatchAddAsync(listProduct);
                PushKafkaAutoMapping(channel, listProduct);
            }

            loggerExtension.ResponseObject = listProduct.Select(x => x.ItemId);
            loggerExtension.LogInfo();
            ResetLastsync(@event.ChannelId, currenDate);
        }

        #region Private method
        /// <summary>
        /// Generate product 
        /// </summary>
        /// <param name="order"></param>
        /// <param name="products"></param>
        /// <param name="productAdd"></param>
        /// <param name="platform"></param>
        /// <param name="channelAuth"></param>
        /// <param name="channel"></param>
        /// <returns></returns>
        private async Task<List<MongoDb.Product>> GenProducts(MongoDb.Order order,
          List<MongoDb.Product> products,
          List<MongoDb.Product> productAdd,
          ChannelClient.Models.Platform platform,
          TokenResponse channelAuth,
          Domain.Model.OmniChannel channel)
        {
            var tikTokClient = new TikTokClient(Settings);
            var listProduct = new List<MongoDb.Product>();
            foreach (var orderDetail in order.OrderDetails)
            {
                var checkExist = products.FirstOrDefault(x => x.ItemId.ToString() == orderDetail.StrProductChannelId);
                if (checkExist != null) continue;

                if (platform == null || channelAuth == null) continue;
                if (productAdd.FirstOrDefault(x => x.StrItemId == orderDetail.StrProductChannelId) != null) continue;

                var productDetails = await tikTokClient.GetProductDetail(new ChannelAuth
                {
                    AccessToken = channelAuth.AccessToken,
                    ShopId = channelAuth.ShopId,
                }, channel.Id, Guid.NewGuid(), orderDetail.StrProductChannelId,
                    orderDetail.StrParentChannelProductId, orderDetail.ProductChannelSku, platform);

                if (productDetails == null || productDetails.ProductDetails[0].Status != "active") continue;

                var variations = productDetails.ProductDetails[0].Variations?.ConvertTo<List<TiktokProductDetailsSku>>();

                if (variations == null || !variations.Any() || !variations.Any(v => v.Id == orderDetail.StrProductChannelId))
                    continue;

                var product = new MongoDb.Product
                {
                    BranchId = order.BranchId,
                    CommonItemId = orderDetail.StrProductChannelId,
                    CommonParentItemId = orderDetail.StrParentChannelProductId,
                    ItemSku = productDetails.ProductDetails[0].Variations.Select(x => x.ConvertTo<TiktokProductDetailsSku>())
                        .FirstOrDefault(x => x.Id == orderDetail.StrProductChannelId)?.SellerSku,
                    ItemName = orderDetail.ProductChannelName,
                    ItemImages = productDetails.ProductDetails[0].Images,
                    RetailerId = order.RetailerId,
                    ChannelId = order.ChannelId,
                    Type = (byte)ChannelProductType.Variation,
                    ChannelType = (byte)ChannelTypeEnum.Tiktok
                };
                listProduct.Add(product);
            }
            return listProduct;
        }

        /// <summary>
        /// Push product to Kafka
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="products"></param>
        private void PushKafkaAutoMapping(Domain.Model.OmniChannel channel,
            List<MongoDb.Product> products)
        {
            var changedItemIds = products.Where(x => x.ChannelId == channel.Id).Select(x => x.CommonItemId).ToList();
            var mappingRequest = new MappingRequest
            {
                ChannelId = channel.Id,
                ChannelType = channel.Type,
                RetailerId = channel.RetailerId,
                BranchId = channel.BranchId,
                IsIgnoreAuditTrail = true,
                LogId = Guid.NewGuid(),
                ItemIds = changedItemIds,
                MessageFrom = "Resync-NewMapping"
            };
            var kafkaMessage = new KafkaMessage("MappingRequest",
                $"{mappingRequest.RetailerId}_{mappingRequest.BranchId}_{mappingRequest.ChannelId}",
                mappingRequest);

            KafkaClient.KafkaClient.Instance.PublishMessage(kafkaMessage.TopicName, kafkaMessage.Key,
                JsonConvert.SerializeObject(kafkaMessage), isV2: true);
        }

        /// <summary>
        /// Get list product 
        /// </summary>
        /// <param name="orderError"></param>
        /// <param name="retailerId"></param>
        /// <param name="channelId"></param>
        /// <returns></returns>
        private async Task<List<MongoDb.Product>> GetProducts(List<MongoDb.Order> orderError,
            int retailerId, long channelId)
        {
            var getItemIds = orderError.Where(x => x.OrderDetails != null).SelectMany(x => x.OrderDetails)
                .Select(x => x.StrProductChannelId).Distinct().ToList();

            if (getItemIds.Any() && getItemIds.Count < 1000)
            {
                return await _productMongoService.GetByStrItemId(retailerId, channelId, getItemIds);
            }

            return await _productMongoService.GetByChanneId(retailerId, channelId);
        }


        /// <summary>
        /// Reset lastsync sync product
        /// </summary>
        /// <param name="channelId"></param>
        /// <param name="currentDate"></param>
        private void ResetLastsync(long channelId, DateTime currentDate)
        {
            var key = string.Format(ShareKernel.Common.KvConstant.CacheTiktokSyncProductOrder, channelId);
            var value = new TiktokSyncProductFromErrorDto(currentDate, false);
            CacheClient.Set(key, value, TimeSpan.FromDays(1));
        }


        /// <summary>
        /// Get lastsync
        /// </summary>
        /// <param name="channelId"></param>
        /// <returns></returns>
        private DateTime GetLastSync(long channelId)
        {
            var key = string.Format(ShareKernel.Common.KvConstant.CacheTiktokSyncProductOrder, channelId);
            var cacheTiktokSyncProductFromError = CacheClient.Get<TiktokSyncProductFromErrorDto>(key);

            var lastsync = cacheTiktokSyncProductFromError != null
                ? cacheTiktokSyncProductFromError.LastSync
                : DateTime.Now.AddDays(-3);

            var value = new TiktokSyncProductFromErrorDto(lastsync, true);
            CacheClient.Set(key, value, TimeSpan.FromDays(1));

            return lastsync;
        }
        #endregion
    }
}

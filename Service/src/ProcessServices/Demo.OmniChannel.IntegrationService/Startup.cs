﻿using Demo.OmniChannel.Business;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.DCControl.ConfigureService;
using Demo.OmniChannel.Infrastructure.EventBus.Extentions;
using Demo.OmniChannel.Infrastructure.IoC;
using Demo.OmniChannel.IntegrationService.Consumer;
using Demo.OmniChannel.IntegrationService.Extensions;
using Demo.OmniChannel.IntegrationService.Impls;
using Demo.OmniChannel.KafkaClient;
using Demo.OmniChannel.MongoService.Common;
using Demo.OmniChannel.MongoService.Impls;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.Services.Impls;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.Services.LogginConfiguration;
using Demo.OmniChannel.ShareKernel.Auth;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Impls;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.Messaging.Redis;
using ServiceStack.OrmLite;
using ServiceStack.Redis;
using System;
using System.Linq;
using System.Reflection;

namespace Demo.OmniChannel.IntegrationService
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            var builder = new ConfigurationBuilder()
                .AddConfiguration(configuration)
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            #region Register ServiceStack License
            Licensing.RegisterLicense(Configuration.GetSection("servicestack:license").Value);
            #endregion register
            services.AddSingleton<IAppSettings>(x => new NetCoreAppSettings(Configuration));

            #region Redis
            var redisConfig = new KvRedisConfig();
            Configuration.GetSection("redis:cache").Bind(redisConfig);
            services.AddSingleton(sc => KvRedisPoolManager.GetClientsManager(redisConfig));
            services.AddSingleton<ICacheClient, KvCacheClient>();
            services.AddSingleton<IKvLockRedis, KvLockRedis>();

            var redisMqConfig = new KvRedisConfig();
            Configuration.GetSection("redis:message").Bind(redisMqConfig);
            var redisFactory = KvRedisPoolManager.GetClientsManager(redisMqConfig);
            var mqHost = new RedisMqServer(redisFactory, retryCount: 2);
            services.AddSingleton<IMessageService>(mqHost);
            services.AddSingleton<IMessageFactory>(c => new RedisMessageFactory(redisFactory));
            services.AddSingleton(redisMqConfig);
            #endregion

            #region Register DatabaseConnectionString
            var kvSql = new KvSqlConnectString();
            Configuration.GetSection("SqlConnectStrings").Bind(kvSql);
            var connectFactory = new OrmLiteConnectionFactory(kvSql.KvChannelEntities, SqlServer2016Dialect.Provider);
            connectFactory.RegisterConnection(nameof(kvSql.KvMasterEntities), kvSql.KvMasterEntities, SqlServer2016Dialect.Provider);
            foreach (var item in kvSql.KvEntitiesDC1)
            {
                connectFactory.RegisterConnection(item.Name.ToLower(), item.Value, SqlServer2016Dialect.Provider);
            }

            OrmLiteConfig.DialectProvider.GetStringConverter().UseUnicode = true;
            services.AddSingleton(kvSql);
            services.AddSingleton<IDbConnectionFactory>(c => connectFactory);
            #endregion

            #region Register Mongo
            services.Configure<MongoDbSettings>(Configuration.GetSection("mongodb"));
            services.UsingMongoDb(Configuration);
            services.AddTransient<IPaymentTransactionService, PaymentTransactionService>();
            services.AddTransient<ILazadaFeePaymentTransactionHistoryService, LazadaFeePaymentTransactionHistoryService>();
            services.AddTransient<IShopeePaymentTransactionHistoryService, ShopeePaymentTransactionHistoryService>();
            services.AddTransient<IOrderMongoService, OrderMongoService>();
            services.AddTransient<IProductMongoService, ProductMongoService>();
            #endregion

            #region Register kafka
            var kafka2 = new Kafka();
            Configuration.GetSection("Kafka2").Bind(kafka2);
            KafkaClient.KafkaClient.Instance.SetKafkaProducerConfig(kafka2, true);
            KafkaClient.KafkaClient.Instance.InitProducer(true);
            MissingMessageSingleton.Instance.LoopDequeue();
            #endregion

            #region Register RabbitMQ
            services.AddConfigEventBus(Configuration);
            services.AddHostedService<EventBusConsumerService>();
            services.AddEventHandlers();
            services.AddProcess();
            services.AddEventBus();
            #endregion

            #region Register Channel Third Party
            services.AddSingleton(c => new ChannelClient.Impls.ChannelClient(c.GetService<IAppSettings>()));
            #endregion

            services.AddScoped<IOrderInternalClient>(c => new OrderInternalClient(c.GetService<IAppSettings>()));
            services.AddScoped<IDeliveryInfoInternalClient>(c => new DeliveryInfoInternalClient(c.GetService<IAppSettings>()));
            services.AddScoped<IExecutionContextService, ExecutionContextService>();
            services.AddScoped<IAuditTrailInternalClient>(c => new AuditTrailInternalClient(c.GetService<IAppSettings>()));
            services.AddScoped<IInvoiceInternalClient>(c => new InvoiceInternalClient(c.GetService<IAppSettings>()));
            services.AddScoped<IOmniChannelAuthService, OmniChannelAuthService>();
            services.AddScoped<IChannelBusiness, ChannelBusiness>();
            services.AddScoped<IOmniChannelSettingService, OmniChannelSettingService>();
            services.AddScoped<IOmniChannelPlatformService, OmniChannelPlatformService>();
            services.AddScoped<IOmniChannelWareHouseService, OmniChannelWareHouseService>();
            services.AddScoped<IChannelClient>(c => new Demo.OmniChannel.ChannelClient.Impls.ChannelClient(c.GetService<IAppSettings>()));

            services.AddScoped<IMicrosoftLoggingContextExtension, MicrosoftLoggingContextExtension>();
            services.AddScoped<IScheduleService>(c => new ScheduleService.Impls.ScheduleService(c.GetRequiredService<IRedisClientsManager>(), c.GetService<IAppSettings>()));

            if (redisMqConfig.EventMessages.Any())
            {
                var lsName = redisMqConfig.EventMessages.Where(t => t.IsActive).Select(v => v.Name);
                var types = typeof(BaseService<>).GetTypeInfo().Assembly.DefinedTypes
                    .Where(p => p.GetTypeInfo().IsAssignableFrom(p.AsType()) && p.IsClass && lsName.Contains(p.Name)).Select(p => p.AsType());
                try
                {
                    foreach (var type in types)
                    {
                        services.AddTransient(typeof(IHostedService), type);
                    }
                }
                catch (Exception e)
                {
                    Log.Logger.Error($"Register {nameof(IHostedService)} error: {e.Message}", e);
                    throw;
                }
            }

            services.AddScoped(c =>
            {
                var eventContextSvc = c.GetRequiredService<IExecutionContextService>();

                if (eventContextSvc.Context == null)
                {
                    return new ExecutionContext();
                }

                var context = new ExecutionContext
                {
                    Id = eventContextSvc.Context.Id,
                    RetailerCode = eventContextSvc.Context.RetailerCode,
                    BranchId = eventContextSvc.Context.BranchId,
                    User = eventContextSvc.Context.User,
                    RetailerId = eventContextSvc.Context.RetailerId,
                    GroupId = eventContextSvc.Context.GroupId,
                    Group = eventContextSvc.Context.Group
                };
                return context;
            });
            SetEncryptPassPhrase();

            services.RegisterApplicationDcControl(new ApplicationDcInputOptions
            {
                ServiceName = "IntegrationService",
                AppSettings = new NetCoreAppSettings(Configuration)
            });
        }

        private void SetEncryptPassPhrase()
        {
            CryptoHelper.PassPhrase = Configuration.GetSection("EncryptPassPhrase").Value;

            if (string.IsNullOrWhiteSpace(CryptoHelper.PassPhrase))
            {
                throw new KvException("Setting EncryptPassPhrase invalid. Check setting file, please.");
            }
        }

        public void Configure(
            IHostApplicationLifetime lifetime, IMessageService messageService)
        {
            lifetime.ApplicationStarted.Register(messageService.Start);
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using ServiceStack.FluentValidation;
using Demo.OmniChannelCore.Api.Sdk.Models;

namespace Demo.OmniChannel.OrderService.Validators
{
    public class CreateOrderValidator : AbstractValidator<Order>
    {
        public CreateOrderValidator()
        {
            ValidateProperties();
        }

        protected void ValidateProperties()
        {
            RuleFor(s => s)
                .Custom((order, context) =>
                {
                    if (!ValidateCustomer(order.CustomerId))
                    {
                        context.AddFailure("Khách hàng yêu cầu bắt buộc");
                    }

                    if (!ValidateCode(order.Code))
                    {
                        context.AddFailure("Mã đơn hàng yêu cầu bắt buộc");
                    }

                    if (!ValidatePriceBook(order.Extra))
                    {
                        context.AddFailure("Bảng giá yêu cầu bắt buộc");
                    }

                    if (!ValidateBranch(order.BranchId))
                    {
                        context.AddFailure("Chi nhánh yêu cầu bắt buộc");
                    }

                    if (order.OrderDetails == null || !ValidateOrderDetail(order.OrderDetails?.ToList()))
                    {
                        context.AddFailure("Chi tiết đơn hàng yêu cầu bắt buộc");
                    }
                });
        }

        private bool ValidateCustomer(long? customerId)
        {
            if (customerId == 0) return false;
            return true;
        }

        private bool ValidateCode(string code)
        {
            if (string.IsNullOrEmpty(code)) return false;
            return true;
        }

        private bool ValidatePriceBook(string extra)
        {
            if (string.IsNullOrEmpty(extra)) return false;
            return true;
        }

        private bool ValidateBranch(int branchId)
        {
            if (branchId == 0) return false;
            return true;
        }

        private bool ValidateOrderDetail(List<OrderDetail> orderDetails)
        {
            if (orderDetails == null || !orderDetails.Any()) return false;
            foreach (var detail in orderDetails)
            {
                if (detail.ProductId == 0)  return false;
            }
            return true;
        }
    }
}

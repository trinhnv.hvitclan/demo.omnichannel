using System;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Demo.OmniChannel.ChannelClient.Impls;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Impls;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ChannelType = Demo.OmniChannel.Sdk.Common.ChannelType;


namespace Demo.OmniChannel.OrderService.Impls
{
    public abstract class BaseService<T> : IHostedService where T : BaseMessage
    {
        protected readonly ILogger Logger;
        protected int ThreadSize { get; set; }
        protected IAppSettings Settings;
        protected IDbConnectionFactory DbConnectionFactory;
        protected Kafka Kafka;
        protected ICacheClient CacheClient;
        protected ChannelClient.Impls.ChannelClient ChannelClient;
        protected KvRedisConfig MqRedisConfig;
        protected IMessageService MessageService;
        protected ChannelType ChannelType;
        protected BaseService(IAppSettings settings,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IMessageService mqService,
            KvRedisConfig mqRedisConfig,
            ILogger logger)
        {
            Logger = logger;
            Settings = settings;
            ChannelClient = channelClient;
            CacheClient = cacheClient;
            DbConnectionFactory = dbConnectionFactory;
            MqRedisConfig = mqRedisConfig;
            MessageService = mqService;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            Logger.LogInformation($"----------------------------------------- start Subscribe {typeof(T).Name} -----------------------------------------------");
            return Task.CompletedTask;
        }
        protected async Task DeActiveChannel(long channelId, KvInternalContext kvInternalContext, long authId, byte channelType)
        {
            var integrationInternalClient = new IntegrationInternalClient(Settings);
            await integrationInternalClient.DeactivateChannel(
                kvInternalContext.ConvertTo<KvInternalContext>(), channelId, EnumHelper.ToDescription((ChannelType)channelType));
        }
        protected abstract Task ProcessMessage(T data);
        public Task StopAsync(CancellationToken cancellationToken)
        {
            Logger.LogInformation($"----------------------------------------- Stop Subscribe {typeof(T).Name} Service -------------------------------------------------");
            return Task.CompletedTask;
        }

        protected async Task<string> GetSendoLocation(ISendoLocationMongoService sendoLocationMongoService, ICacheClient cacheClient, int districtId, string accessToken = null)
        {
            if (districtId > 0)
            {
                var location = cacheClient.Get<SendoLocation>(KvConstant.CacheSendoLocation.Fmt(districtId));
                if (location == null)
                {
                    location = (await sendoLocationMongoService.GetByDistrictId(districtId));
                    if (location == null && !string.IsNullOrEmpty(accessToken))
                    {
                        var sendoClient = new SendoClient(Settings);

                        var locationRes = await sendoClient.GetSendoLocation(accessToken);
                        var locations = locationRes.Select(x => new SendoLocation
                        {
                            DistrictId = x.DistrictId,
                            FullName = x.FullName
                        }).ToList();

                        location = locations.FirstOrDefault(x => x.DistrictId == districtId);
                        await sendoLocationMongoService.AddOrUpdate(locations);
                    }

                    if (location != null)
                    {
                        cacheClient.Set(KvConstant.CacheSendoLocation.Fmt(districtId), location, TimeSpan.FromDays(30));
                    }
                }
                return location?.FullName;
            }
            return null;
        }

        protected DateTime GetPuseChaseDate(byte channelType,
            DateTime purchaseDateInvoice,
            DateTime purchaseDateOrder,
            bool currentSaleTime)
        {
            switch (channelType)
            {
                case (byte)ChannelType.Shopee:
                case (byte)ChannelType.Tiktok:
                case (byte)ChannelType.Lazada:
                case (byte)ChannelType.Tiki:
                case (byte)ChannelType.Sendo:
                    if (DateTime.Compare(purchaseDateInvoice.AddSeconds(1), purchaseDateOrder) < 0)
                    {
                        Logger.LogWarning($"purchaseDateInvoice = {purchaseDateInvoice} less purchaseDateOrder {purchaseDateOrder} ");
                        return purchaseDateOrder.AddSeconds(1);
                    }
                    if (currentSaleTime)
                    {
                        return DateTime.Now;
                    }
                    return purchaseDateInvoice.AddSeconds(1); // add 1s do kv đang validate không trùng purchase order
                default:
                    return DateTime.Now;
            }
        }

        protected void ResetService(Exception ex)
        {
            var onRestart = Settings.Get("IsRestartError258", true);

            if (!onRestart) return;

            if (ex is SqlException)
            {
                Logger.LogWarning($"Error SqlExceptions, message {ex.Message}," +
                    $"InnerExcetpion {ex.InnerException?.Message}");

                if (ex.InnerException != null
                    && ex.InnerException.Message.Contains("Unknown error 258"))
                {
                    Logger.LogWarning("Warning reset service error Unknown error 258");
                    Environment.Exit(1);
                }
            }
        }
    }
}
﻿using Demo.Audit.Model.Message;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.RequestDTO;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.OrderService.OrderDomain;
using Demo.OmniChannel.OrderService.OrderDomain.Interfaces;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Auth;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.EventBus;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Common;
using Demo.OmniChannelCore.Api.Sdk.Impls;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.DotNet.PlatformAbstractions;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.Messaging.Rcon;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Customer = Demo.OmniChannelCore.Api.Sdk.Models.Customer;
using Extra = Demo.OmniChannel.ChannelClient.Models.Extra;
using InvoiceDelivery = Demo.OmniChannelCore.Api.Sdk.Models.InvoiceDelivery;
using InvoiceSurCharges = Demo.OmniChannel.MongoDb.InvoiceSurCharges;
using LazadaStatus = Demo.OmniChannel.ChannelClient.Common.LazadaStatus;
using Order = Demo.OmniChannelCore.Api.Sdk.Models.Order;
using OrderDetail = Demo.OmniChannelCore.Api.Sdk.Models.OrderDetail;
using PartnerDelivery = Demo.OmniChannelCore.Api.Sdk.Models.PartnerDelivery;
using PriceBook = Demo.OmniChannel.ChannelClient.Models.PriceBook;
using ProductMapping = Demo.OmniChannel.MongoDb.ProductMapping;
using ShopeeStatus = Demo.OmniChannel.ChannelClient.Common.ShopeeStatus;
using SurCharge = Demo.OmniChannelCore.Api.Sdk.Models.SurCharge;

namespace Demo.OmniChannel.OrderService.Impls
{
    public class CreateOrderService<T> : BaseService<T> where T : CreateOrderMessage
    {
        private readonly IIntegrationEventService _integrationEventService;
        private readonly ICustomerInternalClient _customerInternalClient;
        private readonly IOrderInternalClient _orderInternalClient;
        private readonly IPartnerDeliveryInternalClient _partnerInternalClient;
        private readonly IProductMappingService _productMappingService;
        private readonly IOrderMongoService _orderMongoService;
        private readonly IInvoiceMongoService _invoiceMongoService;
        private readonly IRetryOrderMongoService _retryOrderMongoService;
        private readonly IRetryInvoiceMongoService _retryInvoiceMongoService;
        private readonly IAuditTrailInternalClient _auditTrailInternalClient;
        private readonly IOmniChannelAuthService _channelAuthService;
        private readonly IKvLockRedis _kvLockRedis;
        private readonly IChannelBusiness _channelBusiness;
        private readonly ISendoLocationMongoService _sendoLocationMongoService;
        private readonly ICacheClient _cacheClient;
        private readonly ISurChargeInternalClient _surChargeInternalClient;
        private readonly IOmniChannelSettingService _omniChannelSettingService;
        private readonly ISurChargeBranchInternalClient _surChargeBranchInternalClient;
        private readonly IOmniChannelPlatformService _omniChannelPlatformService;
        private readonly ICreateOrderDomainService _createOrderDomainService;
        private readonly IDeliveryInfoInternalClient _deliveryInfoInternalClient;


        public CreateOrderService(IAppSettings settings,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            ICustomerInternalClient customerInternalClient,
            IDeliveryInfoInternalClient deliveryInfoInternalClient,
            IOrderInternalClient orderInternalClient,
            IProductMappingService productMappingService,
            IOrderMongoService orderMongoService,
            IRetryOrderMongoService retryOrderMongoService,
            IRetryInvoiceMongoService retryInvoiceMongoService,
            IPartnerDeliveryInternalClient partnerInternalClient,
            IAuditTrailInternalClient auditTrailInternalClient,
            IMessageService messageService,
            IOmniChannelAuthService channelAuthService,
            IInvoiceMongoService invoiceMongoService,
            IKvLockRedis kvLockRedis,
            KvRedisConfig mqRedisConfig,
            IChannelBusiness channelBusiness,
            ISendoLocationMongoService sendoLocationMongoService,
            ISurChargeInternalClient surChargeInternalClient,
            IOmniChannelSettingService omniChannelSettingService,
            ISurChargeBranchInternalClient surChargeBranchInternalClient,
            ILogger logger,
            IOmniChannelPlatformService omniChannelPlatformService,
            IIntegrationEventService integrationEventService,
            ICreateOrderDomainService createOrderDomainService) : base(settings,
            channelClient,
            cacheClient,
            dbConnectionFactory,
            messageService,
            mqRedisConfig,
            logger
            )
        {
            ThreadSize = MqRedisConfig?.EventMessages.FirstOrDefault(x => !string.IsNullOrEmpty(x.Name) &&
                                                                          x.Name.Contains(this.GetType()
                                                                              .Name))?.ThreadSize ?? 10;
            messageService.RegisterHandler<T>(m =>
            {
                try
                {
                    m.Options = (int)MessageOption.None;
                    var task = Task.Run(async () => await ProcessMessage(m.GetBody()));
                    task.GetAwaiter().GetResult();
                    return Task.CompletedTask;
                }
                catch (Exception e)
                {
                    ExceptionHelper.WriteLogExceptionMq(typeof(T), m.Body, e);
                    return string.Empty;
                }
            }, ThreadSize);
            _customerInternalClient = customerInternalClient;
            _orderInternalClient = orderInternalClient;
            _productMappingService = productMappingService;
            _partnerInternalClient = partnerInternalClient;
            _orderMongoService = orderMongoService;
            _auditTrailInternalClient = auditTrailInternalClient;
            _channelAuthService = channelAuthService;
            _invoiceMongoService = invoiceMongoService;
            _retryOrderMongoService = retryOrderMongoService;
            _retryInvoiceMongoService = retryInvoiceMongoService;
            _kvLockRedis = kvLockRedis;
            _channelBusiness = channelBusiness;
            _sendoLocationMongoService = sendoLocationMongoService;
            _cacheClient = cacheClient;
            _surChargeInternalClient = surChargeInternalClient;
            _omniChannelSettingService = omniChannelSettingService;
            _surChargeBranchInternalClient = surChargeBranchInternalClient;
            _omniChannelPlatformService = omniChannelPlatformService;
            _integrationEventService = integrationEventService;
            _createOrderDomainService = createOrderDomainService;
            _deliveryInfoInternalClient = deliveryInfoInternalClient;
        }

        protected override async Task ProcessMessage(T data)
        {
            var loggerObj = new LogObjectMicrosoftExtension(Logger, Guid.NewGuid())
            {
                Action = "CreateOrder",
                RetailerId = data.RetailerId,
                RetailerCode = data.RetailerCode,
                BranchId = data.BranchId,
                OmniChannelId = data.ChannelId,
                OrderId = data.OrderId,
                ChannelType = data.ChannelType,
                ChannelTypeCode = ((ChannelTypeEnum)data.ChannelType).ToString(),
                RequestObject = data.Order
            };
            var origAction = loggerObj.Action;
            var connectStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);
            var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory,
                data.RetailerId, connectStringName, data.BranchId, Settings?.Get<int>("ExecutionContext"));
            var coreContext = context.ConvertTo<KvInternalContext>();
            try
            {
                if (data.IsDbException || data.RetryCount > 0)
                {
                    await IncreaseRetryCount(data);
                }

                var settings = await _omniChannelSettingService.GetChannelSettings(data.ChannelId, data.RetailerId);
                var channel = await _channelBusiness.GetChannel(data.ChannelId, data.RetailerId, loggerObj.Id);
                var platform = await _omniChannelPlatformService.GetById(channel.PlatformId);
                coreContext.UserId = context.User?.Id ?? 0;
                coreContext.LogId = loggerObj.Id;
                loggerObj.GroupId = context.GroupId;
                if (channel == null)
                {
                    loggerObj.Description = "Channel is empty --> ignore";
                    loggerObj.LogInfo(true);

                    return;
                }
                loggerObj.ShopId = channel.IdentityKey;
                data.PriceBookId = channel.BasePriceBookId ?? 0; // Make sure 'Bảng giá bán'
                using var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower());
                var productRepository = new ProductRepository(db);
                var customerRepository = new CustomerRepository(db);
                var posSettingRepository = new PosSettingRepository(db);
                var priceBookRepository = new PriceBookRepository(db);
                var orderRepository = new OrderRepository(db);
                var partnerRepository = new PartnerDeliveryRepository(db);
                var saleChannelRepository = new SaleChannelRepository(db);
                var productFormulaRepository = new ProductFormulaRepository(db);
                var code = ChannelTypeHelper.GetOrderPrefix(data.ChannelType);
                var existOrder =
                    await orderRepository.GetOrderByCodeAsync($"{code}_{data.OrderId}", data.RetailerId);
                if (existOrder != null && !data.IsDbException)
                {
                    await RemoveErrorOrderInMongo(data.OrderId, data.BranchId, data.ChannelId,
                        existOrder.Status == (byte)OrderState.Void);
                    await RemoveMongoRetryOrder(data.OrderId, data.BranchId, data.ChannelId, data.RetailerId);
                    if (existOrder.Status != (byte)OrderState.Void)
                    {
                        await HandleOrderWithCancelProduct(context, data, channel, existOrder, platform, loggerObj);
                        await CreateDeliveryForOrderIfNotExisted(coreContext, data, existOrder, settings, platform, loggerObj);
                        await PublishCreateInvoiceQueue(data, (byte)ChannelType, existOrder, false, loggerObj);
                        loggerObj.ResponseObject = "Pushed order to Invoice create queue";
                        loggerObj.LogInfo(true);
                        return;
                    }

                    loggerObj.Description = "Order existed";
                    loggerObj.LogInfo(true);
                    return;
                }

                if (data.IsFirstSync && (data.OrderStatus == ShopeeStatus.Cancelled ||
                                         data.OrderStatus == ShopeeStatus.Completed ||
                                         data.OrderStatus == LazadaStatus.Cancelled ||
                                         data.OrderStatus == LazadaStatus.ShippedBackSuccess ||
                                         data.OrderStatus == LazadaStatus.Delivered ||
                                         data.OrderStatus == TikiStatus.Canceled ||
                                         data.OrderStatus == TikiStatus.Closed ||
                                         data.OrderStatus == TikiStatus.Returned ||
                                         data.OrderStatus == TikiStatus.Complete))
                {
                    loggerObj.Description = $"Order is first sync and satisfies ignore statuses ({data.OrderStatus})";
                    loggerObj.LogInfo(true);
                    return;
                }

                if (existOrder != null &&
                    (existOrder.Status != (byte)OrderState.Finalized ||
                     existOrder.Status != (byte)OrderState.Ongoing ||
                     existOrder.Status != (byte)OrderState.Void) && data.IsDbException)
                {
                    try
                    {
                        await _orderInternalClient.VoidOrder(coreContext, existOrder.Id, data.RetailerId, true);
                    }
                    catch (Exception ex)
                    {
                        loggerObj.Description = "Retry failed";
                        loggerObj.ResponseObject = $"{ex.Message} - {ex.StackTrace}";
                        loggerObj.LogError(ex, true,
                            ex is OmniChannelCore.Api.Sdk.Common.KvValidateException);
                        return;
                    }
                }
                else if (existOrder != null && existOrder.Status == (byte)OrderState.Void)
                {
                    var ex = new OmniException("Retry failed: Order is void");
                    loggerObj.LogError(ex, true);
                    return;
                }
                var posSetting = new PosSetting(await posSettingRepository.GetSettingAsync(data.RetailerId),
                    data.RetailerId, DbConnectionFactory, CacheClient, Settings);
                if (!posSetting.SellAllowOrder)
                {
                    var log = new AuditTrailLog
                    {
                        FunctionId = AuditTrailHelper.GetAuditTrailFunctionType((byte)ChannelType),
                        Action = (int)AuditTrailAction.OrderIntergate,
                        CreatedDate = DateTime.Now,
                        BranchId = context.BranchId,
                        Content = "Không đồng bộ đơn đặt hàng do chưa bật tính năng đặt hàng"
                    };
                    await _auditTrailInternalClient.AddLogAsync(coreContext, log);
                    var ex = new OmniException("Turn off SellAlowOrder");
                    loggerObj.LogError(ex, true);
                    return;
                }

                var client = ChannelClient.GetClient((byte)ChannelType, Settings);

                var auth = await _channelAuthService.GetChannelAuth(channel, data.LogId);
                if (auth == null || ((byte)ChannelType == (byte)Sdk.Common.ChannelType.Lazada &&
                                     string.IsNullOrEmpty(auth.AccessToken)))
                {
                    loggerObj.Description = "Token is Expired";
                    var ex = new OmniException("Token is Expired");
                    loggerObj.LogError(ex, true);
                    return;
                }

                var authKey = new ChannelAuth
                {
                    AccessToken = auth.AccessToken,
                    RefreshToken = auth.RefreshToken,
                    ShopId = auth.ShopId
                };
                //lấy đơn hàng từ sàn
                var orderDetailResponse = await client.GetOrderDetail(data.RetailerId,
                    data.ChannelId, authKey,
                    new CreateOrderRequest
                    {
                        BranchId = data.BranchId,
                        OrderId = data.OrderId,
                        Order = data.Order,
                        RetryCount = data.RetryCount,
                        PriceBookId = data.PriceBookId
                    }, loggerObj, platform, settings);

                if (orderDetailResponse.IsRetryOrder)
                {
                    await AddOrUpdateRetryOrder(data);

                    var ex = new OmniException("Create order failed - Is retry order. Check action failed to know more info in client get order detail");
                    loggerObj.LogError(ex);
                    return;
                }
                if (orderDetailResponse.IsIgnore)
                {
                    loggerObj.LogWarning("Order isIgnore, clear order in retry order");
                    await RemoveMongoRetryOrder(data.OrderId, data.BranchId, data.ChannelId, data.RetailerId);
                    return;
                }

                if (!orderDetailResponse.IsSuccess || orderDetailResponse.Order == null)
                {
                    var ex = new OmniException("Create order failed Because get order detail failed - isSuccess = false. Check action failed to know more info in client get order detail");
                    loggerObj.LogError(ex);
                    return;
                }

                var kvInternalContext = new KvInternalContext
                {
                    LogId = loggerObj.Id,
                    BranchId = data.BranchId,
                    RetailerId = context.RetailerId,
                    RetailerCode = context.RetailerCode,
                    UserId = context?.User?.Id ?? 0,
                    GroupId = context.GroupId
                };
                var order = orderDetailResponse.Order;
                order.CustomerLocation = await GetSendoLocation(_sendoLocationMongoService, _cacheClient,
                    order.OrderDelivery.ReceiverDistrictId, authKey.AccessToken);

                if (order?.OrderDelivery != null)
                {
                    if (string.IsNullOrEmpty(order.OrderDelivery.LocationName) &&
                        !string.IsNullOrEmpty(order.CustomerLocation))
                    {
                        order.OrderDelivery.LocationName = order.CustomerLocation;
                    }

                    if (string.IsNullOrEmpty(order.OrderDelivery.WardName) &&
                        !string.IsNullOrEmpty(order.CustomerWard))
                    {
                        order.OrderDelivery.WardName = order.CustomerWard;
                    }

                    GetKvLocationWard(order);
                }

                //Get Surcharge
                var lstInvoiceSurcharge = new List<OmniChannelCore.Api.Sdk.Models.InvoiceSurCharges>();
                if (order.SurCharges != null && order.SurCharges.Count > 0)
                {
                    foreach (var itemSurCharge in order.SurCharges)
                    {
                        var codeByGen =
                            GenerateSurchargeCode(data.ChannelType, data.RetailerId, itemSurCharge.Type ?? 0);
                        var surcharge =
                            await _surChargeInternalClient.GetSurchargeByCode(coreContext, codeByGen);

                        if (surcharge != null && !surcharge.isActive)
                        {
                            continue;
                        }

                        if (surcharge == null)
                        {
                            var surchargeTmp = new SurCharge()
                            {
                                Code = codeByGen,
                                Name = itemSurCharge.Name,
                                Value = itemSurCharge.Value,
                                RetailerId = data.RetailerId,
                                isActive = true,
                                isAuto = false,
                                ForAllBranch = true,
                                isReturnAuto = true
                            };
                            surcharge = await _surChargeInternalClient.CreateSurCharge(coreContext, surchargeTmp);
                        }

                        var isApproveSurcharge = await CheckSurchargeActiveWithBranch(coreContext, surcharge, order.BranchId);

                        if (isApproveSurcharge)
                        {
                            //create surcharge for Order
                            var invoiceSurcharge = new OmniChannelCore.Api.Sdk.Models.InvoiceSurCharges()
                            {
                                SurchargeId = surcharge?.Id ?? 0,
                                Price = (decimal)itemSurCharge.Value,
                                RetailerId = data.RetailerId,
                                Name = surcharge?.Name ?? "",
                                SurValue = (decimal)itemSurCharge.Value
                            };

                            lstInvoiceSurcharge.Add(invoiceSurcharge);
                        }
                    }
                }
                List<ProductMapping> mappings = await HandleMapping(data, order);
                var kvProductIds = order.OrderDetails.Where(x => x.ProductId > 0).Select(x => x.ProductId)
                    .ToList();

                var kvProducts =
                    await productRepository.GetProductByIds(data.RetailerId, data.BranchId, kvProductIds);

                //kiểm tra active tính năng tự động đồng bộ hàng lô date
                //trả về danh sách chi tiết đơn hàng
                if (settings.IsAutoSyncBatchExpire)
                {
                    order.OrderDetails = DetechHasExpire.DetectOrderDetailHasBatchExpire(kvProducts, order.OrderDetails);
                }

                //Lấy thông tin khách hàng (Nếu chưa có tạo mới khách hàng)
                var customer = await _createOrderDomainService.CreateCustomerAsync(order, customerRepository,
                    data, posSetting?.ManagerCustomerByBranch ?? false, coreContext, loggerObj);
                if (customer.isError)
                {
                    await WriteLogErrNotCreateCustomer(data, order, kvProducts, context, auth, loggerObj);
                    return;
                }
                if (customer.isRetailerExpireException)
                {
                    await DeActiveChannel(data.ChannelId, coreContext.ConvertTo<Sdk.Common.KvInternalContext>(), auth.Id, data.ChannelType); return;
                }
                order.UpdateCustomerCodeAndName(customer.newCustomer.Id, customer.newCustomer.Code,
                    customer.newCustomer.Name, customer.newCustomer.ContactNumber);


                var isValidateProduct = await ValidateProduct(kvProducts,
                    order, data.ChannelId,
                    data.OrderId, data.ChannelType, context, mappings, auth.Id, data.LogId, loggerObj);
                if (!isValidateProduct)
                {
                    await RemoveMongoRetryOrder(data.OrderId, data.BranchId, data.ChannelId, data.RetailerId);
                    return;
                }

                var saleChannel =
                    await saleChannelRepository.GetByOmmiChannelId(data.RetailerId, data.ChannelId);
                order.SaleChannelId = saleChannel?.Id;

                var partnerDelivery = await partnerRepository.SingleAsync(p =>
                    p.RetailerId == data.RetailerId &&
                    p.Code.ToLower() == order.OrderDelivery.PartnerDelivery.Code.ToLower());

                if (partnerDelivery == null && order.OrderDelivery?.PartnerDelivery != null)
                {
                    var newPartner = order.OrderDelivery.PartnerDelivery;
                    newPartner.RetailerId = data.RetailerId;
                    newPartner.IsOmniChannel = true;
                    try
                    {
                        var partnerId = await _partnerInternalClient.CreatePartner(kvInternalContext,
                            newPartner.ConvertTo<PartnerDelivery>());
                        order.OrderDelivery.DeliveryBy = partnerId;
                        order.OrderDelivery.PartnerDelivery = null;
                    }
                    catch (Exception e)
                    {
                        if (e is KvRetailerExpireException)
                        {
                            loggerObj.Action = "CreatePartnerFailed";
                            loggerObj.LogError(e);
                            await DeActiveChannel(data.ChannelId,
                                kvInternalContext.ConvertTo<Sdk.Common.KvInternalContext>(), auth.Id,
                                data.ChannelType);
                        }
                        else
                        {
                            loggerObj.Action = "CreatePartnerAndGetAgain";
                            loggerObj.LogError(e);
                            partnerDelivery = await partnerRepository.SingleAsync(p =>
                                p.RetailerId == data.RetailerId && p.Code.ToLower() ==
                                order.OrderDelivery.PartnerDelivery.Code.ToLower());
                            if (partnerDelivery != null && order.OrderDelivery != null)
                            {
                                order.OrderDelivery.DeliveryBy = partnerDelivery.Id;
                            }
                        }
                    }
                }
                else if (partnerDelivery != null && order.OrderDelivery != null)
                {
                    if (partnerDelivery.IsOmniChannel != true)
                    {
                        partnerDelivery.IsOmniChannel = true;
                        await _partnerInternalClient.UpdatePartnerOmniChannel(kvInternalContext, partnerDelivery.ConvertTo<PartnerDelivery>());
                    }
                    order.OrderDelivery.DeliveryBy = partnerDelivery.Id;
                    order.OrderDelivery.PartnerDelivery = null;
                }
                loggerObj.Action = origAction;
                loggerObj.Description = null;
                loggerObj.ResponseObject = null;

                var priceBook = await priceBookRepository.SingleAsync(p => p.RetailerId == data.RetailerId && p.Id == data.PriceBookId);

                if (priceBook != null && (priceBook.IsActive) && (priceBook.isDeleted != true) &&
                    priceBook.StartDate < DateTime.Now && priceBook.EndDate > DateTime.Now && priceBook.RetailerId > 0)
                {
                    order.Extra = new Extra
                    {
                        PriceBookId = new PriceBook
                        {
                            Id = priceBook.Id,
                            Name = priceBook.Name
                        }
                    }.ToJson();
                }
                else
                {
                    order.Extra = new Extra
                    {
                        PriceBookId = new PriceBook
                        {
                            Id = 0
                        }
                    }.ToJson();
                }



                await RemoveMongoRetryOrder(data.OrderId, data.BranchId, data.ChannelId, data.RetailerId);
                try
                {
                    var kvOrder = order.ConvertTo<Order>();
                    var kvOrderDetails = order.OrderDetails.Map(x => x.ConvertTo<OrderDetail>());
                    kvOrder.OrderDetails = kvOrderDetails;
                    kvOrder.OrderDelivery = order.OrderDelivery.ConvertTo<InvoiceDelivery>();

                    kvOrder.InvoiceWarranties = await _createOrderDomainService.GetInvoiceWarranties(coreContext,
                        kvProducts, productFormulaRepository,
                        order.OrderDetails.Select(x => new MongoDb.OrderDetail { ProductId = x.ProductId, Uuid = x.Uuid }).ToList(),
                        kvOrder.PurchaseDate);
                    kvOrderDetails.ForEach(x => x.UpdatePropetiesUseWarranty(kvOrder.InvoiceWarranties));

                    if (lstInvoiceSurcharge != null && lstInvoiceSurcharge.Count > 0)
                    {
                        kvOrder.OrderSurCharges = lstInvoiceSurcharge;
                        kvOrder.Surcharge = lstInvoiceSurcharge.Sum(x => x.Price);
                    }
                    var result = await _orderInternalClient.CreateOrder(
                        kvInternalContext,
                        kvOrder,
                        kvOrderDetails,
                        order.OrderDelivery.ConvertTo<InvoiceDelivery>());

                    if (result == null || result.Id <= 0)
                    {
                        loggerObj.Action = "CreateOrderNotSuccess";
                        loggerObj.ResponseObject = kvOrder;
                        var e = new KvException("Tạo đơn hàng không thành công");
                        loggerObj.LogError(e);
                        loggerObj.Action = origAction;
                        loggerObj.ResponseObject = null;
                        throw e;
                    }

                    loggerObj.Action = "CreateOrderSuccess";
                    loggerObj.ResponseObject = kvOrder;
                    loggerObj.LogInfo();
                    loggerObj.Action = origAction;

                    await RemoveErrorOrderInMongo(data.OrderId, data.BranchId, data.ChannelId,
                        order.NewStatus == (byte)OrderState.Void);
                    if (result != null && result.Id > 0)
                    {
                        var existRetryOrder = await _retryOrderMongoService.GetByOrderId(data.OrderId,
                            data.ChannelId, data.BranchId, data.RetailerId);
                        if (existRetryOrder != null && existRetryOrder.IsDbException)
                        {
                            await _retryOrderMongoService.RemoveAsync(existRetryOrder.Id);
                        }
                    }

                    #region Logs

                    await WriteLogForCreateOrder(data, kvOrder, order, coreContext);

                    #endregion

                    // Order is cancelled
                    if (order.NewStatus == (byte)OrderState.Void)
                    {
                        await VoidKvOrder(data, kvInternalContext, result, (byte)ChannelType, order);
                    }

                    if (order.NewStatus == (byte)OrderState.Finalized)
                    {
                        await PublishCreateInvoiceQueue(data, (byte)ChannelType,
                            result.ConvertTo<Domain.Model.Order>(), existOrder == null, loggerObj);
                        loggerObj.ResponseObject = "Pushed to Invoice create queue";
                        loggerObj.LogInfo(true);
                    }
                    loggerObj.ResponseObject = $"Do not create invoice for this order";
                    loggerObj.LogInfo(true);
                }
                catch (Exception e)
                {
                    loggerObj.LogError(e, true, e is OmniChannelCore.Api.Sdk.Common.KvValidateException);
                    if (e is KvRetailerExpireException)
                    {
                        await DeActiveChannel(data.ChannelId,
                            kvInternalContext.ConvertTo<Sdk.Common.KvInternalContext>(), auth.Id,
                            data.ChannelType);
                        return;
                    }

                    if (e is KvDbException || e is SqlException ||
                        (e.Message != null &&
                         (e.Message.ToLower().Contains(DbExceptionMessages.ConnectionTimeout) ||
                          e.Message.ToLower().Contains(DbExceptionMessages.TransactionDeadlocked))))
                    {
                        await AddOrUpdateRetryOrder(data, e is KvDbException);
                        await WriteLogCreateOrderFail(context, order, data.OrderId, data.ChannelId,
                            data.ChannelType, e.Message, auth.Id, data.LogId, loggerObj);
                    }

                    if (string.IsNullOrEmpty(e.Message) ||
                        (!e.Message.Trim()
                             .Equals(
                                 string.Format(KvOrderValidateMessage.OrderExistedVi, $"{code}_{data.OrderId}")
                                     .Trim(), StringComparison.OrdinalIgnoreCase)
                         && !e.Message.Trim()
                             .Equals(
                                 string.Format(KvOrderValidateMessage.OrderExistedEn, $"{code}_{data.OrderId}")
                                     .Trim(), StringComparison.OrdinalIgnoreCase)))
                    {
                        foreach (var detail in order.OrderDetails)
                        {
                            detail.ProductName = kvProducts.FirstOrDefault(x => x.Id == detail.ProductId)
                                ?.FullName;
                        }

                        await SaveOrderSyncError(context, order, data.OrderId, data.ChannelId,
                            (byte)ChannelType, e.Message, e.Message, auth.Id, data.LogId, loggerObj);
                    }

                    return;
                }
            }
            catch (Exception e)
            {
                if (e is KvDbException || (e.Message != null &&
                                           (e.Message.ToLower().Contains(DbExceptionMessages.ConnectionTimeout) ||
                                            e.Message.ToLower().Contains(DbExceptionMessages.TransactionDeadlocked))))
                {
                    await AddOrUpdateRetryOrder(data, e is KvDbException);
                }
                if (e is KvRetailerExpireException)
                {
                    loggerObj.LogWarning(e.Message);
                    await DeActiveChannel(data.ChannelId,
                        coreContext.ConvertTo<Sdk.Common.KvInternalContext>(), 0,
                        data.ChannelType);
                    return;
                }
                loggerObj.LogError(e, true);
            }
        }

        private async Task<Domain.Model.Customer> _GetCustomerAsync(CustomerRepository customerRepository, int retailerid, int branchId, string phone, string customerCode, bool isManagerCustomerByBranch, LogObjectMicrosoftExtension logInfo)
        {
            var loggerObj = new LogObjectMicrosoftExtension(Logger, logInfo.Id)
            {
                Action = "GetCustomerAsync",
                RetailerId = logInfo.RetailerId,
                OmniChannelId = logInfo.OmniChannelId,
                ChannelTypeCode = logInfo.ChannelTypeCode,
                RequestObject = phone
            };
            Domain.Model.Customer existsCustomer = null;
            try
            {
                existsCustomer = await customerRepository.GetByFilterAsync(retailerid, branchId, phone, customerCode, isManagerCustomerByBranch);
            }
            catch (Exception ex)
            {
                loggerObj.Description = "Error_GetByFilterAsync";
                loggerObj.LogError(ex);
            }
            return existsCustomer;
        }

        private async Task WriteLogForCreateOrder(T data, Order kvOrder, KvOrder order, KvInternalContext coreContext)
        {
            try
            {
                var productDetail = new StringBuilder();
                double total = 0;
                if (kvOrder.OrderDetails != null && kvOrder.OrderDetails.Any())
                {
                    productDetail.Append(", bao gồm:<div>");
                    foreach (var item in kvOrder.OrderDetails)
                    {
                        total += ((double)item.Price - (double)item.Discount.GetValueOrDefault()) * item.Quantity;
                        productDetail.Append(
                            $"- [ProductCode]{item.ProductCode}[/ProductCode] : {StringHelper.Normallize(item.Quantity)}*{StringHelper.NormallizeWfp((double)item.Price - (double)item.Discount.GetValueOrDefault())}<br>");
                        var productWarrantys = kvOrder.InvoiceWarranties?.Where(x => x.InvoiceDetailUuid == item.Uuid).ToList();
                        var getWarrantys = GetWarrantysLog(item, productWarrantys);
                        productDetail.Append(string.IsNullOrEmpty(getWarrantys.ToString()) ? string.Empty : $"{getWarrantys}<br>");
                    }

                    productDetail.Append("</div>");
                }

                var channelName = EnumHelper.GetNameByType<ChannelType>((byte)ChannelType);
                var log = new AuditTrailLog
                {
                    FunctionId = AuditTrailHelper.GetAuditTrailFunctionType((byte)ChannelType),
                    Action = (int)AuditTrailAction.OrderIntergate,
                    BranchId = order.BranchId,
                    CreatedDate = DateTime.Now,
                    Content =
                        $"Tạo đơn đặt hàng: [OrderCode]{kvOrder.Code}[/OrderCode] từ đơn đặt hàng {data.OrderId} trên {channelName}, giá trị: {StringHelper.NormallizeWfp(total)}, thời gian: {kvOrder.PurchaseDate:dd/MM/yyyy HH:mm:ss}{productDetail}"
                };
                await _auditTrailInternalClient.AddLogAsync(coreContext, log);
            }
            catch
            {
                // ignored
            }
        }

        private async Task IncreaseRetryCount(T data)
        {
            var existRetryOrder =
                await _retryOrderMongoService.GetByOrderId(data.OrderId, data.ChannelId, data.BranchId,
                    data.RetailerId);
            if (existRetryOrder != null)
            {
                existRetryOrder.ModifiedDate = DateTime.Now;
                existRetryOrder.RetryCount = data.RetryCount + 1;
                await _retryOrderMongoService.UpdateAsync(existRetryOrder.Id, existRetryOrder);
            }

            var existRetryInvoice = await _retryInvoiceMongoService.GetByOrderId(data.OrderId,
                data.ChannelId, data.BranchId, data.RetailerId);
            if (existRetryInvoice != null)
            {
                existRetryInvoice.ModifiedDate = DateTime.Now;
                existRetryInvoice.RetryCount = data.RetryCount + 1;
                await _retryInvoiceMongoService.UpdateAsync(existRetryInvoice.Id, existRetryInvoice);
            }
        }

        private async Task AddOrUpdateRetryOrder(T data, bool isDbException = false)
        {
            var existRetryOrder =
                await _retryOrderMongoService.GetByOrderId(data.OrderId, data.ChannelId, data.BranchId,
                    data.RetailerId);
            if (existRetryOrder != null)
            {
                existRetryOrder.ModifiedDate = DateTime.Now;
                existRetryOrder.RetryCount = data.RetryCount + 1;
                await _retryOrderMongoService.UpdateAsync(existRetryOrder.Id, existRetryOrder);
            }
            else
            {
                await _retryOrderMongoService.AddSync(new RetryOrder
                {
                    ChannelId = data.ChannelId,
                    ChannelType = data.ChannelType,
                    BranchId = data.BranchId,
                    KvEntities = data.KvEntities,
                    RetailerId = data.RetailerId,
                    OrderId = data.OrderId,
                    Order = null,
                    GroupId = data.GroupId,
                    PriceBookId = data.PriceBookId,
                    IsFirstSync = data.IsFirstSync,
                    OrderStatus = data.OrderStatus,
                    CreatedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    IsDbException = isDbException,
                    RetryCount = 1,
                    PlatformId = data.PlatformId
                });
            }
        }

        public virtual async Task<List<ProductMapping>> HandleMapping(T data, KvOrder order)
        {
            List<ProductMapping> mappings = null;
            var channelProductIds = order.OrderDetails.Where(x => !string.IsNullOrEmpty(x.ProductChannelId)).Select(x => x.ProductChannelId).ToList();
            if (channelProductIds.Any())
            {
                mappings =
                    await _productMappingService.GetByChannelProductIds(data.RetailerId, data.ChannelId,
                        channelProductIds,
                        isStringItemId: ConvertHelper.CheckUseStringItemId(data.ChannelType));
            }
            foreach (var detail in order.OrderDetails)
            {
                var mapping = mappings?.FirstOrDefault(x => !string.IsNullOrEmpty(detail.ProductChannelId) && x.CommonProductChannelId == detail.ProductChannelId);
                detail.ProductId = mapping?.ProductKvId ?? 0;
                detail.ProductCode = mapping?.ProductKvSku ?? string.Empty;
            }
            return mappings;
        }

        public virtual Task HandleOrderWithCancelProduct(ExecutionContext context, T data, Domain.Model.OmniChannel channel, Domain.Model.Order existOrder,
            Demo.OmniChannel.ChannelClient.Models.Platform platform,
            LogObjectMicrosoftExtension loggerObj)
        {
            return Task.CompletedTask;
        }

        private async Task PublishCreateInvoiceQueue(T data, byte channelType, Domain.Model.Order existOrder,
            bool hasCreateOrder, LogObjectMicrosoftExtension loggerObj)
        {
            if (hasCreateOrder)
            {
                // Đơn hàng mới và Hóa đơn mới tạo trong thời gian quá gần nhau gây ra trường hợp đồng bộ tồn kho cũ
                var millisecondsDelay =
                    NumberHelper.GetValueOrDefault(Settings?.Get<int>("ChannelProductLock"), 5) * 1000;
                loggerObj.LogInfo(
                    $"Delay create Invoice after: {millisecondsDelay}ms. RetailerId: {data.RetailerId}. OrderId: {existOrder.Code}",
                    true);
                await Task.Delay(millisecondsDelay);
            }

            using (var mq = MessageService.MessageFactory.CreateMessageProducer())
            {
                switch (channelType)
                {
                    case (byte)Sdk.Common.ChannelType.Lazada:
                        {
                            var createInvoice = new LazadaCreateInvoiceMessage
                            {
                                ChannelId = data.ChannelId,
                                ChannelType = data.ChannelType,
                                BranchId = existOrder.BranchId > 0 ? existOrder.BranchId : data.BranchId,
                                KvEntities = data.KvEntities,
                                RetailerId = data.RetailerId,
                                OrderId = data.OrderId,
                                Order = data.Order,
                                KvOrder = existOrder.ToSafeJson(),
                                KvOrderId = existOrder.Id,
                                LogId = data.LogId,
                                PlatformId = data.PlatformId
                            };
                            mq.Publish(createInvoice);
                            break;
                        }
                    case (byte)Sdk.Common.ChannelType.Shopee:
                        {
                            var createInvoice = new ShopeeCreateInvoiceMessage
                            {
                                ChannelId = data.ChannelId,
                                ChannelType = data.ChannelType,
                                BranchId = existOrder.BranchId > 0 ? existOrder.BranchId : data.BranchId,
                                KvEntities = data.KvEntities,
                                RetailerId = data.RetailerId,
                                OrderId = data.OrderId,
                                Order = data.Order,
                                KvOrder = existOrder.ToSafeJson(),
                                KvOrderId = existOrder.Id,
                                LogId = data.LogId,
                                PlatformId = data.PlatformId
                            };
                            mq.Publish(createInvoice);
                            break;
                        }
                    case (byte)Sdk.Common.ChannelType.Tiki:
                        {
                            var createInvoice = new TikiCreateInvoiceMessage
                            {
                                ChannelId = data.ChannelId,
                                ChannelType = data.ChannelType,
                                BranchId = existOrder.BranchId > 0 ? existOrder.BranchId : data.BranchId,
                                KvEntities = data.KvEntities,
                                RetailerId = data.RetailerId,
                                OrderId = data.OrderId,
                                Order = data.Order,
                                KvOrder = existOrder.ToSafeJson(),
                                KvOrderId = existOrder.Id,
                                LogId = data.LogId,
                                PlatformId = data.PlatformId
                            };
                            mq.Publish(createInvoice);
                            break;
                        }
                    case (byte)Sdk.Common.ChannelType.Sendo:
                        {
                            var createInvoice = new SendoCreateInvoiceMessage
                            {
                                ChannelId = data.ChannelId,
                                ChannelType = data.ChannelType,
                                BranchId = existOrder.BranchId > 0 ? existOrder.BranchId : data.BranchId,
                                KvEntities = data.KvEntities,
                                RetailerId = data.RetailerId,
                                OrderId = data.OrderId,
                                Order = data.Order,
                                KvOrder = existOrder.ToSafeJson(),
                                KvOrderId = existOrder.Id,
                                LogId = data.LogId,
                                PlatformId = data.PlatformId
                            };
                            mq.Publish(createInvoice);
                            break;
                        }
                }
            }
        }

        private async Task RemoveErrorOrderInMongo(string orderId, int branchId, long channelId,
            bool isRemoveInvoiceError)
        {
            var orderInMongo = await _orderMongoService.GetByOrderId(orderId, branchId, channelId);
            if (orderInMongo != null)
            {
                await _orderMongoService.RemoveAsync(orderInMongo.Id);
            }

            if (isRemoveInvoiceError)
            {
                var invoiceInMongo = await _invoiceMongoService.GetByOrderId(orderId, branchId, channelId);
                if (invoiceInMongo != null)
                {
                    foreach (var invoice in invoiceInMongo)
                    {
                        await _invoiceMongoService.RemoveAsync(invoice.Id);
                    }
                }
            }
        }

        private async Task RemoveMongoRetryOrder(string orderId, int branchId, long channelId, int retailerId)
        {
            var existRetryOrder = await _retryOrderMongoService.GetByOrderId(orderId, channelId, branchId, retailerId);
            if (existRetryOrder != null && !existRetryOrder.IsDbException)
            {
                await _retryOrderMongoService.RemoveAsync(existRetryOrder.Id);
            }

            var existRetryInvoice =
                await _retryInvoiceMongoService.GetByOrderId(orderId, channelId, branchId, retailerId);
            if (existRetryInvoice != null && !existRetryInvoice.IsDbException)
            {
                await _retryInvoiceMongoService.RemoveAsync(existRetryInvoice.Id);
            }
        }

        private async Task SaveOrderSyncError(ExecutionContext context, KvOrder order, string channelOrderId,
            long channelId, byte channelType, string errorMessage, string auditTrailMessage, long authId, Guid logId, LogObjectMicrosoftExtension logInfo)
        {
            var loggerObj = new LogObjectMicrosoftExtension(Logger, logInfo.Id)
            {
                Action = "SaveOrderSyncError",
                RetailerId = logInfo.RetailerId,
                OmniChannelId = logInfo.OmniChannelId,
                ChannelTypeCode = logInfo.ChannelTypeCode,
                RequestObject = channelOrderId
            };

            try
            {
                var existOrder = await _orderMongoService.GetByOrderId(channelOrderId, context.BranchId, channelId);
                var retryOrder = new MongoDb.Order
                {
                    Code = order.Code,
                    BranchId = order.BranchId,
                    SoldById = order.SoldById ?? 0,
                    SaleChannelId = order.SaleChannelId ?? 0,
                    PurchaseDate = order.PurchaseDate,
                    Description = order.Description,
                    IsSyncSuccess = false,
                    UsingCod = true,
                    OrderDelivery = order.OrderDelivery?.ConvertTo<MongoDb.InvoiceDelivery>(),
                    CustomerId = order.CustomerId ?? 0,
                    CustomerPhone = order.CustomerPhone,
                    CustomerCode = order.CustomerCode,
                    CustomerName = order.CustomerName,
                    CustomerAddress = order.CustomerAddress,
                    //OrderDetails = order.OrderDetails.Select(x => x.ConvertTo<MongoDb.OrderDetail>()).ToList(),
                    NewStatus = order.NewStatus,
                    Status = order.Status,
                    ChannelStatus = order.ChannelStatus,
                    ChannelId = channelId,
                    ErrorMessage = errorMessage,
                    OrderId = channelOrderId,
                    RetailerId = context.RetailerId,
                    CreatedDate = DateTime.Now,
                    Discount = order.Discount ?? 0,
                    OrderSurCharges = order.SurCharges?.Select(p => new MongoDb.InvoiceSurCharges
                    {
                        Name = p.Name,
                        SurValue = p.Value,
                        Price = p.Value ?? 0
                    }).ToList() ?? new List<InvoiceSurCharges>(),
                    OrderDetails = order.OrderDetails?.Select(p => new MongoDb.OrderDetail
                    {
                        ProductId = p.ProductId,
                        ProductCode = p.ProductCode,
                        ProductName = p.ProductName,
                        Price = p.Price,
                        Discount = p.Discount.GetValueOrDefault(),
                        DiscountPrice = p.DiscountPrice,
                        StrProductChannelId = p.ProductChannelId,
                        CommonProductChannelId = p.ProductChannelId,
                        DiscountRatio = p.DiscountRatio.GetValueOrDefault(),
                        Note = p.Note,
                        CommonParentChannelProductId = p.ParentChannelProductId,
                        ProductChannelName = p.ProductChannelName,
                        ProductChannelSku = p.ProductChannelSku,
                        Quantity = p.Quantity,
                        UseProductBatchExpire = p.UseProductBatchExpire,
                        UseProductSerial = p.UseProductSerial,
                        Uuid = p.Uuid
                    }).ToList()
                };
                if (existOrder == null)
                {
                    await _orderMongoService.AddSync(retryOrder);
                }
                else
                {
                    retryOrder.Id = existOrder.Id;
                    await _orderMongoService.UpdateAsync(existOrder.Id, retryOrder);
                }

                PushLishMessageErrorNotification(order, channelId, errorMessage, logInfo);

                #region Audit Trail

                await WriteLogCreateOrderFail(context, order, channelOrderId, channelId, channelType, auditTrailMessage,
                    authId, logId, loggerObj);

                #endregion
            }
            catch (Exception e)
            {
                loggerObj.LogError(e);
                throw;
            }

        }

        private async Task RemoveOrderSyncError(ExecutionContext context, string channelOrderId, long channelId, LogObjectMicrosoftExtension logInfo)
        {
            var loggerObj = new LogObjectMicrosoftExtension(Logger, logInfo.Id)
            {
                Action = "RemoveOrderSyncError",
                RetailerId = logInfo.RetailerId,
                OmniChannelId = logInfo.OmniChannelId,
                ChannelTypeCode = logInfo.ChannelTypeCode,
                RequestObject = channelOrderId
            };

            try
            {
                var existOrder = await _orderMongoService.GetByOrderId(channelOrderId, context.BranchId, channelId);
                if (existOrder != null)
                {
                    await _orderMongoService.RemoveAsync(existOrder.Id);
                    loggerObj.Description = "Remove order sync error success";
                }
                else
                {
                    loggerObj.Description = "Not found order sync error";
                }
                loggerObj.LogInfo(true);
            }
            catch (Exception e)
            {
                loggerObj.LogError(e);
                throw;
            }
        }

        private async Task WriteLogCreateOrderFail(ExecutionContext context, KvOrder order, string channelOrderId,
            long channelId, byte channelType, string auditTrailMessage, long authId, Guid logId, LogObjectMicrosoftExtension logInfo)
        {
            var loggerObj = new LogObjectMicrosoftExtension(Logger, logInfo.Id)
            {
                Action = "WriteLogCreateOrderFail",
                RetailerId = logInfo.RetailerId,
                OmniChannelId = logInfo.OmniChannelId,
                ChannelTypeCode = logInfo.ChannelTypeCode,
                RequestObject = channelOrderId
            };
            try
            {
                var coreContext = context.ConvertTo<KvInternalContext>();
                coreContext.UserId = context.User?.Id ?? 0;
                coreContext.LogId = logId;
                var log = new AuditTrailLog
                {
                    FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channelType),
                    Action = (int)AuditTrailAction.OrderIntergate,
                    CreatedDate = DateTime.Now,
                    BranchId = order.BranchId,
                    Content =
                        $"Tạo đơn đặt hàng KHÔNG thành công: [OrderCode]{order.Code}[/OrderCode] (cho đơn đặt hàng: {channelOrderId}), lý do: <br/>{auditTrailMessage}"
                };
                try
                {
                    await _auditTrailInternalClient.AddLogAsync(coreContext, log);
                }
                catch (Exception e)
                {
                    if (e is KvRetailerExpireException)
                    {
                        loggerObj.LogError(e);
                        await DeActiveChannel(channelId, context.ConvertTo<Sdk.Common.KvInternalContext>(), authId,
                            channelType);
                    }
                }
            }
            catch
            {
                // ignored
            }
        }

        private async Task VoidKvOrder(CreateOrderMessage data, KvInternalContext kvInternalContext, Order result,
            byte channelType, KvOrder order)
        {
            await _orderInternalClient.VoidOrder(kvInternalContext, result.Id);

            #region Logs

            var log = new AuditTrailLog
            {
                FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channelType),
                Action = (int)AuditTrailAction.OrderIntergate,
                CreatedDate = DateTime.Now,
                BranchId = data.BranchId,
                Content = $"Hủy đơn đặt hàng: [OrderCode]{order.Code}[/OrderCode]"
            };
            await _auditTrailInternalClient.AddLogAsync(kvInternalContext, log);

            #endregion
        }

        private async Task<bool> ValidateProduct(List<ProductBranchDTO> kvProducts, KvOrder order,
            long channelId, string orderId, byte channelType, ExecutionContext context,
            List<ProductMapping> mappings, long authId, Guid logId, LogObjectMicrosoftExtension logInfo)
        {
            var loggerObj = new LogObjectMicrosoftExtension(Logger, logInfo.Id)
            {
                Action = "CreateOrderValidateProduct",
                RetailerId = logInfo.RetailerId,
                OmniChannelId = logInfo.OmniChannelId,
                ChannelTypeCode = logInfo.ChannelTypeCode,
                RequestObject = orderId
            };
            var productLog = new StringBuilder();
            var errorMessage = new StringBuilder();
            var isValid = true;
            foreach (var detail in order.OrderDetails)
            {
                var product = kvProducts.FirstOrDefault(x => x.Id == detail.ProductId);
                detail.ProductName = product?.FullName ?? detail.ProductChannelName;
                var channelTrackKey = !string.IsNullOrEmpty(detail.ProductChannelId)
                    ? detail.ProductChannelId
                    : detail.ProductChannelSku;
                if (product == null)
                {
                    errorMessage.Append(
                        $"Hàng hóa {detail.ProductChannelName} ({channelTrackKey}) chưa liên kết với hàng hóa nào trên Demo; ");
                    productLog.AppendFormat(
                        $"- {detail.ProductChannelSku ?? detail.ProductChannelId} ({channelTrackKey}): chưa xác định trên Demo <br/>");
                    isValid = false;
                }
                else
                {
                    var mapping = mappings.FirstOrDefault(x =>
                        x.ChannelId == channelId &&
                        ((!string.IsNullOrEmpty(detail.ProductChannelId) &&
                          x.CommonProductChannelId == detail.ProductChannelId) ||
                         x.ProductChannelSku == detail.ProductChannelSku));
                    if (mapping == null)
                    {
                        errorMessage.Append(
                            $"Hàng hóa {detail.ProductChannelName} ({channelTrackKey}) chưa liên kết với hàng hóa nào trên Demo; ");
                        productLog.AppendFormat(
                            $"- [ProductCode]{detail.ProductCode}[/ProductCode]: liên kết hàng hóa không tồn tại <br/>");
                        isValid = false;
                    }
                    else if (product.isDeleted == true)
                    {
                        errorMessage.Append(
                            $"Hàng hóa {detail.ProductChannelName} ({channelTrackKey}) chưa liên kết với hàng hóa nào trên Demo; ");
                        productLog.AppendFormat($"- [ProductCode]{detail.ProductCode}[/ProductCode]: đã bị xóa <br/>");
                        isValid = false;
                    }
                }
            }

            if (!isValid)
            {
                if ((byte)OrderState.Void == order.Status || (byte)OrderState.Void == order.NewStatus)
                {
                    await RemoveOrderSyncError(context, orderId, channelId, logInfo);
                    loggerObj.Description = "Xóa thông tin đồng bộ đơn lỗi";
                }
                else
                {
                    await SaveOrderSyncError(context, order, orderId, channelId, channelType, errorMessage.ToString(),
                productLog.ToString(), authId, logId, loggerObj);
                    loggerObj.Description = "Lưu thông tin đồng bộ đơn lỗi";
                }
                loggerObj.RequestObject = order.ToSafeJson();
                loggerObj.ResponseObject = errorMessage.ToString();
                loggerObj.LogInfo(true);
                return false;
            }

            return true;
        }

        private void GetKvLocationWard(KvOrder order)
        {
            Location kvLocation = null;
            if (!string.IsNullOrEmpty(order?.OrderDelivery.ChannelState) &&
                !string.IsNullOrEmpty(order?.OrderDelivery.ChannelCity))
            {
                var state = Regex.Replace(StringHelper.ConvertToUnsign(order?.OrderDelivery.ChannelState), @"\s+", "");
                state = !string.IsNullOrEmpty(state) && state.StartsWith("TP.")
                    ? Regex.Replace(state, "TP.", "")
                    : state;
                var city = Regex.Replace(StringHelper.ConvertToUnsign(order?.OrderDelivery.ChannelCity), @"\s+", "");
                kvLocation = CacheClient.Get<Location>(string.Format(KvConstant.LocationCacheKey, $"{state}-{city}"));
                order.OrderDelivery.LocationId = kvLocation?.Id;
                if (kvLocation != null)
                {
                    order.OrderDelivery.LocationName = kvLocation.Name;
                }
            }
            else if (!string.IsNullOrEmpty(order?.OrderDelivery.LocationName))
            {
                var locationKey = Regex.Replace(StringHelper.ConvertToUnsign(order.OrderDelivery.LocationName), @"\s+",
                    "");
                kvLocation = CacheClient.Get<Location>(string.Format(KvConstant.LocationCacheKey, locationKey));
                order.OrderDelivery.LocationId = kvLocation?.Id;
            }

            if (kvLocation != null && (!string.IsNullOrEmpty(order.OrderDelivery?.ChannelWard) && kvLocation.Id > 0))
            {
                var ward = Regex.Replace(StringHelper.ConvertToUnsign(order?.OrderDelivery.ChannelWard), @"\s+", "");
                var kvWard = CacheClient.Get<Wards>(string.Format(KvConstant.WardCacheKey, kvLocation?.Id, ward));
                order.OrderDelivery.WardName = kvWard != null ? kvWard.Name : order.OrderDelivery.ChannelWard;
            }
            else
            {
                order.OrderDelivery.WardName = order.OrderDelivery.ChannelWard;
            }
        }

        private async Task WriteLogErrNotCreateCustomer(T data, KvOrder order, List<ProductBranchDTO> kvProducts,
            ExecutionContext context,
            ChannelClient.Models.TokenResponse auth, LogObjectMicrosoftExtension logInfo)
        {
            foreach (var detail in order.OrderDetails)
            {
                detail.ProductName = kvProducts.FirstOrDefault(x => x.Id == detail.ProductId)?.FullName;
            }

            var errorMessage = "Đồng bộ thông tin khách hàng thất bại.";
            var customerLog = $"- [CustomerCode]: Đồng bộ thông tin khách hàng thất bại <br/>";
            await SaveOrderSyncError(context, order, data.OrderId, data.ChannelId, data.ChannelType, errorMessage,
                customerLog,
                auth.Id, data.LogId, logInfo);
        }

        private string GenerateSurchargeCode(byte channelType, int retailerId, byte surchargeType)
        {
            var code = string.Empty;
            var prefix = "TH";
            if (surchargeType == (byte)ShopeeSurchargeType.FeeShip) prefix = "TL";

            switch (channelType)
            {
                case (byte)Sdk.Common.ChannelType.Lazada:
                    {
                        code = $"{prefix}LZD_{retailerId}";
                        break;
                    }
                case (byte)Sdk.Common.ChannelType.Shopee:
                    {
                        code = $"{prefix}SPE_{retailerId}";
                        break;
                    }
                case (byte)Sdk.Common.ChannelType.Sendo:
                    {
                        code = $"{prefix}SDO_{retailerId}";
                        break;
                    }
                case (byte)Sdk.Common.ChannelType.Tiki:
                    {
                        code = $"{prefix}TIKI_{retailerId}";
                        break;
                    }
                case (byte)Sdk.Common.ChannelType.Tiktok:
                    {
                        code = $"{prefix}TTS_{retailerId}";
                        break;
                    }
            }

            return code;
        }

        protected async Task CreateDeliveryForOrderIfNotExisted(KvInternalContext coreContext, T data, Domain.Model.Order existedOrder, OmniChannelSettingObject settings, Demo.OmniChannel.ChannelClient.Models.Platform platform, LogObjectMicrosoftExtension loggerObj)
        {
            var connectStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);
            using (var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var partnerRepository = new PartnerDeliveryRepository(db);
                var deliveryInfoRepository = new DeliveryInfoRepository(db);
                var deliveryInfoForOrder = await deliveryInfoRepository.GetLastByOrderIdAsync(coreContext.RetailerId, existedOrder.Id);
                if (deliveryInfoForOrder == null)
                {
                    var loggerDelivery = loggerObj.Clone("CreateDeliveryForOrder");
                    var channel = await _channelBusiness.GetChannel(data.ChannelId, data.RetailerId, loggerObj.Id);
                    var auth = await _channelAuthService.GetChannelAuth(channel, data.LogId);
                    var client = ChannelClient.GetClient((byte)ChannelType, Settings);

                    var authKey = new ChannelAuth
                    {
                        AccessToken = auth.AccessToken,
                        RefreshToken = auth.RefreshToken,
                        ShopId = auth.ShopId
                    };
                    var orderDetailResponse = await client.GetOrderDetail(data.RetailerId, data.ChannelId, authKey,
                   new CreateOrderRequest
                   {
                       BranchId = data.BranchId,
                       OrderId = data.OrderId,
                       Order = data.Order,
                       RetryCount = data.RetryCount,
                       PriceBookId = data.PriceBookId
                   }, loggerDelivery, platform, settings);
                    if (orderDetailResponse == null || orderDetailResponse.Order == null)
                    {
                        return;
                    }
                    var kvOrder = orderDetailResponse.Order;
                    var partnerDelivery = await partnerRepository.GetPartnerDeliveryByCodeAsync(coreContext.RetailerId, kvOrder.OrderDelivery.PartnerDelivery.Code.ToLower());
                    if (partnerDelivery == null)
                    {
                        return;
                    }
                    var deliveryInfo = new Demo.OmniChannel.ChannelClient.Models.DeliveryInfo
                    {
                        OrderId = existedOrder.Id,
                        DeliveryBy = partnerDelivery.Id,
                        UseDefaultPartner = false,
                        ServiceType = Delivery.Normal.ToString(),
                        ServiceTypeText = EnumHelper.ToDescription(Delivery.Normal),
                        Price = kvOrder?.OrderDelivery?.Price,
                        Status = kvOrder?.OrderDelivery?.Status ?? (byte)DeliveryStatus.Pending,
                        CreatedBy = existedOrder.CreatedBy,
                        CreatedDate = existedOrder.CreatedDate,
                        ModifiedBy = existedOrder.ModifiedBy,
                        ModifiedDate = existedOrder.ModifiedDate,
                        RetailerId = existedOrder.RetailerId,
                        FeeJson = kvOrder?.OrderDelivery.FeeJson
                    };

                    // DeliveryPackage
                    var deliveryPackage = new Demo.OmniChannel.ChannelClient.Models.DeliveryPackage
                    {
                        OrderId = existedOrder.Id,
                        Weight = kvOrder.OrderDelivery.Weight,
                        Length = kvOrder.OrderDelivery.Length,
                        Width = kvOrder.OrderDelivery.Width,
                        Height = kvOrder.OrderDelivery.Height,
                        Receiver = kvOrder.OrderDelivery.Receiver,
                        ContactNumber = kvOrder.OrderDelivery.ContactNumber,
                        Address = kvOrder.OrderDelivery.Address,
                        LocationId = kvOrder.OrderDelivery.LocationId,
                        LocationName = kvOrder.OrderDelivery.LocationName,
                        WardName = kvOrder.OrderDelivery.WardName,
                        UsingCod = true,
                    };
                    var deliveryInfoCore = deliveryInfo.ConvertTo<Demo.OmniChannelCore.Api.Sdk.Models.DeliveryInfo>();
                    var deliveryPackageCore = deliveryPackage.ConvertTo<Demo.OmniChannelCore.Api.Sdk.Models.DeliveryPackage>();
                    await _deliveryInfoInternalClient.CreateDeliveryForOrderAsync(coreContext, (int)existedOrder.Id, deliveryInfoCore, deliveryPackageCore);
                    loggerDelivery.Description = "Create delivery for order success";
                    loggerDelivery.LogInfo();
                }
            }
        }

        private async Task<bool> CheckSurchargeActiveWithBranch(KvInternalContext coreContext, SurCharge surcharge, int branchId)
        {
            if (surcharge.ForAllBranch) return true;
            var surchargeByBranch =
                await _surChargeBranchInternalClient.GetSurChargeBranchById(coreContext, surcharge.Id);
            var isApproveSurcharge = surchargeByBranch?.Any(b => b.BranchId == branchId) ?? false;
            return isApproveSurcharge;
        }

        private void PushLishMessageErrorNotification(KvOrder kvOrder, long channelId, string errorMessage, LogObjectMicrosoftExtension logInfo)
        {
            logInfo.Clone("PushLishMessageErrorNotification");
            var obj = new Infrastructure.EventBus.Event.ErrorOrderInvoiceNotificationMesage(logInfo.Id)
            {
                RetailerId = kvOrder.RetailerId,
                BranchId = kvOrder.BranchId,
                ChannelId = channelId,
                OrderKv = kvOrder.Code,
                OrderId = kvOrder.Ordersn,
                PurchaseDate = kvOrder.PurchaseDate,
                ErrorType = (int)Demo.OmniChannel.Sdk.Common.SyncTabError.Order,
                ErrorMessage = errorMessage,
                Type = (int)ChannelType
            };
            _integrationEventService.AddEventWithRoutingKeyAsync(obj, RoutingKey.MessageErrorNotification);
            logInfo.RequestObject = obj.ToJson();
            logInfo.LogInfo(true);
        }

        private StringBuilder GetWarrantysLog(OrderDetail orderDetail,
            List<OmniChannelCore.Api.Sdk.Models.InvoiceWarranties> invoiceWarrantys)
        {
            var warrantysLog = new StringBuilder();

            var checkProductIsCombo = !invoiceWarrantys.Where(x => x.ProductId == orderDetail.ProductId).Any();
            var productWarrantysGroup = invoiceWarrantys.GroupBy(x => new { x.ProductId, x.ProductCode }).ToList();
            foreach (var group in productWarrantysGroup)
            {
                var guarrantee = new StringBuilder();
                var mainGuarrantee = string.Empty;
                var productWarrantys = invoiceWarrantys.Where(x => x.ProductId == group.Key.ProductId).ToList();

                productWarrantys?.ForEach(w =>
                {
                    if (w.WarrantyType == (int)WarrantyType.Maintenance)
                    {
                        mainGuarrantee = string.Format("Định kỳ bảo trì: {0} {1} ({2}) đến {3}",
                            w.NumberTime, SystemHelper.GetWarrantyTimeTypeText(w.TimeType).ToLower(),
                            w.Description, ((DateTime)w.ExpireDate).ToString("dd/MM/yyyy"));
                    }
                    else
                    {
                        var guarranteelog = string.Format("{0} {1} ({2}) đến {3}",
                           w.NumberTime, SystemHelper.GetWarrantyTimeTypeText(w.TimeType).ToLower(),
                           w.Description, ((DateTime)w.ExpireDate).ToString("dd/MM/yyyy"));

                        guarrantee.Append(string.IsNullOrEmpty(guarrantee.ToString())
                            ? $"Bảo hành: {guarranteelog}"
                            : $", {guarranteelog}");
                    }
                });
                if (checkProductIsCombo)
                {
                    warrantysLog.Append($"Thành phần bảo hành: [ProductCode]{group.Key.ProductCode}[/ProductCode]<br />");
                }
                warrantysLog.Append(string.IsNullOrEmpty(guarrantee.ToString()) ? string.Empty : $"&nbsp;&nbsp;{guarrantee}<br>");
                warrantysLog.Append(string.IsNullOrEmpty(mainGuarrantee) ? string.Empty : $"&nbsp;&nbsp;{mainGuarrantee}<br>");
            }

            return warrantysLog;
        }
    }
}
﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.RequestDTO;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LazadaStatus = Demo.OmniChannel.ChannelClient.Common.LazadaStatus;
using ShopeeStatus = Demo.OmniChannel.ChannelClient.Common.ShopeeStatus;

namespace Demo.OmniChannel.OrderService.Impls
{
    public class GetListOrderService<T> : BaseService<T> where T : SyncOrdersMessage
    {
        private readonly IScheduleService _scheduleService;
        private readonly IOmniChannelAuthService _channelAuthService;
        private readonly IOmniChannelSettingService _omniChannelSettingService;
        private readonly IOmniChannelPlatformService _omniChannelPlatformService;

        public GetListOrderService(IAppSettings settings,
            ChannelClient.Impls.ChannelClient channelClient,
            IDbConnectionFactory dbConnectionFactory,
            ICacheClient cacheClient,
            IMessageService messageService,
            KvRedisConfig mqRedisConfig,
            IScheduleService scheduleService,
            IOmniChannelAuthService channelAuthService,
            IOmniChannelSettingService omniChannelSettingService,
            ILogger logger,
            IOmniChannelPlatformService omniChannelPlatformService) : base(settings,
            channelClient,
            cacheClient,
            dbConnectionFactory,
            messageService,
            mqRedisConfig,
            logger)
        {
            _scheduleService = scheduleService;
            ThreadSize = MqRedisConfig?.EventMessages.FirstOrDefault(x => !string.IsNullOrEmpty(x.Name) &&
                                                                          x.Name.Contains(GetType()
                                                                              .Name))?.ThreadSize ?? 10;
            messageService.RegisterHandler<T>(m =>
            {
                m.Options = (int)MessageOption.None;
                var task = Task.Run(async () => await ProcessMessage(m.GetBody()));
                task.GetAwaiter().GetResult();
                return Task.CompletedTask;
            }, (messageHandler, msg, exp) =>
            {
                var data = msg.GetBody();
                if (data != null)
                {
                    var helper = new ScheduleHelper(scheduleService);
                    helper.UnlockSchedule(data.ScheduleDto);
                }
                ExceptionHelper.WriteLogExceptionMq(typeof(T), msg.Body, exp);

            }, ThreadSize);

            _channelAuthService = channelAuthService;
            Settings = settings;
            _omniChannelSettingService = omniChannelSettingService;
            _omniChannelPlatformService = omniChannelPlatformService;
        }
        protected override async Task ProcessMessage(T data)
        {
            var loggerObj = new LogObjectMicrosoftExtension(Logger, Guid.NewGuid())
            {
                Action = "GetListOrder",
                RetailerId = data.RetailerId,
                BranchId = data.BranchId,
                OmniChannelId = data.ChannelId,
                Description = $"IsFirstSync: {data.IsFirstSync}, LastSync: {data.ScheduleDto?.LastSync}, SyncOrderFormula: {data.SyncOrderFormula}",
                RequestObject = data.ScheduleDto,
                ChannelType = data.ChannelType,
                ChannelTypeCode = ((ChannelTypeEnum)data.ChannelType).ToString()
            };
            try
            {
                var connectStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);
                var kvContext = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, data.RetailerId, connectStringName, data.BranchId, Settings?.Get<int>("ExecutionContext"));
                var internalContext = kvContext.ConvertTo<KvInternalContext>();
                internalContext.UserId = kvContext.User?.Id ?? 0;
                loggerObj.GroupId = kvContext.GroupId;
                var client = ChannelClient.GetClient((byte)ChannelType, Settings);
                var settings = await _omniChannelSettingService.GetChannelSettings(data.ChannelId, data.RetailerId);
                var platform = await _omniChannelPlatformService.GetById(data.PlatformId);

                var newLastSyncFeature = DateTime.Now;
                var channel = new Domain.Model.OmniChannel
                {
                    Id = data.ChannelId,
                    RetailerId = data.RetailerId,
                    BranchId = data.BranchId,
                    Type = (byte)ChannelType
                };

                var auth = await _channelAuthService.GetChannelAuth(channel, data.LogId);


                if (auth == null)
                {
                    RunningSchedule(data.ChannelType, data.ScheduleDto, data.SyncOrderFormula);
                    loggerObj.Description = "Token is Expired";
                    loggerObj.LogInfo(true);
                    return;
                }
                var authKey = new ChannelAuth
                {
                    AccessToken = auth.AccessToken,
                    RefreshToken = auth.RefreshToken,
                    ShopId = auth.ShopId,
                    Id = auth.Id
                };
                loggerObj.ShopId = auth.ShopId.ToString();
                bool isSuccess;
                List<CreateOrderRequest> orders;
                try
                {
                    (isSuccess, orders) = await client.GetOrders(
                        authKey,
                        new OrderRequest { LastSync = data.LastSync, SyncOrderFormula = data.SyncOrderFormula, ChannelId = data.ChannelId },
                        internalContext,
                        loggerObj,
                        platform);
                }
                catch (Exception e)
                {
                    RunningSchedule(data.ChannelType, data.ScheduleDto, data.SyncOrderFormula);
                    loggerObj.Description = $"{e.Message} - {e.StackTrace}";
                    loggerObj.LogError(e);
                    throw;
                }

                if (!isSuccess)
                {
                    RunningSchedule(data.ChannelType, data.ScheduleDto, data.SyncOrderFormula);
                    loggerObj.Description = "Sync order error";
                    loggerObj.LogInfo();
                    return;
                }
                var pushedOrders = new List<string>();
                foreach (var order in orders)
                {
                    if (data.IsFirstSync && (order.OrderStatus == ShopeeStatus.Cancelled ||
                                         order.OrderStatus == ShopeeStatus.Completed ||
                                         order.OrderStatus == LazadaStatus.Cancelled ||
                                         order.OrderStatus == LazadaStatus.ShippedBackSuccess ||
                                         order.OrderStatus == LazadaStatus.Delivered ||
                                         order.OrderStatus == TikiStatus.Canceled ||
                                         order.OrderStatus == TikiStatus.Closed ||
                                         order.OrderStatus == TikiStatus.Returned ||
                                         order.OrderStatus == TikiStatus.Complete ||
                                         order.OrderStatus == SendoOrderStatusName.Closed ||
                                         order.OrderStatus == SendoOrderStatusName.Cancelled ||
                                         order.OrderStatus == SendoOrderStatusName.Returned ||
                                         order.OrderStatus == SendoOrderStatusName.Completed
                                         ))
                    {
                        continue;
                    }

                    //Lọc các đơn hàng theo option(tạo, tạo hoặc cập nhập)
                    if (settings.SyncOrderFormulaType == (byte)SyncOrderFormulaType.Create &&
                        settings.SyncOrderFormulaDateTime != null && order.CreateDate != null &&
                        order.CreateDate <= settings.SyncOrderFormulaDateTime)
                    {
                        continue;
                    }
                    PushRedisMessageQueue(data, order);
                    pushedOrders.Add(order.OrderId);
                }
                loggerObj.ResponseObject = "Pushed orders: " + string.Join(",", pushedOrders);
                RunningSchedule(data.ChannelType, data.ScheduleDto, data.SyncOrderFormula, newLastSyncFeature);
                loggerObj.LogInfo(true);
            }
            catch (Exception ex)
            {
                loggerObj.LogError(ex);
                RunningSchedule(data.ChannelType, data.ScheduleDto, data.SyncOrderFormula);
            }
        }

        private void PushRedisMessageQueue(T data, CreateOrderRequest order)
        {
            switch ((byte)ChannelType)
            {
                case (byte)Sdk.Common.ChannelType.Lazada:
                    {
                        var createOrder = new LazadaCreateOrderMessage
                        {
                            ChannelId = data.ChannelId,
                            ChannelType = data.ChannelType,
                            BranchId = data.BranchId,
                            KvEntities = data.KvEntities,
                            RetailerId = data.RetailerId,
                            OrderId = order.OrderId,
                            Order = order.Order,
                            GroupId = data.GroupId,
                            PriceBookId = data.BasePriceBookId,
                            OrderStatus = order.OrderStatus,
                            IsFirstSync = data.IsFirstSync,
                            LogId = data.LogId,
                            PlatformId = data.PlatformId
                        };
                        using (var mq = MessageService.MessageFactory.CreateMessageProducer())
                        {
                            mq.Publish(createOrder);
                        }

                        break;
                    }

                case (byte)Sdk.Common.ChannelType.Shopee:
                    {
                        var createOrder = new ShopeeCreateOrderMessage
                        {
                            ChannelId = data.ChannelId,
                            ChannelType = data.ChannelType,
                            BranchId = data.BranchId,
                            KvEntities = data.KvEntities,
                            RetailerId = data.RetailerId,
                            OrderId = order.OrderId,
                            Order = order.Order,
                            GroupId = data.GroupId,
                            PriceBookId = data.BasePriceBookId,
                            OrderStatus = order.OrderStatus,
                            IsFirstSync = data.IsFirstSync,
                            LogId = data.LogId,
                            PlatformId = data.PlatformId,
                            SourceQueue = "Queue is created from GetListOrder"
                        };
                        using (var mq = MessageService.MessageFactory.CreateMessageProducer())
                        {
                            mq.Publish(createOrder);
                        }

                        break;
                    }

                case (byte)Sdk.Common.ChannelType.Tiki:
                    {
                        var createOrder = new TikiCreateOrderMessage
                        {
                            ChannelId = data.ChannelId,
                            ChannelType = data.ChannelType,
                            BranchId = data.BranchId,
                            KvEntities = data.KvEntities,
                            RetailerId = data.RetailerId,
                            OrderId = order.OrderId,
                            Order = order.Order,
                            GroupId = data.GroupId,
                            PriceBookId = data.BasePriceBookId,
                            OrderStatus = order.OrderStatus,
                            IsFirstSync = data.IsFirstSync,
                            LogId = data.LogId,
                            PlatformId = data.PlatformId,
                        };
                        using (var mq = MessageService.MessageFactory.CreateMessageProducer())
                        {
                            mq.Publish(createOrder);
                        }

                        break;
                    }
                case (byte)Sdk.Common.ChannelType.Sendo:
                    {
                        var createOrder = new SendoCreateOrderMessage
                        {
                            ChannelId = data.ChannelId,
                            ChannelType = data.ChannelType,
                            BranchId = data.BranchId,
                            KvEntities = data.KvEntities,
                            RetailerId = data.RetailerId,
                            OrderId = order.OrderId,
                            Order = order.Order,
                            GroupId = data.GroupId,
                            PriceBookId = data.BasePriceBookId,
                            OrderStatus = order.OrderStatus,
                            IsFirstSync = data.IsFirstSync,
                            LogId = data.LogId,
                            PlatformId = data.PlatformId,
                        };
                        using (var mq = MessageService.MessageFactory.CreateMessageProducer())
                        {
                            mq.Publish(createOrder);
                        }

                        break;
                    }
            }
        }
        private void RunningSchedule(byte channelType, ScheduleDto dto, int syncOrderFormula, DateTime? lastSync = null)
        {
            var scheduleHelper = new ScheduleHelper(_scheduleService);

            if (syncOrderFormula == (int)SyncOrderFormula.Now && lastSync == null)
            {
                dto.LastSync = DateTime.Now.AddSeconds(-30);
                scheduleHelper.ReleaseSchedule(dto);
                return;
            }

            if (lastSync != null)
            {
                dto.LastSync = lastSync;
                scheduleHelper.ReleaseSchedule(dto);
                return;
            }
            scheduleHelper.UnlockSchedule(dto);

        }
    }
}
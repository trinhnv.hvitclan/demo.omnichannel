﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Tiktok;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.OrderService.OrderDomain.Interfaces;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Demo.OmniChannelCore.Api.Sdk.Models;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System.Data;
using System.Threading.Tasks;

namespace Demo.OmniChannel.OrderService.Impls
{



    public class TiktokCreateInvoiceService : CreateInvoiceServiceV2<TiktokCreateInvoiceMessageV2>
    {
        private readonly IDeliveryInfoInternalClient _deliveryInfoInternalClient;

        public TiktokCreateInvoiceService(
            IIntegrationEventService integrationEventService,
            IAppSettings settings,
            IProductMappingService productMappingService,
            IRetryInvoiceMongoService retryInvoiceMongoService,
            IInvoiceMongoService invoiceMongoService,
            Demo.OmniChannelCore.Api.Sdk.Interfaces.IInvoiceInternalClient invoiceInternalClient,
            Demo.OmniChannelCore.Api.Sdk.Interfaces.IOrderInternalClient orderInternalClient,
            Demo.OmniChannelCore.Api.Sdk.Interfaces.ISurChargeInternalClient surchargeInternalClient,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IAuditTrailInternalClient auditTrailInternalClient,
            IMessageService mqService,
            IOmniChannelAuthService channelAuthService,
            KvRedisConfig mqRedisConfig,
            IDeliveryInfoInternalClient deliveryInfoInternalClient,
            IDeliveryPackageInternalClient deliveryPackageInternalClient,
            IKvLockRedis kvLockRedis,
            ICustomerInternalClient customerInternalClient,
            IOmniChannelSettingService omniChannelSettingService,
            ICreateOrderDomainService createOrderDomainService,
            ICreateInvoiceDomainService createInvoiceDomainService,
            ILogger<TiktokCreateInvoiceService> logger) : base(
            integrationEventService,
            settings,
            productMappingService,
            retryInvoiceMongoService,
            invoiceMongoService,
            invoiceInternalClient,
            orderInternalClient,
            surchargeInternalClient,
            channelClient,
            cacheClient,
            dbConnectionFactory,
            auditTrailInternalClient,
            mqService,
            channelAuthService,
            mqRedisConfig,
            deliveryInfoInternalClient,
            deliveryPackageInternalClient,
            kvLockRedis,
            customerInternalClient,
            omniChannelSettingService,
            createOrderDomainService,
            createInvoiceDomainService,
            logger)
        {
            ChannelType = ChannelType.Tiktok;
            _deliveryInfoInternalClient = deliveryInfoInternalClient;
        }

        protected async override Task UpdateFeeDeliveryInOrder(IDbConnection db, OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext, byte channelType, long? deliveryInfoId, decimal? price, string feeJson)
        {
            if (price.HasValue && !string.IsNullOrEmpty(feeJson) && deliveryInfoId.HasValue)
            {
                var deliveryInfoRepository = new DeliveryInfoRepository(db);
                var existDelivery = await deliveryInfoRepository.SingleAsync(x => x.Id == deliveryInfoId);
                if (existDelivery != null && existDelivery.FeeJson != feeJson)
                {
                    var deliveryInfo = existDelivery.ConvertTo<DeliveryInfo>();
                    deliveryInfo.UpdateFeeJson(price, feeJson);
                    await _deliveryInfoInternalClient.UpdateDeliveryPriceAndFeeJson(coreContext, deliveryInfo);
                }
            }
        }

        protected override void PublishUpdatePaymentTransaction(TiktokCreateInvoiceMessageV2 data, LogObjectMicrosoftExtension loggerExtension)
        {
            loggerExtension.Action = "PushTiktokSyncPaymentTransaction";
            if (!string.IsNullOrEmpty(data.Order))
            {
                var kvOrder = data.Order.FromJson<Demo.OmniChannel.ChannelClient.Models.KvOrder>();
                if (!string.IsNullOrEmpty(kvOrder?.ChannelOrder))
                {
                    var channelOrder = kvOrder.ChannelOrder.FromJson<TiktokOrderDetail>();
                    if (channelOrder?.SettlementList != null)
                    {
                        using (var mq = MessageService.MessageFactory.CreateMessageProducer())
                        {
                            var message = new TiktokSyncPaymentTransactionMessage
                            {
                                RetailerId = data.RetailerId,
                                OrderId = data.OrderId,
                                DataSettlements = channelOrder.SettlementList.ToSafeJson(),
                                ChannelId = data.ChannelId
                            };
                            mq.Publish(message);
                            loggerExtension.Description = "Push success from createInvoice";
                            loggerExtension.LogInfo();
                        }
                    }
                }
            }
        }
    }
}

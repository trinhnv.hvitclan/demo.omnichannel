﻿using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Microsoft.Extensions.Logging;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.OrderService.Impls
{
    public class SendoGetListOrderService : GetListOrderService<SendoSyncOrderMessage>
    {
        public SendoGetListOrderService(IAppSettings settings,
            ChannelClient.Impls.ChannelClient channelClient,
            IDbConnectionFactory dbConnectionFactory,
            ICacheClient cacheClient,
            IMessageService messageService,
            KvRedisConfig mqRedisConfig,
            IScheduleService scheduleService,
            IOmniChannelAuthService channelAuthService,
            IOmniChannelSettingService omniChannelSettingService,
            IOmniChannelPlatformService omniChannelPlatformService,
            ILogger<SendoGetListOrderService> logger) : base(settings,
            channelClient,
            dbConnectionFactory,
            cacheClient,
            messageService,
            mqRedisConfig,
            scheduleService,
            channelAuthService,
            omniChannelSettingService,
            logger,
            omniChannelPlatformService)
        {
            ChannelType = ChannelType.Sendo;
        }
    }
}
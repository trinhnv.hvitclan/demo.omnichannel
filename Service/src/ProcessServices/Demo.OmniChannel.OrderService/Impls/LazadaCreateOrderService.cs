﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.RequestDTO;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.OrderService.OrderDomain.Interfaces;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Auth;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.OrderService.Impls
{
    public class LazadaCreateOrderService : CreateOrderService<LazadaCreateOrderMessage>
    {
        private readonly IOrderInternalClient _orderInternalClient;
        private readonly IProductMappingService _productMappingService;
        private readonly ChannelClient.Impls.ChannelClient _channelClient;
        private readonly IOmniChannelAuthService _channelAuthService;

        public LazadaCreateOrderService(
            IIntegrationEventService integrationEventService,
            IAppSettings settings,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IDeliveryInfoInternalClient deliveryInfoInternalClient,
            IDbConnectionFactory dbConnectionFactory,
            ICustomerInternalClient customerInternalClient,
            IOrderInternalClient orderInternalClient,
            IProductMappingService productMappingService,
            IOrderMongoService orderMongoService,
            IPartnerDeliveryInternalClient partnerInternalClient,
            IAuditTrailInternalClient auditTrailInternalClient,
            IMessageService messageService,
            IOmniChannelAuthService channelAuthService,
            IInvoiceMongoService invoiceMongoService,
            IRetryOrderMongoService retryOrderMongoService,
            IRetryInvoiceMongoService retryInvoiceMongoService,
            IKvLockRedis kvLockRedis,
            KvRedisConfig mqRedisConfig,
            IChannelBusiness channelBusiness,
            ISendoLocationMongoService sendoLocationMongoService,
            ISurChargeInternalClient surChargeInternalClient,
            IOmniChannelSettingService omniChannelSettingService,
            ISurChargeBranchInternalClient surChargeBranchInternalClient,
            ILogger<LazadaCreateOrderService> logger,
            IOmniChannelPlatformService omniChannelPlatformService,
            ICreateOrderDomainService createOrderDomainService) : base(
            settings,
            channelClient,
            cacheClient,
            dbConnectionFactory,
            customerInternalClient,
            deliveryInfoInternalClient,
            orderInternalClient,
            productMappingService,
            orderMongoService,
            retryOrderMongoService,
            retryInvoiceMongoService,
            partnerInternalClient,
            auditTrailInternalClient,
            messageService,
            channelAuthService,
            invoiceMongoService,
            kvLockRedis,
            mqRedisConfig,
            channelBusiness,
            sendoLocationMongoService,
            surChargeInternalClient,
            omniChannelSettingService,
            surChargeBranchInternalClient,
            logger,
            omniChannelPlatformService,
            integrationEventService,
            createOrderDomainService
            )
        {
            ChannelType = ChannelType.Lazada;
            _orderInternalClient = orderInternalClient;
            _productMappingService = productMappingService;
            _channelClient = channelClient;
            _channelAuthService = channelAuthService;
        }

        public override async Task<List<MongoDb.ProductMapping>> HandleMapping(LazadaCreateOrderMessage data, ChannelClient.Models.KvOrder order)
        {
            var productChannelSkuLst = order.OrderDetails
                .Where(p => !string.IsNullOrEmpty(p.ProductChannelSku))
                .Select(x => x.ProductChannelSku)
                .ToList();
            List<MongoDb.ProductMapping> mappings = await GetProductMapping(data, productChannelSkuLst);

            foreach (var detail in order.OrderDetails)
            {
                var mapping = mappings?
                    .FirstOrDefault(x => (!string.IsNullOrEmpty(detail.ProductChannelId) && x.CommonProductChannelId == detail.ProductChannelId) || x.ProductChannelSku.Equals(detail.ProductChannelSku));

                detail.ProductId = mapping?.ProductKvId ?? 0;
                detail.ProductCode = mapping?.ProductKvSku ?? string.Empty;
            }
            return mappings;
        }

        private async Task<List<MongoDb.ProductMapping>> GetProductMapping(LazadaCreateOrderMessage data, List<string> productChannelSkuLst)
        {
            List<MongoDb.ProductMapping> mappings = null;
            if (productChannelSkuLst.Any())
            {
                mappings = await _productMappingService.GetByChannelProductSkus(data.RetailerId, data.ChannelId, productChannelSkuLst);
            }
            return mappings;
        }

        public override async Task HandleOrderWithCancelProduct(ExecutionContext context, LazadaCreateOrderMessage data, Domain.Model.OmniChannel channel,
            Order existOrder,
            ChannelClient.Models.Platform platform,
            LogObjectMicrosoftExtension loggerObj)
        {
            try
            {
                loggerObj.Action = "BeginUpdateLZDOrder";
                var client = _channelClient.GetClient((byte)ChannelType, Settings);
                var auth = await _channelAuthService.GetChannelAuth(channel, data.LogId);
                if(auth == null)
                {
                    loggerObj.LogWarning("Auth is null!!");
                    return;
                }
                var authKey = new Demo.OmniChannel.ChannelClient.Models.ChannelAuth
                {
                    AccessToken = auth.AccessToken,
                    RefreshToken = auth.RefreshToken,
                    ShopId = auth.ShopId
                };
                var createOrderRequest = new CreateOrderRequest
                {
                    BranchId = data.BranchId,
                    OrderId = data.OrderId,
                    Order = data.Order,
                    RetryCount = data.RetryCount,
                    PriceBookId = data.PriceBookId
                };
                var orderDetailGetResponse = await client.GetOrderDetail(data.RetailerId, data.ChannelId, authKey, createOrderRequest, loggerObj, platform);
                var order = orderDetailGetResponse.Order;
                if (orderDetailGetResponse.Order == null)
                {
                    loggerObj.LogWarning("Order detail is null!!");
                    return;
                }
                if (order.OrderDetails != null && existOrder.OrderDetails != null && order.OrderDetails.Count != existOrder.OrderDetails.Count)
                {

                    await HandleMapping(data, order);
                    var coreContext = new Demo.OmniChannelCore.Api.Sdk.Common.KvInternalContext
                    {
                        LogId = loggerObj.Id,
                        BranchId = data.BranchId,
                        RetailerId = context.RetailerId,
                        RetailerCode = context.RetailerCode,
                        UserId = context.User.Id,
                        GroupId = context.GroupId
                    };
                    var existKvOrder = await _orderInternalClient.GetOrderById(coreContext, existOrder.Id, new string[] { "OrderDetails" });
                    if (existKvOrder != null && order.OrderDetails != null)
                    {
                        existKvOrder.OrderDetails = order.OrderDetails.Select(item => new Demo.OmniChannelCore.Api.Sdk.Models.OrderDetail()
                        {
                            ProductId = item.ProductId,
                            Quantity = item.Quantity,
                            Discount = item.Discount,
                            Price = item.Price,
                        }).ToList();

                        loggerObj.Action = "UpdateLZDOrder";
                        loggerObj.RequestObject = existKvOrder;
                        loggerObj.LogInfo();
                        await _orderInternalClient.UpdateOrder(coreContext, existKvOrder);
                    }
                }
            }
            catch (Exception ex)
            {
                loggerObj.LogError(ex);
            }

        }

    }
}
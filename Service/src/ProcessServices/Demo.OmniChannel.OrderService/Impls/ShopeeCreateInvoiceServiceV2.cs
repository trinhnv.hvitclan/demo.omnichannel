﻿using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.OrderService.OrderDomain.Interfaces;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.EventLogMessage;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.OrderService.Impls
{
    public class ShopeeCreateInvoiceServiceV2 : CreateInvoiceServiceV2<ShopeeCreateInvoiceMessageV2>
    {
        public ShopeeCreateInvoiceServiceV2(
            IIntegrationEventService integrationEventService,
            IAppSettings settings, 
            IProductMappingService productMappingService,
            IRetryInvoiceMongoService retryInvoiceMongoService,
            IInvoiceMongoService invoiceMongoService, 
            IInvoiceInternalClient invoiceInternalClient, 
            IOrderInternalClient orderInternalClient,
            ISurChargeInternalClient surChargeInternalClient,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient, 
            IDbConnectionFactory dbConnectionFactory, 
            IAuditTrailInternalClient auditTrailInternalClient, 
            IMessageService mqService,
            IOmniChannelAuthService channelAuthService,
            KvRedisConfig mqRedisConfig,
            IDeliveryInfoInternalClient deliveryInfoInternalClient,
            IDeliveryPackageInternalClient deliveryPackageInternalClient,
            IKvLockRedis kvLockRedis,
            ICustomerInternalClient customerInternalClient,
            IOmniChannelSettingService omniChannelSettingService,
            ICreateOrderDomainService createOrderDomainService,
            ICreateInvoiceDomainService createInvoiceDomainService,
            ILogger<ShopeeCreateInvoiceServiceV2> logger) : base(
            integrationEventService,
            settings, 
            productMappingService,
            retryInvoiceMongoService,
            invoiceMongoService, 
            invoiceInternalClient, 
            orderInternalClient,
            surChargeInternalClient,
            channelClient, 
            cacheClient, 
            dbConnectionFactory, 
            auditTrailInternalClient, 
            mqService,
            channelAuthService,
            mqRedisConfig,
            deliveryInfoInternalClient,
            deliveryPackageInternalClient,
            kvLockRedis,
            customerInternalClient,
            omniChannelSettingService,
            createOrderDomainService,
            createInvoiceDomainService,
            logger)
        {
            ChannelType = Demo.OmniChannel.Sdk.Common.ChannelType.Shopee;
        }
    }
}
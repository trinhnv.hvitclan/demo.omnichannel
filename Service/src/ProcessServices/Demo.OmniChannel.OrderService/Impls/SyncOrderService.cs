﻿using Demo.Audit.Model.Message;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.OrderService.OrderDomain;
using Demo.OmniChannel.OrderService.OrderDomain.Interfaces;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.Services.LogginConfiguration;
using Demo.OmniChannel.ShareKernel.Auth;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Common;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Customer = Demo.OmniChannelCore.Api.Sdk.Models.Customer;
using InvoiceDelivery = Demo.OmniChannelCore.Api.Sdk.Models.InvoiceDelivery;
using Order = Demo.OmniChannel.MongoDb.Order;
using OrderDetail = Demo.OmniChannelCore.Api.Sdk.Models.OrderDetail;
using PartnerDelivery = Demo.OmniChannel.MongoDb.PartnerDelivery;

namespace Demo.OmniChannel.OrderService.Impls
{
    public abstract class SyncOrderService<T> : BaseService<T> where T : SyncErrorOrderMessage
    {
        private readonly IProductMappingService _productMappingService;
        private readonly IOrderMongoService _orderMongoService;
        private readonly ICustomerInternalClient _customerInternalClient;
        private readonly IPartnerDeliveryInternalClient _partnerInternalClient;
        private readonly IOrderInternalClient _orderInternalClient;
        private readonly IAuditTrailInternalClient _auditTrailInternalClient;
        private readonly IInvoiceMongoService _invoiceMongoService;
        private readonly IRetryOrderMongoService _retryOrderMongoService;
        private readonly IRetryInvoiceMongoService _retryInvoiceMongoService;
        private readonly IOmniChannelAuthService _channelAuthService;
        private readonly IKvLockRedis _kvLockRedis;
        private readonly IChannelBusiness _channelBusiness;
        private readonly ISendoLocationMongoService _sendoLocationMongoService;
        private readonly ISurChargeInternalClient _surChargeInternalClient;
        private readonly IOmniChannelSettingService _omniChannelSettingService;
        private readonly IOmniChannelPlatformService _omniChannelPlatformService;
        private readonly ICreateOrderDomainService _createOrderDomainService;

        public SyncOrderService(IAppSettings settings,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IProductMappingService productMappingService,
            ICustomerInternalClient customerInternalClient,
            IPartnerDeliveryInternalClient partnerInternalClient,
            IOrderInternalClient orderInternalClient,
            IOrderMongoService orderMongoService,
            IAuditTrailInternalClient auditTrailInternalClient,
            IMessageService mqService,
            IInvoiceMongoService invoiceMongoService,
            IRetryOrderMongoService retryOrderMongoService,
            IRetryInvoiceMongoService retryInvoiceMongoService,
            IKvLockRedis kvLockRedis,
            IOmniChannelAuthService channelAuthService,
            KvRedisConfig mqRedisConfig,
            IChannelBusiness channelBusiness,
            ISendoLocationMongoService sendoLocationMongoService,
            ISurChargeInternalClient surChargeInternalClient,
            IOmniChannelSettingService omniChannelSettingService,
            IOmniChannelPlatformService omniChannelPlatformService,
            ICreateOrderDomainService createOrderDomainService,
            ILogger logger) : base(settings,
            channelClient,
            cacheClient,
            dbConnectionFactory,
            mqService,
            mqRedisConfig,
            logger)
        {
            ThreadSize = MqRedisConfig?.EventMessages.FirstOrDefault(x => !string.IsNullOrEmpty(x.Name) &&
                                                                          x.Name.Contains(this.GetType()
                                                                              .Name))?.ThreadSize ?? 10;
            mqService.RegisterHandler<T>(m =>
            {
                try
                {
                    m.Options = (int)MessageOption.None;
                    var task = Task.Run(async () => await ProcessMessage(m.GetBody()));
                    task.GetAwaiter().GetResult();
                    return Task.CompletedTask;
                }
                catch (Exception e)
                {
                    ExceptionHelper.WriteLogExceptionMq(typeof(T), m.Body, e);
                    return string.Empty;
                }
            }, ThreadSize);
            _productMappingService = productMappingService;
            _orderMongoService = orderMongoService;
            _customerInternalClient = customerInternalClient;
            _partnerInternalClient = partnerInternalClient;
            _orderInternalClient = orderInternalClient;
            _auditTrailInternalClient = auditTrailInternalClient;
            _invoiceMongoService = invoiceMongoService;
            _retryOrderMongoService = retryOrderMongoService;
            _retryInvoiceMongoService = retryInvoiceMongoService;
            _kvLockRedis = kvLockRedis;
            _channelAuthService = channelAuthService;
            _channelBusiness = channelBusiness;
            _sendoLocationMongoService = sendoLocationMongoService;
            _surChargeInternalClient = surChargeInternalClient;
            _omniChannelSettingService = omniChannelSettingService;
            _omniChannelPlatformService = omniChannelPlatformService;
            _createOrderDomainService = createOrderDomainService;
        }

        protected override async Task ProcessMessage(T data)
        {
            var retryOrder = new LogObjectMicrosoftExtension(Logger, data.LogId)
            {
                Action = "SyncErrorOrder",
                RetailerId = data.RetailerId,
                RetailerCode = data.RetailerCode,
                BranchId = data.BranchId,
                OmniChannelId = data.ChannelId,
                ChannelType = data.ChannelType,
                ChannelTypeCode = ((ChannelTypeEnum)data.ChannelType).ToString()
            };

            await RetryOrders(data);
            retryOrder.LogInfo(true);
        }

        protected virtual async Task RetryOrders(T data)
        {
            var loggerExtension = new LogObjectMicrosoftExtension(Logger, data.LogId)
            {
                Action = ExceptionType.RetryErrorOrders,
                RetailerId = data.RetailerId,
                RetailerCode = data.RetailerCode,
                BranchId = data.BranchId,
                OmniChannelId = data.ChannelId,
                ChannelType = data.ChannelType,
                ChannelTypeCode = ((ChannelTypeEnum)data.ChannelType).ToString()
            };
            var order = data.Order.FromJson<Order>();
            if (order == null) return;

            var channel = await _channelBusiness.GetChannel(data.ChannelId, data.RetailerId, data.LogId);
            if (channel == null)
            {
                loggerExtension.Description = "Channel is empty or invalid --> ignore";
                loggerExtension.LogInfo();
                return;
            }

            var channelType = channel.Type;
            data.ChannelType = channelType;

            var connectStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);

            var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, data.RetailerId,
                connectStringName, order.BranchId, Settings?.Get<int>("ExecutionContext"));
            var coreContext = context.ConvertTo<KvInternalContext>();
            coreContext.UserId = context.User?.Id ?? 0;
            coreContext.LogId = loggerExtension.Id;
            loggerExtension.GroupId = context.GroupId;

            try
            {
                var orderInMongo = await _orderMongoService.GetByOrderId(order.OrderId, order.BranchId, order.ChannelId);
                if (orderInMongo == null)
                {
                    loggerExtension.Description = "orderInMongo is null --> return";
                    loggerExtension.LogInfo();
                    return;
                }

                using var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower());
                var partnerRepository = new PartnerDeliveryRepository(db);
                var saleChannelRepository = new SaleChannelRepository(db);
                var customerRepository = new CustomerRepository(db);
                var orderRepository = new OrderRepository(db);
                var posSettingRepository = new PosSettingRepository(db);
                var productRepository = new ProductRepository(db);
                var productFormulaRepository = new ProductFormulaRepository(db);
                var client = ChannelClient.GetClient(channelType, Settings);
                var existOrder = await orderRepository.GetOrderByCodeAsync(order.Code, order.RetailerId);
                var omniChannelSettings = await _omniChannelSettingService.GetChannelSettings(data.ChannelId, data.RetailerId);
                var platform = await _omniChannelPlatformService.GetById(channel.PlatformId);
                await RemoveMongoRetryOrder(order.OrderId, order.BranchId, order.ChannelId,
                    orderInMongo.RetailerId);
                if (existOrder != null)
                {
                    if (existOrder.Status != (byte)OrderState.Void)
                    {
                        await PublishCreateInvoiceQueue(data, order, channelType, existOrder, false, loggerExtension);
                    }
                    await RemoveErrorOrder(orderInMongo);
                    return;
                }

                await HandleNewOrderDetailChange(data, client, order, channel, loggerExtension, platform);

                var channelProductIds = order.OrderDetails.Where(p => !string.IsNullOrEmpty(p.CommonProductChannelId) && p.CommonProductChannelId != "0").Select(x => x.CommonProductChannelId).ToList();
                var mappings = await GetMappingInfo(order, data);

                var posSetting = new PosSetting(await posSettingRepository.GetSettingAsync(data.RetailerId), data.RetailerId, DbConnectionFactory, CacheClient, Settings);
                var saleChannel = await saleChannelRepository.GetByOmmiChannelId(data.RetailerId, order.ChannelId);
                order.SaleChannelId = saleChannel?.Id ?? 0;

                var kvInternalContext = new KvInternalContext
                {
                    LogId = loggerExtension.Id,
                    BranchId = order.BranchId,
                    RetailerId = context.RetailerId,
                    RetailerCode = context.RetailerCode,
                    UserId = context.User?.Id ?? 0,
                    GroupId = context.GroupId
                };
                //Lấy thông tin khách hàng (Nếu chưa có tạo mới khách hàng)
                var customer = await CreateCustomerAsync(order, customerRepository,
                    data, posSetting?.ManagerCustomerByBranch ?? false, coreContext, loggerExtension);
                if (customer.isError)
                {
                    var errorMessage = $"Thời gian đồng bộ lại: {DateTime.Now.ToString("dd/MM/yyyy HH:mm")}; Đồng bộ thông tin khách hàng thất bại.";
                    var customerLog = $"- Thời gian đồng bộ lại: {DateTime.Now.ToString("dd/MM/yyyy HH:mm")}; [CustomerCode]: Đồng bộ thông tin khách hàng thất bại <br/>";
                    await SaveOrderSyncError(coreContext, order, order.OrderId, data.ChannelId, data.ChannelType, errorMessage, customerLog);
                    return;
                }

                var productIds = order.OrderDetails.Select(p => p.ProductId).ToList();
                var kvProducts = await productRepository.GetProductByIds(data.RetailerId, data.BranchId, productIds, isLogMonitor: false);

                var isValidateProduct = await ValidateProduct(kvProducts,
                    order, data.RetailerId, order.BranchId, order.ChannelId,
                    order.OrderId, channelType, coreContext, mappings, loggerExtension);
                if (!isValidateProduct)
                {
                    return;
                }

                if (order.OrderDelivery != null)
                {
                    if (string.IsNullOrEmpty(order.OrderDelivery.LocationName))
                    {
                        order.OrderDelivery.LocationName = await GetSendoLocation(_sendoLocationMongoService, CacheClient, order.OrderDelivery.ReceiverDistrictId.GetValueOrDefault());
                    }

                    GetKvLocationWard(order);
                }

                if (order.OrderDelivery != null && order.OrderDelivery?.PartnerDelivery == null)
                    order.OrderDelivery.PartnerDelivery = GetPartnerForChannel(channelType);

                if (order.OrderDelivery?.PartnerDelivery == null) return;

                var partnerDelivery = await partnerRepository.SingleAsync(p =>
                    p.RetailerId == data.RetailerId &&
                    p.Code.ToLower() == order.OrderDelivery.PartnerDelivery.Code.ToLower());

                if (partnerDelivery == null && order.OrderDelivery?.PartnerDelivery != null)
                {
                    var newPartner = order.OrderDelivery.PartnerDelivery;
                    newPartner.RetailerId = data.RetailerId;
                    var createPartnerModel = newPartner.ConvertTo<OmniChannelCore.Api.Sdk.Models.PartnerDelivery>();
                    createPartnerModel.IsOmniChannel = true;
                    var partnerId = await _partnerInternalClient.CreatePartner(kvInternalContext, createPartnerModel);
                    order.OrderDelivery.DeliveryBy = partnerId;
                }
                else if (partnerDelivery != null && order.OrderDelivery != null)
                {
                    if (partnerDelivery.IsOmniChannel != true)
                    {
                        partnerDelivery.IsOmniChannel = true;
                        await _partnerInternalClient.UpdatePartnerOmniChannel(kvInternalContext, partnerDelivery.ConvertTo<Demo.OmniChannelCore.Api.Sdk.Models.PartnerDelivery>());
                    }
                    order.OrderDelivery.DeliveryBy = partnerDelivery.Id;
                }

                //kiểm tra active tính năng tự động đồng bộ hàng lô date
                //trả về danh sách chi tiết đơn hàng
                if (omniChannelSettings.IsAutoSyncBatchExpire)
                {
                    var kvOrderDetails = order.OrderDetails.Map(x => x.ConvertTo<KvOrderDetail>());
                    var orderDetails = DetechHasExpire.DetectOrderDetailHasBatchExpire(kvProducts, kvOrderDetails);
                    order.OrderDetails = orderDetails.Map(o => o.ConvertTo<MongoDb.OrderDetail>());
                }

                var kvOrder = order.ConvertTo<OmniChannelCore.Api.Sdk.Models.Order>();

                var lstInvoiceSurcharge = new List<OmniChannelCore.Api.Sdk.Models.InvoiceSurCharges>();
                if (order.OrderSurCharges != null && order.OrderSurCharges.Any())
                {
                    foreach (var itemSurCharge in order.OrderSurCharges)
                    {
                        var invoiceSurcharge = new OmniChannelCore.Api.Sdk.Models.InvoiceSurCharges();
                        var surchange =
                            await _surChargeInternalClient.GetSurChargeByName(coreContext, itemSurCharge.Name);
                        if (surchange != null && !String.IsNullOrEmpty(surchange.Code))
                        {
                            //create surcharge for Order
                            invoiceSurcharge = new OmniChannelCore.Api.Sdk.Models.InvoiceSurCharges()
                            {
                                SurchargeId = surchange.Id,
                                Price = (Decimal)itemSurCharge.Price,
                                RetailerId = data.RetailerId,
                                Name = surchange.Name,
                                SurValue = (Decimal)itemSurCharge.Price
                            };
                            lstInvoiceSurcharge.Add(invoiceSurcharge);
                        }
                    }

                    kvOrder.OrderSurCharges = lstInvoiceSurcharge;
                    kvOrder.Surcharge = lstInvoiceSurcharge.Sum(x => x.Price);
                }

                kvOrder.CustomerId = customer.newCustomer.Id == 0 ? (long?)null : customer.newCustomer.Id;
                kvOrder.CustomerCode = customer.newCustomer.Code;
                kvOrder.CustomerName = customer.newCustomer.Name;
                kvOrder.OrderDelivery.PartnerDelivery = null;
                kvOrder.CreatedDate = DateTime.Now;
                kvOrder.ModifiedDate = null;
                kvOrder.OrderDetails = order.OrderDetails.Map(x => x.ConvertTo<OrderDetail>());
                kvOrder.OrderDelivery = order.OrderDelivery.ConvertTo<InvoiceDelivery>();
                kvOrder.InvoiceWarranties = await _createOrderDomainService.GetInvoiceWarranties(
                               coreContext, kvProducts, productFormulaRepository, order.OrderDetails, order.PurchaseDate);
                var orderDetail = order.OrderDetails.Map(x => x.ConvertTo<OrderDetail>());
                orderDetail.ForEach(x => x.UpdatePropetiesUseWarranty(kvOrder.InvoiceWarranties));

                var orderDelivery = order.OrderDelivery.ConvertTo<InvoiceDelivery>();
               

                await SetExtra(db, channel, kvOrder);
                var result = await _orderInternalClient.CreateOrder(kvInternalContext, kvOrder,
                    orderDetail,
                    orderDelivery);
                if (result == null || result.Id <= 0)
                {
                    throw new KvException("Tạo đơn hàng không thành công");
                }

                loggerExtension.RequestObject = $"KvOrder: {kvOrder.ToJson()}, Order: {order.ToJson()}";
                loggerExtension.ResponseObject = result.ToJson();
                loggerExtension.Description = "Create order successful";
                loggerExtension.LogInfo();

                AfterCreateOrder(order, data, context, channel);
                await RemoveErrorOrder(orderInMongo);

                #region Logs

                await WriteLogAsync(kvOrder, channelType, order, channel, coreContext);

                #endregion

                if (order.NewStatus == (byte)OrderState.Void)
                {
                    await VoidKvOrder(kvInternalContext, channelType, result);
                }

                if (order.NewStatus == null || order.NewStatus == (byte)OrderState.Pending)
                {
                    var authKey = await GetAuthByChannel(channelType, channel, data.LogId, context);
                    if (authKey == null)
                    {
                        return;
                    }

                    var (orderStatus, _, _) = await client.GetOrderStatus(order.RetailerId, order.ChannelId, data.LogId, order.OrderId, authKey, loggerExtension, true, platform);
                    switch (orderStatus)
                    {
                        case (byte)OrderState.Finalized:
                            await PublishCreateInvoiceQueue(data, order, channelType, result.ConvertTo<Domain.Model.Order>(), existOrder == null, loggerExtension);
                            break;
                        case (byte)OrderState.Void:
                            await VoidKvOrder(kvInternalContext, channelType, result);
                            break;
                    }
                    return;
                }

                if (order.NewStatus == (byte)OrderState.Finalized)
                {
                    await PublishCreateInvoiceQueue(data, order, channelType, result.ConvertTo<Domain.Model.Order>(), existOrder == null, loggerExtension);
                }
            }
            catch (Exception e)
            {
                loggerExtension.LogError(e, false, e is OmniChannelCore.Api.Sdk.Common.KvValidateException);
                if (string.IsNullOrEmpty(e.Message) ||
                    (!e.Message.Trim().Equals(string.Format(KvOrderValidateMessage.OrderExistedVi, order.Code).Trim(), StringComparison.OrdinalIgnoreCase)
                  && !e.Message.Trim().Equals(string.Format(KvOrderValidateMessage.OrderExistedEn, order.Code).Trim(), StringComparison.OrdinalIgnoreCase)))
                {
                    await SaveOrderSyncError(coreContext, order, order.OrderId, order.ChannelId,
                        channelType, e.Message, e.Message);
                }
                if (e is KvDbException || (e.Message != null && (e.Message.ToLower().Contains(DbExceptionMessages.ConnectionTimeout) || e.Message.ToLower().Contains(DbExceptionMessages.TransactionDeadlocked))))
                {
                    await AddOrUpdateRetryOrder(channel, data.KvEntities, order, e is KvDbException);
                }
            }
        }

        protected virtual void AfterCreateOrder(Order order, SyncErrorOrderMessage data, ExecutionContext context, Domain.Model.OmniChannel channel)
        {
        }

        private async Task<ChannelAuth> GetAuthByChannel(byte? channelType, Domain.Model.OmniChannel channel, Guid logId, ShareKernel.Auth.ExecutionContext context)
        {

            var channelAuth = await _channelAuthService.GetChannelAuth(channel, logId);

            if (channelAuth == null || (channelType == (byte)Sdk.Common.ChannelType.Lazada && string.IsNullOrEmpty(channelAuth.AccessToken)))
            {
                return null;
            }
            return new ChannelAuth
            {
                Id = channelAuth.Id,
                AccessToken = channelAuth.AccessToken,
                ShopId = channelAuth.ShopId
            };

        }

        private async Task<Domain.Model.Customer> _GetCustomerAsync(CustomerRepository customerRepository, int retailerid, int branchId, string phone, string customerCode, bool isManagerCustomerByBranch, LogObjectMicrosoftExtension log)
        {
            Domain.Model.Customer existsCustomer = null;
            try
            {
                existsCustomer = await customerRepository.GetByFilterAsync(retailerid, branchId, phone, customerCode, isManagerCustomerByBranch);
            }
            catch (Exception ex)
            {
                log.Action = "GetCustomer";
                log.Description = "Error Get Customer from db";
                log.LogError(ex);
            }
            return existsCustomer;
        }

        private async Task VoidKvOrder(KvInternalContext kvInternalContext, byte? channelType, OmniChannelCore.Api.Sdk.Models.Order order)
        {
            await _orderInternalClient.VoidOrder(kvInternalContext, order.Id);

            #region Logs

            var log = new AuditTrailLog
            {
                FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channelType),
                Action = (int)AuditTrailAction.OrderIntergate,
                CreatedDate = DateTime.Now,
                BranchId = order.BranchId,
                Content = $"Hủy đơn đặt hàng: [OrderCode]{order.Code}[/OrderCode]"
            };
            await _auditTrailInternalClient.AddLogAsync(kvInternalContext, log);
            #endregion
        }

        private PartnerDelivery GetPartnerForChannel(byte? channelType)
        {
            switch (channelType)
            {
                case (byte)Sdk.Common.ChannelType.Shopee:
                    {
                        return new PartnerDelivery
                        {
                            Code = "SHOPEE",
                            Name = "SHOPEE"
                        };
                    }

                case (byte)Sdk.Common.ChannelType.Lazada:
                    {
                        return new PartnerDelivery
                        {
                            Code = "LAZADA",
                            Name = "LAZADA"
                        };
                    }

                case (byte)Sdk.Common.ChannelType.Tiki:
                    {
                        return new PartnerDelivery
                        {
                            Code = "TIKI",
                            Name = "TIKI"
                        };
                    }

                case (byte)Sdk.Common.ChannelType.Sendo:
                    {
                        return new PartnerDelivery
                        {
                            Code = "SENDO",
                            Name = "SENDO"
                        };
                    }
            }
            return null;
        }

        private async Task<bool> ValidateProduct(List<ProductBranchDTO> kvProducts, Order order, int retailerId, int branchId, long channelId, string orderId, byte channelType, KvInternalContext context, List<ProductMapping> mappings, LogObjectMicrosoftExtension loggerExtension)
        {
            loggerExtension.LogInfo($"Retailer: {retailerId} - Channel: {channelId} - OrderId: {orderId} - Action: CreateOrder : {kvProducts.ToSafeJson()}");
            var productLog = new StringBuilder();
            var errorMessage = new StringBuilder();
            var isValid = true;
            foreach (var detail in order.OrderDetails)
            {
                var product = kvProducts.FirstOrDefault(x => x.Id == detail.ProductId);

                detail.ProductName = product?.FullName ?? detail.ProductChannelName;
                var channelTrackKey = !string.IsNullOrEmpty(detail.CommonProductChannelId) ? detail.CommonProductChannelId : detail.ProductChannelSku;

                if (product == null)
                {
                    errorMessage.Append($"Hàng hóa {detail.ProductChannelName} ({channelTrackKey}) chưa liên kết với hàng hóa nào trên Demo; ");
                    productLog.AppendFormat($"- {detail.ProductChannelSku ?? detail.CommonProductChannelId} ({channelTrackKey}): chưa xác định trên Demo <br/>");
                    isValid = false;
                }
                else
                {
                    var mapping = mappings.FirstOrDefault(x => x.ChannelId == channelId && ((!string.IsNullOrEmpty(detail.CommonProductChannelId) && x.CommonProductChannelId == detail.CommonProductChannelId) || x.ProductChannelSku == detail.ProductChannelSku));
                    if (mapping == null)
                    {
                        errorMessage.Append($"Hàng hóa {detail.ProductChannelName} ({channelTrackKey}) chưa liên kết với hàng hóa nào trên Demo; ");
                        productLog.AppendFormat($"- [ProductCode]{detail.ProductCode}[/ProductCode]: liên kết hàng hóa không tồn tại <br/>");
                        isValid = false;
                    }
                    else if (product.isDeleted == true)
                    {
                        errorMessage.Append($"Hàng hóa {detail.ProductChannelName} ({channelTrackKey}) chưa liên kết với hàng hóa nào trên Demo; ");
                        productLog.AppendFormat($"- [ProductCode]{detail.ProductCode}[/ProductCode]: đã bị xóa <br/>");
                        isValid = false;
                    }
                }
            }
            if (!isValid)
            {
                if ((byte)OrderState.Void == order.Status || (byte)OrderState.Void == order.NewStatus)
                {
                    await RemoveOrderSyncError(context, orderId, channelId);
                    loggerExtension.LogInfo($"Retailer: {retailerId} - Channel: {channelId} - OrderId: {orderId} - Remove order sync error");
                }
                else
                {
                    await SaveOrderSyncError(context, order, orderId, channelId, channelType, errorMessage.ToString(), productLog.ToString());
                    loggerExtension.LogInfo($"Retailer: {retailerId} - Channel: {channelId} - OrderId: {orderId} - Save order sync error");
                }
                return false;
            }
            return true;
        }

        private async Task SaveOrderSyncError(KvInternalContext context, Order order, string channelOrderId, long channelId, byte channelType, string errorMessage, string auditTrailMessage)
        {
            var existOrder = await _orderMongoService.GetByOrderId(channelOrderId, context.BranchId, channelId);
            if (existOrder != null)
            {
                existOrder.ErrorMessage = errorMessage;
                existOrder.OrderDetails = order.OrderDetails;
                await _orderMongoService.UpdateAsync(existOrder.Id, existOrder);
            }
            #region Audit Trail
            var log = new AuditTrailLog
            {
                FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channelType),
                Action = (int)AuditTrailAction.OrderIntergate,
                CreatedDate = DateTime.Now,
                BranchId = order.BranchId,
                Content = $"Tạo đơn đặt hàng KHÔNG thành công: [OrderCode]{order.Code}[/OrderCode] (cho đơn đặt hàng: {channelOrderId}), lý do: <br/>{auditTrailMessage}"
            };
            await _auditTrailInternalClient.AddLogAsync(context, log);
            #endregion
        }

        private async Task RemoveOrderSyncError(KvInternalContext context, string channelOrderId, long channelId)
        {
            try
            {
                var existOrder = await _orderMongoService.GetByOrderId(channelOrderId, context.BranchId, channelId);
                if (existOrder != null)
                {
                    await _orderMongoService.RemoveAsync(existOrder.Id);
                }

            }
            catch (Exception e)
            {
                throw;
            }
        }

        protected virtual Task HandleNewOrderDetailChange(T data, IBaseClient client, Demo.OmniChannel.MongoDb.Order order, Domain.Model.OmniChannel omniChannel, LogObjectMicrosoftExtension logInfo, Demo.OmniChannel.ChannelClient.Models.Platform platform)
        {
            return Task.CompletedTask;
        }

        protected virtual async Task<List<ProductMapping>> GetMappingInfo(Demo.OmniChannel.MongoDb.Order order, T data)
        {
            var channelProductIds = order.OrderDetails.Where(p => !string.IsNullOrEmpty(p.CommonProductChannelId) && p.CommonProductChannelId != "0").Select(x => x.CommonProductChannelId).ToList();
            var mappings = await _productMappingService.GetByChannelProductIds(data.RetailerId, data.ChannelId, channelProductIds, isStringItemId: ConvertHelper.CheckUseStringItemId(data.ChannelType));
            foreach (var detail in order.OrderDetails)
            {
                var mapping = mappings?.FirstOrDefault(x => !string.IsNullOrEmpty(detail.CommonProductChannelId) && x.CommonProductChannelId == detail.CommonProductChannelId);
                detail.ProductId = mapping?.ProductKvId ?? 0;
                detail.ProductCode = mapping?.ProductKvSku ?? string.Empty;
            }
            return mappings;
        }

        protected virtual async Task PublishCreateInvoiceQueue(SyncErrorOrderMessage data, Order order, byte channelType, Domain.Model.Order existOrder, bool hasCreateOrder, LogObjectMicrosoftExtension loggerExtension)
        {
            if (hasCreateOrder)
            {
                // Đơn hàng mới và Hóa đơn mới tạo trong thời gian quá gần nhau gây ra trường hợp đồng bộ tồn kho cũ
                var millisecondsDelay = NumberHelper.GetValueOrDefault(Settings?.Get<int>("ChannelProductLock"), 5) * 1000;
                loggerExtension.LogInfo($"Delay create Invoice after: {millisecondsDelay}ms. RetailerId: {data.RetailerId}. OrderId: {existOrder.Code}");
                await Task.Delay(millisecondsDelay);
            }
        }
        private void GetKvLocationWard(Order order)
        {
            Location kvLocation = null;
            if (!string.IsNullOrEmpty(order?.OrderDelivery.ChannelState) && !string.IsNullOrEmpty(order?.OrderDelivery.ChannelCity))
            {
                var state = Regex.Replace(StringHelper.ConvertToUnsign(order?.OrderDelivery.ChannelState), @"\s+", "");
                state = !string.IsNullOrEmpty(state) && state.StartsWith("TP.") ? Regex.Replace(state, "TP.", "") : state;
                var city = Regex.Replace(StringHelper.ConvertToUnsign(order?.OrderDelivery.ChannelCity), @"\s+", "");
                kvLocation = CacheClient.Get<Location>(string.Format(KvConstant.LocationCacheKey, $"{state}-{city}"));
                order.OrderDelivery.LocationId = kvLocation?.Id;
                order.OrderDelivery.LocationName =
                    kvLocation != null ? kvLocation.Name : order.OrderDelivery.ChannelState;
            }
            else if (!string.IsNullOrEmpty(order.OrderDelivery.LocationName))
            {
                var locationKey = Regex.Replace(StringHelper.ConvertToUnsign(order.OrderDelivery.LocationName), @"\s+", "");
                kvLocation = CacheClient.Get<Location>(string.Format(KvConstant.LocationCacheKey, locationKey));
                order.OrderDelivery.LocationId = kvLocation?.Id;
            }
            else
            {
                order.OrderDelivery.LocationName = order.OrderDelivery.ChannelState;
            }

            if (kvLocation != null && (!string.IsNullOrEmpty(order.OrderDelivery?.ChannelWard) && kvLocation.Id > 0))
            {
                var ward = Regex.Replace(StringHelper.ConvertToUnsign(order?.OrderDelivery.ChannelWard), @"\s+", "");
                var kvWard = CacheClient.Get<Wards>(string.Format(KvConstant.WardCacheKey, kvLocation?.Id, ward));
                order.OrderDelivery.WardName = kvWard != null ? kvWard.Name : order.OrderDelivery.ChannelWard;
            }
            else
            {
                order.OrderDelivery.WardName = order.OrderDelivery.ChannelWard;
            }
        }
        private async Task RemoveErrorOrder(Order order)
        {
            if (order != null)
            {
                await _orderMongoService.RemoveAsync(order.Id);

                var invoiceInMongo = await _invoiceMongoService.GetByOrderId(order.OrderId, order.BranchId, order.ChannelId);
                if (invoiceInMongo != null)
                {
                    foreach (var invoice in invoiceInMongo)
                    {
                        await _invoiceMongoService.RemoveAsync(invoice.Id);
                    }
                }
            }
        }
        private async Task RemoveMongoRetryOrder(string orderId, int branchId, long channelId, int retailerId)
        {
            var existRetryOrder = await _retryOrderMongoService.GetByOrderId(orderId, channelId, branchId, retailerId);
            if (existRetryOrder != null)
            {
                await _retryOrderMongoService.RemoveAsync(existRetryOrder.Id);
            }

            var existRetryInvoice = await _retryInvoiceMongoService.GetByOrderId(orderId, channelId, branchId, retailerId);
            if (existRetryInvoice != null)
            {
                await _retryInvoiceMongoService.RemoveAsync(existRetryInvoice.Id);
            }
        }

        private async Task SetExtra(IDbConnection dbConnection, Domain.Model.OmniChannel channel, OmniChannelCore.Api.Sdk.Models.Order kvOrder)
        {
            channel.BasePriceBookId = channel.BasePriceBookId ?? 0;
            if (channel.BasePriceBookId > 0)
            {
                var priceBookRepository = new PriceBookRepository(dbConnection);
                var priceBook = await priceBookRepository.SingleAsync(p => p.RetailerId == channel.RetailerId && p.Id == channel.BasePriceBookId);
                if (priceBook != null && (priceBook.IsActive) && (priceBook.isDeleted != true) && priceBook.StartDate < DateTime.Now && priceBook.EndDate > DateTime.Now)
                {
                    kvOrder.Extra = new ChannelClient.Models.Extra
                    {
                        PriceBookId = new ChannelClient.Models.PriceBook
                        {
                            Id = priceBook.Id,
                            Name = priceBook.Name
                        }
                    }.ToJson();
                }
                else
                {
                    kvOrder.Extra = new ChannelClient.Models.Extra
                    {
                        PriceBookId = new ChannelClient.Models.PriceBook
                        {
                            Id = 0
                        }
                    }.ToJson();
                }
            }
            else
            {
                kvOrder.Extra = new ChannelClient.Models.Extra
                {
                    PriceBookId = new ChannelClient.Models.PriceBook
                    {
                        Id = 0
                    }
                }.ToJson();
            }
        }

        private async Task AddOrUpdateRetryOrder(Domain.Model.OmniChannel channel, string kvEntities, Order order, bool isDbException = false)
        {
            var existRetryOrder = await _retryOrderMongoService.GetByOrderId(order.OrderId, channel.Id, order.BranchId, channel.RetailerId);
            if (existRetryOrder != null)
            {
                existRetryOrder.ModifiedDate = DateTime.Now;
                existRetryOrder.RetryCount = 1;
                await _retryOrderMongoService.UpdateAsync(existRetryOrder.Id, existRetryOrder);
            }
            else
            {
                await _retryOrderMongoService.AddSync(new RetryOrder
                {
                    ChannelId = channel.Id,
                    ChannelType = channel.Type,
                    BranchId = channel.BranchId,
                    KvEntities = kvEntities,
                    RetailerId = channel.RetailerId,
                    OrderId = order.OrderId,
                    Order = null,
                    PriceBookId = (channel.PriceBookId ?? channel.BasePriceBookId) ?? 0,
                    OrderStatus = order.ChannelStatus,
                    CreatedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    IsDbException = isDbException,
                    RetryCount = 1
                });
            }
        }


        #region PRIVATE

        private async Task WriteLogAsync(OmniChannelCore.Api.Sdk.Models.Order kvOrder, byte channelType, Order order, Domain.Model.OmniChannel channel, KvInternalContext coreContext)
        {
            var productDetail = new StringBuilder();
            double total = 0;
            if (kvOrder.OrderDetails != null && kvOrder.OrderDetails.Any())
            {
                productDetail.Append(", bao gồm:<div>");
                foreach (var item in kvOrder.OrderDetails)
                {
                    total += ((double)item.Price - (double)item.Discount.GetValueOrDefault()) * item.Quantity;
                    productDetail.Append(
                        $"- [ProductCode]{item.ProductCode}[/ProductCode] : {StringHelper.Normallize(item.Quantity)}*{StringHelper.NormallizeWfp((double)item.Price - (double)item.Discount.GetValueOrDefault())}<br>");
                    var productWarrantys = kvOrder.InvoiceWarranties?.Where(x => x.InvoiceDetailUuid == item.Uuid).ToList();
                    var getWarrantys = GetWarrantysLog(item, productWarrantys);
                    productDetail.Append(string.IsNullOrEmpty(getWarrantys.ToString()) ? string.Empty : $"{getWarrantys}<br>");
                }

                productDetail.Append("</div>");
            }

            var log = new AuditTrailLog
            {
                FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channelType),
                Action = (int)AuditTrailAction.OrderIntergate,
                CreatedDate = DateTime.Now,
                BranchId = kvOrder.BranchId,
                Content =
                    $"Tạo đơn đặt hàng: [OrderCode]{kvOrder.Code}[/OrderCode] từ đơn đặt hàng {order.ChannelOrder} trên {channel.Name}, giá trị: {StringHelper.NormallizeWfp(total)}, thời gian: {kvOrder.PurchaseDate:dd/MM/yyyy HH:mm:ss}{productDetail}"
            };
            await _auditTrailInternalClient.AddLogAsync(coreContext, log);
        }

        public async Task<(Domain.Model.Customer newCustomer, bool isError, bool isRetailerExpireException)> CreateCustomerAsync(Order kvOrder,
           CustomerRepository customerRepository, BaseMessage data, bool isManagerCustomerByBranch, KvInternalContext kvInternalContext, LogObjectMicrosoftExtension loggerExtension)
        {
            var newCustomer = new Domain.Model.Customer();
            var phone = PhoneNumberHelper.GetPerfectContactNumber(kvOrder.CustomerPhone);

            //Kiểm tra số điện thoại có hợp lệ. Nếu không hợp lệ sẽ tạo khách lẻ
            if (string.IsNullOrEmpty(kvOrder.CustomerCode) && !PhoneNumberHelper.IsValidContactNumber(phone, RegionCode.VN) && data.ChannelType == (byte)ChannelTypeEnum.Shopee)
            {
                loggerExtension.Description = $"KHACHLE Tạo khách lẻ OrderSn : {kvOrder.Code}; Phone before: {kvOrder.CustomerPhone}; Phone after parse: {phone}; Customer code: {kvOrder.CustomerCode}; Customer name: {kvOrder.CustomerName}";
                loggerExtension.LogInfo(true);
                return (newCustomer, false, false);
            }

            using (var cli = _kvLockRedis.GetLockFactory())
            {
                var lockAddCustomerKey = phone;
                if (string.IsNullOrEmpty(phone))
                {
                    lockAddCustomerKey = kvOrder.Code;
                }

                using (cli.AcquireLock(string.Format(KvConstant.LockAddCustomer, lockAddCustomerKey),
                    TimeSpan.FromSeconds(60)))
                {
                    Domain.Model.Customer existsCustomer = await _GetCustomerAsync(customerRepository, data.RetailerId, data.BranchId, phone, kvOrder.CustomerCode, isManagerCustomerByBranch, loggerExtension);

                    newCustomer.ContactNumber = phone;

                    if (existsCustomer == null)
                    {
                        try
                        {
                            var customer = new Customer
                            {
                                Code = kvOrder.CustomerCode,
                                ContactNumber = phone,
                                Name = kvOrder.CustomerName,
                                Address = kvOrder.CustomerAddress,
                                BranchId = data.BranchId,
                                RetailerId = data.RetailerId
                            };
                            var customerId =
                                await _customerInternalClient.CreateCustomerAsync(kvInternalContext,
                                    customer);

                            if (customerId < 1)
                            {
                                return (newCustomer, true, false);
                            }

                            newCustomer.Id = customerId;
                            newCustomer.Code = kvOrder.CustomerCode;
                            newCustomer.Name = kvOrder.CustomerName;
                        }
                        catch (Exception ex)
                        {
                            loggerExtension.Action = ExceptionType.CreateCustomerError;
                            loggerExtension.ResponseObject = ex.Message + $"{ex.StackTrace}";
                            loggerExtension.LogInfo();
                            if (ex is KvRetailerExpireException)
                            {
                                return (newCustomer, false, true);
                            }

                            existsCustomer = await _GetCustomerAsync(customerRepository, data.RetailerId, data.BranchId, phone, kvOrder.CustomerCode, isManagerCustomerByBranch, loggerExtension);

                            if (existsCustomer != null)
                            {
                                newCustomer.Id = existsCustomer.Id;
                                newCustomer.Code = existsCustomer.Code;
                                newCustomer.Name = existsCustomer.Name;
                            }
                        }
                    }
                    else
                    {
                        newCustomer.Id = existsCustomer.Id;
                        newCustomer.Code = existsCustomer.Code;
                        newCustomer.Name = existsCustomer.Name;
                    }
                }


            }
            return (newCustomer, false, false);
        }

        private StringBuilder GetWarrantysLog(OrderDetail orderDetail,
            List<OmniChannelCore.Api.Sdk.Models.InvoiceWarranties> invoiceWarrantys)
        {
            var warrantysLog = new StringBuilder();

            var checkProductIsCombo = !invoiceWarrantys.Where(x => x.ProductId == orderDetail.ProductId).Any();
            var productWarrantysGroup = invoiceWarrantys.GroupBy(x => new { x.ProductId, x.ProductCode }).ToList();
            foreach (var group in productWarrantysGroup)
            {
                var guarrantee = new StringBuilder();
                var mainGuarrantee = string.Empty;
                var productWarrantys = invoiceWarrantys.Where(x => x.ProductId == group.Key.ProductId).ToList();

                productWarrantys?.ForEach(w =>
                {
                    if (w.WarrantyType == (int)WarrantyType.Maintenance)
                    {
                        mainGuarrantee = string.Format("Định kỳ bảo trì: {0} {1} ({2}) đến {3}",
                            w.NumberTime, SystemHelper.GetWarrantyTimeTypeText(w.TimeType).ToLower(),
                            w.Description, ((DateTime)w.ExpireDate).ToString("dd/MM/yyyy"));
                    }
                    else
                    {
                        var guarranteelog = string.Format("{0} {1} ({2}) đến {3}",
                           w.NumberTime, SystemHelper.GetWarrantyTimeTypeText(w.TimeType).ToLower(),
                           w.Description, ((DateTime)w.ExpireDate).ToString("dd/MM/yyyy"));

                        guarrantee.Append(string.IsNullOrEmpty(guarrantee.ToString())
                            ? $"Bảo hành: {guarranteelog}"
                            : $", {guarranteelog}");
                    }
                });
                if (checkProductIsCombo)
                {
                    warrantysLog.Append($"Thành phần bảo hành: [ProductCode]{group.Key.ProductCode}[/ProductCode]<br />");
                }
                warrantysLog.Append(string.IsNullOrEmpty(guarrantee.ToString()) ? string.Empty : $"&nbsp;&nbsp;{guarrantee}<br>");
                warrantysLog.Append(string.IsNullOrEmpty(mainGuarrantee) ? string.Empty : $"&nbsp;&nbsp;{mainGuarrantee}<br>");
            }

            return warrantysLog;
        }
        #endregion

    }
}
﻿using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.OrderService.OrderDomain.Interfaces;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Auth;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System.Collections.Generic;

namespace Demo.OmniChannel.OrderService.Impls
{
    public class LazadaCreateInvoiceService : CreateInvoiceService<LazadaCreateInvoiceMessage>
    {
        private readonly IMessageService _mqService;

        public LazadaCreateInvoiceService(
            IIntegrationEventService integrationEventService,
            IAppSettings settings,
            IProductMappingService productMappingService,
            IRetryInvoiceMongoService retryInvoiceMongoService,
            IInvoiceMongoService invoiceMongoService,
            IInvoiceInternalClient invoiceInternalClient,
            IOrderInternalClient orderInternalClient,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IAuditTrailInternalClient auditTrailInternalClient,
            IMessageService mqService,
            IOmniChannelAuthService channelAuthService,
            KvRedisConfig mqRedisConfig,
            IDeliveryInfoInternalClient deliveryInfoInternalClient,
            IDeliveryPackageInternalClient deliveryPackageInternalClient,
            IKvLockRedis kvLockRedis,
            ICustomerInternalClient customerInternalClient,
            ISurChargeInternalClient surChargeInternalClient,
            IOmniChannelSettingService omniChannelSettingService,
            ISurChargeBranchInternalClient surChargeBranchInternalClient,
            ICreateInvoiceDomainService createInvoiceDomainService,
            IOmniChannelPlatformService omniChannelPlatformService,
            ILogger<LazadaCreateInvoiceService> logger) : base(
            integrationEventService,
            settings,
            productMappingService,
            retryInvoiceMongoService,
            invoiceMongoService,
            invoiceInternalClient,
            orderInternalClient,
            channelClient,
            cacheClient,
            dbConnectionFactory,
            auditTrailInternalClient,
            mqService,
            channelAuthService,
            mqRedisConfig,
            deliveryInfoInternalClient,
            deliveryPackageInternalClient,
            kvLockRedis,
            customerInternalClient, 
            surChargeInternalClient,
            omniChannelSettingService,
            surChargeBranchInternalClient,
            createInvoiceDomainService,
            logger,
            omniChannelPlatformService)
        {
            ChannelType = ChannelType.Lazada;
            _mqService = mqService;
        }

        protected override void AfterCreateInvoice(KvInvoice invoice, CreateInvoiceMessage data, ExecutionContext context, Domain.Model.OmniChannel channel)
        {
            using (var mq = _mqService.MessageFactory.CreateMessageProducer())
            {

                mq.Publish(new LazadaSyncPaymentTransactionMessage
                {
                    OrderSnLst = new List<string> { data.OrderId },
                    ChannelType = (byte)Demo.OmniChannel.ShareKernel.Common.ChannelType.Lazada,
                    RetailerId = invoice.RetailerId,
                    BranchId = invoice.BranchId
                });

                mq.Publish(new LazadaProcessFeeTransactionMessage
                {
                    OrderSn = data.OrderId,
                    ChannelType = (byte)Demo.OmniChannel.ShareKernel.Common.ChannelType.Lazada,
                    RetailerId = invoice.RetailerId,
                    BranchId = invoice.BranchId,
                    KvEntities = data.KvEntities
                });
            }
        }
    }
}
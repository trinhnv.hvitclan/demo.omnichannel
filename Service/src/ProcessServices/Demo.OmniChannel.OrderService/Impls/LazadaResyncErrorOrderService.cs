﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.RequestDTO;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.OrderService.OrderDomain.Interfaces;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Auth;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Order = Demo.OmniChannel.MongoDb.Order;

namespace Demo.OmniChannel.OrderService.Impls
{
    public class LazadaSyncOrderService : SyncOrderService<LazadaSyncErrorOrderMessage>
    {
        private readonly IProductMappingService _productMappingService;
        private readonly IOmniChannelAuthService _channelAuthService;
        private readonly IMessageService _mqService;
        public LazadaSyncOrderService(
            IAppSettings settings,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IProductMappingService productMappingService,
            ICustomerInternalClient customerInternalClient,
            IPartnerDeliveryInternalClient partnerInternalClient,
            IOrderInternalClient orderInternalClient,
            IOrderMongoService orderMongoService,
            IAuditTrailInternalClient auditTrailInternalClient,
            IMessageService mqService,
            IInvoiceMongoService invoiceMongoService,
            IRetryOrderMongoService retryOrderMongoService,
            IRetryInvoiceMongoService retryInvoiceMongoService,
            IKvLockRedis kvLockRedis,
            IOmniChannelAuthService channelAuthService,
            KvRedisConfig mqRedisConfig,
            IChannelBusiness channelBusiness,
            ISendoLocationMongoService sendoLocationMongoService,
            ISurChargeInternalClient surChargeInternalClient,
            IOmniChannelSettingService omniChannelSettingService,
            IOmniChannelPlatformService omniChannelPlatformService,
            ICreateOrderDomainService createOrderDomainService,
            ILogger<LazadaSyncOrderService> logger) : base(settings,
            channelClient,
            cacheClient,
            dbConnectionFactory,
            productMappingService,
            customerInternalClient,
            partnerInternalClient,
            orderInternalClient,
            orderMongoService,
            auditTrailInternalClient,
            mqService,
            invoiceMongoService,
            retryOrderMongoService,
            retryInvoiceMongoService,
            kvLockRedis,
            channelAuthService,
            mqRedisConfig,
            channelBusiness,
            sendoLocationMongoService,
            surChargeInternalClient,
            omniChannelSettingService,
            omniChannelPlatformService,
            createOrderDomainService,
            logger)
        {
            ChannelType = Sdk.Common.ChannelType.Lazada;
            _productMappingService = productMappingService;
            _channelAuthService = channelAuthService;
            _mqService = mqService;
        }


        protected override async Task HandleNewOrderDetailChange(LazadaSyncErrorOrderMessage data, IBaseClient client,
            Order order, Domain.Model.OmniChannel omniChannel, LogObjectMicrosoftExtension logInfo,
            ChannelClient.Models.Platform platform)
        {
            var auth = await _channelAuthService.GetChannelAuth(omniChannel, data.LogId);
            var authKey = new Demo.OmniChannel.ChannelClient.Models.ChannelAuth
            {
                AccessToken = auth.AccessToken,
                RefreshToken = auth.RefreshToken,
                ShopId = auth.ShopId
            };
            var createOrderRequest = new CreateOrderRequest
            {
                BranchId = data.BranchId,
                OrderId = order.OrderId,
                Order = string.Empty,
                RetryCount = 0,
                PriceBookId = omniChannel.BasePriceBookId ?? 0
            };
            var orderDetailGetResponse = await client.GetOrderDetail(data.RetailerId, data.ChannelId, authKey, createOrderRequest, logInfo, platform);
            var kvOrder = orderDetailGetResponse.Order;
            if (orderDetailGetResponse.IsSuccess && kvOrder.OrderDetails.Count != order.OrderDetails.Count)
            {
                order.OrderDetails = kvOrder.OrderDetails.Select(item => new MongoDb.OrderDetail()
                {
                    ProductChannelSku = item.ProductChannelSku,
                    Quantity = item.Quantity,
                    Price = item.Price,
                    ProductChannelName = item.ProductChannelName,
                    Discount = item.Discount ?? 0,
                    Uuid = StringHelper.GenerateUUID()
                }).ToList();
            }
        }

        protected override void AfterCreateOrder(Order order, SyncErrorOrderMessage data, Demo.OmniChannel.ShareKernel.Auth.ExecutionContext context, Domain.Model.OmniChannel channel)
        {
            using (var mq = _mqService.MessageFactory.CreateMessageProducer())
            {

                mq.Publish(new LazadaSyncPaymentTransactionMessage
                {
                    OrderSnLst = new List<string> { order.OrderId },
                    ChannelType = (byte)Demo.OmniChannel.ShareKernel.Common.ChannelType.Lazada,
                    RetailerId = order.RetailerId,
                    BranchId = order.BranchId
                });

                mq.Publish(new LazadaProcessFeeTransactionMessage
                {
                    OrderSn = order.OrderId,
                    ChannelType = (byte)Demo.OmniChannel.ShareKernel.Common.ChannelType.Lazada,
                    RetailerId = order.RetailerId,
                    BranchId = order.BranchId,
                    KvEntities = data.KvEntities
                });
            }
        }

        protected override async Task PublishCreateInvoiceQueue(SyncErrorOrderMessage data, Order order, byte channelType, Domain.Model.Order existOrder, bool hasCreateOrder, LogObjectMicrosoftExtension loggerExtension)
        {
            await base.PublishCreateInvoiceQueue(data, order, channelType, existOrder, hasCreateOrder, loggerExtension);
            using (var mq = MessageService.MessageFactory.CreateMessageProducer())
            {
                var createInvoice = new LazadaCreateInvoiceMessage
                {
                    ChannelId = order.ChannelId,
                    ChannelType = channelType,
                    BranchId = order.BranchId,
                    KvEntities = data.KvEntities,
                    RetailerId = order.RetailerId,
                    OrderId = order.OrderId,
                    Order = data.Order,
                    KvOrder = existOrder.ToSafeJson(),
                    KvOrderId = existOrder.Id,
                    LogId = data.LogId
                };
                mq.Publish(createInvoice);
            }
        }

        protected async override Task<List<MongoDb.ProductMapping>> GetMappingInfo(Order order, LazadaSyncErrorOrderMessage data)
        {
            List<MongoDb.ProductMapping> mappings;
            var channelProductIds = order.OrderDetails.Where(p => !string.IsNullOrEmpty(p.CommonProductChannelId) && p.CommonProductChannelId != "0").Select(x => x.CommonProductChannelId).ToList();
            if (channelProductIds.Count > 0)
            {
                mappings = await _productMappingService.GetByChannelProductIds(data.RetailerId, data.ChannelId, channelProductIds, isStringItemId: ConvertHelper.CheckUseStringItemId((byte)ChannelType));
            }
            else
            {
                var productSkus = order.OrderDetails.Where(p => !string.IsNullOrEmpty(p.ProductChannelSku)).Select(p => p.ProductChannelSku).ToList();
                mappings = await _productMappingService.GetByChannelProductSkus(data.RetailerId, data.ChannelId, productSkus);
            }

            foreach (var detail in order.OrderDetails)
            {
                var mapping = mappings?.FirstOrDefault(x => (!string.IsNullOrEmpty(detail.CommonProductChannelId) && x.CommonProductChannelId == detail.CommonProductChannelId) || x.ProductChannelSku.Equals(detail.ProductChannelSku));

                detail.ProductId = mapping?.ProductKvId ?? 0;
                detail.ProductCode = mapping?.ProductKvSku ?? string.Empty;
            }
            return mappings;
        }
    }


    public class LazadaSyncInvoiceService : SyncInvoiceService<LazadaSyncErrorInvoiceMessage>
    {
        private readonly IMessageService _mqService;

        public LazadaSyncInvoiceService(IAppSettings settings,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IProductMappingService productMappingService,
            IInvoiceInternalClient invoiceInternalClient,
            IInvoiceMongoService invoiceMongoService,
            IAuditTrailInternalClient auditTrailInternalClient,
            IMessageService mqService,
            IOmniChannelAuthService channelAuthService,
            IRetryInvoiceMongoService retryInvoiceMongoService,
            IOrderInternalClient orderInternalClient,
            KvRedisConfig mqRedisConfig,
            IChannelBusiness channelBusiness,
            IDeliveryInfoInternalClient deliveryInfoInternalClient,
            IDeliveryPackageInternalClient deliveryPackageInternalClient,
            IKvLockRedis kvLockRedis,
            ICustomerInternalClient customerInternalClient,
            IOmniChannelSettingService omniChannelSettingService,
            ISurChargeInternalClient surChargeInternalClient,
            ICreateInvoiceDomainService createInvoiceDomainService,
            IOmniChannelPlatformService omniChannelPlatformService,
            ILogger<LazadaSyncInvoiceService> logger) : base(settings,
            channelClient,
            cacheClient,
            dbConnectionFactory,
            productMappingService,
            invoiceInternalClient,
            invoiceMongoService,
            auditTrailInternalClient,
            mqService, channelAuthService,
            retryInvoiceMongoService,
            orderInternalClient,
            mqRedisConfig,
            channelBusiness,
            deliveryInfoInternalClient,
            deliveryPackageInternalClient,
            kvLockRedis,
            customerInternalClient,
            omniChannelSettingService,
            surChargeInternalClient,
            createInvoiceDomainService,
            omniChannelPlatformService,
            logger)
        {
            ChannelType = Sdk.Common.ChannelType.Lazada;
            _mqService = mqService;
        }

        protected override void AfterCreateInvoice(MongoDb.Invoice invoice, SyncErrorInvoiceMessage data, ExecutionContext context, Domain.Model.OmniChannel channel, LogObjectMicrosoftExtension loggerExtension)
        {
            using (var mq = _mqService.MessageFactory.CreateMessageProducer())
            {

                mq.Publish(new LazadaSyncPaymentTransactionMessage
                {
                    OrderSnLst = new List<string> { invoice.ChannelOrderId },
                    ChannelType = (byte)Demo.OmniChannel.ShareKernel.Common.ChannelType.Lazada,
                    RetailerId = invoice.RetailerId,
                    BranchId = invoice.BranchId
                });

                mq.Publish(new LazadaProcessFeeTransactionMessage
                {
                    OrderSn = invoice.ChannelOrderId,
                    ChannelType = (byte)Demo.OmniChannel.ShareKernel.Common.ChannelType.Lazada,
                    RetailerId = invoice.RetailerId,
                    BranchId = invoice.BranchId,
                    KvEntities = data.KvEntities
                });
            }
        }
    }
}
﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.OrderService.OrderDomain.Interfaces;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Auth;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System.Threading.Tasks;

namespace Demo.OmniChannel.OrderService.Impls
{
    public class TikiSyncOrderService : SyncOrderService<TikiSyncErrorOrderMessage>
    {
        public TikiSyncOrderService(IAppSettings settings,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IProductMappingService productMappingService,
            ICustomerInternalClient customerInternalClient,
            IPartnerDeliveryInternalClient partnerInternalClient,
            IOrderInternalClient orderInternalClient,
            IOrderMongoService orderMongoService,
            IAuditTrailInternalClient auditTrailInternalClient,
            IMessageService mqService,
            IInvoiceMongoService invoiceMongoService,
            IRetryOrderMongoService retryOrderMongoService,
            IRetryInvoiceMongoService retryInvoiceMongoService,
            IKvLockRedis kvLockRedis,
            IOmniChannelAuthService channelAuthService,
            KvRedisConfig mqRedisConfig,
            IChannelBusiness channelBusiness,
            ISendoLocationMongoService sendoLocationMongoService,
            ISurChargeInternalClient surChargeInternalClient,
            IOmniChannelSettingService omniChannelSettingService,
            IOmniChannelPlatformService omniChannelPlatformService,
            ICreateOrderDomainService createOrderDomainService,
            ILogger<TikiSyncOrderService> logger
            ) : base(settings,
            channelClient,
            cacheClient,
            dbConnectionFactory,
            productMappingService,
            customerInternalClient,
            partnerInternalClient,
            orderInternalClient,
            orderMongoService,
            auditTrailInternalClient,
            mqService,
            invoiceMongoService,
            retryOrderMongoService,
            retryInvoiceMongoService,
            kvLockRedis,
            channelAuthService,
            mqRedisConfig,
            channelBusiness,
            sendoLocationMongoService,
            surChargeInternalClient,
            omniChannelSettingService,
            omniChannelPlatformService,
            createOrderDomainService,
            logger)
        {
            ChannelType = Sdk.Common.ChannelType.Tiki;
        }

        protected async override Task PublishCreateInvoiceQueue(SyncErrorOrderMessage data, MongoDb.Order order, byte channelType, Domain.Model.Order existOrder, bool hasCreateOrder, LogObjectMicrosoftExtension loggerExtension)
        {
            await base.PublishCreateInvoiceQueue(data, order, channelType, existOrder, hasCreateOrder, loggerExtension);
            using (var mq = MessageService.MessageFactory.CreateMessageProducer())
            {
                var createInvoice = new TikiCreateInvoiceMessage
                {
                    ChannelId = order.ChannelId,
                    ChannelType = channelType,
                    BranchId = order.BranchId,
                    KvEntities = data.KvEntities,
                    RetailerId = order.RetailerId,
                    OrderId = order.OrderId,
                    Order = data.Order,
                    KvOrder = existOrder.ToSafeJson(),
                    KvOrderId = existOrder.Id,
                    LogId = data.LogId
                };
                mq.Publish(createInvoice);
            }

        }
    }

    public class TikiSyncInvoiceService : SyncInvoiceService<TikiSyncErrorInvoiceMessage>
    {
        public TikiSyncInvoiceService(IAppSettings settings,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IProductMappingService productMappingService,
            IInvoiceInternalClient invoiceInternalClient,
            IInvoiceMongoService invoiceMongoService,
            IAuditTrailInternalClient auditTrailInternalClient,
            IMessageService mqService,
            IOmniChannelAuthService channelAuthService,
            IRetryInvoiceMongoService retryInvoiceMongoService,
            IOrderInternalClient orderInternalClient,
            KvRedisConfig mqRedisConfig,
            IChannelBusiness channelBusiness,
            IDeliveryInfoInternalClient deliveryInfoInternalClient,
            IDeliveryPackageInternalClient deliveryPackageInternalClient,
            IKvLockRedis kvLockRedis,
            ICustomerInternalClient customerInternalClient,
            IOmniChannelSettingService omniChannelSettingService,
            ISurChargeInternalClient surChargeInternalClient,
            ICreateInvoiceDomainService createInvoiceDomainService,
            IOmniChannelPlatformService omniChannelPlatformService,
            ILogger<TikiSyncInvoiceService> logger) : base(settings,
            channelClient,
            cacheClient,
            dbConnectionFactory,
            productMappingService,
            invoiceInternalClient,
            invoiceMongoService,
            auditTrailInternalClient,
            mqService, channelAuthService,
            retryInvoiceMongoService,
            orderInternalClient,
            mqRedisConfig,
            channelBusiness,
            deliveryInfoInternalClient,
            deliveryPackageInternalClient,
            kvLockRedis,
            customerInternalClient,
            omniChannelSettingService,
            surChargeInternalClient,
            createInvoiceDomainService,
            omniChannelPlatformService,
            logger)
        {
            ChannelType = Sdk.Common.ChannelType.Tiki;
        }

        protected override void AfterCreateInvoice(MongoDb.Invoice invoice, SyncErrorInvoiceMessage data, ExecutionContext context, Domain.Model.OmniChannel channel, LogObjectMicrosoftExtension loggerExtension)
        {
            loggerExtension.Action = "PushTiktokSyncPaymentTransaction";
            using (var mq = MessageService.MessageFactory.CreateMessageProducer())
            {
                var message = new TiktokSyncPaymentTransactionMessage
                {
                    RetailerId = data.RetailerId,
                    OrderId = invoice.ChannelOrderId,
                    DataSettlements = null,
                    ChannelId = data.ChannelId
                };
                mq.Publish(message);
            }
            loggerExtension.Description = "Push success from sync error invoice";
            loggerExtension.LogInfo(true);
        }
    }
}
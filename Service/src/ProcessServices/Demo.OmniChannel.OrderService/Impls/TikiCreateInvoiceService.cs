﻿using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.OrderService.OrderDomain.Interfaces;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Demo.OmniChannelCore.Api.Sdk.Models;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System.Data;
using System.Threading.Tasks;

namespace Demo.OmniChannel.OrderService.Impls
{
    public class TikiCreateInvoiceService : CreateInvoiceService<TikiCreateInvoiceMessage>
    {
        private readonly IDeliveryInfoInternalClient _deliveryInfoInternalClient;
        public TikiCreateInvoiceService(
            IIntegrationEventService integrationEventService,
            IAppSettings settings,
            IProductMappingService productMappingService,
            IRetryInvoiceMongoService retryInvoiceMongoService,
            IInvoiceMongoService invoiceMongoService,
            IInvoiceInternalClient invoiceInternalClient,
            IOrderInternalClient orderInternalClient,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IAuditTrailInternalClient auditTrailInternalClient,
            IMessageService mqService,
            IOmniChannelAuthService channelAuthService,
            KvRedisConfig mqRedisConfig,
            IDeliveryInfoInternalClient deliveryInfoInternalClient,
            IDeliveryPackageInternalClient deliveryPackageInternalClient,
            IKvLockRedis kvLockRedis,
            ICustomerInternalClient customerInternalClient,
            ISurChargeInternalClient surChargeInternalClient,
            IOmniChannelSettingService omniChannelSettingService,
            ISurChargeBranchInternalClient surChargeBranchInternalClient,
            ICreateInvoiceDomainService createInvoiceDomainService,
            IOmniChannelPlatformService omniChannelPlatformService,
            ILogger<TikiCreateInvoiceService> logger) : base(
            integrationEventService,
            settings,
            productMappingService,
            retryInvoiceMongoService,
            invoiceMongoService,
            invoiceInternalClient,
            orderInternalClient,
            channelClient,
            cacheClient,
            dbConnectionFactory,
            auditTrailInternalClient,
            mqService,
            channelAuthService,
            mqRedisConfig,
            deliveryInfoInternalClient,
            deliveryPackageInternalClient,
            kvLockRedis,
            customerInternalClient, 
            surChargeInternalClient,
            omniChannelSettingService,
            surChargeBranchInternalClient,
            createInvoiceDomainService,
            logger,
            omniChannelPlatformService)
        {
            ChannelType = ChannelType.Tiki;
            _deliveryInfoInternalClient = deliveryInfoInternalClient;
        }
        protected async override Task UpdateFeeDeliveryInOrder(IDbConnection db, OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext, byte channelType, long? deliveryInfoId, decimal? price, string feeJson)
        {
            if (price.HasValue && !string.IsNullOrEmpty(feeJson) && deliveryInfoId.HasValue)
            {
                var deliveryInfoRepository = new DeliveryInfoRepository(db);
                var existDelivery = await deliveryInfoRepository.SingleAsync(x => x.Id == deliveryInfoId);
                if (existDelivery != null && existDelivery.Price != price)
                {
                    var deliveryInfo = existDelivery.ConvertTo<DeliveryInfo>();
                    deliveryInfo.UpdateFeeJson(price, feeJson);
                    await _deliveryInfoInternalClient.UpdateDeliveryPriceAndFeeJson(coreContext, deliveryInfo);
                }
            }
        }
    }
}
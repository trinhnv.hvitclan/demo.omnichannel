﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.OrderService.OrderDomain.Interfaces;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.OrderService.Impls
{
    public class SendoSyncOrderService : SyncOrderService<SendoSyncErrorOrderMessage>
    {
        public SendoSyncOrderService(IAppSettings settings, 
            ChannelClient.Impls.ChannelClient channelClient, 
            ICacheClient cacheClient, 
            IDbConnectionFactory dbConnectionFactory, 
            IProductMappingService productMappingService, 
            ICustomerInternalClient customerInternalClient, 
            IPartnerDeliveryInternalClient partnerInternalClient, 
            IOrderInternalClient orderInternalClient, 
            IOrderMongoService orderMongoService, 
            IAuditTrailInternalClient auditTrailInternalClient, 
            IMessageService mqService, 
            IInvoiceMongoService invoiceMongoService, 
            IRetryOrderMongoService retryOrderMongoService, 
            IRetryInvoiceMongoService retryInvoiceMongoService, 
            IKvLockRedis kvLockRedis, 
            IOmniChannelAuthService channelAuthService, 
            KvRedisConfig mqRedisConfig,
            IChannelBusiness channelBusiness,
            ISendoLocationMongoService sendoLocationMongoService,
            ISurChargeInternalClient surChargeInternalClient,
            IOmniChannelSettingService omniChannelSettingService,
            IOmniChannelPlatformService omniChannelPlatformService,
            ICreateOrderDomainService createOrderDomainService,
            ILogger<SendoSyncOrderService> logger) : base(settings, 
            channelClient, 
            cacheClient, 
            dbConnectionFactory, 
            productMappingService, 
            customerInternalClient, 
            partnerInternalClient, 
            orderInternalClient, 
            orderMongoService, 
            auditTrailInternalClient, 
            mqService, 
            invoiceMongoService, 
            retryOrderMongoService, 
            retryInvoiceMongoService,
            kvLockRedis, 
            channelAuthService, 
            mqRedisConfig,
            channelBusiness,
            sendoLocationMongoService,
            surChargeInternalClient,
            omniChannelSettingService,
            omniChannelPlatformService,
            createOrderDomainService,
            logger)
        {
            ChannelType = Sdk.Common.ChannelType.Sendo;
        }
    }

    public class SendoSyncInvoiceService : SyncInvoiceService<SendoSyncErrorInvoiceMessage>
    {
        public SendoSyncInvoiceService(IAppSettings settings, 
            ChannelClient.Impls.ChannelClient channelClient, 
            ICacheClient cacheClient, 
            IDbConnectionFactory dbConnectionFactory, 
            IProductMappingService productMappingService, 
            IInvoiceInternalClient invoiceInternalClient, 
            IInvoiceMongoService invoiceMongoService, 
            IAuditTrailInternalClient auditTrailInternalClient, 
            IMessageService mqService, 
            IOmniChannelAuthService channelAuthService, 
            IRetryInvoiceMongoService retryInvoiceMongoService, 
            IOrderInternalClient orderInternalClient, 
            KvRedisConfig mqRedisConfig,
            IChannelBusiness channelBusiness,
            IDeliveryInfoInternalClient deliveryInfoInternalClient,
            IDeliveryPackageInternalClient deliveryPackageInternalClient,
            IKvLockRedis kvLockRedis,
            ICustomerInternalClient customerInternalClient,
            IOmniChannelSettingService omniChannelSettingService,
            ISurChargeInternalClient surChargeInternalClient,
            ICreateInvoiceDomainService createInvoiceDomainService,
            IOmniChannelPlatformService omniChannelPlatformService,
            ILogger<SendoSyncInvoiceService> logger) : base(settings, 
            channelClient, 
            cacheClient, 
            dbConnectionFactory, 
            productMappingService, 
            invoiceInternalClient, 
            invoiceMongoService, 
            auditTrailInternalClient, 
            mqService, channelAuthService, 
            retryInvoiceMongoService, 
            orderInternalClient, 
            mqRedisConfig,
            channelBusiness,
            deliveryInfoInternalClient,
            deliveryPackageInternalClient,
            kvLockRedis,
            customerInternalClient,
            omniChannelSettingService,
            surChargeInternalClient,
            createInvoiceDomainService,
            omniChannelPlatformService,
            logger)
        {
            ChannelType = Sdk.Common.ChannelType.Sendo;
        }
    }
}
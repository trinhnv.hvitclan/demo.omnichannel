﻿using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Microsoft.Extensions.Logging;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.OrderService.Impls
{
    public class LazadaGetListOrderService : GetListOrderService<LazadaSyncOrderMessage>
    {
        public LazadaGetListOrderService(IAppSettings settings,
            ChannelClient.Impls.ChannelClient channelClient,
            IDbConnectionFactory dbConnectionFactory,
            ICacheClient cacheClient,
            IMessageService messageService,
            KvRedisConfig mqRedisConfig,
            IScheduleService scheduleService,
            IOmniChannelAuthService channelAuthService,
            IOmniChannelSettingService omniChannelSettingService,
            ILogger<LazadaGetListOrderService> logger,
            IOmniChannelPlatformService omniChannelPlatformService) : base(settings,
            channelClient,
            dbConnectionFactory,
            cacheClient,
            messageService,
            mqRedisConfig,
            scheduleService,
            channelAuthService,
            omniChannelSettingService,
            logger,
            omniChannelPlatformService)
        {
            ChannelType = ChannelType.Lazada;
        }
    }
}
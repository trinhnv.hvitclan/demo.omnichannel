﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.OrderService.OrderDomain.Interfaces;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannelCore.Api.Sdk.Common;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System.Data;
using System.Threading.Tasks;

namespace Demo.OmniChannel.OrderService.Impls
{
    public class TiktokSyncInvoiceService : SyncInvoiceService<TiktokSyncErrorInvoiceMessage>
    {
        private readonly IDeliveryInfoInternalClient _deliveryInfoInternalClient;

        public TiktokSyncInvoiceService(IAppSettings settings,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IProductMappingService productMappingService,
            Demo.OmniChannelCore.Api.Sdk.Interfaces.IInvoiceInternalClient invoiceInternalClient,
            IInvoiceMongoService invoiceMongoService,
            IAuditTrailInternalClient auditTrailInternalClient,
            IMessageService mqService,
            IOmniChannelAuthService channelAuthService,
            IRetryInvoiceMongoService retryInvoiceMongoService,
            Demo.OmniChannelCore.Api.Sdk.Interfaces.IOrderInternalClient orderInternalClient,
            KvRedisConfig mqRedisConfig,
            IChannelBusiness channelBusiness,
            IDeliveryInfoInternalClient deliveryInfoInternalClient,
            IDeliveryPackageInternalClient deliveryPackageInternalClient,
            IKvLockRedis kvLockRedis,
            ICustomerInternalClient customerInternalClient,
            IOmniChannelSettingService omniChannelSettingService,
            ISurChargeInternalClient surChargeInternalClient,
            ICreateInvoiceDomainService createInvoiceDomainService,
            IOmniChannelPlatformService omniChannelPlatformService,
            ILogger<TiktokSyncInvoiceService> logger) : base(settings,
            channelClient,
            cacheClient,
            dbConnectionFactory,
            productMappingService,
            invoiceInternalClient,
            invoiceMongoService,
            auditTrailInternalClient,
            mqService, channelAuthService,
            retryInvoiceMongoService,
            orderInternalClient,
            mqRedisConfig,
            channelBusiness,
            deliveryInfoInternalClient,
            deliveryPackageInternalClient,
            kvLockRedis,
            customerInternalClient,
            omniChannelSettingService,
            surChargeInternalClient,
            createInvoiceDomainService,
            omniChannelPlatformService,
            logger)
        {
            ChannelType = Sdk.Common.ChannelType.Tiktok;
            _deliveryInfoInternalClient = deliveryInfoInternalClient;
        }

        protected override async Task UpdateFeeDeliveryInOrder(IDbConnection db, KvInternalContext coreContext, byte channelType, long? deliveryInfoId, decimal? price, string feeJson)
        {
            if (deliveryInfoId.HasValue)
            {
                var deliveryInfoRepository = new DeliveryInfoRepository(db);
                var existDelivery = await deliveryInfoRepository.SingleAsync(x => x.Id == deliveryInfoId);
                if (existDelivery != null && existDelivery.FeeJson != feeJson)
                {
                    var deliveryInfo = existDelivery.ConvertTo<Demo.OmniChannelCore.Api.Sdk.Models.DeliveryInfo>();
                    deliveryInfo.UpdateFeeJson(price, feeJson);
                    await _deliveryInfoInternalClient.UpdateDeliveryPriceAndFeeJson(coreContext, deliveryInfo);
                }
            }
        }

    }
}

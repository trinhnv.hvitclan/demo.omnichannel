﻿using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.OrderService.OrderDomain.Interfaces;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.OrderService.Impls
{
    public class SendoCreateInvoiceService : CreateInvoiceService<SendoCreateInvoiceMessage>
    {
        public SendoCreateInvoiceService(
            IIntegrationEventService integrationEventService,
            IAppSettings settings,
            IProductMappingService productMappingService,
            IRetryInvoiceMongoService retryInvoiceMongoService,
            IInvoiceMongoService invoiceMongoService,
            IInvoiceInternalClient invoiceInternalClient,
            IOrderInternalClient orderInternalClient,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IAuditTrailInternalClient auditTrailInternalClient,
            IMessageService mqService,
            IOmniChannelAuthService channelAuthService,
            KvRedisConfig mqRedisConfig,
            IDeliveryInfoInternalClient deliveryInfoInternalClient,
            IDeliveryPackageInternalClient deliveryPackageInternalClient,
            IKvLockRedis kvLockRedis,
            ICustomerInternalClient customerInternalClient,
            ISurChargeInternalClient surChargeInternalClient,
            IOmniChannelSettingService omniChannelSettingService,
            ISurChargeBranchInternalClient surChargeBranchInternalClient,
            ICreateInvoiceDomainService createInvoiceDomainService,
            IOmniChannelPlatformService omniChannelPlatformService,
            ILogger<SendoCreateInvoiceService> logger) : base(
            integrationEventService,
            settings,
            productMappingService,
            retryInvoiceMongoService,
            invoiceMongoService,
            invoiceInternalClient,
            orderInternalClient,
            channelClient,
            cacheClient,
            dbConnectionFactory,
            auditTrailInternalClient,
            mqService,
            channelAuthService,
            mqRedisConfig,
            deliveryInfoInternalClient,
            deliveryPackageInternalClient,
            kvLockRedis,
            customerInternalClient,
            surChargeInternalClient,
            omniChannelSettingService,
            surChargeBranchInternalClient,
            createInvoiceDomainService,
            logger,
            omniChannelPlatformService)
        {
            ChannelType = ChannelType.Sendo;
        }
    }
}
﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.OrderService.OrderDomain.Interfaces;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.OrderService.Impls
{
    public class SendoCreateOrderService : CreateOrderService<SendoCreateOrderMessage>
    {
        public SendoCreateOrderService(
            IIntegrationEventService integrationEventService,
            IAppSettings settings,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IDeliveryInfoInternalClient deliveryInfoInternalClient,
            IDbConnectionFactory dbConnectionFactory,
            ICustomerInternalClient customerInternalClient,
            IOrderInternalClient orderInternalClient,
            IProductMappingService productMappingService,
            IOrderMongoService orderMongoService,
            IRetryOrderMongoService retryOrderMongoService,
            IRetryInvoiceMongoService retryInvoiceMongoService,
            IPartnerDeliveryInternalClient partnerInternalClient,
            IAuditTrailInternalClient auditTrailInternalClient,
            IMessageService messageService,
            IOmniChannelAuthService channelAuthService,
            IInvoiceMongoService invoiceMongoService,
            IKvLockRedis kvLockRedis,
            KvRedisConfig mqRedisConfig,
            IChannelBusiness channelBusiness,
            ISendoLocationMongoService sendoLocationMongoService,
            ISurChargeInternalClient surChargeInternalClient,
            IOmniChannelSettingService omniChannelSettingService,
            ISurChargeBranchInternalClient surChargeBranchInternalClient,
            ILogger<SendoCreateOrderService> logger,
            IOmniChannelPlatformService omniChannelPlatformService,
            ICreateOrderDomainService createOrderDomainService) : base(settings,
            channelClient,
            cacheClient,
            dbConnectionFactory,
            customerInternalClient,
            deliveryInfoInternalClient,
            orderInternalClient,
            productMappingService,
            orderMongoService,
            retryOrderMongoService,
            retryInvoiceMongoService,
            partnerInternalClient,
            auditTrailInternalClient,
            messageService,
            channelAuthService,
            invoiceMongoService,
            kvLockRedis,
            mqRedisConfig,
            channelBusiness,
            sendoLocationMongoService,
            surChargeInternalClient,
            omniChannelSettingService,
            surChargeBranchInternalClient,
            logger,
            omniChannelPlatformService,
            integrationEventService,
            createOrderDomainService)
        {
            ChannelType = ChannelType.Sendo;
        }
    }
}
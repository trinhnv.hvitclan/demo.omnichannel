﻿using Demo.Audit.Model.Message;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.RequestDTO;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.OrderService.OrderDomain;
using Demo.OmniChannel.OrderService.OrderDomain.Interfaces;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.Services.LogginConfiguration;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.EventLogMessage;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Common;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Demo.OmniChannelCore.Api.Sdk.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Customer = Demo.OmniChannel.Domain.Model.Customer;
using DeliveryInfo = Demo.OmniChannelCore.Api.Sdk.Models.DeliveryInfo;
using DeliveryInfoDTO = Demo.OmniChannel.ChannelClient.Models.DeliveryInfoDTO;
using Invoice = Demo.OmniChannelCore.Api.Sdk.Models.Invoice;
using InvoiceDelivery = Demo.OmniChannelCore.Api.Sdk.Models.InvoiceDelivery;
using InvoiceDetail = Demo.OmniChannelCore.Api.Sdk.Models.InvoiceDetail;
using InvoiceSurCharges = Demo.OmniChannel.MongoDb.InvoiceSurCharges;
using InvoiceWarranties = Demo.OmniChannelCore.Api.Sdk.Models.InvoiceWarranties;
using ProductMapping = Demo.OmniChannel.MongoDb.ProductMapping;

namespace Demo.OmniChannel.OrderService.Impls
{
    public abstract class CreateInvoiceServiceV2<T> : BaseService<T> where T : CreateInvoiceMessageV2
    {
        private readonly IAppSettings _settings;
        private readonly IIntegrationEventService _integrationEventService;
        private readonly IProductMappingService _productMappingService;
        private readonly IInvoiceMongoService _invoiceMongoService;
        private readonly IInvoiceInternalClient _invoiceInternalClient;
        private readonly IOrderInternalClient _orderInternalClient;
        private readonly ISurChargeInternalClient _surChargeInternalClient;
        private readonly IAuditTrailInternalClient _auditTrailInternalClient;
        private readonly IDeliveryInfoInternalClient _deliveryInfoInternalClient;
        private readonly IOmniChannelAuthService _channelAuthService;
        private readonly IRetryInvoiceMongoService _retryInvoiceMongoService;
        private readonly IDeliveryPackageInternalClient _deliveryPackageInternalClient;
        private readonly IKvLockRedis _kvLockRedis;
        private readonly ICustomerInternalClient _customerInternalClient;
        private readonly IOmniChannelSettingService _omniChannelSettingService;
        private readonly ICreateOrderDomainService _createOrderDomainService;
        private readonly ICreateInvoiceDomainService _createInvoiceDomainService;
        protected CreateInvoiceServiceV2(
            IIntegrationEventService integrationEventService,
            IAppSettings settings,
            IProductMappingService productMappingService,
            IRetryInvoiceMongoService retryInvoiceMongoService,
            IInvoiceMongoService invoiceMongoService,
            IInvoiceInternalClient invoiceInternalClient,
            IOrderInternalClient orderInternalClient,
            ISurChargeInternalClient surChargeInternalClient,
            ChannelClient.Impls.ChannelClient channelClient,
            ICacheClient cacheClient,
            IDbConnectionFactory dbConnectionFactory,
            IAuditTrailInternalClient auditTrailInternalClient,
            IMessageService mqService,
            IOmniChannelAuthService channelAuthService,
            KvRedisConfig mqRedisConfig,
            IDeliveryInfoInternalClient deliveryInfoInternalClient,
            IDeliveryPackageInternalClient deliveryPackageInternalClient,
            IKvLockRedis kvLockRedis,
            ICustomerInternalClient customerInternalClient,
            IOmniChannelSettingService omniChannelSettingService,
            ICreateOrderDomainService createOrderDomainService,
            ICreateInvoiceDomainService createInvoiceDomainService,
            ILogger logger) : base(settings,
            channelClient,
            cacheClient,
            dbConnectionFactory,
            mqService,
            mqRedisConfig,
            logger)
        {
            ThreadSize = MqRedisConfig?.EventMessages.FirstOrDefault(x => !string.IsNullOrEmpty(x.Name) &&
                                                                          x.Name.Contains(this.GetType()
                                                                              .Name))?.ThreadSize ?? 10;
            mqService.RegisterHandler<T>(m =>
            {
                try
                {
                    m.Options = (int)MessageOption.None;

                    LoggingHelper.WriteLogDataReceived(Logger, m.GetBody());

                    var task = Task.Run(async () => await ProcessMessage(m.GetBody()));
                    task.GetAwaiter().GetResult();
                    return Task.CompletedTask;
                }
                catch (Exception e)
                {
                    ExceptionHelper.WriteLogExceptionMq(typeof(T), m.Body, e);
                    return string.Empty;
                }
            }, ThreadSize);
            _productMappingService = productMappingService;
            _retryInvoiceMongoService = retryInvoiceMongoService;
            _invoiceInternalClient = invoiceInternalClient;
            _orderInternalClient = orderInternalClient;
            _invoiceMongoService = invoiceMongoService;
            _auditTrailInternalClient = auditTrailInternalClient;
            _channelAuthService = channelAuthService;
            _deliveryInfoInternalClient = deliveryInfoInternalClient;
            _deliveryPackageInternalClient = deliveryPackageInternalClient;
            _kvLockRedis = kvLockRedis;
            _customerInternalClient = customerInternalClient;
            _omniChannelSettingService = omniChannelSettingService;
            _createOrderDomainService = createOrderDomainService;
            _createInvoiceDomainService = createInvoiceDomainService;
            _surChargeInternalClient = surChargeInternalClient;
            _integrationEventService = integrationEventService;
            _settings = settings;
        }

        protected override async Task ProcessMessage(T data)
        {
            using (var lockByOrderId = _kvLockRedis.GetLockFactory())
            {
                using (lockByOrderId.AcquireLock(string.Format(KvConstant.LockProcessInvoice, data.RetailerId, data.OrderId), TimeSpan.FromSeconds(Settings.Get("LockProcessInvoiceSecond", 60))))
                {
                    var loggerExtension = new LogObjectMicrosoftExtension(Logger, data.LogId)
                    {
                        Action = ExceptionType.CreateInitInvoice,
                        RetailerId = data.RetailerId,
                        BranchId = data.BranchId,
                        OmniChannelId = data.ChannelId,
                        OrderId = data.OrderId,
                        ChannelType = data.ChannelType,
                        ChannelTypeCode = ((ChannelTypeEnum)data.ChannelType).ToString(),
                        RequestObject = data.ToSafeJson(),
                    };

                    var connectStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);
                    if (data.IsDbException || data.RetryCount > 0)
                    {
                        await IncreaseRetryCount(data);
                    }
                    try
                    {
                        var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, data.RetailerId, connectStringName, data.BranchId, Settings?.Get<int>("ExecutionContext"));
                        var coreContext = context.ConvertTo<KvInternalContext>();
                        coreContext.UserId = context.User?.Id ?? 0;
                        coreContext.LogId = loggerExtension.Id;
                        var settings = await _omniChannelSettingService.GetChannelSettings(data.ChannelId, data.RetailerId);
                        using var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower());
                        var deliveryInfoRepository = new DeliveryInfoRepository(db);
                        var invoiceRepository = new InvoiceRepository(db);
                        var orderRepository = new OrderRepository(db);
                        var productRepository = new ProductRepository(db);
                        var priceBookRepository = new PriceBookRepository(db);
                        var customerRepository = new CustomerRepository(db);
                        var productFormulaRepository = new ProductFormulaRepository(db);

                        loggerExtension.GroupId = context.GroupId;
                        var code = ChannelTypeHelper.GetOrderPrefix(data.ChannelType);
                        var existOrder = await orderRepository.GetOrderByCodeAsync($"{code}_{data.OrderId}", data.RetailerId);
                        if (existOrder == null || existOrder.Status == (byte)OrderState.Void)
                        {
                            await RemoveErrorInvoiceInMongo(data.OrderId, data.BranchId, data.ChannelId);

                            var ex = new OmniException("Order status invalid");
                            loggerExtension.LogWarning(ex.Message);
                            PushMessageToInvoiceTrackingEvent(data, "Order status invalid");
                            return;
                        }

                        if (existOrder?.Status == (byte)OrderState.Finalized)
                        {
                            var invoicePrefix = ChannelTypeHelper.GetInvoicePrefix(data.ChannelType);
                            var existInvoices = (await invoiceRepository.GetByOrderId(existOrder.Id, data.RetailerId));

                            var existInvoiceInprogress = existInvoices?.FirstOrDefault(x => x.Status != (byte)InvoiceState.Void && x.Status != (byte)InvoiceState.Issued);

                            if (existInvoiceInprogress != null && !string.IsNullOrEmpty(existInvoiceInprogress.Code) && !existInvoiceInprogress.Code.StartsWith(invoicePrefix))
                            {
                                loggerExtension.Description = "Invoice existed - Issue";
                                await RemoveErrorInvoiceInMongo(data.OrderId, data.BranchId, data.ChannelId);
                                loggerExtension.LogInfo(true);
                                PushMessageToInvoiceTrackingEvent(data, "Invoice existed - Issue");
                                return;
                            }


                            var existInvoiceFinal = existInvoices?.FirstOrDefault(x => x.Status == (byte)InvoiceState.Void || x.Status == (byte)InvoiceState.Issued);
                            if (existInvoiceInprogress == null && existInvoiceFinal != null && !string.IsNullOrEmpty(existInvoiceFinal.Code) && !existInvoiceFinal.Code.StartsWith(invoicePrefix))
                            {
                                loggerExtension.Description = "Invoice existed - Void";
                                await RemoveErrorInvoiceInMongo(data.OrderId, data.BranchId, data.ChannelId);
                                loggerExtension.LogInfo(true);
                                PushMessageToInvoiceTrackingEvent(data, "Invoice existed - Void");
                                return;
                            }

                        }

                        if (data.IsDbException && data.RetailerId > 0 && !string.IsNullOrEmpty(data.InvoiceCode))
                        {
                            var existInvoice =
                                (await invoiceRepository.WhereAsync(x =>
                                    x.RetailerId == data.RetailerId && x.Code == data.InvoiceCode)).FirstOrDefault();
                            if (await RetryErrorInvoice(data, existInvoice, coreContext, loggerExtension))
                            {
                                PushMessageToInvoiceTrackingEvent(data);
                                return;
                            }
                        }
                        var deliveryInfo = await _deliveryInfoInternalClient.GetLastByOrderId(coreContext, data.KvOrderId);

                        if (deliveryInfo == null)
                        {
                            var logDeliveryInfo = loggerExtension.Clone("GetDeliveryInfoCore");
                            logDeliveryInfo.LogWarning("get deliveryInfo core is null! No create invoice");
                            deliveryInfo = (await deliveryInfoRepository.GetLastByOrderIdAsync(data.RetailerId, data.KvOrderId)).ConvertTo<DeliveryInfoForOrder>();
                            if (deliveryInfo == null)
                            {
                                loggerExtension.LogError(new OmniException("get deliveryInfo is null! No create invoice"));
                                PushMessageToInvoiceTrackingEvent(data, "get deliveryInfo is null! No create invoice");
                                return;
                            }
                        }

                        var client = ChannelClient.GetClient((byte)ChannelType, Settings);
                        var channel = new Domain.Model.OmniChannel
                        {
                            Id = data.ChannelId,
                            RetailerId = data.RetailerId,
                            BranchId = data.BranchId,
                            Type = (byte)ChannelType
                        };
                        var auth = await _channelAuthService.GetChannelAuth(channel, data.LogId);
                        if (auth == null || ((byte)ChannelType == (byte)Sdk.Common.ChannelType.Lazada && string.IsNullOrEmpty(auth.AccessToken)))
                        {
                            var ex = new OmniException("Token is Expired");
                            loggerExtension.LogError(ex);
                            PushMessageToInvoiceTrackingEvent(data, "Token is Expired");
                            return;
                        }
                        var kvOrder = existOrder.ConvertTo<KvOrder>();

                        #region Shopee: Update delivery info
                        if (ValidateChannel(data.ChannelType) && kvOrder?.Status == (byte)OrderState.Draft && deliveryInfo != null)
                        {
                            try
                            {
                                var order = JsonConvert.DeserializeObject<KvOrder>(data.Order);
                                loggerExtension.Action = ExceptionType.CreateInvoiceUpdateUpdateDeliveryPackage;
                                if (
                                    order?.OrderDelivery != null
                                    && (
                                        deliveryInfo.Receiver != order.OrderDelivery.Receiver ||
                                        deliveryInfo.ContactNumber != order.OrderDelivery.ContactNumber ||
                                        deliveryInfo.Address != order.OrderDelivery.Address ||
                                        deliveryInfo.WardName != order.OrderDelivery.WardName ||
                                        deliveryInfo.LocationName != order.OrderDelivery.LocationName)
                                )
                                {
                                    var isChangedPhone = PhoneNumberHelper.GetPerfectContactNumber(deliveryInfo.ContactNumber) != PhoneNumberHelper.GetPerfectContactNumber(order.OrderDelivery.ContactNumber);

                                    deliveryInfo.Receiver = order.OrderDelivery.Receiver;
                                    deliveryInfo.ContactNumber = order.OrderDelivery.ContactNumber;
                                    deliveryInfo.Address = order.OrderDelivery.Address;
                                    deliveryInfo.WardName = order.OrderDelivery.WardName;
                                    deliveryInfo.LocationName = order.OrderDelivery.LocationName;

                                    var dpDataChanged = new OmniChannelCore.Api.Sdk.Models.DeliveryPackage
                                    {
                                        RetailerId = kvOrder.RetailerId,
                                        OrderId = kvOrder.Id,
                                        Receiver = order.OrderDelivery.Receiver,
                                        ContactNumber = order.OrderDelivery.ContactNumber,
                                        Address = order.OrderDelivery.Address,
                                        WardName = order.OrderDelivery.WardName,
                                        LocationName = order.OrderDelivery.LocationName,
                                    };
                                    var dpConditionChanged = new DeliveryPackageConditionUpdate { OrderId = kvOrder.Id };
                                    var fieldChangeds = new[] { "Receiver", "ContactNumber", "Address", "WardName", "LocationName" };
                                    var dpUpdatedRow = await _deliveryPackageInternalClient.Update(coreContext, dpDataChanged, dpConditionChanged, fieldChangeds);
                                    loggerExtension.Description = $"update _deliveryPackageInternalClient : {order.ToSafeJson()}";
                                    loggerExtension.LogInfo();

                                    long orderUpdatedCusRow = 0;
                                    var phone = PhoneNumberHelper.GetPerfectContactNumber(order.CustomerPhone);
                                    //Kiểm tra số điện thoại có hợp lệ. 
                                    var isValidPhone = PhoneNumberHelper.IsValidContactNumber(phone, RegionCode.VN) || data.ChannelType != (byte)ChannelTypeEnum.Shopee;
                                    // Update OrderCustomer
                                    if (dpUpdatedRow > 0 && isChangedPhone && isValidPhone)
                                    {
                                        using (var cli = _kvLockRedis.GetLockFactory())
                                        {
                                            var lockAddCustomerKey = phone;
                                            if (string.IsNullOrEmpty(phone))
                                            {
                                                lockAddCustomerKey = order.Code;
                                            }

                                            long? newOrderCustomerId = 0;
                                            using (cli.AcquireLock(string.Format(KvConstant.LockAddCustomer, lockAddCustomerKey), TimeSpan.FromSeconds(60)))
                                            {
                                                var posSettingRepository = new PosSettingRepository(db);
                                                var posSetting = new PosSetting(await posSettingRepository.GetSettingAsync(data.RetailerId), data.RetailerId, DbConnectionFactory, CacheClient, Settings);
                                                var existsCustomer = await customerRepository.GetByFilterAsync(data.RetailerId, data.BranchId, phone, null, posSetting.ManagerCustomerByBranch);
                                                if (existsCustomer == null)
                                                {
                                                    try
                                                    {
                                                        var customerInsert = new OmniChannelCore.Api.Sdk.Models.Customer
                                                        {
                                                            ContactNumber = phone,
                                                            Name = order.CustomerName,
                                                            Address = order.CustomerAddress,
                                                            BranchId = data.BranchId,
                                                            RetailerId = data.RetailerId
                                                        };
                                                        if (!string.IsNullOrEmpty(order.CustomerLocation))
                                                        {
                                                            customerInsert.LocationName = order.CustomerLocation;
                                                        }

                                                        if (!string.IsNullOrEmpty(order.CustomerWard))
                                                        {
                                                            customerInsert.WardName = order.CustomerWard;
                                                        }

                                                        newOrderCustomerId = await _customerInternalClient.CreateCustomerAsync(coreContext, customerInsert);
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        loggerExtension.LogError(ex);
                                                        existsCustomer = await customerRepository.GetByFilterAsync(data.RetailerId, data.BranchId, phone, null, posSetting.ManagerCustomerByBranch);
                                                        newOrderCustomerId = existsCustomer?.Id;
                                                    }
                                                }
                                                else
                                                {
                                                    newOrderCustomerId = existsCustomer?.Id;
                                                }
                                            }

                                            if (newOrderCustomerId != null && newOrderCustomerId > 0)
                                            {
                                                orderUpdatedCusRow = await _orderInternalClient.UpdateCustomer(coreContext, kvOrder.Id, newOrderCustomerId.Value);
                                                kvOrder.CustomerId = newOrderCustomerId.Value;
                                            }
                                        }
                                    }

                                    loggerExtension.Description = $"Success";
                                    loggerExtension.RequestObject = $"{new { OrderId = data.OrderId, DpDataChanged = dpDataChanged, DpConditionChanged = dpConditionChanged, FieldChangeds = fieldChangeds }.ToSafeJson()}";
                                    loggerExtension.ResponseObject = $"{new { DeliveryPackageUpdatedRow = dpUpdatedRow, IsChangedPhone = isChangedPhone, OrderUpdatedCusRow = orderUpdatedCusRow }.ToJson()}";
                                    loggerExtension.LogInfo();
                                }
                            }
                            catch (Exception e)
                            {
                                loggerExtension.Description = "Fail delivery --> ignore";
                                loggerExtension.LogError(e);
                            }
                        }
                        #endregion

                        //Khởi tạo hóa đơn
                        var invoices = await GetKvInvoice(
                            client, data, kvOrder,
                            deliveryInfo, settings.IsConfirmReturning, loggerExtension,
                            invoiceRepository, priceBookRepository, productRepository,
                            coreContext);

                        if (invoices == null || !invoices.Any())
                        {
                            PushMessageToInvoiceTrackingEvent(data, "No created invoice!");
                            loggerExtension.LogWarning("No created invoice!");
                            return;
                        }

                        //Lấy thông tin khách hàng 
                        var customer = kvOrder.CustomerId.HasValue
                            ? (await _customerInternalClient.GetCustomerByIdAsync(coreContext, kvOrder.CustomerId.Value)
                            )?.ConvertTo<Customer>()
                            : null;

                        //Lấy thông tin bảng giá
                        var extra = await _createOrderDomainService.GetExtraAsync(priceBookRepository,
                            invoices.FirstOrDefault().PriceBookId, data);
                        invoices.FirstOrDefault().PriceBookId = extra.PriceBookId.Id;
                        var priceBookName = extra.PriceBookId.Name;

                        foreach (var invoice in invoices)
                        {
                            loggerExtension.Action = ExceptionType.CreateInvoice;
                            try
                            {
                                if (invoice.NewStatus == null)
                                {
                                    loggerExtension.LogWarning("NewStatus is null!");
                                    PushMessageToInvoiceTrackingEvent(data, "NewStatus is null!");
                                    continue;
                                }
                                //Update phí ở order với ChannelType la tiktok
                                await UpdateFeeDeliveryInOrder(db, coreContext, data.ChannelType, deliveryInfo?.DeliveryInfoId, invoice.DeliveryDetail.Price, invoice.DeliveryDetail.FeeJson);
                                invoice.SaleChannelId = existOrder.SaleChannelId;
                                var productIds = invoice.InvoiceDetails.Where(p => !string.IsNullOrEmpty(p.ProductChannelId)).Select(p => p.ProductChannelId.ToString()).ToList();
                                List<ProductMapping> mappings;
                                // Set product kv (Chỉ Lazada cho phép mapping SKU)
                                if (data.ChannelType == (byte)Sdk.Common.ChannelType.Lazada)
                                {
                                    if (productIds.Count > 0)
                                    {
                                        mappings = await _productMappingService.GetByChannelProductIds(data.RetailerId, data.ChannelId, productIds, isStringItemId: ConvertHelper.CheckUseStringItemId(data.ChannelType));
                                    }
                                    else
                                    {
                                        var productSkus = invoice.InvoiceDetails.Where(p => !string.IsNullOrEmpty(p.ProductChannelSku)).Select(p => p.ProductChannelSku).ToList();
                                        mappings = await _productMappingService.GetByChannelProductSkus(data.RetailerId, data.ChannelId, productSkus);
                                    }

                                    foreach (var detail in invoice.InvoiceDetails)
                                    {
                                        var mapping = mappings?.FirstOrDefault(x => (!string.IsNullOrEmpty(detail.ProductChannelId) && x.CommonProductChannelId == detail.ProductChannelId) || x.ProductChannelSku.Equals(detail.ProductChannelSku));

                                        detail.ProductId = mapping?.ProductKvId ?? 0;
                                        detail.ProductCode = mapping?.ProductKvSku ?? string.Empty;
                                    }
                                }
                                else
                                {
                                    mappings = await _productMappingService.GetByChannelProductIds(data.RetailerId, data.ChannelId, productIds, isStringItemId: ConvertHelper.CheckUseStringItemId(data.ChannelType));

                                    foreach (var detail in invoice.InvoiceDetails)
                                    {
                                        var mapping = mappings?.FirstOrDefault(x => !string.IsNullOrEmpty(detail.ProductChannelId) && x.CommonProductChannelId == detail.ProductChannelId);

                                        detail.ProductId = mapping?.ProductKvId ?? 0;
                                        detail.ProductCode = mapping?.ProductKvSku ?? string.Empty;
                                    }
                                }
                                var invoiceKvProductIds = invoice.InvoiceDetails.Where(p => p.ProductId > 0).Select(p => p.ProductId).ToList();
                                var kvProducts = await productRepository.GetProductByIds(data.RetailerId, data.BranchId, invoiceKvProductIds, isLogMonitor: false);
                                var invoiceKvProducts = kvProducts.Where(x => invoiceKvProductIds.Contains(x.Id)).ToList();

                                // Tạo hóa đơn trong lúc hàng hóa đang bị lock ở ProductSubcribe service gây ra trường hợp không đồng bộ đúng stock
                                var millisecondsDelay = await DelayByProductLock(data.ChannelId, invoiceKvProductIds);
                                if (millisecondsDelay > 0) loggerExtension.LogInfo($"Delay create Invoice after: {millisecondsDelay}ms by product lock. RetailerId: {data.RetailerId}. OrderId: {data.OrderId}");
                                await RemoveMongoRetryInvoice(data.OrderId, data.BranchId, data.ChannelId, data.RetailerId);

                                var originalCode = invoice.Code.Split(".")[0];
                                var existedInvoices = await _invoiceInternalClient.GetByOriginalCode(coreContext, originalCode);
                                //var existedInvoices = await invoiceRepository.GetLastExistedInvoiceLst(originalCode, data.RetailerId);
                                Invoice existInvoice;
                                DeliveryInfo existDelivery = null;
                                if (invoices.Count > 1)
                                {
                                    var otherInvoiceCodes = invoices.Where(x => x.Code != invoice.Code).Select(x => x.Code);
                                    existDelivery = await _deliveryInfoInternalClient.GetLastByDeliveryCode(coreContext, invoice.DeliveryCode);
                                    existInvoice = existedInvoices?.FirstOrDefault(x => x.Id == (existDelivery?.InvoiceId ?? 0) && !otherInvoiceCodes.Contains(x.Code));
                                }
                                else
                                {
                                    existInvoice = existedInvoices?.FirstOrDefault(x => !x.Code.ToUpper().Contains("DELETE") && !x.Code.ToUpper().Contains("OLD"));
                                }
                                //Lấy danh sách phụ phí
                                var lstInvoiceSurcharge =
                                    await _createOrderDomainService.GetListInvoiceSurchargeAsync(
                                        invoice.SurCharges?.ToList(),
                                        coreContext, data);

                                if (existInvoice != null)
                                {
                                    invoice.Code = existInvoice.Code;
                                    await RemoveErrorInvoiceInMongo(data.OrderId, data.BranchId, data.ChannelId, invoice.Code);
                                    if ((existInvoice.Status == (byte)InvoiceState.Issued ||
                                         existInvoice.Status == (byte)InvoiceState.Void))
                                    {
                                        PushMessageToInvoiceTrackingEvent(data);
                                        continue;
                                    }

                                    await _surChargeInternalClient.UpdateInvoiceSurcharge(coreContext,
                                        lstInvoiceSurcharge, existInvoice.Id);



                                    existDelivery = existDelivery ?? await _deliveryInfoInternalClient.GetLastByInvoiceId(coreContext, existInvoice?.Id ?? 0);
                                    if (existDelivery == null)
                                    {
                                        PushMessageToInvoiceTrackingEvent(data, "existDelivery is null");
                                        continue;
                                    }
                                    var isChangeDeliveryPriceOrFee = existDelivery.FeeJson != invoice.DeliveryDetail.FeeJson && invoice.DeliveryDetail.Price.HasValue;
                                    if (existDelivery.Status != (byte)DeliveryStatus.Returned && isChangeDeliveryPriceOrFee)
                                    {
                                        existDelivery.UpdateFeeJson(invoice.DeliveryDetail.Price, invoice.DeliveryDetail.FeeJson);
                                        await _deliveryInfoInternalClient.UpdateDeliveryPriceAndFeeJson(coreContext, existDelivery);
                                    }

                                    if (invoice.DeliveryStatus == null)
                                    {
                                        PushMessageToInvoiceTrackingEvent(data, "DeliveryStatus is null");
                                        continue;
                                    }

                                    if (existDelivery.Status == invoice.DeliveryStatus.Value)
                                    {
                                        PushMessageToInvoiceTrackingEvent(data);
                                        continue;
                                    }
                                    // Update với trường hợp thay đổi # 0 issue/KOL-11387
                                    if (invoice.DeliveryDetail.Price.HasValue)
                                    {
                                        existDelivery.UpdateFeeJson(invoice.DeliveryDetail.Price, invoice.DeliveryDetail.FeeJson);
                                    }
                                    await _invoiceInternalClient.UpdateStatus(coreContext,
                                        existDelivery, invoice.DeliveryStatus.Value);
                                    loggerExtension.Description = $"Update status success old delivery status {existDelivery.Status}; new status {invoice.DeliveryStatus.Value} ";
                                    loggerExtension.LogInfo();

                                    if (data.ChannelType == (byte)ShareKernel.Common.ChannelType.Tiktok && invoice.DeliveryStatus.Value == (byte)DeliveryStatus.Void)
                                    {
                                        await _invoiceInternalClient.VoidInvoice(coreContext, existInvoice.Id, channel.RetailerId);
                                        await WriteLogForUpdateInvoice(data, invoice, existDelivery.Status, coreContext, true);
                                        PushMessageToInvoiceTrackingEvent(data);
                                        continue;
                                    }
                                    #region Logs
                                    await WriteLogForUpdateInvoice(data, invoice, existDelivery.Status, coreContext);
                                    PushMessageToInvoiceTrackingEvent(data);
                                    #endregion
                                    continue;
                                }

                                if (invoice.NewStatus == (byte)InvoiceState.Void)
                                {
                                    loggerExtension.Description = $"VoidKvOrder";
                                    loggerExtension.LogInfo();
                                    await RemoveErrorInvoiceInMongo(data.OrderId, data.BranchId, data.ChannelId, invoice.Code);
                                    await VoidKvOrder(data, coreContext, (byte)ChannelType, kvOrder, invoiceRepository, priceBookRepository, productRepository, loggerExtension);
                                    PushMessageToInvoiceTrackingEvent(data);
                                    continue;
                                }

                                var isValidateProduct = await ValidateProduct(invoiceKvProducts,
                                    invoice, data.ChannelId,
                                    data.OrderId, data.ChannelType, kvOrder, customer, coreContext,
                                    mappings, auth.Id, data.LogId, loggerExtension,
                                    settings);

                                if (!isValidateProduct)
                                {
                                    loggerExtension.Action = ExceptionType.InvoiceMappingProduct;
                                    loggerExtension.LogWarning("Mapping invalid");
                                    PushMessageToInvoiceTrackingEvent(data, "Mapping invalid");
                                    continue;
                                }


                                var invoiceInsert = invoice.ConvertTo<Invoice>();

                                invoiceInsert.PurchaseDate = GetPuseChaseDate(data.ChannelType, invoiceInsert.PurchaseDate, existOrder.PurchaseDate, false);

                                foreach (var detail in invoice.InvoiceDetails)
                                {
                                    detail.ProductName = kvProducts.FirstOrDefault(x => x.Id == detail.ProductId)?.FullName;
                                }
                                //Cập nhập lại invoice detail nếu bật tính năng tự động lô date
                                List<Domain.Model.ProductBatchExpireDto> productBatchExPires = null;
                                if (settings.IsAutoSyncBatchExpire)
                                {
                                    var invoiceDetailsCopy = ConvertHelper.DeepCopy(invoice.InvoiceDetails);
                                    productBatchExPires =
                                        await productRepository.GetProductBatchExpireByBranch(
                                            data.RetailerId,
                                            data.BranchId,
                                            invoiceKvProductIds);

                                    invoice.InvoiceDetails = DetechHasExpire.DetectInvoiceDetailHasBatchExpire(
                                        kvProducts,
                                        invoice.InvoiceDetails,
                                        productBatchExPires);
                                    var (isValidAutoSyncBatchExpire, errorMessage, productLog) = _createInvoiceDomainService.ValidateAutoSyncBatchExpire(invoiceDetailsCopy, invoice.InvoiceDetails);
                                    if (!isValidAutoSyncBatchExpire)
                                    {
                                        invoice.InvoiceDetails = invoiceDetailsCopy;
                                        await SaveSyncInvoiceError(coreContext, invoice, kvOrder, data.OrderId, customer, data.ChannelId, (byte)ChannelType, errorMessage, productLog, auth.Id, data.LogId, loggerExtension);
                                        loggerExtension.LogWarning("Validate autoSyncBatchExpire failed");
                                        PushMessageToInvoiceTrackingEvent(data, "Validate autoSyncBatchExpire failed");
                                        continue;
                                    }
                                }

                                if (lstInvoiceSurcharge != null && lstInvoiceSurcharge.Count > 0)
                                {
                                    invoiceInsert.InvoiceSurCharges = lstInvoiceSurcharge;
                                    invoiceInsert.Surcharge = lstInvoiceSurcharge.Sum(x => x.Price);
                                }

                                invoiceInsert.InvoiceWarranties = await _createInvoiceDomainService.GetInvoiceWarranties(coreContext,
                                    productFormulaRepository, kvProducts,
                                    invoice.InvoiceDetails.Select(x => new MongoDb.InvoiceDetail { ProductId = x.ProductId, Uuid = x.Uuid }).ToList(),
                                    invoiceInsert.PurchaseDate);

                                var invoiceDetailsInsert = invoice.InvoiceDetails.ConvertAll(x => x.ConvertTo<InvoiceDetail>());
                                invoiceDetailsInsert.ForEach(x => x.UpdatePropetiesUseWarranty(invoiceInsert.InvoiceWarranties));

                                var invoiceId = await _invoiceInternalClient.CreateInvoice(coreContext, invoiceInsert, invoiceDetailsInsert,
                                    invoice.DeliveryDetail.ConvertTo<InvoiceDelivery>());
                                if (invoiceId == 0)
                                {
                                    var ex = new OmniException("Tạo hóa đơn không thành công");
                                    loggerExtension.LogError(ex);
                                    PushMessageToInvoiceTrackingEvent(data, $"Tạo hóa đơn không thành công");
                                    throw ex;
                                }
                                loggerExtension.Description = $"CreateInvoice success {invoiceId}";
                                loggerExtension.LogInfo();

                                if (invoiceId > 0)
                                {
                                    var existRetryInvoice = await _retryInvoiceMongoService.GetByOrderId(data.OrderId, data.ChannelId, data.BranchId, data.RetailerId);
                                    if (existRetryInvoice != null && existRetryInvoice.IsDbException)
                                    {
                                        await _retryInvoiceMongoService.RemoveAsync(existRetryInvoice.Id);
                                    }
                                }
                                await RemoveErrorInvoiceInMongo(data.OrderId, data.BranchId, data.ChannelId, invoice.Code);
                                #region Logs
                                await WriteLogForCreateInvoice(data, invoice, kvOrder, priceBookName, coreContext,
                                    productBatchExPires, invoiceInsert.InvoiceWarranties);

                                #endregion
                                if (invoice.DeliveryStatus != null && invoice.DeliveryStatus != (byte)DeliveryStatus.Pending && invoiceId > 0)
                                {
                                    existDelivery = await _deliveryInfoInternalClient.GetLastByInvoiceId(coreContext, invoiceId);
                                    await _invoiceInternalClient.UpdateStatus(coreContext,
                                        existDelivery, invoice.DeliveryStatus.Value);

                                    await WriteLogForUpdateInvoice(data, invoice, existDelivery.Status, coreContext);
                                }
                                PushMessageToInvoiceTrackingEvent(data);
                            }
                            catch (Exception e)
                            {
                                loggerExtension.Description = $"{e.Message} - {e.StackTrace}";
                                loggerExtension.ResponseObject = null;
                                PushMessageToInvoiceTrackingEvent(data, $"Error process invoice");
                                loggerExtension.LogError(e, isLogTotalTime: true, e is OmniChannelCore.Api.Sdk.Common.KvValidateException);
                                if (e is KvRetailerExpireException)
                                {
                                    await DeActiveChannel(data.ChannelId, coreContext.ConvertTo<Sdk.Common.KvInternalContext>(), auth.Id, data.ChannelType);
                                    return;
                                }

                                if (e is KvDbException
                                    || (e.Message != null && (e.Message.ToLower().Contains(DbExceptionMessages.ConnectionTimeout)
                                                              || e.Message.ToLower().Contains(DbExceptionMessages.TransactionDeadlocked)
                                                              || e.Message.ToLower().Contains(DbExceptionMessages.DatabaseException))))
                                {
                                    await AddOrUpdateRetryInvoice(data, e is KvDbException, invoice.Code);
                                    await WriteLogForCreateInvoiceFail(coreContext, invoice, kvOrder, data.ChannelId,
                                        data.ChannelType, e.Message, auth.Id, data.LogId, loggerExtension);
                                }

                                await SaveSyncInvoiceError(coreContext, invoice, kvOrder, data.OrderId, customer,
                                    data.ChannelId, (byte)ChannelType, e.Message, e.Message, auth.Id, data.LogId, loggerExtension);
                            }
                        }

                        if (data.ChannelType == (byte)Sdk.Common.ChannelType.Lazada && invoices.Any(x => x.NewStatus == (byte)InvoiceState.Void))
                        {
                            loggerExtension.Action = ExceptionType.InvoiceVoid;
                            var originalCode = invoices.FirstOrDefault(x => !string.IsNullOrEmpty(x.Code))?.Code.Split(".")[0];
                            var existedInvoices = (await invoiceRepository.WhereAsync(x => x.RetailerId == data.RetailerId && x.Code.StartsWith(originalCode))).ToList();

                            var existIds = existedInvoices.Select(x => x.Id).ToList();
                            var existDeliveries = await deliveryInfoRepository.WhereAsync(x => x.RetailerId == data.RetailerId && existIds.Contains(x.InvoiceId ?? 0));
                            var lzdDeliveryCodes = invoices.Select(x => x.DeliveryCode).ToList();
                            foreach (var item in existedInvoices)
                            {
                                var deliveryCodeExist = existDeliveries.FirstOrDefault(x => x.InvoiceId == item.Id)?.DeliveryCode;
                                if (string.IsNullOrEmpty(deliveryCodeExist)) continue;

                                if (item.Status != (byte)InvoiceState.Issued &&
                                    item.Status != (byte)InvoiceState.Void &&
                                    item.OrderId == existOrder.Id &&
                                    !lzdDeliveryCodes.Contains(deliveryCodeExist))
                                {
                                    await _invoiceInternalClient.VoidInvoice(coreContext, item.Id, channel.RetailerId);
                                    await WriteLogForUpdateInvoice(data, new KvInvoice { Code = item.Code, NewStatus = (byte)InvoiceState.Void }, 0, coreContext, true);
                                    loggerExtension.Description = "Invoice void success";
                                    loggerExtension.ResponseObject = null;
                                    loggerExtension.LogInfo();
                                }
                            }
                        }

                        loggerExtension.LogInfo(isLogTotalTime: true);
                    }
                    catch (Exception ex)
                    {
                        PushMessageToInvoiceTrackingEvent(data, $"Error process invoice");
                        loggerExtension.LogError(ex, true);
                        ResetService(ex);
                    }
                    finally
                    {
                        PublishUpdatePaymentTransaction(data, loggerExtension);
                    }
                }
            }
        }

        protected virtual void PublishUpdatePaymentTransaction(T data, LogObjectMicrosoftExtension loggerExtension) { }

        protected virtual void PushMessageToInvoiceTrackingEvent(T message, string error = "")
        {
            if (message.Source != null && message.Source.ToLower().Equals(ProcessOrderSource.WebHook))
            {
                string exChangeTrackProcessExChange = _settings.Get<string>("OrderTrackingEventExChange");
                var eventModel = new TrackModelEvent
                {
                    Code = message.CodeSource,
                    OrderSn = message.OrderSn,
                    ErrorMessage = error,
                    Status = ProcessOrderStatus.Processed,
                    OrderTimestamp = message.Timestamps,
                    ShopId = message.ShopId,
                    Source = message.Source,
                    ProcessType = ProcessFlowType.Invoice
                };
                _integrationEventService.AddEventWithRoutingKeyAsync(new OrderTrackingEvent
                {
                    TrackModelEvent = eventModel,
                }, RoutingKey.OrderTrackingEvent, exChangeTrackProcessExChange);
            }
        }

        protected virtual Task UpdateFeeDeliveryInOrder(IDbConnection db, KvInternalContext coreContext, byte channelType, long? deliveryInfoId, decimal? price, string feeJson)
        {
            return Task.CompletedTask;
        }


        private async Task WriteLogForCreateInvoice(T data, KvInvoice invoice, KvOrder kvOrder, string priceBookName,
            KvInternalContext coreContext, 
            List<Domain.Model.ProductBatchExpireDto> productBatchExPires,
            List<InvoiceWarranties> invoiceWarranties)
        {
            try
            {
                var productDetail = new StringBuilder();
                string batchExPire = string.Empty;
                double total = 0;
                if (invoice.InvoiceDetails != null && invoice.InvoiceDetails.Any())
                {
                    productDetail.Append(", bao gồm:<div>");
                    foreach (var inv in invoice.InvoiceDetails)
                    {
                        if (productBatchExPires != null || productBatchExPires.Any())
                        {
                            var productBatchExPire = productBatchExPires.FirstOrDefault(x => x.ProductId == inv.ProductId);
                            if (productBatchExPire != null) batchExPire = $" , {productBatchExPire.BatchName} - Hạn sử dụng: {productBatchExPire.ExpireDate.ToString("dd/MM/yyyy")}";

                        }
                        total += ((double)inv.Price - (double)(inv.Discount ?? 0)) * inv.Quantity;
                        productDetail.Append(
                            $"- [ProductCode]{inv.ProductCode}[/ProductCode] : {StringHelper.Normallize(inv.Quantity)}*{StringHelper.NormallizeWfp((double)inv.Price - (double)(inv.Discount ?? 0))}{batchExPire}<br>");
                        var productWarrantys = invoiceWarranties?.Where(x => x.InvoiceDetailUuid == inv.Uuid).ToList();
                        var getWarrantys = GetWarrantysLog(inv, productWarrantys);
                        productDetail.Append(string.IsNullOrEmpty(getWarrantys.ToString()) ? string.Empty : $"{getWarrantys}<br>");
                    }

                    productDetail.Append("</div>");
                }

                var log = new AuditTrailLog
                {
                    FunctionId = AuditTrailHelper.GetAuditTrailFunctionType((byte)ChannelType),
                    Action = (int)AuditTrailAction.InvoiceIntergate,
                    CreatedDate = DateTime.Now,
                    BranchId = data.BranchId,
                    Content =
                        $"Tạo hóa đơn [InvoiceCode]{invoice.Code}[/InvoiceCode] (cho đơn đặt hàng [OrderCode]{kvOrder.Code}[/OrderCode])," +
                        $"Bảng giá: {priceBookName}, giá trị: {StringHelper.NormallizeWfp(total)}, thời gian: {invoice.PurchaseDate:dd/MM/yyyy HH:mm:ss}, " +
                        $"trạng thái: {EnumHelper.ToDescription((InvoiceState)invoice.Status)}, trạng thái giao: {EnumHelper.ToDescription((DeliveryStatus)invoice.DeliveryDetail.Status)}{productDetail}"
                };

                await _auditTrailInternalClient.AddLogAsync(coreContext, log);
            }
            catch
            {
                // ignored
            }
        }

        private async Task IncreaseRetryCount(T data)
        {
            var existRetryInvoice = await _retryInvoiceMongoService.GetByOrderId(data.OrderId,
                data.ChannelId, data.BranchId, data.RetailerId);
            if (existRetryInvoice != null)
            {
                existRetryInvoice.ModifiedDate = DateTime.Now;
                existRetryInvoice.RetryCount = data.RetryCount + 1;
                await _retryInvoiceMongoService.UpdateAsync(existRetryInvoice.Id, existRetryInvoice);
            }
        }

        private async Task<bool> RetryErrorInvoice(T data, Domain.Model.Invoice existInvoice, KvInternalContext coreContext,
            LogObjectMicrosoftExtension createInvoiceLog)
        {
            if (existInvoice != null)
            {
                try
                {
                    await _invoiceInternalClient.VoidInvoice(coreContext, existInvoice.Id, data.RetailerId,
                        true);
                    await _orderInternalClient.VoidOrder(coreContext, existInvoice.OrderId.GetValueOrDefault(),
                        data.RetailerId, true);
                    PushCreateOrderMessageQueue(data);
                    createInvoiceLog.Description = "Retry Invoice";
                    createInvoiceLog.LogInfo(false);
                    return true;
                }
                catch (Exception ex)
                {
                    createInvoiceLog.LogError(ex, false, ex is OmniChannelCore.Api.Sdk.Common.KvValidateException);
                    return false;
                }
            }

            return false;
        }
        private void PushCreateOrderMessageQueue(CreateInvoiceMessageV2 invoice)
        {
            switch ((byte)ChannelType)
            {
                case (byte)Sdk.Common.ChannelType.Lazada:
                    {
                        var createOrder = new LazadaCreateOrderMessage
                        {
                            ChannelId = invoice.ChannelId,
                            ChannelType = invoice.ChannelType,
                            BranchId = invoice.BranchId,
                            KvEntities = invoice.KvEntities,
                            RetailerId = invoice.RetailerId,
                            OrderId = invoice.OrderId,
                            Order = invoice.Order,
                            PriceBookId = invoice.PriceBookId,
                        };
                        using (var mq = MessageService.MessageFactory.CreateMessageProducer())
                        {
                            mq.Publish(createOrder);
                        }

                        break;
                    }

                case (byte)Sdk.Common.ChannelType.Shopee:
                    {
                        var createOrder = new ShopeeCreateOrderMessageV2
                        {
                            ChannelId = invoice.ChannelId,
                            ChannelType = invoice.ChannelType,
                            BranchId = invoice.BranchId,
                            KvEntities = invoice.KvEntities,
                            RetailerId = invoice.RetailerId,
                            OrderId = invoice.OrderId,
                            Order = invoice.Order,
                            PriceBookId = invoice.PriceBookId,
                            SourceQueue = "Queue is created from Retry Invoice Error"
                        };
                        using (var mq = MessageService.MessageFactory.CreateMessageProducer())
                        {
                            mq.Publish(createOrder);
                        }

                        break;
                    }

                case (byte)Sdk.Common.ChannelType.Tiki:
                    {
                        var createOrder = new TikiCreateOrderMessage
                        {
                            ChannelId = invoice.ChannelId,
                            ChannelType = invoice.ChannelType,
                            BranchId = invoice.BranchId,
                            KvEntities = invoice.KvEntities,
                            RetailerId = invoice.RetailerId,
                            OrderId = invoice.OrderId,
                            Order = invoice.Order,
                            PriceBookId = invoice.PriceBookId,
                        };
                        using (var mq = MessageService.MessageFactory.CreateMessageProducer())
                        {
                            mq.Publish(createOrder);
                        }

                        break;
                    }
                case (byte)Sdk.Common.ChannelType.Tiktok:
                    {
                        var createOrder = new TikiCreateOrderMessage
                        {
                            ChannelId = invoice.ChannelId,
                            ChannelType = invoice.ChannelType,
                            BranchId = invoice.BranchId,
                            KvEntities = invoice.KvEntities,
                            RetailerId = invoice.RetailerId,
                            OrderId = invoice.OrderId,
                            Order = invoice.Order,
                            PriceBookId = invoice.PriceBookId,
                        };
                        using (var mq = MessageService.MessageFactory.CreateMessageProducer())
                        {
                            mq.Publish(createOrder);
                        }

                        break;
                    }
            }
        }
        private async Task AddOrUpdateRetryInvoice(T data, bool isDbException = false, string invoiceCode = null)
        {
            var existRetryInvoice =
                await _retryInvoiceMongoService.GetByOrderId(data.OrderId, data.ChannelId, data.BranchId,
                    data.RetailerId);
            if (existRetryInvoice != null)
            {
                existRetryInvoice.ModifiedDate = DateTime.Now;
                existRetryInvoice.RetryCount = data.RetryCount + 1;
                await _retryInvoiceMongoService.UpdateAsync(existRetryInvoice.Id, existRetryInvoice);
            }
            else
            {
                await _retryInvoiceMongoService.AddSync(new RetryInvoice
                {
                    ChannelId = data.ChannelId,
                    ChannelType = data.ChannelType,
                    BranchId = data.BranchId,
                    KvEntities = data.KvEntities,
                    RetailerId = data.RetailerId,
                    OrderId = data.OrderId,
                    Order = data.Order,
                    KvOrder = data.KvOrder,
                    KvOrderId = data.KvOrderId,
                    CreatedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    IsDbException = isDbException,
                    Code = invoiceCode,
                    RetryCount = 1
                });
            }
        }
        private async Task RemoveErrorInvoiceInMongo(string orderId, int branchId, long channelId, string invoiceCode = null)
        {
            var invoiceInMongo = await _invoiceMongoService.GetByOrderId(orderId, branchId, channelId, invoiceCode);
            if (invoiceInMongo != null)
            {
                foreach (var invoice in invoiceInMongo)
                {

                    await _invoiceMongoService.RemoveAsync(invoice.Id);
                }
            }
        }
        private async Task RemoveMongoRetryInvoice(string orderId, int branchId, long channelId, int retailerId)
        {
            var existRetryInvoice = await _retryInvoiceMongoService.GetByOrderId(orderId, channelId, branchId, retailerId);
            if (existRetryInvoice != null && !existRetryInvoice.IsDbException)
            {
                await _retryInvoiceMongoService.RemoveAsync(existRetryInvoice.Id);
            }
        }
        private async Task WriteLogForUpdateInvoice(CreateInvoiceMessageV2 data, KvInvoice invoice, byte oldDeliveryStatus, KvInternalContext context, bool logVoidInvoice = false)
        {
            if (oldDeliveryStatus != invoice.DeliveryStatus)
            {
                var log = new AuditTrailLog
                {
                    FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(data.ChannelType),
                    Action = (int)AuditTrailAction.InvoiceIntergate,
                    CreatedDate = DateTime.Now,
                    BranchId = data.BranchId
                };

                if (invoice.NewStatus == (byte)InvoiceState.Void && logVoidInvoice)
                {
                    log.Content = $"Hủy hóa đơn: [InvoiceCode]{invoice.Code}[/InvoiceCode]";
                }
                else
                {
                    var contentLog =
                        new StringBuilder(
                            $"Đồng bộ trạng thái hóa đơn ([InvoiceCode]{invoice.Code}[/InvoiceCode]), vận đơn thành công: <br/>");
                    contentLog.AppendFormat(
                        $"- Vận đơn: Trạng thái: {EnumHelper.ToDescription((DeliveryStatus)oldDeliveryStatus)} -> {EnumHelper.ToDescription((DeliveryStatus)(invoice.DeliveryStatus ?? 0))} <br/>");

                    log.Content = contentLog.ToString();
                }

                await _auditTrailInternalClient.AddLogAsync(context, log);
            }
        }
        private async Task<bool> ValidateProduct(
            List<ProductBranchDTO> kvProducts, KvInvoice invoice,
            long channelId, string orderId, byte channelType, KvOrder order, Customer customer,
            KvInternalContext context, List<ProductMapping> mappings, long authId, Guid logId, LogObjectMicrosoftExtension loggerExtension,
            OmniChannelSettingObject settings)
        {
            var result = true;
            var productLog = new StringBuilder();
            var errorMessage = new StringBuilder();

            foreach (var detail in invoice.InvoiceDetails)
            {
                var product = kvProducts.FirstOrDefault(x => x.Id == detail.ProductId);
                detail.ProductName = product?.FullName ?? detail.ProductChannelName;
                var channelTrackKey = !string.IsNullOrEmpty(detail.ProductChannelId) ? detail.ProductChannelId : detail.ProductChannelSku;
                if (product == null)
                {
                    detail.UseProductBatchExpire = false;
                    detail.UseProductSerial = false;
                    errorMessage.Append($"Hàng hóa {detail.ProductChannelName} ({channelTrackKey}) chưa liên kết với hàng hóa nào trên Demo; ");
                    productLog.AppendFormat($"- {detail.ProductChannelSku ?? detail.ProductChannelId} ({channelTrackKey}): chưa xác định trên Demo <br/>");
                    result = false;
                }
                else
                {
                    detail.UseProductBatchExpire = product.IsBatchExpireControl.HasValue ? product.IsBatchExpireControl.Value : false;
                    detail.UseProductSerial = product.IsLotSerialControl.HasValue ? product.IsLotSerialControl.Value : false;
                    var mapping = mappings.FirstOrDefault(x => x.ChannelId == channelId && ((!string.IsNullOrEmpty(detail.ProductChannelId) && x.CommonProductChannelId == detail.ProductChannelId) || x.ProductChannelSku == detail.ProductChannelSku));
                    if (mapping == null)
                    {
                        errorMessage.Append($"Hàng hóa {detail.ProductChannelName} ({channelTrackKey}) chưa liên kết với hàng hóa nào trên Demo; ");
                        productLog.AppendFormat($"- [ProductCode]{detail.ProductCode}[/ProductCode]: liên kết hàng hóa không tồn tại <br/>");
                        result = false;
                    }
                    else if (product.isDeleted == true)
                    {
                        errorMessage.Append($"Hàng hóa {detail.ProductChannelName} ({channelTrackKey}) chưa liên kết với hàng hóa nào trên Demo; ");
                        productLog.AppendFormat($"- [ProductCode]{detail.ProductCode}[/ProductCode]: đã bị xóa <br/>");
                        result = false;
                    }
                    else if (product.IsLotSerialControl == true)
                    {
                        productLog.AppendFormat($"- [ProductCode]{detail.ProductCode}[/ProductCode]: là sản phẩm serial/imei <br/>");
                        errorMessage.Append($"{detail.ProductCode}: chưa được chọn Serial/IMEI để tạo hóa đơn; ");
                        result = false;
                    }
                    else if (!settings.IsAutoSyncBatchExpire && product.IsBatchExpireControl == true)
                    {
                        productLog.AppendFormat($"- [ProductCode]{detail.ProductCode}[/ProductCode]: là sản phẩm lô/hạn <br/>");
                        errorMessage.Append($"{detail.ProductCode}: chưa xác định Lô/date để tạo hóa đơn; ");
                        result = false;
                    }
                }
            }

            if (!result)
            {
                if ((byte)InvoiceState.Void == invoice.Status || (byte)InvoiceState.Void == invoice.NewStatus)
                {
                    await RemoveSyncInvoiceError(context, invoice, orderId, channelId, loggerExtension);
                }
                else
                {
                    await SaveSyncInvoiceError(context, invoice, order, orderId, customer, channelId, channelType,
                 errorMessage.ToString(), productLog.ToString(), authId, logId, loggerExtension);
                }

            }
            return result;
        }

        private async Task SaveSyncInvoiceError(KvInternalContext context, KvInvoice invoice, KvOrder order,
            string channelOrderId, Customer customer, long channelId, byte channelType, string errorMessage,
            string auditTrailMessage, long authId, Guid logId, LogObjectMicrosoftExtension loggerExtension)
        {
            loggerExtension.Action = ExceptionType.SaveSyncInvoiceError;
            loggerExtension.ResponseObject = $"Invoice Error Information : {customer?.ToJson()}";

            try
            {
                var existInvoice = await _invoiceMongoService.GetByInvoiceId(channelOrderId, context.BranchId, channelId, invoice.Code);
                var retryInvoice = new MongoDb.Invoice
                {
                    Code = invoice.Code,
                    BranchId = invoice.BranchId,
                    SoldById = invoice.SoldById,
                    SaleChannelId = invoice.SaleChannelId ?? 0,
                    PurchaseDate = invoice.PurchaseDate,
                    Description = invoice.Description,
                    IsSyncSuccess = false,
                    UsingCod = true,
                    OrderId = invoice.OrderId,
                    DeliveryDetail = invoice.DeliveryDetail?.ConvertTo<MongoDb.InvoiceDelivery>(),
                    CustomerId = invoice.CustomerId,
                    CustomerName = customer?.Name,
                    CustomerCode = customer?.Code,
                    CustomerPhone = customer?.ContactNumber,
                    NewStatus = invoice.NewStatus,
                    Status = invoice.Status,
                    ChannelStatus = invoice.ChannelStatus,
                    ChannelId = channelId,
                    ErrorMessage = errorMessage,
                    ChannelOrderId = channelOrderId,
                    RetailerId = context.RetailerId,
                    CreatedDate = DateTime.Now,
                    Discount = invoice.Discount,
                    InvoiceDetails = new List<MongoDb.InvoiceDetail>(),
                    InvoiceSurCharges = invoice.SurCharges?.Select(p => new MongoDb.InvoiceSurCharges
                    {
                        Name = p.Name,
                        SurValue = p.Value,
                        Price = p.Value ?? 0
                    }).ToList() ?? new List<InvoiceSurCharges>()
                };
                foreach (var item in invoice.InvoiceDetails)
                {
                    var detail = new MongoDb.InvoiceDetail
                    {
                        CommonProductChannelId = item.ProductChannelId,
                        CommonParentChannelProductId = item.ParentChannelProductId,
                        Price = item.Price,
                        Discount = item.Discount,
                        DiscountPrice = item.DiscountPrice,
                        DiscountRatio = item.DiscountRatio,
                        ProductBatchExpireId = item.ProductBatchExpireId,
                        ProductChannelName = item.ProductChannelName,
                        ProductCode = item.ProductCode,
                        ProductId = item.ProductId,
                        ProductChannelSku = item.ProductChannelSku,
                        ProductName = item.ProductName,
                        Quantity = item.Quantity,
                        SerialNumbers = item.SerialNumbers,
                        SubTotal = item.SubTotal,
                        UsePoint = item.UsePoint,
                        UseProductBatchExpire = item.UseProductBatchExpire,
                        UseProductSerial = item.UseProductSerial,
                        Note = item.Note,
                        ReturnQuantity = item.ReturnQuantity,
                        Uuid = item.Uuid
                    };
                    retryInvoice.InvoiceDetails.Add(detail);
                }

                if (existInvoice == null)
                {
                    await _invoiceMongoService.AddSync(retryInvoice);
                }
                else
                {
                    retryInvoice.Id = existInvoice.Id;
                    await _invoiceMongoService.UpdateAsync(existInvoice.Id, retryInvoice);
                }
                PushLishMessageErrorNotification(order, channelOrderId, channelId, invoice.Code, errorMessage, loggerExtension);
                #region AuditTrail
                await WriteLogForCreateInvoiceFail(context, invoice, order, channelId, channelType, auditTrailMessage, authId, logId, loggerExtension);

                #endregion
            }
            catch (Exception e)
            {
                loggerExtension.LogError(e);
                throw;
            }


        }

        private async Task RemoveSyncInvoiceError(KvInternalContext context, KvInvoice invoice, string channelOrderId, long channelId, LogObjectMicrosoftExtension loggerExtension)
        {
            loggerExtension.Action = ExceptionType.RemoveSyncInvoiceError;
            loggerExtension.ResponseObject = $"Remove Invoice Error Information: " + invoice.ToSafeJson();
            try
            {
                var existInvoice = await _invoiceMongoService.GetByInvoiceId(channelOrderId, context.BranchId, channelId, invoice.Code);

                if (existInvoice != null)
                {
                    await _invoiceMongoService.RemoveAsync(existInvoice.Id);
                }
                loggerExtension.LogInfo(true);
            }
            catch (Exception e)
            {
                loggerExtension.LogError(e);
                throw;
            }
        }

        private async Task WriteLogForCreateInvoiceFail(KvInternalContext context, KvInvoice invoice, KvOrder order,
            long channelId, byte channelType, string auditTrailMessage, long authId, Guid logId, LogObjectMicrosoftExtension logExtension)
        {
            var log = new AuditTrailLog
            {
                FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channelType),
                Action = (int)AuditTrailAction.InvoiceIntergate,
                CreatedDate = DateTime.Now,
                BranchId = context.BranchId,
                Content =
                    $"Tạo hóa đơn KHÔNG thành công: [InvoiceCode]{invoice.Code}[/InvoiceCode] (cho đơn đặt hàng [OrderCode]{order.Code}[/OrderCode]) lý do: <br/>{auditTrailMessage}"
            };
            try
            {
                await _auditTrailInternalClient.AddLogAsync(context, log);
            }
            catch (Exception e)
            {
                logExtension.Action = ExceptionType.CreateInvoice;
                logExtension.LogError(e);
                if (e is KvRetailerExpireException)
                {
                    await DeActiveChannel(channelId, context.ConvertTo<Sdk.Common.KvInternalContext>(), authId, channelType);
                }
            }
        }

        private async Task VoidKvOrder(CreateInvoiceMessageV2 data, KvInternalContext kvInternalContext,
            byte channelType, KvOrder order, InvoiceRepository invoiceRepository,
            PriceBookRepository priceBookRepository, ProductRepository productRepository,
            LogObjectMicrosoftExtension logExtension)
        {
            if (order == null || (order.Status == (byte)OrderState.Void))
            {
                return;
            }

            logExtension.Action = ExceptionType.InvoiceVoidOrder;
            var isVoidInvoiceSuccess = false;
            var existInvoices = await invoiceRepository.GetByOrderId(order.Id, data.RetailerId);
            if (existInvoices != null && existInvoices.Any())
            {
                existInvoices = existInvoices
                    .GroupBy(x => x.Code)
                    .Select(x => x.First())
                    .ToList();
                foreach (var invoice in existInvoices)
                {
                    try
                    {
                        if (invoice.Status == (byte)InvoiceState.Void)
                        {
                            isVoidInvoiceSuccess = true;
                            continue;
                        }
                        await _invoiceInternalClient.VoidInvoice(kvInternalContext, invoice.Id);

                        var invoiceLog = new AuditTrailLog
                        {
                            FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channelType),
                            Action = (int)AuditTrailAction.OrderIntergate,
                            CreatedDate = DateTime.Now,
                            BranchId = data.BranchId
                        };
                        //TMDT-22
                        if (ValidateChannel(channelType))
                        {
                            logExtension.Description = $"Void order and invoice";
                            logExtension.LogInfo();
                            var priceBookName = "Bảng giá chung";

                            if (invoice.PriceBookId > 0)
                            {
                                var priceBook = await priceBookRepository.SingleAsync(p => p.RetailerId == data.RetailerId && p.Id == invoice.PriceBookId);
                                priceBookName = priceBook?.Name ?? priceBookName;
                            }
                            var deliveryInfo = await _deliveryInfoInternalClient.GetLastByInvoiceId(kvInternalContext, invoice.Id);
                            var deliveryCode = deliveryInfo?.DeliveryCode;

                            invoiceLog.Content = $"Hủy hóa đơn: [InvoiceCode]{invoice.Code}[/InvoiceCode] " +
                                $"(cho đơn đặt hàng: [OrderCode]{order.Code}[/OrderCode]), mã vận đơn {deliveryCode},  " +
                                $"Bảng giá: {priceBookName}, giá trị: {StringHelper.NormallizeWfp((double)invoice.Total)}, thời gian: {invoice.PurchaseDate:dd/MM/yyyy HH:mm:ss} bao gồm: <br/>";

                            var kvProducts = await productRepository.GetProductByIds(data.RetailerId, data.BranchId, invoice.InvoiceDetails.Select(y => y.ProductId).ToList(), isLogMonitor: false);
                            foreach (var item in invoice.InvoiceDetails)
                            {
                                var p = kvProducts.FirstOrDefault(x => x.Id == item.ProductId);
                                invoiceLog.Content += $"- [ProductCode]{p?.Code}[/ProductCode]: {StringHelper.Normallize(item.Quantity)} * {StringHelper.NormallizeWfp((double)(item.Price))} <br/>";
                            }
                        }
                        else
                        {
                            invoiceLog.Content = $"Hủy hóa đơn: [InvoiceCode]{invoice.Code}[/InvoiceCode]";
                        }

                        await _auditTrailInternalClient.AddLogAsync(kvInternalContext, invoiceLog);
                        isVoidInvoiceSuccess = true;
                    }
                    catch (Exception ex)
                    {
                        var invoiceLog = new AuditTrailLog
                        {
                            FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channelType),
                            Action = (int)AuditTrailAction.OrderIntergate,
                            CreatedDate = DateTime.Now,
                            BranchId = data.BranchId,
                            Content = $"Cập nhật thông tin đơn đặt hàng KHÔNG thành công: [OrderCode]{order.Code}[/OrderCode], lý do: <br/> Không hủy được hóa đơn liên quan"
                        };
                        await _auditTrailInternalClient.AddLogAsync(kvInternalContext, invoiceLog);
                        logExtension.LogError(ex);
                        return;
                    }
                }
            }

            //TMDT-22 không hủy đơn hàng shopee sau khi hủy hóa đơn
            if (channelType != (byte)Sdk.Common.ChannelType.Shopee ||
                !isVoidInvoiceSuccess ||
                existInvoices == null ||
                (channelType == (byte)Sdk.Common.ChannelType.Shopee && order.Status != (byte)OrderState.Finalized)
            )
            {
                var logOrder = new AuditTrailLog
                {
                    FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channelType),
                    Action = (int)AuditTrailAction.OrderIntergate,
                    CreatedDate = DateTime.Now,
                    BranchId = data.BranchId,
                    Content = $"Hủy đơn đặt hàng: [OrderCode]{order.Code}[/OrderCode]"
                };
                await _orderInternalClient.VoidOrder(kvInternalContext, order.Id);
                await _auditTrailInternalClient.AddLogAsync(kvInternalContext, logOrder);
            }
        }

        private async Task<int> DelayByProductLock(long channelId, List<long> productIds)
        {
            var countDelay = 0;
            var millisecondsDelay = 1000;
            while (true)
            {
                if (countDelay >= 10 || !HasProductLock()) break;

                countDelay++;
                await Task.Delay(millisecondsDelay);

                bool HasProductLock()
                {
                    foreach (var productId in productIds)
                    {
                        var productLock = CacheClient.Get<bool>(string.Format(KvConstant.LockProductStockCache, channelId, productId));

                        if (productLock) return true;
                    }

                    return false;
                }
            }

            return countDelay * millisecondsDelay;
        }

        private bool ValidateChannel(byte channelType)
        {
            if (channelType == (byte)ShareKernel.Common.ChannelType.Shopee || channelType == (byte)ShareKernel.Common.ChannelType.Tiktok)
            {
                return true;
            }
            return false;
        }

        private void PushLishMessageErrorNotification(KvOrder kvOrder, string orderChannelId, long channelId, string invoiceCode, string errorMessage, LogObjectMicrosoftExtension logInfo)
        {
            logInfo.Clone("PushLishMessageErrorNotification");
            var obj = new Infrastructure.EventBus.Event.ErrorOrderInvoiceNotificationMesage(logInfo.Id)
            {
                RetailerId = kvOrder.RetailerId,
                BranchId = kvOrder.BranchId,
                ChannelId = channelId,
                OrderKv = invoiceCode,
                OrderId = orderChannelId,
                PurchaseDate = kvOrder.PurchaseDate,
                ErrorType = (int)Demo.OmniChannel.Sdk.Common.SyncTabError.Invoice,
                ErrorMessage = errorMessage,
                Type = (int)ChannelType
            };
            _integrationEventService.AddEventWithRoutingKeyAsync(obj, RoutingKey.MessageErrorNotification);
            logInfo.RequestObject = obj.ToJson();
            logInfo.LogInfo(true);
        }

        private async Task<List<KvInvoice>> GetKvInvoice(
            IBaseClient client, T data, KvOrder kvOrder,
            DeliveryInfoForOrder deliveryInfo,
            bool isReturningInvoice,
            LogObjectMicrosoftExtension loggerExtension,
            InvoiceRepository invoiceRepository,
            PriceBookRepository priceBookRepository,
            ProductRepository productRepository,
            KvInternalContext kvInternalContext)
        {
            //Nếu đơn hàng trên sàn hủy thực hiện hủy đơn KV
            var channelOrder = JsonConvert.DeserializeObject<KvOrder>(data.Order);
            if (channelOrder.ChannelIsCancelOrder)
            {
                await RemoveErrorInvoiceInMongo(data.OrderId, data.BranchId, data.ChannelId);
                await VoidKvOrder(data, kvInternalContext, (byte)ChannelType, kvOrder, invoiceRepository, priceBookRepository, productRepository, loggerExtension);
                await RemoveMongoRetryInvoice(data.OrderId, data.BranchId, data.ChannelId, data.RetailerId);
                return null;
            }

            return client.GenKvInvoices(
                new CreateInvoiceRequest
                {
                    BranchId = data.BranchId,
                    OrderId = data.OrderId,
                    Order = data.Order,
                    PriceBookId = 0,
                    KvOrder = kvOrder,
                    KvDelivery = deliveryInfo.ConvertTo<DeliveryInfoDTO>(),
                    RetryCount = data.RetryCount,
                    IsConfirmReturning = isReturningInvoice
                });
        }

        private StringBuilder GetWarrantysLog(OmniChannel.ChannelClient.Models.InvoiceDetail invoiceDetail,
           List<InvoiceWarranties> invoiceWarrantys)
        {
            var warrantysLog = new StringBuilder();

            var checkProductIsCombo = !invoiceWarrantys.Where(x => x.ProductId == invoiceDetail.ProductId).Any();
            var productWarrantysGroup = invoiceWarrantys.GroupBy(x => new { x.ProductId, x.ProductCode }).ToList();
            foreach (var group in productWarrantysGroup)
            {
                var guarrantee = new StringBuilder();
                var mainGuarrantee = string.Empty;
                var productWarrantys = invoiceWarrantys.Where(x => x.ProductId == group.Key.ProductId).ToList();

                productWarrantys?.ForEach(w =>
                {
                    if (w.WarrantyType == (int)WarrantyType.Maintenance)
                    {
                        mainGuarrantee = string.Format("Định kỳ bảo trì: {0} {1} ({2}) đến {3}",
                            w.NumberTime, SystemHelper.GetWarrantyTimeTypeText(w.TimeType).ToLower(),
                            w.Description, ((DateTime)w.ExpireDate).ToString("dd/MM/yyyy"));
                    }
                    else
                    {
                        var guarranteelog = string.Format("{0} {1} ({2}) đến {3}",
                           w.NumberTime, SystemHelper.GetWarrantyTimeTypeText(w.TimeType).ToLower(),
                           w.Description, ((DateTime)w.ExpireDate).ToString("dd/MM/yyyy"));

                        guarrantee.Append(string.IsNullOrEmpty(guarrantee.ToString())
                            ? $"Bảo hành: {guarranteelog}"
                            : $", {guarranteelog}");
                    }
                });
                if (checkProductIsCombo)
                {
                    warrantysLog.Append($"Thành phần bảo hành: [ProductCode]{group.Key.ProductCode}[/ProductCode]<br />");
                }
                warrantysLog.Append(string.IsNullOrEmpty(guarrantee.ToString()) ? string.Empty : $"&nbsp;&nbsp;{guarrantee}<br>");
                warrantysLog.Append(string.IsNullOrEmpty(mainGuarrantee) ? string.Empty : $"&nbsp;&nbsp;{mainGuarrantee}<br>");
            }

            return warrantysLog;
        }
    }
}

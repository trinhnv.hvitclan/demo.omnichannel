﻿using System;
using System.Collections.Generic;
using System.Linq;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.Domain.Model;
using ServiceStack;
using InvoiceDetail = Demo.OmniChannel.ChannelClient.Models.InvoiceDetail;

namespace Demo.OmniChannel.OrderService.OrderDomain
{
    public static class DetechHasExpire
    {
        //return: orderDetail list after detech
        public static List<KvOrderDetail> DetectOrderDetailHasBatchExpire(
            IReadOnlyCollection<ProductBranchDTO> productBranchDtos,
            List<KvOrderDetail> kvOrderDetails)
        {
            foreach (var orderDetail in kvOrderDetails)
            {
                var isProductExpire = productBranchDtos?.FirstOrDefault(p => p.Id == orderDetail.ProductId)
                    ?.IsBatchExpireControl ?? false;

                if (isProductExpire) orderDetail.IsBatchExpireControl = true;
            }

            return kvOrderDetails;
        }

        //return: invoiceDetail list after detech
        public static List<InvoiceDetail> DetectInvoiceDetailHasBatchExpire(
            IReadOnlyCollection<ProductBranchDTO> productBranchDtos,
            List<InvoiceDetail> kvInvoiceDetails,
            List<ProductBatchExpireDto> productBatchExpireDtos)
        {
            if (productBatchExpireDtos == null || !productBatchExpireDtos.Any()) return kvInvoiceDetails;

            var invoiceSplitedDetails = new List<InvoiceDetail>();
            foreach (var invoiceDetail in kvInvoiceDetails)
            {
                var productId = invoiceDetail.ProductId;
                var productItem = productBranchDtos?.FirstOrDefault(p => p.Id == productId);
                if (productItem == null) continue;
                if (!productItem.IsBatchExpireControl.GetValueOrDefault()) { invoiceSplitedDetails.Add(invoiceDetail); continue; }
                if (productItem.MasterUnitId > 0 && !productItem.IsHasVariantsAndSingleUnit())
                {
                    productId = productItem.MasterUnitId ?? 0;
                }

                invoiceDetail.IsBatchExpireControl = true;
                var invoiceDetails = SplitProductBatchExpireInvoiceDetail(
                    invoiceDetail,
                    productBatchExpireDtos,
                    productItem.ConversionValue,
                    productId);
                invoiceSplitedDetails.AddRange(invoiceDetails);
            }

            return invoiceSplitedDetails;
        }

        private static List<InvoiceDetail> SplitProductBatchExpireInvoiceDetail(
            InvoiceDetail invoiceDetail, 
            List<ProductBatchExpireDto> productBatchExpireDtos,
            double conversionValue,
            long productId)
        {
            var invoiceDetails = new List<InvoiceDetail>();
            var remainedQuantity = invoiceDetail.Quantity;
            foreach(var productBatchExpire in productBatchExpireDtos)
            {
                if (productBatchExpire.ProductId != productId)
                    continue;
                var onhand = Math.Round(productBatchExpire.OnHand / conversionValue, 3);
                // KOL-9082
                if (onhand >= 0.001)
                {
                    invoiceDetail.ProductBatchExpireId = productBatchExpire.Id;
                    invoiceDetail.Quantity = remainedQuantity;
                    invoiceDetails.Add(invoiceDetail);
                    
                    remainedQuantity = remainedQuantity - onhand;
                    if (remainedQuantity <= 0)      // onhand > remainedQuantity -> stop
                    {
                        break;
                    }
                    invoiceDetail.Quantity = onhand;    // onhand < remainedQuantity -> take all onhand then add new batch
                    // add new batch
                    invoiceDetail = invoiceDetail.CreateCopy();
                }
            }

            return invoiceDetails;
        }
    }
}

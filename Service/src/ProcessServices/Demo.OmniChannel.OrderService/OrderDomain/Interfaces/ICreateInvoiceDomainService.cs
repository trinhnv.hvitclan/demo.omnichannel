﻿using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannelCore.Api.Sdk.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.OrderService.OrderDomain.Interfaces
{
    public interface ICreateInvoiceDomainService
    {
        (bool, string, string) ValidateAutoSyncBatchExpire(List<InvoiceDetail> before,
            List<InvoiceDetail> after);

        Task<List<OmniChannelCore.Api.Sdk.Models.InvoiceWarranties>> GetInvoiceWarranties(
            KvInternalContext context,
           ProductFormulaRepository productFormulaRepository,
           List<Domain.Model.ProductBranchDTO> productData,
           List<MongoDb.InvoiceDetail> invoiceDetails,
           DateTime purchaseDate);

        List<MongoDb.InvoiceDetail> SplitOrderDetails(
            List<Domain.Model.ProductBranchDTO> productData,
            List<MongoDb.InvoiceDetail> invoiceDetails);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannelCore.Api.Sdk.Common;
using Demo.OmniChannelCore.Api.Sdk.Models;

namespace Demo.OmniChannel.OrderService.OrderDomain.Interfaces
{
    public interface ICreateOrderDomainService
    {
        Task<List<InvoiceSurCharges>> GetListInvoiceSurchargeAsync(List<ChannelClient.Models.SurCharge> surCharges,
            KvInternalContext context, BaseMessage data);

        Task<(Domain.Model.Customer newCustomer, bool isError, bool isRetailerExpireException)> CreateCustomerAsync(
            ChannelClient.Models.KvOrder kvOrder,
            CustomerRepository customerRepository, BaseMessage data, bool isManagerCustomerByBranch,
            KvInternalContext kvInternalContext, LogObjectMicrosoftExtension logInfo);

        Task<ChannelClient.Models.Extra> GetExtraAsync(PriceBookRepository priceBookRepository, long priceBookId, BaseMessage data);

        Task<(long partnerId, bool isRetailerExpireException)> CreatePartnerAsync(PartnerDeliveryRepository partnerRepository, ChannelClient.Models.InvoiceDelivery orderDelivery,
            BaseMessage data,
            KvInternalContext kvInternalContext,
            LogObjectMicrosoftExtension logInfo);

        Task<List<InvoiceWarranties>> GetInvoiceWarranties(
           KvInternalContext context,
           List<Domain.Model.ProductBranchDTO> products,
           ProductFormulaRepository productFormulaRepository,
           List<MongoDb.OrderDetail> orderDetails,
          DateTime purchaseDate);
    }
}

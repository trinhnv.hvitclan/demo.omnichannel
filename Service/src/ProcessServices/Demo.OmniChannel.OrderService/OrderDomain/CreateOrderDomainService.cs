﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Infrastructure.DemoGuaranteeClient;
using Demo.OmniChannel.Infrastructure.DemoGuaranteeClient.Interfaces;
using Demo.OmniChannel.OrderService.OrderDomain.Interfaces;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Services.LogginConfiguration;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Common;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Demo.OmniChannelCore.Api.Sdk.Models;
using Microsoft.Extensions.Logging;
using ServiceStack.Data;
using Microsoft.Extensions.Options;
using ServiceStack;

namespace Demo.OmniChannel.OrderService.OrderDomain
{
    public class CreateOrderDomainService : BaseDomainService, ICreateOrderDomainService
    {
        private readonly ISurChargeInternalClient _surChargeInternalClient;
        private readonly ISurChargeBranchInternalClient _surChargeBranchInternalClient;
        private readonly IKvLockRedis _kvLockRedis;
        private readonly ICustomerInternalClient _customerInternalClient;
        private readonly IPartnerDeliveryInternalClient _partnerInternalClient;
        private readonly ILogger<CreateOrderDomainService> _logger;
        private readonly IGuaranteeClient _guaranteeClient;
        private readonly GuaranteeClientConfiguration _crmClientConfiguration;

        public CreateOrderDomainService(
            ISurChargeInternalClient surChargeInternalClient,
            ISurChargeBranchInternalClient surChargeBranchInternalClient,
            IKvLockRedis kvLockRedis,
            ICustomerInternalClient customerInternalClient,
            IPartnerDeliveryInternalClient partnerInternalClient,
            ILogger<CreateOrderDomainService> logger,
            IDbConnectionFactory dbConnectionFactory,
            IGuaranteeClient guaranteeClient,
            Microsoft.Extensions.Options.IOptions<GuaranteeClientConfiguration> crmClientConfiguration
        ) : base(dbConnectionFactory)
        {
            _surChargeInternalClient = surChargeInternalClient;
            _surChargeBranchInternalClient = surChargeBranchInternalClient;
            _kvLockRedis = kvLockRedis;
            _customerInternalClient = customerInternalClient;
            _partnerInternalClient = partnerInternalClient;
            _guaranteeClient = guaranteeClient;
            _crmClientConfiguration = crmClientConfiguration.Value;
            _logger = logger;
        }
        public async Task<List<InvoiceSurCharges>> GetListInvoiceSurchargeAsync(
            List<ChannelClient.Models.SurCharge> surCharges, KvInternalContext coreContext, BaseMessage data)
        {
            var lstInvoiceSurcharge = new List<InvoiceSurCharges>();
            if (surCharges != null && surCharges.Count > 0)
            {
                foreach (var itemSurCharge in surCharges)
                {
                    var codeByGen = GenerateSurchargeCode(data.ChannelType, data.RetailerId, itemSurCharge.Type ?? 0);
                    var surcharge =
                        await _surChargeInternalClient.GetSurchargeByCode(coreContext, codeByGen);

                    if (surcharge != null && !surcharge.isActive)
                    {
                        continue;
                    }

                    if (surcharge == null)
                    {
                        var surchargeTmp = new SurCharge
                        {
                            Code = codeByGen,
                            Name = itemSurCharge.Name,
                            Value = itemSurCharge.Value,
                            RetailerId = data.RetailerId,
                            isActive = true,
                            isAuto = false,
                            ForAllBranch = true,
                            isReturnAuto = true
                        };
                        surcharge = await _surChargeInternalClient.CreateSurCharge(coreContext, surchargeTmp);
                    }

                    var isApproveSurcharge = await CheckSurchargeActiveWithBranch(coreContext, surcharge, data.BranchId);

                    if (isApproveSurcharge)
                    {
                        //create surcharge for Order
                        var invoiceSurcharge = new InvoiceSurCharges
                        {
                            SurchargeId = surcharge?.Id ?? 0,
                            Price = itemSurCharge.Value ?? 0,
                            RetailerId = data.RetailerId,
                            Name = surcharge?.Name,
                            SurValue = itemSurCharge.Value
                        };

                        lstInvoiceSurcharge.Add(invoiceSurcharge);
                    }
                }
            }

            return lstInvoiceSurcharge;
        }

        public async Task<(Domain.Model.Customer newCustomer, bool isError, bool isRetailerExpireException)> CreateCustomerAsync(ChannelClient.Models.KvOrder kvOrder,
            CustomerRepository customerRepository, BaseMessage data, bool isManagerCustomerByBranch, KvInternalContext kvInternalContext, LogObjectMicrosoftExtension logInfo)
        {
            var loggerObj = new LogObjectMicrosoftExtension(_logger, logInfo.Id)
            {
                Action = ExceptionType.CreateCustomer,
                RetailerId = logInfo.RetailerId,
                OmniChannelId = logInfo.OmniChannelId,
                RequestObject = kvOrder
            };
            var newCustomer = new Domain.Model.Customer();
            var phone = PhoneNumberHelper.GetPerfectContactNumber(kvOrder.CustomerPhone);

            //Kiểm tra có on thiết lập đồng bộ khách hàng không
            var settings = await GetPlatformSetting(data.RetailerId);

            if (data.ChannelType == (byte)ChannelTypeEnum.Lazada && !settings.IsLazadaSyncCustomer ||
                data.ChannelType == (byte)ChannelTypeEnum.Tiki && !settings.IsTikiSyncCustomer ||
                data.ChannelType == (byte)ChannelTypeEnum.Sendo && !settings.IsSendoSyncCustomer)
            {
                logInfo.Description = $"Tạo khách lẻ vì settings of sync customer";
                logInfo.LogInfo(true);
                return (newCustomer, false, false);
            }

            using (var cli = _kvLockRedis.GetLockFactory())
            {
                var lockAddCustomerKey = phone;
                if (string.IsNullOrEmpty(phone))
                {
                    lockAddCustomerKey = kvOrder.Code;
                }

                using (cli.AcquireLock(string.Format(KvConstant.LockAddCustomer, lockAddCustomerKey),
                    TimeSpan.FromSeconds(60)))
                {
                    Domain.Model.Customer existsCustomer = await _GetCustomerAsync(customerRepository, data.RetailerId, data.BranchId, phone, kvOrder.CustomerCode, isManagerCustomerByBranch, logInfo);

                    newCustomer.ContactNumber = phone;

                    if (existsCustomer == null)
                    {
                        try
                        {
                            var customer = new Customer
                            {
                                Code = kvOrder.CustomerCode,
                                ContactNumber = phone,
                                Name = kvOrder.CustomerName,
                                Address = kvOrder.CustomerAddress,
                                BranchId = data.BranchId,
                                RetailerId = data.RetailerId
                            };
                            if (!string.IsNullOrEmpty(kvOrder.CustomerLocation))
                            {
                                customer.LocationName = kvOrder.CustomerLocation;
                            }

                            if (!string.IsNullOrEmpty(kvOrder.CustomerWard))
                            {
                                customer.WardName = kvOrder.CustomerWard;
                            }


                            var customerId =
                                await _customerInternalClient.CreateCustomerAsync(kvInternalContext,
                                    customer);

                            loggerObj.ResponseObject = customerId;
                            loggerObj.LogInfo(true);

                            if (customerId < 1)
                            {
                                return (newCustomer, true, false);
                            }

                            newCustomer.Id = customerId;
                            newCustomer.Code = kvOrder.CustomerCode;
                            newCustomer.Name = kvOrder.CustomerName;
                        }
                        catch (Exception ex)
                        {
                            loggerObj.ResponseObject = new { ex.Message, ex.StackTrace };
                            loggerObj.LogInfo(true);
                            if (ex is KvRetailerExpireException)
                            {
                                return (newCustomer, false, true);
                            }

                            existsCustomer = await _GetCustomerAsync(customerRepository, data.RetailerId, data.BranchId, phone, kvOrder.CustomerCode, isManagerCustomerByBranch, loggerObj);

                            if (existsCustomer != null)
                            {
                                newCustomer.Id = existsCustomer.Id;
                                newCustomer.Code = existsCustomer.Code;
                                newCustomer.Name = existsCustomer.Name;
                            }
                        }
                    }
                    else
                    {
                        newCustomer.Id = existsCustomer.Id;
                        newCustomer.Code = existsCustomer.Code;
                        newCustomer.Name = existsCustomer.Name;
                    }
                }


            }
            return (newCustomer, false, false);
        }

        public async Task<ChannelClient.Models.Extra> GetExtraAsync(PriceBookRepository priceBookRepository, long priceBookId, BaseMessage data)
        {
            var extra = new ChannelClient.Models.Extra
            {
                PriceBookId = new ChannelClient.Models.PriceBook
                {
                    Id = 0,
                    Name = "Bảng giá chung"
                }
            };
            if (priceBookId > 0)
            {
                var priceBook = await priceBookRepository.SingleAsync(p => p.RetailerId == data.RetailerId && p.Id == priceBookId);

                if (priceBook != null && priceBook.IsActive && priceBook.isDeleted != true &&
                    priceBook.StartDate < DateTime.Now && priceBook.EndDate > DateTime.Now)
                {
                    extra = new ChannelClient.Models.Extra
                    {
                        PriceBookId = new ChannelClient.Models.PriceBook
                        {
                            Id = priceBook.Id,
                            Name = priceBook.Name
                        }
                    };
                }
            }
            return extra;
        }

        public async Task<(long partnerId, bool isRetailerExpireException)> CreatePartnerAsync(
            PartnerDeliveryRepository partnerRepository, ChannelClient.Models.InvoiceDelivery orderDelivery,
            BaseMessage data,
            KvInternalContext kvInternalContext,
            LogObjectMicrosoftExtension logInfo)
        {
            var loggerObj = new LogObjectMicrosoftExtension(_logger, logInfo.Id)
            {
                Action = ExceptionType.CreatePartner,
                RetailerId = logInfo.RetailerId,
                OmniChannelId = logInfo.OmniChannelId,
                RequestObject = orderDelivery
            };
            long deliveryBy = 0;
            bool isRetailerExpireException = false;
            var partnerDelivery = await partnerRepository.SingleAsync(p =>
                           p.RetailerId == data.RetailerId &&
                           p.Code.ToLower() == orderDelivery.PartnerDelivery.Code.ToLower());

            if (partnerDelivery == null && orderDelivery?.PartnerDelivery != null)
            {
                var newPartner = orderDelivery.PartnerDelivery;
                newPartner.RetailerId = data.RetailerId;
                newPartner.IsOmniChannel = true;
                try
                {
                    var partnerId = await _partnerInternalClient.CreatePartner(kvInternalContext,
                        newPartner.ConvertTo<PartnerDelivery>());
                    loggerObj.ResponseObject = partnerId;
                    loggerObj.LogInfo(true);
                    deliveryBy = partnerId;
                }
                catch (Exception e)
                {
                    if (e is KvRetailerExpireException)
                    {
                        isRetailerExpireException = true;
                    }
                    else
                    {
                        partnerDelivery = await partnerRepository.SingleAsync(p =>
                            p.RetailerId == data.RetailerId && p.Code.ToLower() ==
                            orderDelivery.PartnerDelivery.Code.ToLower());
                        if (partnerDelivery != null)
                        {
                            deliveryBy = partnerDelivery.Id;
                        }
                    }
                    loggerObj.ResponseObject = e.Message + e.StackTrace;
                    loggerObj.LogInfo(true);
                }
            }
            else if (partnerDelivery != null)
            {
                if (partnerDelivery.IsOmniChannel == null || partnerDelivery.IsOmniChannel == false)
                {
                    partnerDelivery.IsOmniChannel = true;
                    await _partnerInternalClient.UpdatePartnerOmniChannel(kvInternalContext,
                            partnerDelivery.ConvertTo<PartnerDelivery>());
                }
                deliveryBy = partnerDelivery.Id;
            }

            return (deliveryBy, isRetailerExpireException);
        }

        public async Task<List<InvoiceWarranties>> GetInvoiceWarranties(
            KvInternalContext context,
            List<Domain.Model.ProductBranchDTO> products,
            ProductFormulaRepository productFormulaRepository,
            List<MongoDb.OrderDetail> orderDetails,
           DateTime purchaseDate)
        {
            if (_crmClientConfiguration.IncludeRetailerIds.Any() && !_crmClientConfiguration.IncludeRetailerIds.Contains(context.RetailerId))
                return new List<InvoiceWarranties>();

            var productIds = orderDetails.Where(x => !string.IsNullOrEmpty(x.Uuid)).Select(x => x.ProductId).ToList();
            if (productIds == null || !productIds.Any()) return new List<InvoiceWarranties>();

            var productCombo = products.Where(x => x.ProductType == (byte)ProductType.Manufactured).ToList();

            var productFormula = productCombo.Any() ? await productFormulaRepository.GetByProductIds(context.RetailerId,
                 productCombo.Select(x => x.Id).ToList()) : new List<Domain.Model.ProductFormula>();

            if (products.All(x => x.IsWarranty == null || x.IsWarranty == 0)
                && productFormula.All(x => x.IsWarranty == null || x.IsWarranty == 0))
            {
                return new List<InvoiceWarranties>();
            }

            productIds.AddRange(productFormula.Select(x => x.MaterialId));

            var warrantys = await _guaranteeClient.GetWarrantyListByProductIds(
                new ShareKernel.Auth.ExecutionContext(context.RetailerId, context.RetailerCode,
                context.GroupId), productIds);

            if (warrantys == null || !warrantys.Any()) return new List<InvoiceWarranties>();

            var invoiceWarrantys = new List<InvoiceWarranties>();

            orderDetails.ForEach(detail =>
            {
                var getproductCombo = productFormula.Where(pf => pf.ProductId == detail.ProductId);
                var warrantysByProduct = warrantys.Where(w => w.ProductId == detail.ProductId
                    || getproductCombo.Select(x => x.MaterialId).Contains(w.ProductId)).ToList();

                var invoiceWarranty = warrantysByProduct?.Select(x =>
                {
                    return new InvoiceWarranties
                    {
                        Description = x.Description,
                        NumberTime = x.NumberTime,
                        TimeType = x.TimeType,
                        ProductId = x.ProductId,
                        ExpireDate = SyncHelper.CalExpireWarranty(x.TimeType, x.NumberTime, purchaseDate),
                        ProductCode = getproductCombo.FirstOrDefault(p => p.MaterialId == x.ProductId)?.MaterialCode,
                        InvoiceDetailUuid = detail.Uuid,
                        WarrantyType = x.WarrantyType
                    };
                }).ToList();
                invoiceWarrantys.AddRange(invoiceWarranty);
            });

            return invoiceWarrantys;
        }

        #region Private method
        private string GenerateSurchargeCode(byte channelType, int retailerId, byte surchargeType)
        {
            var code = string.Empty;
            var prefix = "TH";
            if (surchargeType == (byte)ShopeeSurchargeType.FeeShip) prefix = "TL";

            switch (channelType)
            {
                case (byte)Sdk.Common.ChannelType.Lazada:
                    {
                        code = $"{prefix}LZD_{retailerId}";
                        break;
                    }
                case (byte)Sdk.Common.ChannelType.Shopee:
                    {
                        code = $"{prefix}SPE_{retailerId}";
                        break;
                    }
                case (byte)Sdk.Common.ChannelType.Sendo:
                    {
                        code = $"{prefix}SDO_{retailerId}";
                        break;
                    }
                case (byte)Sdk.Common.ChannelType.Tiki:
                    {
                        code = $"{prefix}TIKI_{retailerId}";
                        break;
                    }
                case (byte)Sdk.Common.ChannelType.Tiktok:
                    {
                        code = $"{prefix}TTS_{retailerId}";
                        break;
                    }
            }

            return code;
        }

        private async Task<bool> CheckSurchargeActiveWithBranch(KvInternalContext coreContext, SurCharge surcharge, int branchId)
        {
            if (surcharge.ForAllBranch) return true;
            var surchargeByBranch =
                await _surChargeBranchInternalClient.GetSurChargeBranchById(coreContext, surcharge.Id);
            var isApproveSurcharge = surchargeByBranch?.Any(b => b.BranchId == branchId) ?? false;
            return isApproveSurcharge;
        }

        private async Task<Domain.Model.Customer> _GetCustomerAsync(CustomerRepository customerRepository, int retailerId, int branchId, string phone, string customerCode, bool isManagerCustomerByBranch, LogObjectMicrosoftExtension logInfo)
        {
            var loggerObj = new LogObjectMicrosoftExtension(_logger, logInfo.Id)
            {
                Action = "GetCustomer",
                RetailerId = logInfo.RetailerId,
                OmniChannelId = logInfo.OmniChannelId,
                RequestObject = customerCode
            };
            Domain.Model.Customer existsCustomer = null;
            try
            {
                existsCustomer = await customerRepository.GetByFilterAsync(retailerId, branchId, phone, customerCode, isManagerCustomerByBranch);
            }
            catch (Exception ex)
            {
                loggerObj.ResponseObject = "Error_GetByFilterAsync";
                loggerObj.LogError(ex, true);
            }
            return existsCustomer;
        }
        #endregion
    }
}

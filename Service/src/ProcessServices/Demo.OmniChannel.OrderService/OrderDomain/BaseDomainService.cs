﻿using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ShareKernel.Dto;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System.Threading.Tasks;

namespace Demo.OmniChannel.OrderService.OrderDomain
{
    public class BaseDomainService
    {
        protected IDbConnectionFactory DbConnectionFactory;

        public BaseDomainService(
            IDbConnectionFactory dbConnectionFactory)
        {
            DbConnectionFactory = dbConnectionFactory;
        }
        protected async Task<OmniPlatformSettingDto> GetPlatformSetting(int retailerId)
        {
            using (var db = await DbConnectionFactory.OpenAsync())
            {
                var omniPlatformSettingRepository = new OmniPlatformSettingRepository(db);

                var settings =
                    await omniPlatformSettingRepository.GetByRetailerAsync(retailerId);

                return settings;
            }
        }
    }
}

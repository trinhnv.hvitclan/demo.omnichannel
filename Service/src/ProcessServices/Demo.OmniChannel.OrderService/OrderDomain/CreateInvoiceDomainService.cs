﻿using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.Infrastructure.DemoGuaranteeClient;
using Demo.OmniChannel.Infrastructure.DemoGuaranteeClient.Interfaces;
using Demo.OmniChannel.OrderService.OrderDomain.Interfaces;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Common;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.OmniChannel.OrderService.OrderDomain
{
    public class CreateInvoiceDomainService : ICreateInvoiceDomainService
    {
        private readonly IGuaranteeClient _guaranteeClient;
        private readonly GuaranteeClientConfiguration _crmClientConfiguration;
        public CreateInvoiceDomainService(
            IGuaranteeClient guaranteeClient,
            IOptions<GuaranteeClientConfiguration> crmClientConfiguration)
        {
            _guaranteeClient = guaranteeClient;
            _crmClientConfiguration = crmClientConfiguration.Value;
        }

        /// <summary>
        /// Validate tự động chọn lô
        /// </summary>
        /// <param name="before"></param>
        /// <param name="after"></param>
        /// <returns></returns>
        public (bool, string, string) ValidateAutoSyncBatchExpire(List<InvoiceDetail> before,
            List<InvoiceDetail> after)
        {
            // Tính tổng sản phẩm xem đã đủ số lượng chưa
            var invalidProductLst = new List<InvoiceDetail>();
            var productGroupBeforeLst = before.GroupBy(item => item.ProductId).Select(item => new
            {
                SumQuantity = item.Sum(x => x.Quantity),
                ProductId = item.Key
            }).ToList();

            foreach (var itemProductGroupBefore in productGroupBeforeLst)
            {
                var sumAfter = after.Where(item => item.ProductId == itemProductGroupBefore.ProductId).Sum(item => item.Quantity);
                if (sumAfter != itemProductGroupBefore.SumQuantity)
                {
                    var invalidProductInfo = before.FirstOrDefault(item => item.ProductId == itemProductGroupBefore.ProductId);
                    invalidProductLst.Add(invalidProductInfo);
                }
            }

            if (invalidProductLst.Count > 0)
            {
                var productLog = new StringBuilder();
                var errorMessage = new StringBuilder();
                foreach (var item in invalidProductLst)
                {
                    productLog.AppendFormat($"-Không đủ số lượng tồn kho cho hàng hóa lô date: [ProductCode]{item.ProductCode}[/ProductCode] số lượng {item.Quantity}<br/>");
                    errorMessage.Append($" Không đủ số lượng tồn kho cho hàng hóa lô date:{item.ProductCode};");
                }
                return (false, errorMessage.ToString(), productLog.ToString());
            }
            return (true, "", "");
        }

        /// <summary>
        /// Lấy thông tin bảo hành bảo trì
        /// </summary>
        /// <param name="context"></param>
        /// <param name="productFormulaRepository"></param>
        /// <param name="productData"></param>
        /// <param name="invoiceDetails"></param>
        /// <param name="purchaseDate"></param>
        /// <returns></returns>
        public async Task<List<OmniChannelCore.Api.Sdk.Models.InvoiceWarranties>> GetInvoiceWarranties(
           KvInternalContext context, ProductFormulaRepository productFormulaRepository,
           List<Domain.Model.ProductBranchDTO> productData,
           List<MongoDb.InvoiceDetail> invoiceDetails,
           DateTime purchaseDate)
        {
            if (_crmClientConfiguration.IncludeRetailerIds.Any() && !_crmClientConfiguration.IncludeRetailerIds.Contains(context.RetailerId))
                return new List<OmniChannelCore.Api.Sdk.Models.InvoiceWarranties>();

            var productIds = invoiceDetails.Where(x => !string.IsNullOrEmpty(x.Uuid)).Select(x => x.ProductId).ToList();
            if (productIds == null || !productIds.Any()) return new List<OmniChannelCore.Api.Sdk.Models.InvoiceWarranties>();

            var productCombo = productData.Where(x => x.ProductType == (byte)ProductType.Manufactured).ToList();

            var productFormula = productCombo.Any() ? await productFormulaRepository.GetByProductIds(context.RetailerId,
                productCombo.Select(x => x.Id).ToList()) : new List<Domain.Model.ProductFormula>();

            if (productData.All(x => x.IsWarranty == null || x.IsWarranty == 0)
                && productFormula.All(x => x.IsWarranty == null || x.IsWarranty == 0))
            {
                return new List<OmniChannelCore.Api.Sdk.Models.InvoiceWarranties>();
            }

            productIds.AddRange(productFormula.Select(x => x.MaterialId));

            var warrantys = await _guaranteeClient.GetWarrantyListByProductIds(
                new ShareKernel.Auth.ExecutionContext(context.RetailerId, context.RetailerCode, 
                context.GroupId), productIds);

            if (warrantys == null || !warrantys.Any()) return new List<OmniChannelCore.Api.Sdk.Models.InvoiceWarranties>();

            return GenerateWarranties(warrantys, productFormula, invoiceDetails, purchaseDate);
        }

        /// <summary>
        /// Tách orderDetails nếu hàng hóa có nhiều serialImei
        /// </summary>
        /// <param name="productData"></param>
        /// <param name="invoiceDetails"></param>
        /// <returns></returns>
        public List<MongoDb.InvoiceDetail> SplitOrderDetails(
            List<Domain.Model.ProductBranchDTO> productData,
            List<MongoDb.InvoiceDetail> invoiceDetails)
        {
            var invoiceDetailsNew = new List<MongoDb.InvoiceDetail>();

            foreach (var detail in invoiceDetails)
            {
                var productKv = productData.FirstOrDefault(x => x.Id == detail.ProductId);
                var serialsList = string.IsNullOrEmpty(detail.SerialNumbers) ? new string[] { } :
                    detail.SerialNumbers.Split(",");
                if (!serialsList.Any() || string.IsNullOrEmpty(detail.Uuid) ||
                    productKv == null || productKv.IsLotSerialControl == false)
                {
                    invoiceDetailsNew.Add(detail);
                    continue;
                }

                for (int i = 0; i < serialsList.Length; i++)
                {
                    var orderSerial = ConvertHelper.DeepCopy(detail);
                    orderSerial.SerialNumbers = serialsList[i];
                    orderSerial.Uuid = $"{detail.Uuid}-{i}";
                    orderSerial.Quantity = Math.Round(detail.Quantity / serialsList.Length, 0);
                    invoiceDetailsNew.Add(orderSerial);
                }
            }
            return invoiceDetailsNew;
        }


        #region Private method
        private List<OmniChannelCore.Api.Sdk.Models.InvoiceWarranties> GenerateWarranties(
            List<Infrastructure.DemoGuaranteeClient.Dtos.WarrantyDto> warrantys,
            List<Domain.Model.ProductFormula> productFormula,
            List<MongoDb.InvoiceDetail> invoiceDetails,
            DateTime purchaseDate)
        {
            var invoiceWarrantys = new List<OmniChannelCore.Api.Sdk.Models.InvoiceWarranties>();
            invoiceDetails.ForEach(detail =>
            {
                var getproductFormula = productFormula.Where(pf => pf.ProductId == detail.ProductId);
                var warrantysByProduct = warrantys.Where(w => w.ProductId == detail.ProductId
                 || getproductFormula.Select(x => x.MaterialId).Contains(w.ProductId)).ToList();

                var invoiceWarranty = warrantysByProduct?.Select(x =>
                {
                    return new OmniChannelCore.Api.Sdk.Models.InvoiceWarranties
                    {
                        Description = x.Description,
                        NumberTime = x.NumberTime,
                        TimeType = x.TimeType,
                        ProductId = x.ProductId,
                        ExpireDate = SyncHelper.CalExpireWarranty(x.TimeType, x.NumberTime, purchaseDate),
                        ProductCode = getproductFormula.FirstOrDefault(p => p.MaterialId == x.ProductId)?.MaterialCode,
                        InvoiceDetailUuid = detail.Uuid,
                        WarrantyType = x.WarrantyType,
                        Status = (int)WarrantyOrderStatus.Finalized,
                        ProductQty = getproductFormula.FirstOrDefault(p => p.MaterialId == x.ProductId)?.Quantity
                    };
                }).ToList();
                invoiceWarrantys.AddRange(invoiceWarranty);
            });
            return invoiceWarrantys;
        }
        #endregion
    }
}

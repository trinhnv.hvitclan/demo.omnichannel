﻿using Demo.OmniChannel.Business;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.DCControl.ConfigureService;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Infrastructure.EventBus.Extentions;
using Demo.OmniChannel.Infrastructure.IoC;
using Demo.OmniChannel.MongoService.Common;
using Demo.OmniChannel.MongoService.Impls;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.OrderService.Impls;
using Demo.OmniChannel.OrderService.OrderDomain;
using Demo.OmniChannel.OrderService.OrderDomain.Interfaces;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.Services.Impls;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.Services.LogginConfiguration;
using Demo.OmniChannel.ShareKernel.Auth;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Impls;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.Messaging.Redis;
using ServiceStack.OrmLite;
using ServiceStack.Redis;
using System;
using System.Linq;
using System.Reflection;
using Demo.OmniChannel.Infrastructure.Extensions;
using Demo.OmniChannel.Infrastructure.DemoGuaranteeClient.Extentions;
using Demo.OmniChannel.ShareKernel.Common;

namespace Demo.OmniChannel.OrderService
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            var builder = new ConfigurationBuilder()
                .AddConfiguration(configuration)
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            #region Register ServiceStack License
            Licensing.RegisterLicense(Configuration.GetSection("servicestack:license").Value);
            #endregion
            services.AddSingleton<IAppSettings>(x => new NetCoreAppSettings(Configuration));
            #region Redis
            var redisConfig = new KvRedisConfig();
            Configuration.GetSection("redis:cache").Bind(redisConfig);
            services.AddSingleton(sc => KvRedisPoolManager.GetClientsManager(redisConfig));
            services.AddSingleton<ICacheClient, KvCacheClient>();
            services.AddSingleton<IKvLockRedis, KvLockRedis>();

            var redisMqConfig = new KvRedisConfig();
            Configuration.GetSection("redis:message").Bind(redisMqConfig);
            services.AddSingleton(redisMqConfig);
            var redisFactory = KvRedisPoolManager.GetClientsManager(redisMqConfig);
            var mqHost = new RedisMqServer(redisFactory, retryCount: 2);
            services.AddSingleton<IMessageService>(mqHost);
            #endregion
            #region Register DatabaseConnectionString
            var kvSql = new KvSqlConnectString();
            Configuration.GetSection("SqlConnectStrings").Bind(kvSql);
            var connectFactory = new OrmLiteConnectionFactory(kvSql.KvChannelEntities, SqlServer2016Dialect.Provider);
            connectFactory.RegisterConnection(nameof(kvSql.KvMasterEntities), kvSql.KvMasterEntities, SqlServer2016Dialect.Provider);
            foreach (var item in kvSql.KvEntitiesDC1)
            {
                connectFactory.RegisterConnection(item.Name.ToLower(), item.Value, SqlServer2016Dialect.Provider);
            }

            OrmLiteConfig.DialectProvider.GetStringConverter().UseUnicode = true;
            OrmLiteConfig.CommandTimeout = 200;
            services.AddSingleton(kvSql);
            services.AddSingleton<IDbConnectionFactory>(c => connectFactory);
            #endregion
            #region Register Mongo
            services.Configure<MongoDbSettings>(Configuration.GetSection("mongodb"));
            services.UsingMongoDb(Configuration);
            services.AddTransient<IProductMappingService, ProductMappingService>();
            services.AddTransient<IProductMongoService, ProductMongoService>();
            services.AddTransient<IOrderMongoService, OrderMongoService>();
            services.AddTransient<IInvoiceMongoService, InvoiceMongoService>();
            services.AddTransient<IRetryOrderMongoService, RetryOrderMongoService>();
            services.AddTransient<IOrderTrackingLogTimeMongoService, OrderTrackingLogTimeMongoService>();
            services.AddTransient<IRetryInvoiceMongoService, RetryInvoiceMongoService>();
            services.AddTransient<ISendoLocationMongoService, SendoLocationMongoService>();
            #endregion

            services.AddScoped<ICustomerInternalClient>(c => new CustomerInternalClient(c.GetService<IAppSettings>()));
            services.AddScoped<IOrderInternalClient>(c => new OrderInternalClient(c.GetService<IAppSettings>())); 
            services.AddScoped<IInvoiceInternalClient>(c => new InvoiceInternalClient(c.GetService<IAppSettings>()));
            services.AddScoped<IPartnerDeliveryInternalClient>(c => new PartnerDeliveryInternalClient(c.GetService<IAppSettings>()));
            services.AddScoped<IAuditTrailInternalClient>(c => new AuditTrailInternalClient(c.GetService<IAppSettings>()));
            services.AddScoped<IDeliveryInfoInternalClient>(c => new DeliveryInfoInternalClient(c.GetService<IAppSettings>()));
            services.AddScoped<ISurChargeInternalClient>(c => new SurChargeInternalClient(c.GetService<IAppSettings>()));
            services.AddScoped<ISurChargeBranchInternalClient>(c => new SurChargeBranchInternalClient(c.GetService<IAppSettings>()));
            services.AddScoped<IDeliveryPackageInternalClient>(c => new DeliveryPackageInternalClient(c.GetService<IAppSettings>()));
            services.AddScoped<IScheduleService>(c => new ScheduleService.Impls.ScheduleService(c.GetRequiredService<IRedisClientsManager>(), c.GetService<IAppSettings>()));
            #region Register Channel Third Party
            services.AddSingleton(c => new ChannelClient.Impls.ChannelClient());
            #endregion

            services.AddScoped<IOmniChannelAuthService, OmniChannelAuthService>();
            services.AddScoped<IChannelBusiness, ChannelBusiness>();
            services.AddScoped<IOmniChannelSettingService, OmniChannelSettingService>();
            services.AddScoped<IExecutionContextService, ExecutionContextService>();
            services.AddScoped<IMicrosoftLoggingContextExtension, MicrosoftLoggingContextExtension>();
            services.AddScoped<ICreateOrderDomainService, CreateOrderDomainService>();
            services.AddScoped<ICreateInvoiceDomainService, CreateInvoiceDomainService>();
            services.AddScoped<IOmniChannelPlatformService, OmniChannelPlatformService>();
            services.AddScoped<IOmniChannelWareHouseService, OmniChannelWareHouseService>();


            #region Register kafka
            var kafka = new Kafka();
            Configuration.GetSection("Kafka").Bind(kafka);
            KafkaClient.KafkaClient.Instance.SetKafkaConsumerConfig(kafka);
            KafkaClient.KafkaClient.Instance.SetKafkaProducerConfig(kafka);
            KafkaClient.KafkaClient.Instance.InitProducer();
            #endregion


            if (redisMqConfig.EventMessages.Any())
            {
                var lsName = redisMqConfig.EventMessages.Where(t => t.IsActive).Select(v => v.Name);
                var types = typeof(BaseService<>).GetTypeInfo().Assembly.DefinedTypes
                    .Where(p => p.GetTypeInfo().IsAssignableFrom(p.AsType()) && p.IsClass && lsName.Contains(p.Name)).Select(p => p.AsType());
                try
                {
                    foreach (var type in types)
                    {
                        services.AddTransient(typeof(IHostedService), type);
                    }
                }
                catch (Exception e)
                {
                    Log.Logger.Error($"Register {nameof(IHostedService)} error: {e.Message}", e);
                    throw;
                }

            }

            services.AddScoped(c =>
            {
                var eventContextSvc = c.GetRequiredService<IExecutionContextService>();

                if (eventContextSvc.Context == null)
                {
                    return new ExecutionContext();
                }

                var context = new ExecutionContext
                {
                    Id = eventContextSvc.Context.Id,
                    RetailerCode = eventContextSvc.Context.RetailerCode,
                    BranchId = eventContextSvc.Context.BranchId,
                    User = eventContextSvc.Context.User,
                    RetailerId = eventContextSvc.Context.RetailerId,
                    GroupId = eventContextSvc.Context.GroupId,
                    Group = eventContextSvc.Context.Group
                };
                return context;
            });

            services.AddConfigEventBus(Configuration);
            services.AddEventBus();
            services.AddConfigConfigGuaranteeClient(Configuration);
            services.RegisterDemoGuaranteeClientService();

            SetEncryptPassPhrase();

            services.RegisterApplicationDcControl(new ApplicationDcInputOptions
            {
                ServiceName = "OrderService",
                AppSettings = new NetCoreAppSettings(Configuration)
            });
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime lifetime, IMessageService messageService)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            lifetime.ApplicationStarted.Register(messageService.Start);
        }



        private void SetEncryptPassPhrase()
        {
            CryptoHelper.PassPhrase = Configuration.GetSection("EncryptPassPhrase").Value;

            if (string.IsNullOrWhiteSpace(CryptoHelper.PassPhrase))
            {
                throw new KvException("Setting EncryptPassPhrase invalid. Check setting file, please.");
            }
        }
    }
}
﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Exceptions;
using ServiceStack.Logging.Serilog;
using ServiceStack.Logging;
using System;
using System.IO;
using ILogger = Serilog.ILogger;

namespace Demo.OmniChannel.OrderService
{
    class Program
    {
        public static readonly string Namespace = typeof(Program).Namespace;
        public static readonly string AppName =
            Namespace.Substring(Namespace.LastIndexOf('.', Namespace.LastIndexOf('.') - 1) + 1);

        public static void Main(string[] args)
        {
            var configuration = GetConfiguration();
            Log.Logger = CreateSerilogLogger(configuration);
            LogManager.LogFactory = new SerilogFactory();
            var host = CreateWebHostBuilder(configuration, args);
            try
            {
                Log.Information($"OrderService task starting console...{TimeZoneInfo.Local.DisplayName} and Datetimenow = {DateTime.Now}");
                host.Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "OrderService corrupted", AppName);
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }
        private static IConfiguration GetConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            return builder.Build();
        }

        private static ILogger CreateSerilogLogger(IConfiguration configuration)
        {
            var log = new LoggerConfiguration()//NOSONAR
                .Enrich.FromLogContext()
                .Enrich.WithProperty("ApplicationContext", AppName)
                .Enrich.WithExceptionDetails()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
            return log;
        }

        public static IWebHostBuilder CreateWebHostBuilder(IConfiguration configuration, string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(x => x.AddConfiguration(configuration))
                .UseSerilog()
                .UseUrls(Environment.GetEnvironmentVariable("ASPNETCORE_URLS") ?? "http://localhost:5004/")
                .UseStartup<Startup>();
    }
}

﻿using Demo.OmniChannel.MongoService.Impls;
using Demo.OmniChannel.MongoService.Interface;

namespace Demo.OmniChannel.OrderServiceV2.Extensions
{
    public static class AddMongoRepositoryExtension
    {
        public static void AddMongoRepository(this IServiceCollection services)
        {
            services.AddTransient<IProductMappingService, ProductMappingService>();
            services.AddTransient<IProductMongoService, ProductMongoService>();
            services.AddTransient<IOrderMongoService, OrderMongoService>();
            services.AddTransient<IInvoiceMongoService, InvoiceMongoService>();
            services.AddTransient<IRetryOrderMongoService, RetryOrderMongoService>();
            services.AddTransient<IOrderTrackingLogTimeMongoService, OrderTrackingLogTimeMongoService>();
            services.AddTransient<IRetryInvoiceMongoService, RetryInvoiceMongoService>();
            services.AddTransient<ISendoLocationMongoService, SendoLocationMongoService>();
        }
    }
}

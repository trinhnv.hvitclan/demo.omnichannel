﻿using Demo.OmniChannel.Business;
using Demo.OmniChannel.Business.Implements;
using Demo.OmniChannel.Business.Interfaces;

namespace Demo.OmniChannel.OrderServiceV2.Extensions
{
    public static class AddBusinessExtension
    {
        public static void AddBusiness(this IServiceCollection services)
        {
            services.AddTransient<IShopeeBusinessInterface, ShopeeBusinessServices>();
            services.AddTransient<ITiktokBusinessInterface, TiktokBusinessServices>();
            services.AddTransient<IAutoCreateProductBusiness, AutoCreateProductBusinessService>();
            services.AddTransient<IChannelBusiness, Business.ChannelBusiness>();
            services.AddTransient<IShopeeBusinessInvoiceInterface, ShopeeBusinessInvoiceService>();
            services.AddTransient<ITiktokBusinessInvoiceInterface, TiktokBusinessInvoiceService>();
            services.AddTransient<ITiktokBusinessSyncInvoicerError, TiktokBusinessSyncInvoicerError>();
            services.AddTransient<IShopeeBusinessSyncInvoicerError, ShopeeBusinessSyncInvoicerError>();
        }

    }
}

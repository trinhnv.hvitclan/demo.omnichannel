﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using ServiceStack.Configuration;
using Common = Demo.OmniChannel.ShareKernel.Common;


namespace Demo.OmniChannel.OrderServiceV2.BackgroundWorker.SyncErrorBusinessOrderHandles
{
    public class TiktokSyncErrorBusinessOrderWorker : SyncErrorBusinessOrderWorker<TiktokSyncErrorCreateOrder>
    {
        private readonly ITiktokBusinessInterface _tiktokBusiness;
        public TiktokSyncErrorBusinessOrderWorker(IAppSettings settings, ITiktokBusinessInterface tiktokBusiness) : base(settings)
        {
            ChannelType = (byte)Demo.OmniChannel.ShareKernel.Common.ChannelType.Tiktok;
            TopicName = $"{Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)Common.KafkaTopicType.SyncErrorOrder && x.ChannelType == ChannelType)?.Name}";
            ConsumerSize = Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)Common.KafkaTopicType.SyncErrorOrder && x.ChannelType == ChannelType)?.Size ?? 1;
            _tiktokBusiness = tiktokBusiness;
        }

        protected override async Task<bool> ProcessMessage(TiktokSyncErrorCreateOrder data, bool isWriteLog = false)
        {
            return await _tiktokBusiness.SyncErrorOrderServiceV2(data.LogId, data.RetailerId, data.BranchId, data.ChannelId, data.OrderId);
        }
    }
}

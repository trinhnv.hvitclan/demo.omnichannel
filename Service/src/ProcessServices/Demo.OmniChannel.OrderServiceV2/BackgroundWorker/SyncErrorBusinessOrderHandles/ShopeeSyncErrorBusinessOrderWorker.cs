﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using ServiceStack.Configuration;
using Common = Demo.OmniChannel.ShareKernel.Common;


namespace Demo.OmniChannel.OrderServiceV2.BackgroundWorker.SyncErrorBusinessOrderHandles
{
    public class ShopeeSyncErrorBusinessOrderWorker : SyncErrorBusinessOrderWorker<ShopeeSyncErrorCreateOrder>
    {
        private readonly IShopeeBusinessInterface _shopeeBusinessInterface;
        public ShopeeSyncErrorBusinessOrderWorker(IAppSettings settings, IShopeeBusinessInterface shopeeBusinessInterface) : base(settings)
        {
            ChannelType = (byte)Demo.OmniChannel.ShareKernel.Common.ChannelType.Shopee;
            TopicName = $"{Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)Common.KafkaTopicType.SyncErrorOrder && x.ChannelType == ChannelType)?.Name}";
            ConsumerSize = Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)Common.KafkaTopicType.SyncErrorOrder && x.ChannelType == ChannelType)?.Size ?? 1;
            _shopeeBusinessInterface = shopeeBusinessInterface;
        }

        protected override async Task<bool> ProcessMessage(ShopeeSyncErrorCreateOrder data, bool isWriteLog = false)
        {
            return await _shopeeBusinessInterface.SyncErrorOrderServiceV2(data.LogId, data.RetailerId, data.BranchId, data.ChannelId, data.OrderSn);
        }
    }
}

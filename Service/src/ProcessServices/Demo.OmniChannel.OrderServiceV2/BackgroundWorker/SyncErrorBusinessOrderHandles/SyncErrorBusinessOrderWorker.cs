﻿using Demo.OmniChannel.Kafka;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.OrderServiceV2.BackgroundWorker.SyncErrorBusinessOrderHandles
{
    public abstract class SyncErrorBusinessOrderWorker<T> : KafkaBaseService<T> where T : SyncErrorCreateOrder
    {
        protected int ChannelType { get; set; }
        protected SyncErrorBusinessOrderWorker(IAppSettings settings) : base(settings)
        {
        }
    }
}

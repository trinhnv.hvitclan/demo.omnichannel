﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.OrderServiceV2.BackgroundWorker.Shopee;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.RedisStreamMessage;
using Newtonsoft.Json;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.OrderServiceV2.BackgroundWorker.DeadLetterQueueHandles
{
    public class TiktokDLQOrderBackgroundWorker : DlqOrderBackgroundWorker<TiktokDeadLetterQueueCreateOrder>
    {
        private readonly ITiktokBusinessInterface _tiktokBusiness;
        public TiktokDLQOrderBackgroundWorker(IAppSettings settings,
            ITiktokBusinessInterface tiktokBusiness) : base(settings)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Tiktok;
            TopicName = $"{Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.DLQCreateOrder && x.ChannelType == ChannelType)?.Name}";
            ConsumerSize = Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.DLQCreateOrder && x.ChannelType == ChannelType)?.Size ?? 1;
            _tiktokBusiness = tiktokBusiness;
        }

        protected override async Task<bool> ProcessMessage(TiktokDeadLetterQueueCreateOrder data, bool isWriteLog = false)
        {
            if (data == null) return false;
            return await _tiktokBusiness.CreateOrderServiceV2(JsonConvert.SerializeObject(data));
        }
    }
}

﻿using Demo.OmniChannel.Kafka;
using Demo.OmniChannel.ShareKernel.RedisStreamMessage;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.OrderServiceV2.BackgroundWorker.Shopee
{
    public abstract class DlqOrderBackgroundWorker<T> : KafkaBaseService<T> where T : DeadLetterQueueCreateOrder
    {
        protected int ChannelType { get; set; }

        protected DlqOrderBackgroundWorker(IAppSettings settings) : base(settings) { }
    }
}

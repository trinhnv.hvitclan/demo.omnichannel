﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.OrderServiceV2.BackgroundWorker.Shopee;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.RedisStreamMessage;
using Newtonsoft.Json;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.OrderServiceV2.BackgroundWorker.DeadLetterQueueHandles.Shopee
{
    public class ShopeeDlqOrderBackgroundWorker : DlqOrderBackgroundWorker<ShopeeDeadLetterQueueCreateOrderV2>
    {
        private readonly IShopeeBusinessInterface _shopeeBusinessInterface;

        public ShopeeDlqOrderBackgroundWorker(IAppSettings settings, IShopeeBusinessInterface shopeeBusinessInterface) : base(settings)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Shopee;
            TopicName = $"{Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.DLQCreateOrder && x.ChannelType == ChannelType)?.Name}";
            ConsumerSize = Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.DLQCreateOrder && x.ChannelType == ChannelType)?.Size ?? 1;
            _shopeeBusinessInterface = shopeeBusinessInterface;
        }

        protected override async Task<bool> ProcessMessage(ShopeeDeadLetterQueueCreateOrderV2 data, bool isWriteLog = false)
        {
            if (data != null)
            {
                var message = JsonConvert.SerializeObject(data);
                return await _shopeeBusinessInterface.CreateOrderServiceV2(message);
            }
            return true;
        }

        protected override async Task<bool> ProcessMessageWithoutDeserialize(string data, bool isWriteLog = false)
        {
            if(data != null)
            {
                return  await _shopeeBusinessInterface.CreateOrderServiceV2(data);
            }
            return true;
        }

    }
}

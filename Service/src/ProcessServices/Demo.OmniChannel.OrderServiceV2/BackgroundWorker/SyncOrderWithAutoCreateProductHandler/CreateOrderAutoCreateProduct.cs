﻿using Demo.OmniChannel.Kafka;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using ServiceStack.Caching;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.OrderServiceV2.BackgroundWorker.SyncOrderWithAutoCreateProductHandler
{
    public abstract class CreateOrderAutoCreateProduct<T> : KafkaBaseService<T> where T : SyncOrderWithAutoCreateProduct
    {
        protected readonly IKvLockRedis KvLockRedis;
        protected int ChannelType { get; set; }

        protected readonly ICacheClient CacheClient;
        protected CreateOrderAutoCreateProduct(
            IAppSettings settings,
            IKvLockRedis kvLockRedis,
            ICacheClient cacheClient) : base(settings)
        {
            KvLockRedis = kvLockRedis;
            CacheClient = cacheClient;
        }
    }
}

﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Newtonsoft.Json;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using Common = Demo.OmniChannel.ShareKernel.Common;


namespace Demo.OmniChannel.OrderServiceV2.BackgroundWorker.SyncOrderWithAutoCreateProductHandler
{
    public class ShopeeCreateOrderAutoCreateProduct : CreateOrderAutoCreateProduct<SyncOrderWithAutoCreateProduct>
    {
        private readonly IAutoCreateProductBusiness _autoCreateProductBusiness;
        private readonly IShopeeBusinessInterface _shopeeBusinessInterface;
        public ShopeeCreateOrderAutoCreateProduct(IAppSettings settings,
            IAutoCreateProductBusiness autoCreateProductBusiness,
            IShopeeBusinessInterface shopeeBusinessInterface,
            IKvLockRedis kvLockRedis,
            ICacheClient cacheClient) : base(settings, kvLockRedis, cacheClient)
        {
            ChannelType = (byte)Common.ChannelType.Shopee;
            TopicName = $"{Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.CreateOrderWithAutoCreateProduct && x.ChannelType == ChannelType)?.Name}";
            ConsumerSize = Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.CreateOrderWithAutoCreateProduct && x.ChannelType == ChannelType)?.Size ?? 1;
            _autoCreateProductBusiness = autoCreateProductBusiness;
            _shopeeBusinessInterface = shopeeBusinessInterface;
        }

        protected override async Task<bool> ProcessMessage(SyncOrderWithAutoCreateProduct data, bool isWriteLog = false)
        {
            var log = new SeriLogObject(data.LogId)
            {
                Action = "SyncOrderWithAutoCreateProduct",
                RetailerId = data.RetailerId,
                OmniChannelId = data.ChannelId,
                RequestObject = data.KvOrder
            };
            try
            {
                await CheckAndWaitingHandleDone(data);
                var response = await _autoCreateProductBusiness.CreateProductToKv(data);
                if (response.IsSuccess)
                {
                    await _shopeeBusinessInterface.RetryOrderServiceV2(data.LogId, 
                        data.RetailerId, data.BranchId, (int)data.ChannelId, data.KvOrder);
                }
                log.Description = $"Auto create product {(response.IsSuccess ? "successfull" : "failed")}," +
                    $" kvProductIds: {response.KvProductIds.ToSafeJson()}";
                log.LogInfo();
            }
            catch (Exception ex)
            {
                var o = ex as OmniException;
                if (o != null && o.ErrorCode.Equals(ErrorCode.ErrorBussiness))
                {
                    log.Description = ex.Message;
                    log.LogWarning();
                }
                else
                {
                    log.LogError(ex);
                }
            }
            finally
            {
                ReleaseLock(data);
            }
            return false;
        }

        #region

        /// <summary>
        /// Kiểm tra xem có đang lock parrentId không?. Nếu lock chờ xử lý
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private async Task CheckAndWaitingHandleDone(SyncOrderWithAutoCreateProduct data)
        {
            var retryCount = 0;
            var kvOrderObject = JsonConvert.DeserializeObject<KvOrder>(data.KvOrder);
            if (kvOrderObject?.OrderDetails == null || !kvOrderObject.OrderDetails.Any()) return;
            var kvCacheClient = (IKvCacheClient)CacheClient;
            var lockParrentWithChannelId = string.Format(KvConstant.LockAutoCreateProductByChannel, data.ChannelId);
            var parentId = kvOrderObject.OrderDetails.GroupBy(x => x.ParentChannelProductId).Select(x => x.Key).Distinct().ToList();

            var isWaiting = IsWaiting(parentId);

            while (isWaiting && retryCount <= 3)
            {
                await Task.Delay(2000);
                retryCount++;
            }
            kvCacheClient.InitSet(lockParrentWithChannelId, parentId);
            kvCacheClient.ExpireEntryAt(lockParrentWithChannelId, DateTime.Now.AddMinutes(1));

            bool IsWaiting(List<string> parentId)
            {
                foreach (var item in parentId)
                {
                    var isLock = kvCacheClient.SetContainsItem(lockParrentWithChannelId, item);
                    if (isLock) return true;
                }
                return false;
            }
        }

        private void ReleaseLock(SyncOrderWithAutoCreateProduct data)
        {
            var kvOrderObject = JsonConvert.DeserializeObject<KvOrder>(data.KvOrder);
            if (kvOrderObject?.OrderDetails == null || !kvOrderObject.OrderDetails.Any()) return;
            var kvCacheClient = (IKvCacheClient)CacheClient;
            var parentId = kvOrderObject.OrderDetails.GroupBy(x => x.ParentChannelProductId).Select(x => x.Key).Distinct().ToList();
            var lockParrentWithChannelId = string.Format(KvConstant.LockAutoCreateProductByChannel, data.ChannelId);

            foreach (var item in parentId)
            {
                kvCacheClient.RemoveItemFromSet(lockParrentWithChannelId, item);
            }
        }
        #endregion
    }
}

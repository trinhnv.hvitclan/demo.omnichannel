﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.OrderServiceV2.BackgroundWorker.RetryCreateOrderHandles.Shopee
{
    public class ShopeeRetryCreateOrderBackgroundWorker : RetryCreateOrderBackgroundWorker<ShopeeRetryCreateOrder>
    {

        private readonly IShopeeBusinessInterface _shopeeBusinessInterface;
        public ShopeeRetryCreateOrderBackgroundWorker(IAppSettings settings, IShopeeBusinessInterface shopeeBusinessInterface) : base(settings)
        {
            ChannelType = (byte)Demo.OmniChannel.ShareKernel.Common.ChannelType.Shopee;
            TopicName = $"{Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.RetryCreateOrder && x.ChannelType == ChannelType)?.Name}";
            ConsumerSize = Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.RetryCreateOrder && x.ChannelType == ChannelType)?.Size ?? 1;
            _shopeeBusinessInterface = shopeeBusinessInterface;
        }
        protected override async Task<bool> ProcessMessage(ShopeeRetryCreateOrder data, bool isWriteLog = false)
        {
            return await _shopeeBusinessInterface.RetryOrderServiceV2(data.LogId, data.RetailerId, data.BranchId, data.ChannelId, data.KvOrder);
        }
    }
}

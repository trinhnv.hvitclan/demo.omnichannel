﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.OrderServiceV2.BackgroundWorker.RetryCreateOrderHandles.Tiktok
{
    public class TiktokRetryCreateOrderBackgroundWorker : RetryCreateOrderBackgroundWorker<TiktokRetryCreateOrder>
    {

        private readonly ITiktokBusinessInterface _tiktokBusinessInterface;
        public TiktokRetryCreateOrderBackgroundWorker(IAppSettings settings, ITiktokBusinessInterface tiktokBusinessInterface) : base(settings)
        {
            ChannelType = (byte)Demo.OmniChannel.ShareKernel.Common.ChannelType.Tiktok;
            TopicName = $"{Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.RetryCreateOrder && x.ChannelType == ChannelType)?.Name}";
            ConsumerSize = Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.RetryCreateOrder && x.ChannelType == ChannelType)?.Size ?? 1;
            _tiktokBusinessInterface = tiktokBusinessInterface;
        }
        protected override async Task<bool> ProcessMessage(TiktokRetryCreateOrder data, bool isWriteLog = false)
        {
            return await _tiktokBusinessInterface.RetryOrderServiceV2(data.LogId, data.RetailerId, data.BranchId, data.ChannelId, data.KvOrder);
        }
    }
}

﻿using Demo.OmniChannel.Kafka;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.OrderServiceV2.BackgroundWorker.RetryCreateOrderHandles
{
    public abstract class RetryCreateOrderBackgroundWorker<T> : KafkaBaseService<T> where T : RetryCreateOrder
    {
        protected int ChannelType { get; set; }
        protected RetryCreateOrderBackgroundWorker(IAppSettings settings) : base(settings) { }
    }
}

﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.OrderServiceV2.BackgroundWorker.Shopee;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.RedisStreamMessage;
using Newtonsoft.Json;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.OrderServiceV2.BackgroundWorker.DeadLetterQueueInvoiceHandles
{
    public class TiktokDLQInvoiceBackgroundWorker : DLQInvoiceBackgroundWorker<DeadLetterQueueCreateInvoiceMessage>
    {
        private readonly ITiktokBusinessInvoiceInterface _tiktokBusinessInvoiceInterface;

        public TiktokDLQInvoiceBackgroundWorker(
            IAppSettings settings,
            ITiktokBusinessInvoiceInterface tiktokBusinessInvoiceInterface) : base(settings)
        {
            ChannelType = (byte)ShareKernel.Common.ChannelType.Tiktok;
            TopicName = $"{Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.DLQCreateInvoice && x.ChannelType == ChannelType)?.Name}";
            ConsumerSize = Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.DLQCreateInvoice && x.ChannelType == ChannelType)?.Size ?? 1;
            _tiktokBusinessInvoiceInterface = tiktokBusinessInvoiceInterface;
        }

        protected override async Task<bool> ProcessMessage(DeadLetterQueueCreateInvoiceMessage data, bool isWriteLog = false)
        {
            var message = JsonConvert.DeserializeObject<CreateInvoiceMessage>(JsonConvert.SerializeObject(data));
            return await _tiktokBusinessInvoiceInterface.HandleCreateInvoice(message);
        }
    }
}

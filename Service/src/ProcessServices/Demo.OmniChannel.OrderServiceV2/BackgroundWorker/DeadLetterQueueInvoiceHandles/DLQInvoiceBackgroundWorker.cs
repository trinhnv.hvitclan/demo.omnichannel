﻿using Demo.OmniChannel.Kafka;
using Demo.OmniChannel.ShareKernel.RedisStreamMessage;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.OrderServiceV2.BackgroundWorker.Shopee
{
    public abstract class DLQInvoiceBackgroundWorker<T> : KafkaBaseService<T> where T : DeadLetterQueueCreateInvoiceMessage
    {
        protected int ChannelType { get; set; }

        protected DLQInvoiceBackgroundWorker(IAppSettings settings) : base(settings) { }
    }
}

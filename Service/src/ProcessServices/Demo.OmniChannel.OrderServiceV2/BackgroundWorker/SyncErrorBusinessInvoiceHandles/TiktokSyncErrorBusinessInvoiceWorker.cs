﻿using Demo.OmniChannel.Business;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using ServiceStack.Configuration;
using Common = Demo.OmniChannel.ShareKernel.Common;

namespace Demo.OmniChannel.OrderServiceV2.BackgroundWorker.SyncErrorBusinessInvoiceHandles
{
    public class TiktokSyncErrorBusinessInvoiceWorker : SyncErrorBusinessInvoiceWorker<TiktokSyncErrorCreateInvoice>
    {
        private readonly ITiktokBusinessSyncInvoicerError _tiktokBusinessSyncInvoicerError;
        public TiktokSyncErrorBusinessInvoiceWorker(IAppSettings settings, ITiktokBusinessSyncInvoicerError TiktokBusinessSyncInvoicerError) : base(settings)
        {
            ChannelType = (byte)Common.ChannelType.Tiktok;
            TopicName = $"{Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)Common.KafkaTopicType.SyncErrorInvoice && x.ChannelType == ChannelType)?.Name}";
            ConsumerSize = Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)Common.KafkaTopicType.SyncErrorInvoice && x.ChannelType == ChannelType)?.Size ?? 1;
            _tiktokBusinessSyncInvoicerError = TiktokBusinessSyncInvoicerError;
        }

        protected override async Task<bool> ProcessMessage(TiktokSyncErrorCreateInvoice data, bool isWriteLog = false)
        {
            return await _tiktokBusinessSyncInvoicerError.ProcessSyncInvoiceError(data);

        }
    }
}

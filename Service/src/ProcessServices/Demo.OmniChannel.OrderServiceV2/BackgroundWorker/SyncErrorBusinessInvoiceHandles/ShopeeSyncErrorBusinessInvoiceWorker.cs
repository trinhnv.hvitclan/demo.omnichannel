﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using ServiceStack.Configuration;
using Common = Demo.OmniChannel.ShareKernel.Common;

namespace Demo.OmniChannel.OrderServiceV2.BackgroundWorker.SyncErrorBusinessInvoiceHandles
{
    public class ShopeeSyncErrorBusinessInvoiceWorker : SyncErrorBusinessInvoiceWorker<ShopeeSyncErrorCreateInvoice>
    {
        private readonly IShopeeBusinessSyncInvoicerError _shopeeBusinessSyncInvoicerError;
        public ShopeeSyncErrorBusinessInvoiceWorker(IAppSettings settings, IShopeeBusinessSyncInvoicerError ShopeeBusinessSyncInvoicerError) : base(settings)
        {
            ChannelType = (byte)Common.ChannelType.Shopee;
            TopicName = $"{Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)Common.KafkaTopicType.SyncErrorInvoice && x.ChannelType == ChannelType)?.Name}";
            ConsumerSize = Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)Common.KafkaTopicType.SyncErrorInvoice && x.ChannelType == ChannelType)?.Size ?? 1;
            _shopeeBusinessSyncInvoicerError = ShopeeBusinessSyncInvoicerError;
        }

        protected override async Task<bool> ProcessMessage(ShopeeSyncErrorCreateInvoice data, bool isWriteLog = false)
        {
            return await _shopeeBusinessSyncInvoicerError.ProcessSyncInvoiceError(data);

        }
    }
}

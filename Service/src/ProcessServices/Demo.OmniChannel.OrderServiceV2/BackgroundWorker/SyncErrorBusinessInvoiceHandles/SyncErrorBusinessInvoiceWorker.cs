﻿using Demo.OmniChannel.Kafka;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.OrderServiceV2.BackgroundWorker.SyncErrorBusinessInvoiceHandles
{
    public abstract class SyncErrorBusinessInvoiceWorker<T> : KafkaBaseService<T> where T : BaseSyncErrorInvoiceMessage
    {
        protected int ChannelType { get; set; }
        protected SyncErrorBusinessInvoiceWorker(IAppSettings settings) : base(settings)
        {
        }
    }
}

﻿using Demo.OmniChannel.Infrastructure.EventBus.Abstractions;
using Demo.OmniChannel.Infrastructure.EventBus.Event;
using Microsoft.Extensions.Hosting;

namespace Demo.OmniChannel.ComparingService.Service
{
    public class EventBusConsumerService : BackgroundService
    {
        private readonly IEventBus _eventBus;

        public EventBusConsumerService(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _eventBus.Subscribe<OmniSyncRecoveryPriceEvent, IIntegrationEventHandler<OmniSyncRecoveryPriceEvent>>();
            return Task.CompletedTask;
        }
    }
}

﻿using Serilog;
using Serilog.Formatting.Json;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Redis;
using Microsoft.Extensions.DependencyInjection;
using Demo.OmniChannel.MongoService.Common;
using Demo.OmniChannel.Infrastructure.IoC;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.MongoService.Impls;
using Demo.OmniChannel.Infrastructure.Common;
using Serilog.Exceptions;
using Demo.OmniChannel.DCControl.ConfigureService;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Demo.OmniChannel.Infrastructure.EventBus.Extentions;
using Demo.OmniChannel.ComparingService.Service;
using Demo.OmniChannel.ComparingService.Extensions;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.Business;
using ServiceStack.Messaging.Redis;
using ServiceStack.Messaging;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Demo.OmniChannelCore.Api.Sdk.Impls;
using Demo.OmniChannel.ShareKernel.Common;

var NameSpace = "Demo.OmniChannel.ComparingService";
var AppName = NameSpace.Substring(NameSpace.LastIndexOf('.', NameSpace.LastIndexOf('.') - 1) + 1);

var configuration = new ConfigurationBuilder()
        .SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
        .AddEnvironmentVariables()
        .Build();

#region Serilog config
SubLoggerConfiguration subLoggerConfiguration = new SubLoggerConfiguration();
configuration.GetSection("Serilog:SubLogger").Bind(subLoggerConfiguration);

Log.Logger = new LoggerConfiguration() //NOSONAR
    .Enrich.FromLogContext()
    .Enrich.WithProperty("ApplicationContext", AppName)
    .Enrich.WithExceptionDetails()
    .ReadFrom.Configuration(configuration)
    .WriteTo.Console()
    .WriteTo.Logger(l =>
        l.Filter.ByIncludingOnly(e => e.Level == subLoggerConfiguration.Error.Level)
        .WriteTo.File(new JsonFormatter(),
                        subLoggerConfiguration.Error.PathFormat,
                        rollingInterval: RollingInterval.Day,
                        rollOnFileSizeLimit: true,
                        fileSizeLimitBytes: 100000000))
    .WriteTo.Logger(l =>
        l.Filter.ByIncludingOnly(e => e.Level == subLoggerConfiguration.Information.Level)
        .WriteTo.File(new JsonFormatter(),
                        subLoggerConfiguration.Information.PathFormat,
                        rollingInterval: RollingInterval.Day,
                        rollOnFileSizeLimit: true,
                        fileSizeLimitBytes: 100000000))
    .WriteTo.Logger(l =>
        l.Filter.ByIncludingOnly(e => e.Level == subLoggerConfiguration.Warning.Level)
        .WriteTo.File(new JsonFormatter(),
                        subLoggerConfiguration.Warning.PathFormat,
                        rollingInterval: RollingInterval.Day,
                        rollOnFileSizeLimit: true,
                        fileSizeLimitBytes: 100000000))
    .CreateLogger();
#endregion 

var host = Host.CreateDefaultBuilder(args)
    .UseSerilog()
    .ConfigureAppConfiguration(x => x.AddConfiguration(configuration))
    .ConfigureServices((hostBuilderContext, services) =>
    {
        #region Register ServiceStack License
        Licensing.RegisterLicense(configuration.GetSection("servicestack:license").Value);
        #endregion

        #region StackExchange Redis

        var redisConfig = new KvRedisConfig();
        configuration.GetSection("redis:cache").Bind(redisConfig);
        services.AddSingleton(sc => KvRedisPoolManager.GetClientsManager(redisConfig));
        services.AddSingleton<ICacheClient, KvCacheClient>();
        services.AddSingleton<IKvLockRedis, KvLockRedis>();

        var redisMqConfig = new KvRedisConfig();
        configuration.GetSection("redis:message").Bind(redisMqConfig);
        var redisFactory = KvRedisPoolManager.GetClientsManager(redisMqConfig);
        var mqHost = new RedisMqServer(redisFactory, retryCount: 2);
        services.AddSingleton<IMessageService>(mqHost);
        services.AddSingleton<IMessageFactory>(c => new RedisMessageFactory(redisFactory));
        #endregion

        #region Register appsettings

        services.AddSingleton<IAppSettings>(x => new NetCoreAppSettings(configuration));

        #endregion

        #region Register DatabaseConnectionString
        var kvSql = new KvSqlConnectString();
        configuration.GetSection("SqlConnectStrings").Bind(kvSql);
        var connectFactory = new OrmLiteConnectionFactory(kvSql.KvChannelEntities, SqlServer2016Dialect.Provider);
        connectFactory.RegisterConnection(nameof(kvSql.KvMasterEntities), kvSql.KvMasterEntities, SqlServer2016Dialect.Provider);
        foreach (var item in kvSql.KvEntitiesDC1)
        {
            connectFactory.RegisterConnection(item.Name.ToLower(), item.Value, SqlServer2016Dialect.Provider);
        }

        OrmLiteConfig.DialectProvider.GetStringConverter().UseUnicode = true;
        services.AddSingleton(kvSql);
        services.AddSingleton<IDbConnectionFactory>(c => connectFactory);
        #endregion

        #region Register Mongo
        services.Configure<MongoDbSettings>(configuration.GetSection("mongodb"));
        services.UsingMongoDb(configuration);
        services.AddTransient<IProductMappingService, ProductMappingService>();
        services.AddTransient<IProductMongoService, ProductMongoService>();
        services.AddTransient<IProductComparingHistoryService, ProductComparingHistoryService>();
        #endregion

        #region Register DcControl
        services.RegisterApplicationDcControl(new ApplicationDcInputOptions
        {
            ServiceName = "ComparingService",
            AppSettings = new NetCoreAppSettings(configuration)
        });
        #endregion

        #region Register Business
        services.AddScoped<IChannelBusiness, ChannelBusiness>();
        services.AddScoped<IPriceBusiness, PriceBusiness>();
        #endregion

        #region Register RabbitMQ
        services.AddConfigEventBus(configuration);
        services.AddEventBus();
        services.AddHostedService<EventBusConsumerService>();
        services.AddEventHandlers();
        services.AddProcess();
        #endregion

        #region Auditrail
        services.AddScoped<IAuditTrailInternalClient>(c => new AuditTrailInternalClient(c.GetService<IAppSettings>()));
        #endregion

    })
    .Build();

try
{
    Log.Information($"Starting {AppName}");
    await host.RunAsync();
}
catch (Exception ex)
{
    Log.Error(ex, "Host terminated unexpectedly");
    throw;
}
finally
{
    Log.CloseAndFlush();
}



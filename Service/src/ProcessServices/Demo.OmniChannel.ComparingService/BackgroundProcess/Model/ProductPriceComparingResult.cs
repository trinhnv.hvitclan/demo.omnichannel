﻿
namespace Demo.OmniChannel.ComparingService.BackgroundProcess.Model
{
    public class BaseComparingResult
    {
        public long ChannelId { get; set; }
        public int RetailerId { get; set; }
        public bool IsIgnoreAuditTrail { get; set; }
        public int BranchId { get; set; }
        public Guid LogId { get; set; }
        public long PlatformId { get; set; }
    }
    public class ProductPriceComparingResult : BaseComparingResult
    {
        public ProductPriceComparingResult(
            long channelId,
            int retailerId,
            string itemId,
            string itemSku,
            byte itemType,
            string parrentId,
            long kvProductId,
            byte[] revision,
            decimal? priceKv,
            decimal? price,
            string errorMessage,
            byte type,
            decimal? salePriceKv,
            decimal? specialPrice,
            DateTime? startSaleDate,
            DateTime? endSaleDate,
            DateTime? specialFromTime,
            DateTime? specialToTime
            )
        {
            ChannelId = channelId;
            RetailerId = retailerId;
            ItemId = itemId;
            ItemSku = itemSku;
            ItemType = itemType;
            ParrentId = parrentId;
            PriceKv = priceKv;
            KvProductId = kvProductId;
            Revision = revision;
            Price = price;
            ErrorMessage = errorMessage;
            Type = type;
            SalePriceKv = salePriceKv;
            SpecialPrice = specialPrice;
            StartSaleDateKv = startSaleDate;
            EndSaleDateKv = endSaleDate;
            SpecialFromTime = specialFromTime;
            SpecialToTime = specialToTime;
        }
        public string ItemId { get; set; }

        public string ItemSku { get; set; }
        public byte ItemType { get; set; }
        public string ParrentId { get; set; }
        /// <summary>
        /// Giá kv
        /// </summary>
        public decimal? PriceKv { get; set; }

        /// <summary>
        /// Giá khuyến mại Kv
        /// </summary>
        public decimal? SalePriceKv { get; set; }

        /// <summary>
        /// Id sản phẩm Kv
        /// </summary>
        public long KvProductId { get; set; }

        /// <summary>
        /// Ngày bắt đầu khuyến mại
        /// </summary>
        public DateTime? StartSaleDateKv { get; set; }

        /// <summary>
        /// Ngày kết thúc khuyến mại
        /// </summary>
        public DateTime? EndSaleDateKv { get; set; }

        /// <summary>
        /// Ngày bắt đầu khuyến mại
        /// </summary>
        public DateTime? SpecialFromTime { get; set; }

        /// <summary>
        /// Ngày kết thúc khuyến mại
        /// </summary>
        public DateTime? SpecialToTime { get; set; }

        /// <summary>
        /// Revision
        /// </summary>
        public byte[] Revision { get; set; }

        /// <summary>
        /// Giá trên sàn
        /// </summary>
        public decimal? Price { get; set; }

        /// <summary>
        /// Giá khuyến mại trên sàn
        /// </summary>
        public decimal? SpecialPrice { get; set; }

        /// <summary>
        /// Message lỗi
        /// </summary>
        public string ErrorMessage { get; set; }


        /// <summary>
        /// Giá hoặc tồn
        /// </summary>
        public byte Type { get; set; }
    }
}

﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ComparingService.BackgroundProcess.Model;
using Demo.OmniChannel.Domain.Common;
using Demo.OmniChannel.Infrastructure.EventBus.Event;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using ServiceStack;
using ServiceStack.Data;
using ServiceStack.Redis;

namespace Demo.OmniChannel.ComparingService.BackgroundProcess.Type
{
    public class ProductPriceProcess : BaseProcess
    {
        private readonly IChannelBusiness _channelBusiness;
        private readonly IPriceBusiness _priceBusiness;
        private readonly IIntegrationEventService _integrationEventService;
        private readonly IProductComparingHistoryService _productComparingHistoryService;

        public ProductPriceProcess(
            IRedisClientsManager redisClientsManager,
            IDbConnectionFactory dbConnectionFactory,
            IChannelBusiness channelBusiness,
            IPriceBusiness priceBusiness,
            IIntegrationEventService integrationEventService,
            IProductComparingHistoryService productComparingHistoryService) :
            base(redisClientsManager, dbConnectionFactory)
        {
            _channelBusiness = channelBusiness;
            _priceBusiness = priceBusiness;
            _integrationEventService = integrationEventService;
            _productComparingHistoryService = productComparingHistoryService;
        }

        /// <summary>
        /// Handle comparing price
        /// </summary>
        /// <param name="event"></param>
        /// <returns></returns>
        public async Task HandleComparingPrice(OmniSyncRecoveryPriceEvent @event)
        {
            var log = new SeriLogObject(@event.Id)
            {
                Action = "HandleComparingPrice",
                RetailerId = @event.RetailerId,
                RequestObject = @event.Products,
            };

            var context = await GetKvInternalContextAsync(@event.RetailerId);
            if (context == null)
            {
                log.Description = "context is null";
                log.LogWarning();
                return;
            }

            // Get channel
            var channel = await _channelBusiness.GetChannel(@event.ChannelId,
                @event.RetailerId, @event.Id);

            // Get product with price kv
            var productsKV = await _priceBusiness.GetProductPricesAsync(
                context.Group.ConnectionString, channel, @event.Products);

            // Compare price kv with price shop
            var productsFailed = Comparing(productsKV, @event.Products, channel);

            if (!productsFailed.Any())
            {
                log.Description = "No find product failed";
                log.LogWarning();
                return;
            }

            // Save mongo
            await _productComparingHistoryService.BatchAddAsync(
                productsFailed.Select(x => x.ConvertTo<ProductComparingHistory>()).ToList());

            // Push message to rabbit MQ
            var response = PushMessageToRabbit(productsFailed, channel, context);

            log.Description = "Push message successfull";
            log.ResponseObject = response.Body.MultiProducts.Select(x => new { x.ItemId, x.Price, x.SalePrice });
            log.LogInfo();
        }

        #region Private method
        /// <summary>
        /// Comparing price
        /// </summary>
        /// <param name="productsKV"> product kv</param>
        /// <param name="products"> product shopee, tikok, lazada, tiki...</param>
        /// <returns></returns>
        private static List<ProductPriceComparingResult> Comparing(List<MultiProductItem> productsKV,
            List<ProductPrice> products, Domain.Model.OmniChannel channel)
        {
            var productsFailed = new List<ProductPriceComparingResult>();

            foreach (var product in products)
            {
                var productKv = productsKV.FirstOrDefault(x => x.ItemId == product.ItemId);
                if (productKv == null) continue;
                if (productKv.Price == null) continue;
                var isErrorPrice = ComparePrice(product.Price, productKv.Price);
                var isErrorSalePrice = CompareSpecialPrice(channel, product.SpecialPrice, productKv.SalePrice);
                (var isErrorStartDate, var isErrorEndDate) = CompareStartDateEndDate(productKv.StartSaleDate,
                    product.SpecialFromTime, product.SpecialToTime, productKv.EndSaleDate);

                if (isErrorPrice || isErrorSalePrice || isErrorStartDate || isErrorEndDate)
                {
                    productsFailed.Add(new ProductPriceComparingResult(channel.Id, channel.RetailerId,
                      product.ItemId, productKv.ItemSku, productKv.ItemType, product.ParrentId, productKv.KvProductId,
                      productKv.Revision, productKv.Price, product.Price,
                      $"Error price {isErrorPrice}, Error specialPrice {isErrorSalePrice}, Error start date {isErrorStartDate}," +
                      $" Error end date {isErrorEndDate}", (byte)ScheduleType.SyncPrice, productKv.SalePrice, product.SpecialPrice,
                      productKv.StartSaleDate, productKv.EndSaleDate, product.SpecialFromTime, product.SpecialToTime));
                }
            }
            return productsFailed;
        }

        /// <summary>
        /// Push message to rabbit mq
        /// </summary>
        /// <param name="productsFailed"></param>
        private SyncPriceEvent PushMessageToRabbit(List<ProductPriceComparingResult> productsFailed,
            Domain.Model.OmniChannel channel, KvInternalContext context)
        {
            var eventData = new SyncPriceEvent
            {
                Body = new SyncPriceBody
                {
                    ChannelId = channel.Id,
                    ChannelType = channel.Type,
                    KvEntities = context.Group.ConnectionString,
                    BranchId = channel.BranchId,
                    Source = SourceQueue.ComparingResync,
                    RetailerId = channel.RetailerId,
                    MultiProducts = productsFailed.Select(p => new SyncItemPrice
                    {
                        ItemId = p.ItemId,
                        ItemSku = p.ItemSku,
                        KvProductId = p.KvProductId,
                        ParentItemId = p.ParrentId,
                        Price = p.PriceKv,
                        ItemType = p.ItemType,
                        SalePrice = p.SalePriceKv,
                        StartSaleDate = p.StartSaleDateKv,
                        EndSaleDate = p.EndSaleDateKv,
                        Revision = p.Revision
                    }).ToList(),
                }
            };
            _integrationEventService.AddEventWithRoutingKeyAsync(eventData,
                string.Format(RoutingKey.SyncPrice,
                EnumHelper.GetNameByType<ChannelType>(channel.Type).ToLower()));

            return eventData;
        }

        /// <summary>
        /// Compare startDate and EndDate
        /// </summary>
        /// <param name="startSaleDate"></param>
        /// <param name="specialFromTime"></param>
        /// <param name="specialToTime"></param>
        /// <param name="endSaleDate"></param>
        /// <returns></returns>
        private static (bool isErrorStartDate, bool isErrorEndDate) CompareStartDateEndDate(
            DateTime? startSaleDate, DateTime? specialFromTime, DateTime? specialToTime, DateTime? endSaleDate)
        {
            var isErrorStartDate = false;
            var isErrorEndDate = false;

            //Bỏ qua startSaleDate == endSaleDate 

            if (startSaleDate != null && endSaleDate != null &&
                DateHelper.AlmostEquals(startSaleDate.Value,
                    endSaleDate.Value)) return (false, false);

            if (specialFromTime != null && startSaleDate != null)
            {
                isErrorStartDate = !DateHelper.AlmostEquals(startSaleDate.Value,
                    specialFromTime.Value);
            }

            if (endSaleDate != null && specialToTime != null)
            {
                isErrorEndDate = !DateHelper.AlmostEquals(endSaleDate.Value,
                    specialToTime.Value);
            }
            return (isErrorStartDate, isErrorEndDate);
        }


        /// <summary>
        /// Compare Price
        /// </summary>
        /// <param name="price"></param>
        /// <param name="priceKv"></param>
        /// <returns>true: Error price</returns>
        private static bool ComparePrice(decimal? price,
            decimal? priceKv)
        {
            if (price == null || priceKv == null)
                return false;

            if (FloatingHelper.AlmostEquals((float)price.Value, (float)priceKv.Value, 0))
                return false;

            return true;
        }

        /// <summary>
        /// Compare SpecialPrice
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="specialPrice"></param>
        /// <param name="specialPriceKv"></param>
        /// <returns>true: Error price special</returns>
        private static bool CompareSpecialPrice(Domain.Model.OmniChannel channel, decimal? specialPrice,
            decimal? specialPriceKv)
        {
            if (channel.Type == (byte)ChannelType.Tiktok) return false;

            if (specialPrice == null || specialPriceKv == null) return false;

            if (specialPrice.Value == specialPriceKv.Value) return false;

            return true;
        }
        #endregion
    }
}

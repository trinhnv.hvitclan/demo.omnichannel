﻿using Demo.OmniChannel.ComparingService.BackgroundProcess.Type;
using Demo.OmniChannel.Infrastructure.EventBus.Abstractions;
using Demo.OmniChannel.Infrastructure.EventBus.Event;

namespace Demo.OmniChannel.ComparingService.EventHandlers
{
    public class ProductPriceEventHandler : IIntegrationEventHandler<OmniSyncRecoveryPriceEvent>
    {
        private readonly ProductPriceProcess ProductPriceProcess;

        public ProductPriceEventHandler(
            ProductPriceProcess productPriceProcess)
        {
            ProductPriceProcess = productPriceProcess;
        }
        public async Task Handle(OmniSyncRecoveryPriceEvent @event)
        {
            await ProductPriceProcess.HandleComparingPrice(@event);
        }
    }
}

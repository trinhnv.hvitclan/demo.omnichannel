﻿using Demo.OmniChannel.ComparingService.BackgroundProcess.Type;
using Microsoft.Extensions.DependencyInjection;

namespace Demo.OmniChannel.ComparingService.Extensions
{
    public static class AddProcessExtension
    {
        public static void AddProcess(this IServiceCollection services)
        {
            services.AddScoped<ProductPriceProcess>();
        }
    }
}

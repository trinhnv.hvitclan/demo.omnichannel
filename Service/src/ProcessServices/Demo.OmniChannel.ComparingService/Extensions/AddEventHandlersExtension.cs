﻿using Demo.OmniChannel.ComparingService.EventHandlers;
using Demo.OmniChannel.Infrastructure.EventBus.Abstractions;
using Demo.OmniChannel.Infrastructure.EventBus.Event;
using Microsoft.Extensions.DependencyInjection;

namespace Demo.OmniChannel.ComparingService.Extensions
{
    public static class AddEventHandlersExtension
    {
        public static void AddEventHandlers(this IServiceCollection services)
        {
            services.AddScoped<IIntegrationEventHandler<OmniSyncRecoveryPriceEvent>, ProductPriceEventHandler>();
        }
    }
}

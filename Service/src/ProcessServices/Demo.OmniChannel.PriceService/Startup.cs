﻿using System;
using System.Linq;
using System.Reflection;
using Demo.OmniChannel.Business;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.Infrastructure.IoC;
using Demo.OmniChannel.Infrastructure.Logging;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Infrastructure.CronJob;
using Demo.OmniChannel.MongoService.Common;
using Demo.OmniChannel.MongoService.Impls;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.PriceService.Impls;
using Demo.OmniChannel.PriceService.Jobs;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Impls;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.Messaging.Redis;
using ServiceStack.OrmLite;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.Services.Impls;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ShareKernel.Exceptions;
using ServiceStack.Caching;
using Demo.OmniChannel.Services.LogginConfiguration;
using Demo.OmniChannel.ShareKernel.Auth;
using Microsoft.AspNetCore.Hosting;
using Demo.OmniChannel.DCControl.ConfigureService;
using Demo.OmniChannel.Infrastructure.EventBus.Extentions;
using Demo.OmniChannel.ShareKernel.Common;

namespace Demo.OmniChannel.PriceService
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            var builder = new ConfigurationBuilder()
                .AddConfiguration(configuration)
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            #region Register ServiceStack License
            Licensing.RegisterLicense(Configuration.GetSection("servicestack:license").Value);
            #endregion

            services.AddSingleton<IAppSettings>(x => new NetCoreAppSettings(Configuration));
            #region Redis
            var redisConfig = new KvRedisConfig();
            Configuration.GetSection("redis:cache").Bind(redisConfig);
            services.AddSingleton(sc => KvRedisPoolManager.GetClientsManager(redisConfig));
            services.AddSingleton<ICacheClient, KvCacheClient>();
            services.AddSingleton<IKvLockRedis, KvLockRedis>();

            var redisMqConfig = new KvRedisConfig();
            Configuration.GetSection("redis:message").Bind(redisMqConfig);
            services.AddSingleton(redisMqConfig);
            var redisFactory = KvRedisPoolManager.GetClientsManager(redisMqConfig);
            var mqHost = new RedisMqServer(redisFactory, retryCount: 2);
            services.AddSingleton<IMessageService>(mqHost);
            services.AddSingleton<IMessageFactory>(c => new RedisMessageFactory(redisFactory));
            #endregion

            #region Register kafka
            #region Register kafka
            var kafka = new Kafka();
            Configuration.GetSection("Kafka").Bind(kafka);
            KafkaClient.KafkaClient.Instance.SetKafkaProducerConfig(kafka, false);
            KafkaClient.KafkaClient.Instance.InitProducer(false);
            #endregion
            #endregion

            #region Register DatabaseConnectionString
            var kvSql = new KvSqlConnectString();
            Configuration.GetSection("SqlConnectStrings").Bind(kvSql);
            var connectFactory = new OrmLiteConnectionFactory(kvSql.KvChannelEntities, SqlServer2016Dialect.Provider);
            //Register Channel Db
            //SqlServerDialect.Provider.GetStringConverter().UseUnicode = true;
            //Register Master Db
            connectFactory.RegisterConnection(nameof(kvSql.KvMasterEntities), kvSql.KvMasterEntities, SqlServer2016Dialect.Provider);
            foreach (var item in kvSql.KvEntitiesDC1)
            {
                connectFactory.RegisterConnection(item.Name.ToLower(), item.Value, SqlServer2016Dialect.Provider);
            }
            OrmLiteConfig.DialectProvider.GetStringConverter().UseUnicode = true;
            services.AddSingleton(kvSql);
            services.AddSingleton<IDbConnectionFactory>(c => connectFactory);
            #endregion

            #region Register Mongo
            services.Configure<MongoDbSettings>(Configuration.GetSection("mongodb"));
            services.UsingMongoDb(Configuration);
            services.AddTransient<IProductMappingService, ProductMappingService>();
            services.AddTransient<IProductMongoService, ProductMongoService>();
            #endregion

            #region Register Channel Third Party
            services.AddSingleton(c => new ChannelClient.Impls.ChannelClient());
            #endregion

            services.AddScoped<IExecutionContextService, ExecutionContextService>();

            services.AddScoped<IAuditTrailInternalClient>(c => new AuditTrailInternalClient(c.GetService<IAppSettings>()));

            services.AddScoped<IOmniChannelAuthService, OmniChannelAuthService>();
            services.AddScoped<IOmniChannelSettingService, OmniChannelSettingService>();
            services.AddScoped<IOmniChannelWareHouseService, OmniChannelWareHouseService>();

            services.AddScoped<IChannelBusiness, ChannelBusiness>();
            services.AddScoped<IMappingBusiness, MappingBusiness>();
            services.AddScoped<IPriceBusiness, PriceBusiness>();

            services.AddScoped<IMicrosoftLoggingContextExtension, MicrosoftLoggingContextExtension>();
            services.AddScoped<IOmniChannelPlatformService, OmniChannelPlatformService>();

            services.AddConfigEventBus(Configuration);
            services.AddEventBus();

            //services.AddCronJob<MonitorJob>(c =>
            //{
            //    c.TimeZoneInfo = TimeZoneInfo.Local;
            //    c.CronExpression = @"* * * * *";
            //});
            //services.AddSingleton<IHostedService, SyncPriceService>();
            if (redisMqConfig.EventMessages.Any())
            {
                var lsName = redisMqConfig.EventMessages.Where(t => t.IsActive).Select(v => v.Name);
                var types = typeof(BaseService<>).GetTypeInfo().Assembly.DefinedTypes
                    .Where(p => p.GetTypeInfo().IsAssignableFrom(p.AsType()) && p.IsClass && lsName.Contains(p.Name)).Select(p => p.AsType());
                try
                {
                    foreach (var type in types)
                    {
                        services.AddTransient(typeof(IHostedService), type);
                    }
                }
                catch (Exception e)
                {
                    Log.Logger.Error($"Register {nameof(IHostedService)} error: {e.Message}", e);
                    throw;
                }

            }

            services.AddScoped(c =>
            {
                var eventContextSvc = c.GetRequiredService<IExecutionContextService>();

                if (eventContextSvc.Context == null)
                {
                    return new ExecutionContext();
                }

                var context = new ExecutionContext
                {
                    Id = eventContextSvc.Context.Id,
                    RetailerCode = eventContextSvc.Context.RetailerCode,
                    BranchId = eventContextSvc.Context.BranchId,
                    User = eventContextSvc.Context.User,
                    RetailerId = eventContextSvc.Context.RetailerId,
                    GroupId = eventContextSvc.Context.GroupId,
                    Group = eventContextSvc.Context.Group
                };
                return context;
            });

            SetEncryptPassPhrase();

            services.RegisterApplicationDcControl(new ApplicationDcInputOptions
            {
                ServiceName = "PriceService",
                AppSettings = new NetCoreAppSettings(Configuration)
            });
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime lifetime, IMessageService messageService)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            lifetime.ApplicationStarted.Register(messageService.Start);
        }

        private void SetEncryptPassPhrase()
        {
            CryptoHelper.PassPhrase = Configuration.GetSection("EncryptPassPhrase").Value;

            if (string.IsNullOrWhiteSpace(CryptoHelper.PassPhrase))
            {
                throw new KvException("Setting EncryptPassPhrase invalid. Check setting file, please.");
            }
        }
    }
}
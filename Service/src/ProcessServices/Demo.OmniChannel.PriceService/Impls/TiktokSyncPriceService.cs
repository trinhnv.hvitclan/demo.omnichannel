﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.RequestDTO;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Auth;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MultiProductItem = Demo.OmniChannel.ShareKernel.KafkaMessage.MultiProductItem;
using Platform = Demo.OmniChannel.ChannelClient.Models.Platform;

namespace Demo.OmniChannel.PriceService.Impls
{
    public class TiktokSyncPriceService : BaseService<TiktokSyncPriceMessage>
    {
        private readonly IChannelBusiness _channelBusiness;
        private readonly ICacheClient _cacheClient;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly IOmniChannelAuthService _channelAuthService;
        private readonly IOmniChannelPlatformService _omniChannelPlatformService;
        private readonly IProductMongoService _productMongoService;
        private readonly IIntegrationEventService _integrationEventService;

        public TiktokSyncPriceService(
            IIntegrationEventService integrationEventService,
          ILogger<TiktokSyncPriceService> logger,
          KvRedisConfig mqRedisConfig,
          IMessageService messageService,
          IChannelBusiness channelBusiness,
          ICacheClient cacheClient,
          IDbConnectionFactory dbConnectionFactory,
          IOmniChannelAuthService channelAuthService,
          IOmniChannelPlatformService omniChannelPlatformService,
          IAuditTrailInternalClient auditTrailInternalClient,
          IProductMappingService productMappingService,
          IProductMongoService productMongoService,
          IAppSettings settings
     ) : base(logger,
         cacheClient,
         productMappingService,
         dbConnectionFactory,
         messageService,
         auditTrailInternalClient)
        {
            var threadSize = mqRedisConfig?.EventMessages.FirstOrDefault(x => !string.IsNullOrEmpty(x.Name) &&
                                                                         x.Name.Contains(GetType()
                                                                             .Name))?.ThreadSize ?? 10;
            messageService.RegisterHandler<TiktokSyncPriceMessage>(m =>
            {
                try
                {
                    m.Options = (int)MessageOption.None;
                    var task = Task.Run(async () => await ProcessMessage(m.GetBody()));
                    task.GetAwaiter().GetResult();
                    return Task.CompletedTask;
                }
                catch (Exception e)
                {
                    ExceptionHelper.WriteLogExceptionMq(typeof(TiktokSyncPriceMessage), m.Body, e);
                    return string.Empty;
                }
            }, threadSize);
            _channelBusiness = channelBusiness;
            _cacheClient = cacheClient;
            _dbConnectionFactory = dbConnectionFactory;
            _channelAuthService = channelAuthService;
            _omniChannelPlatformService = omniChannelPlatformService;
            _productMongoService = productMongoService;
            Settings = settings;
            _integrationEventService = integrationEventService;
        }

        protected override async Task ProcessMessage(TiktokSyncPriceMessage data)
        {
            var logger = new LogObjectMicrosoftExtension(Logger, data.LogId)
            {
                Action = "TiktokMultiSyncPriceV2",
                RetailerCode = data.RetailerCode,
                RetailerId = data.RetailerId,
                BranchId = data.BranchId,
                OmniChannelId = data.ChannelId,
                RequestObject = $"Id: {data.ItemId} Sku:{data.ItemSku} BasePrice:{data.BasePrice} SalePrice: {data.SalePrice} MultiProduct:{data.MultiProducts.ToSafeJson()}",
                ChannelType = data.ChannelType,
                ChannelTypeCode = ((ChannelTypeEnum)data.ChannelType).ToString()
            };

            var channel = await _channelBusiness.GetChannel(data.ChannelId, data.RetailerId, data.LogId);
            if (channel == null)
            {
                logger.LogError(new Exception("Channel is null"));
                return;
            }

            var auth = await _channelAuthService.GetChannelAuth(channel, data.LogId);
            var platform = await _omniChannelPlatformService.GetById(channel.PlatformId);
            if (auth == null)
            {
                logger.LogError(new Exception("Token is Expired"));
                return;
            }

            var authKey = new ChannelAuth
            {
                AccessToken = auth.AccessToken,
                RefreshToken = auth.RefreshToken,
                ShopId = auth.ShopId
            };
            await SyncPrice(data, authKey, channel, logger, platform);

            logger.LogInfo();
        }

        private async Task SyncPrice(SyncPriceMessage data, ChannelAuth authKey,
            Domain.Model.OmniChannel channel, LogObjectMicrosoftExtension syncOnHandLog,
            Platform platform)
        {
            var products = new List<MultiProductItem>();

            data.MultiProducts = data.MultiProducts?.Where(item => item.Price.HasValue).ToList();
            var groupKvProductId = data.MultiProducts?.Where(item => item.Price.HasValue).GroupBy(x => x.KvProductId)
                .Select(x => new MultiProductItem
                {
                    KvProductId = x.Key,
                    Price = x.First().Price,
                    Revision = x.First().Revision,
                    StartSaleDate = x.First().StartSaleDate,
                    EndSaleDate = x.First().EndSaleDate,
                    SalePrice = x.First().SalePrice
                }).ToList();
            if (groupKvProductId == null || !groupKvProductId.Any()) return;

            foreach (var productItem in groupKvProductId)
            {
                products.AddRange(await GenerateProducts(data, productItem));
            }

            foreach (var product in products)
            {
                await SyncMultiPrice(data, product.ParentItemId, new List<MultiProductItem> { product }, authKey, channel, syncOnHandLog, platform);
            }
        }

        private async Task SyncMultiPrice(SyncPriceMessage data,
            string parrentItemId,
            List<MultiProductItem> listProducts,
            ChannelAuth authKey, Domain.Model.OmniChannel channel,
            LogObjectMicrosoftExtension loggerExtension, Platform platform)
        {
            var client = new ChannelClient.Impls.TikTokClient(Settings);
            var connectionStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);
            var context = await ContextHelper.GetExecutionContext(_cacheClient,
              _dbConnectionFactory,
              data.RetailerId,
              connectionStringName,
              data.BranchId,
              Settings?.Get<int>("ExecutionContext"));

            var productsWithGroupParrent = listProducts.ConvertAll(x => x.ConvertTo<ChannelClient.RequestDTO.MultiProductItem>());

            var (isSuccess, message, errorMessage) =
                await client.SyncPriceWithGroupParrent(authKey, parrentItemId, productsWithGroupParrent, loggerExtension, platform);

            var multiProductResponse = !string.IsNullOrEmpty(message)
                          ? JsonConvert.DeserializeObject<MultiProductResponse>(message)
                          : new MultiProductResponse();

            await ProcessResponseData(data, channel, productsWithGroupParrent.Select(x => x.ItemId).ToList(),
                listProducts, multiProductResponse, context, connectionStringName, isSuccess, errorMessage);

            var delProductIds = multiProductResponse.Product.Where(x => SelfAppConfig.Tiktok.MsgProductDelete.Any(msg => msg == x.ErrorMessage))
               .Select(x => x.ItemId).ToList();
            if (delProductIds.Any()) await DeleteProductWithErrorMessage(channel, delProductIds, connectionStringName);
        }

        private async Task ProcessResponseData(
            SyncPriceMessage data,
            Domain.Model.OmniChannel channel,
            List<string> batchItemIds,
            List<MultiProductItem> listProducts,
            MultiProductResponse multiProductResponse,
            ExecutionContext context,
            string connectionStringName,
            bool isSuccess,
            string errorMessage)
        {
            var channelProducts =
                        await _productMongoService.GetByProductChannelIds(data.RetailerId, data.ChannelId,
                            batchItemIds, isStringItemId: ConvertHelper.CheckUseStringItemId(channel.Type));
            var logItemSuccess = new List<string>();
            var logItemFail = new List<string>();

            var salePriceName = await GetSalePriceName(data, channel);

            if (!isSuccess)
            {
                if (channelProducts != null && channelProducts.Any())
                {
                    await _productMongoService.UpdateFailedMessageAndStatusCode(channelProducts.Select(p => p.Id).ToList(), 0,
                        errorMessage, SyncErrorType.Price);

                    foreach (var channelProductItem in channelProducts)
                    {
                        var map = listProducts.FirstOrDefault(p =>p.ItemId == channelProductItem.StrItemId);
                        if (map != null)
                        {
                            PushLishMessageErrorNotification(channel, map, errorMessage, data);
                        }
                    }
                }
                logItemFail.Add($"- {errorMessage}");

                await HandleAuditMessage(channel, data, context, logItemSuccess, logItemFail, null);
                return;
            }

            foreach (var item in multiProductResponse.Product)
            {
                var map = listProducts.FirstOrDefault(p => item.ItemId == p.ItemId);
                var pro = channelProducts.FirstOrDefault(x => x.CommonItemId == item.ItemId);
                if (pro != null && !string.IsNullOrEmpty(item.ErrorMessage))
                {
                    var lstErrorRemoveMsg = GetProductRemoveError(data.ChannelType);
                    lstErrorRemoveMsg = lstErrorRemoveMsg.Select(p => p.Trim().ToLower()).ToList();
                    if (!string.IsNullOrEmpty(item.ErrorMessage) &&
                        lstErrorRemoveMsg.Contains(item.ErrorMessage.Trim().ToLower()))
                    {
                        await _productMongoService.RemoveAsync(pro.Id);

                        PushMessageRemoveProductMapping(context, connectionStringName, data, pro);
                    }
                    else
                    {
                        var err = pro.SyncErrors.FirstOrDefault(x => x.Type == SyncErrorType.Price);
                        if (err != null)
                        {
                            err.Message = StringHelper.ReplaceHexadecimalSymbols(item.ErrorMessage);
                        }
                        else
                        {
                            pro.SyncErrors.Add(new SyncError
                            {
                                Type = SyncErrorType.Price,
                                Message = StringHelper.ReplaceHexadecimalSymbols(item.ErrorMessage)
                            });
                        }
                        pro.SyncStatusCode = null;
                        await _productMongoService.UpdateAsync(pro.Id, pro);

                      
                    }
                    PushLishMessageErrorNotification(channel, map, item.ErrorMessage, data);
                    logItemFail.Add(
                        $"- [ProductCode]{map?.KvProductSku}[/ProductCode] KHÔNG thành công: Giá bán: {item.ErrorMessage} <br/>");
                }
                else if (pro != null)
                {
                    pro.SyncErrors.RemoveAll(x => x.Type == SyncErrorType.Price);
                    pro.SyncStatusCode = null;
                    await _productMongoService.UpdateAsync(pro.Id, pro);
                    logItemSuccess.Add(
                        $"- [ProductCode]{map?.KvProductSku}[/ProductCode] thành công: Giá bán: {NumberHelper.NormallizePrice((double)(item.BasePrice))} <br/>");
                    if (item.SalePrice != null && !string.IsNullOrEmpty(salePriceName) && channel.PriceBookId != null &&
                        channel.PriceBookId > 0 && ((double)(item.SalePrice) > 0 &&
                                                    (double)(item.SalePrice) < (double)(item.BasePrice)))
                    {
                        logItemSuccess.Add(
                            $"- [ProductCode]{map?.KvProductSku}[/ProductCode] thành công: Giá khuyến mãi: {NumberHelper.NormallizePrice((double)(item.SalePrice ?? 0))} <br/>");
                    }
                }
            }

            SaveCacheProductsSyncSuccess(listProducts, data.ChannelId);

            await HandleAuditMessage(channel, data, context, logItemSuccess, logItemFail,
                salePriceName);
        }

        private void PushLishMessageErrorNotification(Domain.Model.OmniChannel channel, MultiProductItem item, string errorMessage, SyncPriceMessage data)
        {
            var logger = new LogObjectMicrosoftExtension(Logger, data.LogId)
            {
                Action = "PushErrorProductNotificationMesage",
                RetailerCode = data.RetailerCode,
                RetailerId = data.RetailerId,
                BranchId = data.BranchId,
                OmniChannelId = data.ChannelId,
            };
            var obj = new Infrastructure.EventBus.Event.ErrorProductNotificationMesage(data.LogId)
            {
                RetailerId = channel.RetailerId,
                BranchId = channel.BranchId,
                ChannelId = channel.Id,
                ErrorType = (int)Sdk.Common.SyncTabError.Product,
                ItemSku = item.KvProductSku,
                ItemName = item.ProductName,
                ErrorMessage = errorMessage,
                Type = channel.Type
            };
            _integrationEventService.AddEventWithRoutingKeyAsync(obj, RoutingKey.MessageErrorProductNotification);
            logger.RequestObject = obj.ToJson();
            logger.LogInfo(true);
        }

        private async Task DeleteProductWithErrorMessage(Domain.Model.OmniChannel channel,
           List<string> delProductIds,
           string connectStringName
           )
        {
            await _productMongoService.RemoveByItemIds(channel.RetailerId, channel.Id, delProductIds,
                isStringItemId: ConvertHelper.CheckUseStringItemId(channel?.Type));

            var removeMsg = new RemoveProductMappingMessage
            {
                RetailerId = channel.RetailerId,
                BranchId = channel.BranchId,
                ChannelIds = new List<long> { channel.Id },
                ChannelProductIds = delProductIds,
                ConnectStringName = connectStringName,
                IsDeleteKvProduct = false,
                LogId = Guid.NewGuid(),
                MessageFrom = "TiktokSyncPriceService"
            };
            var kafkaConfig = Settings.Get<Kafka>("Kafka");
            var topicName = kafkaConfig.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.RemoveMappingRequest && x.ChannelType == channel.Type)?.Name;
            KafkaClient.KafkaClient.Instance.PublishMessage($"{topicName}",
                $"{removeMsg.RetailerId}_{removeMsg.BranchId}_{channel.Id}", JsonConvert.SerializeObject(removeMsg), isV2: true);
        }
    }
}
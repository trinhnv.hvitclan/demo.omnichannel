﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Demo.Audit.Model.Message;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.RequestDTO;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.PriceService.Common;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Sdk.OmniExceptions;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.Services.LogginConfiguration;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using ChannelType = Demo.OmniChannel.ShareKernel.Common.ChannelType;
using MultiProductItem = Demo.OmniChannel.ShareKernel.KafkaMessage.MultiProductItem;
using Platform = Demo.OmniChannel.ChannelClient.Models.Platform;

namespace Demo.OmniChannel.PriceService.Impls
{
    public abstract class SyncPriceService<T> : BaseService<T> where T : SyncPriceMessage
    {
        private readonly ChannelClient.Impls.ChannelClient _channelClient;
        private readonly IProductMongoService _productMongoService;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly ICacheClient _cacheClient;
        protected ChannelType ChannelType;
        private readonly IOmniChannelAuthService _channelAuthService;
        private readonly IMessageService _messageService;
        private readonly IChannelBusiness _channelBusiness;
        private readonly IOmniChannelPlatformService _omniChannelPlatformService;

        public SyncPriceService(IProductMongoService productMongoService,
            ChannelClient.Impls.ChannelClient channelClient,
            IProductMappingService productMappingService,
            ICacheClient cacheClient,
            IAppSettings settings,
            IDbConnectionFactory dbConnectionFactory,
            IAuditTrailInternalClient auditTrailInternalClient,
            IMessageService messageService,
            KvRedisConfig mqRedisConfig,
            IOmniChannelAuthService channelAuthService,
            IChannelBusiness channelBusiness,
            IOmniChannelPlatformService omniChannelPlatformService,
            ILogger logger
        ) : base(logger,
            cacheClient,
            productMappingService,
            dbConnectionFactory,
            messageService,
            auditTrailInternalClient)
        {
            //Kafka = KafkaClient.KafkaClient.Instance.GetKafka();
            //TopicName = $"{Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.SyncPrice)?.Name}";
            //ConsumerSize = Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.SyncPrice)?.Size ?? 1;
            var threadSize = mqRedisConfig?.EventMessages.FirstOrDefault(x => !string.IsNullOrEmpty(x.Name) &&
                                                                          x.Name.Contains(this.GetType()
                                                                              .Name))?.ThreadSize ?? 10;
            messageService.RegisterHandler<T>(m =>
            {
                try
                {
                    m.Options = (int)MessageOption.None;
                    var task = Task.Run(async () => await ProcessMessage(m.GetBody()));
                    task.Wait();
                    return Task.CompletedTask;
                }
                catch (Exception e)
                {
                    ExceptionHelper.WriteLogExceptionMq(typeof(T), m.Body, e);
                    return string.Empty;
                }
            }, threadSize);
            _channelClient = channelClient;
            _productMongoService = productMongoService;
            _cacheClient = cacheClient;
            Settings = settings;
            _dbConnectionFactory = dbConnectionFactory;
            _channelAuthService = channelAuthService;
            _channelBusiness = channelBusiness;
            _messageService = messageService;
            _omniChannelPlatformService = omniChannelPlatformService;
        }
        protected override async Task ProcessMessage(T data)
        {
            var action = data.MultiProducts?.Any() == true ? ExceptionType.MultiSyncPrice : ExceptionType.SyncPrice;

            var loggerExtension = new LogObjectMicrosoftExtension(Logger, data.LogId)
            {
                Action = action,
                RetailerCode = data.RetailerCode,
                RetailerId = data.RetailerId,
                BranchId = data.BranchId,
                OmniChannelId = data.ChannelId,
                RequestObject = $"Id: {data.ItemId} Sku:{data.ItemSku} BasePrice:{data.BasePrice} SalePrice: {data.SalePrice} MultiProduct:{data.MultiProducts.ToSafeJson()}",
                ChannelType = data.ChannelType,
                ChannelTypeCode = ((ChannelTypeEnum)data.ChannelType).ToString()
            };
            try
            {
                var channel = await _channelBusiness.GetChannel(data.ChannelId, data.RetailerId, data.LogId);
                if (channel == null)
                {
                    loggerExtension.LogWarning($"Not found retailerId: {data.RetailerId} and ChannelId: {data.ChannelId}");
                    return;
                }
                var connectStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);
                var context = await ContextHelper.GetExecutionContext(_cacheClient, _dbConnectionFactory, data.RetailerId, connectStringName, data.BranchId, Settings?.Get<int>("ExecutionContext"));
                var client =
                    _channelClient.GetClient((byte)ChannelType, Settings);

                context.ShopId = channel.IdentityKey;
                context.ChannelId = channel.Id;

                var coreContext = context.ConvertTo<OmniChannelCore.Api.Sdk.Common.KvInternalContext>();
                coreContext.UserId = context.User?.Id ?? 0;
                var auth = await _channelAuthService.GetChannelAuth(channel, data.LogId);
                var platform = await _omniChannelPlatformService.GetById(channel.PlatformId);

                if (auth == null)
                {
                    loggerExtension.Description = "Token is Expired";
                    var ex = new OmniException("Token is Expired");
                    loggerExtension.LogError(ex, true);
                    return;
                }

                var authKey = new ChannelAuth
                {
                    AccessToken = auth.AccessToken,
                    RefreshToken = auth.RefreshToken,
                    ShopId = auth.ShopId
                };
                if (data.ChannelType == (byte)ChannelType.Lazada)
                {
                    await Task.Delay(Settings?.Get<int>("LazadaDelaySyncPrice", 1) ?? 1);
                }
                await SynPrice(data, client, authKey, channel, loggerExtension, coreContext, platform);

                loggerExtension.LogInfo();
            }
            catch (Exception ex)
            {
                loggerExtension.LogError(ex);
                throw;
            }
        }

        private async Task SyncMultiPrice(SyncPriceMessage data, List<MultiProductItem> listProducts,
            IBaseClient client, ChannelAuth authKey, Domain.Model.OmniChannel channel,
            LogObjectMicrosoftExtension loggerExtension, byte itemType,
            Platform platform)
        {
            try
            {
                var pageSize = GetSyncMultiPageSize(data.ChannelType);
                var totalProduct = listProducts.Count / pageSize;
                var totalPage = listProducts.Count % pageSize == 0 ? totalProduct : totalProduct + 1;
                for (var i = 0; i < totalPage; i++)
                {
                    var currentPage = i * pageSize;
                    var batchItem = listProducts.Skip(currentPage).Take(pageSize).ToList();
                    var batchItemIds = batchItem.Select(p => p.ItemId).ToList();
                    var (isSuccess, message, statusCode) =
                        await client.SyncMultiPrice(data.RetailerId, data.ChannelId, data.LogId, authKey,
                            batchItem.ConvertAll(x => x.ConvertTo<ChannelClient.RequestDTO.MultiProductItem>()),
                            itemType, loggerExtension, platform);

                    if (!isSuccess && statusCode is (int)HttpStatusCode.BadGateway)
                    {
                        var failIds = await _productMongoService.GetByProductChannelIds(data.RetailerId, data.ChannelId,
                                    batchItemIds, ConvertHelper.CheckUseStringItemId((byte)ChannelType));
                        if (failIds != null && failIds.Any())
                        {
                            await _productMongoService.UpdateFailedMessageAndStatusCode(failIds.Select(p => p.Id).ToList(), (int)HttpStatusCode.BadGateway,
                                "Bad Gateway", SyncErrorType.Price);
                        }
                    }
                    else
                    {
                        if (isSuccess)
                        {
                            foreach (var kvProduct in listProducts)
                            {
                                _cacheClient.Set(KvConstant.KvPriceRevision.FormatWith(data.ChannelId, kvProduct.KvProductId),
                                    new PriceDto { KvProductId = kvProduct.KvProductId, BasePrice = kvProduct.Price, Revision = kvProduct.Revision },
                                    TimeSpan.FromDays(Settings?.Get<int>("RevisionProductExpired", 1) ?? 1));
                            }
                        }

                        var multiProductResponse = !string.IsNullOrEmpty(message)
                            ? JsonConvert.DeserializeObject<MultiProductResponse>(message)
                            : new MultiProductResponse();

                        await HandleProductWhenSyncPriceSuccess(data, channel, batchItemIds, listProducts, multiProductResponse,
                            loggerExtension);
                    }
                }
            }
            catch (Exception ex)
            {
                loggerExtension.LogError(ex);
                throw;
            }
        }

        private async Task SynPrice(SyncPriceMessage data, IBaseClient client, ChannelAuth authKey,
            Domain.Model.OmniChannel channel, LogObjectMicrosoftExtension syncOnHandLog,
            OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext,
            Platform platform)
        {
            var products = new List<MultiProductItem>();
            if (data.MultiProducts == null || !data.MultiProducts.Any())
            {
                var (priceBook, revision) = await GetKvProductInfo(data, channel, coreContext, syncOnHandLog);
                if (priceBook != null)
                {
                    data.MultiProducts = new List<MultiProductItem>
                    {
                        new MultiProductItem
                        {
                            ItemId = data.ItemId,
                            ItemSku = data.ItemSku,
                            ItemType = data.ItemType,
                            ParentItemId = data.ParentItemId,
                            KvProductId = data.KvProductId,
                            Price = priceBook.BasePrice,
                            SalePrice = priceBook.SalePrice,
                            EndSaleDate = priceBook.EndSaleTime,
                            StartSaleDate = priceBook.StartSaleTime,
                        }
                    };
                }
            }

            data.MultiProducts = data.MultiProducts?.Where(item => item.Price.HasValue).ToList();
            var groupKvProductId = data.MultiProducts?.Where(item => item.Price.HasValue).GroupBy(x => x.KvProductId)
                .Select(x => new MultiProductItem
                {
                    KvProductId = x.Key,
                    Price = x.First().Price,
                    Revision = x.First().Revision,
                    StartSaleDate = x.First().StartSaleDate,
                    EndSaleDate = x.First().EndSaleDate,
                    SalePrice = x.First().SalePrice
                }).ToList();
            if (groupKvProductId == null || !groupKvProductId.Any()) return;

            foreach (var productItem in groupKvProductId)
            {
                products.AddRange(await GenerateProducts(data, productItem));
            }

            var productsVariation = products
                .Where(x => x.ItemType == (byte)ShareKernel.Common.ChannelProductType.Variation).ToList();
            var productsNormal = products
                .Where(x => x.ItemType == (byte)ShareKernel.Common.ChannelProductType.Normal).ToList();

            if (productsVariation.Any())
            {
                await SyncMultiPrice(data, productsVariation, client, authKey, channel, syncOnHandLog, (byte)ShareKernel.Common.ChannelProductType.Variation, platform);
            }
            if (productsNormal.Any())
            {
                await SyncMultiPrice(data, productsNormal, client, authKey, channel, syncOnHandLog, (byte)ShareKernel.Common.ChannelProductType.Normal, platform);
            }
        }

        private async Task<(PriceBookDTO, byte[])> GetKvProductInfo(SyncPriceMessage data, Domain.Model.OmniChannel channel, OmniChannelCore.Api.Sdk.Common.KvInternalContext coreContext, LogObjectMicrosoftExtension loggerExtension)
        {
            var retVal = new PriceBookDTO();
            var connectStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);
            using var db = _dbConnectionFactory.OpenDbConnection(connectStringName.ToLower());
            var productBranchRepository = new ProductBranchRepository(db);
            var priceBookRepository = new PriceBookRepository(db);
            var productRepository = new ProductRepository(db);
            var channelProducts =
                await GetChannelProducts(channel.RetailerId, channel.Id, data.KvProductId);
            if (channelProducts == null || !channelProducts.Any())
            {
                loggerExtension.Description = "Empty Mapping Product";
                loggerExtension.LogInfo();
                return (null, null);
            }
            var kvProduct = await productRepository.GetByIdAsync(data.KvProductId);
            if (kvProduct.isDeleted.GetValueOrDefault())
            {
                loggerExtension.Description = "Product is deleted";
                loggerExtension.LogInfo();
                return (null, null);
            }

            var active = await productBranchRepository.WhereAsync(x => x.RetailerId == data.RetailerId &&
                                                                       x.ProductId == data.KvProductId && x.BranchId == channel.BranchId);
            if (active.Any(x => x.IsActive == false))
            {
                loggerExtension.Description = "product is inactive";
                loggerExtension.LogInfo();
                return (null, null);
            }
            if (channel.BasePriceBookId.GetValueOrDefault() > 0)
            {
                // Bảng giá bán
                var (price, _, _, errorMessage) = await priceBookRepository.GetPriceByPriceBook(data.RetailerId, data.BranchId, data.KvProductId, channel.BasePriceBookId.GetValueOrDefault());
                if (price.HasValue)
                {
                    retVal.BasePrice = price.Value;
                }
                else
                {
                    // log bảng giá bán không hợp lệ
                    var log = new AuditTrailLog
                    {
                        FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channel.Type),
                        Action = (int)AuditTrailAction.ProductIntergate,
                        CreatedDate = DateTime.Now,
                        BranchId = channel.BranchId
                    };
                    var typeName = Enum.GetName(typeof(ChannelType), channel.Type);
                    var contentLog = new StringBuilder($"Đồng bộ thông tin giá bán hàng hóa [ProductCode]{kvProduct?.Code}[/ProductCode] KHÔNG thành công: {typeName} shop: {channel.Email ?? channel.Name}, Bảng giá bán không hợp lệ :{errorMessage}");
                    if (!data.IsIgnoreAuditTrail)
                    {
                        log.Content = contentLog.ToString();
                        await _auditTrailInternalClient.AddLogAsync(coreContext, log);
                    }
                    retVal.BasePrice = null;

                    //Update lỗi bảng giá bán không hợp lệ
                    await LogPriceErrorMongo(data, channelProducts, errorMessage);
                }
            }
            else
            {
                retVal.BasePrice = kvProduct.BasePrice;
                retVal.PriceBookName = "Bảng giá chung";
            }

            bool isSalePriceExisted = false;
            if (channel.PriceBookId != null && channel.PriceBookId > 0)
            {
                // Bảng giá khuyến mại
                var (price, startDate, endDate, errorMessage) = await priceBookRepository.GetPriceByPriceBook(data.RetailerId, data.BranchId, data.KvProductId, channel.PriceBookId.GetValueOrDefault());
                if (price.HasValue)
                {
                    retVal.SalePrice = price.Value;
                    retVal.StartSaleTime = startDate;
                    retVal.EndSaleTime = endDate;
                }
                else
                {
                    retVal.SalePrice = null;
                }
                if (price > 0)
                    isSalePriceExisted = true;
            }
            else
            {
                retVal.SalePrice = null;
                retVal.StartSaleTime = DateTime.Now;
                retVal.EndSaleTime = DateTime.Now;
            }

            if (data.ChannelType == (byte)ChannelType.Sendo && !isSalePriceExisted && data.IsMessageFromTrackingServices)
            {
                retVal.SalePrice = 0;
                retVal.StartSaleTime = null;
                retVal.EndSaleTime = null;
            }

            var (basePriceBookName, salePriceBookName) = await priceBookRepository.GetChannelPriceBookName(channel);
            retVal.PriceBookName = basePriceBookName;
            retVal.SalePriceBookName = salePriceBookName;

            return (retVal, kvProduct.Revision);
        }

        private async Task LogPriceErrorMongo(SyncPriceMessage data,List<ProductMapping> channelProducts, string errorMessage)
        {
            var productMappings = channelProducts.Where(t => t.ProductKvId == data.KvProductId && t.RetailerId == data.RetailerId && t.ChannelId == data.ChannelId).ToList();
            var errorProducts = await _productMongoService.GetByProductChannelIds(data.RetailerId, data.ChannelId, productMappings?.Select(t => t.ProductChannelId.ToString()).ToList(), isStringItemId: ConvertHelper.CheckUseStringItemId((byte)ChannelType));
            foreach (var errorProduct in errorProducts)
            {
                var err = errorProduct.SyncErrors.FirstOrDefault(x => x.Type == SyncErrorType.Price);
                if (err != null)
                {
                    err.Message = StringHelper.ReplaceHexadecimalSymbols(errorMessage);
                }
                else
                {
                    errorProduct.SyncErrors.Add(new SyncError
                    {
                        Type = SyncErrorType.Price,
                        Message = StringHelper.ReplaceHexadecimalSymbols(errorMessage)
                    });
                }
                await _productMongoService.UpdateAsync(errorProduct.Id, errorProduct);
            }
        }

        private async Task HandleProductWhenSyncPriceSuccess(SyncPriceMessage data, Domain.Model.OmniChannel channel,
            List<string> batchItemIds, List<MultiProductItem> listProducts, MultiProductResponse multiProductResponse,
            LogObjectMicrosoftExtension loggerExtension)
        {
            var logResult = loggerExtension.Clone("MultiSyncPriceResult");

            var channelProducts =
                        await _productMongoService.GetByProductChannelIds(data.RetailerId, data.ChannelId,
                            batchItemIds, isStringItemId: ConvertHelper.CheckUseStringItemId((byte)ChannelType));
            var logItemSuccess = new List<string>();
            var logItemFail = new List<string>();
            var connectStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);
            var context = await ContextHelper.GetExecutionContext(_cacheClient,
                _dbConnectionFactory,
                data.RetailerId,
                connectStringName,
                data.BranchId,
                Settings?.Get<int>("ExecutionContext"));
            var coreContext = context.ConvertTo<OmniChannelCore.Api.Sdk.Common.KvInternalContext>();
            coreContext.UserId = context.User?.Id ?? 0;
            var salePriceName = await GetSalePriceName(data, channel);

            foreach (var item in multiProductResponse.Product)
            {
                var map = listProducts.FirstOrDefault(p => item.ItemId == p.ItemId);
                var pro = channelProducts.FirstOrDefault(x => x.CommonItemId == item.ItemId);
                if (pro != null && !string.IsNullOrEmpty(item.ErrorMessage))
                {
                    var lstErrorRemoveMsg = GetProductRemoveError(data.ChannelType);
                    lstErrorRemoveMsg = lstErrorRemoveMsg.Select(p => p.Trim().ToLower()).ToList();
                    if (!string.IsNullOrEmpty(item.ErrorMessage) &&
                        lstErrorRemoveMsg.Contains(item.ErrorMessage.Trim().ToLower()))
                    {
                        logResult.Action = ExceptionType.RemoveMappingBySyncPrice;
                        logResult.RequestObject = $"ItemId: {item.ItemId} ItemSku:{item.ItemSku}";
                        logResult.Description = $"Remove mapping by PriceService (SyncPrice response: {item.ErrorMessage})";
                        logResult.LogInfo();

                        await _productMongoService.RemoveAsync(pro.Id);

                        var configMappingV2Feature = Settings?.Get<MappingV2Feature>("MappingV2Feature");
                        if (configMappingV2Feature != null && configMappingV2Feature.IsValid(context.Group?.Id ?? 0, data.RetailerId))
                        {
                            var removeMsg = new RemoveProductMappingMessage
                            {
                                RetailerId = context.RetailerId,
                                BranchId = context.BranchId,
                                ChannelIds = new List<long> { data.ChannelId },
                                ChannelProductIds = new List<string> { pro.CommonItemId },
                                ConnectStringName = connectStringName,
                                IsDeleteKvProduct = false,
                                LogId = data.LogId,
                                MessageFrom = "SyncPriceService"
                            };
                            var kafkaMessage = new KafkaMessage(configMappingV2Feature.TopicRemoveMappingRequestName,
                                $"{removeMsg.RetailerId}_{removeMsg.BranchId}_{data.ChannelId}", removeMsg);
                            KafkaClient.KafkaClient.Instance.PublishMessage(kafkaMessage.TopicName, kafkaMessage.Key, JsonConvert.SerializeObject(kafkaMessage), isV2: true);
                        }
                        else
                        {
                            using var mq = _messageService.CreateMessageProducer();
                            var removeMsg = new RemoveProductMappingMessage
                            {
                                RetailerId = context.RetailerId,
                                BranchId = context.BranchId,
                                ChannelIds = new List<long> { data.ChannelId },
                                ChannelProductIds = new List<string> { pro.CommonItemId },
                                ConnectStringName = connectStringName,
                                IsDeleteKvProduct = false,
                                LogId = data.LogId
                            };
                            mq.Publish(removeMsg);
                        }
                    }
                    else
                    {
                        var err = pro.SyncErrors.FirstOrDefault(x => x.Type == SyncErrorType.Price);
                        if (err != null)
                        {
                            err.Message = StringHelper.ReplaceHexadecimalSymbols(item.ErrorMessage);
                        }
                        else
                        {
                            pro.SyncErrors.Add(new SyncError
                            {
                                Type = SyncErrorType.Price,
                                Message = StringHelper.ReplaceHexadecimalSymbols(item.ErrorMessage)
                            });
                        }
                        pro.SyncStatusCode = null;
                        await _productMongoService.UpdateAsync(pro.Id, pro);
                        PushLishMessageErrorNotification(channel, map, item.ErrorMessage, data);

                    }
                    logItemFail.Add(
                        $"- [ProductCode]{map?.KvProductSku}[/ProductCode] KHÔNG thành công: Giá bán: {item.ErrorMessage} <br/>");
                }
                else if (pro != null)
                {
                    pro.SyncErrors.RemoveAll(x => x.Type == SyncErrorType.Price);
                    pro.SyncStatusCode = null;
                    await _productMongoService.UpdateAsync(pro.Id, pro);
                    logItemSuccess.Add(
                        $"- [ProductCode]{map?.KvProductSku}[/ProductCode] thành công: Giá bán: {NumberHelper.NormallizePrice((double)(item.BasePrice))} <br/>");
                    if (item.SalePrice != null && !string.IsNullOrEmpty(salePriceName) && channel.PriceBookId != null &&
                        channel.PriceBookId > 0 && ((double)(item.SalePrice) > 0 &&
                                                    (double)(item.SalePrice) < (double)(item.BasePrice)))
                    {
                        logItemSuccess.Add(
                            $"- [ProductCode]{map?.KvProductSku}[/ProductCode] thành công: Giá khuyến mãi: {NumberHelper.NormallizePrice((double)(item.SalePrice ?? 0))} <br/>");
                    }
                }
            }

           
            if (data.IsIgnoreAuditTrail) return;

            var log = new AuditTrailLog
            {
                FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channel.Type),
                Action = (int)AuditTrailAction.ProductIntergate,
                CreatedDate = DateTime.Now,
                BranchId = data.BranchId
            };
            var typeName = EnumHelper.GetNameByType<ChannelType>(channel.Type);
            var contentLog = new StringBuilder($"Đồng bộ thông tin giá bán hàng hóa: {typeName} shop: {channel.Email ?? channel.Name}, Bảng giá bán: {data.PriceBookName}");

            contentLog.Append(!string.IsNullOrEmpty(salePriceName)
                ? $", Bảng giá khuyến mãi: {salePriceName} <br/>"
                : "<br/>");

            contentLog.Append($"{string.Join("", logItemSuccess)}{string.Join("", logItemFail)}");
            log.Content = contentLog.ToString();
            await _auditTrailInternalClient.AddLogAsync(coreContext, log);
        }

        protected virtual void PushLishMessageErrorNotification(Domain.Model.OmniChannel channel, MultiProductItem item, string errorMessage, SyncPriceMessage data)
        {

        }

    }

}
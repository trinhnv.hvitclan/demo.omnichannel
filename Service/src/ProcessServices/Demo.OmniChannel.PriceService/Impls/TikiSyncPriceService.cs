﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.PriceService.Impls
{
    public class TikiSyncPriceService : SyncPriceService<TikiSyncPriceMessage>
    {
        private readonly IIntegrationEventService _integrationEventService;

        public TikiSyncPriceService(
            IIntegrationEventService integrationEventService,
            IProductMongoService productMongoService,
            ChannelClient.Impls.ChannelClient channelClient,
            IProductMappingService productMappingService,
            ICacheClient cacheClient,
            IAppSettings settings,
            IDbConnectionFactory dbConnectionFactory,
            IAuditTrailInternalClient auditTrailInternalClient,
            IMessageService messageService,
            KvRedisConfig mqRedisConfig,
            IOmniChannelAuthService channelAuthService,
            IChannelBusiness channelBusiness,
            IOmniChannelPlatformService omniChannelPlatformService,
            ILogger<TikiSyncPriceService> logger) : base(productMongoService,
            channelClient,
            productMappingService,
            cacheClient,
            settings,
            dbConnectionFactory,
            auditTrailInternalClient,
            messageService,
            mqRedisConfig,
            channelAuthService,
            channelBusiness,
            omniChannelPlatformService,
            logger)
        {
            ChannelType = ChannelType.Tiki;
            _integrationEventService = integrationEventService;
        }

        protected override void PushLishMessageErrorNotification(Domain.Model.OmniChannel channel, MultiProductItem item, string errorMessage, SyncPriceMessage data)
        {
            var logger = new LogObjectMicrosoftExtension(Logger, data.LogId)
            {
                Action = "PushErrorProductNotificationMesage",
                RetailerCode = data.RetailerCode,
                RetailerId = data.RetailerId,
                BranchId = data.BranchId,
                OmniChannelId = data.ChannelId,
            };
            var obj = new Infrastructure.EventBus.Event.ErrorProductNotificationMesage(data.LogId)
            {
                RetailerId = channel.RetailerId,
                BranchId = channel.BranchId,
                ChannelId = channel.Id,
                ErrorType = (int)Sdk.Common.SyncTabError.Product,
                ItemSku = item.KvProductSku,
                ItemName = item.ProductName,
                ErrorMessage = errorMessage,
                Type = channel.Type
            };
            _integrationEventService.AddEventWithRoutingKeyAsync(obj, RoutingKey.MessageErrorProductNotification);
            logger.RequestObject = obj.ToJson();
            logger.LogInfo(true);
        }
    }
}
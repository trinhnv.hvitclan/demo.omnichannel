﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.PriceService.Impls
{
    public class ShopeeSyncPriceService : SyncPriceService<ShopeeSyncPriceMessage>
    {
        public ShopeeSyncPriceService(IProductMongoService productMongoService,
            ChannelClient.Impls.ChannelClient channelClient,
            IProductMappingService productMappingService,
            ICacheClient cacheClient,
            IAppSettings settings,
            IDbConnectionFactory dbConnectionFactory,
            IAuditTrailInternalClient auditTrailInternalClient,
            IMessageService messageService,
            KvRedisConfig mqRedisConfig,
            IOmniChannelAuthService channelAuthService,
            IChannelBusiness channelBusiness,
            IOmniChannelPlatformService omniChannelPlatformService,
            ILogger<ShopeeSyncPriceService> logger) : base(productMongoService,
            channelClient,
            productMappingService,
            cacheClient,
            settings,
            dbConnectionFactory,
            auditTrailInternalClient,
            messageService,
            mqRedisConfig,
            channelAuthService,
            channelBusiness,
            omniChannelPlatformService,
            logger)
        {
            ChannelType = ChannelType.Shopee;
        }
    }
}
﻿using Demo.Audit.Model.Message;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.RequestDTO;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Auth;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MultiProductItem = Demo.OmniChannel.ShareKernel.KafkaMessage.MultiProductItem;
using Platform = Demo.OmniChannel.ChannelClient.Models.Platform;

namespace Demo.OmniChannel.PriceService.Impls
{
    public class ShopeeSyncPriceServiceV2 : BaseService<ShopeeSyncPriceMessageV2>
    {
        private readonly IChannelBusiness _channelBusiness;
        private readonly ICacheClient _cacheClient;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly IOmniChannelAuthService _channelAuthService;
        private readonly IOmniChannelPlatformService _omniChannelPlatformService;
        private readonly IProductMongoService _productMongoService;

        public ShopeeSyncPriceServiceV2(
             ILogger<ShopeeSyncPriceServiceV2> logger,
             KvRedisConfig mqRedisConfig,
             IMessageService messageService,
             IChannelBusiness channelBusiness,
             ICacheClient cacheClient,
             IDbConnectionFactory dbConnectionFactory,
             IOmniChannelAuthService channelAuthService,
             IOmniChannelPlatformService omniChannelPlatformService,
             IAuditTrailInternalClient auditTrailInternalClient,
             IProductMappingService productMappingService,
             IProductMongoService productMongoService,
             IAppSettings settings
        ) : base(logger,
            cacheClient,
            productMappingService,
            dbConnectionFactory,
            messageService,
            auditTrailInternalClient)
        {
            var threadSize = mqRedisConfig?.EventMessages.FirstOrDefault(x => !string.IsNullOrEmpty(x.Name) &&
                                                                         x.Name.Contains(GetType()
                                                                             .Name))?.ThreadSize ?? 10;
            messageService.RegisterHandler<ShopeeSyncPriceMessageV2>(m =>
            {
                try
                {
                    m.Options = (int)MessageOption.None;
                    var task = Task.Run(async () => await ProcessMessage(m.GetBody()));
                    task.GetAwaiter().GetResult();
                    return Task.CompletedTask;
                }
                catch (Exception e)
                {
                    ExceptionHelper.WriteLogExceptionMq(typeof(ShopeeSyncPriceMessageV2), m.Body, e);
                    return string.Empty;
                }
            }, threadSize);
            _channelBusiness = channelBusiness;
            _cacheClient = cacheClient;
            _dbConnectionFactory = dbConnectionFactory;
            _channelAuthService = channelAuthService;
            _omniChannelPlatformService = omniChannelPlatformService;
            _productMongoService = productMongoService;
            Settings = settings;
        }

        protected override async Task ProcessMessage(ShopeeSyncPriceMessageV2 data)
        {
            var logger = new LogObjectMicrosoftExtension(Logger, data.LogId)
            {
                Action = "ShopeeMultiSyncPriceV2",
                RetailerCode = data.RetailerCode,
                RetailerId = data.RetailerId,
                BranchId = data.BranchId,
                OmniChannelId = data.ChannelId,
                RequestObject = $"Id: {data.ItemId} Sku:{data.ItemSku} BasePrice:{data.BasePrice} SalePrice: {data.SalePrice} MultiProduct:{data.MultiProducts.ToSafeJson()}",
                ChannelType = data.ChannelType,
                ChannelTypeCode = ((ChannelTypeEnum)data.ChannelType).ToString()
            };

            var channel = await _channelBusiness.GetChannel(data.ChannelId, data.RetailerId, data.LogId);
            if (channel == null)
            {
                logger.LogError(new Exception("Channel is null"));
                return;
            }

            var auth = await _channelAuthService.GetChannelAuth(channel, data.LogId);
            var platform = await _omniChannelPlatformService.GetById(channel.PlatformId);
            if (auth == null)
            {
                logger.LogError(new Exception("Token is Expired"));
                return;
            }

            var authKey = new ChannelAuth
            {
                AccessToken = auth.AccessToken,
                RefreshToken = auth.RefreshToken,
                ShopId = auth.ShopId
            };
            await SyncPrice(data, authKey, channel, logger, platform);

            logger.LogInfo();
        }

        private async Task SyncPrice(SyncPriceMessage data, ChannelAuth authKey,
            Domain.Model.OmniChannel channel, LogObjectMicrosoftExtension syncOnHandLog,
            Platform platform)
        {
            var products = new List<MultiProductItem>();

            data.MultiProducts = data.MultiProducts?.Where(item => item.Price.HasValue).ToList();
            var groupKvProductId = data.MultiProducts?.Where(item => item.Price.HasValue).GroupBy(x => x.KvProductId)
                .Select(x => new MultiProductItem
                {
                    KvProductId = x.Key,
                    Price = x.First().Price,
                    Revision = x.First().Revision,
                    StartSaleDate = x.First().StartSaleDate,
                    EndSaleDate = x.First().EndSaleDate,
                    SalePrice = x.First().SalePrice
                }).ToList();
            if (groupKvProductId == null || !groupKvProductId.Any()) return;

            foreach (var productItem in groupKvProductId)
            {
                products.AddRange(await GenerateProducts(data, productItem));
            }

            var parrent = products.GroupBy(x => x.ParentItemId).Select(x => new { Parrent = x.Key, Child = x.ToList() }).ToList();

            foreach (var item in parrent)
            {
                await SyncMultiPrice(data, item.Parrent, item.Child, authKey, channel, syncOnHandLog, platform);
            }
        }

        private async Task SyncMultiPrice(SyncPriceMessage data,
            string parrentItemId,
            List<MultiProductItem> listProducts,
            ChannelAuth authKey, Domain.Model.OmniChannel channel,
            LogObjectMicrosoftExtension loggerExtension, Platform platform)
        {
            var client = new ChannelClient.Impls.ShopeeClientV2(Settings);
            var connectionStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);
            var context = await ContextHelper.GetExecutionContext(_cacheClient,
              _dbConnectionFactory,
              data.RetailerId,
              connectionStringName,
              data.BranchId,
              Settings?.Get<int>("ExecutionContext"));

            var productsWithGroupParrent = listProducts.ConvertAll(x => x.ConvertTo<ChannelClient.RequestDTO.MultiProductItem>());

            var (isSuccess, message, errorMessage) =
                await client.SyncPriceWithGroupParrent(authKey, parrentItemId, productsWithGroupParrent, loggerExtension, platform);

            var multiProductResponse = !string.IsNullOrEmpty(message)
                          ? JsonConvert.DeserializeObject<MultiProductResponse>(message)
                          : new MultiProductResponse();

            await ProcessResponseData(data, channel, productsWithGroupParrent.Select(x => x.ItemId).ToList(),
                listProducts, multiProductResponse, context, connectionStringName, isSuccess, errorMessage);
        }

        private async Task ProcessResponseData(
            SyncPriceMessage data,
            Domain.Model.OmniChannel channel,
            List<string> batchItemIds,
            List<MultiProductItem> listProducts,
            MultiProductResponse multiProductResponse,
            ExecutionContext context,
            string connectionStringName,
            bool isSuccess,
            string errorMessage)
        {
            var channelProducts =
                        await _productMongoService.GetByProductChannelIds(data.RetailerId, data.ChannelId,
                            batchItemIds, isStringItemId: ConvertHelper.CheckUseStringItemId(channel.Type));
            var logItemSuccess = new List<string>();
            var logItemFail = new List<string>();

            var salePriceName = await GetSalePriceName(data, channel);

            if (!isSuccess)
            {
                if (channelProducts != null && channelProducts.Any())
                {
                    await _productMongoService.UpdateFailedMessageAndStatusCode(channelProducts.Select(p => p.Id).ToList(), 0,
                        errorMessage, SyncErrorType.Price);
                }
                logItemFail.Add($"- {errorMessage}");
                await HandleAuditMessage(channel, data, context, logItemSuccess, logItemFail, null);
                return;
            }

            foreach (var item in multiProductResponse.Product)
            {
                var map = listProducts.FirstOrDefault(p => item.ItemId == p.ItemId);
                var pro = channelProducts.FirstOrDefault(x => x.CommonItemId == item.ItemId);
                if (pro != null && !string.IsNullOrEmpty(item.ErrorMessage))
                {
                    var lstErrorRemoveMsg = GetProductRemoveError(data.ChannelType);
                    lstErrorRemoveMsg = lstErrorRemoveMsg.Select(p => p.Trim().ToLower()).ToList();
                    if (!string.IsNullOrEmpty(item.ErrorMessage) &&
                        lstErrorRemoveMsg.Contains(item.ErrorMessage.Trim().ToLower()))
                    {
                        await _productMongoService.RemoveAsync(pro.Id);

                        PushMessageRemoveProductMapping(context, connectionStringName, data, pro);
                    }
                    else
                    {
                        var err = pro.SyncErrors.FirstOrDefault(x => x.Type == SyncErrorType.Price);
                        if (err != null)
                        {
                            err.Message = StringHelper.ReplaceHexadecimalSymbols(item.ErrorMessage);
                        }
                        else
                        {
                            pro.SyncErrors.Add(new SyncError
                            {
                                Type = SyncErrorType.Price,
                                Message = StringHelper.ReplaceHexadecimalSymbols(item.ErrorMessage)
                            });
                        }
                        pro.SyncStatusCode = null;
                        await _productMongoService.UpdateAsync(pro.Id, pro);
                    }
                    logItemFail.Add(
                        $"- [ProductCode]{map?.KvProductSku}[/ProductCode] KHÔNG thành công: Giá bán: {item.ErrorMessage} <br/>");
                }
                else if (pro != null)
                {
                    pro.SyncErrors.RemoveAll(x => x.Type == SyncErrorType.Price);
                    pro.SyncStatusCode = null;
                    await _productMongoService.UpdateAsync(pro.Id, pro);
                    logItemSuccess.Add(
                        $"- [ProductCode]{map?.KvProductSku}[/ProductCode] thành công: Giá bán: {NumberHelper.NormallizePrice((double)(item.BasePrice))} <br/>");
                    if (item.SalePrice != null && !string.IsNullOrEmpty(salePriceName) && channel.PriceBookId != null &&
                        channel.PriceBookId > 0 && ((double)(item.SalePrice) > 0 &&
                                                    (double)(item.SalePrice) < (double)(item.BasePrice)))
                    {
                        logItemSuccess.Add(
                            $"- [ProductCode]{map?.KvProductSku}[/ProductCode] thành công: Giá khuyến mãi: {NumberHelper.NormallizePrice((double)(item.SalePrice ?? 0))} <br/>");
                    }
                }
            }

            SaveCacheProductsSyncSuccess(listProducts, data.ChannelId);

            await HandleAuditMessage(channel, data, context, logItemSuccess, logItemFail,
                salePriceName);
        }
    }
}
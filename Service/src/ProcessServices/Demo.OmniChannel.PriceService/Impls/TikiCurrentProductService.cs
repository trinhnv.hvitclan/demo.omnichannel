﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Microsoft.Extensions.Logging;
using ServiceStack.Caching;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.PriceService.Impls
{
    public class TikiCurrentProductService : BaseCurrentProductService<TikiCurrentProductMessage>
    {
        public TikiCurrentProductService(ICacheClient cacheClient,
            IMessageService mqService,
            KvRedisConfig mqRedisConfig,
            IPriceBusiness priceBusiness,
            IMappingBusiness mappingBusiness,
            ILogger<TikiCurrentProductService> logger) : base(cacheClient,
            mqService,
            mqRedisConfig,
            priceBusiness,
            mappingBusiness,
            logger)
        {
        }
    }
}
﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Microsoft.Extensions.Logging;
using ServiceStack.Caching;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.PriceService.Impls
{
    public class ShopeeCurrentProductService :BaseCurrentProductService<ShopeeCurrentProductMessage>
    {
        public ShopeeCurrentProductService(ICacheClient cacheClient,
            IMessageService mqService,
            KvRedisConfig mqRedisConfig,
            IPriceBusiness priceBusiness,
            IMappingBusiness mappingBusiness,
            ILogger<ShopeeCurrentProductService> logger) : base(cacheClient,
            mqService,
            mqRedisConfig,
            priceBusiness,
            mappingBusiness,
            logger)
        {
        }
    }
}
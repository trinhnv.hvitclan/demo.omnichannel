﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ServiceStack.Configuration;
using System.Linq;
using Demo.OmniChannel.MongoDb;
using ServiceStack.Caching;
using System;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Repository.Impls;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using Demo.OmniChannel.ChannelClient.Common;
using Newtonsoft.Json;
using ServiceStack.Messaging;
using ServiceStack;
using Demo.Audit.Model.Message;
using System.Text;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;

namespace Demo.OmniChannel.PriceService.Impls
{
    public abstract class BaseService<T> : IHostedService where T : class
    {
        protected readonly ILogger Logger;
        protected IAppSettings Settings;
        protected ICacheClient CacheClient;
        protected readonly IProductMappingService ProductMappingService;
        protected readonly IDbConnectionFactory DbConnectionFactory;
        protected readonly IMessageService MessageService;
        protected readonly IAuditTrailInternalClient _auditTrailInternalClient;

        protected BaseService(
            ILogger logger)
        {
            Logger = logger;
        }
        protected BaseService(
            ILogger logger,
            ICacheClient cacheClient,
            IProductMappingService productMappingService,
            IDbConnectionFactory dbConnectionFactory,
            IMessageService messageService,
             IAuditTrailInternalClient auditTrailInternalClient)
        {
            Logger = logger;
            CacheClient = cacheClient;
            ProductMappingService = productMappingService;
            DbConnectionFactory = dbConnectionFactory;
            MessageService = messageService;
            _auditTrailInternalClient = auditTrailInternalClient;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Logger.LogInformation($"----------------------------------------- Start {typeof(T).Name} Service -----------------------------------------------");
            return Task.CompletedTask;
        }
        protected abstract Task ProcessMessage(T data);
        public Task StopAsync(CancellationToken cancellationToken)
        {

            Logger.LogInformation($"----------------------------------------- Stop {typeof(T).Name} Service -------------------------------------------------");
            return Task.CompletedTask;
        }
        protected int GetSyncMultiPageSize(byte channelType)
        {
            int? pageSize = 20;
            switch (channelType)
            {
                case (byte)ChannelType.Lazada:
                    {
                        pageSize = Settings?.Get<int>("SyncMultiLazadaPageSize");
                        if (pageSize == null || pageSize == 0)
                        {
                            pageSize = 20;
                        }
                        break;
                    }

                case (byte)ChannelType.Shopee:
                    {
                        pageSize = Settings?.Get<int>("SyncMultiShopeePageSize");
                        if (pageSize == null || pageSize == 0)
                        {
                            pageSize = 50;
                        }
                        break;
                    }
                case (byte)ChannelType.Tiki:
                    {
                        pageSize = Settings?.Get<int>("SyncMultiTikiPageSize");
                        if (pageSize == null || pageSize == 0)
                        {
                            pageSize = 50;
                        }
                        break;
                    }
                case (byte)ChannelType.Sendo:
                    {
                        pageSize = Settings?.Get<int>("SyncMultiSendoPageSize");
                        if (pageSize == null || pageSize == 0)
                        {
                            pageSize = 20;
                        }
                        break;
                    }
            }
            return pageSize.Value;
        }
        protected async Task<List<MultiProductItem>> GenerateProducts(SyncPriceMessage data, MultiProductItem productItem)
        {
            var listProductMapping = await GetChannelProducts(data.RetailerId, data.ChannelId, productItem.KvProductId);
            if (listProductMapping == null || !listProductMapping.Any()) return new List<MultiProductItem>();
            var listMultiProductsCombine = (from pro in listProductMapping
                                            select new MultiProductItem
                                            {
                                                ItemId = pro.CommonProductChannelId,
                                                ItemSku = pro.ProductChannelSku,
                                                ParentItemId = pro.CommonParentProductChannelId,
                                                ItemType = pro.ProductChannelType,
                                                Price = productItem.Price,
                                                SalePrice = productItem.SalePrice,
                                                StartSaleDate = productItem.StartSaleDate,
                                                EndSaleDate = productItem.EndSaleDate,
                                                KvProductSku = pro.ProductKvSku,
                                                Revision = productItem.Revision,
                                                KvProductId = pro.ProductKvId,
                                                ProductName = pro.ProductKvFullName
                                            }).ToList();

            return listMultiProductsCombine;
        }
        protected async Task<List<ProductMapping>> GetChannelProducts(long retailerId, long channelId, long productId)
        {
            try
            {
                List<ProductMapping> dataInCache = null;
                if (CacheClient != null)
                {
                    dataInCache = CacheClient.Get<List<ProductMapping>>(string.Format(
                        KvConstant.ListProductMappingCacheKey,
                        retailerId, channelId, productId));
                }

                if (dataInCache != null && dataInCache.Any())
                {
                    return dataInCache;
                }

                var retVal =
                    await ProductMappingService.GetListByKvSingleProductId(retailerId, channelId, productId);

                if (retVal == null)
                {
                    return null;
                }

                CacheClient?.Set(string.Format(KvConstant.ListProductMappingCacheKey, retailerId, channelId, productId), retVal,
                    DateTime.Now.AddDays(1));
                return retVal;
            }
            catch
            {
                return new List<ProductMapping>();
            }
        }
        protected async Task<string> GetSalePriceName(SyncPriceMessage data, Domain.Model.OmniChannel channel)
        {
            var connectStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);
            using var db = DbConnectionFactory.OpenDbConnection(connectStringName.ToLower());
            var priceBookRepository = new PriceBookRepository(db);
            var (_, salePriceBookName) =
                await priceBookRepository.GetChannelPriceBookName(channel);
            return salePriceBookName;
        }
        protected List<string> GetProductRemoveError(byte channelType)
        {
            var lstErrorRemoveMsg = new List<string>();
            switch (channelType)
            {
                case (byte)ChannelType.Lazada:
                    {
                        lstErrorRemoveMsg = Settings.Get<List<string>>("LazadaProductRemoveErrorMsg");
                        if (lstErrorRemoveMsg == null || !lstErrorRemoveMsg.Any())
                        {
                            lstErrorRemoveMsg = new List<string>() { "SELLER_SKU_NOT_FOUND" };
                        }
                        break;
                    }

                case (byte)ChannelType.Shopee:
                    {
                        lstErrorRemoveMsg = Settings.Get<List<string>>("ShopeeProductRemoveErrorMsg");
                        if (lstErrorRemoveMsg == null || !lstErrorRemoveMsg.Any())
                        {
                            lstErrorRemoveMsg = new List<string>() { "You provided an invalid variation id", "Can't edit deleted/invalid items" };
                        }
                        break;
                    }
            }
            return lstErrorRemoveMsg;
        }

        protected void PushMessageRemoveProductMapping(ShareKernel.Auth.ExecutionContext context,
         string connectionStringName, SyncPriceMessage data, MongoDb.Product pro)
        {
            var configMappingV2Feature = Settings?.Get<MappingV2Feature>("MappingV2Feature");
            if (configMappingV2Feature != null && configMappingV2Feature.IsValid(context.Group?.Id ?? 0, data.RetailerId))
            {
                var removeMsg = new RemoveProductMappingMessage
                {
                    RetailerId = context.RetailerId,
                    BranchId = context.BranchId,
                    ChannelIds = new List<long> { data.ChannelId },
                    ChannelProductIds = new List<string> { pro.CommonItemId },
                    ConnectStringName = connectionStringName,
                    IsDeleteKvProduct = false,
                    LogId = data.LogId,
                    MessageFrom = "SyncPriceService"
                };
                var kafkaMessage = new KafkaMessage(configMappingV2Feature.TopicRemoveMappingRequestName,
                    $"{removeMsg.RetailerId}_{removeMsg.BranchId}_{data.ChannelId}", removeMsg);
                KafkaClient.KafkaClient.Instance.PublishMessage(kafkaMessage.TopicName, kafkaMessage.Key, JsonConvert.SerializeObject(kafkaMessage), isV2: true);
            }
            else
            {
                using var mq = MessageService.CreateMessageProducer();
                var removeMsg = new RemoveProductMappingMessage
                {
                    RetailerId = context.RetailerId,
                    BranchId = context.BranchId,
                    ChannelIds = new List<long> { data.ChannelId },
                    ChannelProductIds = new List<string> { pro.CommonItemId },
                    ConnectStringName = connectionStringName,
                    IsDeleteKvProduct = false,
                    LogId = data.LogId
                };
                mq.Publish(removeMsg);
            }
        }

        /// <summary>
        /// Lưu cache theo Revision (Luồng resync product sẽ check nếu đồng bộ rồi sẽ không đồng bộ lại)
        /// </summary>
        /// <param name="listProducts"></param>
        /// <param name="channelId"></param>
        protected void SaveCacheProductsSyncSuccess(List<MultiProductItem> listProducts,
            long channelId)
        {
            foreach (var kvProduct in listProducts)
            {
                CacheClient.Set(KvConstant.KvPriceRevision.FormatWith(channelId, kvProduct.KvProductId),
                    new PriceDto { KvProductId = kvProduct.KvProductId, BasePrice = kvProduct.Price, Revision = kvProduct.Revision },
                    TimeSpan.FromDays(Settings?.Get<int>("RevisionProductExpired", 1) ?? 1));
            }
        }

        protected async Task HandleAuditMessage(Domain.Model.OmniChannel channel,
            SyncPriceMessage data,
           ShareKernel.Auth.ExecutionContext executionContext,
            List<string> logItemSuccess,
            List<string> logItemFail,
            string salePriceName)
        {
            var coreContext = executionContext.ConvertTo<OmniChannelCore.Api.Sdk.Common.KvInternalContext>();
            coreContext.UserId = executionContext.User?.Id ?? 0;

            var log = new AuditTrailLog
            {
                FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channel.Type),
                Action = (int)AuditTrailAction.ProductIntergate,
                CreatedDate = DateTime.Now,
                BranchId = data.BranchId
            };
            var typeName = EnumHelper.GetNameByType<ChannelType>(channel.Type);
            var contentLog = new StringBuilder($"Đồng bộ thông tin giá bán hàng hóa: {typeName} shop:" +
                $" {channel.Email ?? channel.Name}, Bảng giá bán: {data.PriceBookName}");

            contentLog.Append(!string.IsNullOrEmpty(salePriceName)
                ? $", Bảng giá khuyến mãi: {salePriceName} <br/>"
                : "<br/>");
            contentLog.Append($"{string.Join("", logItemSuccess)}{string.Join("", logItemFail)}");

            log.Content = contentLog.ToString();
            await _auditTrailInternalClient.AddLogAsync(coreContext, log);
        }
    }
}
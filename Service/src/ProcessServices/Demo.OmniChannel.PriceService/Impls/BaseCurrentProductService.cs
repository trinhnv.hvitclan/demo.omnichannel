﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.PriceService.Impls
{
    public class BaseCurrentProductService<T> : BaseService<T> where T : CurrentProductMessage
    {
        private readonly ICacheClient _cacheClient;
        private readonly IPriceBusiness _priceBusiness;
        private readonly IMappingBusiness _mappingBusiness;
        public BaseCurrentProductService(
            ICacheClient cacheClient,
            IMessageService mqService,
            KvRedisConfig mqRedisConfig,
            IPriceBusiness priceBusiness,
            IMappingBusiness mappingBusiness,
            ILogger logger
            ) : base(logger)
        {

            var threadSize = mqRedisConfig?.EventMessages.FirstOrDefault(x => !string.IsNullOrEmpty(x.Name) &&
                                                                              x.Name.Contains(this.GetType()
                                                                                  .Name))?.ThreadSize ?? 10;
            mqService.RegisterHandler<T>(m =>
            {
                try
                {
                    m.Options = (int)MessageOption.None;
                    var task = Task.Run(async () => await ProcessMessage(m.GetBody()));
                    task.Wait();
                    return Task.CompletedTask;
                }
                catch (Exception e)
                {
                    Logger.LogError(e, e.Message);
                    return string.Empty;
                }
            }, threadSize);
            _cacheClient = cacheClient;
            _priceBusiness = priceBusiness;
            _mappingBusiness = mappingBusiness;
        }
        protected override async Task ProcessMessage(T data)
        {
            var loggerExtension = new LogObjectMicrosoftExtension(Logger, data.LogId)
            {
                Action = 
                    $"{nameof(BaseCurrentProductService<T>)}",
                OmniChannelId = data.ChannelId,
                RetailerId = data.RetailerId,
                BranchId = data.BranchId,
                ChannelType = data.ChannelType,
                ChannelTypeCode = ((ChannelTypeEnum)data.ChannelType).ToString(),
            };
            if (data.Products == null || !data.Products.Any())
            {
                loggerExtension.LogInfo();
                return;
            }
            var kvProductIds = data.Products.Select(p => p.KvProductId).ToList();
            var productMaps = await _mappingBusiness.GetMappingsByProductIds(data, kvProductIds);
            if (productMaps == null || !productMaps.Any())
            {
                loggerExtension.LogInfo();
                return;
            }

            var channel = new Domain.Model.OmniChannel
            {
                Id = data.ChannelId,
                Name = data.ChannelName,
                Type = data.ChannelType,
                RetailerId = data.RetailerId,
                BranchId = data.BranchId,
                PriceBookId = data.PriceBookId,
                BasePriceBookId = data.BasePriceBookId
            };
            var mapIds = productMaps.Select(p => p.ProductKvId).ToList();
            var kvProductWithMaps = data.Products.Where(p => mapIds.Contains(p.KvProductId)).ToList();
            var lstRedisKey = mapIds
                .Select(p => KvConstant.KvPriceRevision.FormatWith(data.ChannelId, p)).ToList();
            var currentSyncPrices = ((IKvCacheClient)_cacheClient).GetValues<PriceDto>(lstRedisKey);
            List<ProductMapping> reSyncPrices = new List<ProductMapping>();
            foreach (var item in kvProductWithMaps)
            {
                var currentProduct = currentSyncPrices.FirstOrDefault(x => x.KvProductId == item.KvProductId);
                var currentMapping = productMaps.FirstOrDefault(x => x.ProductKvId == item.KvProductId);
                if (currentMapping == null)
                {
                    continue;
                }

                if (currentProduct == null)
                {
                    reSyncPrices.Add(currentMapping);
                    continue;
                }
                if (System.Collections.StructuralComparisons.StructuralComparer.Compare(item.Revision,
                        currentProduct.Revision) > 0)
                {
                    reSyncPrices.Add(currentMapping);
                }

            }
            if (!reSyncPrices.Any())
            {
                return;
            }

            await _priceBusiness.SyncMultiPrice(data.ConnectStringName, channel,
                reSyncPrices.Select(p => p.CommonProductChannelId).ToList(), reSyncPrices.Select(p => p.ProductKvId).ToList(),
                false, data.LogId, true, GetSyncMultiPageSize(data.ChannelType), reSyncPrices, false, "Queue created from CurrentProductService run backup price");
            loggerExtension.TotalItem = reSyncPrices.Count;
            loggerExtension.LogInfo(true);
        }
    }
}
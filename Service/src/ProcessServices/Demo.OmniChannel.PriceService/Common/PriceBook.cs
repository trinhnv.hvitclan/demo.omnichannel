﻿using System;

namespace Demo.OmniChannel.PriceService.Common
{
    public class PriceBookDTO
    {
        public decimal? BasePrice { get; set; }
        public decimal? SalePrice { get; set; }
        public DateTime? StartSaleTime { get; set; }
        public DateTime? EndSaleTime { get; set; }
        public string PriceBookName { get; set; }
        public string SalePriceBookName { get; set; }
    }
}
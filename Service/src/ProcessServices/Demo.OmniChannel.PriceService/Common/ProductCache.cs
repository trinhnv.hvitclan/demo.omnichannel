﻿namespace Demo.OmniChannel.PriceService.Common
{
    public class ProductCache
    {
        public decimal BasePrice { get; set; }
        public decimal SalePrice { get; set; }
    }
}
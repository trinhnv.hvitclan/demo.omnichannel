﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Infrastructure.CronJob;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.PriceService.Jobs
{
    public class MonitorJob : CronJobService
    {
        private readonly ILogger<MonitorJob> _logger;
        private readonly IMessageService _messageService;
        public MonitorJob(
            IScheduleConfig<MonitorJob> config, 
            IMessageService messageService, 
            ILogger<MonitorJob> logger) : base(config.CronExpression, config.TimeZoneInfo)
        {
            _messageService = messageService;
            _logger = logger;
        }
        public override Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("MonitorJob starts.");
            return base.StartAsync(cancellationToken);
        }

        public override Task DoWork(CancellationToken cancellationToken)
        {
            var loggerExtension = new LogObjectMicrosoftExtension(_logger, Guid.NewGuid())
            {
                Action = "HealthCheck"
            };
            IMessageHandlerStats statsMap = _messageService.GetStats();
            loggerExtension.Description = $"{_messageService.GetStatus()} - {statsMap.TotalNormalMessagesReceived} - {statsMap.TotalMessagesProcessed}";
            _logger.LogInformation("HealthCheck");
            return Task.CompletedTask;
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("MonitorJob stopping.");
            return base.StopAsync(cancellationToken);
        }
    }
}

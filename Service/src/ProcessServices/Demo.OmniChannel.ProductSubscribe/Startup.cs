﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Demo.OmniChannel.Business;
using Demo.OmniChannel.Infrastructure.IoC;
using Demo.OmniChannel.Infrastructure.Logging;
using Demo.OmniChannel.MongoService.Common;
using Demo.OmniChannel.ProductSubscribe.Impls;
using Demo.OmniChannel.Sdk.Impls;
using Demo.OmniChannel.Sdk.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using ServiceStack.Messaging;
using ServiceStack.Messaging.Redis;
using Demo.OmniChannel.ProductSubscribe.Impls.RedisMq;
using Serilog;
using ServiceStack.Caching;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Demo.OmniChannelCore.Api.Sdk.Impls;
using Demo.OmniChannel.ProductSubscribe.Impls.Proxy;
using Demo.OmniChannel.ProductSubscribe.Impls.RabbitMq;
using Demo.OmniChannel.DCControl.ConfigureService;
using Demo.OmniChannel.Infrastructure.EventBus.Extentions;
using Demo.OmniChannel.ProductSubscribe.Extensions;

namespace Demo.OmniChannel.ProductSubscribe
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            var builder = new ConfigurationBuilder()
                .AddConfiguration(configuration)
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            #region Register ServiceStack License
            Licensing.RegisterLicense(Configuration.GetSection("servicestack:license").Value);
            #endregion register
            services.AddSingleton<IAppSettings>(x => new NetCoreAppSettings(Configuration));
            #region Register kafka
            var kafka = new Kafka();
            Configuration.GetSection("Kafka").Bind(kafka);
            KafkaClient.KafkaClient.Instance.SetKafkaConsumerConfig(kafka);
            KafkaClient.KafkaClient.Instance.SetKafkaProducerConfig(kafka);
            KafkaClient.KafkaClient.Instance.InitProducer();
            var kafka2 = new Kafka();
            Configuration.GetSection("Kafka2").Bind(kafka2);
            KafkaClient.KafkaClient.Instance.SetKafkaProducerConfig(kafka2, true);
            KafkaClient.KafkaClient.Instance.InitProducer(true);
            #endregion

            #region Register Serilog
            Configuration.ConfigLog(GetType(), "Demo.ommichannel.productsubscribe is started.");
            #endregion
            var isProxyMode = Configuration.GetSection("IsProxyMode").Get<bool>();
            if (isProxyMode)
            {
                services.AddHostedService<ProxySyncEsSubscribeService>();
                services.AddHostedService<ProxyPriceBookSyncEsSubcribeService>();
            }
            else
            {
                #region Redis
                var redisConfig = new KvRedisConfig();
                Configuration.GetSection("redis:cache").Bind(redisConfig);
                services.AddSingleton(sc => KvRedisPoolManager.GetClientsManager(redisConfig));
                services.AddSingleton<ICacheClient, KvCacheClient>();
                services.AddSingleton<IKvLockRedis, KvLockRedis>();

                var redisMqConfig = new KvRedisConfig();
                Configuration.GetSection("redis:message").Bind(redisMqConfig);
                services.AddSingleton(redisMqConfig);
                var redisFactory = KvRedisPoolManager.GetClientsManager(redisMqConfig);
                var mqHost = new RedisMqServer(redisFactory, retryCount: 2);
                services.AddSingleton<IMessageFactory>(c => new RedisMessageFactory(redisFactory));
                services.AddSingleton<IMessageService>(mqHost);

                #endregion
                #region Register DatabaseConnectionString
                var kvSql = new KvSqlConnectString();
                Configuration.GetSection("SqlConnectStrings").Bind(kvSql);
                var connectFactory = new OrmLiteConnectionFactory(kvSql.KvChannelEntities, SqlServer2016Dialect.Provider);
                connectFactory.RegisterConnection(nameof(kvSql.KvMasterEntities), kvSql.KvMasterEntities, SqlServer2016Dialect.Provider);
                foreach (var item in kvSql.KvEntitiesDC1)
                {
                    connectFactory.RegisterConnection(item.Name.ToLower(), item.Value, SqlServer2016Dialect.Provider);
                }
                OrmLiteConfig.DialectProvider.GetStringConverter().UseUnicode = true;
                services.AddSingleton(kvSql);
                services.AddSingleton<IDbConnectionFactory>(c => connectFactory);
                #endregion
                #region Register Mongo
                services.Configure<MongoDbSettings>(Configuration.GetSection("mongodb"));
                services.UsingMongoDb(Configuration);
                #endregion
                services.Configure<List<KvChannelConfig>>(Configuration.GetSection("Channel"));
                //services.AddTransient<SyncEsSubscribeService>();
                //services.AddHostedService<SyncEsSubscribeService>();
                //services.AddHostedService<SyncEsSubscribeServiceDC2>();
                // Register Business
                services.AddScoped<IPriceBusiness, PriceBusiness>();
                services.AddScoped<IOnHandBusiness, OnHandBusiness>();
                services.AddScoped<IMappingBusiness, MappingBusiness>();
                services.AddScoped<IChannelBusiness, ChannelBusiness>();
                services.AddScoped<IProductMappingInternalClient, ProductMappingInternalClient>();
                services.AddScoped<IAuditTrailInternalClient>(c => new AuditTrailInternalClient(c.GetService<IAppSettings>()));

                if (kafka.AllTopics.Any())
                {
                    var lsName = kafka.AllTopics.Where(t => t.IsActive).Select(v => v.ServiceName);
                    var types = typeof(BaseService<>).GetTypeInfo().Assembly.DefinedTypes
                        .Where(p => p.GetTypeInfo().IsAssignableFrom(p.AsType()) && p.IsClass && lsName.Contains(p.Name)).Select(p => p.AsType());
                    try
                    {
                        foreach (var type in types)
                        {
                            services.AddTransient(typeof(IHostedService), type);
                        }
                    }
                    catch (Exception e)
                    {
                        Log.Logger.Error($"Register {nameof(IHostedService)} error: {e.Message}", e);
                    }
                }

                if (redisMqConfig.EventMessages.Any())
                {

                    var lsName = redisMqConfig.EventMessages.Where(t => t.IsActive).Select(v => v.Name);
                    var types = typeof(BaseRedisService<>).GetTypeInfo().Assembly.DefinedTypes
                        .Where(p => p.GetTypeInfo().IsAssignableFrom(p.AsType()) && p.IsClass && lsName.Contains(p.Name)).Select(p => p.AsType());
                    try
                    {
                        foreach (var type in types)
                        {

                            services.AddTransient(type);

                        }
                    }
                    catch (Exception e)
                    {
                        Log.Logger.Error(e.Message, e);
                        throw;
                    }
                }
                #region RabbitMQ Service 
                services.AddHostedService<SyncEsDeadLetterQueueService>();
                #endregion RabbitMQ Service
                #region Register RabbitMQ
                services.AddConfigEventBus(Configuration);
                services.AddEventBus();
                services.AddEventHandlers();
                services.AddProcess();
                services.ConsumerService();
                #endregion
            }

            services.RegisterApplicationDcControl(new ApplicationDcInputOptions
            {
                ServiceName = "ProductSubcribeService",
                AppSettings = new NetCoreAppSettings(Configuration)
            });
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime lifetime, IAppSettings settings)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            var isProxyMode = Configuration.GetSection("IsProxyMode").Get<bool>();
            if (!isProxyMode)
            {
                var messageService = app.ApplicationServices.GetRequiredService<IMessageService>();
                var eventRegister = settings.Get<KvRedisConfig>("redis:message").EventMessages;

                if (eventRegister != null && eventRegister.Any(x => x.Name.Equals(nameof(ProcessRemoveService)) && x.IsActive))
                {
                    messageService.RegisterHandler<ProcessRemoveMessage<Domain.Model.ProductSubscribe>>(m =>
                    {
                        using (var scope = app.ApplicationServices.CreateScope())
                        {
                            var task = Task.Run(async () =>
                            {
                                var svc = scope.ServiceProvider.GetService<ProcessRemoveService>();
                                m.Options = (int)MessageOption.None;
                                await svc.ProcessMessage(m.GetBody());
                            });
                            task.Wait();
                            return null;
                        }


                    }, (messageHandler, message, ex) => ExceptionHelper.WriteLogExceptionMq(messageHandler.MessageType, message.Body, ex),
                        eventRegister.FirstOrDefault(x => x.Name.Equals(nameof(ProcessRemoveService)) && x.IsActive)
                            ?.ThreadSize ?? 1);
                }

                if (eventRegister != null && eventRegister.Any(x => x.Name.Equals(nameof(ProcessStockService)) && x.IsActive))
                {
                    messageService.RegisterHandler<ProcessStockMessage<Domain.Model.ProductSubscribe>>(m =>
                    {
                        using (var scope = app.ApplicationServices.CreateScope())
                        {
                            var task = Task.Run(async () =>
                            {
                                var svc = scope.ServiceProvider.GetService<ProcessStockService>();
                                m.Options = (int)MessageOption.None;
                                await svc.ProcessMessage(m.GetBody());
                            });
                            task.Wait();
                            return null;
                        }

                    }, (messageHandler, message, ex) => ExceptionHelper.WriteLogExceptionMq(messageHandler.MessageType, message.Body, ex),
                        eventRegister.FirstOrDefault(x => x.Name.Equals(nameof(ProcessStockService)) && x.IsActive)
                            ?.ThreadSize ?? 1);
                }

                if (eventRegister != null && eventRegister.Any(x => x.Name.Equals(nameof(ProcessPriceService)) && x.IsActive))
                {
                    messageService.RegisterHandler<ProcessPriceMessage<Domain.Model.ProductSubscribe>>(m =>
                    {
                        using (var scope = app.ApplicationServices.CreateScope())
                        {
                            var task = Task.Run(async () =>
                            {
                                var svc = scope.ServiceProvider.GetService<ProcessPriceService>();
                                m.Options = (int)MessageOption.None;
                                await svc.ProcessMessage(m.GetBody());
                            });
                            task.Wait();
                            return null;
                        }

                    }, (messageHandler, message, ex) => ExceptionHelper.WriteLogExceptionMq(messageHandler.MessageType, message.Body, ex),
                        eventRegister.FirstOrDefault(x => x.Name.Equals(nameof(ProcessPriceService)) && x.IsActive)
                            ?.ThreadSize ?? 1);
                }
                lifetime.ApplicationStarted.Register(messageService.Start);
            }
        }
    }
}
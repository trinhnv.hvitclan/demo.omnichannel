﻿using Demo.OmniChannel.Infrastructure.EventBus.Abstractions;
using Demo.OmniChannel.Infrastructure.EventBus.Event;
using Microsoft.Extensions.Hosting;
using System.Threading.Tasks;
using System.Threading;

namespace Demo.OmniChannel.ProductSubscribe.Consumer
{
    public class SyncOnHandConsumerService : BackgroundService
    {
        private readonly IEventBus _eventBus;

        public SyncOnHandConsumerService(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _eventBus.Subscribe<OmniSyncOnhandEvent, IIntegrationEventHandler<OmniSyncOnhandEvent>>();
            return Task.CompletedTask;
        }
    }
}

﻿using Demo.OmniChannel.ShareKernel.Common;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.Redis;
using System.Linq;

namespace Demo.OmniChannel.ProductSubscribe.Impls
{
    public class PriceBookSyncEsSubcribeDC2Service : PriceBookSyncEsSubscribeService
    {
        public PriceBookSyncEsSubcribeDC2Service(IAppSettings settings,
            IMessageService messageService,
            IRedisClientsManager redisClientsManager,
            IDbConnectionFactory dbConnectionFactory) : base(settings, messageService, redisClientsManager,
            dbConnectionFactory)
        {
            TopicName = $"{Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.PriceBookSyncEsDc2Message)?.Name}";
            ConsumerSize = Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.PriceBookSyncEsDc2Message)?.Size ?? 1;
            MessageService = messageService;
        }
    }
}

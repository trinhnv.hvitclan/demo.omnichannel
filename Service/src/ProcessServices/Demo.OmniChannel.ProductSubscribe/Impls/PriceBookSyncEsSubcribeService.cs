﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.Redis;

namespace Demo.OmniChannel.ProductSubscribe.Impls
{
    public class PriceBookSyncEsSubscribeService : BaseService<PriceBookSyncEsMessage<Domain.Model.ProductSubscribe>>
    {
        protected IMessageService MessageService;
        public PriceBookSyncEsSubscribeService(IAppSettings settings,
            IMessageService messageService,
            IRedisClientsManager redisClientsManager,
            IDbConnectionFactory dbConnectionFactory) : base(redisClientsManager,
            dbConnectionFactory, settings)
        {
            TopicName = $"{Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.PriceBookSyncEsMessage)?.Name}";
            ConsumerSize = Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.PriceBookSyncEsMessage)?.Size ?? 1;
            MessageService = messageService;
        }

        protected override async Task<bool> ProcessMessage(PriceBookSyncEsMessage<Domain.Model.ProductSubscribe> data, bool isWriteLog = false)
        {
            var logId = Guid.NewGuid();
            var logProcessSyncEsMessage = new LogObject(Logger, logId)
            {
                Action = "PriceBookSyncEsSubscribe",
                RetailerId = data.RetailerId,
                RequestObject = data?.ToSafeJson()
            };

            LoggingHelper.WriteLogDataReceived(Settings, Logger, "PriceBookSyncEsSubscribe.ProcessMessage", data.RetailerId, data);
            isWriteLog = Settings?.Get<bool?>("WriteLogProcessMessage") ?? false;
            if (await base.ProcessMessage(data, isWriteLog)) return false;
            switch (data.EventName)
            {
                case EsEventName.AddPriceBook:
                case EsEventName.UpdatePriceBook:
                    {
                        if (data.PriceBookId <= 0)
                        {
                            return false;
                        }

                        if (data.Products == null || !data.Products.Any())
                        {
                            return false;
                        }
                        var updatePriceMessage = new ProcessPriceMessage<Domain.Model.ProductSubscribe>
                        {
                            RetailerId = data.RetailerId,
                            BranchId = data.BranchId,
                            SentTime = data.SentTime,
                            Products = data.Products,
                            ConnectionString = data.ConnectionString,
                            PriceBookId = data.PriceBookId,
                            LogId = logId,
                            IsFromPriceBook = true
                        };
                        using (var msg = MessageService.MessageFactory.CreateMessageProducer())
                        {
                            msg.Publish(updatePriceMessage);
                        }
                        break;
                    }

                case EsEventName.DeletePriceBook:
                    {
                        if (data.PriceBookId <= 0)
                        {
                            return false;
                        }
                        if (data.Products == null || !data.Products.Any())
                        {
                            return false;
                        }
                        var updatePriceMessage = new ProcessPriceMessage<Domain.Model.ProductSubscribe>
                        {
                            RetailerId = data.RetailerId,
                            BranchId = data.BranchId,
                            SentTime = data.SentTime,
                            Products = data.Products,
                            ConnectionString = data.ConnectionString,
                            PriceBookId = data.PriceBookId,
                            LogId = logId,
                            IsFromPriceBook = true
                        };
                        using (var msg = MessageService.MessageFactory.CreateMessageProducer())
                        {
                            msg.Publish(updatePriceMessage);
                        }
                        break;
                    }
            }

            if (isWriteLog)
            {
                logProcessSyncEsMessage.LogInfo();
            }
            return true;
        }
    }
}
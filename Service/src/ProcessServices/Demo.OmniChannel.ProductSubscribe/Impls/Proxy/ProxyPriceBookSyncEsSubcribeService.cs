﻿using Demo.OmniChannel.Sdk.Impls;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using ServiceStack.Configuration;
using ServiceStack.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.ProductSubscribe.Impls.Proxy
{
    public class ProxyPriceBookSyncEsSubcribeService : BaseService<PriceBookSyncEsMessage<Domain.Model.ProductSubscribe>>
    {

        private ILog _log => LogManager.GetLogger(typeof(ProxySyncEsSubscribeService));
        public ProxyPriceBookSyncEsSubcribeService(IAppSettings settings) : base(settings)
        {
            TopicName = $"{Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.PriceBookSyncEsMessage)?.Name}";
            ConsumerSize = Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.PriceBookSyncEsMessage)?.Size ?? 1;
        }
        protected override async Task<bool> ProcessMessage(PriceBookSyncEsMessage<Domain.Model.ProductSubscribe> data, bool isWriteLog = false)
        {
            using (var client = new PriceBookInternalClient(Settings))
            {
                var request = await client.ListenProxyPriceBookSubscribe(data);
                if (request != null)
                {
                    _log.Info($"Send request ListenProxyPriceBookSubscribe to {request.ResponseUri?.AbsolutePath} {request.ResponseStatus} {request.Content}");
                }
            }

            return true;
        }


    }
}

﻿using Demo.OmniChannel.Sdk.Impls;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using ServiceStack.Configuration;
using ServiceStack.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.ProductSubscribe.Impls
{
    public class ProxySyncEsSubscribeService : BaseService<SyncEsMessage<Domain.Model.ProductSubscribe>>
    {
        private ILog _log => LogManager.GetLogger(typeof(ProxySyncEsSubscribeService));
        public ProxySyncEsSubscribeService(IAppSettings settings): base(settings)
        {
            Kafka = KafkaClient.KafkaClient.Instance.GetKafka();
            TopicName = $"{Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.SyncEsMessage)?.Name}";
            ConsumerSize = Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.SyncEsMessage)?.Size ??
                           1;
        }
        protected override async Task<bool> ProcessMessage(SyncEsMessage<Domain.Model.ProductSubscribe> data, bool isWriteLog = false)
        {
            using (var client = new ChannelProductInternalClient(Settings))
            {
                var request = await client.ListenProxyProductSubscribe(data);
                if (request != null)
                {
                    _log.Info($"Send request ListenProxyProductSubscribe to {request.ResponseUri?.AbsolutePath} {request.ResponseStatus} {request.Content}");
                }
            }

            return true;
        }
    }
}
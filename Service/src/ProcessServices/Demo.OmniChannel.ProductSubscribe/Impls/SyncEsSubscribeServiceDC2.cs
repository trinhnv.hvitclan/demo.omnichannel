﻿using System.Linq;
using Demo.OmniChannel.ShareKernel.Common;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.Redis;

namespace Demo.OmniChannel.ProductSubscribe.Impls
{
    public class SyncEsSubscribeServiceDC2 : SyncEsSubscribeService
    {
        public SyncEsSubscribeServiceDC2(IAppSettings settings,
            IDbConnectionFactory dbConnectionFactory,
            IMessageService messageService,
            IRedisClientsManager redisClientsManager)
            : base(settings,dbConnectionFactory,
                messageService,
                redisClientsManager)
        {
            Kafka = KafkaClient.KafkaClient.Instance.GetKafka();
            TopicName = $"{Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.SyncEsDc2Message)?.Name}";
            ConsumerSize = Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.SyncEsDc2Message)?.Size ??
                           1;
            Settings = settings;
            DbConnectionFactory = dbConnectionFactory;
            IsFromDr = true;
        }
    }
}
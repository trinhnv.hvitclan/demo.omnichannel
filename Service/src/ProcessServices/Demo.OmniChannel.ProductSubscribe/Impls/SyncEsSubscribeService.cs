﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.Redis;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.ProductSubscribe.Impls
{
    public class SyncEsSubscribeService : BaseService<SyncEsMessage<Domain.Model.ProductSubscribe>>
    {
        protected bool IsFromDr;
        protected IMessageService MessageService;

        public SyncEsSubscribeService(IAppSettings settings,
            IDbConnectionFactory dbConnectionFactory,
            IMessageService messageService,
            IRedisClientsManager redisClientsManager) : base(redisClientsManager, dbConnectionFactory, settings)
        {
            TopicName = $"{Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.SyncEsMessage)?.Name}";
            ConsumerSize = Kafka.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.SyncEsMessage)?.Size ?? 1;
            DbConnectionFactory = dbConnectionFactory;
            IsFromDr = false;
            MessageService = messageService;
        }
        protected override async Task<bool> ProcessMessage(SyncEsMessage<Domain.Model.ProductSubscribe> data, bool isWriteLog = false)
        {
            var logId = Guid.NewGuid();
            var logProcessSyncEsMessage = new LogObject(Logger, logId)
            {
                Action = "SyncEsSubscribeService",
                RetailerId = data.RetailerId,
                RequestObject = data.ToSafeJson()
            };
            LoggingHelper.WriteLogDataReceived(Settings, Logger, "ProductSubscribeService.ProcessMessage", data.RetailerId, data);
            isWriteLog = Settings?.Get<bool?>("WriteLogProcessMessage") ?? false;
            if (await base.ProcessMessage(data, isWriteLog)) return false;
            data.ConnectionString = await GetConnectionString(data.RetailerId);
            using (var client = RedisClientsManager.GetClient())
            {
                var configMappingV2Feature = Settings.Get<MappingV2Feature>("MappingV2Feature");
                var coreContext = await ContextHelper.GetExecutionContext(client, DbConnectionFactory, data.RetailerId,
                    data.ConnectionString, data.BranchId, Settings?.Get<int>("ExecutionContext"));
                if (configMappingV2Feature.IsValid(coreContext.Group?.Id ?? 0, data.RetailerId))
                {
                    logProcessSyncEsMessage.Description = $"Retailer {data.RetailerId} will use mapping v2. Exit...";
                    logProcessSyncEsMessage.LogInfo();
                    return true;
                }
                logProcessSyncEsMessage.Description = $"Retailer {data.RetailerId} will use mapping v1. Continue...";
                logProcessSyncEsMessage.LogInfo();
            }
            switch (data.Type)
            {
                case (byte)EsEventType.Product:
                    {
                        if (data.Advice == (byte)TrackingAdvice.Remove)
                        {
                            if (!string.IsNullOrEmpty(data.Ids))
                            {
                                var productIds = data.Ids.Split(",").Select(long.Parse).ToList();
                                var removeMessage = new ProcessRemoveMessage<Domain.Model.ProductSubscribe>
                                {
                                    RetailerId = data.RetailerId,
                                    BranchId = data.BranchId,
                                    SentTime = data.SentTime,
                                    ProductIds = productIds,
                                    ConnectionString = data.ConnectionString,
                                    LogId = logId
                                };
                                using (var msg = MessageService.MessageFactory.CreateMessageProducer())
                                {
                                    msg.Publish(removeMessage);
                                }
                            }
                            break;
                        }
                        if (data.EventName == EsEventName.UpdateStock)
                        {
                            var stockUpdateMessage = new ProcessStockMessage<Domain.Model.ProductSubscribe>
                            {
                                RetailerId = data.RetailerId,
                                BranchId = data.BranchId,
                                SentTime = data.SentTime,
                                Products = data.Products,
                                ConnectionString = data.ConnectionString,
                                LogId = logId
                            };
                            using (var msg = MessageService.MessageFactory.CreateMessageProducer())
                            {
                                msg.Publish(stockUpdateMessage);
                            }
                            break;
                        }

                        var updatePriceMessage = new ProcessPriceMessage<Domain.Model.ProductSubscribe>
                        {
                            RetailerId = data.RetailerId,
                            BranchId = data.BranchId,
                            SentTime = data.SentTime,
                            Products = data.Products,
                            PriceBookId = 0,
                            ConnectionString = data.ConnectionString,
                            LogId = logId,
                            IsFromPriceBook = false
                        };
                        using (var msg = MessageService.MessageFactory.CreateMessageProducer())
                        {
                            msg.Publish(updatePriceMessage);
                        }
                        break;
                    }

            }
            if (isWriteLog)
            {
                logProcessSyncEsMessage.LogInfo();
            }
            return true;
        }

        //private void MappingSubscribe(SyncEsMessage<Domain.Model.ProductSubscribe> data)
        //{
        //    var message = new ProductMappingTrackingMessage
        //    {
        //        KvEntities = data.ConnectionString,
        //        BranchId = data.BranchId,
        //        RetailerId = data.RetailerId
        //    };

        //    if (data.Advice == (byte)TrackingAdvice.Remove)
        //    {
        //        message.Ids = data.Ids;
        //        message.IsDeleteMapping = true;
        //    }
        //    else
        //    {
        //        if (data.Products == null || !data.Products.Any())
        //        {
        //            return;
        //        }
        //        var kvProducts = new List<KvProduct>();
        //        foreach (var item in data.Products)
        //        {
        //            var p = new KvProduct
        //            {
        //                KvProductId = item.Id,
        //                KvProductSku = item.Code,
        //                KvProductFullName = item.FullName
        //            };
        //            if (item.ProductBranch != null && item.ProductBranch.Any())
        //            {
        //                p.KvInActiveBranchIds = item.ProductBranch.Where(x => x.IsActive == false).Select(x => x.BranchId).ToList();
        //            }
        //            kvProducts.Add(p);
        //        }

        //        message.KvProducts = kvProducts;
        //    }

        //    using (var msg = MessageService.MessageFactory.CreateMessageProducer())
        //    {
        //        msg.Publish(message);
        //    }
        //}
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Confluent.Kafka;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Microsoft.Extensions.Hosting;
using Polly;
using Polly.Retry;
using Polly.Timeout;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.OrmLite;
using ServiceStack.Redis;
using Demo.OmniChannel.Domain.Model;

namespace Demo.OmniChannel.ProductSubscribe.Impls
{
    public abstract class BaseService<T> : IHostedService where T : BaseRedisMessage
    {
        protected ILog Logger => LogManager.GetLogger(typeof(BaseService<T>));
        protected string TopicName { get; set; }
        protected int ConsumerSize { get; set; }
        protected IAppSettings Settings;
        protected Kafka Kafka;
        private readonly List<Task> _listTask = new List<Task>();
        private readonly CancellationTokenSource _stoppingCts = new CancellationTokenSource();
        protected IRedisClientsManager RedisClientsManager;
        protected IDbConnectionFactory DbConnectionFactory;
        protected BaseService(IRedisClientsManager redisClientsManager, IDbConnectionFactory dbConnectionFactory, IAppSettings settings)
        {
            Kafka = KafkaClient.KafkaClient.Instance.GetKafka();
            RedisClientsManager = redisClientsManager;
            DbConnectionFactory = dbConnectionFactory;
            Settings = settings;
        }

        protected BaseService(IAppSettings settings)
        {
            Kafka = KafkaClient.KafkaClient.Instance.GetKafka();
            Settings = settings;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            if (Settings.Get("kafka.IsLongRunning", false))
            {
                for (var i = 0; i < ConsumerSize; i++)
                {
                    _listTask.Add(Task.Factory.StartNew(Execute, _stoppingCts.Token, TaskCreationOptions.LongRunning, TaskScheduler.Current));
                    Logger.Info("consumer created");
                }
            }
            else
            {
                for (var i = 0; i < ConsumerSize; i++)
                {
                    _listTask.Add(Task.Factory.StartNew(Execute, _stoppingCts.Token));
                    Logger.Info("consumer created");
                }
            }
            return Task.CompletedTask;
        }
        private AsyncRetryPolicy GetAsyncRetryPolicy()
        {
            var retries = 1;
            return Policy.Handle<Exception>()
                .WaitAndRetryForeverAsync(
                    attempt => TimeSpan.FromSeconds(0.1 * Math.Pow(2, attempt)), // Back off!  200, 400, 800, 1600, 3200, 6400, 12800 (ms)
                    (exception, calculatedWaitDuration) =>  // Capture some info for logging!
                    {
                        retries++;
                        if (retries > 5)
                        {
                            Logger.Info($"URGENT consumer {TopicName} may have retry more than 5 times + {exception.Message} + {exception.StackTrace}");
                        }
                    });
        }
        private async Task Execute()
        {
            var policy = GetAsyncRetryPolicy();
            await policy.ExecuteAsync(async () => await SubExcute()); // async execute overload

        }

        private async Task SubExcute()
        {
            var consumerBuilder = new ConsumerBuilder<Ignore, string>(KafkaClient.KafkaClient.Instance.GetKafkaConsumerConfig());
            consumerBuilder.SetLogHandler((_, e) =>
            {
                if (Kafka.IsLogHandler)
                {
                    Logger.Info($"Kafka consumer log handler - {e.Name}, {e.Facility}, {e.Level.ToString()}, message: {e.Message}");
                }

            }).SetErrorHandler((_, e) =>
            {
                Logger.Error($"Kafka consumer error - {e}");
            });
            using (var consumer = consumerBuilder.Build())
            {
                consumer.Subscribe(TopicName);
                try
                {
                    while (true)
                    {
                        try
                        {
                            var consumeResult = consumer.Consume();
                            try
                            {
                                var timeoutPolicy = Policy.TimeoutAsync(Settings.Get("kafka.MaxPollIntervalMs", 300000) / 1000 - 10, TimeoutStrategy.Pessimistic); 
                                await timeoutPolicy.ExecuteAsync(async () =>
                                {
                                    var message = consumeResult.Message.Value.FromJson<T>();
                                    await ProcessMessage(message);
                                });

                            }
                            catch (Exception e)
                            {
                                Logger.Error($"Kafka exception: {e.Message}", e);
                            }

                            var isAutoCommit = Kafka?.IsAutoCommit ?? false;
                            if (!isAutoCommit)
                            {
                                if (consumeResult.Offset % Settings.Get("kafka.CommitPeriod", 5) != 0) continue;
                                try
                                {
                                    consumer.Commit(consumeResult);
                                }
                                catch (KafkaException e)
                                {
                                    Logger.Error($"Kafka Consume Commit error: {e} , Message length: {consumeResult.Message?.Value.Length}", e);
                                }
                            }
                        }
                        catch (ConsumeException ce)
                        {
                            Logger.Error($"ConsumeException: {ce.Message} | Topic: {TopicName}", ce);
                            consumer.Close();
                            throw;
                        }
                    }
                }
                catch (OperationCanceledException e)
                {
                    Logger.Error($"Consumer is Down: {e.ToJson()}", e);
                    consumer.Close();
                }
            }
        }

        protected virtual async Task<bool> ProcessMessage(T data, bool isWriteLog = false)
        {
            if (data.RetailerId <= 0)
            {
                return true;
            }
            var logFilterRetailer = new LogObject(Logger, data.LogId)
            {
                Action = "FilterByRetailer",
                RetailerId = data.RetailerId,
                RequestObject = data
            };
            var isValid = await ValidateByRetailer(data.RetailerId);

            if (isWriteLog)
            {
                logFilterRetailer.ResponseObject = isValid;
                logFilterRetailer.LogInfo();
            }

            return !isValid;
        }

        protected async Task<string> GetConnectionString(long retailerId)
        {
            var retailer = await GetRetailer(retailerId);
            if (retailer == null) return null;
            var kvGroup = await GetRetailerGroup(retailer.GroupId);
            if (kvGroup == null) return null;
            if (string.IsNullOrEmpty(kvGroup.ConnectionString))
            {
                return null;
            }
            return kvGroup.ConnectionString.Substring(kvGroup.ConnectionString.IndexOf('=') + 1);
        }


        protected async Task<KvRetailer> GetRetailer(long retailerId)
        {
            using (var client = RedisClientsManager.GetClient())
            {
                var kvRetailer = client.Get<KvRetailer>(string.Format(KvConstant.RetailerIdCacheKey, retailerId));
                if (kvRetailer == null)
                {
                    using (var dbMaster = await DbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities))
                    {
                        kvRetailer = await dbMaster.SingleAsync<KvRetailer>(x => x.Id == retailerId);
                        if (kvRetailer != null)
                        {
                            client.Set(string.Format(KvConstant.RetailerIdCacheKey, retailerId), kvRetailer, DateTime.Now.AddDays(7));
                        }
                    }
                }
                return kvRetailer;
            }
        }

        protected async Task<Domain.Common.KvGroup> GetRetailerGroup(int groupId)
        {
            using (var client = RedisClientsManager.GetClient())
            {
                var kvGroup = client.Get<Domain.Common.KvGroup>(string.Format(KvConstant.KvGroupCacheKey, groupId));
                if (kvGroup == null)
                {
                    using (var dbMaster = await DbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities))
                    {
                        kvGroup = await dbMaster.SingleAsync<Domain.Common.KvGroup>(x => x.Id == groupId);
                        if (kvGroup != null)
                        {
                            client.Set(string.Format(KvConstant.KvGroupCacheKey, groupId), kvGroup, DateTime.Now.AddDays(7));
                        }
                    }
                }
                return kvGroup;
            } 
        }

        protected async Task<bool> ValidateByRetailer(int retailerId)
        {
            using (var client = RedisClientsManager.GetClient())
            {
                if (!client.ContainsKey(KvConstant.RetailersByChannelsKey))
                {
                    using (var db = await DbConnectionFactory.OpenAsync())
                    {
                        using (_ = db.OpenTransaction(System.Data.IsolationLevel.ReadUncommitted))
                        {
                            var ids = await db.SelectAsync<long>(db.From<Domain.Model.OmniChannel>()
                                .Where(x => x.IsActive && (x.IsDeleted == null || x.IsDeleted == false))
                                .Select(x => x.RetailerId));
                            if (ids == null || !ids.Any())
                            {
                                return false;
                            }

                            ids = ids.GroupBy(x => x).Select(p => p.FirstOrDefault()).ToList();
                            client.AddRangeToSet(KvConstant.RetailersByChannelsKey, ids.Select(p => p.ToString()).ToList());
                            var expire = DateTime.Now.AddDays(Settings.Get("CacheRegisterRetailer", 10));
                            client.ExpireEntryAt(KvConstant.RetailersByChannelsKey, expire);
                            return ids.Contains(retailerId);
                        }
                    }
                }
                return client.SetContainsItem(KvConstant.RetailersByChannelsKey, retailerId.ToString());
            }
        }
        public async Task StopAsync(CancellationToken cancellationToken)
        {
            Logger.Info("Consummer is going down");
            if (!_listTask.Any())
            {
                return;
            }
            try
            {
                _stoppingCts.Cancel();
            }
            finally
            {
                await Task.WhenAll(_listTask);
            }
        }

    }
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.ProductSubscribe.Impls.RedisMq
{
    public class ProcessRemoveService : BaseRedisService<ProcessRemoveMessage<Domain.Model.ProductSubscribe>>
    {
        private IMappingBusiness _mappingBusiness;
        public ProcessRemoveService(IProductMappingService productMappingService,
            IDbConnectionFactory dbConnectionFactory,
            IAppSettings settings,
            ICacheClient cacheClient,
            IMessageService messageService,
            IMappingBusiness mappingBusiness) : base(productMappingService,
            dbConnectionFactory,
            settings,
            cacheClient,
            messageService)
        {
            _mappingBusiness = mappingBusiness;
        }

        public override async Task<List<Domain.Model.OmniChannel>> ProcessMessage(ProcessRemoveMessage<Domain.Model.ProductSubscribe> data)
        {
            var logMapping = new LogObject(Logger, data.LogId)
            {
                Action = "RemoveMappingSubscribe",
                RetailerId = data.RetailerId,
                RequestObject = data.ToSafeJson()
            };

            var channels = await base.ProcessMessage(data);
            if (data.ProductIds != null && data.ProductIds.Any())
            {
                var channelIds = channels.Select(x => x.Id).ToList();
                await _mappingBusiness.RemoveMappings(data.ConnectionString, data.RetailerId, data.BranchId, channelIds, data.ProductIds, null, data.LogId, true);
                logMapping.Description = "Process remove mapping successful";
                logMapping.LogInfo();
            }
            else
            {
                logMapping.Description = "Process remove mapping fail, productIds is null";
                logMapping.LogInfo();
            }
            return channels;
        }
    }
}
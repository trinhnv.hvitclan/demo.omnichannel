﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.ProductSubscribe.Impls.RedisMq
{

    public class ProcessStockService : BaseRedisService<ProcessStockMessage<Domain.Model.ProductSubscribe>>
    {
        private readonly IOnHandBusiness _onHandBusiness;
        private readonly IProductMappingService _productMappingService;

        public ProcessStockService(IProductMappingService productMappingService,
            IDbConnectionFactory dbConnectionFactory,
            ICacheClient cacheClient,
            IOnHandBusiness onHandBusiness,
            IAppSettings settings,
            IMessageService messageService) : base(productMappingService,
            dbConnectionFactory,
            settings,
            cacheClient,
            messageService)
        {
            _onHandBusiness = onHandBusiness;
            _productMappingService = productMappingService;
        }

        public override async Task<List<Domain.Model.OmniChannel>> ProcessMessage(
            ProcessStockMessage<Domain.Model.ProductSubscribe> data)
        {
            var logStock = new LogObject(Logger, data.LogId)
            {
                Action = "ProcessStockMessage",
                RetailerId = data.RetailerId,
                BranchId = data.BranchId,
                RequestObject = data?.Products?.Select(x => x.ProductBranch)
            };
            try
            {
                var channels = await base.ProcessMessage(data);
                if (data.Products == null || !data.Products.Any() ||
                    channels == null || !channels.Any())
                {
                    logStock.LogWarning("Products or channel is null");
                    return channels;
                }

                var products = data.Products.Where(x => x.ProductBranch != null && x.ProductBranch.All(p => p.IsActive == null || p.IsActive == true) &&
                x.ProductType != (byte)ProductType.Manufactured && x.ProductType != (byte)ProductType.Service).ToList();
                if (!products.Any())
                {
                    logStock.LogWarning("Products is null");
                    return channels;
                }

                //Lọc các kênh bán không bật đồng bộ tồn và sửa tồn ở một chi nhánh khác chi nhánh thiết lập đồng bộ
                var channelFilter = channels.Where(x => IsSettingSyncBranch(products, x)).ToList();
                if (!channelFilter.Any())
                {
                    logStock.LogWarning("Channel is null");
                    return channels;
                }

                await OnHandSubscribe(channelFilter, products, data);

                logStock.LogInfo();
                return channels;
            }
            catch (Exception e)
            {
                logStock.LogError(e);
                return null;
            }
        }

        #region Private method
        private async Task OnHandSubscribe(List<Domain.Model.OmniChannel> channels,
           List<Domain.Model.ProductSubscribe> products,
           ProcessStockMessage<Domain.Model.ProductSubscribe> data)
        {
            var kvProductIds = products.Select(p => p.Id).ToList();
            var productMappings = await _productMappingService.Get(data.RetailerId,
                    channels.Select(x => x.Id).ToList(), kvProductIds);

            foreach (var channel in channels)
            {
                var channelProducts = productMappings.Where(x => x.ChannelId == channel.Id && kvProductIds.Contains(x.ProductKvId)).ToList();
                await _onHandBusiness.SyncMultiOnHand(
                       data.ConnectionString,
                       channel,
                       null,
                       kvProductIds,
                       false,
                       data.LogId, null, true, 50,
                       channelProducts, false);
            }
        }
        #endregion
    }
}


﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.ProductSubscribe.Impls.RedisMq
{
    public class ProcessPriceService : BaseRedisService<ProcessPriceMessage<Domain.Model.ProductSubscribe>>
    {
        private readonly IPriceBusiness _priceBusiness;
        private readonly IMappingBusiness _mappingBusiness;
        private readonly IProductMappingService _productMappingService;

        public ProcessPriceService(IProductMappingService productMappingService,
            IDbConnectionFactory dbConnectionFactory,
            IAppSettings settings,
            ICacheClient cacheClient,
            IMessageService messageService,
            IPriceBusiness priceBusiness,
            IMappingBusiness mappingBusiness) : base(productMappingService,
            dbConnectionFactory,
            settings,
            cacheClient,
            messageService)
        {
            _priceBusiness = priceBusiness;
            _mappingBusiness = mappingBusiness;
            _productMappingService = productMappingService;
            _ = new SelfAppConfig(settings);
        }

        public override async Task<List<Domain.Model.OmniChannel>> ProcessMessage(ProcessPriceMessage<Domain.Model.ProductSubscribe> data)
        {
            var logProcessPrice = new LogObject(Logger, data.LogId)
            {
                Action = "ProcessPriceMessage",
                RetailerId = data.RetailerId,
                RequestObject = data?.ToSafeJson(),
            };
            try
            {
                var channels = await base.ProcessMessage(data);
                if (data.Products == null || !data.Products.Any())
                {
                    return channels;
                }

                var totalProduct = data.Products.Count;

                try
                {
                    if (!data.IsFromPriceBook)
                    {
                        await MappingSubscribe(data, channels);
                    }
                }
                catch (Exception e)
                {
                    Logger.Error($"MappingSubscribe error: {e.Message}", e);
                }

                //Lọc các kênh bán không bật đồng bộ giá 
                var channelFilter = channels.Where(x => IsSettingSyncPrice(x, data.PriceBookId)).ToList();
                if (!channelFilter.Any())
                {
                    logProcessPrice.LogWarning("Channel is null");
                    return channels;
                }

                await PriceSubscribe(channelFilter, data);

                return channelFilter;
            }
            catch (Exception ex)
            {
                logProcessPrice.LogError(ex);
                throw;
            }
        }
        #region Private method
        private async Task MappingSubscribe(ProcessPriceMessage<Domain.Model.ProductSubscribe> data, List<Domain.Model.OmniChannel> channels)
        {
            //Mapping V2 Config
            var configMappingV2Feature = Settings.Get<MappingV2Feature>("MappingV2Feature");
            var coreContext = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, data.RetailerId,
                        data.ConnectionString, data.BranchId, Settings?.Get<int>("ExecutionContext"));
            var isUseMappingV2 = configMappingV2Feature.IsValid(coreContext.Group?.Id ?? 0, data.RetailerId);

            var kvProducts = new List<KvProductMapping>();
            foreach (var item in data.Products)
            {
                if (!isUseMappingV2)
                {
                    var p = new KvProductMapping
                    {
                        Id = item.Id,
                        Code = item.Code,
                        Fullname = item.FullName
                    };
                    if (item.ProductBranch != null && item.ProductBranch.Any())
                    {
                        p.InActiveBranchIds = item.ProductBranch.Where(x => x.IsActive == false).Select(x => x.BranchId).ToList();
                    }

                    kvProducts.Add(p);
                    continue;
                }

                // s1: base
                // check ProductUnit
                // if any -> use ProductUnit
                // else -> use original way
                List<int> isActive = null;
                if (item.ProductBranch != null && item.ProductBranch.Any())
                {
                    isActive = item.ProductBranch.Where(x => x.IsActive == false).Select(x => x.BranchId).ToList();
                }
                if (item.ProductUnit != null && item.ProductUnit.Any())
                {
                    foreach (var prod in item.ProductUnit)
                    {
                        var p = new KvProductMapping
                        {
                            Id = prod.Id,
                            Code = prod.Code,
                            Fullname = prod.FullName,
                        };
                        if (isActive != null && isActive.Any())
                        {
                            p.InActiveBranchIds = isActive;
                        }
                        kvProducts.Add(p);
                    }
                }
                else
                { // originial way
                    var p = new KvProductMapping
                    {
                        Id = item.Id,
                        Code = item.Code,
                        Fullname = item.FullName
                    };
                    if (isActive != null && isActive.Any())
                    {
                        p.InActiveBranchIds = isActive;
                    }
                    kvProducts.Add(p);
                }


                // s2: ProductAttribute any
                // select top (1) to get ProductId
                // check product where MasterProductId= ProductId -> listProductAttribute
                // foreach pro in ProductAttribute 
                // find product where MasterUnitId = pro.Id

                if (item.ProductAttribute != null && item.ProductAttribute.Any())
                {
                    using (var dbRetail = await DbConnectionFactory.OpenAsync(data.ConnectionString.ToLower()))
                    {
                        using (dbRetail.OpenTransaction(IsolationLevel.ReadUncommitted))
                        {
                            var productRepo = new ProductRepository(dbRetail);

                            var parentProd = productRepo.Where(x => x.Id == item.ProductAttribute[0].ProductId && item.RetailerId == data.RetailerId).FirstOrDefault();
                            if (parentProd == null) continue;
                            var productAtrributeChilds = productRepo.Where(x => x.MasterProductId == parentProd.Id && item.RetailerId == data.RetailerId).ToList();

                            // find product have MasterUnitId is NULL
                            var productMaster = productAtrributeChilds.Where(x => x.MasterUnitId == null).ToList();
                            // find list branch in ProductBranchRepository x => x.IsActive == false)
                            var dicProductBranch = new Dictionary<long, List<int>>();
                            foreach (var prodMaster in productMaster)
                            {
                                var productBranchRepo = new ProductBranchRepository(dbRetail);
                                var listBranchDeActive = productBranchRepo.Where(x => x.ProductId == prodMaster.Id && x.IsActive == false && item.RetailerId == data.RetailerId)
                                    .Select(x => x.BranchId).ToList();
                                if (listBranchDeActive.Any())
                                {
                                    dicProductBranch.Add(prodMaster.Id, listBranchDeActive);
                                }
                            }
                            foreach (var child in productAtrributeChilds)
                            {
                                var kvPdMapping = new KvProductMapping
                                {
                                    Id = child.Id,
                                    Code = child.Code,
                                    Fullname = child.FullName,
                                };
                                if (dicProductBranch.ContainsKey(kvPdMapping.Id) || dicProductBranch.ContainsKey(child.MasterUnitId ?? 0))
                                {
                                    var keyPro = child.MasterUnitId ?? child.Id;
                                    kvPdMapping.InActiveBranchIds = dicProductBranch[keyPro];
                                }
                                kvProducts.Add(kvPdMapping);
                            }
                        }
                    }

                }
            }
            kvProducts = kvProducts.GroupBy(t => t.Id).Select(t => t.FirstOrDefault()).ToList();
            
            if (isUseMappingV2)
            {
                await _mappingBusiness.CreateMappingAsyncV2(data.ConnectionString, data.RetailerId, data.BranchId, channels, kvProducts, data.LogId, "MappingSubscribe", NumberHelper.GetValueOrDefault(Settings?.Get<int?>("MappingPageSize"), 50));
            }
            else
            {
                await _mappingBusiness.CreateMappingAsync(data.ConnectionString, data.RetailerId, data.BranchId, channels, kvProducts, data.LogId, "MappingSubscribe", NumberHelper.GetValueOrDefault(Settings?.Get<int?>("MappingPageSize"), 50));
            }
        }

        private async Task PriceSubscribe(List<Domain.Model.OmniChannel> channels,
           ProcessPriceMessage<Domain.Model.ProductSubscribe> data)
        {
            var sourceQueue = data.IsFromPriceBook
                  ? "Queue created from PriceBookSyncEsSubscribeService Tracking"
                  : "Queue created from SyncEsSubscribeService Tracking";

            var kvProductIds = data.Products.Select(p => p.Id).ToList();
            var productMappings = await _productMappingService.Get(data.RetailerId,
                    channels.Select(x => x.Id).ToList(), kvProductIds);

            foreach (var channel in channels)
            {
                var channelProducts = productMappings.Where(x => x.ChannelId == channel.Id && kvProductIds.Contains(x.ProductKvId)).ToList();
                await _priceBusiness.SyncMultiPrice(data.ConnectionString,
                           channel,
                           null,
                           kvProductIds,
                           false,
                           data.LogId,
                           true, 50, channelProducts,
                           false, sourceQueue);
            }
        }
    }
    #endregion
}

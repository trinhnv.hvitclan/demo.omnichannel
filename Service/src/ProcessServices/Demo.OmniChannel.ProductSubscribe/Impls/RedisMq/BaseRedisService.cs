﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using MongoDB.Bson.Serialization.Serializers;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.Messaging;

namespace Demo.OmniChannel.ProductSubscribe.Impls.RedisMq
{
    public abstract class BaseRedisService<T> where T : BaseRedisMessage
    {
        protected IDbConnectionFactory DbConnectionFactory;
        protected ICacheClient CacheClient;
        protected IAppSettings Settings;
        protected IProductMappingService ProductMappingService;
        protected IMessageService MessageService;
        protected KvRedisConfig MqRedisConfig;
        protected ILog Logger => LogManager.GetLogger(typeof(BaseRedisService<>));

        protected BaseRedisService(IProductMappingService productMappingService,
            IDbConnectionFactory dbConnectionFactory,
            IAppSettings settings,
            ICacheClient cacheClient,
            IMessageService messageService)
        {
            DbConnectionFactory = dbConnectionFactory;
            CacheClient = cacheClient;
            Settings = settings;
            ProductMappingService = productMappingService;
            MessageService = messageService;
        }

        public virtual async Task<List<Domain.Model.OmniChannel>> ProcessMessage(T data)
        {
            var channels = await FilterByRetailer(data.RetailerId);
            if (channels == null || !channels.Any())
            {
                return new List<Domain.Model.OmniChannel>();
            }

            //if (IsFromDr)
            //{
            //    var threadSleep = TimeSpan.FromSeconds(Settings?.Get<int>("ChannelSyncThreadSleep") ?? 5);
            //    await Task.Delay(threadSleep);
            //}
            return channels;
        }
        protected List<long> GetAndLockProductSync(long channelId, List<long> productIdReqs, string key)
        {
            var result = new List<long>();

            foreach (var productIdReq in productIdReqs)
            {
                var productLock = CacheClient.Get<bool>(string.Format(key, channelId, productIdReq));

                if (productLock) continue;

                result.Add(productIdReq);

                var expireIn = TimeSpan.FromSeconds(Settings?.Get<int?>("ChannelProductLock") ?? 1);
                CacheClient.Set(string.Format(key, channelId, productIdReq), true, expireIn);
            }

            return result;
        }

        protected async Task<List<ProductMapping>> GetChannelProducts(long retailerId, long channelId, long productId)
        {
            List<ProductMapping> dataInCache = null;
            if (CacheClient != null)
            {
                dataInCache = CacheClient.Get<List<ProductMapping>>(string.Format(
                    KvConstant.ListProductMappingCacheKey,
                    retailerId, channelId, productId));
            }

            if (dataInCache != null && dataInCache.Any())
            {
                return dataInCache;
            }

            var retVal =
                await ProductMappingService.GetListByKvSingleProductId(retailerId, channelId, productId);

            if (retVal == null)
            {
                return null;
            }

            CacheClient?.Set(string.Format(KvConstant.ListProductMappingCacheKey, retailerId, channelId, productId), retVal,
                DateTime.Now.AddDays(30));
            return retVal;
        }

        protected bool IsSettingSyncBranch(List<Domain.Model.ProductSubscribe> products,
          Domain.Model.OmniChannel channel)
        {
            if (!channel.OmniChannelSchedules?.Any(s => s.Type == (byte)ScheduleType.SyncOnhand) ?? true)
            {
                return false;
            }

            if (products.FirstOrDefault().ProductBranch == null ||
                products.FirstOrDefault().ProductBranch.All(x => x.BranchId != channel.BranchId))
            {
                return false;
            }
            return true;
        }

        protected bool IsSettingSyncPrice(Domain.Model.OmniChannel channel, long priceBookId)
        {
            if (!channel.OmniChannelSchedules?.Any(s => s.Type == (byte)ScheduleType.SyncPrice) ?? true)
            {
                return false;
            }

            if (channel.BasePriceBookId.GetValueOrDefault() != priceBookId &&
                channel.PriceBookId.GetValueOrDefault() != priceBookId)
            {
                return false;
            }

            return true;
        }

        protected async Task<ProductMapping> GetChannelProduct(long retailerId, long channelId, long productId)
        {
            ProductMapping dataInCache = null;
            if (CacheClient != null)
            {
                dataInCache = CacheClient.Get<ProductMapping>(string.Format(KvConstant.ProductMappingCacheKey,
                    channelId, retailerId, productId));
            }

            if (dataInCache != null)
            {
                return dataInCache;
            }
            var retVal = await ProductMappingService.GetByKvSingleProductId(retailerId, channelId, productId);
            if (retVal == null)
            {
                return null;
            }
            CacheClient?.Set(string.Format(KvConstant.ProductMappingCacheKey, channelId, retailerId, productId),
                retVal);
            return retVal;
        }

        #region Private method
        private async Task<List<Domain.Model.OmniChannel>> FilterByRetailer(int retailerId)
        {
            var channels =
                CacheClient.Get<List<Domain.Model.OmniChannel>>(string.Format(KvConstant.ChannelsByRetailerKey, retailerId));
            if (channels == null)
            {
                using (var db = DbConnectionFactory.OpenDbConnection())
                {
                    var kvChannelRepository = new OmniChannelRepository(db);
                    channels = await kvChannelRepository.GetByRetailerAsync(retailerId, type: null, isIncludeInactive: false);
                }

                if (channels != null && channels.Any())
                {
                    var expiredIn = TimeSpan.FromDays(Settings.Get<int?>("CacheChannelByRetailer") ?? 30);
                    CacheClient.Set(string.Format(KvConstant.ChannelsByRetailerKey, retailerId), channels,
                        expiredIn);
                }
            }
            return channels;
        }
        #endregion
    }
}
﻿using Demo.OmniChannel.ChannelClient.FeatureToggle;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities.RabbitMq;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Demo.OmniChannel.ProductSubscribe.Impls.RabbitMq
{
    public class SyncEsDeadLetterQueueService : IHostedService
    {
        protected ILog Logger => LogManager.GetLogger(typeof(SyncEsDeadLetterQueueService));
        protected IAppSettings Settings;
        protected IRedisClientsManager RedisClientsManager;
        protected IDbConnectionFactory DbConnectionFactory;
        protected IMessageService MessageService;
        private IModel _channel;
        private IConnection _connection;


        public SyncEsDeadLetterQueueService(IAppSettings settings, IRedisClientsManager redisClientsManager, IDbConnectionFactory dbConnectionFactory, IMessageService messageService)
        {
            Settings = settings;
            RedisClientsManager = redisClientsManager;
            DbConnectionFactory = dbConnectionFactory;
            MessageService = messageService;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            InitRabbitMq();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _channel.Dispose();
            _connection.Dispose();
            return Task.CompletedTask;
        }

        private void InitRabbitMq()
        {
            try
            {
                var rabbitMqConfig = Settings.Get<RabbitMqConfig>("rabbitmq"); 

                var factory = new ConnectionFactory()
                {
                    HostName = rabbitMqConfig.HostName,
                    Port = rabbitMqConfig.Port,
                    UserName = rabbitMqConfig.UserName,
                    Password = rabbitMqConfig.Password,
                    VirtualHost = rabbitMqConfig.VirtualHost,
                };
                while (_connection == null)
                {
                    try
                    {
                        _connection = factory.CreateConnection();
                    }
                    catch (Exception ex)
                    {
                        Logger.Warn($"SyncEsDeadLetterQueueService: Error while create connection RabbitMq Consumer: {ex.Message}");
                    }
                }

                _channel = _connection.CreateModel();

                var arguments = new Dictionary<string, object>() { { "x-queue-type", "quorum" } };
                _channel.ExchangeDeclare(exchange: rabbitMqConfig.Exchange, type: rabbitMqConfig.ExchangeType, durable: true, autoDelete: false, arguments: arguments);
                _channel.QueueBind(
                    queue: rabbitMqConfig.QueueName,
                    exchange: rabbitMqConfig.Exchange,
                    routingKey: rabbitMqConfig.RoutingKey);

                var consumer = new EventingBasicConsumer(_channel);
                consumer.Received += async (model, args) =>
                {
                    try
                    {
                        await OnMessageReceivedAsync(args);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error($"SyncEsDeadLetterQueueService: Error while proccessing RabbitMq Consumer: {ex.Message}");
                    }
                };

                _channel.BasicConsume(queue: rabbitMqConfig.QueueName,
                                     autoAck: false,
                                     consumer: consumer);

            }
            catch (Exception ex)
            {
                Logger.Error($"Error while starting RabbitMq Consumer: {ex.Message}");
            }

        }

        private async Task OnMessageReceivedAsync(BasicDeliverEventArgs args)
        {
            try
            {
                var body = args.Body.ToArray();
                var jsonString = Encoding.UTF8.GetString(body);
                var message = JsonConvert.DeserializeObject<SyncEsMessage<Domain.Model.ProductSubscribe>>(jsonString);
                Logger.Info($"SyncEsDeadLetterQueueService: Received from RabbitMQ - Dead letter queue: {message.ToSafeJson()}");
                await ProcessSyncEs(message);
                Console.WriteLine(@"SyncEsDeadLetterQueueService: Done process dead letter queue");
            }
            catch (Exception ex)
            {
                Logger.Error($"Error while processing RabbitMq Consumer: {ex.Message}");
            }
            finally
            {
                _channel.BasicAck(deliveryTag: args.DeliveryTag, multiple: false);
            }
        }

        private async Task ProcessSyncEs(SyncEsMessage<Domain.Model.ProductSubscribe> data)
        {
            try
            {
                var logId = Guid.NewGuid();
                var logProcessSyncEsMessage = new LogObject(Logger, logId)
                {
                    Action = "SyncEsSubscribeService",
                    RetailerId = data.RetailerId,
                    RequestObject = data.ToSafeJson()
                };
                if (await FilterByRetailer(data)) return;
                int? groupId;
                (data.ConnectionString, groupId) = await GetConnectionString(data.RetailerId);

                switch (data.Type)
                {
                    case (byte)EsEventType.Product:
                        {
                            if (data.Advice == (byte)TrackingAdvice.Remove)
                            {
                                if (!string.IsNullOrEmpty(data.Ids))
                                {
                                    var productIds = data.Ids.Split(",").Select(long.Parse).ToList();
                                    var removeMessage = new ProcessRemoveMessage<Domain.Model.ProductSubscribe>
                                    {
                                        RetailerId = data.RetailerId,
                                        BranchId = data.BranchId,
                                        SentTime = data.SentTime,
                                        ProductIds = productIds,
                                        ConnectionString = data.ConnectionString,
                                        LogId = logId
                                    };
                                    using var msg = MessageService.MessageFactory.CreateMessageProducer();
                                    msg.Publish(removeMessage);
                                }
                                break;
                            }
                            if (data.EventName == EsEventName.UpdateStock)
                            {
                                var stockUpdateMessage = new ProcessStockMessage<Domain.Model.ProductSubscribe>
                                {
                                    RetailerId = data.RetailerId,
                                    BranchId = data.BranchId,
                                    SentTime = data.SentTime,
                                    Products = data.Products,
                                    ConnectionString = data.ConnectionString,
                                    LogId = logId
                                };

                                if (new ProcessStockMessageV2Toggle(Settings).Enable(data.RetailerId, groupId ?? 0))
                                {
                                    using (var msg = MessageService.MessageFactory.CreateMessageQueueClient())
                                    {
                                        var mess = new Message { CreatedDate = DateTime.Now, Id = Guid.NewGuid() };
                                        mess.Body = stockUpdateMessage;
                                        msg.Publish($"mq:{Settings.Get("ProcessStockMessageV2_Topic", "ProcessStockMessageV2_Topic")}.inq", mess);
                                    }
                                }
                                else
                                {
                                    using (var msg = MessageService.MessageFactory.CreateMessageProducer())
                                    {
                                        msg.Publish(stockUpdateMessage);
                                    }
                                }
                                
                                break;
                            }

                            var updatePriceMessage = new ProcessPriceMessage<Domain.Model.ProductSubscribe>
                            {
                                RetailerId = data.RetailerId,
                                BranchId = data.BranchId,
                                SentTime = data.SentTime,
                                Products = data.Products,
                                PriceBookId = 0,
                                ConnectionString = data.ConnectionString,
                                LogId = logId,
                                IsFromPriceBook = false
                            };
                            using (var msg = MessageService.MessageFactory.CreateMessageProducer())
                            {
                                msg.Publish(updatePriceMessage);
                            }
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                Logger.Error($"SyncEsDeadLetterQueueService: Error while ProcessSyncEs: {ex.Message}");
            }
        }

        #region HELPER METHOD

        private async Task<bool> FilterByRetailer(SyncEsMessage<Domain.Model.ProductSubscribe> data, bool isWriteLog = false)
        {
            if (data.RetailerId <= 0)
            {
                return false;
            }
            var logFilterRetailer = new LogObject(Logger, data.LogId)
            {
                Action = "FilterByRetailer",
                RetailerId = data.RetailerId,
                ResponseObject = data
            };
            var isValid = await ValidateByRetailer(data.RetailerId);

            if (isWriteLog)
            {
                logFilterRetailer.LogInfo();
            }

            return !isValid;
        }

        protected async Task<(string connectionString, int? groupId)> GetConnectionString(long retailerId)
        {
            var retailer = await GetRetailer(retailerId);
            if (retailer == null) return (null, null);
            var kvGroup = await GetRetailerGroup(retailer.GroupId);
            if (kvGroup == null) return (null, null);
            if (string.IsNullOrEmpty(kvGroup.ConnectionString))
            {
                return (null, null);
            }
            return (kvGroup.ConnectionString.Substring(kvGroup.ConnectionString.IndexOf('=') + 1), kvGroup.Id);
        }


        private async Task<KvRetailer> GetRetailer(long retailerId)
        {
            try
            {
                using var client = RedisClientsManager.GetClient();
                var kvRetailer = client.Get<KvRetailer>(string.Format(KvConstant.RetailerIdCacheKey, retailerId));
                if (kvRetailer == null)
                {
                    using var dbMaster = await DbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities);
                    using (_ = dbMaster.OpenTransaction(System.Data.IsolationLevel.ReadUncommitted))
                    {
                        kvRetailer = await dbMaster.SingleAsync<KvRetailer>(x => x.Id == retailerId);
                        if (kvRetailer != null)
                        {
                            client.Set(string.Format(KvConstant.RetailerIdCacheKey, retailerId), kvRetailer, DateTime.Now.AddDays(7));
                        }
                    }
                }
                return kvRetailer;
            }
            catch (Exception ex)
            {
                Logger.Error($"Error while ProcessSyncEs: {ex.Message}");
                return new KvRetailer();
            }
        }

        private async Task<Domain.Common.KvGroup> GetRetailerGroup(int groupId)
        {
            using var client = RedisClientsManager.GetClient();
            var kvGroup = client.Get<Domain.Common.KvGroup>(string.Format(KvConstant.KvGroupCacheKey, groupId));
            if (kvGroup == null)
            {
                using var dbMaster = await DbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities);
                using (_ = dbMaster.OpenTransaction(System.Data.IsolationLevel.ReadUncommitted))
                {
                    kvGroup = await dbMaster.SingleAsync<Domain.Common.KvGroup>(x => x.Id == groupId);
                    if (kvGroup != null)
                    {
                        client.Set(string.Format(KvConstant.KvGroupCacheKey, groupId), kvGroup, DateTime.Now.AddDays(7));
                    }
                }
            }
            return kvGroup;
        }

        private async Task<bool> ValidateByRetailer(int retailerId)
        {
            using var client = RedisClientsManager.GetClient();
            if (!client.ContainsKey(KvConstant.RetailersByChannelsKey))
            {
                using var db = await DbConnectionFactory.OpenAsync();
                using (_ = db.OpenTransaction(System.Data.IsolationLevel.ReadUncommitted))
                {
                    var ids = await db.SelectAsync<long>(db.From<Domain.Model.OmniChannel>()
                        .Where(x => x.IsActive && (x.IsDeleted == null || x.IsDeleted == false))
                        .Select(x => x.RetailerId));
                    if (ids == null || !ids.Any())
                    {
                        return false;
                    }

                    ids = ids.GroupBy(x => x).Select(p => p.FirstOrDefault()).ToList();
                    client.AddRangeToSet(KvConstant.RetailersByChannelsKey, ids.Select(p => p.ToString()).ToList());
                    var expire = DateTime.Now.AddDays(Settings.Get("CacheRegisterRetailer", 10));
                    client.ExpireEntryAt(KvConstant.RetailersByChannelsKey, expire);
                    return ids.Contains(retailerId);
                }
            }
            return client.SetContainsItem(KvConstant.RetailersByChannelsKey, retailerId.ToString());
        }

        #endregion HELPER METHOD
    }
}

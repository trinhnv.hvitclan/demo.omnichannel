﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.WindowsServices;
using System;
using System.Diagnostics;
using System.Linq;

namespace Demo.OmniChannel.ProductSubscribe
{
    class Program
    {
        static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();
            try
            {
                if (Debugger.IsAttached || args.Contains("--console"))
                {
                    host.Run();
                }
                else
                {
                    host.RunAsService();
                }
            }
            catch (Exception ex)
            {
                host.Run();
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseUrls(Environment.GetEnvironmentVariable("ASPNETCORE_URLS") ?? "http://localhost:5005/")
                .UseStartup<Startup>();
    }
}
﻿using Demo.OmniChannel.Infrastructure.EventBus.Abstractions;
using Demo.OmniChannel.Infrastructure.EventBus.Event;
using Demo.OmniChannel.ProductSubscribe.BackgroundProcess;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Demo.OmniChannel.ProductSubscribe.EventHandlers
{
    public class SyncPriceEventHandler : IIntegrationEventHandler<OmniSyncPriceEvent>
    {
        private readonly SyncPriceProcess _syncPriceProcess;

        public SyncPriceEventHandler(
            SyncPriceProcess syncPriceProcess)
        {
            _syncPriceProcess = syncPriceProcess;
        }
        public async Task Handle(OmniSyncPriceEvent @event)
        {
            await _syncPriceProcess.HandleSyncPriceProcess(@event);
        }
    }
}

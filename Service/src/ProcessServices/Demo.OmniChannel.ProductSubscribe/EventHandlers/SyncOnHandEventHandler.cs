﻿using Demo.OmniChannel.Infrastructure.EventBus.Abstractions;
using Demo.OmniChannel.Infrastructure.EventBus.Event;
using Demo.OmniChannel.ProductSubscribe.BackgroundProcess;
using System.Threading.Tasks;

namespace Demo.OmniChannel.ProductSubscribe.EventHandlers
{
    public class SyncOnHandEventHandler : IIntegrationEventHandler<OmniSyncOnhandEvent>
    {
        private readonly SyncOnHandProcess _syncOnHandProcess;

        public SyncOnHandEventHandler(
            SyncOnHandProcess syncOnHandProcess)
        {
            _syncOnHandProcess = syncOnHandProcess;
        }
        public async Task Handle(OmniSyncOnhandEvent @event)
        {
            await _syncOnHandProcess.HandleSyncOnHandProcess(@event);
        }
    }
}

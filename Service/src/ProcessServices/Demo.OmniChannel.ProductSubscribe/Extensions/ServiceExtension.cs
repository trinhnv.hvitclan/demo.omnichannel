﻿using Demo.OmniChannel.Infrastructure.EventBus.Abstractions;
using Demo.OmniChannel.Infrastructure.EventBus.Event;
using Demo.OmniChannel.ProductSubscribe.BackgroundProcess;
using Demo.OmniChannel.ProductSubscribe.Consumer;
using Demo.OmniChannel.ProductSubscribe.EventHandlers;
using Microsoft.Extensions.DependencyInjection;

namespace Demo.OmniChannel.ProductSubscribe.Extensions
{
    public static class ServiceExtension
    {
        public static void AddProcess(this IServiceCollection services)
        {
            services.AddScoped<SyncOnHandProcess>();
            services.AddScoped<SyncPriceProcess>();
        }
        public static void AddEventHandlers(this IServiceCollection services)
        {
            services.AddScoped<IIntegrationEventHandler<OmniSyncOnhandEvent>, SyncOnHandEventHandler>();
            services.AddScoped<IIntegrationEventHandler<OmniSyncPriceEvent>, SyncPriceEventHandler>();
        }
        public static void ConsumerService(this IServiceCollection services)
        {
            services.AddHostedService<SyncOnHandConsumerService>();
            services.AddHostedService<SyncPriceConsumerService>();
        }
    }
}

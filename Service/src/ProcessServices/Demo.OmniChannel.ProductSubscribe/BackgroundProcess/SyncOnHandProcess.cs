﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Infrastructure.EventBus.Event;
using ServiceStack.Data;
using ServiceStack.Redis;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.ProductSubscribe.BackgroundProcess
{
    public class SyncOnHandProcess : BaseBackgroundProcess
    {
        private readonly IOnHandBusiness _onHandBusiness;
        private readonly IChannelBusiness _channelBusiness;

        public SyncOnHandProcess(
            IRedisClientsManager redisClientsManager,
            IDbConnectionFactory dbConnectionFactory,
            IOnHandBusiness onHandBusiness,
            IChannelBusiness channelBusiness) : base(redisClientsManager, dbConnectionFactory)
        {
            _onHandBusiness = onHandBusiness;
            _channelBusiness = channelBusiness;
        }

        /// <summary>
        /// Handle comparing price
        /// </summary>
        /// <param name="event"></param>
        /// <returns></returns>
        public async Task HandleSyncOnHandProcess(OmniSyncOnhandEvent @event)
        {
            var log = new SeriLogObject(@event.Id)
            {
                Action = "HandleSyncOnHandProcess",
                RetailerId = @event.RetailerId,
                OmniChannelId = @event.ChannelId,
                RequestObject = @event.ChannelProductIds,
            };

            if (@event.ChannelProductIds == null || !@event.ChannelProductIds.Any())
            {
                log.Description = "ChannelProductIds  is null";
                log.LogWarning();
                return;
            }

            var channel = await _channelBusiness.GetChannel(@event.ChannelId, @event.RetailerId, @event.Id);

            if (channel == null)
            {
                log.Description = "channel is null";
                log.LogWarning();
                return;
            }
            var context = await GetKvInternalContextAsync(@event.RetailerId);
            if (context == null)
            {
                log.Description = "context is null";
                log.LogWarning();
                return;
            }
            await SyncMultiOnHandProcess(channel, @event, context.Group.ConnectionString);
            log.Description = "Push HandleSyncOnHandProcess successfull";
            log.LogInfo();
        }
        private async Task SyncMultiOnHandProcess(Domain.Model.OmniChannel channel,
               OmniSyncOnhandEvent data, string connectionString)
        {

            await _onHandBusiness.SyncMultiOnHand(
                   connectionString,
                   channel,
                   data.ChannelProductIds,
                   data.KvProductIds,
                   false,
                   data.Id, null, true, 50,
                   null, true);
        }
    }

}

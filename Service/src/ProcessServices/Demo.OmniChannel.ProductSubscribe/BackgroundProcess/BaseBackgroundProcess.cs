﻿using Demo.OmniChannel.Domain.Common;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.ShareKernel.Common;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using ServiceStack.Redis;
using System;
using System.Threading.Tasks;

namespace Demo.OmniChannel.ProductSubscribe.BackgroundProcess
{
    public class BaseBackgroundProcess
    {
        protected IRedisClientsManager RedisClientsManager;
        protected IDbConnectionFactory DbConnectionFactory;
        public BaseBackgroundProcess(
            IRedisClientsManager redisClientsManager,
            IDbConnectionFactory dbConnectionFactory)
        {
            RedisClientsManager = redisClientsManager;
            DbConnectionFactory = dbConnectionFactory;
        }


        protected async Task<KvInternalContext?> GetKvInternalContextAsync(long retailerId)
        {
            var retailer = await GetRetailer(retailerId);
            if (retailer == null) return null;
            var kvGroup = await GetRetailerGroup(retailer.GroupId);
            if (kvGroup == null) return null;

            var kvInternalContext = new KvInternalContext
            {
                RetailerCode = retailer.Code,
                RetailerId = retailer.Id,
                Group = new Domain.Common.KvGroup
                {
                    Id = kvGroup?.Id ?? 0,
                    ConnectionString = kvGroup?.ConnectionString.Substring(kvGroup.ConnectionString.IndexOf('=') + 1)
                }
            };
            return kvInternalContext;
        }
        protected async Task<KvRetailer?> GetRetailer(long retailerId)
        {
            using (var client = RedisClientsManager.GetClient())
            {
                var kvRetailer = client.Get<KvRetailer>(string.Format(KvConstant.RetailerIdCacheKey, retailerId));
                if (kvRetailer == null)
                {
                    using (var dbMaster = await DbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities))
                    {
                        kvRetailer = await dbMaster.SingleAsync<KvRetailer>(x => x.Id == retailerId);
                        if (kvRetailer != null)
                        {
                            client.Set(string.Format(KvConstant.RetailerIdCacheKey, retailerId), kvRetailer, DateTime.Now.AddDays(7));
                        }
                    }
                }
                return kvRetailer;
            }
        }

        protected async Task<Domain.Common.KvGroup?> GetRetailerGroup(int groupId)
        {
            using (var client = RedisClientsManager.GetClient())
            {
                var kvGroup = client.Get<Domain.Common.KvGroup>(string.Format(KvConstant.KvGroupCacheKey, groupId));
                if (kvGroup == null)
                {
                    using (var dbMaster = await DbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities))
                    {
                        kvGroup = await dbMaster.SingleAsync<Domain.Common.KvGroup>(x => x.Id == groupId);
                        if (kvGroup != null)
                        {
                            client.Set(string.Format(KvConstant.KvGroupCacheKey, groupId), kvGroup, DateTime.Now.AddDays(7));
                        }
                    }
                }
                return kvGroup;
            }
        }
    }
}

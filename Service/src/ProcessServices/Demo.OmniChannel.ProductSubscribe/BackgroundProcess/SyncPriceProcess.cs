﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Infrastructure.EventBus.Event;
using ServiceStack.Data;
using ServiceStack.Redis;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.ProductSubscribe.BackgroundProcess
{
    public class SyncPriceProcess: BaseBackgroundProcess
    {
        private readonly IPriceBusiness _priceBusiness;
        private readonly IChannelBusiness _channelBusiness;

        public SyncPriceProcess(
            IRedisClientsManager redisClientsManager,
            IDbConnectionFactory dbConnectionFactory,
            IPriceBusiness priceBusiness,
            IChannelBusiness channelBusiness) : base(redisClientsManager, dbConnectionFactory)
        {
            _priceBusiness = priceBusiness;
            _channelBusiness = channelBusiness;
        }

        /// <summary>
        /// Handle comparing price
        /// </summary>
        /// <param name="event"></param>
        /// <returns></returns>
        public async Task HandleSyncPriceProcess(OmniSyncPriceEvent @event)
        {
            var log = new SeriLogObject(@event.Id)
            {
                Action = "HandleSyncPriceProcess",
                RetailerId = @event.RetailerId,
                OmniChannelId = @event.ChannelId,
                RequestObject = @event.ChannelProductIds,
            };
            if (@event.ChannelProductIds == null || !@event.ChannelProductIds.Any())
            {
                log.Description = "ChannelProductIds  is null";
                log.LogWarning();
                return;
            }
            var channel = await _channelBusiness.GetChannel(@event.ChannelId, @event.RetailerId, @event.Id);

            if (channel == null)
            {
                log.Description = "channel is null";
                log.LogWarning();
                return;
            }
            var context = await GetKvInternalContextAsync(@event.RetailerId);
            if (context == null)
            {
                log.Description = "context is null";
                log.LogWarning();
                return;
            }
            await SyncMultiPriceProcess(channel, @event, context.Group.ConnectionString);
            log.Description = "Push HandleSyncPriceProcess successfull";
            log.LogInfo();
        }
        private async Task SyncMultiPriceProcess(Domain.Model.OmniChannel channel,
            OmniSyncPriceEvent data,string connectionString)
        {
                await _priceBusiness.SyncMultiPrice(connectionString,
                           channel,
                           data.ChannelProductIds,
                           data.KvProductIds,
                           false,
                           data.Id,
                           true, 50, null,
                           false, "Queue created from HandleSyncOnHandProcess");
        }
    }

}

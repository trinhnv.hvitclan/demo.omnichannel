﻿using Grpc.Core;
using Demo.OmniChannel.MappingProductGRPCServerOmniChannel;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.OrmLite;
using System.Text.Json;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.Repository.Common;
using AutoMapper;
using Demo.OmniChannel.Business.Interfaces;
using System.Text;
using Demo.OmniChannel.Repository.Impls;
using OmniChannelResponse = Demo.OmniChannel.MappingProductGRPCServerOmniChannel.OmniChannelResponse;
using OmniChannelRequest = Demo.OmniChannel.MappingProductGRPCServerOmniChannel.OmniChannelRequest;
using Demo.OmniChannel.Domain.ResultModel;
using Demo.OmniChannel.ChannelClient.Models;
using ServiceStack;
using Demo.OmniChannel.CoreDemoContext.Interfaces;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using ServiceStack.Messaging;
using ServiceStack.Caching;
using Demo.Audit.Model.Message;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannelCore.Api.Sdk.Common;

namespace Demo.OmniChannel.MappingProductGRPCServer.Services
{
    public class OmniChannelService : OmniChannelGrpc.OmniChannelGrpcBase
    {
        private static ILog _logger => LogManager.GetLogger(typeof(OmniChannelService));
        private readonly IAppSettings _appSettings;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly IOnHandBusiness _onHandBusiness;
        private readonly IPriceBusiness _priceBusiness;
        private readonly ISaleChannelInternalClient _saleChannelInternalClient;
        private readonly IDemoOmniChannelCoreContext _DemoOmniChannelCoreContext;
        private readonly ICacheClient _cacheClient;
        private readonly IMessageFactory _messageFactory;
        private readonly IMapper _mapper;
        private readonly IAuditTrailInternalClient _auditTrailInternalClient;

        public OmniChannelService(
            IAppSettings settings,
            IDbConnectionFactory dbConnectionFactory,
            IMapper mapper,
            IOnHandBusiness onHandBusiness,
            IPriceBusiness priceBusiness,
            ISaleChannelInternalClient saleChannelInternalClient,
            IDemoOmniChannelCoreContext DemoOmniChannelCoreContext,
            ICacheClient cacheClient,
            IMessageFactory messageFactory,
            IAuditTrailInternalClient auditTrailInternalClient
            )
        {
            _appSettings = settings;
            _dbConnectionFactory = dbConnectionFactory;
            _mapper = mapper;
            _onHandBusiness = onHandBusiness;
            _priceBusiness = priceBusiness;
            _saleChannelInternalClient = saleChannelInternalClient;
            _DemoOmniChannelCoreContext = DemoOmniChannelCoreContext;
            _cacheClient = cacheClient;
            _messageFactory = messageFactory;
            _auditTrailInternalClient = auditTrailInternalClient;
        }

        public override Task<PingResponse> Ping(PingRequest request, ServerCallContext context)
        {
            return Task.FromResult(new PingResponse() { Message = "Pong" });
        }

        public override async Task<OmniChannelResponse> GetRetailerById(OmniChannelRequest request, ServerCallContext context)
        {
            try
            {
                var parsedRequest = JsonSerializer.Deserialize<GetRetailerById>(request.RequestObject);
                if (parsedRequest == null)
                {
                    throw new OmniException($"Cannot Deserialize from {request.RequestObject}");
                }

                var retailer = await _DemoOmniChannelCoreContext.GetRetailer(parsedRequest.RetailerId);
                if (retailer == null)
                {
                    throw new OmniException($"KV retailer info is null with request {request.RequestObject}");
                }

                var kvRetailerDto = _mapper.Map<KvRetailer, KvRetailerDto>(retailer);
                string result = JsonSerializer.Serialize(kvRetailerDto);
                return GetResponseSuccess(result);
            }
            catch (Exception exception)
            {
                _logger.Error($"GetRetailerById: Request: {JsonSerializer.Deserialize<GetRetailerById>(request.RequestObject).ToSafeJson()} {exception.Message} - {exception.StackTrace}");
                throw new RpcException(new Status(StatusCode.Aborted, exception.ToString()));
            }
        }

        public override async Task<OmniChannelResponse> GetPosParameter(OmniChannelRequest request, ServerCallContext context)
        {
            try
            {
                var parsedRequest = JsonSerializer.Deserialize<GetPosParameter>(request.RequestObject);
                if (parsedRequest == null)
                {
                    throw new OmniException($"Cannot Deserialize from {request.RequestObject}");
                }

                var kvContext = await _DemoOmniChannelCoreContext.CreateContext(parsedRequest.RetailerId);
                var connectionStringName = kvContext.Group.ConnectionStringName;
                using (var dbShard = _dbConnectionFactory.Open(connectionStringName))
                {
                    var posSettingRepository = new PosSettingRepository(dbShard);
                    var posParameters = await posSettingRepository.GetSettingOmniChannel(parsedRequest.RetailerId, nameof(PosSetting.OmniChannel));
                    if (posParameters == null)
                    {
                        throw new OmniException($"POS parameter is null with request {request.RequestObject}");
                    }

                    var posParameterDto = _mapper.Map<PosParameter, PosParameterDto>(posParameters);
                    string result = JsonSerializer.Serialize(posParameterDto);
                    return GetResponseSuccess(result);
                }
            }
            catch (Exception exception)
            {
                _logger.Error($"GetPosParameter: Request: {JsonSerializer.Deserialize<GetPosParameter>(request.RequestObject).ToSafeJson()} {exception.Message} - {exception.StackTrace}");
                throw new RpcException(new Status(StatusCode.Aborted, exception.ToString()));
            }
        }

        public override async Task<OmniChannelResponse> SyncMultiOnHand(OmniChannelRequest request, ServerCallContext context)
        {
            try
            {
                var parsedRequest = JsonSerializer.Deserialize<SyncMultiOnhand>(request.RequestObject);
                if (parsedRequest == null)
                {
                    throw new OmniException($"Cannot Deserialize from {request.RequestObject}");
                }

                var kvContext = await _DemoOmniChannelCoreContext.CreateContext(parsedRequest.RetailerId);
                var connectStringName = kvContext.Group.ConnectionStringName;
                if (parsedRequest.ChannelProductIds != null && parsedRequest.ChannelProductIds.Any())
                {
                    var pageSize = _appSettings.Get<int>("multiSyncPageSize", 50);
                    var total = parsedRequest.ChannelProductIds.Count / pageSize;
                    total = parsedRequest.ChannelProductIds.Count % pageSize == 0 ? total : total + 1;
                    for (int i = 0; i < total; i++)
                    {
                        var currentPage = i * pageSize;
                        await _onHandBusiness.SyncMultiOnHand(connectStringName, parsedRequest.Channel,
                                parsedRequest.ChannelProductIds.Skip(currentPage).Take(pageSize).ToList(), null, parsedRequest.IsIgnoreAuditTrail, parsedRequest.LogId, null);
                    }
                    return GetResponseSuccess("SyncMultiOnHand successfully");
                }
                await _onHandBusiness.SyncMultiOnHand(connectStringName, parsedRequest.Channel,
                    null, null, parsedRequest.IsIgnoreAuditTrail, parsedRequest.LogId, null, true);

                _logger.Info($"SyncMultiOnHand successfully - RetailerId: {parsedRequest.RetailerId} | LogId: {parsedRequest.LogId} | ChannelId: {parsedRequest.Channel?.Id} | ChannelName: {parsedRequest.Channel?.Name}");
                return GetResponseSuccess("SyncMultiOnHand successfully");
            }
            catch (Exception exception)
            {
                _logger.Error($"SyncMultiOnHand: Request: {JsonSerializer.Deserialize<SyncMultiOnhand>(request.RequestObject).ToSafeJson()} {exception.Message} - {exception.StackTrace}");
                throw new RpcException(new Status(StatusCode.Aborted, exception.ToString()));
            }
        }

        public override async Task<OmniChannelResponse> SyncMultiPrice(OmniChannelRequest request, ServerCallContext context)
        {
            try
            {
                var parsedRequest = JsonSerializer.Deserialize<SyncMultiOnhand>(request.RequestObject);
                if (parsedRequest == null)
                {
                    throw new OmniException($"Cannot Deserialize from {request.RequestObject}");
                }

                var kvContext = await _DemoOmniChannelCoreContext.CreateContext(parsedRequest.RetailerId);
                var connectStringName = kvContext.Group.ConnectionStringName;
              
                if (parsedRequest.ChannelProductIds != null && parsedRequest.ChannelProductIds.Any())
                {
                    var pageSize = _appSettings.Get<int>("multiSyncPageSize", 50);
                    var total = parsedRequest.ChannelProductIds.Count / pageSize;
                    total = parsedRequest.ChannelProductIds.Count % pageSize == 0 ? total : total + 1;
                    for (int i = 0; i < total; i++)
                    {
                        var currentPage = i * pageSize;
                        await _priceBusiness.SyncMultiPrice(connectStringName, parsedRequest.Channel,
                            parsedRequest.ChannelProductIds.Skip(currentPage).Take(pageSize).ToList(), null, parsedRequest.IsIgnoreAuditTrail,
                            parsedRequest.LogId, false, 50, null, true, "Queue created from enable or SyncErrorChannel ");
                    }
                    return GetResponseSuccess("SyncMultiPrice successfully");
                }
                await _priceBusiness.SyncMultiPrice(connectStringName, parsedRequest.Channel, null, null, parsedRequest.IsIgnoreAuditTrail, parsedRequest.LogId);

                _logger.Info($"SyncMultiPrice successfully - RetailerId: {parsedRequest.RetailerId} | LogId: {parsedRequest.LogId} | ChannelId: {parsedRequest.Channel?.Id} | ChannelName: {parsedRequest.Channel?.Name}");
                return GetResponseSuccess("SyncMultiPrice successfully");
            }
            catch (Exception exception)
            {
                _logger.Error($"SyncMultiPrice: Request: {JsonSerializer.Deserialize<SyncMultiOnhand>(request.RequestObject).ToSafeJson()} {exception.Message} - {exception.StackTrace}");
                throw new RpcException(new Status(StatusCode.Aborted, exception.ToString()));
            }
        }

        public override async Task<OmniChannelResponse> CreateChannelAuditTrail(OmniChannelRequest request, ServerCallContext context)
        {
            try
            {
                var parsedRequest = JsonSerializer.Deserialize<CreateChannelAuditTrail>(request.RequestObject);
                if (parsedRequest == null)
                {
                    throw new OmniException($"Cannot Deserialize from {request.RequestObject}");
                }

                var req = parsedRequest.Request ?? new ChannelCreateOrUpdateRequest();
                var channel = req.Channel;
                var isNewChannel = parsedRequest.IsNewChanel;
                var existChannel = parsedRequest.ExistChannel ?? new Domain.Model.OmniChannel();
                var existSchedules = parsedRequest.ExistSchedules ?? new List<OmniChannelSchedule>();
                var existSettings = parsedRequest.ExistSettings ?? new OmniChannelSettingObject();
                if (req.IsIgnoreAuditTrail)
                {
                    throw new OmniException("Request ignore audit trail");
                }

                var channelNames = channel.Name;
                if (!string.IsNullOrEmpty(channel.Email))
                {
                    channelNames += $" - {channel.Email}";
                }
                var logContent = new StringBuilder($"Thiết lập kết nối {Enum.GetName(typeof(ChannelType), channel.Type)}: {channelNames}<br/>");

                var kvContext = await _DemoOmniChannelCoreContext.CreateContext(parsedRequest.RetailerId);
                var connectStringName = kvContext.Group.ConnectionStringName;
                //Chi nhánh đồng bộ
                using (var dbShard = _dbConnectionFactory.Open(connectStringName))
                {
                    var branchRepository = new BranchRepository(dbShard);
                    var newBranchName = (await branchRepository.GetByIdAsync(channel.BranchId))?.Name;
                    if (isNewChannel || existChannel == null || channel.BranchId == existChannel?.BranchId)
                    {
                        logContent.AppendFormat("Chi nhánh đồng bộ: {0} <br/>", newBranchName ?? "");
                    }
                    else
                    {
                        var oldBranchName = (await branchRepository.GetByIdAsync(existChannel?.BranchId))?.Name;
                        logContent.AppendFormat("Chi nhánh đồng bộ: {0} -> {1} <br/>", oldBranchName ?? "", newBranchName ?? "");
                    }
                }

                // Đồng bộ số lượng bán
                bool existSyncOnHand = existSchedules.Any(x => x.Type == (int)ScheduleType.SyncOnhand);
                var syncOnHandLogContent = string.Empty;
                syncOnHandLogContent = HandleSyncOnHandAuditTrail(req, existChannel, isNewChannel, existSyncOnHand);
                logContent.AppendFormat("Đồng bộ số lượng bán: {0} <br/>", syncOnHandLogContent);
                // Phân bổ số lượng bán
                if (req.IsSyncOnHand)
                {
                    var isDistributeMultiMapping = existSettings != null && existSettings.DistributeMultiMapping ? "Có" : "Không";
                    if (channel.OmniChannelSettings?.DistributeMultiMapping != null && 
                        channel.OmniChannelSettings?.DistributeMultiMapping != existSettings?.DistributeMultiMapping)
                    {
                        isDistributeMultiMapping = existSettings != null && existSettings.DistributeMultiMapping ? "Có -> Không" : "Không -> Có";
                    }
                    logContent.AppendFormat(
                        $"Phân bổ số lượng bán của hàng nhiều liên kết: {isDistributeMultiMapping}<br/>");
                }

                // Đồng bộ giá bán
                bool existSyncPrice = existSchedules.Any(x => x.Type == (int)ScheduleType.SyncPrice);
                var syncPriceLogContent = string.Empty;
                using (var dbShard = _dbConnectionFactory.Open(connectStringName))
                {
                    var priceBookRepository = new PriceBookRepository(dbShard);
                    syncPriceLogContent = await HandleSyncPriceAuditTrailAsync(req, existChannel, priceBookRepository, isNewChannel, existSyncPrice);
                }
                logContent.AppendFormat("Đồng bộ giá bán: {0} <br/>", syncPriceLogContent);

                //Đồng bộ đơn hàng
                bool existSyncOrder = existSchedules.Any(x => x.Type == (int)ScheduleType.SyncOrder);
                var syncOrtherChangeStatus = "";
                var syncOrderReturningStatus = string.Empty;
                var syncOrderAutoSyncBatchExpire = string.Empty;
                var syncOrderSaleTime = string.Empty;
                var syncOrderAutoCreatedProduct = string.Empty;
                HandleSyncOrderAuditTrailAsync(req, existChannel, existSettings, existSyncOrder,
                    isNewChannel, ref syncOrtherChangeStatus, ref syncOrderReturningStatus, 
                    ref syncOrderAutoSyncBatchExpire, ref syncOrderSaleTime, ref syncOrderAutoCreatedProduct);

                logContent.AppendFormat(
                    $"Đồng bộ đơn hàng: {syncOrtherChangeStatus}" +
                    $"{(!string.IsNullOrEmpty(syncOrderReturningStatus) ? ", xác nhận đã chuyển hoàn: " + syncOrderReturningStatus : string.Empty)}" +
                    $"{(!string.IsNullOrEmpty(syncOrderAutoSyncBatchExpire) ? ", tự động chọn hàng lô date khi tạo hóa đơn: " + syncOrderAutoSyncBatchExpire : string.Empty)}" +
                    $"{(!string.IsNullOrEmpty(syncOrderAutoSyncBatchExpire) ? ", thời gian bán: " + syncOrderSaleTime : string.Empty)}" +
                    $"{(!string.IsNullOrEmpty(syncOrderAutoCreatedProduct) ? ", Khi đơn hàng có hàng hóa chưa liên kết: " + syncOrderAutoCreatedProduct : string.Empty)} <br/>");

                // Tự động xóa mapping
                var isAutoDeleteMappingOld = existChannel?.IsAutoDeleteMapping ?? false;
                var isAutoDeleteMappingNew = channel.IsAutoDeleteMapping ?? false;
                string isAutoDeleteMappingText = isAutoDeleteMappingNew ? "Có" : "Không";
                if (existChannel != null && isAutoDeleteMappingOld != isAutoDeleteMappingNew)
                {
                    isAutoDeleteMappingText = isAutoDeleteMappingNew ? "Không -> Có" : "Có -> Không";
                }

                logContent.AppendFormat($"Xóa liên kết hàng hóa nếu Mã hàng - SKU thay đổi: {isAutoDeleteMappingText}<br/>");

                // Đồng bộ mặt hàng khi trùng SKU
                var isAutoMappingProductOld = existChannel?.IsAutoMappingProduct ?? false;
                var isAutoMappingProductNew = channel.IsAutoMappingProduct;
                string isAutoMappingProductText = isAutoMappingProductNew ? "Có" : "Không";
                if (existChannel != null && isAutoMappingProductOld != isAutoMappingProductNew)
                {
                    isAutoMappingProductText = isAutoMappingProductNew ? "Không -> Có" : "Có -> Không";
                }
                logContent.AppendFormat($"Tự động liên kết trùng SKU: {isAutoMappingProductText}<br/>");

                var isAutoCopyProductOld = existSettings?.IsAutoCopyProduct ?? false;
                var isAutoCopyProductNew = channel.OmniChannelSettings?.IsAutoCopyProduct ?? false;
                string isAutoCopyProductText = isAutoCopyProductNew ? "Có" : "Không";
                if (existChannel != null && isAutoCopyProductOld != isAutoCopyProductNew)
                {
                    isAutoCopyProductText = isAutoCopyProductNew ? "Không -> Có" : "Có -> Không";
                }
                logContent.AppendFormat(
                        $"Tự động sao chép hàng hóa khi có hàng hóa mới tạo trên sàn: {isAutoCopyProductText}<br/>");

                var log = new AuditTrailLog
                {
                    FunctionId = (int)FunctionType.PosParameter,
                    Action = isNewChannel ? (int)AuditTrailAction.Create : (int)AuditTrailAction.Update,
                    Content = logContent.ToString(),
                    CreatedDate = DateTime.Now,
                    BranchId = channel.BranchId
                };
                var executionContext = await ContextHelper.GetExecutionContext(_cacheClient, _dbConnectionFactory, parsedRequest.RetailerId, connectStringName, channel.BranchId, _appSettings.Get<int>("ExecutionContext"));
                var coreContext = executionContext.ConvertTo<KvInternalContext>();
                coreContext.UserId = executionContext.User?.Id ?? 0;
                await _auditTrailInternalClient.AddLogAsync(coreContext, log);

                _logger.Info($"CreateChannelAuditTrail successfully - RetailerId: {parsedRequest.RetailerId} | ChannelName: {channel.Name} | ChannelType: {channel.Type}");
                return GetResponseSuccess("CreateChannelAuditTrail successfully");
            }
            catch (Exception exception)
            {
                _logger.Error($"CreateChannelAuditTrail: Request: {JsonSerializer.Deserialize<CreateChannelAuditTrail>(request.RequestObject).ToSafeJson()} {exception.Message} - {exception.StackTrace}");
                throw new RpcException(new Status(StatusCode.Aborted, exception.ToString()));
            }
        }

        public override async Task<OmniChannelResponse> GetBranchById(OmniChannelRequest request, ServerCallContext context)
        {
            try
            {
                var parsedRequest = JsonSerializer.Deserialize<GetBranchById>(request.RequestObject);
                if (parsedRequest == null)
                {
                    throw new OmniException($"GetBranchById cannot deserialize from {request.RequestObject}");
                }

                var kvContext = await _DemoOmniChannelCoreContext.CreateContext(parsedRequest.RetailerId);
                var connectStringName = kvContext.Group.ConnectionStringName;
                using (var dbShard = _dbConnectionFactory.Open(connectStringName))
                {
                    var branchRepository = new BranchRepository(dbShard);
                    var branch = await branchRepository.GetByIdAsync(parsedRequest.BranchId);
                    if (branch == null)
                    {
                        throw new OmniException($"GetBranchById return null with request: {request.RequestObject}");
                    }

                    string result = JsonSerializer.Serialize(branch);
                    return GetResponseSuccess(result);
                }
            }
            catch (Exception exception)
            {
                _logger.Error($"GetBranchById: Request: {JsonSerializer.Deserialize<GetBranchById>(request.RequestObject).ToSafeJson()} {exception.Message} - {exception.StackTrace}");
                throw new RpcException(new Status(StatusCode.Aborted, exception.ToString()));
            }
        }

        public override async Task<OmniChannelResponse> GetPosSetting(OmniChannelRequest request, ServerCallContext context)
        {
            try
            {
                var parsedRequest = JsonSerializer.Deserialize<GetPosSetting>(request.RequestObject);
                if (parsedRequest == null)
                {
                    throw new OmniException($"GetPosSetting cannot deserialize from {request.RequestObject}");
                }

                var kvContext = await _DemoOmniChannelCoreContext.CreateContext(parsedRequest.RetailerId);
                var connectionStringName = kvContext.Group.ConnectionStringName;
                using (var dbShard = _dbConnectionFactory.Open(connectionStringName))
                {
                    var posSettingRepository = new PosSettingRepository(dbShard);
                    var posSettingDict = await posSettingRepository.GetSettingAsync(parsedRequest.RetailerId);
                    if (posSettingDict == null)
                    {
                        throw new OmniException("PosSetting is null");
                    }

                    string result = JsonSerializer.Serialize(posSettingDict);
                    return GetResponseSuccess(result);
                }
            }
            catch (Exception exception)
            {
                _logger.Error($"GetPosSetting: Request: {JsonSerializer.Deserialize<GetPosSetting>(request.RequestObject).ToSafeJson()} {exception.Message} - {exception.StackTrace}");
                throw new RpcException(new Status(StatusCode.Aborted, exception.ToString()));
            }
        }

        public override async Task<OmniChannelResponse> CreateOmniChannelCoreContext(OmniChannelRequest request, ServerCallContext context)
        {
            try
            {
                var parsedRequest = JsonSerializer.Deserialize<CreateOmniChannelCoreContext>(request.RequestObject);
                if (parsedRequest == null)
                {
                    throw new OmniException($"CreateOmniChannelCoreContext cannot deserialize from {request.RequestObject}");
                }

                var kvContext = await _DemoOmniChannelCoreContext.CreateContext(parsedRequest.RetailerId);
                var connectionStringName = kvContext.Group.ConnectionStringName;
                var coreContext = await _DemoOmniChannelCoreContext.CreateOmniChannelCoreContext(parsedRequest.RetailerId, parsedRequest.BranchId, connectionStringName);
                int saleChannelId = await _saleChannelInternalClient.CreateOrUpdateSaleChannel(coreContext, parsedRequest.RetailerId, parsedRequest.ChannelType, parsedRequest.ChannelId, parsedRequest.ChannelName);
                SaleChannelDto saleChannelDto = new SaleChannelDto(saleChannelId);
                string result = JsonSerializer.Serialize(saleChannelDto);

                _logger.Info($"CreateOmniChannelCoreContext successfully - RetailerId: {parsedRequest.RetailerId} | ChannelId: {parsedRequest.ChannelId} | ChannelName: {parsedRequest.ChannelName} | ChannelType: {parsedRequest.ChannelType}");
                return GetResponseSuccess(result);
            }
            catch (Exception exception)
            {
                _logger.Error($"CreateOmniChannelCoreContext: Request: {JsonSerializer.Deserialize<GetPosSetting>(request.RequestObject).ToSafeJson()} {exception.Message} - {exception.StackTrace}");
                throw new RpcException(new Status(StatusCode.Aborted, exception.ToString()));
            }
        }

        public override async Task<OmniChannelResponse> SyncProduct(OmniChannelRequest request, ServerCallContext context)
        {
            try
            {
                var parsedRequest = JsonSerializer.Deserialize<SyncProduct>(request.RequestObject);
                if (parsedRequest == null)
                {
                    throw new OmniException($"SyncProduct cannot deserialize from {request.RequestObject}");
                }

                var kvContext = await _DemoOmniChannelCoreContext.CreateContext(parsedRequest.RetailerId);
                var connectionStringName = kvContext.Group.ConnectionStringName;
                var message = new SyncProductMessage
                {
                    ChannelId = parsedRequest.ChannelId,
                    ChannelType = parsedRequest.ChannelType,
                    RetailerId = parsedRequest.RetailerId,
                    BranchId = parsedRequest.BranchId,
                    RetailerCode = parsedRequest.RetailerCode,
                    KvEntities = connectionStringName,
                    IsIgnoreAuditTrail = parsedRequest.IsIgnoreAuditTrail,
                    SendTime = Helpers.GmtToVietNamTimeZone(DateTime.UtcNow),
                    LogId = parsedRequest.LogId,
                    IsUpdateChannel = parsedRequest.IsUpdateChannel,
                };

                var cacheKey = string.Format(KvConstant.IsRunningGetProductKey, parsedRequest.ChannelId);
                _cacheClient.Set(cacheKey, true, TimeSpan.FromMinutes(2));

                using (var mq = _messageFactory.CreateMessageProducer())
                {
                    mq.Publish(message);
                }

                _logger.Info($"SyncProduct successfully - RetailerId: {parsedRequest.RetailerId} | LogId: {parsedRequest.LogId} | ChannelId: {parsedRequest.ChannelId}");
                return GetResponseSuccess("Publish Sync Product message successfully");
            }
            catch (Exception exception)
            {
                _logger.Error($"SyncProduct: Request: {JsonSerializer.Deserialize<SyncProduct>(request.RequestObject).ToSafeJson()} {exception.Message} - {exception.StackTrace}");
                throw new RpcException(new Status(StatusCode.Aborted, exception.ToString()));
            }
        }

        public override async Task<OmniChannelResponse> EnableChannelAuditTrail(OmniChannelRequest request, ServerCallContext context)
        {
            try
            {
                var parsedRequest = JsonSerializer.Deserialize<EnableChannelAuditTrail>(request.RequestObject);
                if (parsedRequest == null)
                {
                    throw new OmniException($"Cannot Deserialize from {request.RequestObject}");
                }

                var kvContext = await _DemoOmniChannelCoreContext.CreateContext(parsedRequest.RetailerId);
                var connectStringName = kvContext.Group.ConnectionStringName;
                var executionContext = await ContextHelper.GetExecutionContext(_cacheClient, _dbConnectionFactory, parsedRequest.RetailerId, connectStringName, parsedRequest.BranchId, _appSettings.Get<int>("ExecutionContext"));
                var coreContext = executionContext.ConvertTo<KvInternalContext>();
                coreContext.UserId = executionContext.User?.Id ?? 0;

                var log = new AuditTrailLog
                {
                    FunctionId = (int)FunctionType.PosParameter,
                    Action = (int)AuditTrailAction.Update,
                    Content = parsedRequest.LogContent,
                    CreatedDate = DateTime.Now,
                    BranchId = parsedRequest.BranchId
                };
                await _auditTrailInternalClient.AddLogAsync(coreContext, log);

                
                _logger.Info($"EnableChannelAuditTrail successfully - RetailerId: {parsedRequest.RetailerId}");
                return GetResponseSuccess("EnableChannelAuditTrail successfully");
            }
            catch (Exception exception)
            {
                _logger.Error($"EnableChannelAuditTrail: Request: {JsonSerializer.Deserialize<EnableChannelAuditTrail>(request.RequestObject).ToSafeJson()} {exception.Message} - {exception.StackTrace}");
                throw new RpcException(new Status(StatusCode.Aborted, exception.ToString()));
            }
        }

        private static OmniChannelResponse GetResponseSuccess(string result)
        {
            return new OmniChannelResponse
            {
                Error = "0",
                Result = result
            };
        }

        private static string HandleSyncOnHandAuditTrail(ChannelCreateOrUpdateRequest req, Domain.Model.OmniChannel? existChannel, bool isNewChanel, bool existSyncOnHand)
        {
            var syncOnHandLogContent = string.Empty;
            if (!req.IsSyncOnHand)
            {
                syncOnHandLogContent = (existSyncOnHand ? "Có -> " : "") + "Không";
            }
            else
            {
                if (isNewChanel)
                {
                    syncOnHandLogContent = $"Có, số lượng bán = {EnumHelper.ToDescription((SyncOnHandFormula)req.Channel.SyncOnHandFormula)}";
                }
                else
                {
                    if (!existSyncOnHand)
                    {
                        syncOnHandLogContent = $"Không -> Có, số lượng bán = {EnumHelper.ToDescription((SyncOnHandFormula)req.Channel.SyncOnHandFormula)}";
                    }
                    else
                    {
                        syncOnHandLogContent += existChannel != null
                            ? $"Có, số lượng bán = {EnumHelper.ToDescription((SyncOnHandFormula)existChannel.SyncOnHandFormula)}"
                            : "";

                        if (req.Channel.SyncOnHandFormula != existChannel?.SyncOnHandFormula)
                        {
                            syncOnHandLogContent += $" -> {EnumHelper.ToDescription((SyncOnHandFormula)req.Channel.SyncOnHandFormula)}";
                        }
                    }
                }
            }
            return syncOnHandLogContent;
        }

        private static async Task<string> HandleSyncPriceAuditTrailAsync(ChannelCreateOrUpdateRequest req, Domain.Model.OmniChannel? existChannel, PriceBookRepository priceBookRepository, bool isNewChanel, bool existSyncPrice)
        {
            var syncPriceLogContent = string.Empty;
            if (!req.IsSyncPrice)
            {
                syncPriceLogContent = (existSyncPrice ? "Có -> " : "") + "Không";
                return syncPriceLogContent;
            }

            //Case: IsSyncPrice Handle new channel
            Domain.Model.OmniChannel priceBookParams = new Domain.Model.OmniChannel();
            priceBookParams.BasePriceBookId = req.Channel.BasePriceBookId;
            priceBookParams.PriceBookId = req.Channel.PriceBookId;
            priceBookParams.Type = req.Channel.Type;
            var (newBasePriceBookName, newPriceBookName) = await priceBookRepository.GetChannelPriceBookName(priceBookParams);
            if (isNewChanel)
            {
                syncPriceLogContent = $"Có, Bảng giá bán = {newBasePriceBookName}";
                if (req.Channel.PriceBookId.HasValue && !string.IsNullOrEmpty(newPriceBookName))
                {
                    syncPriceLogContent += $", Bảng giá khuyến mại = {newPriceBookName}";
                }

                return syncPriceLogContent;
            }

            //Case: IsSyncPrice with exist channel
            if (!existSyncPrice)
            {
                syncPriceLogContent = $"Không -> Có, Bảng giá bán = {newBasePriceBookName}";
                if (req.Channel.PriceBookId.HasValue && !string.IsNullOrEmpty(newPriceBookName))
                {
                    syncPriceLogContent += $", Bảng giá khuyến mại = {newPriceBookName}";
                }
            }
            else
            {
                var (oldBasePriceBookName, oldPriceBookName) = await priceBookRepository.GetChannelPriceBookName(existChannel);
                syncPriceLogContent = $"Có, Bảng giá bán = {oldBasePriceBookName}";
                if (req.Channel.BasePriceBookId != existChannel?.BasePriceBookId)
                {
                    syncPriceLogContent += $" -> {newBasePriceBookName}";
                }
                if (req.Channel.PriceBookId.HasValue && !string.IsNullOrEmpty(oldPriceBookName))
                {
                    syncPriceLogContent += $", Bảng giá khuyến mại = {oldPriceBookName}";
                    if (req.Channel.PriceBookId != existChannel?.PriceBookId)
                    {
                        syncPriceLogContent += $" -> {newPriceBookName}";
                    }
                }
            }
            return syncPriceLogContent;
        }

        private static void HandleSyncOrderAuditTrailAsync(ChannelCreateOrUpdateRequest req,
            Domain.Model.OmniChannel? existChannel,
            OmniChannelSettingObject existSettings, bool existSyncOrder,
            bool isNewChanel, ref string syncOrtherChangeStatus,
            ref string syncOrderReturningStatus,
            ref string syncOrderAutoSyncBatchExpire,
            ref string syncOrderSaleTime,
            ref string syncOrderAutoCreatedProduct)
        {
            if (!isNewChanel && existSyncOrder != req.IsSyncOrder)
            {
                syncOrtherChangeStatus = req.IsSyncOrder ? "Không -> Có" : "Có -> Không";
            }
            else
            {
                syncOrtherChangeStatus = req.IsSyncOrder ? "Có" : "Không";
            }

            HandleSyncOrderWithFormula(req, existChannel, existSettings, ref syncOrtherChangeStatus);

            HandleSyncOrderWithOutFormula(req, isNewChanel, existSettings, existSyncOrder,
                ref syncOrderReturningStatus, ref syncOrderAutoSyncBatchExpire, ref syncOrderSaleTime,
                ref syncOrderAutoCreatedProduct);
        }

        private static void HandleSyncOrderWithFormula(ChannelCreateOrUpdateRequest req, Domain.Model.OmniChannel? existChannel,
            OmniChannelSettingObject existSettings, ref string syncOrtherChangeStatus)
        {
            if (!req.IsSyncOrder || req.Channel.SyncOrderFormula == null) return;

            var syncOrderFormulaTypeReq = req.Channel.OmniChannelSettings?.SyncOrderFormulaType
                    ?? (byte)SyncOrderFormulaType.CreatedUpdate;

            var syncOrderFormulaReq =
                req.Channel.SyncOrderFormula ?? 30;

            if (existChannel != null && existChannel.SyncOrderFormula != null && 
                (existSettings.SyncOrderFormulaType != syncOrderFormulaTypeReq || existChannel.SyncOrderFormula != syncOrderFormulaReq))
            {
                syncOrtherChangeStatus += $", đồng bộ đơn hàng " +
                $"{EnumHelper.ToDescription((SyncOrderFormulaType)existSettings.SyncOrderFormulaType)} " +
                $"{EnumHelper.ToDescription((SyncOrderFormula)existChannel.SyncOrderFormula)}" +
                $" -> " +
                $"{EnumHelper.ToDescription((SyncOrderFormulaType)syncOrderFormulaTypeReq)} " +
                $"{EnumHelper.ToDescription((SyncOrderFormula)syncOrderFormulaReq)}";
            } else
            {
                syncOrtherChangeStatus += $", đồng bộ đơn hàng " +
                   $"{EnumHelper.ToDescription((SyncOrderFormulaType)syncOrderFormulaTypeReq)} " +
                   $"{EnumHelper.ToDescription((SyncOrderFormula)syncOrderFormulaReq)}";
            }
        }

        private static void HandleSyncOrderWithOutFormula(ChannelCreateOrUpdateRequest req, bool isNewChanel,
            OmniChannelSettingObject existSettings,
            bool existSyncOrder,
            ref string syncOrderReturningStatus,
            ref string syncOrderAutoSyncBatchExpire,
            ref string syncOrderSaleTime,
            ref string syncOrderAutoCreatedProduct)
        {
            if (!req.IsSyncOrder) return;
            syncOrderAutoSyncBatchExpire = req.Channel.OmniChannelSettings?.IsAutoSyncBatchExpire ?? false ? "Có" : "Không";
            if (req.Channel.Type == (int)ChannelType.Shopee || req.Channel.Type == (int)ChannelType.Lazada
                || req.Channel.Type == (int)ChannelType.Tiktok)
                syncOrderReturningStatus =
                    req.Channel.OmniChannelSettings?.IsConfirmReturning ?? false ? "Có" : "Không";

            syncOrderSaleTime = $"{EnumHelper.ToDescription((SyncOrderSaleTimeType)(req.Channel.OmniChannelSettings?.SyncOrderSaleTimeType ?? 0))} ";

            if (req.Channel.Type == (int)ChannelType.Shopee)
                syncOrderAutoCreatedProduct =
                    $"{EnumHelper.ToDescription((SyncOrderAutoCreateProduct)((req.Channel.OmniChannelSettings?.IsAutoCreatingProduct ?? false) ? 1 : 0))} ";

            if (isNewChanel || !existSyncOrder) return;
            if (existSettings.IsAutoSyncBatchExpire != req.Channel.OmniChannelSettings?.IsAutoSyncBatchExpire)
            {
                syncOrderAutoSyncBatchExpire = req.Channel.OmniChannelSettings?.IsAutoSyncBatchExpire ?? false
                    ? $"Không -> Có"
                    : $"Có -> Không";
            }

            if (existSettings.IsConfirmReturning != req.Channel.OmniChannelSettings?.IsConfirmReturning)
            {
                syncOrderReturningStatus =
                    req.Channel.OmniChannelSettings?.IsConfirmReturning ?? false
                        ? $"Không -> Có"
                        : $"Có -> Không";
            }

            if (existSettings.SyncOrderSaleTimeType != req.Channel.OmniChannelSettings?.SyncOrderSaleTimeType)
            {
                syncOrderSaleTime =
                    $"{EnumHelper.ToDescription((SyncOrderSaleTimeType)existSettings.SyncOrderSaleTimeType)} " +
                    $" -> " +
                    $"{EnumHelper.ToDescription((SyncOrderSaleTimeType)(req.Channel.OmniChannelSettings?.SyncOrderSaleTimeType ?? 0))}";
            }

            if (req.Channel.Type == (int)ChannelType.Shopee &&
                existSettings.IsAutoCreatingProduct != req.Channel.OmniChannelSettings?.IsAutoCreatingProduct)
            {
                syncOrderAutoCreatedProduct =
                    $"{EnumHelper.ToDescription((SyncOrderAutoCreateProduct)(existSettings.IsAutoCreatingProduct ? 1 : 0))} " +
                    $" -> " +
                    $"{EnumHelper.ToDescription((SyncOrderAutoCreateProduct)((req.Channel.OmniChannelSettings?.IsAutoCreatingProduct ?? false) ? 1 : 0))}";
            }
        }
    }
}

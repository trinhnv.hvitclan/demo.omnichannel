﻿using Grpc.Core;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.FeatureToggle;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.MappingProductGRPCServerSyncEs;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Newtonsoft.Json;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using ServiceStack.Redis;

namespace Demo.OmniChannel.MappingProductGRPCServer.Services
{
    public class SyncEsSubcribeService: SyncEs.SyncEsBase
    {
        protected ILog Logger => LogManager.GetLogger(typeof(SyncEsSubcribeService));
        protected IMessageService MessageService;
        protected IRedisClientsManager RedisClientsManager;
        protected IAppSettings Settings;
        protected IDbConnectionFactory DbConnectionFactory;

        public SyncEsSubcribeService(IMessageService messageService, IDbConnectionFactory dbConnectionFactory,
            IRedisClientsManager redisClientsManager, IAppSettings settings)
        {
            DbConnectionFactory = dbConnectionFactory;
            RedisClientsManager = redisClientsManager;
            MessageService = messageService;
            Settings = settings;
        }

        public override Task<MappingProductGRPCServerSyncEs.PingMappingResponse> PingMapping(MappingProductGRPCServerSyncEs.PingMappingRequest request, ServerCallContext context)
        {
            return Task.FromResult(new MappingProductGRPCServerSyncEs.PingMappingResponse() { Message = "Pong" });
        }

        public override async Task<SyncEsKafkaMessageResponse> GetSyncEsKafka(SyncEsKafkaMessageRequest request, ServerCallContext context)
        {
            try
            {
                //Parse message
                var data = JsonConvert.DeserializeObject<SyncEsMessage<Domain.Model.ProductSubscribe>>(request.RequestObject);
                if (data == null)
                {
                    throw new OmniException($"Cannot Deserialize from {request.RequestObject}");
                }

                var logId = Guid.NewGuid();
                var logProcessSyncEsMessage = new LogObject(Logger, logId)
                {
                    Action = "GrpcSyncEsSubscribeService",
                    RetailerId = data.RetailerId,
                    RequestObject = data.ToSafeJson()
                };
                var isWriteLog = Settings?.Get<bool?>("WriteLogProcessMessage") ?? false;
                var groupResult = await GetConnectionString(data.RetailerId);

                if (await ProcessMessage(data, isWriteLog))
                {
                    logProcessSyncEsMessage.Description = "ProcessMessageFALSE";
                    logProcessSyncEsMessage.LogInfo();
                    return new SyncEsKafkaMessageResponse { Result = false };
                }

                using (var client = RedisClientsManager.GetClient())
                {
                    var configMappingV2Feature = Settings.Get<MappingV2Feature>("MappingV2Feature");
                    if (configMappingV2Feature.IsValid(groupResult.groupId ?? 0, data.RetailerId))
                    {
                        logProcessSyncEsMessage.Description = $"Retailer {data.RetailerId} will use mapping v2. Continue...";
                        logProcessSyncEsMessage.LogInfo();
                    }
                    else
                    {
                        logProcessSyncEsMessage.Description = $"Retailer {data.RetailerId} will use mapping v1. Exit...";
                        logProcessSyncEsMessage.LogInfo();
                        return new SyncEsKafkaMessageResponse { Result = true };
                    }
                }

                switch (data.Type)
                {
                    case (byte)EsEventType.Product:
                        {
                            if (data.Advice == (byte)TrackingAdvice.Remove)
                            {
                                if (!string.IsNullOrEmpty(data.Ids))
                                {
                                    logProcessSyncEsMessage.Description = "ProcessRemoveMessageBegin";
                                    logProcessSyncEsMessage.LogInfo();
                                    var productIds = data.Ids.Split(",").Select(long.Parse).ToList();
                                    var removeMessage = new ProcessRemoveMessage<Domain.Model.ProductSubscribe>
                                    {
                                        RetailerId = data.RetailerId,
                                        BranchId = data.BranchId,
                                        SentTime = data.SentTime,
                                        ProductIds = productIds,
                                        ConnectionString = data.ConnectionString,
                                        LogId = logId
                                    };
                                    using (var msg = MessageService.MessageFactory.CreateMessageProducer())
                                    {
                                        msg.Publish(removeMessage);
                                    }
                                }
                                break;
                            }
                            if (data.EventName == EsEventName.UpdateStock)
                            {
                                logProcessSyncEsMessage.Description = "ProcessStockMessageBegin";
                                logProcessSyncEsMessage.LogInfo();
                                var stockUpdateMessage = new ProcessStockMessage<Domain.Model.ProductSubscribe>
                                {
                                    RetailerId = data.RetailerId,
                                    BranchId = data.BranchId,
                                    SentTime = data.SentTime,
                                    Products = data.Products,
                                    ConnectionString = data.ConnectionString,
                                    LogId = logId
                                };
                                if (new ProcessStockMessageV2Toggle(Settings).Enable(data.RetailerId, groupResult.groupId ?? 0))
                                {
                                    using (var msg = MessageService.MessageFactory.CreateMessageQueueClient())
                                    {
                                        var mess = new Message { CreatedDate = DateTime.Now, Id = Guid.NewGuid() };
                                        mess.Body = stockUpdateMessage;
                                        msg.Publish($"mq:{Settings.Get("ProcessStockMessageV2_Topic", "ProcessStockMessageV2_Topic")}.inq", mess);
                                    }

                                } else
                                {
                                    using (var msg = MessageService.MessageFactory.CreateMessageProducer())
                                    {
                                        msg.Publish(stockUpdateMessage);
                                    }
                                }
                                
                                break;
                            }

                            logProcessSyncEsMessage.Description = "updatePriceMessageBEGIN";
                            logProcessSyncEsMessage.LogInfo();

                            var updatePriceMessage = new ProcessPriceMessage<Domain.Model.ProductSubscribe>
                            {
                                RetailerId = data.RetailerId,
                                BranchId = data.BranchId,
                                SentTime = data.SentTime,
                                Products = data.Products,
                                PriceBookId = 0,
                                ConnectionString = data.ConnectionString,
                                LogId = logId,
                                IsFromPriceBook = false
                            };
                            using (var msg = MessageService.MessageFactory.CreateMessageProducer())
                            {
                                msg.Publish(updatePriceMessage);
                            }
                            break;
                        }

                }
                if (isWriteLog)
                {
                    logProcessSyncEsMessage.LogInfo();
                }
                return new SyncEsKafkaMessageResponse { Result = true };

            }
            catch (Exception exception)
            {
                throw new RpcException(new Status(StatusCode.Internal, exception.ToString()));
            }
        }


        private async Task<bool> ProcessMessage(SyncEsMessage<Domain.Model.ProductSubscribe> data, bool isWriteLog = false)
        {
            if (data.RetailerId <= 0)
            {
                return false;
            }
            var logFilterRetailer = new LogObject(Logger, data.LogId)
            {
                Action = "FilterByRetailer",
                RetailerId = data.RetailerId,
                ResponseObject = data
            };
            var isValid = await ValidateByRetailer(data.RetailerId);

            if (isWriteLog)
            {
                logFilterRetailer.LogInfo();
            }

            return !isValid;
        }

        protected async Task<(string connectionString, int? groupId)> GetConnectionString(long retailerId)
        {
            var retailer = await GetRetailer(retailerId);
            if (retailer == null) return (null, null);
            var kvGroup = await GetRetailerGroup(retailer.GroupId);
            if (kvGroup == null) return (null, null);
            if (string.IsNullOrEmpty(kvGroup.ConnectionString))
            {
                return (null, null);
            }
            return (kvGroup.ConnectionString.Substring(kvGroup.ConnectionString.IndexOf('=') + 1), kvGroup.Id);
        }


        private async Task<KvRetailer> GetRetailer(long retailerId)
        {
            using (var client = RedisClientsManager.GetClient())
            {
                var kvRetailer = client.Get<KvRetailer>(string.Format(KvConstant.RetailerIdCacheKey, retailerId));
                if (kvRetailer == null)
                {
                    using (var dbMaster = await DbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities))
                    {
                        using (_ = dbMaster.OpenTransaction(System.Data.IsolationLevel.ReadUncommitted))
                        {
                            kvRetailer = await dbMaster.SingleAsync<KvRetailer>(x => x.Id == retailerId);
                            if (kvRetailer != null)
                            {
                                client.Set(string.Format(KvConstant.RetailerIdCacheKey, retailerId), kvRetailer, DateTime.Now.AddDays(7));
                            }
                        }
                    }
                }
                return kvRetailer;
            }
        }

        private async Task<Domain.Common.KvGroup> GetRetailerGroup(int groupId)
        {
            using (var client = RedisClientsManager.GetClient())
            {
                var kvGroup = client.Get<Domain.Common.KvGroup>(string.Format(KvConstant.KvGroupCacheKey, groupId));
                if (kvGroup == null)
                {
                    using (var dbMaster = await DbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities))
                    {
                        using (_ = dbMaster.OpenTransaction(System.Data.IsolationLevel.ReadUncommitted))
                        {
                            kvGroup = await dbMaster.SingleAsync<Domain.Common.KvGroup>(x => x.Id == groupId);
                            if (kvGroup != null)
                            {
                                client.Set(string.Format(KvConstant.KvGroupCacheKey, groupId), kvGroup, DateTime.Now.AddDays(7));
                            }
                        }
                    }
                }
                return kvGroup;
            }
        }

        private async Task<bool> ValidateByRetailer(int retailerId)
        {
            using (var client = RedisClientsManager.GetClient())
            {
                if (!client.ContainsKey(KvConstant.RetailersByChannelsKey))
                {
                    using (var db = await DbConnectionFactory.OpenAsync())
                    {
                        using (_ = db.OpenTransaction(System.Data.IsolationLevel.ReadUncommitted))
                        {
                            var ids = await db.SelectAsync<long>(db.From<Domain.Model.OmniChannel>()
                                .Where(x => x.IsActive && (x.IsDeleted == null || x.IsDeleted == false))
                                .Select(x => x.RetailerId));
                            if (ids == null || !ids.Any())
                            {
                                return false;
                            }

                            ids = ids.GroupBy(x => x).Select(p => p.FirstOrDefault()).ToList();
                            client.AddRangeToSet(KvConstant.RetailersByChannelsKey, ids.Select(p => p.ToString()).ToList());
                            var expire = DateTime.Now.AddDays(Settings.Get("CacheRegisterRetailer", 10));
                            client.ExpireEntryAt(KvConstant.RetailersByChannelsKey, expire);
                            return ids.Contains(retailerId);
                        }
                    }
                }
                return client.SetContainsItem(KvConstant.RetailersByChannelsKey, retailerId.ToString());
            }
        }
    }
}

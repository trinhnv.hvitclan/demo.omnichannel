﻿using Demo.OmniChannel.ShareKernel.Dto;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ShareKernel.EventBus;

namespace Demo.OmniChannel.MappingProductGRPCServer.Services
{
    public class ProductMappingRequest
    {
        public int RetailerId { get; set; }
    }

    public class GetChannelByIdRequest : ProductMappingRequest
    {
        public long ChannelId { get; set; }
    }

    public class GetChannelByIdsRequest : ProductMappingRequest
    {
        public List<long> ChannelIds { get; set; } = new List<long>();
    }

    public class GetOmniScheduleRequest : ProductMappingRequest
    {
        public long ChannelId { get; set; }
        public int Type { get; set; }
    }

    public class GetKvProduct : ProductMappingRequest
    {
        public int BranchId { get; set; }
    }

    public class GetKvProductByIdsRequest : GetKvProduct
    {
        public List<long> ProductIds { get; set; } = new List<long>();
        public bool IsIncludeDelete { get; set; }
    }

    public class GetKvProductByCodesRequest : GetKvProduct
    {
        public List<string> Codes { get; set; } = new List<string>();
    }

    public class GetKvProductChildByUnitRequest : GetKvProduct
    {
        public List<long> ProductIds { get; set; } = new List<long>();
    }

    public class UpdateKvProductRelation : ProductMappingRequest
    {
        public List<ProductRelation>? KvProducts { get; set; }
        public bool IsRelate { get; set; }
        public ProductMappingCallback? Callback { get; set; }
        public string? TraceId { get; set; }
    }

    public class MappingResult : IntegrationEvent
    {
        public List<ProductMappingResult>? ProductMappings { get; set; }
        public string? TraceId { get; set; }
    }

    public class ProductMappingCallback
    {
        public string? Type { get; set; }
        public string? RoutingKey { get; set; }
    }

    public static class CallBackType
{
        public const string RabbitMQ = "rabbitmq";
    }

public class ProductMappingResult
    {
        public bool Success { get; set; } = true;
        public string? Id { get; set; }
        public string Error { get; set; } = "";
    }

    public class SyncOnhandAndSyncPrice : ProductMappingRequest
    {
        public List<ProductMapping> ProductMappings { get; set; } = new List<ProductMapping>();
        public List<ProductMapping> DeleteProductMappings { get; set; } = new List<ProductMapping>();
        public List<ProductRelation> KvProducts { get; set; } = new List<ProductRelation>();
        public bool IsRelate { get; set; }
        public string?  LogId { get; set; }
    }

    public class CreateMappingAuditTrail: ProductMappingRequest
    {
        public List<ProductMapping> ProductMappings { get; set; } = new List<ProductMapping>();
        public List<ProductRelation> KvProducts { get; set; } = new List<ProductRelation>();
        public bool IsRelate { get; set; }
        public string? LogId { get; set; }
        public int BranchId { get; set; }

    }

    public class DeleteMappingAuditTrail : ProductMappingRequest
    {
        public List<ProductMapping> RevertProductMapping { get; set; } = new List<ProductMapping>();
        public List<ProductRelation> KvProducts { get; set; } = new List<ProductRelation>();
        public bool IsRelate { get; set; }
        public string? LogId { get; set; }
        public int BranchId { get; set; }

    }

    public class ProductMapping
    {
        public string? Id { get; set; }
        public int ChannelId { get; set; }
        public string? ProductChannelName { get; set; }
        public string? ProductChannelSku { get; set; }
        public byte ProductChannelType { get; set; }
        public string? ProductKvFullName { get; set; }
        public long ProductKvId { get; set; }
        public string? ProductKvSku { get; set; }
        public string? RetailerCode { get; set; }
        public int RetailerId { get; set; }
        public string? CommonProductChannelId { get; set; }
        public string? CommonParentProductChannelId { get; set; }
        public long ParentProductChannelId { get; set; }
        public string? StrProductChannelId { get; set; }
        public string? StrParentProductChannelId { get; set; }
        public string? LatestBasePrice { get; set; }
        public string? LatestSalePrice { get; set; }
        public float LastesOnHand { get; set; }

    }

    public class BaseOmniChannelRequest
    {
        public int RetailerId { get; set; }
    }

    public class GetRetailerById: BaseOmniChannelRequest
    {
    }

    public class GetPosParameter : BaseOmniChannelRequest
    {
    }

    public class SyncMultiOnhand : BaseOmniChannelRequest
    {
        public Domain.Model.OmniChannel? Channel { get; set; }
        public Guid LogId { get; set; }
        public List<string> ChannelProductIds { get; set; } = new List<string>();
        public bool IsIgnoreAuditTrail { get; set; }
    }

    public class CreateChannelAuditTrail : BaseOmniChannelRequest
    {
        public ChannelCreateOrUpdateRequest Request { get; set; } = new ChannelCreateOrUpdateRequest();
        public bool IsNewChanel { get; set; }
        public Domain.Model.OmniChannel ExistChannel { get; set; } = new Domain.Model.OmniChannel();
        public Domain.Model.OmniChannel Exist { get; set; } = new Domain.Model.OmniChannel();
        public Domain.Model.OmniChannel Result { get; set; } = new Domain.Model.OmniChannel();
        public OmniChannelSettingObject ExistSettings { get; set; } = new OmniChannelSettingObject();
        public List<OmniChannelSchedule> ExistSchedules { get; set; } = new List<OmniChannelSchedule>();
    }

    public class EnableChannelAuditTrail : BaseOmniChannelRequest
    {
        public string? LogContent { get; set; }
        public int BranchId { get; set; }
    }

    public class GetBranchById : BaseOmniChannelRequest
    {
        public int BranchId { get; set; }
    }

    public class GetPosSetting : BaseOmniChannelRequest
    {
    }

    public class SyncProduct: BaseOmniChannelRequest
    {
        public long ChannelId { get; set; }
        public byte ChannelType { get; set; }
        public int BranchId { get; set; }
        public string? RetailerCode { get; set; }
        public bool IsIgnoreAuditTrail { get; set; }
        public Guid LogId { get; set; }
        public bool IsUpdateChannel { get; set; }
    }

    public class CreateOmniChannelCoreContext : BaseOmniChannelRequest
    {
        public int BranchId { get; set; }
        public byte ChannelType { get; set; }
        public long ChannelId { get; set; }
        public string? ChannelName { get; set; }
    }

    public class ChannelCreateOrUpdateRequest
    {
        public AuditTrailChannelInfo Channel { get; set; } = new AuditTrailChannelInfo();
        public bool IsSyncOrder { get; set; }
        public bool IsSyncOnHand { get; set; }
        public bool IsSyncPrice { get; set; }
        public long AuthId { get; set; }
        public bool IsIgnoreAuditTrail { get; set; }
    }

    public class AuditTrailChannelInfo
    {
        public string? Name { get; set; }
        public string? Email { get; set; }
        public byte Type { get; set; }
        public int BranchId { get; set; }
        public bool? IsAutoDeleteMapping { get; set; }
        public bool IsAutoMappingProduct { get; set; }
        public byte SyncOnHandFormula { get; set; }
        public int? SyncOrderFormula { get; set; }
        public long? PriceBookId { get; set; }
        public long? BasePriceBookId { get; set; }
        public OmniChannelSettingDto OmniChannelSettings { get; set; } = new OmniChannelSettingDto();
    }
}

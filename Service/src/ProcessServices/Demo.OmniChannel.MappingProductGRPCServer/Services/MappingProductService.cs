﻿using Grpc.Core;
using Demo.Audit.Model.Message;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannelCore.Api.Sdk.Common;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Serilog;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System.Text.Json;

namespace Demo.OmniChannel.MappingProductGRPCServer.Services
{
    public class MappingProductService : OmniProduct.OmniProductBase
    {
        private IDbConnectionFactory DbConnectionFactory { get; }
        private ICacheClient CacheClient { get; }
        public IOnHandBusiness OnHandBusiness { get; set; }
        public IPriceBusiness PriceBusiness { get; set; }
        public IAuditTrailInternalClient AuditTrailInternalClient { get; set; }
        public IAppSettings AppSettings { get; set; }
        public IIntegrationEventService IntegrationEventService { get; set; }


        public MappingProductService(IDbConnectionFactory connectionFactory, ICacheClient cacheClient,
            IOnHandBusiness onHandBusiness, IPriceBusiness priceBusiness,
            IAuditTrailInternalClient auditTrailInternalClient, IAppSettings appSettings, IIntegrationEventService integrationEventService)
        {
            DbConnectionFactory = connectionFactory;
            CacheClient = cacheClient;
            OnHandBusiness = onHandBusiness;
            PriceBusiness = priceBusiness;
            AuditTrailInternalClient = auditTrailInternalClient;
            AppSettings = appSettings;
            IntegrationEventService = integrationEventService;
        }

        public override Task<PingMappingResponse> PingMapping(PingMappingRequest request, ServerCallContext context)
        {
            return Task.FromResult(new PingMappingResponse() { Message = "Pong" });
        }

        public override async Task<OmniProductResponse> GetChannelById(OmniProductRequest request, ServerCallContext context)
        {
            try
            {
                var newReq = JsonSerializer.Deserialize<GetChannelByIdRequest>(request.RequestObject);
                if (newReq == null)
                {
                    throw new OmniException($"Cannot Deserialize from {request.RequestObject}");
                }
                using var db = await DbConnectionFactory.OpenAsync();
                using (db.OpenTransaction(System.Data.IsolationLevel.ReadUncommitted))
                {
                    var omniRepo = new OmniChannelRepository(db);
                    var result = await omniRepo.GetByIdAsync(newReq.ChannelId, true);
                    return GetResponseSuccess(result.ToSafeJson());
                }
            }
            catch (Exception exception)
            {
                Log.Error($"GetChannelById: Request: {JsonSerializer.Deserialize<GetChannelByIdRequest>(request.RequestObject).ToSafeJson()} {exception.Message} - {exception.StackTrace}");
                throw new RpcException(new Status(StatusCode.Aborted, exception.ToString()));
            }
        }

        public override async Task<OmniProductResponse> GetChannelByIds(OmniProductRequest request, ServerCallContext context)
        {
            try
            {
                var newReq = JsonSerializer.Deserialize<GetChannelByIdsRequest>(request.RequestObject);
                if (newReq == null)
                {
                    throw new OmniException($"Cannot Deserialize from {request.RequestObject}");
                }
                using var db = await DbConnectionFactory.OpenAsync();
                using (db.OpenTransaction(System.Data.IsolationLevel.ReadUncommitted))
                {
                    var omniRepo = new OmniChannelRepository(db);
                    var result = await omniRepo.GetByIdsAsync(newReq.ChannelIds);
                    return GetResponseSuccess(result.ToSafeJson());
                }
            }
            catch (Exception exception)
            {
                Log.Error($"GetChannelByIds: Request: {JsonSerializer.Deserialize<GetChannelByIdsRequest>(request.RequestObject).ToSafeJson()} {exception.Message} - {exception.StackTrace}");
                throw new RpcException(new Status(StatusCode.Aborted, exception.ToString()));
            }
        }

        public override async Task<OmniProductResponse> GetOmniChannelSchedule(OmniProductRequest request, ServerCallContext context)
        {
            try
            {
                var newReq = JsonSerializer.Deserialize<GetOmniScheduleRequest>(request.RequestObject);
                if (newReq == null)
                {
                    throw new OmniException($"Cannot Deserialize from {request.RequestObject}");
                }

                using var db = await DbConnectionFactory.OpenAsync();
                using (db.OpenTransaction(System.Data.IsolationLevel.ReadUncommitted))
                {
                    var scheduleRepo = new OmniChannelScheduleRepository(db);
                    var result = await scheduleRepo.GetSchedule(newReq.ChannelId, newReq.Type);
                    return GetResponseSuccess(result.ToSafeJson());
                }
            }
            catch (Exception exception)
            {
                Log.Error($"GetOmniChannelSchedule: Request: {JsonSerializer.Deserialize<GetOmniScheduleRequest>(request.RequestObject).ToSafeJson()} {exception.Message} - {exception.StackTrace}");
                throw new RpcException(new Status(StatusCode.Aborted, exception.ToString()));
            }
        }

        public override async Task<OmniProductResponse> GetKvProductByIds(OmniProductRequest request, ServerCallContext context)
        {
            try
            {
                var newReq = JsonSerializer.Deserialize<GetKvProductByIdsRequest>(request.RequestObject);
                if (newReq == null)
                {
                    throw new OmniException($"Cannot Deserialize from {request.RequestObject}");
                }

                var nameConnection = await GetNameConnection(newReq.RetailerId);

                using var db = await DbConnectionFactory.OpenAsync(nameConnection);
                var productRepo = new ProductRepository(db);
                var isLogMonitor = AppSettings.Get("IsLogMonitorSql", false);
                var result = await productRepo.GetProductByIdsIncludeDeleted(newReq.RetailerId, newReq.BranchId, newReq.ProductIds, isIncludeDeleted: newReq.IsIncludeDelete, isLogMonitor);
                return GetResponseSuccess(result.ToSafeJson());
            }
            catch (Exception exception)
            {
                Log.Error($"GetKvProductByIds: Request: {JsonSerializer.Deserialize<GetKvProductByIdsRequest>(request.RequestObject).ToSafeJson()} {exception.Message} - {exception.StackTrace}");
                throw new RpcException(new Status(StatusCode.Aborted, exception.ToString()));
            }
        }

        public override async Task<OmniProductResponse> GetKvProductByCodes(OmniProductRequest request, ServerCallContext context)
        {
            try
            {
                var newReq = JsonSerializer.Deserialize<GetKvProductByCodesRequest>(request.RequestObject);
                if (newReq == null)
                {
                    throw new OmniException($"Cannot Deserialize from {request.RequestObject}");
                }
                var nameConnection = await GetNameConnection(newReq.RetailerId);

                using var db = await DbConnectionFactory.OpenAsync(nameConnection);
                var productRepo = new ProductRepository(db);
                var codes = Enumerable.ToHashSet(newReq.Codes.Distinct());
                var result = await productRepo.GetProductByCode(newReq.RetailerId, newReq.BranchId, codes);
                return GetResponseSuccess(result.ToSafeJson());
            }
            catch (Exception exception)
            {
                Log.Error($"GetKvProductByCodes: Request: {JsonSerializer.Deserialize<GetKvProductByCodesRequest>(request.RequestObject).ToSafeJson()} {exception.Message} - {exception.StackTrace}");
                throw new RpcException(new Status(StatusCode.Aborted, exception.ToString()));
            }
        }

        public override async Task<OmniProductResponse> GetKvProductChildByUnit(OmniProductRequest request, ServerCallContext context)
        {
            try
            {
                var newReq = JsonSerializer.Deserialize<GetKvProductChildByUnitRequest>(request.RequestObject);
                if (newReq == null)
                {
                    throw new OmniException($"Cannot Deserialize from {request.RequestObject}");
                }

                var nameConnection = await GetNameConnection(newReq.RetailerId);

                using var db = await DbConnectionFactory.OpenAsync(nameConnection);
                var productRepo = new ProductRepository(db);
                var isLogMonitor = AppSettings.Get("IsLogMonitorSql", false);
                var result = await productRepo.GetProductChildUnitByIds(newReq.RetailerId, newReq.BranchId, newReq.ProductIds, isLogMonitor);
                return GetResponseSuccess(result.ToSafeJson());
            }
            catch (Exception exception)
            {
                Log.Error($"GetKvProductChildByUnit: Request: {JsonSerializer.Deserialize<GetKvProductChildByUnitRequest>(request.RequestObject).ToSafeJson()} {exception.Message} - {exception.StackTrace}");
                throw new RpcException(new Status(StatusCode.Aborted, exception.ToString()));
            }
        }

        public override async Task<OmniProductResponse> UpdateKvProductRelation(OmniProductRequest request, ServerCallContext context)
        {
            try
            {
                var newReq = JsonSerializer.Deserialize<UpdateKvProductRelation>(request.RequestObject);
                if (newReq == null)
                {
                    throw new OmniException($"Cannot Deserialize from {request.RequestObject}");
                }
                Log.Information(request.ToSafeJson());

                var nameConnection = await GetNameConnection(newReq.RetailerId);
                using var db = await DbConnectionFactory.OpenAsync(nameConnection);
                var productRepo = new ProductRepository(db);
                var result = await productRepo.UpdateKvProductRelation(newReq.KvProducts, newReq.IsRelate);

                // Callback if request exist
                if (!string.IsNullOrEmpty(newReq.TraceId) &&
                    newReq.Callback != null &&
                    CallBackType.RabbitMQ.Equals(newReq.Callback.Type) &&
                    !string.IsNullOrEmpty(newReq.Callback.RoutingKey))
                {
                    Log.Information($"Publish message to rabbitmq: {newReq.TraceId}");
                    PublishResultToRabbit(request, newReq.Callback.RoutingKey);
                }
                return GetResponseSuccess(result.ToSafeJson());
            }
            catch (Exception exception)
            {
                Log.Error($"UpdateKvProductRelation: Request: {JsonSerializer.Deserialize<UpdateKvProductRelation>(request.RequestObject).ToSafeJson()} {exception.Message} - {exception.StackTrace}");
                throw new RpcException(new Status(StatusCode.Aborted, exception.ToString()));
            }
        }

        public override async Task<OmniProductResponse> UpdateKvProductRelationRevert(OmniProductRequest request, ServerCallContext context)
        {
            try
            {
                var newReq = JsonSerializer.Deserialize<UpdateKvProductRelation>(request.RequestObject);
                if (newReq == null)
                {
                    throw new OmniException($"Cannot Deserialize from {request.RequestObject}");
                }
                if (newReq.KvProducts == null || newReq.KvProducts.Count == 0)
                {
                    return GetResponseSuccess("Success");
                }
                Log.Information(request.ToSafeJson());
                var nameConnection = await GetNameConnection(newReq.RetailerId);
                using var db = await DbConnectionFactory.OpenAsync(nameConnection);
                var productRepo = new ProductRepository(db);
                var result = await productRepo.UpdateKvProductRelation(newReq.KvProducts, newReq.IsRelate, true);
                return GetResponseSuccess(result.ToSafeJson());
            }
            catch (Exception exception)
            {
                Log.Error($"UpdateKvProductRelationRevert: Request: {JsonSerializer.Deserialize<UpdateKvProductRelation>(request.RequestObject).ToSafeJson()} {exception.Message} - {exception.StackTrace}");
                throw new RpcException(new Status(StatusCode.Aborted, exception.ToString()));
            }
        }

        public override async Task<OmniProductResponse> SyncOnhandAndSyncPrice(OmniProductRequest request, ServerCallContext context)
        {
            Log.Information("Begin SyncOnhandAndSyncPrice ");
            try
            {
                var newReq = JsonSerializer.Deserialize<SyncOnhandAndSyncPrice>(request.RequestObject);
                if (newReq == null)
                {
                    throw new OmniException($"Cannot Deserialize from {request.RequestObject}");
                }

                var kvConnectionName = await GetNameConnection(newReq.RetailerId);
                
                using var db = await DbConnectionFactory.OpenAsync();
                using (db.OpenTransaction(System.Data.IsolationLevel.ReadUncommitted))
                {
                    var listId = new List<int>();
                    if (newReq.DeleteProductMappings is {Count: > 0})
                    {
                        listId = newReq.DeleteProductMappings.Select(p => p.ChannelId).Distinct().ToList();
                    }
                    else
                    {
                        if (newReq.ProductMappings is {Count: > 0})
                        {
                            listId = newReq.ProductMappings.Select(p => p.ChannelId).Distinct().ToList();
                        }
                    }

                    foreach (var channelId in listId)
                    {
                        var omniRepo = new OmniChannelRepository(db);
                        var channel = await omniRepo.GetByIdAsync(channelId, true);
                        if (channel.OmniChannelSchedules?.Any(x => x.Type == (byte)ScheduleType.SyncOnhand) == true)
                        {
                            Log.Information($"Start SyncMultiOnHand, request: {request.ToSafeJson()}");
                            await OnHandBusiness.SyncMultiOnHand(kvConnectionName,
                                channel,
                                newReq.DeleteProductMappings.Any() ? null : newReq.ProductMappings.Select(p => p.CommonProductChannelId).ToList(),
                                newReq.DeleteProductMappings.Any() ? newReq.DeleteProductMappings.Select(p => p.ProductKvId).ToList() : newReq.ProductMappings.Select(p => p.ProductKvId).ToList(),
                                false,
                                Guid.NewGuid(),
                                null);

                        }
                        if (channel.OmniChannelSchedules?.Any(x => x.Type == (byte)ScheduleType.SyncPrice) == true)
                        {
                            await PriceBusiness.SyncMultiPrice(kvConnectionName,
                                channel,
                                newReq.DeleteProductMappings.Any() ? newReq.DeleteProductMappings.Select(p => p.CommonProductChannelId).ToList() : newReq.ProductMappings.Select(p => p.CommonProductChannelId).ToList(),
                                null,
                                false,
                                Guid.NewGuid(),
                                true, 50, null, true, "Queue created from SyncOnhandAndSyncPrice");

                        }

                    }
                }
                Log.Information($"Success SyncOnhandAndSyncPrice LogId: {newReq.LogId}");
            }
            catch (Exception ex)
            {
                var req = JsonSerializer.Deserialize<SyncOnhandAndSyncPrice>(request.RequestObject);
                Log.Error($"SyncOnhandAndSyncPrice: Request: {req.ToSafeJson()} - Error Message:{ex.Message} - StackTrace: {ex.StackTrace}");
            }
            return GetResponseSuccess("Success SyncOnhandAndSyncPrice");
        }

        public override async Task<OmniProductResponse> CreateMappingAuditTrail(OmniProductRequest request, ServerCallContext context)
        {
            try
            {
                var newReq = JsonSerializer.Deserialize<CreateMappingAuditTrail>(request.RequestObject);
                if (newReq == null)
                {
                    throw new OmniException($"Cannot Deserialize from {request.RequestObject}");
                }
                Log.Information(request.ToSafeJson());
                if (newReq.ProductMappings.Count <= 0)
                {
                    return GetResponseSuccess("true");
                }
                using var db = await DbConnectionFactory.OpenAsync();
                Domain.Model.OmniChannel channel;
                using (db.OpenTransaction(System.Data.IsolationLevel.ReadUncommitted))
                {
                    var omniRepo = new OmniChannelRepository(db);
                    channel = await omniRepo.GetByIdAsync(newReq.ProductMappings[0].ChannelId);
                }
                var auditTrailItems = newReq.ProductMappings.Select(newMapping => new ProductMappingItemAuditTrail
                {
                    ProductKvSku = newMapping.ProductKvSku,
                    ParentProductChannelId = newMapping.ParentProductChannelId.ToString(),
                    ProductChannelId = newMapping.StrProductChannelId,
                    ProductChannelSku = newMapping.ProductChannelSku
                }).ToList();
                var logContent = AuditTrailHelper.GetMappingProductContent(
                   new ProductMappingAuditTrail
                   {
                       ChannelType = channel?.Type,
                       ChannelName = channel?.Name,
                       Items = auditTrailItems
                   }, false);
                var log = new AuditTrailLog
                {
                    FunctionId = (int)FunctionType.MappingSalesChannel,
                    Action = (int)AuditTrailAction.Create,
                    CreatedDate = DateTime.Now,
                    BranchId = newReq.BranchId,
                    Content = logContent.ToString()
                };
                var kvConnectionName = await GetNameConnection(newReq.RetailerId);
                var executionContext = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, newReq.RetailerId, kvConnectionName, newReq.BranchId, 7);
                var coreContext = executionContext.ConvertTo<KvInternalContext>();
                coreContext.UserId = executionContext.User?.Id ?? 0;
                await AuditTrailInternalClient.AddLogAsync(coreContext, log);
                return GetResponseSuccess("true");
            }
            catch (Exception exception)
            {
                Log.Error($"CreateMappingAuditTrail: Request: {request.ToSafeJson()} - Error Message:{exception.Message} - StackTrace: {exception.StackTrace}");
                return GetResponseSuccess("true");
            }
        }

        public override async Task<OmniProductResponse> DeleteMappingAuditTrail(OmniProductRequest request,
            ServerCallContext context)
        {
            try
            {
                var newReq = JsonSerializer.Deserialize<DeleteMappingAuditTrail>(request.RequestObject);
                if (newReq == null)
                {
                    throw new OmniException($"Cannot Deserialize from {request.RequestObject}");
                }

                Log.Information(request.ToSafeJson());

                var kvConnectionName = await GetNameConnection(newReq.RetailerId);

                using var db = await DbConnectionFactory.OpenAsync();
                Domain.Model.OmniChannel? channel = null;
                if (newReq.RevertProductMapping is {Count: > 0})
                {
                    using (db.OpenTransaction(System.Data.IsolationLevel.ReadUncommitted))
                    {
                        var omniRepo = new OmniChannelRepository(db);
                        channel = await omniRepo.GetByIdAsync(newReq.RevertProductMapping[0].ChannelId);
                    }
                }
                var auditTrailItems = new List<ProductMappingItemAuditTrail>();
                if (newReq.RevertProductMapping is {Count: > 0})
                {
                    auditTrailItems = newReq.RevertProductMapping.Select(newMapping => new ProductMappingItemAuditTrail
                    {
                        ProductKvSku = newMapping.ProductKvSku,
                        ParentProductChannelId = newMapping.ParentProductChannelId.ToString(),
                        ProductChannelId = newMapping.StrProductChannelId,
                        ProductChannelSku = newMapping.ProductChannelSku
                    }).ToList();
                }

                var logContent = AuditTrailHelper.GetMappingProductContent(
                    new ProductMappingAuditTrail
                    {
                        ChannelType = channel?.Type,
                        ChannelName = channel?.Name,
                        Items = auditTrailItems
                    }, true);

                var log = new AuditTrailLog
                {
                    FunctionId = (int) FunctionType.MappingSalesChannel,
                    Action = (int) AuditTrailAction.Reject,
                    CreatedDate = DateTime.Now,
                    BranchId = channel?.BranchId ?? 0,
                    Content = logContent.ToString()
                };
                var executionContext = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory,
                    newReq.RetailerId, kvConnectionName, newReq.BranchId, 7);
                var coreContext = executionContext.ConvertTo<KvInternalContext>();
                coreContext.UserId = executionContext.User?.Id ?? 0;
                await AuditTrailInternalClient.AddLogAsync(coreContext, log);
                return GetResponseSuccess("true");
            }
            catch (Exception exception)
            {
                Log.Error(
                    $"DeleteMappingAuditTrail: Request: {request.ToSafeJson()} - Error Message:{exception.Message} - StackTrace: {exception.StackTrace}");
                return GetResponseSuccess("true");
            }
        }

        private static OmniProductResponse GetResponseSuccess(string result)
        {
            return new OmniProductResponse
            {
                Error = "0",
                Result = result
            };
        }

        private async Task<string> GetNameConnection(int retailerId)
        {
            var key = $"kol.mappingProduct.ConnectionString.{retailerId}";
            var result = CacheClient.Get<string>($"kol.mappingProduct.ConnectionString.{retailerId}");
            if (result == null)
            {
                using var dbMaster = await DbConnectionFactory.OpenAsync(nameof(KvSqlConnectString.KvMasterEntities));
                var kvRetailer = await dbMaster.SingleAsync<KvRetailer>(x => x.Id == retailerId);
                var kvGroup = await dbMaster.SingleAsync<KvGroup>(x => x.Id == kvRetailer.GroupId);
                result = kvGroup.ConnectionString.ToLower().Replace("name=", "");
                CacheClient.Set(key, result);
            }
            return result;
        }

        private void PublishResultToRabbit(OmniProductRequest request, string routingKey)
        {
            try
            {
                var mappingResult = JsonSerializer.Deserialize<MappingResult>(request.RequestObject);
                if (mappingResult != null)
                {
                    IntegrationEventService.AddEventWithRoutingKeyAsync(mappingResult, routingKey);
                    Log.Information($"Publish message to rabbitmq successfully: {mappingResult.ToSafeJson()} to routing key: {routingKey}");
                }
            }
            catch (Exception e)
            {
                Log.Error($"UpdateKvProductRelation publish rabbit message request: {JsonSerializer.Deserialize<UpdateKvProductRelation>(request.RequestObject).ToSafeJson()} {e.Message} - {e.StackTrace}");
            }
        }
    }

}

using Demo.OmniChannel.Business;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.Api.Extensions;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.DCControl.ConfigureService;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Infrastructure.IoC;
using Demo.OmniChannel.MappingProductGRPCServer.Services;
using Demo.OmniChannel.MongoService.Common;
using Demo.OmniChannel.MongoService.Impls;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannelCore.Api.Sdk.Impls;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Serilog;
using Serilog.Exceptions;
using Serilog.Formatting.Json;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.Logging.Serilog;
using ServiceStack.Messaging;
using ServiceStack.Messaging.Redis;
using ServiceStack.OrmLite;
using AutoMapper;
using Demo.OmniChannel.Services.AutoMapperConfigurations;
using Demo.OmniChannel.CoreDemoContext.Interfaces;
using Demo.OmniChannel.CoreDemoContext.Implements;
using Demo.OmniChannel.Infrastructure.EventBus.Extentions;
using Demo.OmniChannel.CoreDemoContext.ConfigExtentions;
using Demo.OmniChannel.ShareKernel.Common;

var NameSpace = "Demo.OmniChannel.MappingProductGRPCServer";
var AppName = NameSpace.Substring(NameSpace.LastIndexOf('.', NameSpace.LastIndexOf('.') - 1) + 1);

var configuration = new ConfigurationBuilder()
        .SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
        .AddEnvironmentVariables()
        .Build();

#region Serilog config
SubLoggerConfiguration subLoggerConfiguration = new SubLoggerConfiguration();
configuration.GetSection("Serilog:SubLogger").Bind(subLoggerConfiguration);

Log.Logger = new LoggerConfiguration() //NOSONAR
    .Enrich.FromLogContext()
    .Enrich.WithProperty("ApplicationContext", AppName)
    .Enrich.WithExceptionDetails()
    .ReadFrom.Configuration(configuration)
    .WriteTo.Console()
    .WriteTo.Logger(l =>
        l.Filter.ByIncludingOnly(e => e.Level == subLoggerConfiguration.Error.Level)
        .WriteTo.File(new JsonFormatter(),
                        subLoggerConfiguration.Error.PathFormat,
                        rollingInterval: RollingInterval.Day,
                        rollOnFileSizeLimit: true,
                        fileSizeLimitBytes: 100000000))
    .WriteTo.Logger(l =>
        l.Filter.ByIncludingOnly(e => e.Level == subLoggerConfiguration.Information.Level)
        .WriteTo.File(new JsonFormatter(),
                        subLoggerConfiguration.Information.PathFormat,
                        rollingInterval: RollingInterval.Day,
                        rollOnFileSizeLimit: true,
                        fileSizeLimitBytes: 100000000))
    .WriteTo.Logger(l =>
        l.Filter.ByIncludingOnly(e => e.Level == subLoggerConfiguration.Warning.Level)
        .WriteTo.File(new JsonFormatter(),
                        subLoggerConfiguration.Warning.PathFormat,
                        rollingInterval: RollingInterval.Day,
                        rollOnFileSizeLimit: true,
                        fileSizeLimitBytes: 100000000))
    .CreateLogger();
LogManager.LogFactory = new SerilogFactory();
#endregion 


var builder = WebApplication.CreateBuilder(args);

// Additional configuration is required to successfully run gRPC on macOS.
// For instructions on how to configure Kestrel and gRPC clients on macOS, visit https://go.microsoft.com/fwlink/?linkid=2099682

// Add services to the container.
builder.Services.AddGrpc(options =>
{
    int.TryParse(configuration.GetSection("MaxReceiveMessageSize")?.Value, out var maxReceiveMessageSize);
    if (maxReceiveMessageSize <= 0)
    {
        maxReceiveMessageSize = 10; // Default 10 MB
    }

    options.EnableDetailedErrors = true;
    options.MaxReceiveMessageSize = maxReceiveMessageSize * 1024 * 1024;
});

builder.WebHost.UseSerilog()
    .UseKestrel(webBuilder =>
    {
        webBuilder.ListenAnyIP(50001, o => o.Protocols = HttpProtocols.Http2);
    })
    .ConfigureAppConfiguration(x => x.AddConfiguration(configuration))
    .ConfigureServices(services => 
    {
        #region Register ServiceStack License
        Licensing.RegisterLicense(configuration.GetSection("servicestack:license").Value);
        #endregion
        var redisConfig = new KvRedisConfig();
        configuration.GetSection("redis:cache").Bind(redisConfig);
        services.AddSingleton(sc => KvRedisPoolManager.GetClientsManager(redisConfig));
        services.AddSingleton<ICacheClient, KvCacheClient>();
        services.AddSingleton<IAppSettings>(x => new NetCoreAppSettings(configuration));

        var redisMqConfig = new KvRedisConfig();
        configuration.GetSection("redis:message").Bind(redisMqConfig);
        var redisFactory = KvRedisPoolManager.GetClientsManager(redisMqConfig);
        var mqHost = new RedisMqServer(redisFactory, retryCount: 2);
        services.AddSingleton<IMessageService>(mqHost);
        services.AddSingleton<IMessageFactory>(c => new RedisMessageFactory(redisFactory));
        services.AddSingleton(redisMqConfig);

        #region Register Mongo
        var mongoSetting = new MongoDbSettings();
        configuration.GetSection("mongodb").Bind(mongoSetting);
        if (!string.IsNullOrEmpty(mongoSetting.ConnectionString))
        {
            services.Configure<MongoDbSettings>(configuration.GetSection("mongodb"));
            services.UsingMongoDb(configuration);
            services.AddTransient<IRetryOrderMongoService, RetryOrderMongoService>();
            services.AddTransient<IRetryInvoiceMongoService, RetryInvoiceMongoService>();
            services.AddTransient<IProductMongoService, ProductMongoService>();
            services.AddTransient<IProductMappingService, ProductMappingService>();
            services.AddTransient<ISendoLocationMongoService, SendoLocationMongoService>();
        }
        #endregion

        #region Register RabbitMQ
        services.AddConfigEventBus(configuration);
        services.AddEventBus();
        #endregion

        #region Register DatabaseConnectionString
        var kvSql = new KvSqlConnectString();
        configuration.GetSection("SqlConnectStrings").Bind(kvSql);
        var connectFactory = new OrmLiteConnectionFactory(kvSql.KvChannelEntities, SqlServer2016Dialect.Provider);
        connectFactory.RegisterConnection(nameof(kvSql.KvMasterEntities), kvSql.KvMasterEntities, SqlServer2016Dialect.Provider);
        foreach (var item in kvSql.KvEntitiesDC1)
        {
            connectFactory.RegisterConnection(item.Name.ToLower(), item.Value, SqlServer2016Dialect.Provider);
        }

        OrmLiteConfig.DialectProvider.GetStringConverter().UseUnicode = true;
        services.AddSingleton(kvSql);
        services.AddSingleton<IDbConnectionFactory>(c => connectFactory);
        #endregion

        services.AddTransient<IChannelBusiness, ChannelBusiness>();

        // Register Business
        services.AddScoped<IPriceBusiness, PriceBusiness>();
        services.AddScoped<IOnHandBusiness, OnHandBusiness>();
        services.AddScoped<IMappingBusiness, MappingBusiness>();

        // Register Auto Mapper
        services.AddSingleton(AutoMapping.RegisterMappings().CreateMapper());

        // Register Core Api 
        services.AddScoped<ISaleChannelInternalClient>(c => new SaleChannelInternalClient(c.GetService<IAppSettings>()));
        services.AddScoped<IDemoOmniChannelCoreContext>(c => new DemoOmniChannelCoreContext(c.GetService<ICacheClient>(), c.GetService<IDbConnectionFactory>(), c.GetService<IAppSettings>()));

        //Audittrail
        services.AddScoped<IAuditTrailInternalClient>(c => new AuditTrailInternalClient(c.GetService<IAppSettings>()));

        services.AddDICoreDemoContextConfiguration();

        services.RegisterApplicationDcControl(new ApplicationDcInputOptions
        {
            ServiceName = "MappingProductGRPCService",
            AppSettings = new NetCoreAppSettings(configuration)
        });
    })
;

var app = builder.Build();

// Configure the HTTP request pipeline.
app.MapGrpcService<MappingProductService>();
app.MapGrpcService<SyncEsSubcribeService>();
app.MapGrpcService<OmniChannelService>();

app.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");

try
{
    Log.Information($"Starting {AppName}");
    await app.RunAsync();
}
catch (Exception ex)
{
    Log.Error(ex, "Host terminated unexpectedly");
    throw;
}
finally
{
    Log.CloseAndFlush();
}

﻿using Demo.OmniChannel.Business;
using Demo.OmniChannel.Business.Implements;
using Demo.OmniChannel.Business.Interfaces;

namespace Demo.OmniChannel.OrderGRPCServer.Extensions
{
    public static class AddBusinessExtension
    {
        public static void AddBusiness(this IServiceCollection services)
        {
            services.AddTransient<IShopeeBusinessInterface, ShopeeBusinessServices>();
            services.AddTransient<ITiktokBusinessInterface, TiktokBusinessServices>();
            services.AddTransient<IAutoCreateProductBusiness, AutoCreateProductBusinessService>();
            services.AddTransient<IChannelBusiness, Business.ChannelBusiness>();
            services.AddTransient<IShopeeBusinessInvoiceInterface, ShopeeBusinessInvoiceService>();
            services.AddTransient<ITiktokBusinessInvoiceInterface, TiktokBusinessInvoiceService>();
        }
    }
}

﻿using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.Services.Impls;
using Demo.OmniChannel.Services.Interfaces;
using ServiceStack.Configuration;
using ServiceStack.Redis;

namespace Demo.OmniChannel.OrderGRPCServer.Extensions
{
    public static class AddServiceExtension
    {
        public static void AddService(this IServiceCollection services)
        {
            services.AddTransient<IOmniChannelSettingService, OmniChannelSettingService>();
            services.AddTransient<IOmniChannelService, OmniChannelService>();
            services.AddTransient<IOmniChannelPlatformService, OmniChannelPlatformService>();
            services.AddTransient<IOmniChannelAuthService, OmniChannelAuthService>();
            services.AddTransient<IChannelClient>(c => new Demo.OmniChannel.ChannelClient.Impls.ChannelClient(c.GetService<IAppSettings>()));
            services.AddScoped<IScheduleService>(c => new ScheduleService.Impls.ScheduleService(c.GetRequiredService<IRedisClientsManager>(), c.GetService<IAppSettings>()));
        }
    }
}

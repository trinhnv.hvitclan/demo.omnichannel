﻿using Demo.OmniChannel.Sdk.Impls;
using Demo.OmniChannel.Sdk.Interfaces;
using Demo.OmniChannelCore.Api.Sdk.Impls;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;

namespace Demo.OmniChannel.OrderGRPCServer.Extensions
{
    public static class AddInternalClientExtension
    {
        public static void AddInternalClient(this IServiceCollection services)
        {
            services.AddTransient<OmniChannelCore.Api.Sdk.Interfaces.IOrderInternalClient, 
                OmniChannelCore.Api.Sdk.Impls.OrderInternalClient>();

            services.AddTransient<OmniChannelCore.Api.Sdk.Interfaces.IInvoiceInternalClient,
                OmniChannelCore.Api.Sdk.Impls.InvoiceInternalClient>();
            services.AddTransient<IIntegrationInternalClient, IntegrationInternalClient>();
            services.AddTransient<IAuditTrailInternalClient, AuditTrailInternalClient>();
            services.AddTransient<IPartnerDeliveryInternalClient, PartnerDeliveryInternalClient>();
            services.AddTransient<ISurChargeInternalClient, SurChargeInternalClient>();
            services.AddTransient<IDeliveryInfoInternalClient, DeliveryInfoInternalClient>();
            services.AddTransient<ISurChargeBranchInternalClient, SurChargeBranchInternalClient>();
            services.AddTransient<ICustomerInternalClient, CustomerInternalClient>();
            services.AddTransient<IProductInternalClient, ProductInternalClient>();
            services.AddTransient<ICategoryInternalClient, CategoryInternalClient>();
            services.AddTransient<IAttributeInternalClient, AttributeInternalClient>();
            services.AddTransient<IProductAttributeInternalClient, ProductAttributeInternalClient>();
            services.AddTransient<IProductImageInternalClient, ProductImageInternalClient>();
            services.AddTransient<IProductFormulaInternalClient, ProductFormulaInternalClient>();
            services.AddTransient<Demo.OmniChannelCore.Api.Sdk.Interfaces.IPriceBookInternalClient,
                Demo.OmniChannelCore.Api.Sdk.Impls.PriceBookInternalClient>();
            services.AddTransient<IDeliveryPackageInternalClient, DeliveryPackageInternalClient>();
        }
    }
}

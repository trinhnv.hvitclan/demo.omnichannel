﻿using Grpc.Core;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ShareKernel.RedisStreamMessage;
using Newtonsoft.Json;

namespace Demo.OmniChannel.OrderGRPCServer.Services
{
    public class TiktokOrderService : TiktokOrder.TiktokOrderBase
    {
        private readonly ITiktokBusinessInterface _tiktokBusiness;
        private readonly ITiktokBusinessInvoiceInterface _tiktokBusinessInvoice;

        public TiktokOrderService(
            ITiktokBusinessInterface tiktokBusiness,
            ITiktokBusinessInvoiceInterface tiktokBusinessInvoice)
        {
            _tiktokBusiness = tiktokBusiness;
            _tiktokBusinessInvoice = tiktokBusinessInvoice;
        }

        public override async Task<CreateTiktokOrderResponse> CreateTiktokOrder(
            CreateTiktokOrderRequest request, ServerCallContext context)
        {
            var isCreateOrder = await _tiktokBusiness.CreateOrderServiceV2(request.Message);
            return new CreateTiktokOrderResponse
            {
                IsCreateOrderSuccess = isCreateOrder
            };
        }

        public override async Task<CreateTiktokInvoiceResponse> CreateTiktokInvoice(CreateTiktokInvoiceRequest request,
            ServerCallContext context)
        {
            var createInvoiceRequest = JsonConvert.DeserializeObject<CreateInvoiceMessage>(request.Message);
            var isCreateInvoice = await _tiktokBusinessInvoice.HandleCreateInvoice(createInvoiceRequest);
            return new CreateTiktokInvoiceResponse
            {
                IsCreateInvoiceSuccess = isCreateInvoice
            };
        }
    }
}

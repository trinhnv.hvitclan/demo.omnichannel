﻿using Grpc.Core;

namespace Demo.OmniChannel.OrderGRPCServer.Services
{
    public class HealthCheckService : HealthCheck.HealthCheckBase
    {
        public override Task<HealthCheckResult> Status(StatusRequest request, ServerCallContext context)
        {
            return Task.FromResult(new HealthCheckResult
            {
                Status = "pong"
            });
        }

    }
}

﻿using Grpc.Core;
using Demo.OmniChannel.Business;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ShareKernel.RedisStreamMessage;
using Newtonsoft.Json;

namespace Demo.OmniChannel.OrderGRPCServer.Services
{
    public class ShopeeOrderV2Service : ShopeeOrderV2.ShopeeOrderV2Base
    {

        private readonly IShopeeBusinessInterface _shopeeBusinessInterface;

        private readonly IShopeeBusinessInvoiceInterface _shopeeBusinessInvoiceInterface;

        public ShopeeOrderV2Service(
            IShopeeBusinessInterface shopeeBusinessInterface,
            IShopeeBusinessInvoiceInterface shopeeBusinessInvoiceInterface)
        {
            _shopeeBusinessInterface = shopeeBusinessInterface;
            _shopeeBusinessInvoiceInterface = shopeeBusinessInvoiceInterface;
        }

        public override async Task<CreateShopeeOrderV2Response> CreateShopeeOrderV2(CreateShopeeOrderV2Request request, ServerCallContext context)
        {
            var isCreateOrder = await _shopeeBusinessInterface.CreateOrderServiceV2(request.Message);
            return new CreateShopeeOrderV2Response
            {
                IsCreateOrderSuccess = isCreateOrder
            };
        }

        public override async Task<CreateShopeeInvoiceResponse> CreateShopeeInvoice(CreateShopeeInvoiceRequest request,
            ServerCallContext context)
        {
            var createInvoiceRequest = JsonConvert.DeserializeObject<CreateInvoiceMessage>(request.Message);
            var isCreateInvoice = await _shopeeBusinessInvoiceInterface.HandleCreateInvoice(createInvoiceRequest);
            return new CreateShopeeInvoiceResponse
            {
                IsCreateInvoiceSuccess = isCreateInvoice
            };
        }

        public override async Task<PingResponse> Ping(PingRequest request, ServerCallContext context)
        {
            await Task.Delay(100);
            return new PingResponse
            {
                ResponseMessage = "Pong"
            };
        }
    }
}

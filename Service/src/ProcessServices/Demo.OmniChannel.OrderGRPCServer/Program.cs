using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Extensions;
using Demo.OmniChannel.DCControl.ConfigureService;
using Demo.OmniChannel.Infrastructure.EventBus.Extentions;
using Demo.OmniChannel.Infrastructure.DemoGuaranteeClient.Extentions;
using Demo.OmniChannel.Infrastructure.IoC;
using Demo.OmniChannel.KafkaClient;
using Demo.OmniChannel.MongoService.Common;
using Demo.OmniChannel.OrderGRPCServer.Services;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.Utilities;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Serilog;
using Serilog.Exceptions;
using Serilog.Formatting.Json;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.Messaging.Redis;
using ServiceStack.OrmLite;
using ServiceStack.Logging;
using ServiceStack.Logging.Serilog;
using Demo.OmniChannel.DomainService.Extentions;
using Demo.OmniChannel.Infrastructure.Extensions;
using Demo.OmniChannel.ShareKernel.Common;

using Demo.OmniChannel.OrderGRPCServer.Extensions;

var NameSpace = "Demo.OmniChannel.OrderGRPCServer";
var AppName = NameSpace.Substring(NameSpace.LastIndexOf('.', NameSpace.LastIndexOf('.') - 1) + 1);

var configuration = new ConfigurationBuilder()
        .SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
        .AddEnvironmentVariables()
        .Build();

#region Serilog config
SubLoggerConfiguration subLoggerConfiguration = new SubLoggerConfiguration();
configuration.GetSection("Serilog:SubLogger").Bind(subLoggerConfiguration);

Log.Logger = new LoggerConfiguration() //NOSONAR
    .Enrich.FromLogContext()
    .Enrich.WithProperty("ApplicationContext", AppName)
    .Enrich.WithExceptionDetails()
    .ReadFrom.Configuration(configuration)
    .WriteTo.Console()
    .WriteTo.Logger(l =>
        l.Filter.ByIncludingOnly(e => e.Level == subLoggerConfiguration.Error.Level)
        .WriteTo.File(new JsonFormatter(),
                        subLoggerConfiguration.Error.PathFormat,
                        rollingInterval: RollingInterval.Day,
                        rollOnFileSizeLimit: true,
                        fileSizeLimitBytes: 100000000))
    .WriteTo.Logger(l =>
        l.Filter.ByIncludingOnly(e => e.Level == subLoggerConfiguration.Information.Level)
        .WriteTo.File(new JsonFormatter(),
                        subLoggerConfiguration.Information.PathFormat,
                        rollingInterval: RollingInterval.Day,
                        rollOnFileSizeLimit: true,
                        fileSizeLimitBytes: 100000000))
    .WriteTo.Logger(l =>
        l.Filter.ByIncludingOnly(e => e.Level == subLoggerConfiguration.Warning.Level)
        .WriteTo.File(new JsonFormatter(),
                        subLoggerConfiguration.Warning.PathFormat,
                        rollingInterval: RollingInterval.Day,
                        rollOnFileSizeLimit: true,
                        fileSizeLimitBytes: 100000000))
    .CreateLogger();
LogManager.LogFactory = new SerilogFactory();
#endregion 





var builder = WebApplication.CreateBuilder(args);

// Additional configuration is required to successfully run gRPC on macOS.
// For instructions on how to configure Kestrel and gRPC clients on macOS, visit https://go.microsoft.com/fwlink/?linkid=2099682






// Add services to the container.
builder.Services.AddGrpc();

builder.WebHost.UseSerilog()
    .UseKestrel(webBuilder =>
    {
        webBuilder.ListenAnyIP(50001, o => o.Protocols = HttpProtocols.Http2);
    })
    .ConfigureAppConfiguration(x => x.AddConfiguration(configuration))
    .ConfigureServices(services =>
    {
        #region Register ServiceStack License
        Licensing.RegisterLicense(configuration.GetSection("servicestack:license").Value);
        #endregion
        var redisConfig = new KvRedisConfig();
        configuration.GetSection("redis:cache").Bind(redisConfig);
        services.AddSingleton(sc => KvRedisPoolManager.GetClientsManager(redisConfig));
        services.AddSingleton<ICacheClient, KvCacheClient>();
        services.AddSingleton<IKvLockRedis, KvLockRedis>();

        var redisMqConfig = new KvRedisConfig();
        configuration.GetSection("redis:message").Bind(redisMqConfig);
        services.AddSingleton(redisMqConfig);
        var redisFactory = KvRedisPoolManager.GetClientsManager(redisMqConfig);
        var mqHost = new RedisMqServer(redisFactory, retryCount: 2);
        services.AddSingleton<IMessageService>(mqHost);

        services.AddSingleton<IAppSettings>(x => new NetCoreAppSettings(configuration));


        #region Register DatabaseConnectionString
        var kvSql = new KvSqlConnectString();
        configuration.GetSection("SqlConnectStrings").Bind(kvSql);
        var connectFactory = new OrmLiteConnectionFactory(kvSql.KvChannelEntities, SqlServer2016Dialect.Provider);
        connectFactory.RegisterConnection(nameof(kvSql.KvMasterEntities), kvSql.KvMasterEntities, SqlServer2016Dialect.Provider);
        foreach (var item in kvSql.KvEntitiesDC1)
        {
            connectFactory.RegisterConnection(item.Name.ToLower(), item.Value, SqlServer2016Dialect.Provider);
        }

        OrmLiteConfig.DialectProvider.GetStringConverter().UseUnicode = true;
        OrmLiteConfig.CommandTimeout = 200;
        services.AddSingleton(kvSql);
        services.AddSingleton<IDbConnectionFactory>(c => connectFactory);
        #endregion


        #region Register kafka
        var kafka = new Demo.OmniChannel.Utilities.Kafka();
        configuration.GetSection("Kafka").Bind(kafka);
        KafkaClient.Instance.SetKafkaConsumerConfig(kafka);
        KafkaClient.Instance.SetKafkaProducerConfig(kafka);
        KafkaClient.Instance.InitProducer();
        #endregion


        #region Register Mongo
        services.Configure<MongoDbSettings>(configuration.GetSection("mongodb"));
        services.UsingMongoDb(configuration);
        services.AddMongoRepository();
        #endregion


        #region Register Service
        services.AddDomainService();
        services.AddInternalClient();
        services.AddService();
        services.AddDIBusinessClient();
        services.AddBusiness();
        #endregion


        #region Register Event bus, guarantee
        services.AddConfigEventBus(configuration);
        services.AddEventBus();
        services.AddConfigConfigGuaranteeClient(configuration);
        services.RegisterDemoGuaranteeClientService();
        #endregion

        #region Set EncryptPassPhrase
        CryptoHelper.PassPhrase = configuration.GetSection("EncryptPassPhrase").Value;
        if (string.IsNullOrWhiteSpace(CryptoHelper.PassPhrase))
        {
            throw new KvException("Setting EncryptPassPhrase invalid. Check setting file, please.");
        }
        #endregion

        #region Register DcControl
        services.RegisterApplicationDcControl(new ApplicationDcInputOptions
        {
            ServiceName = "OrderGRPCService",
            AppSettings = new NetCoreAppSettings(configuration)
        });
        #endregion
    })
;

var app = builder.Build();

// Configure the HTTP request pipeline.
app.MapGrpcService<ShopeeOrderV2Service>();
app.MapGrpcService<HealthCheckService>();
app.MapGrpcService<TiktokOrderService>();

app.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");


try
{
    Log.Information($"Starting {AppName}");
    
    await app.RunAsync();
}
catch (Exception ex)
{
    Log.Error(ex, "Host terminated unexpectedly");
    throw;
}
finally
{
    Log.CloseAndFlush();
}




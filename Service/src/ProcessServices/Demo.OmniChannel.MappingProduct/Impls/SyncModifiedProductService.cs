﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Infrastructure.EventBus.Event;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Auth;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Newtonsoft.Json;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogObject = Demo.OmniChannel.Infrastructure.Auditing.LogObject;

namespace Demo.OmniChannel.MappingProduct.Impls
{
    public class SyncModifiedProductService : BaseService<SyncModifiedProductMessage>
    {
        private readonly IProductMongoService _productMongoService;
        private readonly IProductMappingService _productMapMongoService;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private ChannelClient.Impls.ChannelClient _channelClient;
        private readonly ICacheClient _cacheClient;
        private readonly IScheduleService _scheduleService;
        private readonly IOmniChannelAuthService _channelAuthService;
        private readonly IChannelBusiness _channelBusiness;
        private readonly IMappingBusiness _mappingBusiness;
        private readonly IOmniChannelPlatformService _omniChannelPlatformService;
        private readonly IIntegrationEventService _integrationEventService;
        private readonly IRetryRefreshTokenMongoService _retryRefreshTokenMongoService;

        public SyncModifiedProductService(IProductMongoService productMongoService,
            IProductMappingService kvChannelMapScheduleRepo,
            IAppSettings settings,
            ChannelClient.Impls.ChannelClient channelClient,
            IDbConnectionFactory dbConnectionFactory,
            ICacheClient cacheClient,
            IScheduleService scheduleService,
            IOmniChannelAuthService channelAuthService,
            IMessageFactory messageFactory,
            IChannelBusiness channelBusiness,
            IMappingBusiness mappingBusiness,
            IOmniChannelPlatformService omniChannelPlatformService,
            IIntegrationEventService integrationEventService,
            IRetryRefreshTokenMongoService retryRefreshTokenMongoService)
        {
            _channelClient = channelClient;
            _productMongoService = productMongoService;
            _productMapMongoService = kvChannelMapScheduleRepo;
            _settings = settings;
            _dbConnectionFactory = dbConnectionFactory;
            _cacheClient = cacheClient;
            _scheduleService = scheduleService;
            _channelAuthService = channelAuthService;
            _messageFactory = messageFactory;
            _channelBusiness = channelBusiness;
            _mappingBusiness = mappingBusiness;
            _omniChannelPlatformService = omniChannelPlatformService;
            _integrationEventService = integrationEventService;
            _retryRefreshTokenMongoService = retryRefreshTokenMongoService;

        }
        public async Task ProcessSyncModifiedProduct(SyncModifiedProductMessage data)
        {
            string logInfo = string.Empty;
            var timeLastSync = _settings.Get("LastSyncModifiedProductTimeDiff", -12);
            var lastSync = DateTime.Now.AddHours(timeLastSync);
            var syncModifiedProductLog = new LogObject(Logger, data.LogId)
            {
                Action = "SyncModifiedProduct",
                RetailerId = data.RetailerId,
                BranchId = data.BranchId,
                OmniChannelId = data.ChannelId,
                RequestObject = data.ScheduleDto
            };
            syncModifiedProductLog.AddToParentIds(data.ParentLogIds);
            TokenResponse authData = null;
            Domain.Model.OmniChannel channelData = null;
            try
            {
                var channel = await _channelBusiness.GetChannel(data.ChannelId, data.RetailerId, syncModifiedProductLog.Id);

                var connectStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1).ToLower();
                if (channel == null)
                {
                    syncModifiedProductLog.ResponseObject = $"ChannelId {data.ChannelId} not found";
                    syncModifiedProductLog.LogInfo();
                    return;
                }
                channelData = channel;
                var context = await ContextHelper.GetExecutionContext(_cacheClient, _dbConnectionFactory, data.RetailerId, connectStringName, data.BranchId, _settings?.Get<int>("ExecutionContext"));
                var auth = await _channelAuthService.GetChannelAuth(channel, syncModifiedProductLog.Id);
                if (auth == null)
                {
                    throw new KvException($"SyncModifiedProduct ChannelId:{data.ChannelId} RetailerId:{data.RetailerId} Unauthorized: Access is denied due to invalid credentials");
                }
                authData = auth;
                var platform = await _omniChannelPlatformService.GetById(channel.PlatformId);
                _channelClient = new ChannelClient.Impls.ChannelClient(_settings);
                var client = _channelClient.CreateClient(data.ChannelType, context.GroupId, context.RetailerId);
                var response = await client.GetListProduct(
                    new ChannelAuth { ShopId = auth.ShopId, AccessToken = auth.AccessToken }, data.ChannelId,
                    syncModifiedProductLog.Id, false, data.LastSyncDateTime, context.ConvertTo<KvInternalContext>(), platform);
                if (response == null || (response.Total <= 0 && response.RemovedProductIds?.Count <= 0))
                {
                    await RunningSchedule(data.ScheduleDto, data.LastSyncDateTime);
                    if (response != null && response.TokenIsExpiredOrInvalid)
                    {
                        syncModifiedProductLog.Description = "TokenIsExpiredOrInvalid";
                        syncModifiedProductLog.LogError();
                    }
                    return;
                }

                var newProducts = new List<Product>();
                var updateProducts = new List<ChannelClient.RequestDTO.Product>();
                var delProductIds = new List<string>();
                var changedItemIds = new List<string>();
                (newProducts, updateProducts, delProductIds, changedItemIds) = await GetListProductsByStatus(data, response, channel);

                if (channel.Type == (byte)ChannelTypeEnum.Lazada)
                {
                    var delProductIdsResponse = await LazadaDeleteProduct(data, context, channel, syncModifiedProductLog);

                    delProductIds.AddRange(delProductIdsResponse);
                }

                if (delProductIds.Any())
                {
                    await _productMongoService.RemoveByItemIds(channel.RetailerId, channel.Id, delProductIds, isStringItemId: ConvertHelper.CheckUseStringItemId(channel?.Type));
                    var configMappingV2Feature = _settings?.Get<MappingV2Feature>("MappingV2Feature");
                    if (configMappingV2Feature == null)
                    {
                        throw new KvException("Get MappingV2Feature failed");
                    }

                    var removeMsg = new RemoveProductMappingMessage
                    {
                        RetailerId = data.RetailerId,
                        BranchId = data.BranchId,
                        ChannelIds = new List<long> { data.ChannelId },
                        ChannelProductIds = delProductIds,
                        ConnectStringName = connectStringName,
                        IsDeleteKvProduct = false,
                        LogId = syncModifiedProductLog.Id,
                        MessageFrom = "SyncModifiedProduct-RemoveMapping"
                    };
                    var kafkaMessage = new KafkaMessage(configMappingV2Feature.TopicRemoveMappingRequestName,
                        $"{removeMsg.RetailerId}_{removeMsg.BranchId}_{data.ChannelId}", removeMsg);
                    KafkaClient.KafkaClient.Instance.PublishMessage(kafkaMessage.TopicName, kafkaMessage.Key, JsonConvert.SerializeObject(kafkaMessage), isV2: true);
                }

                if (newProducts.Any())
                {
                    await _productMongoService.BatchAddAsync(newProducts);
                    PushEventNewProductToSyncPosOnline(context, data, newProducts, syncModifiedProductLog);
                }
                if (updateProducts.Any())
                {
                    await UpdateProductAndMapping(connectStringName, data.RetailerId, channel, data.BranchId, updateProducts, syncModifiedProductLog.Id);
                }

                await RunningSchedule(data.ScheduleDto, lastSync);
                if (channel.IsAutoMappingProduct && changedItemIds.Count > 0)
                {
                    var configMappingV2Feature = _settings.Get<MappingV2Feature>("MappingV2Feature");
                    if (configMappingV2Feature == null)
                    {
                        throw new KvException("Get MappingV2Feature failed");
                    }

                    var mappingRequest = new MappingRequest
                    {
                        ChannelId = data.ChannelId,
                        ChannelType = data.ChannelType,
                        RetailerId = data.RetailerId,
                        BranchId = data.BranchId,
                        RetailerCode = data.RetailerCode,
                        KvEntities = data.KvEntities,
                        IsIgnoreAuditTrail = data.IsIgnoreAuditTrail,
                        LogId = syncModifiedProductLog.Id,
                        ItemIds = changedItemIds,
                        MessageFrom = "SyncModifiedProduct-NewMapping"
                    };
                    var kafkaMessage = new KafkaMessage(configMappingV2Feature.TopicMappingRequestName,
                        $"{mappingRequest.RetailerId}_{mappingRequest.BranchId}_{mappingRequest.ChannelId}", mappingRequest);
                    KafkaClient.KafkaClient.Instance.PublishMessage(kafkaMessage.TopicName, kafkaMessage.Key, JsonConvert.SerializeObject(kafkaMessage), isV2: true);
                }

                syncModifiedProductLog.ResponseObject = logInfo;
                syncModifiedProductLog.LogInfo();

            }
            catch (OmniException ex)
            {
                if (authData != null && channelData.Type == (byte)ChannelTypeEnum.Lazada &&
                    (ex.ErrorCode.Equals(LazadaErrorCode.IllegalAccessToken) || ex.ErrorCode.Equals(LazadaErrorCode.IllegalRefreshToken)))
                {
                    await HandleRetryRefreshToken(authData, channelData, syncModifiedProductLog);
                }
                await RunningSchedule(data.ScheduleDto);
                syncModifiedProductLog.LogError(new OmniException($"{ex.Message}, StackTrace:{ex.StackTrace}"));
            }
        }

        private void PushEventNewProductToSyncPosOnline(ExecutionContext context, SyncModifiedProductMessage data, List<Product> products, LogObject syncModifiedProductLog)
        {
            var logPushSyncEvent = syncModifiedProductLog.Clone("PushEventNewProductToSyncPosOnline");
            var obj = new NewProductIntegrationEvent
            {
                RetailerId = data.RetailerId,
                NewProducts = products,
                RetailerCode = context.RetailerCode,
                UserId = context?.User?.Id,
                ChannelId = data.ChannelId,
            };
            _integrationEventService.AddEventWithRoutingKeyAsync(obj, RoutingKey.AddNewProduct);
            logPushSyncEvent.ResponseObject = obj.ToJson();
            logPushSyncEvent.LogInfo(true);
        }

        private async Task<(List<Product>, List<ChannelClient.RequestDTO.Product>, List<string>, List<string>)> GetListProductsByStatus(
            SyncModifiedProductMessage data, ChannelClient.RequestDTO.ProductListResponse response,
            Domain.Model.OmniChannel channel)
        {
            var newProducts = new List<Product>();
            var updateProducts = new List<ChannelClient.RequestDTO.Product>();
            var delProductIds = new List<string>();
            var changeItemIds = response.Data.Select(x => x.ItemId).Union(response.RemovedProductIds).Union(response.ParentChangeIds).Distinct().ToList();
            if (changeItemIds.Count > 1000)
            {
                changeItemIds = null;
            }
            var listCurrentProduct = await _productMongoService.Get(data.RetailerId, new List<long> { data.ChannelId }, changeItemIds, isStringItemId: ConvertHelper.CheckUseStringItemId(channel.Type));

            foreach (var item in response.Data)
            {
                if (item.ItemId != item.ParentItemId)
                {
                    var existParentProduct = listCurrentProduct
                        .Any(x => x.RetailerId == data.RetailerId && x.ChannelId == data.ChannelId && x.CommonItemId == item.ParentItemId);
                    if (existParentProduct)
                    {
                        delProductIds.Add(item.ParentItemId);
                    }
                }
                else
                {
                    var deleteProducts = listCurrentProduct
                        .Where(x => x.RetailerId == data.RetailerId
                        && x.ChannelId == data.ChannelId
                        && x.CommonParentItemId != x.CommonItemId
                        && x.CommonParentItemId == item.ItemId);

                    if (deleteProducts.Any())
                    {
                        delProductIds.AddRange(deleteProducts.Select(x => x.CommonItemId).ToList());
                    }
                }

                var existProduct = listCurrentProduct
                    .Any(x => x.RetailerId == data.RetailerId
                    && x.ChannelId == data.ChannelId
                    && x.CommonItemId == item.ItemId);

                if (existProduct)
                {
                    updateProducts.Add(item);
                }
                else
                {
                    var exist = newProducts.Any(x => x.CommonItemId == item.ItemId);
                    if (!exist)
                    {
                        newProducts.Add(new Product
                        {
                            BranchId = channel.BranchId,
                            CommonItemId = item.ItemId,
                            CommonParentItemId = item.ParentItemId,
                            ItemSku = item.ItemSku.Trim(),
                            ItemName = item.ItemName,
                            ItemImages = item.ItemImages,
                            RetailerId = data.RetailerId,
                            RetailerCode = data.RetailerCode,
                            ChannelId = data.ChannelId,
                            Type = item.Type,
                            CreatedDate = DateTime.Now,
                            ChannelType = data.ChannelType,
                            IsConnected = false,
                            Status = item.Status,
                            SuperId = item.SuperId,
                        });
                    }
                }
            }

            var changedItemIds = updateProducts.Select(x => x.ItemId).Union(newProducts.Select(x => x.CommonItemId)).ToList();

            if (response.ParentChangeIds.Any())
            {
                foreach (var parentId in response.ParentChangeIds)
                {
                    var variantIds = listCurrentProduct.Where(x => x.CommonParentItemId == parentId).Select(x => x.CommonItemId);
                    var deletedVariantIds = variantIds.Where(x => !changedItemIds.Contains(x)).ToList();
                    delProductIds.AddRange(deletedVariantIds);
                }
            }

            if (response.RemovedProductIds.Any())
            {
                var channelProductDeleted = listCurrentProduct.Where(x => response.RemovedProductIds.Contains(x.CommonItemId));
                var deletedItemIds = channelProductDeleted.Select(x => x.CommonItemId).ToList();
                delProductIds.AddRange(deletedItemIds);
            }
            return (newProducts, updateProducts, delProductIds, changedItemIds);
        }

        private async Task RunningSchedule(ScheduleDto dto, DateTime? lastSync = null)
        {
            var sche = new ScheduleHelper(_scheduleService);
            if (lastSync != null)
            {
                dto.LastSync = lastSync;
                sche.ReleaseSchedule(dto);

                using (var db = _dbConnectionFactory.OpenDbConnection())
                {
                    var kvChannelRepository = new OmniChannelScheduleRepository(db);
                    await kvChannelRepository.UpdateExecutePlan(dto.Id, lastSync, null);
                }

                return;
            }
            sche.UnlockSchedule(dto);
        }

        private async Task UpdateProductAndMapping(string connectStringName, int retailerId, Domain.Model.OmniChannel channel, int branchId, List<ChannelClient.RequestDTO.Product> products, Guid logId)
        {
            if (products?.Any() != true) return;

            var mappingDeletes = new List<ProductMapping>();
            if (channel.IsAutoDeleteMapping == true)
            {
                mappingDeletes = await _productMapMongoService.GetMappingForDelete(retailerId, channel.Id, products, ConvertHelper.CheckUseStringItemId(channel.Type));
            }

            await _productMongoService.UpdateProducts(retailerId, channel.Id, channel.Type, products, ConvertHelper.CheckUseStringItemId(channel.Type));
            await _productMapMongoService.UpdateMappings(retailerId, channel.Id, products, ConvertHelper.CheckUseStringItemId(channel.Type), _cacheClient);

            // Xóa mapping sau khi mapping cũ đã update (cần cho việc ghi audit)
            if (mappingDeletes?.Any() == true)
            {
                var configMappingV2Feature = _settings?.Get<MappingV2Feature>("MappingV2Feature");
                if (configMappingV2Feature == null)
                {
                    throw new KvException("Get MappingV2Feature failed");
                }

                var removeMsg = new RemoveProductMappingMessage
                {
                    RetailerId = retailerId,
                    BranchId = branchId,
                    ChannelIds = new List<long> { channel.Id },
                    ChannelProductIds = mappingDeletes.Select(x => x.CommonProductChannelId).ToList(),
                    ConnectStringName = connectStringName,
                    IsDeleteKvProduct = false,
                    LogId = logId,
                    MessageFrom = "SyncModifiedProduct-AfterUpdate"
                };
                var kafkaMessage = new KafkaMessage(configMappingV2Feature.TopicRemoveMappingRequestName,
                    $"{removeMsg.RetailerId}_{removeMsg.BranchId}_{channel.Id}", removeMsg);
                KafkaClient.KafkaClient.Instance.PublishMessage(kafkaMessage.TopicName, kafkaMessage.Key, JsonConvert.SerializeObject(kafkaMessage), isV2: true);
                _mappingBusiness.SetCacheWaitingDeleteMapping(channel.Id, mappingDeletes.Select(x => x.ProductKvId).ToList());
            }
        }

        private async Task<List<string>> LazadaDeleteProduct(SyncModifiedProductMessage data, ExecutionContext context, Domain.Model.OmniChannel channel, LogObject syncModifiedProductLog)
        {
            var client = _channelClient.GetClient(data.ChannelType, _settings);
            var auth = await _channelAuthService.GetChannelAuth(channel, syncModifiedProductLog.Id);
            var delProductIds = new List<string>();
            var listDeletedProduct = await client.GetListDeletedProduct(
                                    new ChannelAuth { ShopId = auth.ShopId, AccessToken = auth.AccessToken }, data.ChannelId,
                                    syncModifiedProductLog.Id, false, context.ConvertTo<KvInternalContext>(), data.LastSyncDateTime);

            if (!listDeletedProduct.VariationProductActives.Any()) return delProductIds;

            var parentIdsDeleted = listDeletedProduct.VariationProductActives.Select(x => x.ParentId).ToList();
            var currentProduct = await _productMongoService.GetProductByParentIds(data.RetailerId,
                new List<long> { data.ChannelId }, parentIdsDeleted);

            var productMappingVariations = currentProduct?.Where(x =>
                listDeletedProduct.VariationProductActives.Any(variation => x.CommonParentItemId == variation.ParentId)).ToList();

            var productChannelActives =
                listDeletedProduct.VariationProductActives.Where(pv =>
                    pv.VariationProductActiveIds != null).SelectMany(pv => pv.VariationProductActiveIds).ToList();

            var productMappingVariationDeletes =
                productMappingVariations?.Where(p => productChannelActives.Contains(p.CommonItemId)).ToList();

            var productMappingVariationDeletesIds =
                productMappingVariationDeletes?.Select(x => x.CommonItemId).ToList();

            if (productMappingVariationDeletesIds != null)
                delProductIds.AddRange(productMappingVariationDeletesIds);

            return delProductIds;
        }

        private async Task HandleRetryRefreshToken(TokenResponse authData, Domain.Model.OmniChannel data, LogObject logger)
        {
            try
            {
                var retryRefreshToken = new RetryRefreshToken
                {
                    RetailerId = data.RetailerId,
                    BranchId = data.BranchId,
                    ChannelId = data.Id,
                    ChannelType = data.Type,
                    AuthId = authData.Id,
                    ShopId = data.IdentityKey,
                    ChannelName = data.Name,
                    RetryCount = 0,
                    IsActivate = true,
                    PlatformId = data.PlatformId,
                    CreatedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now
                };
                if (!string.IsNullOrEmpty(authData.AccessToken))
                {
                    retryRefreshToken.AccessToken = CryptoHelper.RijndaelEncrypt(authData.AccessToken);
                }

                if (!string.IsNullOrEmpty(authData.RefreshToken))
                {
                    retryRefreshToken.RefreshToken = CryptoHelper.RijndaelEncrypt(authData.RefreshToken);
                }
                var result = await _retryRefreshTokenMongoService.AddWithoutExist(retryRefreshToken);
                if (result != null)
                {
                    logger.ResponseObject = $"Create RetryRefreshToken succeed with id: {result.Id}";
                    logger.LogInfo();
                }
            }
            catch(Exception ex)
            {
                logger.LogError(new LazadaException($"{ex.Message}, StackTrace:{ex.StackTrace}"));
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Newtonsoft.Json;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.MappingProduct.Impls
{
    public class MappingProductService : BaseService<MappingProductMessage>
    {
        private readonly IProductMongoService _productMongoService;
        private readonly IProductMappingService _productMappingService;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly ICacheClient _cacheClient;
        private readonly IChannelBusiness _channelBusiness;
        private readonly SelfAppConfig _config;

        public MappingProductService(IProductMongoService productMongoService,
            IProductMappingService productMappingService,
            IDbConnectionFactory dbConnectionFactory,
            ICacheClient cacheClient,
        IAppSettings settings, IMessageFactory messageFactory, IChannelBusiness channelBusiness)
        {
            _productMongoService = productMongoService;
            _productMappingService = productMappingService;
            _dbConnectionFactory = dbConnectionFactory;
            _cacheClient = cacheClient;
            _settings = settings;
            _messageFactory = messageFactory;
            _channelBusiness = channelBusiness;
            _config = new SelfAppConfig(settings);
        }
        public async Task ProcessMappingProduct(MappingProductMessage data)
        {
            var mappingProductLog = new LogObject(Logger, data.LogId)
            {
                Action = "MappingProduct",
                RetailerId = data.RetailerId,
                RetailerCode =  data.RetailerCode,
                BranchId = data.BranchId,
                OmniChannelId = data.ChannelId
            };


            var channelItems = await _productMongoService.Get(data.RetailerId, new List<long> { data.ChannelId }, data.ItemIds, isConnected: false, isStringItemId: ConvertHelper.CheckUseStringItemId(data.ChannelType));
            if (channelItems.Count <= 0)
            {
                mappingProductLog.Description = "Empty Mapping Product";
                mappingProductLog.LogInfo();
                return;
            }

            var channel = await _channelBusiness.GetChannel(data.ChannelId, data.RetailerId, data.LogId);

            if (channel == null)
            {
                mappingProductLog.Description = "Channel is null";
                mappingProductLog.LogInfo();
                return;
            }
            
            var connectStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);

            var pageSize = _settings?.Get<int>("mappingPageSize") ?? 100;
            channelItems.Sort((item1, item2) => string.Compare(item1.ItemSku, item2.ItemSku));
            var currentOffset = 0;

            while (currentOffset < channelItems.Count)
            {
                var items = GetNextPage(currentOffset, channelItems, pageSize);
                await ProcessCurrentPageMapping(channel, connectStringName, data.LogId, items);
                currentOffset += items.Count;
            }
            mappingProductLog.RequestObject = $"Requested products: {string.Join(',', data.ItemIds)}";
            mappingProductLog.Description = $"Channel:OmnichannelId:{channel.Id},IsActive:{channel.IsActive},IsAutoMapping:{channel.IsAutoMappingProduct},IsAutoDelete:{channel.IsAutoDeleteMapping}";
            mappingProductLog.ResponseObject =
                $"Product no mapping: {string.Join(",", channelItems.Select(x => x.ItemId).ToList())}";
            mappingProductLog.TotalItem = channelItems.Count;
            mappingProductLog.LogInfo();
        }

        private List<MongoDb.Product> GetNextPage(int startFrom, List<MongoDb.Product> channelItems, int pageSize)
        {
            var ilast = startFrom + pageSize - 1;
            ilast = ilast + 1 > channelItems.Count ? channelItems.Count - 1 : ilast;
            // if Sku is empty then paging normally
            if (!String.IsNullOrEmpty(channelItems[ilast].ItemSku)
                && ilast+1 < channelItems.Count
                && channelItems[ilast].ItemSku == channelItems[ilast+1].ItemSku)
            {
                var dupsku = channelItems[ilast].ItemSku;
                // if current range filled with 1 sku, then extend it to map all sku
                if (channelItems[startFrom].ItemSku == channelItems[ilast].ItemSku)
                {
                    // extend
                    var eilast = channelItems.FindIndex(ilast+2, item => item.ItemSku != dupsku);
                    ilast = eilast > 0 ? eilast-1 : ilast+1;
                }
                else // if not, truncate it
                {
                    // truncate
                    var tilast = channelItems.FindLastIndex(ilast-1, ilast - startFrom, item => item.ItemSku != dupsku);
                    ilast = tilast > 0 ? tilast : startFrom;
                }
                
            }

            return channelItems.GetRange(startFrom, ilast - startFrom + 1);
        }

        private async Task ProcessCurrentPageMapping(Domain.Model.OmniChannel channel, string connectStringName, Guid logId, List<MongoDb.Product> items)
        {
            var mappingProductLog = new LogObject(Logger, logId)
            {
                Action = "CurrentPageMapping",
                RetailerId = channel.RetailerId,
                BranchId = channel.BranchId,
                OmniChannelId = channel.Id,
                RequestObject = items.Select(x => x.ItemId).ToList()
            };
            try
            {
                List<Product> kvProducts;
                List<ProductMappingPre> productMapping;
                var activeFeatureMultiSku = SelfAppConfig.CheckActiveFeature(SelfAppConfig.UseMultiMappingSku, channel.RetailerId);
                var codes = new HashSet<string>(items.GroupBy(x => x.ItemSku).Select(p => p.FirstOrDefault()?.ItemSku.Trim()).ToList());
                if (!codes.Any()) return;
                using (var db = await _dbConnectionFactory.OpenAsync(connectStringName.ToLower()))
                {
                    var productRepo = new ProductRepository(db);
                    kvProducts = await productRepo.GetProductByCode(channel.RetailerId, channel.BranchId, codes);
                }

                if (activeFeatureMultiSku && channel.Type == (byte)ChannelType.Shopee)
                {
                    productMapping = await AddMultiProductMappingPres(kvProducts, channel, items);
                }
                else
                {
                    productMapping = await AddProductMappingPres(kvProducts, channel, items);
                }

                if (productMapping.Any())
                {
                    var configMappingV2Feature = _settings?.Get<MappingV2Feature>("MappingV2Feature");
                    var coreContext = await ContextHelper.GetExecutionContext(_cacheClient, _dbConnectionFactory, channel.RetailerId, connectStringName, channel.BranchId, _settings?.Get<int>("ExecutionContext"));
                    if (configMappingV2Feature != null &&
                        configMappingV2Feature.IsValid(coreContext.Group?.Id ?? 0, channel.RetailerId))
                    {
                        var mappingRequest = new MappingRequest
                        {
                            ConnectStringName = connectStringName,
                            RetailerId = channel.RetailerId,
                            BranchId = channel.BranchId,
                            ChannelId = channel.Id,
                            ProductMappings = productMapping,
                            IsAddNew = true,
                            LogId = logId,
                            MessageFrom = "CurrentPageMapping"
                        };
                        var kafkaMessage = new KafkaMessage(configMappingV2Feature.TopicMappingRequestName,
                            $"{mappingRequest.RetailerId}_{mappingRequest.BranchId}_{mappingRequest.ChannelId}", mappingRequest);
                        KafkaClient.KafkaClient.Instance.PublishMessage(kafkaMessage.TopicName, kafkaMessage.Key, JsonConvert.SerializeObject(kafkaMessage), isV2: true);
                    }
                    else
                    {
                        using (var mq = _messageFactory.CreateMessageProducer())
                        {
                            var addMappingMessage = new CreateProductMappingMessage
                            {
                                ConnectStringName = connectStringName,
                                RetailerId = channel.RetailerId,
                                BranchId = channel.BranchId,
                                ChannelId = channel.Id,
                                ProductMappings = productMapping,
                                IsAddNew = true,
                                LogId = logId,
                                MessageFrom = "CurrentPageMapping"
                            };
                            mq.Publish(addMappingMessage);
                        }
                    }
                }

                mappingProductLog.ResponseObject = productMapping;
                mappingProductLog.TotalItem = productMapping.Count;
                mappingProductLog.LogInfo();
            }
            catch (Exception e)
            {
                mappingProductLog.LogError(e);
                throw;
            }
        }

        #region Private method

        public async Task<List<ProductMappingPre>> AddProductMappingPres(List<Product> kvProducts, Domain.Model.OmniChannel channel, List<MongoDb.Product> items)
        {
            var productMapping = new List<ProductMappingPre>();

            var kvProductIds = (await _productMappingService.GetByRetailerChannelId(channel.RetailerId, channel.Id))?.Select(x => x.ProductKvId).ToList();
            var kvProductMappedIds = kvProductIds?.Any() == true ? new HashSet<long>(kvProductIds) : new HashSet<long>();
            if (kvProductMappedIds.Any())
            {
                kvProducts = kvProducts.Where(x => !kvProductMappedIds.Contains(x.Id)).ToList();
            }
            var kvCacheClient = (IKvCacheClient)_cacheClient;
            var lockMappingChannelIdKey = string.Format(KvConstant.LockMappingChannelId, channel.Id);
            foreach (var p in kvProducts)
            {
                var productMap = new ProductMappingPre
                {
                    ProductKvId = p.Id,
                    ProductKvSku = p.Code.Trim(),
                    ProductKvFullName = p.FullName,
                    RetailerId = channel.RetailerId,
                    ChannelId = channel.Id
                };
                var channelProduct = items.FirstOrDefault(x => x.ItemSku.Equals(p.Code));
                if (channelProduct != null)
                {
                    productMap.ProductChannelId = channelProduct.CommonItemId;
                    productMap.ParentProductChannelId = channelProduct.CommonParentItemId;
                    productMap.ProductChannelType = channelProduct.Type;
                    productMap.ProductChannelSku = channelProduct.ItemSku.Trim();
                    productMap.ProductChannelName = channelProduct.ItemName;
                    productMap.ChannelProductKey = channelProduct.Id;
                    productMap.ProductChannelObjectIdKey = channelProduct.Id;

                    var existed = productMapping.Any(x => x.ChannelId == channel.Id && (x.ProductChannelId == productMap.ProductChannelId || x.ProductChannelSku == productMap.ProductChannelSku));
                    var productIdIsProcessing = kvCacheClient.SetContainsItem(lockMappingChannelIdKey, productMap.ProductChannelSku); //Kiểm tra kvproduct đang được xử lý mapping chưa, tránh nhiều hàng trên sàn cùng mapping với 1 hàng KV

                    if (!existed && !productIdIsProcessing)
                    {
                        productMapping.Add(productMap);
                        if (!kvCacheClient.ContainsKey(lockMappingChannelIdKey))
                        {
                            kvCacheClient.InitSet(lockMappingChannelIdKey, new List<string> { productMap.ProductChannelSku });
                            kvCacheClient.ExpireEntryAt(lockMappingChannelIdKey, DateTime.Now.AddMinutes(2));
                        }
                        else
                        {
                            kvCacheClient.AddItemToSet(lockMappingChannelIdKey, productMap.ProductChannelSku);
                        }
                    }
                }
            }

            return productMapping;
        }

        public async Task<List<ProductMappingPre>> AddMultiProductMappingPres(List<Product> kvProducts, Domain.Model.OmniChannel channel, List<MongoDb.Product> items)
        {
            var productMapping = new List<ProductMappingPre>();
            var lstMapping = await _productMappingService.GetByProductChannelIds(channel.RetailerId, channel.Id, items.Select(x => x.CommonItemId).ToList(), ConvertHelper.CheckUseStringItemId(channel.Type));
            foreach (var p in kvProducts)
            {
                var channelProducts = items.Where(x => x.ItemSku == p.Code).ToList();
                foreach (var channelProduct in channelProducts)
                {
                    var mappingExist =
                        lstMapping.FirstOrDefault(x => x.CommonProductChannelId == channelProduct.CommonItemId);
                    if (mappingExist != null) continue;
                    var productMap = new ProductMappingPre
                    {
                        ProductChannelObjectIdKey = channelProduct.Id,
                        ProductKvId = p.Id,
                        ProductKvSku = p.Code.Trim(),
                        ProductKvFullName = p.FullName,
                        RetailerId = channel.RetailerId,
                        ChannelId = channel.Id,
                        ProductChannelId = channelProduct.CommonItemId,
                        ParentProductChannelId = channelProduct.CommonParentItemId,
                        ProductChannelType = channelProduct.Type,
                        ProductChannelSku = channelProduct.ItemSku.Trim(),
                        ProductChannelName = channelProduct.ItemName,
                        ChannelProductKey = channelProduct.Id,
                    };
                    var existed = productMapping.Any(x => x.ChannelId == channel.Id && x.ProductChannelId == productMap.ProductChannelId);

                    if (!existed )
                    {
                        productMapping.Add(productMap);
                    }
                }
            }

            return productMapping;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Demo.Audit.Model.Message;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ShareKernel.Auth;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Common;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Newtonsoft.Json;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using Product = Demo.OmniChannel.Domain.Model.Product;

namespace Demo.OmniChannel.MappingProduct.Impls
{
    public class CreateProductMappingService : BaseService<CreateProductMappingService>
    {
        private readonly IProductMongoService _productMongoService;
        private readonly IProductMappingService _productMappingService;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly ICacheClient _cacheClient;
        private readonly IAuditTrailInternalClient _auditTrailInternalClient;
        private readonly IOnHandBusiness _onHandBusiness;
        private readonly IPriceBusiness _priceBusiness;
        private readonly IChannelBusiness _channelBusiness;
        private readonly IMappingBusiness _mappingBusiness;
        private readonly ChannelClient.Common.SelfAppConfig _config;

        public CreateProductMappingService(IDbConnectionFactory dbConnectionFactory, IAppSettings settings,
            ICacheClient cacheClient, IProductMappingService productMappingService, IProductMongoService productMongoService,
            IAuditTrailInternalClient auditTrailInternalClient, IOnHandBusiness onHandBusiness, IPriceBusiness priceBusiness, IMessageFactory messageFactory, IChannelBusiness channelBusiness, IMappingBusiness mappingBusiness)
        {
            _dbConnectionFactory = dbConnectionFactory;
            _productMappingService = productMappingService;
            _productMongoService = productMongoService;
            _auditTrailInternalClient = auditTrailInternalClient;
            _settings = settings;
            _cacheClient = cacheClient;
            _onHandBusiness = onHandBusiness;
            _priceBusiness = priceBusiness;
            _messageFactory = messageFactory;
            _channelBusiness = channelBusiness;
            _mappingBusiness = mappingBusiness;
            _config = new ChannelClient.Common.SelfAppConfig(settings);
        }
        public async Task ProcessCreateMappingProduct(CreateProductMappingMessage data)
        {
            var mappingLog = new LogObject(Logger, data.LogId)
            {
                Action = "CreateMappingProduct",
                RetailerId = data.RetailerId,
                BranchId = data.BranchId,
                OmniChannelId = data.ChannelId,
                RequestObject = data.ProductMappings.Select(x => new ProductMappingPre
                {
                    ProductKvId = x.ProductKvId, ProductKvSku = x.ProductKvSku, ProductKvFullName = x.ProductKvFullName,
                    ProductChannelId = x.ProductChannelId, ProductChannelSku = x.ProductChannelSku, ProductChannelName = x.ProductChannelName,
                    ChannelProductKey = x.ChannelProductKey
                })
            };
            try
            {
                var channel = (await GetChannelsByRetailer(data.RetailerId)).FirstOrDefault(x => x.Id == data.ChannelId);
                if (channel == null)
                {
                    mappingLog.Description = "Channel is null";
                    mappingLog.LogInfo();
                    return;
                }

                var context = await ContextHelper.GetExecutionContext(_cacheClient, _dbConnectionFactory, data.RetailerId, data.ConnectStringName, data.BranchId, _settings.Get<int>("ExecutionContext"));
                var newProductMapping = new List<ProductMapping>();

                List<Product> kvProducts;
                using (var dbRetail = await _dbConnectionFactory.OpenAsync(data.ConnectStringName.ToLower()))
                {
                    using (dbRetail.OpenTransaction(IsolationLevel.ReadUncommitted))
                    {
                        var productRepo = new ProductRepository(dbRetail);
                        var productIds = data.ProductMappings.Select(x => x.ProductKvId).ToList();
                        kvProducts = await productRepo.WhereAsync(x => x.RetailerId == data.RetailerId && productIds.Contains(x.Id) && (x.isDeleted == null || x.isDeleted == false));
                    }
                }

                var channelProductKeys = new List<string>();
                // logging for KOL-3350  
                var skipMapping = new List<long>();
                foreach (var mapping in data.ProductMappings)
                {
                    var kvProduct = kvProducts.FirstOrDefault(x => x.Id == mapping.ProductKvId);
                    if (kvProduct == null)
                    {
                        skipMapping.Add(mapping.ProductKvId);
                        continue;
                    }

                    var fullName = string.IsNullOrEmpty(mapping.ProductKvFullName) ? kvProduct.FullName : mapping.ProductKvFullName;
                    var map = new ProductMapping
                    {
                        RetailerId = data.RetailerId,
                        ChannelId = mapping.ChannelId,
                        ProductKvId = kvProduct.Id,
                        ProductKvSku = kvProduct.Code,
                        ProductKvFullName = fullName,
                        CommonProductChannelId = mapping.ProductChannelId,
                        ProductChannelSku = mapping.ProductChannelSku,
                        ProductChannelName = mapping.ProductChannelName,
                        CommonParentProductChannelId = mapping.ParentProductChannelId,
                        ProductChannelType = mapping.ProductChannelType,
                        CreatedDate = DateTime.Now,
                        LatestBasePrice = kvProduct.BasePrice
                    };

                    newProductMapping.Add(map);
                    if (!string.IsNullOrEmpty(mapping.ChannelProductKey))
                    {
                        channelProductKeys.Add(mapping.ChannelProductKey);
                    }
                }
                var missedChannelProductKey = (newProductMapping.Count != channelProductKeys.Count);

                if (newProductMapping.Any())
                {
                    await _productMappingService.AddOrUpdate(data.RetailerId, newProductMapping);
                    await _productMongoService.UpdateProductsConnect(channelProductKeys, true);
                    var kvProductIdNeedRelate = newProductMapping.Select(x => x.ProductKvId).ToList();
                    UpdateProductFlagRelate(context, data.LogId, kvProductIdNeedRelate, true);

                    var mappingKeyList = newProductMapping.Select(x => string.Format(KvConstant.ListProductMappingCacheKey,
                        x.RetailerId, x.ChannelId, x.ProductKvId)).Distinct().ToList();
                    _cacheClient.RemoveAll(mappingKeyList);
                }

                #region Logs
                if (data.IsAddNew)
                {
                    var productMappingAuditTrail = new ProductMappingAuditTrail { ChannelType = channel?.Type, ChannelName = channel?.Name, Items = new List<ProductMappingItemAuditTrail>() };
                    var listMapping = newProductMapping.Where(x => x.ChannelId == channel.Id);

                    foreach (var mapping in listMapping)
                    {
                        productMappingAuditTrail.Items.Add(new ProductMappingItemAuditTrail
                        {
                            ProductKvSku = mapping.ProductKvSku,
                            ParentProductChannelId = mapping.CommonParentProductChannelId,
                            ProductChannelId = mapping.CommonProductChannelId,
                            ProductChannelSku = mapping.ProductChannelSku
                        });
                    }

                    var logContent = AuditTrailHelper.GetMappingProductContent(productMappingAuditTrail, false);

                    var log = new AuditTrailLog
                    {
                        FunctionId = (int)FunctionType.MappingSalesChannel,
                        Action = (int)AuditTrailAction.Create,
                        CreatedDate = DateTime.Now,
                        BranchId = channel.BranchId,
                        Content = logContent.ToString()
                    };

                    var coreContext = context.ConvertTo<KvInternalContext>();
                    coreContext.UserId = context.User?.Id ?? 0;
                    await _auditTrailInternalClient.AddLogAsync(coreContext, log);

                    var channelMappings = newProductMapping.Where(x => x.ChannelId == channel.Id).ToList();
                    var productChannelIds = channelMappings.Select(x => x.CommonProductChannelId).ToList();
                    var productKvIds = channelMappings.Select(x => x.ProductKvId).ToList();
                    if (productKvIds.Any())
                    {
                        var stageSyncToChannel = new LogStage("SyncToChannel");
                        if (channel.OmniChannelSchedules?.Any(x => x.Type == (byte)ScheduleType.SyncOnhand) == true)
                        {
                            await _onHandBusiness.SyncMultiOnHand(data.ConnectStringName, channel, productChannelIds, productKvIds, false, data.LogId, null, true);
                        }
                        if (channel.OmniChannelSchedules?.Any(x => x.Type == (byte)ScheduleType.SyncPrice) == true)
                        {
                            await _priceBusiness.SyncMultiPrice(data.ConnectStringName, channel, productChannelIds, productKvIds, false, data.LogId, true, 50, null, true, "Queue created from CreateProductMapping");
                        }
                        stageSyncToChannel.EndStage();
                        mappingLog.LogStages.Add(stageSyncToChannel);
                    }
                }
                #endregion

                mappingLog.Description = "Create mappings successful" + (missedChannelProductKey ? " MissedProductKey" : "") + (skipMapping.Count > 0 ? " SkipMapping" : "");
                mappingLog.ResponseObject = new
                {
                    NewProductMapping = newProductMapping.Select(x => new ProductMapping
                    {
                        ProductKvId = x.ProductKvId,
                        ProductKvSku = x.ProductKvSku,
                        ProductChannelId = x.ProductChannelId,
                        ProductChannelSku = x.ProductChannelSku
                    }),
                    SkipMapping = skipMapping
                };
                mappingLog.LogInfo();
            }
            catch (Exception ex)
            {
                mappingLog.Description = ex.Message;
                mappingLog.LogError(ex);
                throw;
            }
        }

        public async Task ProcessRemoveMappingProduct(RemoveProductMappingMessage data)
        {
            var includeRetailIds = _settings.Get<List<int>>("UseDeletedLazadaProduct:IncludeRetailIds");
            var isActiveDeletedLazadaProduct = _settings.Get<string>("UseDeletedLazadaProduct:IsActive");
            var isExistsIncludeRetailerIds = includeRetailIds != null && includeRetailIds.Any();
            var isLazadaGetDeletedProduct = false;
            var activeFeatureMultiSku = ChannelClient.Common.SelfAppConfig.CheckActiveFeature(ChannelClient.Common.SelfAppConfig.UseMultiMappingSku, data.RetailerId);
            if (!string.IsNullOrEmpty(isActiveDeletedLazadaProduct) && Convert.ToBoolean(isActiveDeletedLazadaProduct))
            {
                if (!isExistsIncludeRetailerIds)
                {
                    isLazadaGetDeletedProduct = true;
                }

                if (includeRetailIds != null && includeRetailIds.Contains(data.RetailerId))
                {
                    isLazadaGetDeletedProduct = true;
                }
            }

            try
            {
                var mappingLog = new LogObject(Logger, data.LogId)
                {
                    Action = "RemoveMappingProduct",
                    RetailerId = data.RetailerId,
                    BranchId = data.BranchId,
                    RequestObject = data
                };

                var productMappingDeleted = new List<ProductMapping>();
                var kvPrdIds = data.KvProductIds;
                using (var db = await _dbConnectionFactory.OpenAsync(data.ConnectStringName.ToLower()))
                {
                    var productRepo = new ProductRepository(db);
                    var products = await productRepo.GetProductChildUnitByIds(data.RetailerId, data.BranchId, data.KvProductIds);
                    var productChildUnitIds = products?.Select(m => m.Id).ToList();
                    if (productChildUnitIds != null)
                    {
                        kvPrdIds = kvPrdIds?.Union(productChildUnitIds).ToList();
                    }
                }

                if (kvPrdIds != null && kvPrdIds.Any())
                {
                    productMappingDeleted = await _productMappingService.DeleteByKvProductId(data.RetailerId, data.KvProductIds, data.ChannelIds);
                }

                var channels = await GetChannelsByRetailer(data.RetailerId);
                if (data.ChannelIds != null && data.ChannelIds.Any())
                {
                    channels = channels.Where(x => data.ChannelIds.Contains(x.Id)).ToList();
                }

                if ((data.ChannelProductIds != null && data.ChannelProductIds.Any()) || (data.ChannelProductParentIds != null && data.ChannelProductParentIds.Any()))
                {
                    foreach (var channel in channels)
                    {
                        List<ProductMapping> channelProducts = null;
                        List<string> channelProductIds;
                        List<ProductMapping> mappingDeletedByChannel = null;
                        switch (channel.Type)
                        {
                            case (byte)ChannelTypeEnum.Lazada:
                                if (isLazadaGetDeletedProduct)
                                {
                                    if (data.ChannelProductIds != null && data.ChannelProductIds.Any())
                                    {
                                        channelProducts = await _productMappingService.GetByChannelProductIds(data.RetailerId, channel.Id, data.ChannelProductIds, isStringItemId: ConvertHelper.CheckUseStringItemId(channel.Type));
                                    }
                                    if (data.ChannelProductParentIds != null && data.ChannelProductParentIds.Any())
                                    {
                                        channelProducts = await _productMappingService.GetByChannelProductParentIds(data.RetailerId, channel.Id, data.ChannelProductParentIds);
                                    }
                                    
                                    channelProductIds = channelProducts?.Select(x => x.CommonProductChannelId).ToList();
                                    mappingDeletedByChannel = await _productMappingService.RemoveByChannelProductId(data.RetailerId, channel.Id, channelProductIds, isStringItemId: ConvertHelper.CheckUseStringItemId(channel.Type));

                                }
                                else
                                {
                                    if (data.ChannelProductIds != null && data.ChannelProductIds.Any())
                                    {
                                        channelProducts = await _productMappingService.GetByChannelProductIds(data.RetailerId, channel.Id, data.ChannelProductIds, isStringItemId: ConvertHelper.CheckUseStringItemId(channel.Type));
                                        channelProductIds = channelProducts.Select(x => x.CommonProductChannelId).ToList();
                                        mappingDeletedByChannel = await _productMappingService.RemoveByChannelProductId(data.RetailerId, channel.Id, channelProductIds, isStringItemId: ConvertHelper.CheckUseStringItemId(channel.Type));
                                    }
                                }
                                break;
                            default:
                                if (data.ChannelProductIds != null && data.ChannelProductIds.Any())
                                {
                                    channelProducts = await _productMappingService.GetByChannelProductIds(data.RetailerId, channel.Id, data.ChannelProductIds, isStringItemId: ConvertHelper.CheckUseStringItemId(channel.Type));
                                    channelProductIds = channelProducts.Select(x => x.CommonProductChannelId).ToList();
                                    mappingDeletedByChannel = await _productMappingService.RemoveByChannelProductId(data.RetailerId, channel.Id, channelProductIds, isStringItemId: ConvertHelper.CheckUseStringItemId(channel.Type));
                                }
                                break;
                        }
                        if (mappingDeletedByChannel != null && mappingDeletedByChannel.Any())
                        {
                            productMappingDeleted.AddRange(mappingDeletedByChannel);
                        }
                    }
                }

                if (productMappingDeleted == null || !productMappingDeleted.Any()) return;

                //Update flag mapping for kv product
                var kvProductIds = productMappingDeleted.Select(x => x.ProductKvId).ToList();
                var context = await ContextHelper.GetExecutionContext(_cacheClient, _dbConnectionFactory, data.RetailerId, data.ConnectStringName, data.BranchId, _settings.Get<int>("ExecutionContext"));

                var productMappings = await _productMappingService.Get(data.RetailerId, null, kvProductIds);
                var kvProductIdStillRelates = productMappings.GroupBy(x => x.ProductKvId).Select(x => x.Key).ToList();
                var kvProductIdNeedDels = kvProductIds.Where(x => !kvProductIdStillRelates.Contains(x)).ToList();
                UpdateProductFlagRelate(context, data.LogId, kvProductIdNeedDels, false);

                var mappingKeyList = productMappingDeleted.Select(x => string.Format(KvConstant.ListProductMappingCacheKey,
                    x.RetailerId, x.ChannelId, x.ProductKvId)).Distinct().ToList();
                _cacheClient.RemoveAll(mappingKeyList);

                //Update flag mapping for channel product
                foreach (var item in productMappingDeleted)
                {
                    var channel = await _channelBusiness.GetChannel(item.ChannelId, item.RetailerId, data.LogId);
                    await _productMongoService.UpdateProductConnect(data.RetailerId, item.ChannelId, item.CommonProductChannelId, false, isStringItemId: ConvertHelper.CheckUseStringItemId(channel?.Type));
                    await _productMongoService.UpdateProductSyncSuccess(data.RetailerId, item.ChannelId, item.CommonProductChannelId, isStringItemId: ConvertHelper.CheckUseStringItemId(channel?.Type));
                    _cacheClient.Remove(string.Format(KvConstant.ProductMappingCacheKey, item.ChannelId, data.RetailerId, item.ProductKvId));
                    if (!data.IsDeleteKvProduct)
                    {
                        var branchId = channels.FirstOrDefault(x => x.Id == item.ChannelId)?.BranchId ?? 0;
                        await CreateNewMappingAfterDeleted(item, branchId, data.ConnectStringName, data.LogId, productMappings);
                    }
                }

                //sync onhand
                var channelProductMappingDeleted =
                    channels.Where(x => productMappingDeleted.Select(pm => pm.ChannelId).Contains(x.Id));
                foreach (var channelProd in channelProductMappingDeleted)
                {
                    if (activeFeatureMultiSku && channelProd.Type == (byte) ChannelType.Shopee)
                    {
                        var channelMappings = productMappingDeleted.Where(x => x.ChannelId == channelProd.Id).ToList();
                        var productKvIds = channelMappings.Select(x => x.ProductKvId).ToList();
                        if (productKvIds.Any())
                        {
                            var stageSyncToChannel = new LogStage("SyncToChannel");
                            if (channelProd.OmniChannelSchedules?.Any(x => x.Type == (byte)ScheduleType.SyncOnhand) == true)
                            {
                                await _onHandBusiness.SyncMultiOnHand(data.ConnectStringName, channelProd, null, productKvIds, false, data.LogId, null,  true);
                            }
                            stageSyncToChannel.EndStage();
                            mappingLog.LogStages.Add(stageSyncToChannel);
                        }
                    }
                }


                #region Logs
                var mappingChannelIds = productMappingDeleted.Select(x => x.ChannelId).Distinct();
                var mappingChannels = channels.Where(x => mappingChannelIds.Contains(x.Id)).OrderBy(x => x.Type).ThenByDescending(x => x.CreatedDate);

                var logContent = new StringBuilder();
                logContent.Append("Hủy bỏ liên kết hàng hóa: <br/>");
                foreach (var channel in mappingChannels)
                {
                    var productMappingAuditTrail = new ProductMappingAuditTrail
                    {
                        ChannelType = channel.Type,
                        ChannelName = channel.Name,
                        Items = new List<ProductMappingItemAuditTrail>()
                    };

                    var listMapping = productMappingDeleted.Where(x => x.ChannelId == channel.Id);
                    foreach (var mapping in listMapping)
                    {
                        productMappingAuditTrail.Items.Add(new ProductMappingItemAuditTrail { ProductKvSku = mapping.ProductKvSku, ParentProductChannelId = mapping.CommonParentProductChannelId, ProductChannelId = mapping.CommonProductChannelId, ProductChannelSku = mapping.ProductChannelSku });
                    }

                    var subLogContent = AuditTrailHelper.GetMappingProductContent(productMappingAuditTrail, true, false);
                    logContent.Append(subLogContent);
                }
                var branchIds = mappingChannels.Where(x => x.BranchId > 0).Select(x => x.BranchId).Distinct().ToList();
                var log = new AuditTrailLog
                {
                    FunctionId = (int)FunctionType.MappingSalesChannel,
                    Action = (int)AuditTrailAction.Reject,
                    CreatedDate = DateTime.Now,
                    BranchId = branchIds.Count() == 1 ? branchIds.First() : context.BranchId,
                    Content = logContent.ToString()
                };

                var coreContext = context.ConvertTo<KvInternalContext>();
                coreContext.UserId = context.User?.Id ?? 0;
                await _auditTrailInternalClient.AddLogAsync(coreContext, log);
                #endregion

                mappingLog.Description = "Remove mappings successful";
                mappingLog.LogInfo();
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                // Xóa mapping trước khi thực hiện đồng bộ (chưa xóa xong sẽ chờ)
                if (data?.ChannelIds?.Any() == true && data?.KvProductIds?.Any() == true)
                {
                    foreach (var channelId in data.ChannelIds)
                    {
                        _mappingBusiness.RemoveCacheWaitingDeleteMapping(channelId, data.KvProductIds);
                    }
                }
            }
            
        }

        private void UpdateProductFlagRelate(ExecutionContext context, Guid logId, List<long> kvProductIds, bool isRelate)
        {
            using (var mq = _messageFactory.CreateMessageProducer())
            {
                var message = new UpdateProductFlagRelateMessage
                {
                    ExecutionContext = context,
                    ProductIds = kvProductIds,
                    IsRelate = isRelate,
                    LogId = logId
                };
                mq.Publish(message);
            }
        }

        private async Task<List<Domain.Model.OmniChannel>> GetChannelsByRetailer(int retailerId)
        {
            var channels = _cacheClient.Get<List<Domain.Model.OmniChannel>>(string.Format(KvConstant.ChannelsByRetailerKey, retailerId));
            if (channels == null)
            {
                using (var db = _dbConnectionFactory.OpenDbConnection())
                {
                    var kvChannelRepository = new OmniChannelRepository(db);
                    channels = await kvChannelRepository.GetByRetailerAsync(retailerId, type: null, isIncludeInactive: false);
                }

                if (channels != null && channels.Any())
                {
                    var expiredIn = TimeSpan.FromDays(_settings.Get<int?>("CacheChannelByRetailer") ?? 30);
                    _cacheClient.Set(string.Format(KvConstant.ChannelsByRetailerKey, retailerId), channels, expiredIn);
                }
            }
            return channels;
        }

        private async Task CreateNewMappingAfterDeleted(ProductMapping oldMapping, int branchId, string connectStringName, Guid logId, List<ProductMapping> productMappings)
        {
            var listNewMapping = new List<ProductMappingPre>();
            if (!string.IsNullOrEmpty(oldMapping.ProductKvSku))
            {
                using (var db = _dbConnectionFactory.Open(connectStringName.ToLower()))
                {
                    var productRepo = new ProductRepository(db);
                    var channelProduct = (await _productMongoService.GetProductNotConnnectBySku(oldMapping.RetailerId, new List<long> { oldMapping.ChannelId }, oldMapping.ProductKvSku)).FirstOrDefault();
                    var kvProduct = (await productRepo.GetProductByCode(oldMapping.RetailerId, branchId, new HashSet<string> { oldMapping.ProductKvSku })).FirstOrDefault();
                    if (channelProduct != null && kvProduct != null)
                    {
                        var map = new ProductMappingPre
                        {
                            ProductChannelObjectIdKey = channelProduct.Id,
                            ProductKvId = oldMapping.ProductKvId,
                            ProductKvSku = oldMapping.ProductKvSku,
                            ProductKvFullName = oldMapping.ProductKvFullName,
                            RetailerId = oldMapping.RetailerId,
                            ChannelId = oldMapping.ChannelId,
                            ProductChannelId = channelProduct.CommonItemId,
                            ProductChannelSku = channelProduct.ItemSku,
                            ProductChannelType = channelProduct.Type,
                            ParentProductChannelId = channelProduct.CommonParentItemId,
                            ProductChannelName = channelProduct.ItemName,
                            ChannelProductKey = channelProduct.Id,
                        };
                        listNewMapping.Add(map);
                    }
                }
            }

            if (!string.IsNullOrEmpty(oldMapping.ProductChannelSku))
            {
                if (string.IsNullOrEmpty(connectStringName))
                {
                    return;
                }
                using (var db = _dbConnectionFactory.Open(connectStringName.ToLower()))
                {
                    var productRepo = new ProductRepository(db);
                    var kvProducts = productRepo.Where(x => x.RetailerId == oldMapping.RetailerId && x.Code == oldMapping.ProductChannelSku && x.IsActive && (x.isDeleted == null || x.isDeleted == false));
                    var kvProduct = kvProducts.FirstOrDefault(x => x.Code == oldMapping.ProductChannelSku);
                    var channel = await _channelBusiness.GetChannel(oldMapping.ChannelId, oldMapping.RetailerId, logId);
                    var channelProduct = await _productMongoService.GetProductById(oldMapping.RetailerId, oldMapping.ChannelId, oldMapping.CommonProductChannelId, isStringItemId: ConvertHelper.CheckUseStringItemId(channel?.Type));
                    if (channelProduct == null) return;
                    var existMapping = await _productMappingService.GetByKvSingleProductId(oldMapping.RetailerId, oldMapping.ChannelId, kvProduct?.Id ?? 0);
                    if (kvProduct != null && existMapping == null)
                    {
                        var map = new ProductMappingPre
                        {
                            ProductChannelObjectIdKey = channelProduct.Id,
                            ProductKvId = kvProduct.Id,
                            ProductKvSku = kvProduct.Code,
                            ProductKvFullName = kvProduct.FullName,
                            RetailerId = oldMapping.RetailerId,
                            ChannelId = oldMapping.ChannelId,
                            ProductChannelId = channelProduct.CommonItemId,
                            ProductChannelSku = channelProduct.ItemSku,
                            ProductChannelType = channelProduct.Type,
                            ParentProductChannelId = channelProduct.CommonParentItemId,
                            ProductChannelName = channelProduct.ItemName,
                            ChannelProductKey = channelProduct.Id,
                        };
                        listNewMapping.Add(map);
                    }
                }
            }

            using (var mq = _messageFactory.CreateMessageProducer())
            {
                var channelIds = listNewMapping.GroupBy(x => x.ChannelId).Select(x => x.Key);
                foreach (var channelId in channelIds)
                {
                    var mappings = listNewMapping.Where(x => x.ChannelId == channelId).ToList();
                    var productChannelIdMapping = productMappings.Where(pm => pm.ChannelId == channelId)
                        .Select(pm => pm.CommonProductChannelId).ToList();
                    var newMappings = mappings.Where(x =>
                        !productChannelIdMapping.Contains(x.ProductChannelId)).ToList();
                    if (!mappings.Any()) break;
                    var configMappingV2Feature = _settings?.Get<MappingV2Feature>("MappingV2Feature");
                    var coreContext = await ContextHelper.GetExecutionContext(_cacheClient, _dbConnectionFactory, oldMapping.RetailerId, connectStringName, branchId, _settings?.Get<int>("ExecutionContext"));
                    if (configMappingV2Feature != null &&
                        configMappingV2Feature.IsValid(coreContext.Group?.Id ?? 0, oldMapping.RetailerId))
                    {
                        var mappingRequest = new MappingRequest
                        {
                            ConnectStringName = connectStringName,
                            RetailerId = oldMapping.RetailerId,
                            BranchId = branchId,
                            ChannelId = channelId,
                            ProductMappings = newMappings,
                            IsAddNew = true,
                            LogId = logId,
                            MessageFrom = "CreateNewMappingAfterDeleted"
                        };
                        var kafkaMessage = new KafkaMessage(configMappingV2Feature.TopicMappingRequestName,
                            $"{mappingRequest.RetailerId}_{mappingRequest.BranchId}_{mappingRequest.ChannelId}", mappingRequest);
                        KafkaClient.KafkaClient.Instance.PublishMessage(kafkaMessage.TopicName, kafkaMessage.Key, JsonConvert.SerializeObject(kafkaMessage), isV2: true);
                    }
                    else
                    {
                        var addMappingMessage = new CreateProductMappingMessage
                        {
                            ConnectStringName = connectStringName,
                            RetailerId = oldMapping.RetailerId,
                            BranchId = branchId,
                            ChannelId = channelId,
                            ProductMappings = newMappings,
                            IsAddNew = true,
                            LogId = logId,
                            MessageFrom = "CreateNewMappingAfterDeleted"
                        };
                        mq.Publish(addMappingMessage);
                    }
                }
            }
        }
    }
}
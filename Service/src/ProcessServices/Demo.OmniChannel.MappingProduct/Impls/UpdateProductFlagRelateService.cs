﻿using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannelCore.Api.Sdk.Common;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.Messaging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.MappingProduct.Impls
{
    public class UpdateProductFlagRelateService : BaseService<UpdateProductFlagRelateService>
    {
        private readonly IProductInternalClient _productInternalClient;

        public UpdateProductFlagRelateService(IAppSettings settings, 
            IProductInternalClient productInternalClient,
            IMessageFactory messageFactory)
        {
            _settings = settings;
            _productInternalClient = productInternalClient;
            _messageFactory = messageFactory;
        }
        public async Task ProcessUpdateProductFlagRelate(UpdateProductFlagRelateMessage data)
        {
            var log = new Infrastructure.Auditing.LogObject(Logger, data.LogId)
            {
                Action = "UpdateProductFlagRelate",
                RetailerId = data.ExecutionContext.RetailerId,
                BranchId = data.ExecutionContext.BranchId,
                RequestObject = data
            };
            if (data.ProductIds?.Any() != true) return;

            try
            {
                var coreContext = data.ExecutionContext.ConvertTo<KvInternalContext>();
                coreContext.UserId = data.ExecutionContext.User?.Id ?? 0;

                await _productInternalClient.UpdateIsRelateToOmniChannel(coreContext, data.ProductIds, data.IsRelate);

                log.LogInfo();
            }
            catch (Exception ex)
            {
                log.Description = $"MappingProduct RetailerId:{data.ExecutionContext.RetailerId} Update IsRelateToOmniChannel: {ex.Message}";
                log.LogError(ex);
                if (data.ReTryAttempt >= 3) return;

                using (var mq = _messageFactory.CreateMessageProducer())
                {
                    data.ReTryAttempt += 1;
                    mq.Publish(data);
                }
            }
        }
    }
}

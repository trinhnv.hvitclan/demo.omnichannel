﻿using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Microsoft.Extensions.Hosting;
using ServiceStack.Configuration;
using ServiceStack.Logging;
using ServiceStack.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Demo.OmniChannel.MappingProduct.Impls
{
    public abstract class BaseService<T> : IHostedService where T : class
    {
        protected IAppSettings _settings;
        private readonly List<Task> _listTask = new List<Task>();
        private readonly CancellationTokenSource _stoppingCts = new CancellationTokenSource();
        protected ILog Logger => LogManager.GetLogger(typeof(BaseService<T>));
        protected IMessageFactory _messageFactory;

        protected BaseService()
        {
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
        
        public async Task StopAsync(CancellationToken cancellationToken)
        {
            Logger.Info("Consummer is going down");
            if (!_listTask.Any())
            {
                return;
            }
            try
            {
                _stoppingCts.Cancel();
            }
            finally
            {
                await Task.WhenAll(_listTask);
            }

        }
        
        protected void PushRedisMessageQueue(ScheduleDto item, string kvEntities, Guid logId, Domain.Model.OmniChannel channel)
        {
            item.LastSync = null;
            switch (item.ChannelType)
            {
                case (byte)ChannelType.Lazada:
                    {
                        using var mq = _messageFactory.CreateMessageProducer();
                        mq.Publish(new LazadaSyncOrderMessage
                        {
                            ChannelId = item.ChannelId,
                            ChannelType = item.ChannelType,
                            RetailerId = item.RetailerId,
                            BranchId = item.ChannelBranch,
                            KvEntities = kvEntities,
                            ScheduleDto = item,
                            BasePriceBookId = item.BasePriceBookId,
                            SyncOrderFormula = item.SyncOrderFormula ?? 30,
                            IsFirstSync = true,
                            LogId = logId,
                            PlatformId = channel.PlatformId
                        });

                        break;
                    }

                case (byte)ChannelType.Tiki:
                    {
                        using var mq = _messageFactory.CreateMessageProducer();
                        mq.Publish(new TikiSyncOrderMessage
                        {
                            ChannelId = item.ChannelId,
                            ChannelType = item.ChannelType,
                            RetailerId = item.RetailerId,
                            BranchId = item.ChannelBranch,
                            KvEntities = kvEntities,
                            ScheduleDto = item,
                            BasePriceBookId = item.BasePriceBookId,
                            SyncOrderFormula = item.SyncOrderFormula ?? 30,
                            IsFirstSync = true,
                            LogId = logId,
                            PlatformId = channel.PlatformId
                        });

                        break;
                    }
                case (byte)ChannelType.Sendo:
                {
                    using var mq = _messageFactory.CreateMessageProducer();
                    mq.Publish(new SendoSyncOrderMessage
                    {
                        ChannelId = item.ChannelId,
                        ChannelType = item.ChannelType,
                        RetailerId = item.RetailerId,
                        BranchId = item.ChannelBranch,
                        KvEntities = kvEntities,
                        ScheduleDto = item,
                        BasePriceBookId = item.BasePriceBookId,
                        SyncOrderFormula = item.SyncOrderFormula ?? 30,
                        IsFirstSync = true,
                        LogId = logId,
                        PlatformId = channel.PlatformId
                    });

                    break;
                }
            }
        }
    }
}
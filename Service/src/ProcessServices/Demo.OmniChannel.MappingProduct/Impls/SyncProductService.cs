﻿using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Newtonsoft.Json;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChannelType = Demo.OmniChannel.ShareKernel.Common.ChannelType;
using ScheduleType = Demo.OmniChannel.ShareKernel.Common.ScheduleType;

namespace Demo.OmniChannel.MappingProduct.Impls
{
    public class SyncProductService : BaseService<SyncProductMessage>
    {
        private readonly IProductMongoService _productMongoService;
        private readonly IProductMappingService _productMappingService;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private ChannelClient.Impls.ChannelClient _channelClient;
        private readonly IScheduleService _scheduleService;
        private readonly ICacheClient _cacheClient;
        private readonly IOmniChannelAuthService _channelAuthService;
        private readonly IOnHandBusiness _onHandBusiness;
        private readonly IPriceBusiness _priceBusiness;
        private readonly IOmniChannelPlatformService _omniChannelPlatformService;

        public SyncProductService(IProductMongoService productMongoService,
            IProductMappingService productMappingService,
            IDbConnectionFactory dbConnectionFactory,
            ChannelClient.Impls.ChannelClient channelClient,
            IAppSettings settings,
            IScheduleService scheduleService,
            ICacheClient cacheClient,
            IOmniChannelAuthService channelAuthService,
            IOnHandBusiness onHandBusiness,
            IPriceBusiness priceBusiness,
            IMessageFactory messageFactory,
            IOmniChannelPlatformService omniChannelPlatformService) : base()
        {
            _channelClient = channelClient;
            _productMongoService = productMongoService;
            _productMappingService = productMappingService;
            _dbConnectionFactory = dbConnectionFactory;
            _settings = settings;
            _scheduleService = scheduleService;
            _cacheClient = cacheClient;
            _channelAuthService = channelAuthService;
            _onHandBusiness = onHandBusiness;
            _priceBusiness = priceBusiness;
            _messageFactory = messageFactory;
            _omniChannelPlatformService = omniChannelPlatformService;
        }

        public async Task ProcessSyncProduct(SyncProductMessage data)
        {
            string logInfo = string.Empty;
            var syncProductLog = new LogObject(Logger, data.LogId)
            {
                Action = "SyncProduct",
                RequestObject = data,
                RetailerId = data.RetailerId,
                BranchId = data.BranchId,
                OmniChannelId = data.ChannelId
            };
            if ((data.SendTime != null && data.SendTime.Value.AddMinutes(30) < DateTime.Now) || data.RetryCount > 5)
            {
                syncProductLog.Description = $"Message queue date invalid with retry count {data.RetryCount}";
                syncProductLog.LogInfo();
                return;
            }
            var cacheKey = string.Format(KvConstant.IsRunningGetProductKey, data.ChannelId);
            try
            {
                using (var connection = _dbConnectionFactory.OpenDbConnection())
                {
                    var channelRepository = new OmniChannelRepository(connection);
                    var channel = await channelRepository.GetByIdAsync(data.ChannelId, true);
                    if (channel == null)
                    {
                        syncProductLog.ResponseObject = $"ChannelId {data.ChannelId} not found";
                        syncProductLog.LogInfo();
                        return;
                    }
                    var platform = await _omniChannelPlatformService.GetById(channel.PlatformId);
                    if (data.IsUpdateChannel)
                    {
                        var result = await _productMongoService.UpdateBranch(data.RetailerId, data.ChannelId, data.BranchId);
                        logInfo += $"Updated {result.ModifiedCount} products";

                        var connectStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1);

                        var mappingProducts = await _productMappingService.GetByChannelId(data.ChannelId);
                        var channelProductIds = mappingProducts.Select(p => p.CommonProductChannelId).ToList(); 
                        
                        if (channel.OmniChannelSchedules?.Any(x => x.Type == (byte)ScheduleType.SyncOnhand) == true)
                        {
                            await _onHandBusiness.SyncMultiOnHand(connectStringName, channel, channelProductIds, null, false, data.LogId, null, data.IsIgnoreAuditTrail);
                        }

                        if (channel.OmniChannelSchedules?.Any(x => x.Type == (byte)ScheduleType.SyncPrice) == true)
                        {
                            await _priceBusiness.SyncMultiPrice(connectStringName, channel, channelProductIds, null, false, data.LogId, data.IsIgnoreAuditTrail, 50, null, true, "Queue created from SyncProductService");
                        }
                    }
                    else
                    {
                        var connectStringName = data.KvEntities.Substring(data.KvEntities.IndexOf('=') + 1).ToLower();
                        var context = await ContextHelper.GetExecutionContext(_cacheClient, _dbConnectionFactory, data.RetailerId, connectStringName, data.BranchId, _settings?.Get<int>("ExecutionContext"));
                        var auth = await _channelAuthService.GetChannelAuth(channel, data.LogId);
                        if (auth == null) throw new KvException($"Channel {data.ChannelId} - Unauthorized: Access is denied due to invalid credentials");
                        List<MongoDb.Product> products = new List<MongoDb.Product>();
                        _channelClient = new ChannelClient.Impls.ChannelClient(_settings);
                        var client = _channelClient.CreateClient(data.ChannelType, context.GroupId, context.RetailerId);
                        var response = await client.GetListProduct(
                            new ChannelAuth { ShopId = auth.ShopId, AccessToken = auth.AccessToken }, data.ChannelId,
                            data.LogId, true, null, context.ConvertTo<KvInternalContext>(), platform);
                        response.Data.ForEach(x =>
                        {
                            products.Add(new MongoDb.Product
                            {
                                BranchId = data.BranchId,
                                CommonItemId = x.ItemId,
                                CommonParentItemId = x.ParentItemId,
                                ItemSku = x.ItemSku.Trim(),
                                ItemName = x.ItemName,
                                ItemImages = x.ItemImages,
                                RetailerId = data.RetailerId,
                                RetailerCode = data.RetailerCode,
                                ChannelId = data.ChannelId,
                                Type = x.Type,
                                ChannelType = data.ChannelType,
                                Status = x.Status,
                                SuperId = x.SuperId,
                            });
                        });

                        await _productMongoService.RemoveByChannelId(data.RetailerId, data.ChannelId);
                        await _productMappingService.RemoveByChannelId(data.ChannelId);

                        var pageSize = _settings?.Get<int>("mappingPageSize") ?? 100;
                        var total = products.Count / pageSize;
                        total = products.Count % pageSize == 0 ? total : total + 1;
                        for (int i = 0; i < total; i++)
                        {
                            var currentPage = i * pageSize;
                            await _productMongoService.BatchAddAsync(products.Skip(currentPage).Take(pageSize).ToList());
                        }

                        logInfo += $"Add new {response.Total} products";

                        var scheduleRepository = new OmniChannelScheduleRepository(connection);

                        if (channel.IsAutoMappingProduct)
                        {
                            var configMappingV2Feature = _settings?.Get<ChannelClient.Common.MappingV2Feature>("MappingV2Feature");
                            if (configMappingV2Feature != null && configMappingV2Feature.IsValid(context.Group?.Id ?? 0, data.RetailerId))
                            {
                                var mappingRequest = new MappingRequest
                                {

                                    ChannelId = data.ChannelId,
                                    ChannelType = data.ChannelType,
                                    RetailerId = data.RetailerId,
                                    BranchId = data.BranchId,
                                    RetailerCode = data.RetailerCode,
                                    KvEntities = data.KvEntities,
                                    IsIgnoreAuditTrail = data.IsIgnoreAuditTrail,
                                    IsFirstMapping = true,
                                    LogId = data.LogId
                                };
                                var kafkaMessage = new KafkaMessage(configMappingV2Feature.TopicMappingRequestName,
                                    $"{mappingRequest.RetailerId}_{mappingRequest.BranchId}_{mappingRequest.ChannelId}", mappingRequest);
                                KafkaClient.KafkaClient.Instance.PublishMessage(kafkaMessage.TopicName, kafkaMessage.Key, JsonConvert.SerializeObject(kafkaMessage), isV2: true);
                            }
                            else
                            {
                                using (var mq = _messageFactory.CreateMessageProducer())
                                {
                                    var mappingProductMessage = new MappingProductMessage
                                    {
                                        ChannelId = data.ChannelId,
                                        ChannelType = data.ChannelType,
                                        RetailerId = data.RetailerId,
                                        BranchId = data.BranchId,
                                        RetailerCode = data.RetailerCode,
                                        KvEntities = data.KvEntities,
                                        IsIgnoreAuditTrail = data.IsIgnoreAuditTrail,
                                        IsFirstMapping = true,
                                        LogId = data.LogId
                                    };
                                    mq.Publish(mappingProductMessage);
                                }
                            }
                        }

                        //Nếu khách bật đồng bộ đơn sẽ đồng bộ đơn luôn. Không cần đợi schedule
                        var scheduleOrder = await scheduleRepository.GetSchedule(channel.Id, (int)ScheduleType.SyncOrder);
                        if (scheduleOrder != null)
                        {
                            PushRedisMessageQueue(scheduleOrder, connectStringName, data.LogId, channel);
                        }

                        DateTime? lastSync = null;
                        if (response.Total != 0)
                        {
                            lastSync = DateTime.Now.AddHours(-12);
                        }

                        var schedule = await scheduleRepository.ResetExecutePlans(channel.Id, (byte)ScheduleType.SyncModifiedProduct, lastSync);

                        var scheduleDto = new ScheduleDto
                        {
                            ChannelId = channel.Id,
                            ChannelType = channel.Type,
                            BasePriceBookId = channel.BasePriceBookId.GetValueOrDefault(),
                            ChannelBranch = channel.BranchId,
                            IsRunning = false,
                            RetailerId = channel.RetailerId,
                            LastSync = DateTime.Now.AddHours(-12),
                            NextRun = DateTime.Now.AddMinutes(2),
                            Type = (byte)ScheduleType.SyncModifiedProduct
                        };
                        if (schedule != null)
                        {
                            scheduleDto.Id = schedule.Id;
                            scheduleDto.LastSync = schedule.LastSync;
                        }
                        var scheduleService = new ScheduleHelper(_scheduleService);
                        scheduleService.ReleaseSchedule(scheduleDto);

                    }
                }
                syncProductLog.ResponseObject = logInfo;
                syncProductLog.LogInfo();
                _cacheClient.Remove(cacheKey);
            }
            catch (Exception e)
            {
                using (var mq = _messageFactory.CreateMessageProducer())
                {
                    data.RetryCount ++;
                    mq.Publish(data);
                }
                syncProductLog.LogError(e);
                throw;
            }
        }
    }
}
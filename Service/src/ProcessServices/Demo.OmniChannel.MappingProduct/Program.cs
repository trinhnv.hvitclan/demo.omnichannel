﻿using System;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.WindowsServices;
using Serilog;

namespace Demo.OmniChannel.MappingProduct
{
    class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();
            try
            {
                if (Debugger.IsAttached || args.Contains("--console"))
                {
                    host.Run();
                }
                else
                {
                    host.RunAsService();
                }
            }
            catch(Exception ex)
            {
                Log.Fatal("Service run error:" + ex.StackTrace);
                host.Run();
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseUrls(Environment.GetEnvironmentVariable("ASPNETCORE_URLS") ?? "http://localhost:5001/")
                .UseStartup<Startup>();
    }
}

﻿using System;
using Demo.OmniChannel.Infrastructure.Logging;
using Demo.OmniChannelCore.Api.Sdk.Impls;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.MappingProduct.Impls;
using Demo.OmniChannel.MongoService.Common;
using Demo.OmniChannel.Utilities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using ServiceStack.Redis;
using System.Threading.Tasks;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using ServiceStack.Messaging.Redis;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.Services.Impls;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.Business;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ShareKernel.Exceptions;
using ServiceStack.Caching;
using Demo.OmniChannel.Infrastructure.CronJob;
using Demo.OmniChannel.Infrastructure.IoC;
using Demo.OmniChannel.KafkaClient;
using Demo.OmniChannel.MappingProduct.Jobs;
using Demo.OmniChannel.DCControl.ConfigureService;
using Demo.OmniChannel.Infrastructure.EventBus.Extentions;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.MongoService.Impls;
using Demo.OmniChannel.MongoService.Interface;

namespace Demo.OmniChannel.MappingProduct
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            var builder = new ConfigurationBuilder()
                .AddConfiguration(configuration)
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        public void ConfigureServices(IServiceCollection services)
        {
            #region Register ServiceStack License
            Licensing.RegisterLicense(Configuration.GetSection("servicestack:license").Value);
            #endregion register
            services.AddSingleton(c => new NetCoreAppSettings(Configuration));

            #region Register kafka
            var kafka = new Kafka();
            Configuration.GetSection("Kafka").Bind(kafka);
            KafkaClient.KafkaClient.Instance.SetKafkaConsumerConfig(kafka);
            KafkaClient.KafkaClient.Instance.SetKafkaProducerConfig(kafka);
            KafkaClient.KafkaClient.Instance.InitProducer();
            var kafka2 = new Kafka();
            Configuration.GetSection("Kafka2").Bind(kafka2);
            KafkaClient.KafkaClient.Instance.SetKafkaProducerConfig(kafka2, true);
            KafkaClient.KafkaClient.Instance.InitProducer(true);
            MissingMessageSingleton.Instance.LoopDequeue();
            #endregion

            #region Redis

            var redisConfig = new KvRedisConfig();
            Configuration.GetSection("redis:cache").Bind(redisConfig);
            services.AddSingleton(sc => KvRedisPoolManager.GetClientsManager(redisConfig));            
            services.AddSingleton<ICacheClient, KvCacheClient>();
            services.AddSingleton<IKvLockRedis, KvLockRedis>();
            
            var redisMqConfig = new KvRedisConfig();
            Configuration.GetSection("redis:message").Bind(redisMqConfig);
            var redisFactory = KvRedisPoolManager.GetClientsManager(redisMqConfig);
            var mqHost = new RedisMqServer(redisFactory, retryCount: 2);
            services.AddSingleton<IMessageService>(mqHost);
            services.AddSingleton<IMessageFactory>(c => new RedisMessageFactory(redisFactory));
            #endregion

            #region Register DatabaseConnectionString
            var kvSql = new KvSqlConnectString();
            Configuration.GetSection("SqlConnectStrings").Bind(kvSql);
            var connectFactory = new OrmLiteConnectionFactory(kvSql.KvChannelEntities, SqlServer2016Dialect.Provider);
            //Register Channel Db
            //SqlServerDialect.Provider.GetStringConverter().UseUnicode = true;
            //Register Master Db
            connectFactory.RegisterConnection(nameof(kvSql.KvMasterEntities), kvSql.KvMasterEntities, SqlServer2016Dialect.Provider);
            foreach (var item in kvSql.KvEntitiesDC1)
            {
                connectFactory.RegisterConnection(item.Name.ToLower(), item.Value, SqlServer2016Dialect.Provider);
            }
            OrmLiteConfig.DialectProvider.GetStringConverter().UseUnicode = true;
            services.AddSingleton(kvSql);
            services.AddSingleton<IDbConnectionFactory>(c => connectFactory);
            #endregion

            services.AddSingleton<IAppSettings>(x => new NetCoreAppSettings(Configuration));

            #region Register Mongo
            services.Configure<MongoDbSettings>(Configuration.GetSection("mongodb"));
            services.UsingMongoDb(Configuration);
            #endregion

            #region Register Serilog
            Configuration.ConfigLog(GetType(), "Demo.ommichannel.mappingproduct is started.");
            #endregion

            #region Register Channel Third Party
            services.AddSingleton(c => new ChannelClient.Impls.ChannelClient());
            #endregion

            services.AddScoped<IAuditTrailInternalClient>(c => new AuditTrailInternalClient(c.GetService<IAppSettings>()));
            services.AddHostedService<MappingProductService>();
            services.AddHostedService<SyncProductService>();
            services.AddHostedService<SyncModifiedProductService>();
            services.AddHostedService<MappingProductService>();
            services.AddHostedService<SyncModifiedProductService>();
            services.AddHostedService<SyncProductService>();
            services.AddSingleton<IProductInternalClient, ProductInternalClient>();
            services.AddTransient<CreateProductMappingService>();
            services.AddTransient<UpdateProductFlagRelateService>();
            services.AddTransient<MappingProductService>();
            services.AddTransient<SyncModifiedProductService>();
            services.AddTransient<SyncProductService>();
            services.AddScoped<IPriceBusiness, PriceBusiness>();
            services.AddScoped<IOnHandBusiness, OnHandBusiness>();
            services.AddScoped<IChannelBusiness, ChannelBusiness>();
            services.AddScoped<IMappingBusiness, MappingBusiness>();
            services.AddCronJob<MonitorJob>(c =>
            {
                c.TimeZoneInfo = TimeZoneInfo.Local;
                c.CronExpression = Configuration.GetValue<string>("HealthCheck:Cron", "* * * * *");
            });
            services.AddScoped<IOmniChannelAuthService, OmniChannelAuthService>();
            services.AddScoped<IOmniChannelSettingService, OmniChannelSettingService>();
            services.AddScoped<IOmniChannelPlatformService, OmniChannelPlatformService>();
            services.AddScoped<IRetryRefreshTokenMongoService, RetryRefreshTokenMongoService>();
            services.AddScoped<IOmniChannelWareHouseService, OmniChannelWareHouseService>();
            services.AddConfigEventBus(Configuration);
            services.AddEventBus();

            services.AddScoped<IScheduleService>(c => new ScheduleService.Impls.ScheduleService(c.GetRequiredService<IRedisClientsManager>(), c.GetService<IAppSettings>()));
            SetEncryptPassPhrase();

            services.RegisterApplicationDcControl(new ApplicationDcInputOptions
            {
                ServiceName = "MappingProductService",
                AppSettings = new NetCoreAppSettings(Configuration)
            });
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime lifetime, IMessageService messageService)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            var settings = app.ApplicationServices.GetService<IAppSettings>();

            messageService.RegisterHandler<CreateProductMappingMessage>(m =>
            {
                var task = Task.Run(async () => {
                    var svc = app.ApplicationServices.GetService<CreateProductMappingService>();
                    m.Options = (int)MessageOption.None;
                    await svc.ProcessCreateMappingProduct(m.GetBody());
                });
                task.Wait();
                return Task.CompletedTask;
            }, (messageHandler, message, ex) => ExceptionHelper.WriteLogExceptionMq(messageHandler.MessageType, message?.Body, ex), settings.Get<int>("RedisProductMappingThread"));

            messageService.RegisterHandler<MappingProductMessage>(m =>
            {
                var task = Task.Run(async () => {
                    var svc = app.ApplicationServices.GetService<MappingProductService>();
                    m.Options = (int)MessageOption.None;
                    await svc.ProcessMappingProduct(m.GetBody());
                });
                task.Wait();
                return Task.CompletedTask;
            }, (messageHandler, message, ex) => ExceptionHelper.WriteLogExceptionMq(messageHandler.MessageType, message?.Body, ex), settings.Get<int>("MappingProductThread"));

            messageService.RegisterHandler<SyncModifiedProductMessage>(m =>
            {
                var task = Task.Run(async () => {
                    var svc = app.ApplicationServices.GetService<SyncModifiedProductService>();
                    m.Options = (int)MessageOption.None;
                    await svc.ProcessSyncModifiedProduct(m.GetBody());
                });
                task.Wait();
                return Task.CompletedTask;
            }, (messageHandler, message, ex) => ExceptionHelper.WriteLogExceptionMq(messageHandler.MessageType, message?.Body, ex), settings.Get<int>("SyncModifiedProductThread"));

            messageService.RegisterHandler<SyncProductMessage>(m =>
            {
                var task = Task.Run(async () => {
                    var svc = app.ApplicationServices.GetService<SyncProductService>();
                    m.Options = (int)MessageOption.None;
                    await svc.ProcessSyncProduct(m.GetBody());
                });
                task.Wait();
                return Task.CompletedTask;
            }, (messageHandler, message, ex) => ExceptionHelper.WriteLogExceptionMq(messageHandler.MessageType, message?.Body, ex), settings.Get<int>("SyncProductThread"));

            messageService.RegisterHandler<UpdateProductFlagRelateMessage>(m =>
            {
                var task = Task.Run(async () => {
                    var svc = app.ApplicationServices.GetService<UpdateProductFlagRelateService>();
                    m.Options = (int)MessageOption.None;
                    await svc.ProcessUpdateProductFlagRelate(m.GetBody());
                });
                task.Wait();
                return Task.CompletedTask;
            }, (messageHandler, message, ex) => ExceptionHelper.WriteLogExceptionMq(messageHandler.MessageType, message?.Body, ex), settings.Get<int>("RedisProductMappingThread"));

            messageService.RegisterHandler<RemoveProductMappingMessage>(m =>
            {
                var task = Task.Run(async () => {
                    var svc = app.ApplicationServices.GetService<CreateProductMappingService>();
                    m.Options = (int)MessageOption.None;
                    await svc.ProcessRemoveMappingProduct(m.GetBody());
                });
                task.Wait();
                return Task.CompletedTask;
            }, (messageHandler, message, ex) => ExceptionHelper.WriteLogExceptionMq(messageHandler.MessageType, message?.Body, ex), settings.Get<int>("RedisProductMappingThread"));


            lifetime.ApplicationStarted.Register(messageService.Start);
            
        }

        private void SetEncryptPassPhrase()
        {
            CryptoHelper.PassPhrase = Configuration.GetSection("EncryptPassPhrase").Value;

            if (string.IsNullOrWhiteSpace(CryptoHelper.PassPhrase))
            {
                throw new KvException("Setting EncryptPassPhrase invalid. Check setting file, please.");
            }
        }
    }
}
﻿using Demo.OmniChannel.Infrastructure.Auditing;
using Demo.OmniChannel.Infrastructure.CronJob;
using ServiceStack;
using ServiceStack.Logging;
using ServiceStack.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ShareKernel.Common;
using Microsoft.Extensions.Hosting;
using ServiceStack.Configuration;
using ServiceStack.Redis;

namespace Demo.OmniChannel.MappingProduct.Jobs
{
    public class MonitorJob : CronJobService

    {
        private ILog _logger = LogManager.GetLogger(typeof(MonitorJob));
        private readonly IMessageService _messageService;
        private readonly IApplicationLifetime _applicationLifetime;
        private readonly IAppSettings _settings;
        private readonly IRedisClientsManager _redisClientsManager;
        private readonly KvRedisConfig _redisConfig;
        public MonitorJob(IScheduleConfig<MonitorJob> config, IMessageService messageService, IApplicationLifetime applicationLifetime, IAppSettings settings) : base(config.CronExpression, config.TimeZoneInfo)
        {
            _messageService = messageService;
            _applicationLifetime = applicationLifetime;
            _settings = settings;
            _redisConfig = _settings.Get<KvRedisConfig>("redis:message");
            _redisClientsManager = KvRedisPoolManager.GetClientsManager(_redisConfig);
        }
        public override Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.Info("MonitorJob starts.");
            return base.StartAsync(cancellationToken);
        }

        public override async Task DoWork(CancellationToken cancellationToken)
        {
            var log = new LogMonitorObject(_logger, Guid.NewGuid())
            {
                Action = "HealthCheck"
            };
            long totalQueue = 0;
            try
            {
                IMessageHandlerStats statsMap = _messageService.GetStats();
                var status = _messageService.GetStatus();
                var content = "Service Name: kv-product-mapping <br/>" +
                              $"Status: {status} <br/>" +
                              $"TotalMessageProcessed: {statsMap.TotalMessagesProcessed} <br/>" +
                              $"TotalMessagesFailed: {statsMap.TotalMessagesFailed} <br/>";
                if (_redisConfig?.QueueNames != null && _redisConfig.QueueNames.Any())
                {
                    using (var client = _redisClientsManager.GetClient())
                    {
                        foreach (var queueName in _redisConfig.QueueNames)
                        {
                            var total = client.GetListCount(queueName);
                            totalQueue += total;
                            content += $"Total {queueName}: {total} <br/>";
                        }
                    }
                }
                log.TotalMessagesFailed = statsMap.TotalMessagesFailed;
                log.TotalMessagesProcessed = statsMap.TotalNormalMessagesReceived;
                log.TotalMessagesProcessed = statsMap.TotalMessagesProcessed;
                log.Description = content.ToSafeJson();
                log.Status = status;
                if (totalQueue -
                    (statsMap.TotalMessagesProcessed + statsMap.TotalMessagesFailed) >=
                    _settings.Get<int>("TotalMessageAlert"))
                {
                    var slackHelper = new SlackHelper(_settings);
                    await slackHelper.SendMessage("[OmniChannel] kv-product-mapping", content, _settings.GetString("HealthCheck:KibanaMonitorLink"), "#eb8334");
                }
                if (status.Equals(RedisMqStatus.Stopped))
                {
                    using (var mailHelper = new EmailHelper(_settings))
                    {
                        await mailHelper.SendAsync(_settings.Get<List<string>>("HealthCheck:EmailNotifiers"), null, $"[OmniChannel] kv-product-mapping is not working", content, "good");
                    }
                    _applicationLifetime.StopApplication();
                    log.Description = "Restart service after redis message queue fail over";
                }
                _logger.Info("HealthCheck");
            }
            catch (Exception e)
            {
                log.LogError(e, false);
            }
           
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.Info("MonitorJob stopping.");
            return base.StopAsync(cancellationToken);
        }
    }
}

﻿using System.Threading.Tasks;
using KiotViet.OmniChannel.Business;
using KiotViet.OmniChannel.Business.Interfaces;
using KiotViet.OmniChannel.DCControl.ConfigureService;
using KiotViet.OmniChannel.Infrastructure.Common;
using KiotViet.OmniChannel.Infrastructure.IoC;
using KiotViet.OmniChannel.Infrastructure.Logging;
using KiotViet.OmniChannel.MongoService.Common;
using KiotViet.OmniChannel.Redis;
using KiotViet.OmniChannel.Services.Impls;
using KiotViet.OmniChannel.Services.Interfaces;
using KiotViet.OmniChannel.ShareKernel.Exceptions;
using KiotViet.OmniChannel.ShareKernel.KafkaMessage;
using KiotViet.OmniChannel.ShopeeItemNameService.Impls;
using KiotViet.OmniChannel.Utilities;
using KiotViet.OmniChannelCore.Api.Sdk.Impls;
using KiotViet.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.Messaging.Redis;
using ServiceStack.OrmLite;

namespace KiotViet.OmniChannel.ShopeeItemNameService
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            var builder = new ConfigurationBuilder()
                .AddConfiguration(configuration)
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        public void ConfigureServices(IServiceCollection services)
        {
            #region Register ServiceStack License
            Licensing.RegisterLicense(Configuration.GetSection("servicestack:license").Value);
            #endregion register
            services.AddSingleton(c => new NetCoreAppSettings(Configuration));
            // services.AddSingleton(new confi(Configuration));
            #region Redis
            var redisConfig = new KvRedisConfig();
            Configuration.GetSection("redis:cache").Bind(redisConfig);
            services.AddSingleton(sc => KvRedisPoolManager.GetClientsManager(redisConfig));
            services.AddSingleton<ICacheClient, KvCacheClient>();
            services.AddSingleton<IKvLockRedis, KvLockRedis>();

            var redisMqConfig = new KvRedisConfig();
            Configuration.GetSection("redis:message").Bind(redisMqConfig);
            var redisFactory = KvRedisPoolManager.GetClientsManager(redisMqConfig);
            var mqHost = new RedisMqServer(redisFactory, retryCount: 2);
            services.AddSingleton<IMessageService>(mqHost);
            services.AddSingleton<IMessageFactory>(c => new RedisMessageFactory(redisFactory));
            #endregion

            #region Register DatabaseConnectionString
            var kvSql = new KvSqlConnectString();
            Configuration.GetSection("SqlConnectStrings").Bind(kvSql);
            var connectFactory = new OrmLiteConnectionFactory(kvSql.KvChannelEntities, SqlServer2016Dialect.Provider);
            connectFactory.RegisterConnection(nameof(kvSql.KvMasterEntities), kvSql.KvMasterEntities, SqlServer2016Dialect.Provider);
            OrmLiteConfig.DialectProvider.GetStringConverter().UseUnicode = true;
            services.AddSingleton(kvSql);
            services.AddSingleton<IDbConnectionFactory>(c => connectFactory);
            #endregion

            services.AddSingleton<IAppSettings>(x => new NetCoreAppSettings(Configuration));

            #region Register Mongo
            services.Configure<MongoDbSettings>(Configuration.GetSection("mongodb"));
            services.UsingMongoDb(Configuration);
            #endregion

            #region Register Serilog
            Configuration.ConfigLog(GetType(), "kiotviet.ommichannel.shopeeitemname is started.");
            #endregion

            #region Register Channel Third Party
            services.AddSingleton(c => new ChannelClient.Impls.ChannelClient());
            #endregion

            services.AddScoped<IAuditTrailInternalClient>(c => new AuditTrailInternalClient(c.GetService<IAppSettings>()));
            services.AddTransient<SyncShopeeItemNameService>();
            services.AddTransient<SyncTiktokItemNameService>();
            services.AddScoped<IChannelBusiness, ChannelBusiness>();
            services.AddScoped<IChannelProductBusiness, ChannelProductBusiness>();

            services.AddScoped<IOmniChannelAuthService, OmniChannelAuthService>();
            services.AddScoped<IOmniChannelSettingService, OmniChannelSettingService>();
            services.AddScoped<IOmniChannelPlatformService, OmniChannelPlatformService>();
            //services.AddCronJob<MonitorJob>(c =>
            //{
            //    c.TimeZoneInfo = TimeZoneInfo.Local;
            //    c.CronExpression = @"* * * * *";
            //});
            SetEncryptPassPhrase();

            services.RegisterApplicationDcControl(new ApplicationDcInputOptions
            {
                ServiceName = "ShopeeItemNameService",
                AppSettings = new NetCoreAppSettings(Configuration)
            });
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime lifetime, IMessageService messageService)
        {
            var settings = app.ApplicationServices.GetService<IAppSettings>();

            messageService.RegisterHandler<SyncGroupShopeeItemNameMessage>(m =>
            {
                m.Options = (int)MessageOption.None;
                var svc = app.ApplicationServices.GetService<SyncShopeeItemNameService>();
                svc.ProcessSyncGroupShopeeItemName(m.GetBody());

                return Task.CompletedTask;
            }, (messageHandler, message, ex) => ExceptionHelper.WriteLogExceptionMq(messageHandler.MessageType, message.Body, ex), NumberHelper.GetValueOrDefault(settings.Get<int>("RedisSyncGroupShopeeItemNameThread"), 10));

            messageService.RegisterHandler<SyncShopeeItemNameMessage>(m =>
            {
                m.Options = (int)MessageOption.None;
                var svc = app.ApplicationServices.GetService<SyncShopeeItemNameService>();
                var task = svc.ProcessSyncShopeeItemName(m.GetBody());
                task.Wait();

                return Task.CompletedTask;
            }, (messageHandler, message, ex) => ExceptionHelper.WriteLogExceptionMq(messageHandler.MessageType, message.Body, ex), NumberHelper.GetValueOrDefault(settings.Get<int>("RedisSyncShopeeItemNameThread"), 30));

            messageService.RegisterHandler<SyncGroupTiktokItemNameMessage>(m =>
            {
                m.Options = (int)MessageOption.None;
                var svc = app.ApplicationServices.GetService<SyncTiktokItemNameService>();
                svc.ProcessSyncGroupTiktokItemName(m.GetBody());

                return Task.CompletedTask;
            }, (messageHandler, message, ex) => ExceptionHelper.WriteLogExceptionMq(messageHandler.MessageType, message.Body, ex), NumberHelper.GetValueOrDefault(settings.Get<int>("RedisSyncGroupTiktokItemNameThread"), 0));

            messageService.RegisterHandler<SyncTiktokItemNameMessage>(m =>
            {
                m.Options = (int)MessageOption.None;
                var svc = app.ApplicationServices.GetService<SyncTiktokItemNameService>();
                var task = svc.ProcessSyncTiktokItemName(m.GetBody());
                task.Wait();

                return Task.CompletedTask;
            }, (messageHandler, message, ex) => ExceptionHelper.WriteLogExceptionMq(messageHandler.MessageType, message.Body, ex), NumberHelper.GetValueOrDefault(settings.Get<int>("RedisSyncTiktokItemNameThread"), 0));

            lifetime.ApplicationStarted.Register(messageService.Start);
        }

        private void SetEncryptPassPhrase()
        {
            CryptoHelper.PassPhrase = Configuration.GetSection("EncryptPassPhrase").Value;

            if (string.IsNullOrWhiteSpace(CryptoHelper.PassPhrase))
            {
                throw new KvException("Setting EncryptPassPhrase invalid. Check setting file, please.");
            }
        }
    }
}
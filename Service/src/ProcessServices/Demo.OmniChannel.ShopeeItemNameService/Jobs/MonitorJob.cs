﻿using System;
using System.Threading;
using System.Threading.Tasks;
using KiotViet.OmniChannel.Infrastructure.Auditing;
using KiotViet.OmniChannel.Infrastructure.CronJob;
using ServiceStack;
using ServiceStack.Logging;
using ServiceStack.Messaging;

namespace KiotViet.OmniChannel.ShopeeItemNameService.Jobs
{
    public class MonitorJob : CronJobService

    {
        private ILog _logger = LogManager.GetLogger(typeof(MonitorJob));
        private IMessageService _messageService;
        public MonitorJob(IScheduleConfig<MonitorJob> config, IMessageService messageService) : base(config.CronExpression, config.TimeZoneInfo)
        {
            _messageService = messageService;
        }
        public override Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.Info("MonitorJob starts.");
            return base.StartAsync(cancellationToken);
        }

        public override Task DoWork(CancellationToken cancellationToken)
        {
            var log = new LogObject(_logger, Guid.NewGuid())
            {
                Action = "HealthCheck"
            };
            IMessageHandlerStats statsMap = _messageService.GetStats();
            log.Description = $"{_messageService.GetStatus()} - {statsMap.TotalNormalMessagesReceived} - {statsMap.TotalMessagesProcessed}";
            _logger.Info("HealthCheck");
            return Task.CompletedTask;
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.Info("MonitorJob stopping.");
            return base.StopAsync(cancellationToken);
        }
    }
}

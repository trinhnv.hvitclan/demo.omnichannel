﻿using System;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.WindowsServices;

namespace KiotViet.OmniChannel.ShopeeItemNameService
{
    class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();
            try
            {
                if (Debugger.IsAttached || args.Contains("--console"))
                {
                    host.Run();
                }
                else
                {
                    host.RunAsService();
                }
            }
            catch
            {
                host.Run();
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseUrls(Environment.GetEnvironmentVariable("ASPNETCORE_URLS") ?? "http://localhost:5009/")
                .UseStartup<Startup>();
    }
}

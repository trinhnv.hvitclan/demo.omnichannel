﻿using KiotViet.OmniChannel.Business.Interfaces;
using KiotViet.OmniChannel.ChannelClient.Common;
using KiotViet.OmniChannel.ChannelClient.Models;
using KiotViet.OmniChannel.Domain.Model;
using KiotViet.OmniChannel.Infrastructure.Auditing;
using KiotViet.OmniChannel.MongoService.Interface;
using KiotViet.OmniChannel.Services.Interfaces;
using KiotViet.OmniChannel.ShareKernel.Common;
using KiotViet.OmniChannel.ShareKernel.Exceptions;
using KiotViet.OmniChannel.ShareKernel.KafkaMessage;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChannelProductType = KiotViet.OmniChannel.ChannelClient.Common.ChannelProductType;

namespace KiotViet.OmniChannel.ShopeeItemNameService.Impls
{
    public class SyncShopeeItemNameService
    {
        private ILog Logger => LogManager.GetLogger(typeof(SyncShopeeItemNameService));
        private readonly ChannelClient.Impls.ChannelClient _channelClient;
        private readonly IAppSettings _settings;
        private readonly IOmniChannelAuthService _channelAuthService;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly ICacheClient _cacheClient;
        private readonly IProductMongoService _productMongoService;
        private readonly IChannelBusiness _channelBusiness;
        private readonly IMessageService _messageService;
        private readonly IChannelProductBusiness _channelProductBusiness;
        private readonly IOmniChannelPlatformService _omniChannelPlatformService;

        public SyncShopeeItemNameService(ChannelClient.Impls.ChannelClient channelClient,
            IAppSettings settings,
            IOmniChannelAuthService channelAuthService,
            IDbConnectionFactory dbConnectionFactory,
            ICacheClient cacheClient,
            IProductMongoService productMongoService,
            IChannelBusiness channelBusiness, IMessageService messageService,
            IChannelProductBusiness channelProductBusiness,
            IOmniChannelPlatformService omniChannelPlatformService)
        {
            _channelClient = channelClient;
            _settings = settings;
            _channelAuthService = channelAuthService;
            _dbConnectionFactory = dbConnectionFactory;
            _cacheClient = cacheClient;
            _productMongoService = productMongoService;
            _channelBusiness = channelBusiness;
            _messageService = messageService;
            _channelProductBusiness = channelProductBusiness;
            _omniChannelPlatformService = omniChannelPlatformService;
        }

        public void ProcessSyncGroupShopeeItemName(SyncGroupShopeeItemNameMessage data)
        {
            var log = new LogObject(Logger, data.LogId)
            {
                Action = "SyncGroupShopeeItemNameMessage",
                RetailerId = data.RetailerId,
                OmniChannelId = data.ChannelId,
                RequestObject = data
            };
            var responseObjects = new List<object>();

            try
            {
                Main();
            }
            catch (Exception ex)
            {
                responseObjects.Add($"Error: {ex}");
                Logger.Error($"{nameof(KvShopeeItemNameException)} message: {ex?.Message} - Context: {data?.ToJson()}", new KvShopeeItemNameException(ex?.Message, ex));
            }
            finally
            {
                log.ResponseObject = responseObjects;
                log.LogInfo();
            }

            void Main()
            {
                var includeRetailerIds = _settings.Get<List<int>>("FilterRetailer:IncludeIds");
                if (includeRetailerIds?.Any() == true && includeRetailerIds.All(x => x != data.RetailerId))
                {
                    responseObjects.Add("Retailer id not in filter setting.");
                    return;
                }

                var excludeRetailerIds = _settings.Get<List<int>>("FilterRetailer:ExcludeIds");
                if (excludeRetailerIds?.Any() == true && excludeRetailerIds.Any(x => x == data.RetailerId))
                {
                    responseObjects.Add("Retailer id exclude by filter setting.");
                    return;
                }

                if (data?.ItemIds?.Any() != true) return;

                var group = new GroupShopeeItemName();
                group.RetailerId = data.RetailerId;
                group.ChannelId = data.ChannelId;
                group.ItemIds = data.ItemIds;
                var groupId = Guid.NewGuid().ToString();
                foreach (var itemId in data.ItemIds)
                {
                    using (var mq = _messageService.CreateMessageProducer())
                    {
                        mq.Publish(new SyncShopeeItemNameMessage
                        {
                            GroupId = groupId,
                            RetailerId = data.RetailerId,
                            ChannelId = data.ChannelId,
                            KvEntities = data.KvEntities,
                            ItemId = itemId,
                            LogId = data.LogId
                        });
                    }
                }
            }
        }

        public async Task<KvRetailer> GetRetailer(long retailerId)
        {
            var kvRetailer = _cacheClient.Get<KvRetailer>(string.Format(KvConstant.RetailerIdCacheKey, retailerId));
            if (kvRetailer == null)
            {
                using (var dbMaster = await _dbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities))
                {
                    kvRetailer = await dbMaster.SingleAsync<KvRetailer>(x => x.Id == retailerId);
                    if (kvRetailer != null)
                    {
                        _cacheClient.Set(string.Format(KvConstant.RetailerIdCacheKey, retailerId), kvRetailer, DateTime.Now.AddDays(7));
                    }
                }
            }
            return kvRetailer;
        }

        public async Task<Domain.Common.KvGroup> GetRetailerGroup(int groupId)
        {
            var kvGroup = _cacheClient.Get<Domain.Common.KvGroup>(string.Format(KvConstant.KvGroupCacheKey, groupId));
            if (kvGroup == null)
            {
                using (var dbMaster = await _dbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities))
                {
                    kvGroup = await dbMaster.SingleAsync<Domain.Common.KvGroup>(x => x.Id == groupId);
                    if (kvGroup != null)
                    {
                        _cacheClient.Set(string.Format(KvConstant.KvGroupCacheKey, groupId), kvGroup, DateTime.Now.AddDays(7));
                    }
                }
            }
            return kvGroup;
        }

        private async Task<bool> CheckOnOffConfigShopeeV2(int retailerId)
        {
            var retailer = await GetRetailer(retailerId);
            if (retailer == null) return true;
            return IsUseShopeeMigrationV2ApiFeature(retailer.GroupId, retailerId);

        }

        private bool IsUseShopeeMigrationV2ApiFeature(int groupId, long retailerId)
        {
            var useShopeeMigrationV2ApiFeature = _settings.Get<ShopeeMigrationV2ApiFeature>("ShopeeMigrationV2ApiFeature");
            if (useShopeeMigrationV2ApiFeature != null && useShopeeMigrationV2ApiFeature.Enable
                && (IsValidGroupAndRetailerId(useShopeeMigrationV2ApiFeature) || IsValidGroup(useShopeeMigrationV2ApiFeature, groupId) || IsValidRetailerId(useShopeeMigrationV2ApiFeature, retailerId)))
            {
                return true;
            }
            return false;
        }

        private bool IsValidGroupAndRetailerId(ShopeeMigrationV2ApiFeature useShopeeMigrationV2ApiFeature)
        {
            if (useShopeeMigrationV2ApiFeature != null && useShopeeMigrationV2ApiFeature.Enable
                && useShopeeMigrationV2ApiFeature.IncludeGroup.Count == 0
                && useShopeeMigrationV2ApiFeature.IncludeRetail.Count == 0)
            {
                return true;
            }
            return false;
        }

        private bool IsValidGroup(ShopeeMigrationV2ApiFeature useShopeeMigrationV2ApiFeature, int groupId)
        {
            if (useShopeeMigrationV2ApiFeature != null
                && useShopeeMigrationV2ApiFeature.Enable
                && useShopeeMigrationV2ApiFeature.IncludeGroup.Any(item => item == groupId))
            {
                return true;
            }
            return false;
        }

        private bool IsValidRetailerId(ShopeeMigrationV2ApiFeature useShopeeMigrationV2ApiFeature, long retailerId)
        {
            if (useShopeeMigrationV2ApiFeature != null
                && useShopeeMigrationV2ApiFeature.Enable
                && useShopeeMigrationV2ApiFeature.IncludeRetail.Any(item => item == retailerId))
            {
                return true;
            }
            return false;
        }

        public async Task ProcessSyncShopeeItemName(SyncShopeeItemNameMessage data)
        {
            var log = new LogObject(Logger, data.LogId)
            {
                Action = "SyncShopeeItemNameMessage",
                RetailerId = data.RetailerId,
                OmniChannelId = data.ChannelId,
                RequestObject = data
            };
            var responseObjects = new List<object>();

            try
            {
                if (await CheckOnOffConfigShopeeV2(data.RetailerId))
                {
                    return;
                }
                await Main();
            }
            catch (Exception ex)
            {
                responseObjects.Add($"Error: {ex}");
                Logger.Error($"{nameof(KvShopeeItemNameException)} message: {ex?.Message} - Context: {data?.ToJson()}", new KvShopeeItemNameException(ex?.Message, ex));
            }
            finally
            {
                log.ResponseObject = responseObjects;
                log.LogInfo();
            }

            async Task Main()
            {
                var channel = await _channelBusiness.GetChannel(data.ChannelId, data.RetailerId, data.LogId);
                if (channel == null)
                {
                    responseObjects.Add("Channel is null --> return");
                    return;
                }
                var auth = await _channelAuthService.GetChannelAuth(channel, data.LogId);
                var platform = await _omniChannelPlatformService.GetById(channel.PlatformId);
                var client = _channelClient.GetClient((byte)ChannelType.Shopee, _settings);
                ChannelClient.RequestDTO.ProductDetailResponse responseShopee = null;

                try
                {
                    responseShopee = await client.GetProductDetail(
                        new ChannelAuth { ShopId = auth.ShopId, AccessToken = auth.AccessToken }, channel.Id,
                        data.LogId, data.ItemId, null, null, platform);
                }
                catch (Exception ex)
                {
                    Logger.Error($"{nameof(KvShopeeItemNameException)} get product detail message: {ex?.Message} - Context: {data?.ToJson()}", new KvShopeeItemNameException(ex?.Message, ex));
                    responseObjects.Add($"Error: {ex}");
                }

                var detail = responseShopee?.ProductDetails?.FirstOrDefault();
                var variations = detail?.Variations?.Select(x => (Variations)x)?.ToList();

                if (detail == null || string.IsNullOrEmpty(detail.Name))
                {
                    responseObjects.Add("Detail is null or detail name is empty --> return");

                    return;
                }

                log.Description = $"detailShopeeProduct : {detail.ItemId} and Images : {detail.Images.ToSafeJson()} ";
                log.LogInfo();
                responseObjects.Add(new
                {
                    Step = "UpdateDb",
                    ItemId = detail.ItemId,
                    Name = detail.Name,
                    Variations = variations
                });

                await _productMongoService.UpdateProductName(data.RetailerId, data.ChannelId, data.ItemId, detail.Name, variations);

                //Hiện tại chỉ update ảnh cho hàng thường
                if (!detail.Variations.Any())
                {
                    var productUpdate = new ChannelClient.RequestDTO.Product
                    {
                        ItemId = detail.ItemId,
                        ItemSku = detail.Sku,
                        ItemImages = detail.Images,
                        ParentItemId = detail.ItemId.ToString(),
                        Type = (byte)ChannelProductType.Normal,
                        Status = detail.Status,
                    };

                    log.Description = $"productUpdateMongo{detail.ItemId} : {productUpdate.ItemImages.ToSafeJson()} ";
                    log.LogInfo();

                    await _productMongoService.UpdateProducts(data.RetailerId, data.ChannelId,
                        (byte)ChannelType.Shopee, new List<ChannelClient.RequestDTO.Product> { productUpdate }, false);
                }
            }
        }
    }
}
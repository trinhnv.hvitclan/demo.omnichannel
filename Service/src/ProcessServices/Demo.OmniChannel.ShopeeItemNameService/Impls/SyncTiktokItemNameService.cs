﻿using KiotViet.OmniChannel.Business.Interfaces;
using KiotViet.OmniChannel.ChannelClient.Common;
using KiotViet.OmniChannel.ChannelClient.Models;
using KiotViet.OmniChannel.ChannelClient.ResponseDTO.Shopee;
using KiotViet.OmniChannel.Domain.Model;
using KiotViet.OmniChannel.Infrastructure.Auditing;
using KiotViet.OmniChannel.MongoService.Interface;
using KiotViet.OmniChannel.Services.Interfaces;
using KiotViet.OmniChannel.ShareKernel.Common;
using KiotViet.OmniChannel.ShareKernel.Exceptions;
using KiotViet.OmniChannel.ShareKernel.KafkaMessage;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProductDetailResponse = KiotViet.OmniChannel.ChannelClient.RequestDTO.ProductDetailResponse;

namespace KiotViet.OmniChannel.ShopeeItemNameService.Impls
{
    public class SyncTiktokItemNameService
    {
        private static ILog Logger => LogManager.GetLogger(typeof(SyncTiktokItemNameService));
        private readonly ChannelClient.Impls.ChannelClient _channelClient;
        private readonly IAppSettings _settings;
        private readonly IOmniChannelAuthService _channelAuthService;
        private readonly IProductMongoService _productMongoService;
        private readonly IChannelBusiness _channelBusiness;
        private readonly IMessageService _messageService;
        private readonly IOmniChannelPlatformService _omniChannelPlatformService;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly ICacheClient _cacheClient;

        public SyncTiktokItemNameService(ChannelClient.Impls.ChannelClient channelClient,
            IAppSettings settings,
            IOmniChannelAuthService channelAuthService,
            IProductMongoService productMongoService,
            IChannelBusiness channelBusiness, IMessageService messageService,
            IOmniChannelPlatformService omniChannelPlatformService,
            IDbConnectionFactory dbConnectionFactory,
            ICacheClient cacheClient)
        {
            _channelClient = channelClient;
            _settings = settings;
            _channelAuthService = channelAuthService;
            _productMongoService = productMongoService;
            _channelBusiness = channelBusiness;
            _messageService = messageService;
            _omniChannelPlatformService = omniChannelPlatformService;
            _dbConnectionFactory = dbConnectionFactory;
            _cacheClient = cacheClient;
        }

        public void ProcessSyncGroupTiktokItemName(SyncGroupTiktokItemNameMessage data)
        {
            var log = new LogObject(Logger, data.LogId)
            {
                Action = "SyncGroupTiktokItemNameMessage",
                RetailerId = data.RetailerId,
                OmniChannelId = data.ChannelId,
                RequestObject = data
            };
            var responseObjects = new List<object>();

            try
            {
                Main();
            }
            catch (Exception ex)
            {
                responseObjects.Add($"Error: {ex}");
                Logger.Error($"{nameof(KvTiktokItemNameException)} message: {ex.Message} - Context: {data.ToJson()}", new KvTiktokItemNameException(ex.Message, ex));
            }
            finally
            {
                log.ResponseObject = responseObjects;
                log.LogInfo();
            }

            void Main()
            {

                if (data.ItemIds?.Any() != true) return;
                var groupId = Guid.NewGuid().ToString();
                foreach (var itemId in data.ItemIds)
                {
                    using var mq = _messageService.CreateMessageProducer();
                    mq.Publish(new SyncTiktokItemNameMessage
                    {
                        GroupId = groupId,
                        RetailerId = data.RetailerId,
                        ChannelId = data.ChannelId,
                        KvEntities = data.KvEntities,
                        ItemId = itemId,
                        LogId = data.LogId
                    });
                }
            }
        }

        private async Task<bool> CheckOnOffConfigTiktok(int retailerId)
        {
            var retailer = await GetRetailer(retailerId);
            if (retailer == null) return true;
            return IsUseTiktokOptimizeProductApiFeature(retailer.GroupId, retailerId);

        }

        private bool IsUseTiktokOptimizeProductApiFeature(int groupId, long retailerId)
        {
            var tiktokOptimizeProductApiFeature = _settings.Get<TiktokOptimizeProductApiFeature>("TiktokOptimizeProductApiFeature");
            if (tiktokOptimizeProductApiFeature != null && tiktokOptimizeProductApiFeature.Enable
                && (IsValidGroupAndRetailerId(tiktokOptimizeProductApiFeature) || IsValidGroup(tiktokOptimizeProductApiFeature, groupId) || IsValidRetailerId(tiktokOptimizeProductApiFeature, retailerId)))
            {
                return true;
            }
            return false;
        }

        private bool IsValidGroupAndRetailerId(TiktokOptimizeProductApiFeature tiktokOptimizeProductApiFeature)
        {
            if (tiktokOptimizeProductApiFeature != null && tiktokOptimizeProductApiFeature.Enable
                && tiktokOptimizeProductApiFeature.IncludeGroup.Count == 0
                && tiktokOptimizeProductApiFeature.IncludeRetail.Count == 0)
            {
                return true;
            }
            return false;
        }

        public async Task<KvRetailer> GetRetailer(long retailerId)
        {
            var kvRetailer = _cacheClient.Get<KvRetailer>(string.Format(KvConstant.RetailerIdCacheKey, retailerId));
            if (kvRetailer == null)
            {
                using (var dbMaster = await _dbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities))
                {
                    kvRetailer = await dbMaster.SingleAsync<KvRetailer>(x => x.Id == retailerId);
                    if (kvRetailer != null)
                    {
                        _cacheClient.Set(string.Format(KvConstant.RetailerIdCacheKey, retailerId), kvRetailer, DateTime.Now.AddDays(7));
                    }
                }
            }
            return kvRetailer;
        }

        private bool IsValidGroup(TiktokOptimizeProductApiFeature tiktokOptimizeProductApiFeature, int groupId)
        {
            if (tiktokOptimizeProductApiFeature != null
                && tiktokOptimizeProductApiFeature.Enable
                && tiktokOptimizeProductApiFeature.IncludeGroup.Any(item => item == groupId))
            {
                return true;
            }
            return false;
        }

        private bool IsValidRetailerId(TiktokOptimizeProductApiFeature tiktokOptimizeProductApiFeature, long retailerId)
        {
            if (tiktokOptimizeProductApiFeature != null
                && tiktokOptimizeProductApiFeature.Enable
                && tiktokOptimizeProductApiFeature.IncludeRetail.Any(item => item == retailerId))
            {
                return true;
            }
            return false;
        }

        public async Task ProcessSyncTiktokItemName(SyncTiktokItemNameMessage data)
        {
            var log = new LogObject(Logger, data.LogId)
            {
                Action = "SyncTiktokItemNameMessage",
                RetailerId = data.RetailerId,
                OmniChannelId = data.ChannelId,
                RequestObject = data
            };
            var responseObjects = new List<object>();

            try
            {
                if (await CheckOnOffConfigTiktok(data.RetailerId))
                {
                    return;
                }
                await Main();
            }
            catch (Exception ex)
            {
                responseObjects.Add($"Error: {ex}");
                Logger.Error($"{nameof(KvShopeeItemNameException)} message: {ex.Message} - Context: {data.ToJson()}", new KvShopeeItemNameException(ex.Message, ex));
            }
            finally
            {
                log.ResponseObject = responseObjects;
                log.LogInfo();
            }

            async Task Main()
            {
                var channel = await _channelBusiness.GetChannel(data.ChannelId, data.RetailerId, data.LogId);
                if (channel == null)
                {
                    responseObjects.Add("Channel is null --> return");
                    return;
                }

                var auth = await _channelAuthService.GetChannelAuth(channel, data.LogId);
                var platform = await _omniChannelPlatformService.GetById(channel.PlatformId);
                var client = _channelClient.GetClient((byte) ChannelType.Tiktok, _settings);
                ProductDetailResponse responseShopee = null;
                try
                {
                    responseShopee = await client.GetProductDetail(new ChannelAuth {ShopId = auth.ShopId, AccessToken = auth.AccessToken}, channel.Id,
                        data.LogId, data.ItemId, null, null, platform);
                }
                catch (Exception ex)
                {
                    Logger.Error($"{nameof(KvShopeeItemNameException)} get product detail message: {ex.Message} - Context: {data.ToJson()}", new KvShopeeItemNameException(ex.Message, ex));
                    responseObjects.Add($"Error: {ex}");
                }

                var detail = responseShopee?.ProductDetails?.FirstOrDefault();
                var variations = detail?.Variations?.Select(x => (TiktokProductDetailsSku) x).ToList();
                //Đưa về cấu trúc variations chung của shopee
                if (variations != null)
                {
                    var variationsParse = variations.Select(x => new Variations
                    {
                        VariationId = long.Parse(x.Id),
                        VariationSku = x.SellerSku,
                        Name = string.Join(",", x.SalesAttributes.Select(y => y.ValueName))
                    }).ToList();
                    if (string.IsNullOrEmpty(detail.Name))
                    {
                        responseObjects.Add("Detail is null or detail name is empty --> return");
                        return;
                    }

                    responseObjects.Add(new
                    {
                        Step = "UpdateDb",
                        detail.ItemId,
                        detail.Name,
                        Variations = variations
                    });
                    await _productMongoService.UpdateProductName(data.RetailerId, data.ChannelId, data.ItemId, detail.Name, variationsParse);
                }
            }
        }
    }
}
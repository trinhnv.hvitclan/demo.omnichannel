﻿using Demo.OmniChannel.DCControl.ConfigureService;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Infrastructure.EventBus.Extentions;
using Demo.OmniChannel.Infrastructure.IoC;
using Demo.OmniChannel.Infrastructure.Logging;
using Demo.OmniChannel.MongoService.Common;
using Demo.OmniChannel.NotificationService.Impls.RabbitMq;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ShareKernel.Common;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.Messaging.Redis;
using ServiceStack.OrmLite;


namespace Demo.OmniChannel.NotificationService
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            var builder = new ConfigurationBuilder()
                .AddConfiguration(configuration)
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            #region ServiceStack License
            Licensing.RegisterLicense(Configuration.GetSection("servicestack:license").Value);
            #endregion

            #region NetCoreAppSettings
            services.AddSingleton<IAppSettings>(x => new NetCoreAppSettings(Configuration));
            #endregion

            #region Serilog
            Configuration.ConfigLog(GetType(), "Demo.ommichannel.notificationsubscribe is started.");
            #endregion

            #region Redis
            var redisConfig = new KvRedisConfig();
            Configuration.GetSection("redis:cache").Bind(redisConfig);
            services.AddSingleton(sc => KvRedisPoolManager.GetClientsManager(redisConfig));
            services.AddSingleton<ICacheClient, KvCacheClient>();
            services.AddSingleton<IKvLockRedis, KvLockRedis>();


            var redisMqConfig = new KvRedisConfig();
            Configuration.GetSection("redis:message").Bind(redisMqConfig);
            services.AddSingleton(redisMqConfig);
            var redisFactory = KvRedisPoolManager.GetClientsManager(redisMqConfig);
            var mqHost = new RedisMqServer(redisFactory, retryCount: 2);
            services.AddSingleton<IMessageService>(mqHost);
            #endregion

            #region MSSQL
            var kvSql = new KvSqlConnectString();
            Configuration.GetSection("SqlConnectStrings").Bind(kvSql);
            var connectFactory = new OrmLiteConnectionFactory(kvSql.KvChannelEntities, SqlServer2016Dialect.Provider);
            connectFactory.RegisterConnection(nameof(kvSql.KvMasterEntities), kvSql.KvMasterEntities, SqlServer2016Dialect.Provider);
            foreach (var item in kvSql.KvEntitiesDC1)
            {
                connectFactory.RegisterConnection(item.Name.ToLower(), item.Value, SqlServer2016Dialect.Provider);
            }
            OrmLiteConfig.DialectProvider.GetStringConverter().UseUnicode = true;
            services.AddSingleton(kvSql);
            services.AddSingleton<IDbConnectionFactory>(c => connectFactory);
            #endregion

            #region RabbitMQ Consumer
            services.AddHostedService<HandlerNotificationService>();
            #endregion

            #region RabbitMQ Producer
            services.AddConfigEventBus(Configuration);
            services.AddEventBus();
            #endregion

            #region Mongodb
            services.Configure<MongoDbSettings>(Configuration.GetSection("mongodb"));
            services.UsingMongoDb(Configuration);
            #endregion
            services.RegisterApplicationDcControl(new ApplicationDcInputOptions
            {
                ServiceName = "NotificationSubscribe",
                AppSettings = new NetCoreAppSettings(Configuration)
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
        }
    }
}
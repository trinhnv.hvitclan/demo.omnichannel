﻿# runtime image
FROM docker.citigo.com.vn/kvxteam/mcr.microsoft.com-dotnet-aspnet:3.1 AS base
WORKDIR /app
EXPOSE 80

# build image
FROM docker.citigo.com.vn/kvxteam/mcr.microsoft.com-dotnet-sdk:3.1 AS build


# copy and restore nuget package
WORKDIR /
COPY Demo.OmniChannel.Api.sln .
COPY ./Api /Api
COPY ./src /src
COPY ./Components ./Components
RUN dotnet restore

# build
WORKDIR /src/ProcessServices/Demo.OmniChannel.NotificationSubscribe
RUN dotnet build Demo.OmniChannel.NotificationSubscribe.csproj -c Release -o /app

# publish
FROM build AS publish
RUN dotnet publish "Demo.OmniChannel.NotificationSubscribe.csproj" -c Release -o /app/publish

# Install tool for debugger
FROM build AS tools-install

RUN dotnet tool install --tool-path /dotnetcore-tools dotnet-sos --version 6.0.351802
RUN dotnet tool install --tool-path /dotnetcore-tools dotnet-trace --version 6.0.351802
RUN dotnet tool install --tool-path /dotnetcore-tools dotnet-dump --version 6.0.351802
RUN dotnet tool install --tool-path /dotnetcore-tools dotnet-counters --version 6.0.351802
RUN dotnet tool install --tool-path /dotnetcore-tools JetBrains.dotTrace.GlobalTools

FROM base AS final

WORKDIR /root
RUN apt-get update -y && apt-get install -y wget && \
wget -O dotMemoryclt.zip https://www.nuget.org/api/v2/package/JetBrains.dotMemory.Console.linux-x64/2022.1.2 && \
apt-get install -y unzip && \
unzip dotMemoryclt.zip -d ./dotMemoryclt && \
chmod +x -R dotMemoryclt/*

WORKDIR /app

# Setup debugger tools
RUN apt-get update \
  && apt-get upgrade -y \
  && apt-get install -y \
     file \
     lldb \
  && rm -rf /var/lib/apt/lists/*
COPY --from=tools-install /dotnetcore-tools /opt/dotnetcore-tools
COPY --from=tools-install /dotnetcore-tools /opt/dotnetcore-tools
ENV PATH="/opt/dotnetcore-tools:${PATH}"
RUN dotnet-sos install

COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Demo.OmniChannel.NotificationSubscribe.dll"]
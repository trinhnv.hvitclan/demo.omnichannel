﻿using Demo.OmniChannel.ShareKernel.Dto;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.EventBus;
using Demo.OmniChannel.Utilities.RabbitMq;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Logging;
using ServiceStack.OrmLite;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Demo.OmniChannel.NotificationService.Impls.RabbitMq
{
    public class HandlerNotificationService : IHostedService
    {
        private readonly string HandlerNotificationRoutingKey;
        protected IIntegrationEventService IntegrationEventService { get; set; }

        protected ILog Logger => LogManager.GetLogger(typeof(HandlerNotificationService));
        protected IAppSettings Settings;
        protected IRedisClientsManager RedisClientsManager;
        protected IDbConnectionFactory DbConnectionFactory;
        private IModel _channel;
        private IConnection _connection;

        public HandlerNotificationService(
            IAppSettings settings,
            IRedisClientsManager redisClientsManager,
            IDbConnectionFactory dbConnectionFactory,
            IIntegrationEventService integrationEventService
            )
        {
            Settings = settings;
            RedisClientsManager = redisClientsManager;
            DbConnectionFactory = dbConnectionFactory;
            IntegrationEventService = integrationEventService;
            HandlerNotificationRoutingKey = Settings.Get<string>("EventBus:HandlerNotificationRoutingKey");
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            InitRabbitMq();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _channel.Dispose();
            _connection.Dispose();
            return Task.CompletedTask;
        }

        private void InitRabbitMq()
        {
            try
            {
                var rabbitMqConfig = Settings.Get<RabbitMqConfig>("rabbitmq");

                var factory = new ConnectionFactory()
                {
                    HostName = rabbitMqConfig.HostName,
                    Port = rabbitMqConfig.Port,
                    UserName = rabbitMqConfig.UserName,
                    Password = rabbitMqConfig.Password,
                    VirtualHost = rabbitMqConfig.VirtualHost,
                };
                while (_connection == null)
                {
                    try
                    {
                        _connection = factory.CreateConnection();
                    }
                    catch (Exception ex)
                    {
                        Logger.Warn($"HandlerNotificationService: Error while create connection RabbitMq Consumer: {ex.Message}");
                    }
                }

                _channel = _connection.CreateModel();

                var arguments = new Dictionary<string, object>() { { "x-queue-type", "quorum" } };

                _channel.ExchangeDeclare(exchange: rabbitMqConfig.Exchange, type: rabbitMqConfig.ExchangeType, durable: true, autoDelete: false, arguments: arguments);

                _channel.QueueDeclare(queue: rabbitMqConfig.QueueName,
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments);

                _channel.QueueBind(
                    queue: rabbitMqConfig.QueueName,
                    exchange: rabbitMqConfig.Exchange,
                    routingKey: rabbitMqConfig.RoutingKey);

                var consumer = new EventingBasicConsumer(_channel);
                consumer.Received += async (model, args) =>
                {
                    try
                    {
                        await OnMessageReceivedAsync(args);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error($"HandlerNotificationService: Error while proccessing RabbitMq Consumer: {ex.Message}");
                    }
                };

                _channel.BasicConsume(queue: rabbitMqConfig.QueueName,
                                     autoAck: false,
                                     consumer: consumer);

            }
            catch (Exception ex)
            {
                Logger.Error($"Error while starting RabbitMq Consumer: {ex.Message}");
            }

        }

        private async Task OnMessageReceivedAsync(BasicDeliverEventArgs args)
        {
            try
            {
                var body = args.Body.ToArray();
                var jsonString = Encoding.UTF8.GetString(body);
                var message = JsonConvert.DeserializeObject<ChannelExpiredByRetailer>(jsonString);
                Logger.Info($"HandlerNotificationService: Received from RabbitMQ: {message.ToSafeJson()}");
                await SendNotification(message);
                Console.WriteLine(@"HandlerNotificationService: Done process success");
            }
            catch (Exception ex)
            {
                Logger.Error($"Error while processing RabbitMq Consumer: {ex.Message}");
            }
            finally
            {
                _channel.BasicAck(deliveryTag: args.DeliveryTag, multiple: false);
            }
        }

        private async Task SendNotification(ChannelExpiredByRetailer message)
        {
            #region Lấy thông tin Retailer | Group
            var retailerId = message.RetailerId;
            var retailer = await GetRetailer(retailerId);
            if (retailer == null)
            {
                return;
            }
            var kvGroup = await GetRetailerGroup(retailer.GroupId);
            if (kvGroup == null)
            {
                return;
            }
            #endregion

            #region Kiểm tra thông tin hợp đồng | Check setting xem đã gửi noti chưa
            if (message.OmniChannelExpires == null || message.OmniChannelExpires.Count <= 0)
            {
                return;
            }

            var omniPosParameter = await GetPosParametersByRetailersAsync(retailer.Id, nameof(PosSetting.OmniChannel), kvGroup);
            if (!IsValidContractOmni(omniPosParameter))
            {
                return;
            }
            #endregion

            #region Check setting đã đánh dấu đã gửi noti chưa, và lọc ra các Sàn đã đánh dấu
            var settings = await GetSettingIsSentExpirationNotification(message);
            if (settings != null && settings.Count > 0)
            {
                var existOmniChannelIds = settings.Select(x => x.OmniChannelId).ToList();
                message.OmniChannelExpires = message.OmniChannelExpires.Where(x => !existOmniChannelIds.Contains(x.ChannelId)).ToList();
            }

            if (message.OmniChannelExpires.Count <= 0)
            {
                return;
            }
            #endregion
            var isFreePremium = kvGroup.IsFreePremium ?? false;

            // Tạm thời off cho bản free
            if (isFreePremium)
            {
                return;
            }
            #region Gửi event sang hệ thống Notification của POS Online
            IntegrationEventService.AddEventWithRoutingKeyAsync(message, HandlerNotificationRoutingKey);
            #endregion

            #region Đánh dấu là đã gửi thông báo
            await MarkChannelSentNotification(message);
            #endregion
        }


        private async Task<List<OmniChannelSetting>> GetSettingIsSentExpirationNotification(ChannelExpiredByRetailer message)
        {
            if (message.OmniChannelExpires == null)
            {
                return new List<OmniChannelSetting>();
            }
            using (var db = await DbConnectionFactory.OpenAsync())
            {
                var omniChannelSettingRepository = new OmniChannelSettingRepository(db);
                var omniChannelIds = message.OmniChannelExpires.Select(x => x.ChannelId).ToList();
                var settings = await omniChannelSettingRepository.GetChannelSettings(omniChannelIds, message.RetailerId);
                if (settings == null) return new List<OmniChannelSetting>();
                return settings.Where(x => nameof(OmniChannelSettingDto.IsSentExpirationNotification).Equals(x.Name)).ToList();
            }
        }

        private async Task MarkChannelSentNotification(ChannelExpiredByRetailer message)
        {

            if (message.OmniChannelExpires == null)
            {
                return;
            }
            using (var db = await DbConnectionFactory.OpenAsync())
            {
                var omniChannelRepository = new OmniChannelRepository(db);
                var omniChannelSettingRepository = new OmniChannelSettingRepository(db);
                var omniChannelIds = message.OmniChannelExpires.Select(x => x.ChannelId).ToList();

                var channels = await omniChannelRepository.WhereAsync(x => x.RetailerId == message.RetailerId && omniChannelIds.Contains(x.Id));
                await omniChannelSettingRepository.MarkChannelSentNotificationExpired(true, message.RetailerId, channels);
            }
        }

        #region HELPER METHOD

        private async Task<KvRetailer> GetRetailer(long retailerId)
        {
            try
            {
                using var client = RedisClientsManager.GetClient();
                var kvRetailer = client.Get<KvRetailer>(string.Format(KvConstant.RetailerIdCacheKey, retailerId));
                if (kvRetailer == null)
                {
                    using var dbMaster = await DbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities);
                    using (_ = dbMaster.OpenTransaction(System.Data.IsolationLevel.ReadUncommitted))
                    {
                        kvRetailer = await dbMaster.SingleAsync<KvRetailer>(x => x.Id == retailerId);
                        if (kvRetailer != null)
                        {
                            client.Set(string.Format(KvConstant.RetailerIdCacheKey, retailerId), kvRetailer, DateTime.Now.AddDays(7));
                        }
                    }
                }
                return kvRetailer;
            }
            catch (Exception ex)
            {
                Logger.Error($"Error while HandlerNotificationService: {ex.Message}");
                return new KvRetailer();
            }
        }

        private async Task<Domain.Common.KvGroup> GetRetailerGroup(int groupId)
        {
            using var client = RedisClientsManager.GetClient();
            var kvGroup = client.Get<Domain.Common.KvGroup>(string.Format(KvConstant.KvGroupCacheKey, groupId));
            if (kvGroup == null)
            {
                using var dbMaster = await DbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities);
                using (_ = dbMaster.OpenTransaction(System.Data.IsolationLevel.ReadUncommitted))
                {
                    kvGroup = await dbMaster.SingleAsync<Domain.Common.KvGroup>(x => x.Id == groupId);
                    if (kvGroup != null)
                    {
                        client.Set(string.Format(KvConstant.KvGroupCacheKey, groupId), kvGroup, DateTime.Now.AddDays(7));
                    }
                }
            }
            return kvGroup;
        }

        private async Task<Domain.Model.PosParameter> GetPosParametersByRetailersAsync(int retailerId, string keyPosparameter, Domain.Common.KvGroup kvGroup)
        {
            var connectionString = kvGroup.ConnectionString.Substring(kvGroup.ConnectionString.IndexOf('=') + 1)?.ToLower();
            Logger.Info($"GetPosParametersByRetailersAsync {retailerId} - {connectionString}");
            using (var dbMaster = DbConnectionFactory.Open(connectionString))
            {
                var posParameters = await dbMaster
                    .SingleAsync<Demo.OmniChannel.Domain.Model.PosParameter>(t => t.RetailerId == retailerId && t.Key == keyPosparameter);
                return posParameters;
            }
        }

        private bool IsValidContractOmni(Domain.Model.PosParameter posParameter)
        {
            return posParameter != null && posParameter.isActive && posParameter.BlockUnit.GetValueOrDefault(0) > 0 && posParameter.ExpiredDate.GetValueOrDefault() >= DateTime.Now;
        }

        #endregion HELPER METHOD
    }

    public class ChannelExpiredByRetailer : IntegrationEvent
    {
        public int RetailerId { get; set; }
        public List<OmniChannelExpiredPayload> OmniChannelExpires { get; set; }
    }

    public class OmniChannelExpiredPayload
    {
        public long ChannelId { get; set; }
        public string Name { get; set; }
        public byte ChannelType { get; set; }
        public string TypeName
        {
            get
            {
                return EnumHelper.EnumTypeTo<ChannelType>(ChannelType).ToString();
            }
        }
    }
}

﻿using Funq;
using Demo.ElasticSearch.Sdk.Impls;
using Demo.ElasticSearch.Sdk.Interfaces;
using Demo.Mapping.Sdk.Impls;
using Demo.Mapping.Sdk.Interfaces;
using Demo.OmniChannel.Api.Extensions;
using Demo.OmniChannel.Api.ServiceInterface;
using Demo.OmniChannel.Business;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.CoreDemoContext.ConfigExtentions;
using Demo.OmniChannel.DCControl.ConfigureService;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Infrastructure.IoC;
using Demo.OmniChannel.Infrastructure.Logging;
using Demo.OmniChannel.Infrastructure.Securities;
using Demo.OmniChannel.KafkaClient;
using Demo.OmniChannel.MongoService.Common;
using Demo.OmniChannel.MongoService.Impls;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.Auth;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Impls;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using ServiceStack;
using ServiceStack.Auth;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using ServiceStack.Redis;
using ServiceStack.Text;

namespace Demo.OmniChannel.Api
{
    public class AppHost : AppHostBase
    {
        public AppHost()
            : base("Demo.OmmiChannel.Api", typeof(BaseApi).Assembly) { }
        private IHttpContextAccessor _httpContextAccessor;
        public override void Configure(Container container)
        {
            ConfigPlugins();
            container.Register(c => AppSettings);
            SelfAppConfigSingleton.Instance.InitAppSettings(AppSettings);
            #region Register ServiceStack License
            Licensing.RegisterLicense(AppSettings.GetString("servicestack:license"));
            #endregion register
            //Register Redis

            #region Register kafka
            var kafka = AppSettings.Get<Kafka>("Kafka");
            KafkaClient.KafkaClient.Instance.SetKafkaConsumerConfig(kafka);
            KafkaClient.KafkaClient.Instance.SetKafkaProducerConfig(kafka);
            KafkaClient.KafkaClient.Instance.InitProducer();
            var kafka2 = AppSettings.Get<Kafka>("Kafka2");
            KafkaClient.KafkaClient.Instance.SetKafkaProducerConfig(kafka2, true);
            KafkaClient.KafkaClient.Instance.InitProducer(true);
            MissingMessageSingleton.Instance.LoopDequeue();
            #endregion

            #region Register mongoRepos
            container.AddScoped<IPaymentTransactionService, PaymentTransactionService>();

            #endregion

            var redisCacheConfig = AppSettings.Get<KvRedisConfig>("redis:cache");
            if (redisCacheConfig != null)
            {
                container.AddSingleton(sc => KvRedisPoolManager.GetClientsManager(redisCacheConfig));
                container.RegisterAs<KvCacheClient, ICacheClient>();
                container.RegisterAs<KvLockRedis, IKvLockRedis>();
            }
            //Register Redis Message Queue
            var redisMqConfig = AppSettings.Get<KvRedisConfig>("redis:message");
            if (redisMqConfig != null)
            {
                container.Register<IMessageFactory>(c => new RedisMessageFactory(KvRedisPoolManager.GetClientsManager(redisMqConfig)));
            }

            //Register Db
            var kvSql = AppSettings.Get<KvSqlConnectString>("SqlConnectStrings");
            var connectFactory = new OrmLiteConnectionFactory(kvSql.KvChannelEntities, SqlServer2016Dialect.Provider);
            connectFactory.RegisterConnection(nameof(kvSql.KvMasterEntities), kvSql.KvMasterEntities, SqlServer2016Dialect.Provider);
            foreach (var item in kvSql.KvEntitiesDC1)
            {
                connectFactory.RegisterConnection(item.Name.ToLower(), item.Value, SqlServer2016Dialect.Provider);
            }
            OrmLiteConfig.DialectProvider.GetStringConverter().UseUnicode = true;
            OrmLiteConfig.CommandTimeout = 200;
            container.Register<IDbConnectionFactory>(c => connectFactory);

            //Error for handle exception API
            ServiceExceptionHandlers.Add((httpReq, request, exception) =>
            {
                if ((exception as HttpError)?.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    return DtoUtils.CreateErrorResponse(new object(), exception);
                }

                ExceptionHelper.WriteErrorApiHandler(httpReq, exception);
                ExceptionHelper.HandleExceptions(AppSettings, exception, httpReq);

                _httpContextAccessor = new HttpContextAccessor();
                var context = _httpContextAccessor.HttpContext.Request.Headers;
                context.TryGetValue("Retailer", out var retailerCode);
                //Check Notexists
                var ex = exception;
                //or return your own custom response
                return DtoUtils.CreateErrorResponse(new object(), ex);
            });

            container.Register(c => connectFactory.Open()).ReusedWithin(ReuseScope.Request);
            //Register MongoDb
            var mongo = AppSettings.Get<MongoDbSettings>("mongodb");
            container.Register(c =>
            {
                var client = new MongoClient(mongo.ConnectionString);
                return client.GetDatabase(mongo.DatabaseName);
            }).ReusedWithin(ReuseScope.Request);
            container.RegisterService();
            //Register Repositories
            container.RegisterOmmiChannelRepositories();
            //Register Services
            container.RegisterOmmiChannelServices();
            AppSettings.ConfigLog(GetType(), "Demo.ommichannel.api is started.");
            //Register Core Api
            container.AddScoped<IAuditTrailInternalClient>(c => new AuditTrailInternalClient(AppSettings));
            container.AddScoped<ISaleChannelInternalClient>(c => new SaleChannelInternalClient(AppSettings));
            container.AddScoped<IOrderInternalClient>(c => new OrderInternalClient(AppSettings));
            container.AddScoped<ICategoryInternalClient>(c => new CategoryInternalClient(AppSettings));
            container.AddScoped<IProductAttributeInternalClient>(c => new ProductAttributeInternalClient(AppSettings));
            container.AddScoped<IAttributeInternalClient>(c => new AttributeInternalClient(AppSettings));
            container.AddScoped<IProductInternalClient>(c => new ProductInternalClient(AppSettings));
            container.AddScoped<IPosSettingInternalClient>(c => new PosSettingInternalClient(AppSettings));
            container.AddScoped<IInvoiceInternalClient>(c => new InvoiceInternalClient(AppSettings));
            container.AddScoped<ICustomerInternalClient>(c => new CustomerInternalClient(AppSettings));

            container.Register<IScheduleService>(c =>
                new ScheduleService.Impls.ScheduleService(HostContext.TryResolve<IRedisClientsManager>(),
                    HostContext.TryResolve<IAppSettings>())).ReusedWithin(ReuseScope.Request);
            // Register business
            container.AddScoped<IPriceBusiness, PriceBusiness>();
            container.AddScoped<IOnHandBusiness, OnHandBusiness>();
            container.AddScoped<IChannelBusiness, ChannelBusiness>();

            container.AddScoped<IBranchInternalClient, BranchInternalClient>();
            container.AddScoped<IPriceBookInternalClient, PriceBookInternalClient>();
            container.AddScoped<IMappingInternalClient, MappingInternalClient>();
            container.AddScoped<IProductSearchClient, ProductSearchClient>();

            container.AddSingleton(c => new ChannelClient.Impls.ChannelClient(c.GetService<IAppSettings>()));
            container.AddScoped<IChannelClient>(c => new Demo.OmniChannel.ChannelClient.Impls.ChannelClient(c.GetService<IAppSettings>()));


            container.AddDICoreDemoContextConfiguration();

            container.AddDomainService();

            SetEncryptPassPhrase();


            this.RegisterDcControl(new ApiDcInputOptions
            {
                AppSettings = AppSettings,
                ServiceName = "OmniChannelApi",
                Container = container,
            });

            ConfigJsonSerializer();
        }

        private void ConfigJsonSerializer()
        {
            JsConfig.EmitCamelCaseNames = false;
            JsConfig.DateHandler = DateHandler.ISO8601;
        }

        private void SetEncryptPassPhrase()
        {
            CryptoHelper.PassPhrase = AppSettings.GetString("EncryptPassPhrase");

            if (string.IsNullOrWhiteSpace(CryptoHelper.PassPhrase))
            {
                throw new KvException("Setting EncryptPassPhrase invalid. Check setting file, please.");
            }
        }

        private void ConfigPlugins()
        {
            Plugins.Add(new CorsFeature());
            Plugins.Add(
                new AuthFeature(
                    () => new KVSession(),
                    new IAuthProvider[]
                    {
                        new JwtAuthProvider(AppSettings)
                        {
                            PopulateSessionFilter = (session, o, req) =>
                            {
                                var sess = session as KVSession;
                                if (sess == null) return;

                                sess.CurrentRetailerCode = o["kvrcode"];
                                sess.KvSessionId = o["kvses"];
                                sess.CurrentRetailerId = ConvertHelper.ToInt32(o["kvrid"]);
                                sess.GroupId = ConvertHelper.ToInt32(o["kvrgid"]);
                                sess.CurrentIndustryId  =ConvertHelper.ToInt32(o["kvrindid"]);
                                var uid = ConvertHelper.ToInt64(o["kvuid"]);
                                var uname = o["preferred_username"];
                                sess.CurrentBranchId = ConvertHelper.ToInt32(o["kvbid"]);
                                sess.CurrentLang = o["kvlang"];
                                sess.HttpMethod = req.Verb;
                                sess.CurrentUser = new SessionUser
                                {
                                    Id = uid,
                                    UserName = uname,
                                    GivenName = o["kvugvname"],
                                    RetailerId = ConvertHelper.ToInt32(o["kvurid"]),
                                    Type = ConvertHelper.ToByte(o["kvutype"]),
                                    IsLimitTime = ConvertHelper.ToBoolean(o["kvulimit"]),
                                    IsAdmin =  ConvertHelper.ToBoolean(o["kvuadmin"]),
                                    IsActive = ConvertHelper.ToBoolean(o["kvuact"]),
                                    IsLimitedByTrans = ConvertHelper.ToBoolean(o["kvulimittrans"]),
                                    IsShowSumRow = ConvertHelper.ToBoolean(o["kvushowsum"]),
                                    GroupId = ConvertHelper.ToInt32(o["kvrgid"]),
                                    KvSessionId = o["kvses"],
                                    IndustryId = ConvertHelper.ToInt32(o["kvrindid"]),
                                    Language = o["kvlang"]
                                };
                            }
                        },
                    }));

            var enableFeatures = Feature.All;
            var configEnableMeta = AppSettings.Get<bool>("EnableMetadataFeature", true);
            if (!configEnableMeta) enableFeatures = enableFeatures.Remove(
                Feature.Metadata | Feature.Soap11 | Feature.Soap12);

            SetConfig(new HostConfig
            {
                EnableFeatures = enableFeatures,
                UseCamelCase = false,
                DefaultContentType = MimeTypes.Json,
                MapExceptionToStatusCode =
                {
                    {typeof(KvValidateException), 420 },
                    {typeof(TokenException), 401},
                    {typeof(KvException), 420 },
                }
            });
        }
    }
}
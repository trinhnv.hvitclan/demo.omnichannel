﻿using System.Threading.Tasks;
using AutoMapper;
using Demo.OmniChannel.Api.Extensions;
using Demo.OmniChannel.Api.loC.NativeInjector;
using Demo.OmniChannel.Infrastructure.CrmClient.Extentions;
using Demo.OmniChannel.Infrastructure.EventBus.Extentions;
using Demo.OmniChannel.Infrastructure.Extensions;
using Demo.OmniChannel.Services.Impls;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.Services.LogginConfiguration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ServiceStack;

namespace Demo.OmniChannel.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(Startup));
            services.AddHttpContextAccessor();
            services.AddSingleton(Services.AutoMapperConfigurations.AutoMapping.RegisterMappings().CreateMapper());
            services.AddScoped<IExecutionContextService, ExecutionContextService>();
            services.AddSingleton(c => new ChannelClient.Impls.ChannelClient());
            services.AddScoped<IMicrosoftLoggingContextExtension, MicrosoftLoggingContextExtension>();
            RegisterService(services);
            services.AddCors(o => o.AddPolicy("Policy", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));
            services.AddConfigCrmClient(Configuration);
            services.AddConfigEventBus(Configuration);
            services.AddEventBus();
            services.AddMvc();
            services.AddExecutionContext(Configuration);
            services.RegisterCrmClientService();
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors("Policy");
            app.UseServiceStack(new AppHost
            {
                AppSettings = new NetCoreAppSettings(Configuration)
            });
            app.Run(context =>
            {
                if (env.IsDevelopment())
                {
                    context.Response.Redirect("/metadata");
                }
                else
                {
                    // we need return default text to fix ERR_TOO_MANY_REDIRECTS in production
                    // cause we don't public metadata in production
                    context.Response.WriteAsync("KV Omnichannel API v1");
                }
                return Task.FromResult(0);
            });
        }

        private void RegisterService(IServiceCollection services)
        {
            NativeInjectorBootStrapper.Register(services, Configuration);
        }
    }
}
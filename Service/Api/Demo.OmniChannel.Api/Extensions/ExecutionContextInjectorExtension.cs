﻿using System;
using System.Globalization;
using System.Threading;
using Demo.OmniChannel.Domain.Common;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Infrastructure.Securities;
using Demo.OmniChannel.ShareKernel.Common;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Auth;
using ServiceStack.Caching;
using ServiceStack.Data;
using ServiceStack.OrmLite;

namespace Demo.OmniChannel.Api.Extensions
{
    public static class ExecutionContextInjectorExtension
    {
        public static void AddExecutionContext(this IServiceCollection services, IConfiguration configuration)
        {

            KvInternalContext ImplementationFactory(IServiceProvider c)
            {
                var logger = HostContext.TryResolve<ILogger<Startup>>();
                try
                {
                    var request = HostContext.GetCurrentRequest();
                    var authService = HostContext.ResolveService<AuthenticateService>(request);
                    var session = authService.GetSession().ConvertTo<KVSession>();

                    //kiểm tra nếu từ service gọi sẽ có InternalApiToken. Không sẽ lấy thông tin từ bearToken
                    var isInternalApiToken = request.Headers.Get("InternalApiToken");
                    string retailerCode;
                    int branchId;
                    long userId;
                    if (!string.IsNullOrEmpty(isInternalApiToken))
                    {
                        retailerCode = request.Headers.Get("Retailer");
                        int.TryParse(request.Headers.Get("BranchId"), out branchId);
                        long.TryParse(request.Headers.Get("User"), out userId);
                    }
                    else
                    {
                        retailerCode = session.CurrentRetailerCode;
                        branchId = session.CurrentBranchId;
                        userId = session.CurrentUser?.Id ?? 0;
                    }

                    if (string.IsNullOrEmpty(retailerCode)) return new KvInternalContext();

                    var cacheClient = HostContext.TryResolve<ICacheClient>();
                    var kvRetailer = cacheClient.Get<KvRetailer>(string.Format(KvConstant.RetailerCodeCacheKey, retailerCode));
                    if (kvRetailer == null)
                    {
                        using var dbMaster = HostContext.TryResolve<IDbConnectionFactory>()
                            .Open(nameof(KvSqlConnectString.KvMasterEntities));
                        kvRetailer = dbMaster.Single<KvRetailer>(x => x.Code == retailerCode);
                    }

                    var kvGroup = cacheClient.Get<Domain.Common.KvGroup>(string.Format(KvConstant.KvGroupCacheKey, kvRetailer.GroupId));
                    if (kvGroup == null)
                    {
                        using var dbMaster = HostContext.TryResolve<IDbConnectionFactory>()
                            .Open(nameof(KvSqlConnectString.KvMasterEntities));
                        kvGroup = dbMaster.Single<Domain.Common.KvGroup>(x => x.Id == kvRetailer.GroupId);
                    }

                    var kvInternalContext = new KvInternalContext
                    {
                        RetailerCode = retailerCode,
                        RetailerId = kvRetailer.Id,
                        Token = request.GetJwtToken(),
                        BranchId = branchId,
                        UserId = userId,
                        Group = new Domain.Common.KvGroup { Id = kvGroup?.Id ?? 0, ConnectionString = kvGroup?.ConnectionString, IsFreePremium = kvGroup?.IsFreePremium }
                    };
                    SetUiCulture(session.Language);
                    return kvInternalContext;
                }
                catch (Exception e)
                {
                    logger.LogError($"Create {nameof(KvInternalContext)} error: {e.Message}", e);
                    return null;
                }
            }
            SetUiCulture();
            services.AddScoped(ImplementationFactory);
        }

        private static void SetUiCulture(string lang = "vi-VN")
        {
            if (string.IsNullOrEmpty(lang))
            {
                lang = "vi-VN";
            }

            var info = CultureInfo.CreateSpecificCulture(lang);
            info.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            info.NumberFormat.CurrencyDecimalSeparator = ".";
            info.NumberFormat.CurrencyGroupSeparator = ",";
            info.NumberFormat.CurrencySymbol = string.Empty;
            info.NumberFormat.NumberDecimalSeparator = ".";
            info.NumberFormat.NumberGroupSeparator = ",";
            info.NumberFormat.PercentDecimalSeparator = ".";
            info.NumberFormat.PercentGroupSeparator = ",";
            Thread.CurrentThread.CurrentCulture = info;
            Thread.CurrentThread.CurrentUICulture = info;
            Thread.CurrentThread.CurrentUICulture = info;
        }
    }
}

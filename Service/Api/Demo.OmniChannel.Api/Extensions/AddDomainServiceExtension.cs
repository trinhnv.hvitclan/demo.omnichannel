﻿using Funq;
using Demo.OmniChannel.Services.Impls;
using Demo.OmniChannel.Services.Interfaces;
using ServiceStack;

namespace Demo.OmniChannel.Api.Extensions
{
    public static class AddDomainServiceExtension
    {
        public static void AddDomainService(this Container container)
        {
            container.AddScoped<IOmniPlatformSettingService, OmniPlatformSettingService>();
        }
    }
}

﻿using Demo.OmniChannel.Infrastructure.Persistence.Ef;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Demo.OmniChannel.Api.Extensions
{
    public static class AddDbContextExtension
    {
        public static void AddCustomDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionStr = configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<EfDbContext>(opts =>
            {
                opts.UseSqlServer(connectionStr);
            });
        }
    }
}
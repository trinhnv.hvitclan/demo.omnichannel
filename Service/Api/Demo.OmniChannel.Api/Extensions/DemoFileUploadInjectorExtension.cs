﻿using Demo.OmniChannel.FileUpload;
using Demo.OmniChannel.FileUpload.Providers.AmazoneS3;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Demo.OmniChannel.Api.Extensions
{
    public static class DemoFileUploadInjectorExtension
    {
        public static void AddDemoFileUpload(this IServiceCollection services, IConfiguration configuration)
        {
            var amazoneS3FileUploadConfiguration = new AmazoneS3FileUploadConfiguration();
            configuration.GetSection("DemoFileUpload").Bind(amazoneS3FileUploadConfiguration);
            services.AddScoped<IDemoFileUpload>(c => new AmazoneS3FileUploadProvider(amazoneS3FileUploadConfiguration));
        }
    }
}

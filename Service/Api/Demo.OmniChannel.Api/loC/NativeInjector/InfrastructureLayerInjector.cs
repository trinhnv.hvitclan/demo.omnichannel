﻿using Demo.OmniChannel.Api.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Demo.OmniChannel.Api.loC.NativeInjector
{
    public class InfrastructureLayerInjector
    {
        protected InfrastructureLayerInjector() { }
        public static void Register(IServiceCollection services, IConfiguration configuration)
        {
            // Demo FileUpload
            services.AddDemoFileUpload(configuration);

        }
    }
}

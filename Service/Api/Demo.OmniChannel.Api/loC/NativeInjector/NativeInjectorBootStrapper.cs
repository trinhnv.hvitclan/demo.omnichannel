﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Demo.OmniChannel.Api.loC.NativeInjector
{
    public class NativeInjectorBootStrapper
    {
        protected NativeInjectorBootStrapper() { }
        public static void Register(IServiceCollection services, IConfiguration configuration)
        {
            InfrastructureLayerInjector.Register(services, configuration);
        }
    }
}

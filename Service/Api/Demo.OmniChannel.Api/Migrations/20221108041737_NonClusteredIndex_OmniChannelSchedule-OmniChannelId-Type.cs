﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Demo.OmniChannel.Api.Migrations
{
    public partial class NonClusteredIndex_OmniChannelScheduleOmniChannelIdType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "NonClusteredIndex_OmniChannelSchedule-OmniChannelId-Type",
                table: "OmniChannelSchedule");

            migrationBuilder.CreateIndex(
                name: "NonClusteredIndex_OmniChannelSchedule-OmniChannelId-Type",
                table: "OmniChannelSchedule",
                columns: new[] { "OmniChannelId", "Type" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "NonClusteredIndex_OmniChannelSchedule-OmniChannelId-Type",
                table: "OmniChannelSchedule");

            migrationBuilder.CreateIndex(
                name: "IX_OmniChannelSchedule_OmniChannelId",
                table: "OmniChannelSchedule",
                column: "OmniChannelId");
        }
    }
}

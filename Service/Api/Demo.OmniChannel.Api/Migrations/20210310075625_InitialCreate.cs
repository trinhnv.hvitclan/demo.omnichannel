﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Demo.OmniChannel.Api.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "OmniChannel",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CreateBy = table.Column<long>(nullable: false),
                    ModifiedBy = table.Column<long>(nullable: true),
                    Type = table.Column<byte>(nullable: false),
                    Status = table.Column<byte>(nullable: false),
                    BranchId = table.Column<int>(nullable: false),
                    RetailerId = table.Column<int>(nullable: false),
                    PriceBookId = table.Column<long>(nullable: true),
                    BasePriceBookId = table.Column<long>(nullable: true),
                    SyncOnHandFormula = table.Column<byte>(nullable: false),
                    SyncOrderFormula = table.Column<int>(nullable: true),
                    IsApplyAllFormula = table.Column<bool>(nullable: false),
                    IsAutoMappingProduct = table.Column<bool>(nullable: false),
                    IsAutoDeleteMapping = table.Column<bool>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    IdentityKey = table.Column<string>(nullable: true),
                    RegisterDate = table.Column<DateTime>(nullable: true),
                    ExtraKey = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OmniChannel", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OmniChannelAuth",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ShopId = table.Column<long>(nullable: false),
                    AccessToken = table.Column<string>(maxLength: 1000, nullable: true),
                    RefreshToken = table.Column<string>(maxLength: 1000, nullable: true),
                    ExpiresIn = table.Column<int>(nullable: false),
                    RefreshExpiresIn = table.Column<int>(nullable: false),
                    ExpiryDate = table.Column<DateTime>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CreateBy = table.Column<long>(nullable: false),
                    ModifiedBy = table.Column<long>(nullable: true),
                    OmniChannelId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OmniChannelAuth", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OmniChannelAuth_OmniChannel_OmniChannelId",
                        column: x => x.OmniChannelId,
                        principalTable: "OmniChannel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OmniChannelSchedule",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NextRun = table.Column<DateTime>(nullable: true),
                    LastSync = table.Column<DateTime>(nullable: true),
                    IsRunning = table.Column<bool>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    OmniChannelId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OmniChannelSchedule", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OmniChannelSchedule_OmniChannel_OmniChannelId",
                        column: x => x.OmniChannelId,
                        principalTable: "OmniChannel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "NonClusteredIndex_OmniChannelApp",
                table: "OmniChannel",
                column: "RetailerId")
                .Annotation("SqlServer:IncludeIndex", "[BranchId], [PriceBookId]");

            migrationBuilder.CreateIndex(
                name: "IX_OmniChannelAuth_OmniChannelId",
                table: "OmniChannelAuth",
                column: "OmniChannelId",
                unique: true,
                filter: "[OmniChannelId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_OmniChannelSchedule_OmniChannelId",
                table: "OmniChannelSchedule",
                column: "OmniChannelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OmniChannelAuth");

            migrationBuilder.DropTable(
                name: "OmniChannelSchedule");

            migrationBuilder.DropTable(
                name: "OmniChannel");
        }
    }
}

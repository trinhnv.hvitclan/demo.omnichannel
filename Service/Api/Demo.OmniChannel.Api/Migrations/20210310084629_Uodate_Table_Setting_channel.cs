﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Demo.OmniChannel.Api.Migrations
{
    public partial class Uodate_Table_Setting_channel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "OmniChannelSetting",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    ChannelType = table.Column<byte>(nullable: false),
                    OmniChannelId = table.Column<long>(nullable: false),
                    BranchId = table.Column<int>(nullable: false),
                    RetailerId = table.Column<int>(nullable: false),
                    ShopId = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OmniChannelSetting", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OmniChannelSetting_OmniChannel_OmniChannelId",
                        column: x => x.OmniChannelId,
                        principalTable: "OmniChannel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OmniChannelSetting_OmniChannelId",
                table: "OmniChannelSetting",
                column: "OmniChannelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OmniChannelSetting");
        }
    }
}

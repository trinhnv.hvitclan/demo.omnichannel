﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Demo.OmniChannel.Api.Migrations
{
    public partial class addNonClusterIndexOmniChannelSetting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_OmniChannelSetting_OmniChannelId",
                table: "OmniChannelSetting");

            migrationBuilder.CreateIndex(
                name: "NonClusteredIndex_OmniChannelSetting_OmniChannelId",
                table: "OmniChannelSetting",
                columns: new[] { "OmniChannelId", "RetailerId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "NonClusteredIndex_OmniChannelSetting_OmniChannelId",
                table: "OmniChannelSetting");

            migrationBuilder.CreateIndex(
                name: "IX_OmniChannelSetting_OmniChannelId",
                table: "OmniChannelSetting",
                column: "OmniChannelId");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Demo.OmniChannel.Api.Migrations
{
    public partial class addNonClusterIndexSettingOmniChannelIdName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "OmniChannelSetting",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "NonClusteredIndex_OmniChannelSetting-OmniChannelId-Name",
                table: "OmniChannelSetting",
                columns: new[] { "OmniChannelId", "Name" },
                unique: true,
                filter: "[Name] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "NonClusteredIndex_OmniChannelSetting-OmniChannelId-Name",
                table: "OmniChannelSetting");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "OmniChannelSetting",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}

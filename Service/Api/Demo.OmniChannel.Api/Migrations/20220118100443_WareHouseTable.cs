﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Demo.OmniChannel.Api.Migrations
{
    public partial class WareHouseTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "OmniChannelWareHouse",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    WareHouseId = table.Column<long>(nullable: false),
                    OmniChannelId = table.Column<long>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CreateBy = table.Column<long>(nullable: false),
                    ModifiedBy = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    RetailerId = table.Column<long>(nullable: false),
                    StreetInfo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OmniChannelWareHouse", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OmniChannelWareHouse_OmniChannel_OmniChannelId",
                        column: x => x.OmniChannelId,
                        principalTable: "OmniChannel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OmniChannelWareHouse_OmniChannelId",
                table: "OmniChannelWareHouse",
                column: "OmniChannelId");

            migrationBuilder.CreateIndex(
                name: "NonClusteredIndex_OmniChannelWareHouse_OmniChannelId",
                table: "OmniChannelWareHouse",
                columns: new[] { "RetailerId", "OmniChannelId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OmniChannelWareHouse");
        }
    }
}

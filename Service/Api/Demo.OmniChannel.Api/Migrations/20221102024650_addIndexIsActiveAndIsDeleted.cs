﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Demo.OmniChannel.Api.Migrations
{
    public partial class addIndexIsActiveAndIsDeleted : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "NonClusteredIndex_IsActive_IsDeleted",
                table: "OmniChannel",
                columns: new[] { "IsActive", "IsDeleted" })
                .Annotation("SqlServer:IncludeIndex", "[Id], [Name], [Type]");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "NonClusteredIndex_IsActive_IsDeleted",
                table: "OmniChannel");
        }
    }
}

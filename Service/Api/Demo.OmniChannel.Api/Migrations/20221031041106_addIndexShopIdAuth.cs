﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Demo.OmniChannel.Api.Migrations
{
    public partial class addIndexShopIdAuth : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "NonClusteredIndex_OmniChannelAuth_ShopId",
                table: "OmniChannelAuth",
                column: "ShopId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "NonClusteredIndex_OmniChannelAuth_ShopId",
                table: "OmniChannelAuth");
        }
    }
}

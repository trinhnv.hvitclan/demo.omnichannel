﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Demo.OmniChannel.Api.Migrations
{
    public partial class addOmniChannelIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "IdentityKey",
                table: "OmniChannel",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "NonClusteredIndex_OmniChannel_PlatformId",
                table: "OmniChannel",
                columns: new[] { "IdentityKey", "PlatformId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "NonClusteredIndex_OmniChannel_PlatformId",
                table: "OmniChannel");

            migrationBuilder.AlterColumn<string>(
                name: "IdentityKey",
                table: "OmniChannel",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Demo.OmniChannel.Api.Migrations
{
    public partial class updateIndexIsActiveAndIsDeleted : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "NonClusteredIndex_IsActive_IsDeleted",
                table: "OmniChannel");

            migrationBuilder.CreateIndex(
                name: "NonClusteredIndex_IsActive_IsDeleted",
                table: "OmniChannel",
                columns: new[] { "IsActive", "IsDeleted" })
                .Annotation("SqlServer:IncludeIndex", "[Id], [Name], [Type], [RetailerId]");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "NonClusteredIndex_IsActive_IsDeleted",
                table: "OmniChannel");

            migrationBuilder.CreateIndex(
                name: "NonClusteredIndex_IsActive_IsDeleted",
                table: "OmniChannel",
                columns: new[] { "IsActive", "IsDeleted" })
                .Annotation("SqlServer:IncludeIndex", "[Id], [Name], [Type]");
        }
    }
}

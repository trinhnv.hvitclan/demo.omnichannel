﻿using System.IO;
using Demo.OmniChannel.Infrastructure.Persistence.Ef;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Demo.OmniChannel.Api.DbContextFactoryForMigration
{
    public class ApplicationContextDbFactory : IDesignTimeDbContextFactory<EfDbContext>
    {
        public EfDbContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            //var connectionStr = configuration.GetConnectionString("DefaultConnection");
            var connectionStr = configuration.GetValue<string>("SqlConnectStrings:KvChannelEntities");
            var optionsBuilder = new DbContextOptionsBuilder<EfDbContext>();
            optionsBuilder.UseSqlServer(connectionStr, b => b.MigrationsAssembly("Demo.OmniChannel.Api"));

            return new EfDbContext(optionsBuilder.Options);
        }
    }
}
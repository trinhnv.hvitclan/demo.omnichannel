﻿using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.Utilities;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Api.ServiceInterface
{
    public class ProxyProductApi : Service
    {
        protected ILog Logger = LogManager.GetLogger(typeof(ProxyProductApi));
        public IAppSettings AppSettings { get; set; }
        public async Task Post(ProxyProductSubscribe req)
        {
            var kafkaConfig = AppSettings.Get<Kafka>("kafka");
            KafkaClient.KafkaClient.Instance.PublishMessage(
                $"{kafkaConfig.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.SyncEsDc2Message)?.Name}",
                string.Empty, req.ToJson());
            Logger.Info($"ProxyProductSubscribe RetailerId:{req.RetailerId} ProductId:{req.Ids} req : {req.ToSafeJson()}");
            await Task.Yield();
        }

        public async Task Post(ProxyPriceBookSubscribe req)
        {
            var kafkaConfig = AppSettings.Get<Kafka>("kafka");
            KafkaClient.KafkaClient.Instance.PublishMessage(
                $"{kafkaConfig.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.PriceBookSyncEsDc2Message)?.Name}",
                string.Empty, req.ToJson());
            Logger.Info($"ProxyPriceBookSubscribe RetailerId:{req.RetailerId} req : {req.ToSafeJson()}");
            await Task.Yield();
        }
    }
}
﻿using Demo.OmniChannel.Domain.Common;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Securities;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ShareKernel.Auth;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Utilities;
using ILogger = Microsoft.Extensions.Logging.ILogger;
using Demo.OmniChannel.Application.Type;
using System.Net;

namespace Demo.OmniChannel.Api.ServiceInterface
{
    [Authenticate]
    public class BaseApi : Service
    {
        protected ILogger Logger;
        protected int CurrentRetailerId => SessionAs<KVSession>().CurrentRetailerId;
        protected int CurrentBranchId
        {
            get
            {
                var branchByReq = ConvertHelper.ToInt32(Request?.GetParam("BranchId"));
                if (branchByReq > 0) return branchByReq;

                var branchByHeader = ConvertHelper.ToInt32(Request?.GetHeader("branchid"));
                if (branchByHeader > 0) return branchByHeader;

                return SessionAs<KVSession>().CurrentBranchId;
            }
        }
        protected string CurrentRetailerCode => SessionAs<KVSession>().CurrentRetailerCode;
        protected int GroupId => SessionAs<KVSession>().GroupId;
        private PosSetting PosSettingPrivate { get; set; }
        protected PosSetting KvPosSetting => PosSettingPrivate ?? (PosSettingPrivate = GetPosSettings().Result);
        public IMessageFactory MessageFactory { get; set; }
        public KvInternalContext InternalContext { get; set; }
        public ICacheClient CacheClient { get; set; }
        public IDbConnectionFactory DbConnectionFactory { get; set; }
        public IAppSettings AppSettings { get; set; }
        public BaseApi(ILogger logger)
        {
            Logger = logger;
        }

        protected void UpdateProductFlagRelate(List<long> kvProductIds, bool isRelate, Guid logId)
        {
            if (kvProductIds?.Any() == true)
            {
                using (var mq = MessageFactory.CreateMessageProducer())
                {
                    var context = new ExecutionContext
                    {
                        RetailerId = InternalContext.RetailerId,
                        RetailerCode = InternalContext.RetailerCode,
                        BranchId = InternalContext.BranchId,
                        User = new SessionUser
                        {
                            Id = InternalContext.UserId
                        }
                    };
                    var msg = new UpdateProductFlagRelateMessage
                    {
                        ExecutionContext = context,
                        ProductIds = kvProductIds,
                        IsRelate = isRelate,
                        LogId = logId
                    };
                    mq.Publish(msg);
                }
            }
        }

        protected async Task<KvRetailer> GetRetailer(long retailerId)
        {
            var kvRetailer = CacheClient.Get<KvRetailer>(string.Format(KvConstant.RetailerIdCacheKey, retailerId));
            if (kvRetailer == null)
            {
                using (var dbMaster = await DbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities))
                {
                    kvRetailer = await dbMaster.SingleAsync<KvRetailer>(x => x.Id == retailerId);
                    if (kvRetailer != null)
                    {
                        CacheClient.Set(string.Format(KvConstant.RetailerIdCacheKey, retailerId), kvRetailer, DateTime.Now.AddDays(7));
                    }
                }
            }
            return kvRetailer;
        }

        protected async Task<Domain.Common.KvGroup> GetRetailerGroup(int groupId)
        {
            var kvGroup = CacheClient.Get<Domain.Common.KvGroup>(string.Format(KvConstant.KvGroupCacheKey, groupId));
            if (kvGroup == null)
            {
                using (var dbMaster = await DbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities))
                {
                    kvGroup = await dbMaster.SingleAsync<Domain.Common.KvGroup>(x => x.Id == groupId);
                    if (kvGroup != null)
                    {
                        CacheClient.Set(string.Format(KvConstant.KvGroupCacheKey, groupId), kvGroup, DateTime.Now.AddDays(7));
                    }
                }
            }
            return kvGroup;
        }

        protected async Task<List<int>> GetActiveRetailerIdsByGroup(int groupId)
        {
            var kvRetailers = CacheClient.Get<List<int>>(string.Format(KvConstant.GroupIdWithActiveRetailerKey, groupId));
            if (kvRetailers == null)
            {
                using (var dbMaster = await DbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities))
                {
                    var retailers = await dbMaster
                        .SelectAsync<KvRetailer>(retailer => retailer.IsActive && retailer.GroupId == groupId &&
                        retailer.ExpiryDate != null && retailer.ExpiryDate > DateTime.Now);
                    if (retailers != null)
                    {
                        var retailerIds = retailers.Map(retailer => retailer.Id);
                        CacheClient.Set(string.Format(KvConstant.GroupIdWithActiveRetailerKey, groupId), retailerIds, DateTime.Now.AddDays(3));
                        return retailerIds;
                    }
                }
            }
            return kvRetailers;
        }

        protected string GetKvEntitiesConnectString()
        {
            var connectionString = InternalContext.Group?.ConnectionString?.ToLower().Replace("name=", "");
            return connectionString;
        }

        protected async Task<PosSetting> GetPosSettings()
        {
            var connectionString = GetKvEntitiesConnectString();
            using (var db = await DbConnectionFactory.OpenAsync(connectionString))
            {
                var posSettingRepository = new PosSettingRepository(db);
                var retailerId = InternalContext?.RetailerId ?? 0;
                var posSetting = new PosSetting(await posSettingRepository.GetSettingAsync(retailerId),
                    retailerId, DbConnectionFactory, CacheClient, AppSettings);
                return posSetting;
            }
        }

        protected Response<object> BadRequest(string message,
            IEnumerable<ErrorResult> errors, object data = null)
        {
            Response.StatusCode = (int)HttpStatusCode.BadRequest;

            return new Response<object>
            {
                Message = message,
                Errors = errors,
                Result = data
            };
        }

        protected Response<T> Ok<T>(string msg, T obj) where T : class
        {
            Response.StatusCode = (int)HttpStatusCode.OK;
            return new Response<T>
            {
                Message = msg,
                Result = obj
            };
        }

        protected Response<T> Ok<T>(T obj) where T : class
        {
            Response.StatusCode = (int)HttpStatusCode.OK;
            return new Response<T>
            {
                Message = string.Empty,
                Result = obj
            };
        }

    }
}

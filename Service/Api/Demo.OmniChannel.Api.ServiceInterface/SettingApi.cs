﻿using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.Resource;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Api.ServiceInterface
{
    public class SettingApi : BaseApi
    {
        public  IOmniPlatformSettingRepository OmniPlatformSettingRepository { get; set; }
        public IOmniPlatformSettingService OmniPlatformSettingService { get; set; }

        public SettingApi(ILogger<SettingApi> logger) : base(logger)
        {
         
        }

        public async Task<object> Post(CreateUpdateSettingCustomer req)
        {
            var validateResult = await new CreateUpdateSettingCustomerValidator().ValidateAsync(req);

            if (!validateResult.IsValid)
            {
                throw new OmniException(validateResult.Errors.FirstOrDefault()?.ErrorMessage);
            }

            var settings = await OmniPlatformSettingService.CreateOrUpdateAsync(req);

            return Ok(string.Format(KvMessage.updateSuccessfull, "thiết lập"), settings);
        }

        public async Task<object> Get(GetSettingByRetailer req)
        {
            var settings = await OmniPlatformSettingRepository.GetByRetailerAsync(CurrentRetailerId);

            return Ok(settings);
        }
    }
}
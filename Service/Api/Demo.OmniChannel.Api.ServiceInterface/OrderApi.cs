﻿using Demo.Audit.Model.Message;
using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Common;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Api.ServiceInterface
{
    public class OrderApi : BaseApi
    {
        public IOrderMongoService OrderMongoService { get; set; }
        public IOmniChannelService OmniChannelService { get; set; }
        public IAuditTrailInternalClient AuditTrailInternalClient { get; set; }

        public OrderApi(ILogger<OrderApi> logger) : base(logger)
        {
        }
        public async Task<object> Any(GetOrderSyncError req)
        {
            var orderDataSource = await OrderMongoService.GetByChannelId(req.RetailerId, req.Top, req.Skip, req.OrderCode, req.SearchTerm, req.ChannelIds, req.BranchIds, req.KeySearchProduct);

            if (orderDataSource.Total == 0 || orderDataSource.Data.Count == 0)
            {
                return new PagingDataSource<OrderDto>();
            }

            var orderDtos = orderDataSource.Data.Select(x => x.ConvertTo<OrderDto>()).ToList();

            foreach (var item in orderDtos)
            {
                var order = orderDataSource.Data.FirstOrDefault(x => x.Code == item.Code && x.ChannelId == item.ChannelId);
                if (order == null) continue;

                item.OrderDetails = order.OrderDetails.Select(x => new OrderDetailDto
                {
                    CommonParentChannelProductId = x.CommonParentChannelProductId,
                    CommonProductChannelId = x.CommonProductChannelId,
                    Discount = x.Discount,
                    DiscountPrice = x.DiscountPrice,
                    DiscountRatio = x.DiscountRatio,
                    Note = x.Note,
                    Price = x.Price,
                    ProductChannelName = x.ProductChannelName,
                    ProductName = x.ProductName,
                    ProductChannelSku = x.ProductChannelSku,
                    ProductCode = x.ProductCode,
                    ProductId = x.ProductId,
                    Quantity = x.Quantity,
                    UseProductBatchExpire = x.UseProductBatchExpire,
                    UseProductSerial = x.UseProductSerial,
                    Uuid = x.Uuid
                }).ToList();
            }
            return new PagingDataSource<OrderDto>
            {
                Total = orderDataSource.Total,
                Data = orderDtos
            };
        }

        public async Task<object> Any(GetOrderByIds req)
        {
            return await OrderMongoService.GetByIdAsync(req.Id);
        }

        public async Task<object> Delete(RemoveOrderSyncError req)
        {
            var logId = Guid.NewGuid();
            var logMessage = new LogObjectMicrosoftExtension(Logger, logId)
            {
                Action = "RemoveOrderSyncError",
                RetailerId = InternalContext.RetailerId,
                RequestObject = req.ToJson(),
            };

            var order = await OrderMongoService.GetByIdAsync(req.Id);
            if (order != null)
            {
                await OrderMongoService.RemoveAsync(req.Id);
                var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
                var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, InternalContext.RetailerId, connectStringName, InternalContext.BranchId, AppSettings.Get<int>("ExecutionContext"));
                var channel = await OmniChannelService.GetByIdAsync(order.ChannelId);
                var log = new AuditTrailLog
                {
                    FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channel.Type),
                    Action = (int)AuditTrailAction.OrderIntergate,
                    CreatedDate = DateTime.Now,
                    BranchId = order.BranchId,
                    Content = $"Xóa đơn hàng: [OrderCode]{order.Code}[/OrderCode] của gian hàng {context.RetailerCode} trên sàn {channel?.Name} khỏi danh sách đồng bộ đặt hàng lỗi."
                };
                var coreContext = context.ConvertTo<KvInternalContext>();
                coreContext.UserId = context.User?.Id ?? 0;
                await AuditTrailInternalClient.AddLogAsync(coreContext, log);
            }
            logMessage.LogInfo();
            return true;
        }

        public async Task<object> Post(UpdateErrorMessageOrder req)
        {
            var existOrder = await OrderMongoService.GetByIdAsync(req.Order.Id);
            if (existOrder != null)
            {
                existOrder.ErrorMessage = req.ErrorMessage;
                existOrder.OrderDetails = req.Order.OrderDetails;
                await OrderMongoService.UpdateAsync(existOrder.Id, existOrder);
            }

            var order = await OrderMongoService.UpdateMessageById(req.Order.Id, req.ErrorMessage);
            if (order == null) return true;

            var channel = await OmniChannelService.GetByIdAsync(order.ChannelId);
            if (channel == null) return true;

            var connectStringName =
                InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') +
                                                                  1);
            var orderId = order.Code.Substring(order.Code.IndexOf('_') + 1);
            var log = new AuditTrailLog
            {
                FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channel.Type),
                Action = (int)AuditTrailAction.OrderIntergate,
                CreatedDate = DateTime.Now,
                BranchId = order.BranchId,
                Content =
                    $"Tạo đơn đặt hàng KHÔNG thành công: [OrderCode]{order.Code}[/OrderCode] (cho đơn đặt hàng: {orderId}), lý do: <br/>{req.AuditTrailMessage}"
            };

            var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory,
                InternalContext.RetailerId, connectStringName, InternalContext.BranchId,
                AppSettings.Get<int>("ExecutionContext"));
            var coreContext = context.ConvertTo<KvInternalContext>();
            coreContext.UserId = context.User?.Id ?? 0;
            await AuditTrailInternalClient.AddLogAsync(coreContext, log);
            //using (var msqClient = MessageFactory.CreateMessageQueueClient())
            //{
            //    msqClient.Publish(new AuditTrailMessage
            //    {
            //        Context = context,
            //        Data = log,
            //        LotId = ObjectId.GenerateNewId().ToString(),
            //        Key = context.Id
            //    });
            //}

            return true;
        }

        public async Task<long> Any(TotalOrderError req)
        {
            return await OrderMongoService.GetTotalOrderError(InternalContext.RetailerId, req.ChannelIds);
        }

        public object Post(MultiSyncErrorOrder req)
        {
            var logId = Guid.NewGuid();
            var logMessage = new LogObjectMicrosoftExtension(Logger, logId)
            {
                Action = "MultiSyncErrorOrder",
                RetailerId = InternalContext.RetailerId,
                RequestObject= req.ToJson(),
            };

            var orders = req.Orders.FromJson<List<OrderDto>>();
            if (orders != null && orders.Any())
            {
                orders = orders.GroupBy(x => x.Code).Select(x => x.First()).ToList();
            }
            else
            {
                return false;
            }

            var channels = req.Channels.FromJson<List<Domain.Model.OmniChannel>>();

            foreach (var order in orders)
            {
                var channel = channels.FirstOrDefault(x => x.Id == order.ChannelId);
                if (channel == null)
                {
                    logMessage.LogInfo($"{req.ToSafeJson()}. FAIL: Channel Empty");
                    continue;
                }

                var orderRetry = order.ConvertTo<MongoDb.Order>();
                orderRetry.OrderDetails = order.OrderDetails.Select(x => new MongoDb.OrderDetail
                {
                    CommonParentChannelProductId = x.ParentChannelProductId ?? x.StrParentChannelProductId,
                    CommonProductChannelId = x.ProductChannelId ?? x.StrProductChannelId,
                    StrProductChannelId = x.StrProductChannelId,
                    Discount = x.Discount,
                    DiscountPrice = x.DiscountPrice,
                    DiscountRatio = x.DiscountRatio,
                    Note = x.Note,
                    Price = x.Price,
                    ProductChannelName = x.ProductChannelName,
                    ProductChannelSku = x.ProductChannelSku,
                    ProductCode = x.ProductCode,
                    ProductId = x.ProductId,
                    ProductName = x.ProductName,
                    Quantity = x.Quantity,
                    UseProductBatchExpire = x.UseProductBatchExpire,
                    UseProductSerial = x.UseProductSerial,
                    Uuid = string.IsNullOrEmpty(x.Uuid) ? StringHelper.GenerateUUID() : x.Uuid,
                }).ToList();

                PushSyncErrorOrderMessage(channel, orderRetry, logId);
            }



            logMessage.LogInfo();
            return true;
        }

        private void PushSyncErrorOrderMessage(Domain.Model.OmniChannel channel, MongoDb.Order order, Guid logId)
        {
            switch (channel.Type)
            {
                case (byte)ChannelType.Lazada:
                    {
                        var message = new LazadaSyncErrorOrderMessage
                        {
                            RetailerCode = InternalContext.RetailerCode,
                            KvEntities = InternalContext.Group?.ConnectionString,
                            Order = order.ToSafeJson(),
                            RetailerId = InternalContext.RetailerId,
                            ChannelId = order.ChannelId,
                            BranchId = order.BranchId,
                            LogId = logId
                        };

                        using (var mq = MessageFactory.CreateMessageProducer())
                        {
                            mq.Publish(message);
                        }
                        break;
                    }
                case (byte)ChannelType.Shopee:
                    {
                        var messageKafka = new ShopeeSyncErrorCreateOrder
                        {
                            OrderSn = order.OrderId,
                            LogId = logId,
                            BranchId = order.BranchId,
                            ChannelId = (int)order.ChannelId,
                            RetailerId = order.RetailerId
                        };
                        var kafkaConfig = AppSettings.Get<Kafka>("Kafka");
                        KafkaClient.KafkaClient.Instance.PublishMessage($"{kafkaConfig.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.SyncErrorOrder && x.ChannelType == channel.Type)?.Name}", string.Empty, messageKafka.ToJson());
                        break;
                    }
                case (byte)ChannelType.Tiki:
                    {
                        var message = new TikiSyncErrorOrderMessage
                        {
                            RetailerCode = InternalContext.RetailerCode,
                            KvEntities = InternalContext.Group?.ConnectionString,
                            Order = order.ToSafeJson(),
                            RetailerId = InternalContext.RetailerId,
                            ChannelId = order.ChannelId,
                            BranchId = order.BranchId,
                            LogId = logId
                        };
                        using (var mq = MessageFactory.CreateMessageProducer())
                        {
                            mq.Publish(message);
                        }
                        break;
                    }
                case (byte)ChannelType.Sendo:
                    {
                        var message = new SendoSyncErrorOrderMessage
                        {
                            RetailerCode = InternalContext.RetailerCode,
                            KvEntities = InternalContext.Group?.ConnectionString,
                            Order = order.ToSafeJson(),
                            RetailerId = InternalContext.RetailerId,
                            ChannelId = order.ChannelId,
                            BranchId = order.BranchId,
                            LogId = logId
                        };
                        using (var mq = MessageFactory.CreateMessageProducer())
                        {
                            mq.Publish(message);
                        }
                        break;
                    }
                case (byte)ChannelType.Tiktok:
                    {
                        var messageKafka = new TiktokSyncErrorCreateOrder
                        {
                            OrderId = order.OrderId,
                            LogId = logId,
                            BranchId = order.BranchId,
                            ChannelId = (int)order.ChannelId,
                            RetailerId = order.RetailerId
                        };
                        var kafkaConfig = AppSettings.Get<Kafka>("Kafka");
                        KafkaClient.KafkaClient.Instance.PublishMessage($"{kafkaConfig.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.SyncErrorOrder && x.ChannelType == channel.Type)?.Name}", string.Empty, messageKafka.ToJson());
                        break;
                    }
            }
        }
    }
}
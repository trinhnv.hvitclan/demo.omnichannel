using ServiceStack;
using Microsoft.Extensions.Logging;
using ServiceStack.Caching;
using Demo.OmniChannel.Domain.Common;
using ServiceStack.Data;
using ServiceStack.Configuration;
using System.Threading.Tasks;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Application.Type;
using System.Collections.Generic;
using System;
using System.Net;
using Demo.OmniChannel.Application.Log;

namespace Demo.OmniChannel.Api.ServiceInterface
{
    [AccessInternalFilterAttribute]
    public class BaseInternalApi : Service
    {
        public ICacheClient CacheClient { get; set; }
        public KvInternalContext InternalContext { get; set; }
        public IDbConnectionFactory DbConnectionFactory { get; set; }
        public IAppSettings AppSettings { get; set; }
        protected ILogger Logger;
        public BaseInternalApi(ILogger logger)
        {
            Logger = logger;
        }

        protected async Task<OmniChannelCore.Api.Sdk.Common.KvInternalContext> GetCoreContext()
        {
            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, InternalContext.RetailerId,
                connectStringName, InternalContext.BranchId, AppSettings.Get<int>("ExecutionContext"));
            var coreContext = context.ConvertTo<OmniChannelCore.Api.Sdk.Common.KvInternalContext>();
            coreContext.UserId = context.User?.Id ?? 0;
            return coreContext;
        }


        protected object BadGateway(string message, IEnumerable<ErrorResult> errors, object data = null)
        {
            var ex = new Exception(message);
            ex.Data.Add("errors", errors);
            var exErr = GetMessageTemplateError(ex.Message);
            Logger.LogError(ex, exErr.ToJson());

            Response.StatusCode = (int)HttpStatusCode.BadGateway;

            return new
            {
                ResponseStatus = new ResponseStatus
                {
                    ErrorCode = HttpStatusCode.BadGateway.ToString(),
                    Message = ex.Message,
                    Errors = errors.ConvertTo<List<ResponseError>>()
                }
            };
        }

        protected Response<T> Ok<T>(string msg, T obj) where T : class
        {
            Response.StatusCode = (int)HttpStatusCode.OK;
            return new Response<T>
            {
                Message = msg,
                Result = obj
            };
        }

        #region MyRegion
        private MessageTemplateWriteLogger GetMessageTemplateError(string description)
        {
            var modelTemplate = new MessageTemplateWriteLogger
            {
                GroupId = InternalContext?.Group?.Id ?? 0,
                BranchId = InternalContext.BranchId,
                RetailerCode = InternalContext.RetailerCode,
                RetailerId = InternalContext.RetailerId,
                Description = description
            };

            return modelTemplate;
        }
        #endregion
    }
}

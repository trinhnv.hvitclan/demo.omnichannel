﻿using Demo.Audit.Model.Message;
using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.Api.ServiceModel.Retailer;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee;
using Demo.OmniChannel.CoreDemoContext.Interfaces;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.ShareKernel.Dto;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Resource;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Linq;
using System.Threading.Tasks;
using ChannelType = Demo.OmniChannel.ShareKernel.Common.ChannelType;
using Demo.OmniChannel.Api.ServiceModel.Request;

namespace Demo.OmniChannel.Api.ServiceInterface.Retailer
{
    public class TikiApi : BaseApi
    {
        public ChannelApi ChannelApi { get; set; }
        private readonly ISaleChannelInternalClient _saleChannelInternalClient;
        private readonly IDemoOmniChannelCoreContext _DemoOmniChannelCoreContext;
        private readonly IChannelClient _channelClient;
        public IAuditTrailInternalClient AuditTrailInternalClient { get; set; }

        public TikiApi(ILogger<TikiApi> logger) : base(logger)
        {
        }

        public TikiApi(ILogger<TikiApi> logger,
            ISaleChannelInternalClient saleChannelInternalClient,
            IDemoOmniChannelCoreContext DemoOmniChannelCoreContext,
            IChannelClient channelClient) : base(logger)
        {
            _saleChannelInternalClient = saleChannelInternalClient;
            _DemoOmniChannelCoreContext = DemoOmniChannelCoreContext;
            _channelClient = channelClient;
        }
        #region POST methods
        public async Task<object> Post(TikiEnableChannel req)
        {
            var client = _channelClient.GetClient((byte)ChannelType.Tiki, AppSettings);
            var token = await client.CreateToken(req.Code, null, null);
            if (token == null)
            {
                throw new KvValidateTikiException(KVMessage.GlobalErrorSummary);
            }
            var auth = new ChannelAuth
            {
                AccessToken = token.AccessToken,
                RefreshToken = token.RefreshToken,
                ExpiresIn = token.ExpiresIn,
                RefreshExpiresIn = token.RefreshExpiresIn
            };
            var retval = await client.GetShopInfo(auth, InternalContext.ConvertTo<Demo.OmniChannel.Sdk.Common.KvInternalContext>(), true);
            if (retval == null)
            {
                throw new KvValidateTikiException(KVMessage.GlobalErrorSummary);
            }
            if (string.IsNullOrEmpty(req.IdentityKey) ||
                !req.IdentityKey.Equals(retval.IdentityKey, StringComparison.OrdinalIgnoreCase))
            {
                throw new KvValidateTikiException(string.Format(KVMessage.OmniChannelReAuthenAnotherShopFail, req.ShopName));
            }

            // Adding ShopId value for OmniChannelAuth
            long.TryParse(retval.IdentityKey, out var shopId);
            auth.ShopId = shopId;
            var enableChannel = new EnableChannel
            {
                Auth = auth.ConvertTo<Demo.OmniChannel.Domain.Model.OmniChannelAuth>(),
                BranchName = req.BranchName,
                ChannelId = req.ChannelId,
                PriceBookName = req.PriceBookName,
                SalePriceBookName = req.SalePriceBookName,
                Email = retval.Email,
                ShopName = req.ShopName,
                PlatformId = 0
            };
            return await ChannelApi.Post(enableChannel);
        }
        public async Task<object> Post(TikiSellerRegister req)
        {
            var newTikiApp = new OmniChannelRequest
            {
                Name = req.StoreName,
                BranchId = CurrentBranchId,
                RetailerId = CurrentRetailerId,
                Type = (byte)ChannelType.Tiki,
                Status = (byte)ChannelStatusType.Pending,
                IdentityKey = req.SellerId,
                RegisterDate = DateTime.Now
            };
            var r = await ChannelApi.Post(new ChannelCreateOrUpdate()
            {
                AuthId = 0,
                IsIgnoreAuditTrail = false,
                IsSyncOnHand = false,
                IsSyncOrder = false,
                IsSyncPrice = false,
                Channel = newTikiApp
            });
            var coreContext = await _DemoOmniChannelCoreContext.CreateOmniChannelCoreContext(CurrentRetailerId, InternalContext.BranchId, InternalContext.Group.ConnectionString);
            if (!string.IsNullOrWhiteSpace(r.IdentityKey))
            {
                await _saleChannelInternalClient.CreateOrUpdateSaleChannel(coreContext, CurrentRetailerId, (byte)ChannelType.Tiki, r.Id, r.Name);
            }

            var log = new AuditTrailLog
            {
                FunctionId = (int)FunctionType.PosParameter,
                Action = (int)AuditTrailAction.Create,
                Content = $"Đăng ký bán hàng cùng Tiki: {r.Name}",
                CreatedDate = DateTime.Now,
                BranchId = InternalContext.BranchId
            };
            await AuditTrailInternalClient.AddLogAsync(coreContext, log);
            return r;
        }
        public async Task<bool> Post(TikiRemoveAuth req)
        {
            var removeChannelAuth = new RemoveChannelAuth
            {
                AuthId = req.AuthId
            };
            return await ChannelApi.Post(removeChannelAuth);
        }
        public async Task<TikiResponse> Post(TikiAppPost req)
        {
            await ValidateTikiConnection(req);
            req.TikiApp.RetailerId = CurrentRetailerId;
            req.TikiApp.Type = (byte)ChannelType.Tiki;
            var exist = req.TikiApp.Id > 0 ? await ChannelApi.Get(new GetById { Id = req.TikiApp.Id }) : null;
            if (req.TikiApp.Status != (byte)ChannelStatusType.Approved && exist != null && exist.Type == (byte)ChannelType.Tiki
             && exist.Status != 0)
            {
                req.TikiApp.IsActive = false;
            }

            var r = await ChannelApi.Post(new ChannelCreateOrUpdate()
            {
                AuthId = req.AuthId,
                IsIgnoreAuditTrail = false,
                IsSyncOnHand = req.IsSyncOnHand,
                IsSyncOrder = req.IsSyncOrder,
                IsSyncPrice = req.IsSyncPrice,
                Channel = req.TikiApp.ConvertTo<OmniChannelRequest>()
            });
            var result = r.ConvertTo<TikiResponse>();
            if (!string.IsNullOrWhiteSpace(r.IdentityKey))
            {
                var coreContext = await _DemoOmniChannelCoreContext.CreateOmniChannelCoreContext(CurrentRetailerId, InternalContext.BranchId, InternalContext.Group.ConnectionString);
                result.SaleChannelId = await _saleChannelInternalClient.CreateOrUpdateSaleChannel(coreContext, CurrentRetailerId, (byte)ChannelType.Tiki, r.Id, r.Name);
            }
            return result;
        }

        public async Task<object> Post(GetWareHouses request)
        {
            var internalReq = new GetListChannelWareHouseInfo
            {
                OmniChannelAuthId = request.OmniAuthenId
            };
            return await ChannelApi.Post(internalReq);
        }
        #endregion

        #region GET methods
        public async Task<object> Get(TikiProfile req)
        {
            var client = _channelClient.GetClient((byte)ChannelType.Tiki, AppSettings);
            var authen = new ChannelAuth
            {
                Code = req.Code
            };
            var retval = await client.GetShopInfo(authen, InternalContext.ConvertTo<Demo.OmniChannel.Sdk.Common.KvInternalContext>());
            return retval.ConvertTo<ShopInfoResponseV2>();
        }

        public object Get(TikiAuthConfig req)
        {
            var tikiAppId = AppSettings.Get<string>("TikiClientAppId");
            var tikiCallBackUrl = AppSettings.Get<string>("TikiCallBack");
            return new
            {
                ClientId = tikiAppId,
                CallBackUrl = tikiCallBackUrl
            };
        }
        public async Task<Domain.Common.PagingDataSource<TikiResponse>> Get(TikiApps req)
        {
            var result = new Domain.Common.PagingDataSource<TikiResponse>();
            var newReq = new GetByRetailer
            {
                OnlyIsActive = false,
                RetailerId = InternalContext.RetailerId,
                Type = (byte)ChannelType.Tiki
            };
            var tikiLst = await ChannelApi.Any(newReq);
            if (tikiLst == null)
            {
                return result;
            }
            var tiki = tikiLst.Select(p =>
            {
                var item = p.ConvertTo<TikiResponse>();
                item.TotalProductConnected = p.TotalProductConnected;
                item.Name = p.Name;
                item.OmniChannelSchedules = p.OmniChannelSchedules.Map(x => x.ConvertTo<OmniChannelSchedule>()).ToList();
                return item;
            }).ToList();
            var branchIds = tiki.Where(item => item.BranchId.HasValue).Select(p => p.BranchId.Value);
            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            using (var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var branchRepository = new BranchRepository(db);
                var lstBranch = (await branchRepository.GetListBranchByIds(CurrentRetailerId, branchIds.ToList()))
                    .ToDictionary(item => item.Id, item => item.Name);
                var priceBookIds = tiki.Where(item => item.PriceBookId.HasValue).Select(p => p.PriceBookId.Value);
                var basePriceBookIds = tiki.Where(item => item.BasePriceBookId.HasValue).Select(p => p.BasePriceBookId.Value);
                var priceBookRepository = new PriceBookRepository(db);
                var lstPriceBook = (await priceBookRepository.GetByFilterAsync(CurrentRetailerId, priceBookIds.ToList(), true))
                    .ToDictionary(item => item.Id, item => item.Name);
                var lstBasePriceBook = (await priceBookRepository.GetByFilterAsync(CurrentRetailerId, basePriceBookIds.ToList(), true))
                    .ToDictionary(item => item.Id, item => item.Name);
                tiki.ForEach(x =>
                {
                    x.BranchName = lstBranch.ContainsKey(x.BranchId ?? 0) ? lstBranch[x.BranchId ?? 0] : "";
                    if (x.BasePriceBookId.HasValue)
                    {
                        x.BasePriceBookName = lstBasePriceBook.ContainsKey(x.BasePriceBookId ?? 0) ? lstBasePriceBook[x.BasePriceBookId ?? 0] : "Bảng giá chung";
                    }
                    x.IsSyncPrice = x.OmniChannelSchedules.Any(y => y.Type == (byte)Sdk.Common.ScheduleType.SyncPrice);
                    x.IsSyncOnHand = x.OmniChannelSchedules.Any(y => y.Type == (byte)Sdk.Common.ScheduleType.SyncOnhand);
                    x.SyncOnHandFormulaName = x.IsSyncOnHand ? $"{KvMessage.ecommSellQty} = {EnumHelper.ToDescription((EcommerceSellQtyFormula)x.SyncOnHandFormula)}" : string.Empty;
                    x.IsSyncOrder = x.OmniChannelSchedules.Any(y => y.Type == (byte)Sdk.Common.ScheduleType.SyncOrder);
                });
                result.Data = tiki;
                result.Total = tiki.Count;
            }
            return result;
        }
        public async Task<TikiResponse> Get(TikiAppGet req)
        {

            var result = await ChannelApi.Get(new GetById
            {
                Id = req.Id
            });
            var res = result.ConvertTo<TikiResponse>();
            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);

            using (var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var branchRepository = new BranchRepository(db);
                var priceBookRepository = new PriceBookRepository(db);

                var branch = await branchRepository.GetByIdAsync(res.BranchId ?? 0);
                if (branch == null)
                {
                    res.BranchId = -1;
                }
                res.BranchName = branch?.Name;
                if (result.OmniChannelSchedules != null && result.OmniChannelSchedules.Any())
                {
                    res.OmniChannelSchedules = result.OmniChannelSchedules.Map(x => x.ConvertTo<OmniChannelSchedule>());
                    res.IsSyncOnHand = result.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncOnhand);
                    res.IsSyncPrice = result.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncPrice);
                    res.IsSyncOrder = result.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncOrder);
                    res.SyncOnHandFormula = res.IsSyncOnHand ? result.SyncOnHandFormula : (byte)0;
                    res.SyncOrderFormula = result.SyncOrderFormula;
                    if (res.PriceBookId.HasValue)
                    {
                        var lstPriceBook = (await priceBookRepository.WhereAsync(p => p.RetailerId == CurrentRetailerId
                        && p.IsActive && p.StartDate < DateTime.Now && p.EndDate > DateTime.Now))
                        .ToDictionary(item => item.Id, item => item.Name);
                        var isActivePromoPriceBook = lstPriceBook.ContainsKey(res.PriceBookId.Value);
                        var pricebookName = isActivePromoPriceBook ? lstPriceBook[res.PriceBookId.Value] : string.Empty;
                        if (!isActivePromoPriceBook || pricebookName.Contains("{DEL"))
                        {
                            res.PriceBookId = 0;
                            res.PriceBookName = "Bảng giá chung";
                        }
                        else
                        {
                            res.PriceBookName = lstPriceBook[res.PriceBookId.Value];
                        }
                    }
                }
            }
            if (result.OmniChannelWareHouses != null && result.OmniChannelWareHouses.Any())
            {
                res.OmniChannelWareHouses = result.OmniChannelWareHouses.Map(x => x.ConvertTo<OmniChannelWareHouse>());
            }
            var cacheKey = string.Format(KvConstant.IsRunningGetProductKey, req.Id);
            if (CacheClient.Get<bool>(cacheKey))
            {
                res.IsRunningGetProduct = true;
            }
            return res;
        }
        #endregion

        #region DELETE methods
        public async Task<object> Delete(TikiAppGet req)
        {
            if (req.Id <= 0)
            {
                throw new KvValidateTikiException(KVMessage.NotFound);
            }
            var exist = await ChannelApi.Get(new GetById { Id = req.Id });
            if (exist == null)
            {
                throw new KvException(KVMessage.NotFound);
            }
            await ChannelApi.Delete(new GetById { Id = req.Id });
            return new { req.Id, Message = KVMessage.GlobalDeleteSuccess };
        }
        #endregion

        #region PRIVATE methods
        private async Task<bool> ValidateTikiConnection(TikiAppPost req)
        {
            if (req.TikiApp == null)
            {
                throw new KvValidateLazadaException(KVMessage.GlobalErrorSummary);
            }
            if (req.IsSyncPrice && req.TikiApp.BasePriceBookId == null)
            {
                throw new KvValidateLazadaException(KVMessage.LazadaEmptyBasePriceBook);
            }
            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            using (var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var branchRepository = new BranchRepository(db);
                var existBranch = await branchRepository.GetByIdAsync(req.TikiApp.BranchId);
                if (existBranch == null || (existBranch.LimitAccess ?? false))
                {
                    throw new KvValidateBranchException(KVMessage.LazadaEmptyBranch);
                }
            }
            if (!KvPosSetting.SellAllowOrder && (req.TikiApp.SyncOnHandFormula == (byte)EcommerceSellQtyFormula.SubOrder || req.TikiApp.SyncOnHandFormula == (byte)EcommerceSellQtyFormula.SubOrderAddSupplierOrder))
            {
                throw new KvValidateLazadaException(KVMessage.EcommNotEnableSellAllowOrder);
            }
            if (!KvPosSetting.UseOrderSupplier && (req.TikiApp.SyncOnHandFormula == (byte)EcommerceSellQtyFormula.AddSupplierOrder || req.TikiApp.SyncOnHandFormula == (byte)EcommerceSellQtyFormula.SubOrderAddSupplierOrder))
            {
                throw new KvValidateLazadaException(KVMessage.EcommNotEnableUseOrderSupplier);
            }
            return true;
        }
        #endregion

    }
}

﻿using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.EndPoints;
using Demo.OmniChannel.ChannelClient.Helper;
using Demo.OmniChannel.ChannelClient.Impls;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ChannelClient.ResponseDTO.Shopee;
using Demo.OmniChannel.CoreDemoContext.Interfaces;
using Demo.OmniChannel.ShareKernel.Dto;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Resource;
using Demo.OmniChannel.Sdk.Dtos;
using Demo.OmniChannel.Sdk.RequestDtos;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChannelCreateOrUpdate = Demo.OmniChannel.Api.ServiceModel.ChannelCreateOrUpdate;
using EnableChannel = Demo.OmniChannel.Api.ServiceModel.EnableChannel;
using GetById = Demo.OmniChannel.Api.ServiceModel.GetById;
using GetByRetailer = Demo.OmniChannel.Api.ServiceModel.GetByRetailer;
using LogisticsRequest = Demo.OmniChannel.Api.ServiceModel.LogisticsRequest;
using RemoveChannelAuth = Demo.OmniChannel.Api.ServiceModel.RemoveChannelAuth;
using ShopeeAuthConfigV2 = Demo.OmniChannel.Api.ServiceModel.ShopeeAuthConfigV2;
using ShopeeProfileAndUpdateAuth = Demo.OmniChannel.Api.ServiceModel.ShopeeProfileAndUpdateAuth;
using Demo.OmniChannel.Api.ServiceModel.Request;

namespace Demo.OmniChannel.Api.ServiceInterface
{
    public class ShopeeApi : BaseApi
    {

        public ChannelApi ChannelApi { get; set; }
        public LogisticsApi LogisticsApi { get; set; }

        private readonly IAppSettings _settings;
        private readonly ChannelClient.Impls.ChannelClient _channelClient;
        private readonly IOmniChannelAuthService _omniChannelAuthService;
        private readonly IOmniChannelPlatformService _omniChannelPlatformService;

        private readonly ISaleChannelInternalClient _saleChannelInternalClient;
        private readonly IDemoOmniChannelCoreContext _DemoOmniChannelCoreContext;
        private readonly IOmniChannelService _omniChannelService;

        public ShopeeApi(ILogger<ShopeeApi> logger) : base(logger)
        {
        }

        public ShopeeApi(ILogger<ShopeeApi> logger,
            IAppSettings settings,
            ChannelClient.Impls.ChannelClient channelClient,
            IOmniChannelAuthService omniChannelAuthService,
            IOmniChannelPlatformService omniChannelPlatformService,
            ISaleChannelInternalClient saleChannelInternalClient,
            IDemoOmniChannelCoreContext DemoOmniChannelCoreContext,
            IOmniChannelService OmniChannelService
            ) : base(logger)
        {
            _ = new SelfAppConfig(settings);
            _settings = settings;
            _channelClient = channelClient;
            _omniChannelAuthService = omniChannelAuthService;
            _omniChannelPlatformService = omniChannelPlatformService;
            _saleChannelInternalClient = saleChannelInternalClient;
            _DemoOmniChannelCoreContext = DemoOmniChannelCoreContext;
            _omniChannelService = OmniChannelService;
        }



        #region POST methods
        public async Task<object> Post(ShopeeEnableChannelV2 req)
        {
            var newRequest = new ServiceModel.ShopeeProfileAndToken
            {
                Code = req.Code,
                IdentityKey = req.IdentityKey,
                PlatformId = req.PlatformId,
                ShopId = req.ShopId,
                ShopName = req.ShopName
            };

            var getShopInfoAndToken = await Get(newRequest);

            var enableChannel = new EnableChannel
            {
                Auth = getShopInfoAndToken.Auth.ConvertTo<Domain.Model.OmniChannelAuth>(),
                BranchName = req.BranchName,
                ChannelId = req.ChannelId,
                PriceBookName = req.PriceBookName,
                SalePriceBookName = req.SalePriceBookName,
                Email = getShopInfoAndToken.Email,
                ShopName = req.ShopName,
                PlatformId = req.PlatformId
            };
            return await ChannelApi.Post(enableChannel);

        }
        public async Task<object> Post(ShopeeRemoveAuth req)
        {
            var removeChannelAuth = new RemoveChannelAuth
            {
                AuthId = req.AuthId
            };
            return await ChannelApi.Post(removeChannelAuth);
        }
        public async Task<ShopeeResponse> Post(ShopeeCreateOrUpdate req)
        {
            await ValidateShopeeConnection(req);
            req.ShopeeApp.RetailerId = CurrentRetailerId;
            req.ShopeeApp.Type = (byte)ChannelType.Shopee;
            var r = await ChannelApi.Post(new ChannelCreateOrUpdate()
            {
                AuthId = req.AuthId,
                IsIgnoreAuditTrail = false,
                IsSyncOnHand = req.IsSyncOnHand,
                IsSyncOrder = req.IsSyncOrder,
                IsSyncPrice = req.IsSyncPrice,
                Channel = req.ShopeeApp
            });
            var result = r.ConvertTo<ShopeeResponse>();
            if (!string.IsNullOrWhiteSpace(r.IdentityKey))
            {
                var coreContext = await _DemoOmniChannelCoreContext.CreateOmniChannelCoreContext(CurrentRetailerId, InternalContext.BranchId, InternalContext.Group.ConnectionString);
                result.SaleChannelId = await _saleChannelInternalClient.CreateOrUpdateSaleChannel(coreContext, CurrentRetailerId, (byte)ChannelType.Shopee, r.Id, r.Name);
            }
            return result;
        }
        #endregion


        #region GET methods

        public async Task<List<ShopeeResponse>> Get(ShopeeShopList req)
        {
            var result = new List<ShopeeResponse>();
            var newRequest = new GetByRetailer
            {
                Type = (byte)ChannelType.Shopee,
                OnlyIsActive = req.IsActive,
                RetailerId = InternalContext.RetailerId,
                BranchId = req.IncludeBranch ? InternalContext.BranchId : 0
            };
            var shopLst = await ChannelApi.Any(newRequest);
            if (shopLst != null && shopLst.Any())
            {
                result = shopLst.Select(p =>
                {
                    var item = p.ConvertTo<ShopeeResponse>();
                    item.Name = p.Name;
                    item.ShopId = Helpers.ToLong(p.IdentityKey, 0);
                    return item;
                }).ToList();
            }
            return result;
        }
        public async Task<ShopeeResponse> Get(ShopeeGet req)
        {
            var result = await ChannelApi.Get(new GetById
            {
                Id = req.Id
            });
            var res = result.ConvertTo<ShopeeResponse>();
            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1) ?? "";
            using (var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var branchRepository = new BranchRepository(db);

                var branch = await branchRepository.GetByIdAsync(res.BranchId ?? 0);
                if (branch == null)
                {
                    res.BranchId = -1;
                }
                res.BranchName = branch?.Name;
                if (result.OmniChannelSchedules != null && result.OmniChannelSchedules.Any())
                {
                    res.OmniChannelSchedules = result.OmniChannelSchedules.Map(x => x.ConvertTo<Demo.OmniChannel.Domain.Model.OmniChannelSchedule>());
                    res.IsSyncOnHand = result.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncOnhand);
                    res.IsSyncPrice = result.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncPrice);
                    res.IsSyncOrder = result.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncOrder);
                    res.SyncOnHandFormula = res.IsSyncOnHand ? result.SyncOnHandFormula : (byte)0;
                    res.SyncOrderFormula = result.SyncOrderFormula;
                }
            }
            var cacheKey = string.Format(KvConstant.IsRunningGetProductKey, req.Id);
            if (CacheClient.Get<bool>(cacheKey))
            {
                res.IsRunningGetProduct = true;
            }
            return res;
        }
        public async Task<object> Get(GetShopeeAttributes req)
        {
            var newReq = req.ConvertTo<ServiceModel.AttributesRequest>();
            return await LogisticsApi.Get(newReq);
        }
        public async Task<object> Get(GetShopeeLogistics req)
        {
            var logisticsRequest = new LogisticsRequest
            {
                ChannelIds = req.ChannelIds
            };
            return await LogisticsApi.Get(logisticsRequest);
        }
        public async Task<Domain.Common.PagingDataSource<ShopeeResponse>> Get(ShopeeApps req)
        {
            var result = new Domain.Common.PagingDataSource<ShopeeResponse>();
            var newReq = new GetByRetailer
            {
                OnlyIsActive = false,
                RetailerId = InternalContext.RetailerId,
                Type = (byte)Demo.OmniChannel.ShareKernel.Common.ChannelType.Shopee
            };
            var shopeeLst = await ChannelApi.Any(newReq);
            if (shopeeLst == null)
            {
                return result;
            }
            var shopee = shopeeLst.Select(p =>
            {
                var item = p.ConvertTo<ShopeeResponse>();
                item.TotalProductConnected = p.TotalProductConnected;
                item.Name = p.Name;
                item.IsExpireWarning = p.IsExpireWarning;
                item.OmniChannelSchedules = p.OmniChannelSchedules.Map(x => x.ConvertTo<Demo.OmniChannel.Domain.Model.OmniChannelSchedule>()).ToList();
                return item;
            }).ToList();
            var branchIds = shopee.Where(item => item.BranchId.HasValue).Select(p => p.BranchId.Value);
            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            using (var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var branchRepository = new BranchRepository(db);
                var lstBranch = (await branchRepository.GetListBranchByIds(CurrentRetailerId, branchIds.ToList()))
                    .ToDictionary(item => item.Id, item => item.Name);
                var basePriceBookIds = shopee.Where(item => item.BasePriceBookId.HasValue).Select(p => p.BasePriceBookId.Value);
                var priceBookRepository = new PriceBookRepository(db);
                var lstBasePriceBook = (await priceBookRepository.GetByFilterAsync(CurrentRetailerId, basePriceBookIds.ToList(), true))
                    .ToDictionary(item => item.Id, item => item.Name);
                shopee.ForEach(x =>
                {
                    x.BranchName = lstBranch.ContainsKey(x.BranchId ?? 0) ? lstBranch[x.BranchId ?? 0] : "";
                    if (x.BasePriceBookId.HasValue)
                    {
                        x.BasePriceBookName = lstBasePriceBook.ContainsKey(x.BasePriceBookId ?? 0) ? lstBasePriceBook[x.BasePriceBookId ?? 0] : "Bảng giá chung";
                    }
                    x.IsSyncPrice = x.OmniChannelSchedules.Any(y => y.Type == (byte)Sdk.Common.ScheduleType.SyncPrice);
                    x.IsSyncOnHand = x.OmniChannelSchedules.Any(y => y.Type == (byte)Sdk.Common.ScheduleType.SyncOnhand);
                    x.SyncOnHandFormulaName = x.IsSyncOnHand ? $"{KvMessage.ecommSellQty} = {EnumHelper.ToDescription((EcommerceSellQtyFormula)x.SyncOnHandFormula)}" : string.Empty;
                    x.IsSyncOrder = x.OmniChannelSchedules.Any(y => y.Type == (byte)Sdk.Common.ScheduleType.SyncOrder);
                });
                result.Data = shopee;
                result.Total = shopee.Count;
            }
            return result;
        }
        public async Task<object> Get(ShopeeAuthConfigV2 req)
        {
            var getConfig = await _omniChannelPlatformService.GetCurrent((int)PlatformType.Shopee);
            if (getConfig?.DataObject == null) throw new OmniException("Vui lòng liên hệ nhà cung cấp!");
            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var sign = ShopeeHelperV2.GenerateSignature(getConfig.DataObject.PartnerId,
                getConfig.DataObject.Token,
                timestamp, string.Empty, null, ShopeeEndpointV2.AuthPartner);
            return new
            {
                getConfig.DataObject.PartnerId,
                RedirectUrl = SelfAppConfig.ShopeeCallBack,
                Sign = sign,
                TimeStamp = timestamp,
                PlatformId = getConfig.Id,
                OAuthURL = $"{SelfAppConfig.ShopeeApiEndpointV2}/{ShopeeEndpointV2.AuthPartner}"
            };
        }

        public async Task<ShopInfoResponseV2> Get(ShopeeProfileAndUpdateAuth req)
        {
            var client = _channelClient.GetClientV2((int)ChannelTypeEnum.Shopee, _settings);
            var getConfig = await _omniChannelPlatformService.GetById(req.PlatformId);
            var getToken = await client.CreateToken(req.Code, new ChannelAuth { ShopId = req.ShopId }, getConfig);

            var result = await _omniChannelAuthService.CreateOrUpdate(new Domain.Model.OmniChannelAuth
            {
                ShopId = getToken.ShopId,
                AccessToken = getToken.AccessToken,
                RefreshToken = getToken.RefreshToken,
                ExpiresIn = getToken.ExpiresIn,
                RefreshExpiresIn = getToken.RefreshExpiresIn
            });

            var auth = new ChannelAuth
            {
                AccessToken = getToken.AccessToken,
                ShopId = getToken.ShopId
            };

            var getShopInfo = await client.GetShopInfo(auth, null, false, getConfig);

            return new ShopInfoResponseV2
            {
                AuthId = result,
                Name = getShopInfo.Name,
                IdentityKey = getShopInfo.IdentityKey
            };
        }

        public async Task<ProfileAndTokenResp> Get(ServiceModel.ShopeeProfileAndToken req)
        {
            var client = _channelClient.GetClientV2((int)ChannelTypeEnum.Shopee, _settings);
            var getConfig = await _omniChannelPlatformService.GetById(req.PlatformId);
            var getToken = await client.CreateToken(req.Code, new ChannelAuth { ShopId = req.ShopId }, getConfig);
            if (getToken == null) throw new OmniException("Có lỗi xảy ra. Vui lòng thử lại trong ít phút.");

            var getShopInfo = await client.GetShopInfo(
                new ChannelAuth { ShopId = getToken.ShopId, AccessToken = getToken.AccessToken }, null, true,
                getConfig);
            if (getShopInfo == null) throw new OmniException("Có lỗi xảy ra. Vui lòng thử lại trong ít phút.");
            if (string.IsNullOrEmpty(req.IdentityKey) ||
                !req.IdentityKey.Equals(getShopInfo.IdentityKey, StringComparison.OrdinalIgnoreCase))
            {
                throw new OmniException(
                    $"Tài khoản kết nối lại không phải tài khoản {req.ShopName}. Xin vui lòng thử lại.");
            }

            return new ProfileAndTokenResp
            {
                Name = getShopInfo.Name,
                Email = getShopInfo.Email,
                Auth = new ChannelAuthRequest
                {
                    AccessToken = getToken.AccessToken,
                    RefreshToken = getToken.RefreshToken,
                    ExpiresIn = getToken.ExpiresIn,
                    RefreshExpiresIn = getToken.RefreshExpiresIn,
                    ShopId = getToken.ShopId
                }
            };
        }

        public async Task<object> Get(GetBrandReq req)
        {
            var channel = await _omniChannelService.GetByIdAsync(req.ChannelId);
            var client = new ShopeeClientV2(AppSettings);

            var platforms =
                await _omniChannelPlatformService.GetById(channel.PlatformId);
            var authData = await _omniChannelAuthService.GetChannelAuth(channel, Guid.NewGuid());
            var auth = new ChannelAuth { ShopId = Convert.ToInt64(channel.IdentityKey), AccessToken = authData.AccessToken };

            var brachs = await client.GetBrands(req.CategoryId, auth,
                Guid.NewGuid(), platforms, req.NextOffSet);
            return new
            {
                HasNextPage = brachs.hasNextPage,
                NextOfSet = brachs.nextOffSet,
                ListBrand = brachs.brandList
            };
        }

        #endregion


        #region DELETE methods
        public async Task<object> Delete(ShopeeGet req)
        {
            if (req.Id <= 0)
            {
                throw new KvValidateShopeeException(KVMessage.NotFound);
            }
            var exist = await ChannelApi.Get(new GetById { Id = req.Id });
            if (exist == null)
            {
                throw new KvException(KVMessage.NotFound);
            }
            await ChannelApi.Delete(new GetById { Id = req.Id });
            return new { req.Id, Message = KVMessage.GlobalDeleteSuccess };
        }
        #endregion


        #region PRIVATE method 


        private async Task<bool> ValidateShopeeConnection(ShopeeCreateOrUpdate req)
        {
            if (req.ShopeeApp == null)
            {
                throw new KvValidateShopeeException(KVMessage.GlobalErrorSummary);
            }

            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            using (var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var branchRepository = new BranchRepository(db);
                var existBranch = await branchRepository.GetByIdAsync(req.ShopeeApp.BranchId);
                if (existBranch == null || (existBranch.LimitAccess ?? false))
                {
                    throw new KvValidateShopeeException(KVMessage.ShopeeEmptyBranch);
                }
            }
            if (req.IsSyncPrice && req.ShopeeApp.BasePriceBookId == null)
            {
                throw new KvValidateShopeeException(KVMessage.ShopeeEmptyBasePriceBook);
            }
            if (!KvPosSetting.SellAllowOrder && (req.ShopeeApp.SyncOnHandFormula == (byte)EcommerceSellQtyFormula.SubOrder || req.ShopeeApp.SyncOnHandFormula == (byte)EcommerceSellQtyFormula.SubOrderAddSupplierOrder))
            {
                throw new KvValidateLazadaException(KVMessage.EcommNotEnableSellAllowOrder);
            }
            if (!KvPosSetting.UseOrderSupplier && (req.ShopeeApp.SyncOnHandFormula == (byte)EcommerceSellQtyFormula.AddSupplierOrder || req.ShopeeApp.SyncOnHandFormula == (byte)EcommerceSellQtyFormula.SubOrderAddSupplierOrder))
            {
                throw new KvValidateLazadaException(KVMessage.EcommNotEnableUseOrderSupplier);
            }
            return true;
        }

        #endregion
    }
}

﻿using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.Api.ServiceModel.Retailer;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.CoreDemoContext.Interfaces;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.ShareKernel.Dto;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Resource;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.OrmLite;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Api.ServiceModel.Request;

namespace Demo.OmniChannel.Api.ServiceInterface.Retailer
{
    public class TiktokRetailerApi : BaseApi
    {
        public ISaleChannelInternalClient SaleChannelInternalClient { get; set; }
        public IDemoOmniChannelCoreContext DemoOmniChannelCoreContext { get; set; }

        public TiktokRetailerApi(ILogger<ChannelRetailerApi> logger) : base(logger)
        {
        }

        public async Task<PagingDataSource<TiktokResponse>> Get(TiktokAppByRetail _)
        {
            var retailerId = CurrentRetailerId;
            var response = new PagingDataSource<TiktokResponse>();

            var getByRetailerReq = new GetByRetailer
            {
                RetailerId = retailerId,
                Type = (byte)ChannelType.Tiktok,
                OnlyIsActive = false,
            };
            var tiktokDatas = await HostContext.ResolveService<ChannelApi>(Request).Any(getByRetailerReq);
            if (tiktokDatas == null)
            {
                return response;
            }
            var tiktoks = tiktokDatas.Select(p =>
            {
                var item = p.ConvertTo<TiktokResponse>();
                item.Name = p.Name;
                item.TotalProductConnected = p.TotalProductConnected;
                item.OmniChannelSchedules = p.OmniChannelSchedules.Map(x => x.ConvertTo<OmniChannelSchedule>()).ToList();
                item.IsExpireWarning = p.IsExpireWarning;
                return item;
            }).ToList();

            var connectionString = GetKvEntitiesConnectString();
            using (var db = await DbConnectionFactory.OpenAsync(connectionString))
            {
                var branchIds = tiktoks.Select(p => p.BranchId ?? 0).ToList();
                var branchRepo = new BranchRepository(db);
                var lstBranch = await branchRepo.GetListBranchByIds(retailerId, branchIds);
                var branches = lstBranch.ToDictionary(x => x.Id, y => y.Name);

                var priceBookIds = tiktoks.Select(p => p.BasePriceBookId ?? 0).ToList();
                var priceBookRepo = new PriceBookRepository(db);
                var priceBooks =
                    (await priceBookRepo.GetByFilterAsync(retailerId, priceBookIds, true)).ToDictionary(x => x.Id,
                        y => y.Name);
                tiktoks.ForEach(x =>
                {
                    x.IsSyncPrice = x.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncPrice);
                    x.IsSyncOnHand = x.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncOnhand);
                    x.SyncOnHandFormulaName = x.IsSyncOnHand ? $"{KvMessage.ecommSellQty} = {EnumHelper.ToDescription((EcommerceSellQtyFormula)x.SyncOnHandFormula)}" : string.Empty;
                    x.IsSyncOrder = x.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncOrder);
                    x.BranchName = branches.ContainsKey(x.BranchId ?? 0) ? branches[x.BranchId ?? 0] : "";
                    if (x.BasePriceBookId.HasValue)
                    {
                        x.BasePriceBookName = priceBooks.ContainsKey(x.BasePriceBookId ?? 0) ? priceBooks[x.BasePriceBookId ?? 0] : "Bảng giá chung";
                    }
                });
                response.Data = tiktoks;
                response.Total = tiktoks.Count;
                return response;
            }
        }

        public async Task<TiktokResponse> Get(TiktokGetByRetail req)
        {
            var result = await HostContext.ResolveService<ChannelApi>(Request).Get(new GetById
            {
                Id = req.Id
            });
            var res = result?.ConvertTo<TiktokResponse>() ?? new TiktokResponse();

            var connectionString = GetKvEntitiesConnectString();
            using (var db = await DbConnectionFactory.OpenAsync(connectionString))
            {
                var branchRepo = new BranchRepository(db);
                var branch = await branchRepo.GetByIdAsync(res.BranchId);
                res.BranchName = branch?.Name;
                if (result?.OmniChannelSchedules != null && result.OmniChannelSchedules.Any())
                {
                    res.OmniChannelSchedules = result.OmniChannelSchedules.Map(x => x.ConvertTo<OmniChannelSchedule>());
                    res.IsSyncOnHand = result.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncOnhand && y.OmniChannelId == req.Id);
                    res.IsSyncPrice = result.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncPrice && y.OmniChannelId == req.Id);
                    res.IsSyncOrder = result.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncOrder && y.OmniChannelId == req.Id);
                    res.SyncOnHandFormula = res.IsSyncOnHand ? result.SyncOnHandFormula : (byte)0;
                    res.SyncOrderFormula = result.SyncOrderFormula;
                    res.TotalProductConnected = result.TotalProductConnected;
                }
                var cacheKey = string.Format(KvConstant.IsRunningGetProductKey, req.Id);
                if (CacheClient.Get<bool>(cacheKey))
                {
                    res.IsRunningGetProduct = true;
                }
                return res;
            }
        }

        public async Task<object> Post(TiktokEnableChannelByRetail req)
        {
            var tiktokProfileAndTokenReq = new TikTokEnableChannel
            {
                Code = req.Code,
                IdentityKey = req.IdentityKey,
                PlatformId = req.PlatformId,
                ShopName = req.ShopName
            };
            var shopInfo = await HostContext.ResolveService<TikTokApi>(Request).Get(tiktokProfileAndTokenReq);

            var enableChannelReq = new EnableChannel
            {
                ChannelId = req.ChannelId,
                Auth = shopInfo.Auth.ConvertTo<OmniChannelAuth>(),
                BranchName = req.BranchName,
                PriceBookName = req.PriceBookName,
                SalePriceBookName = req.SalePriceBookName,
                ShopName = shopInfo.Name,
                Email = shopInfo.Email,
                PlatformId = req.PlatformId,
            };
            await HostContext.ResolveService<ChannelApi>(Request).Post(enableChannelReq);

            return true;

        }

        public async Task<object> Post(TiktokCreateOrUpdateByRetail req)
        {
            req.TiktokApp.RetailerId = CurrentRetailerId;
            req.TiktokApp.Type = (byte)ChannelType.Tiktok;

            var createOrUpdateReq = new ChannelCreateOrUpdate
            {
                Channel = req.TiktokApp,
                IsSyncPrice = req.IsSyncPrice,
                IsSyncOnHand = req.IsSyncOnHand,
                IsSyncOrder = req.IsSyncOrder,
                AuthId = req.AuthId,
                IsIgnoreAuditTrail = false,
            };

            var retval = await HostContext.ResolveService<ChannelApi>(Request).Post(createOrUpdateReq);
            var result = retval.ConvertTo<TiktokResponse>();

            if (!string.IsNullOrWhiteSpace(retval.IdentityKey))
            {
                var coreContext = await DemoOmniChannelCoreContext.CreateOmniChannelCoreContext(CurrentRetailerId, InternalContext.BranchId, InternalContext.Group.ConnectionString);
                result.SaleChannelId = await SaleChannelInternalClient.CreateOrUpdateSaleChannel(coreContext, CurrentRetailerId, (byte)ChannelType.Tiktok, retval.Id, retval.Name);
            }

            return result;
        }

        public async Task<object> Delete(TiktokGetByRetail req)
        {
            var deleteByIdReq = new GetById
            {
                Id = req.Id
            };
            var res = await HostContext.ResolveService<ChannelApi>(Request).Delete(deleteByIdReq);

            if (res == null) throw new KvTiktokException(KVMessage.NotFound);

            return new { req.Id, Message = KVMessage.GlobalDeleteSuccess };
        }
    }
}

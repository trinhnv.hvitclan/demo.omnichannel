﻿using Demo.OmniChannel.Api.ServiceModel.Retailer;
using Demo.OmniChannel.ChannelClient.Models;
using Microsoft.Extensions.Logging;
using ServiceStack;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Api.ServiceInterface.Retailer
{
    public class ChannelRetailerApi : BaseApi
    {
        public ChannelApi ChannelApi { get; set; }

        public ChannelRetailerApi(ILogger<ChannelRetailerApi> logger) : base(logger)
        {
        }

        public async Task<CategoriesResp> Get(GetCategoriesReqByRetail req)
        {
            var newReq = req.ConvertTo<ServiceModel.GetCategoriesReq>();
            return await ChannelApi.Get(newReq);
        }
    }
}

﻿using Demo.ElasticSearch.Sdk.Dtos;
using Demo.ElasticSearch.Sdk.Interfaces;
using Demo.Mapping.Sdk.Interfaces;
using Demo.OmniChannel.Api.ServiceInterface.Utils;
using Demo.OmniChannel.Api.ServiceInterface.Validators;
using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.Api.ServiceModel.Retailer;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.FeatureToggle;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Resource;
using Demo.OmniChannel.Sdk.RequestDtos;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Dto;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.OrmLite;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using AddProductAndMapping = Demo.OmniChannel.Api.ServiceModel.AddProductAndMapping;
using AddProductMapping = Demo.OmniChannel.Api.ServiceModel.AddProductMapping;
using AddProductMappingByRetail = Demo.OmniChannel.Api.ServiceModel.Retailer.AddProductMappingByRetail;
using ChannelType = Demo.OmniChannel.Sdk.Common.ChannelType;
using DeleteProductMapping = Demo.OmniChannel.Api.ServiceModel.DeleteProductMapping;
using DeleteProductMappingById = Demo.OmniChannel.Api.ServiceModel.DeleteProductMappingById;
using GetById = Demo.OmniChannel.Api.ServiceModel.GetById;
using GetByRetailer = Demo.OmniChannel.Api.ServiceModel.GetByRetailer;
using GetChannelExpiry = Demo.OmniChannel.Api.ServiceModel.GetChannelExpiry;
using GetInvoiceById = Demo.OmniChannel.Api.ServiceModel.GetInvoiceById;
using GetInvoiceSyncError = Demo.OmniChannel.Api.ServiceModel.GetInvoiceSyncError;
using GetOrderSyncError = Demo.OmniChannel.Api.ServiceModel.GetOrderSyncError;
using GetProductByChannel = Demo.OmniChannel.Api.ServiceModel.GetProductByChannel;
using GetProductByChannelId = Demo.OmniChannel.Api.ServiceModel.GetProductByChannelId;
using GetProductByKvProductIds = Demo.OmniChannel.Api.ServiceModel.GetProductByKvProductIds;
using GetProductBySearch = Demo.OmniChannel.Api.ServiceModel.GetProductBySearch;
using GetProductByTrackingKey = Demo.OmniChannel.Api.ServiceModel.GetProductByTrackingKey;
using GetProductDetail = Demo.OmniChannel.Api.ServiceModel.GetProductDetail;
using GetProductMapping = Demo.OmniChannel.Api.ServiceModel.GetProductMapping;
using GetProductMappingForward = Demo.OmniChannel.Api.ServiceModel.GetProductMappingForward;
using GetProductSyncError = Demo.OmniChannel.Api.ServiceModel.GetProductSyncError;
using GetQuantityOfProductSyncError = Demo.OmniChannel.Api.ServiceModel.GetQuantityOfProductSyncError;
using GetSaleChannelOmniByType = Demo.OmniChannel.Api.ServiceModel.GetSaleChannelOmniByType;
using KvValidateException = Demo.OmniChannel.ShareKernel.Exceptions.KvValidateException;
using MultiSyncErrorInvoice = Demo.OmniChannel.Api.ServiceModel.MultiSyncErrorInvoice;
using MultiSyncErrorOrder = Demo.OmniChannel.Api.ServiceModel.MultiSyncErrorOrder;
using OmniChannelExpiryResponse = Demo.OmniChannel.Sdk.RequestDtos.OmniChannelExpiryResponse;
using Order = Demo.OmniChannel.Sdk.RequestDtos.Order;
using PriceBook = Demo.OmniChannel.Domain.Model.PriceBook;
using ProductBatchExpiresInfo = Demo.OmniChannel.Api.ServiceModel.ProductBatchExpiresInfo;
using ScheduleType = Demo.OmniChannel.Sdk.Common.ScheduleType;
using SelfAppConfig = Demo.OmniChannel.ChannelClient.Common.SelfAppConfig;
using SyncErrorChannel = Demo.OmniChannel.Api.ServiceModel.SyncErrorChannel;
using TotalInvoiceError = Demo.OmniChannel.Api.ServiceModel.TotalInvoiceError;
using TotalOrderError = Demo.OmniChannel.Api.ServiceModel.TotalOrderError;

namespace Demo.OmniChannel.Api.ServiceInterface.Retailer
{
    public class OmniChannelApi : BaseApi
    {
        public IProductMappingService ProductMappingService { get; set; }
        public IProductMongoService ProductMongoService { get; set; }
        public IMappingInternalClient MappingInternalClient { get; set; }
        public IOmniChannelService OmniChannelService { get; set; }
        public IProductSearchClient ProductSearchClient { get; set; }

        public OmniChannelApi(ILogger<OmniChannelApi> logger) : base(logger)
        {
            JsConfig.EmitCamelCaseNames = false;
        }

        public async Task<GetPermissionResponse> Get(GetPermissionRequestByRetail req)
        {
            var newReq = req.ConvertTo<GetPermissionRequest>();
            return await HostContext.ResolveService<LicenseApi>(Request).Get(newReq);
        }

        public async Task<GetPermissionAddChannelResponse> Get(GetAddChannelPrecheckByRetail req)
        {
            var newReq = req.ConvertTo<GetPermissionAddChannelRequest>();
            return await HostContext.ResolveService<LicenseApi>(Request).Get(newReq);
        }

        public async Task<object> Get(GetWarningExpiryOmniChannelsByRetail req)
        {
            if (!KvPosSetting.ManagerCustomerByBranch)
            {
                return new OmniChannelExpiryDto();
            }
            var newReq = req.ConvertTo<GetChannelExpiry>();
            var expiChannels = (await HostContext.ResolveService<ChannelApi>(Request).Get(newReq)).ToSafeJson().FromJson<List<OmniChannelExpiryResponse>>();
            if (expiChannels?.Any() != true)
            {
                return new OmniChannelExpiryDto();
            }

            var types = expiChannels.Select(x => x.ChannelType).Distinct();

            var expiredChannels = new List<OmniChannelExpireWarning>();
            var willExpireChannels = new List<OmniChannelExpireWarning>();

            foreach (var type in types)
            {
                //Lấy danh sách sắp hết hạn
                var channelAboutToExpire = expiChannels.Where(x =>
                    x.IsActive && x.ChannelType == type && x.ExpiryDate.Subtract(DateTime.Now).Days >= 0).ToList();
                //Lấy danh sách channel đã quá hạn
                var channelToExpired = expiChannels.Where(x =>
                    x.IsActive && x.ChannelType == type && x.ExpiryDate.Subtract(DateTime.Now).Days < 0).ToList();
                if (channelAboutToExpire.Count > 0)
                {
                    var expDate = channelAboutToExpire
                        .OrderByDescending(x => x.ExpiryDate).First().ExpiryDate;

                    var grp = new OmniChannelExpireWarning
                    {
                        Type = GetChannelTypeName(type),
                        Total = channelAboutToExpire.Count,
                        ExpiredAfterDay = expDate.Subtract(DateTime.Now).Days,
                        IsExpired = false
                    };
                    willExpireChannels.Add(grp);
                }

                if (channelToExpired.Count > 0)
                {
                    var expDate = channelToExpired.OrderBy(x => x.ExpiryDate).First().ExpiryDate;

                    var grp = new OmniChannelExpireWarning
                    {
                        Type = GetChannelTypeName(type),
                        Total = channelToExpired.Count,
                        ExpiredAfterDay = expDate.Subtract(DateTime.Now).Days,
                        IsExpired = false
                    };
                    willExpireChannels.Add(grp);
                }


                var expiredTotal = expiChannels.Count(x => !x.IsActive && x.ChannelType == type);
                if (expiredTotal <= 0) continue;
                var expiredWarning = new OmniChannelExpireWarning
                {
                    Type = GetChannelTypeName(type),
                    Total = expiredTotal,
                    IsExpired = true

                };
                expiredChannels.Add(expiredWarning);
            }

            return new OmniChannelExpiryDto
            {
                ExpiredChannels = expiredChannels,
                ChannelListIsAboutToExpire = willExpireChannels
            };
        }

        public async Task<object> Get(GetQuantityOfProductSyncErrorByRetail req)
        {
            var getByRetailerReq = new GetByRetailer
            {
                RetailerId = CurrentRetailerId,
                Type = req.ChannelType,
                OnlyIsActive = true,
            };

            var channels = await HostContext.ResolveService<ChannelApi>(Request).Any(getByRetailerReq);
            var filterChannelIds = channels.Select(x => x.Id).ToList();

            var getQuantityOfProductSyncErrorReq = new GetQuantityOfProductSyncError
            {
                ChannelIds = filterChannelIds
            };

            var result = await HostContext.ResolveService<ChannelProductApi>(Request)
                .Any(getQuantityOfProductSyncErrorReq);
            return new { Quantity = result };
        }

        public async Task<object> Get(GetChannelProductDetailByRetail req)
        {
            var getProductDetailReq = new GetProductDetail
            {
                ChannelId = req.ChannelId,
                ProductId = req.ProductId,
                ProductSku = req.ProductSku,
                ParentProductId = req.ParentProductId
            };

            var result = await HostContext.ResolveService<ChannelProductApi>(Request).Any(getProductDetailReq);
            return result;
        }

        public async Task<object> Get(GetChannelOrderSyncErrorByRetail req)
        {
            var logInfo = new LogObjectMicrosoftExtension(Logger, Guid.NewGuid())
            {
                Action = "GetChannelOrderSyncErrorByRetail",
                RetailerId = InternalContext.RetailerId,
                RequestObject = req.ToJson()
            };
            var retailerId = CurrentRetailerId;
            var channels = await GetChannelsByFilter(retailerId, req.ChannelType, ChannelTypeHelper.GetChannelTypes(req.ChannelTypes), req.ChannelIds);

            var filterChannelIds = req.ChannelIds == null ? channels.Select(x => x.Id).ToList()
                : req.ChannelIds.Where(x => channels.Any(y => y.Id == x)).ToList();

            var getOrderSyncErrorReq = new GetOrderSyncError
            {
                RetailerId = retailerId,
                ChannelIds = filterChannelIds,
                BranchIds = req.BranchIds,
                Skip = req.Skip,
                Top = req.Top,
                SearchTerm = req.OtherSearchTerm,
                OrderCode = req.OrderCode,
                KeySearchProduct = req.KeySearchProduct,
            };
            var response = (await HostContext.ResolveService<OrderApi>(Request).Any(getOrderSyncErrorReq))
                .ToSafeJson().FromJson<Sdk.Common.PagingDataSource<Order>>();

            if (response == null || response.Total <= 0)
            {
                return new SyncErrorResponse<OrderDto>();
            }

            var retVal = new SyncErrorResponse<OrderDto>
            {
                Data = response.Data.Select(x => x.ToSafeJson().FromJson<OrderDto>()).ToList(),
                Total = response.Total
            };
            logInfo.Description = "Begin execute SQL";
            logInfo.LogInfo();
            var connectionString = GetKvEntitiesConnectString();
            using (var db = await DbConnectionFactory.OpenAsync(connectionString))
            {
                var productImageRepo = new ProductImageRepository(db);
                var productIdsAll = retVal.Data.SelectMany(x => x.OrderDetails).Select(x => x.ProductId).Distinct().ToList();
                var productImages = await productImageRepo.GetProductImagesByProductIds(productIdsAll, retailerId);

                var branchRepo = new BranchRepository(db);
                var branchIds = response.Data.Select(x => x.BranchId).Distinct().ToList();
                var branches = await branchRepo.GetListBranchByIds(retailerId, branchIds);

                var detailHasBatchExpireds = retVal.Data.Where(x => x.OrderDetails.Any(p => p.UseProductBatchExpire))
                    .SelectMany(x => x.OrderDetails).ToList();
                var productIds = detailHasBatchExpireds.Select(p => p.ProductId).Distinct().ToList();
                List<ProductBatchExpiresInfo> productBatchExpireInfo = null;
                if (productIds.Any())
                {
                    var productBatchExpireRepo = new ProductBatchExpireRepository(db);
                    var productBatchExpire = await productBatchExpireRepo.GetListByProductIds(retailerId, productIds);

                    var productBatchExpireBranchRepo = new ProductBatchExpireBranchRepository(db);
                    var lsBatchExpireId = productBatchExpire.Select(a => a.Id).ToList();
                    var productBatchExpireBranches =
                        await productBatchExpireBranchRepo.GetListByBatchExpireIdsAndOnhand(retailerId,
                            CurrentBranchId, lsBatchExpireId);
                    productBatchExpireInfo = (from o in productBatchExpire
                                              join o1 in productBatchExpireBranches on o.Id equals o1.ProductBatchExpireId
                                              select new ProductBatchExpiresInfo
                                              {
                                                  Id = o.Id,
                                                  ProductId = o.ProductId,
                                                  OnHand = Math.Round(o1.OnHand, 3),
                                                  Status = o1.Status,
                                                  BatchName = o.BatchName,
                                                  ExpireDate = o.ExpireDate,
                                                  FullNameVirgule = o.FullNameVirgule
                                              }).ToList();
                }

                var detailHasEmeis = retVal.Data.Where(p => p.OrderDetails.Any(x => x.UseProductSerial)).SelectMany(x => x.OrderDetails).ToList();
                var productEmeiIds = detailHasEmeis.Select(x => x.ProductId).ToList();
                var productImeisDic = new Dictionary<long, IEnumerable<string>>();
                if (productEmeiIds.Any())
                {
                    var productRepo = new ProductRepository(db);
                    var productIdsHashSet = new HashSet<long>(productEmeiIds);
                    var productImeis = await productRepo.GetByIds(retailerId, productIdsHashSet);

                    var productIdsByImeis = productImeis.Select(x => x.Id).ToList();
                    var productSerialRepo = new ProductSerialRepository(db);
                    var productSerials = await productSerialRepo.GetListByProductIds(retailerId, productEmeiIds);

                    foreach (var productId in productIdsByImeis)
                    {
                        productImeisDic[productId] = productSerials.Where(x => x.ProductId == productId)
                            .Select(x => x.SerialNumber);
                    }
                }

                var customerRepo = new CustomerRepository(db);
                var customerIds = retVal.Data.Where(p => p.CustomerId > 0).Select(p => p.CustomerId).Distinct().ToList();
                var customers = await customerRepo.GetListByIds(retailerId, customerIds);
                foreach (var item in retVal.Data)
                {
                    item.ChannelName = channels.FirstOrDefault(p => p.Id == item.ChannelId)?.Name;
                    item.SaleChannelId = (int)(channels.FirstOrDefault(p => p.Id == item.ChannelId)?.Id ?? 0);
                    item.BranchName = branches.FirstOrDefault(x => x.Id == item.BranchId)?.Name;
                    item.PriceBookId = channels.FirstOrDefault(p => p.Id == item.ChannelId)?.BasePriceBookId ?? 0;
                    item.ErrorMappingProductIDs = GetErrorMappingIdsFromMessage<OrderDto>(item);

                    var customer = customers.FirstOrDefault(x => x.Id == item.CustomerId);
                    if (customer != null)
                    {
                        item.CustomerCode = customer.Code;
                        item.CustomerName = customer.Name;
                        item.CustomerPhone = customer.ContactNumber;
                    }
                    foreach (var detail in item.OrderDetails)
                    {
                        detail.ProductImages = productImages.Where(x => x.ProductId == detail.ProductId).Select(x => x.Image).ToList();
                        if (detail.UseProductBatchExpire && productBatchExpireInfo != null)
                        {
                            detail.ProductBatchExpires = productBatchExpireInfo.Where(x => x.ProductId == detail.ProductId).ToList();
                        }
                        if (detail.UseProductSerial && productImeisDic.ContainsKey(detail.ProductId))
                        {
                            detail.ProductSerials = productImeisDic[detail.ProductId].ToList();
                        }
                    }
                }
                logInfo.LogInfo(true);
                return retVal;
            }
        }

        public async Task<object> Get(GetChannelInvoiceSyncErrorByRetail req)
        {
            var logInfo = new LogObjectMicrosoftExtension(Logger, Guid.NewGuid())
            {
                Action = "GetChannelInvoiceSyncErrorByRetail",
                RetailerId = InternalContext.RetailerId,
                RequestObject = req.ToJson()
            };
            var retailerId = CurrentRetailerId;

            var channels = await GetChannelsByFilter(retailerId, req.ChannelType, ChannelTypeHelper.GetChannelTypes(req.ChannelTypes), req.ChannelIds);

            var filterChannelIds = req.ChannelIds == null ? channels.Select(x => x.Id).ToList() : req.ChannelIds.Where(x => channels.Any(y => y.Id == x)).ToList();

            var getInvoiceSyncErrorReq = new GetInvoiceSyncError
            {
                RetailerId = retailerId,
                ChannelIds = filterChannelIds,
                BranchIds = req.BranchIds,
                Skip = req.Skip,
                Top = req.Top,
                SearchTerm = req.OtherSearchTerm,
                InvoiceCode = req.InvoiceCode,
                KeySearchProduct = req.KeySearchProduct,
            };
            var retVal = (await HostContext.ResolveService<InvoiceApi>(Request).Any(getInvoiceSyncErrorReq))
                .ToSafeJson().FromJson<Sdk.Common.PagingDataSource<InvoiceDto>>();
            logInfo.Description = "Begin execute SQL";
            logInfo.LogInfo();
            var connectionString = GetKvEntitiesConnectString();
            using (var db = await DbConnectionFactory.OpenAsync(connectionString))
            {
                var productImageRepo = new ProductImageRepository(db);
                var productIdsAll = retVal.Data.SelectMany(x => x.InvoiceDetails).Select(x => x.ProductId).Distinct().ToList();
                var productImages = await productImageRepo.GetProductImagesByProductIds(productIdsAll, retailerId);

                var branchRepo = new BranchRepository(db);
                var branchIds = retVal.Data.Select(x => x.BranchId).ToList();
                var branches = await branchRepo.GetListBranchByIds(retailerId, branchIds);

                var detailHasBatchExpireds = retVal.Data.Where(x => x.InvoiceDetails.Any(p => p.UseProductBatchExpire))
                    .SelectMany(x => x.InvoiceDetails).ToList();
                var productIds = detailHasBatchExpireds.Select(p => p.ProductId).ToList();

                List<ProductBatchExpiresInfo> productBatchExpireInfo = null;
                var customerIds = retVal.Data.Where(p => p.CustomerId > 0).Select(p => p.CustomerId ?? 0).Distinct().ToList();

                var customerRepo = new CustomerRepository(db);
                var customers = await customerRepo.GetListByIds(retailerId, customerIds);
                List<MasterProduct> masterIds = new List<MasterProduct>();
                if (productIds.Any())
                {
                    var productRepo = new ProductRepository(db);
                    var masterProducts = await productRepo.GetListByIds(retailerId, productIds);
                    var masterUnitIds = masterProducts.Where(p => p.MasterUnitId > 0).Select(p => new
                    {
                        p.Id,
                        MasterProductId = p.MasterUnitId.GetValueOrDefault(),
                        p.ConversionValue
                    }).ToList();
                    if (masterUnitIds.Any())
                    {
                        foreach (var unit in masterUnitIds)
                        {
                            masterIds.Add(new MasterProduct { MasterProductId = unit.MasterProductId, ProductId = unit.Id, ConversionValue = unit.ConversionValue });
                        }
                        productIds = productIds.Concat(masterUnitIds.Select(p => p.MasterProductId)).ToList();
                    }

                    var productBatchExpireRepo = new ProductBatchExpireRepository(db);
                    var productBatchExpire = await productBatchExpireRepo.GetListByProductIds(retailerId, productIds);
                    var lsBatchExpireId =
                        productBatchExpire.Select(a => a.Id);

                    var productBatchExpireBranchRepo = new ProductBatchExpireBranchRepository(db);
                    var productBatchExpireBranches = await productBatchExpireBranchRepo.GetListByBatchExpireIdsAndOnhand(retailerId, CurrentBranchId,
                            lsBatchExpireId.ToList());
                    productBatchExpireInfo = (from o in productBatchExpire
                                              join o1 in productBatchExpireBranches on o.Id equals o1.ProductBatchExpireId
                                              select new ProductBatchExpiresInfo
                                              {
                                                  Id = o.Id,
                                                  ProductId = o.ProductId,
                                                  OnHand = Math.Round(o1.OnHand, 3),
                                                  Status = o1.Status,
                                                  BatchName = o.BatchName,
                                                  ExpireDate = o.ExpireDate,
                                                  FullNameVirgule = o.FullNameVirgule,
                                                  BranchId = o1.BranchId,
                                              }).ToList();
                }

                var detailHasEmeis = retVal.Data.Where(p => p.InvoiceDetails.Any(x => x.UseProductSerial)).SelectMany(x => x.InvoiceDetails).ToList();
                var productEmeiIds = detailHasEmeis.Select(x => x.ProductId).ToList();
                List<ProductSerial> productSerials = new List<ProductSerial>();
                if (productEmeiIds.Any())
                {
                    var productSerialRepo = new ProductSerialRepository(db);
                    productSerials = await productSerialRepo.GetListByProductIds(retailerId, productEmeiIds);
                }
                foreach (var item in retVal.Data)
                {
                    item.ChannelName = channels.FirstOrDefault(p => p.Id == item.ChannelId)?.Name;
                    item.SaleChannelId = (int)(channels.FirstOrDefault(p => p.Id == item.ChannelId)?.Id ?? 0);
                    item.BranchName = branches.FirstOrDefault(x => x.Id == item.BranchId)?.Name;
                    item.PriceBookId = channels.FirstOrDefault(p => p.Id == item.ChannelId)?.BasePriceBookId ?? 0;
                    item.ErrorMappingProductIDs = GetErrorMappingIdsFromMessage<InvoiceDto>(item);

                    var customer = customers.FirstOrDefault(x => x.Id == item.CustomerId);
                    if (customer != null)
                    {
                        item.CustomerCode = customer.Code;
                        item.CustomerName = customer.Name;
                        item.CustomerPhone = customer.ContactNumber;
                    }
                    item.InvoiceDetails = UpdateInvoiceDetail(item, masterIds, productBatchExpireInfo, productSerials, productImages);
                }
                logInfo.LogInfo(true);
                return retVal;
            }
        }

        public async Task<object> Get(GetChannelProductSyncErrorByRetail req)
        {
            var logInfo = new LogObjectMicrosoftExtension(Logger, Guid.NewGuid())
            {
                Action = "GetChannelProductSyncErrorByRetail",
                RetailerId = InternalContext.RetailerId,
                RequestObject = req.ToJson()
            };
            var retailerId = CurrentRetailerId;
            var channels = await GetChannelsByFilter(CurrentRetailerId, req.ChannelType, ChannelTypeHelper.GetChannelTypes(req.ChannelTypes), req.ChannelIds.ToList());

            var filterChannelIds = req.ChannelIds == null ? channels.Select(x => x.Id).ToList()
                : req.ChannelIds.Where(x => channels.Any(y => y.Id == x)).ToList();

            var getProductSyncErrorReq = new GetProductSyncError
            {
                ChannelIds = filterChannelIds,
                BranchId = CurrentBranchId,
                ProductSearchTerm = req.ProductSearchTerm,
                ErrorType = req.ErrorType
            };

            var listProducts = (await HostContext.ResolveService<ChannelProductApi>(Request).Any(getProductSyncErrorReq)).ToSafeJson()
                .FromJson<List<ChannelProduct>>();
            var syncErrorProducts = listProducts.GroupBy(x => x.ChannelId)
                .Select(p =>
                {
                    var r = new ProductSyncError();
                    var channelId = p.FirstOrDefault()?.ChannelId ?? 0;
                    r.Channel = (channels.FirstOrDefault(c => c.Id == channelId))?.ToSafeJson().FromJson<ChannelResponse>();
                    r.ChannelProducts = p.ToList();
                    return r;
                }).ToList();
            syncErrorProducts = syncErrorProducts.Where(x => x.Channel != null).ToList();
            var res = new SyncErrorResponse<ProductSyncError>
            {
                Total = syncErrorProducts.Count,
                Data = syncErrorProducts.Skip(req.Skip).Take(req.Top).ToList()
            };

            var connectionString = GetKvEntitiesConnectString();
            var basePriceBookIds = channels.Where(x => x.BasePriceBookId.HasValue).Select(x => x.BasePriceBookId ?? 0);
            var priceBookIds = channels.Where(x => x.PriceBookId.HasValue).Select(x => x.PriceBookId ?? 0);
            priceBookIds = priceBookIds.Union(basePriceBookIds).Distinct();
            var productIds = res.Data.SelectMany(x => x.ChannelProducts).Select(x => x.KvProductId).Distinct().ToList();

            var priceBooks = new List<PriceBookDetail>();
            var productDic = new Dictionary<int, List<ProductForChannelMapping>>();
            var branchIds = res.Data.Select(x => x.Channel).Where(x => x.BranchId.HasValue).Select(x => x.BranchId ?? 0).Distinct().ToList();
            logInfo.Description = "Begin execute SQL";
            logInfo.LogInfo();
            using (var db = await DbConnectionFactory.OpenAsync(connectionString))
            {
                var priceBookRepo = new PriceBookRepository(db);
                var productRepo = new ProductRepository(db);
                priceBooks = await priceBookRepo.GetDetailByPriceBookIdsAndProductIds(retailerId, priceBookIds.ToList(), productIds);
                foreach (var branchId in branchIds)
                {
                    var productIdsByBranch = res.Data.Where(x => x.Channel.BranchId == branchId).SelectMany(x => x.ChannelProducts)
                        .Select(x => x.KvProductId).Distinct()
                        .ToList();
                    var product = await productRepo.GetProductForMapping(retailerId, branchId, productIdsByBranch);
                    productDic[branchId] = product;
                }
            }


            res.Data.ForEach(item =>
            {
                item.ChannelProducts?.ForEach(p =>
                {
                    var products = productDic.ContainsKey(p.BranchId) ? productDic[p.BranchId] : null;
                    var product = products.FirstOrDefault(x => x.Id == p.KvProductId);
                    p.Price = priceBooks.FirstOrDefault(x => x.ProductId == p.KvProductId && x.PriceBookId == item.Channel.BasePriceBookId)?.Price ??
                        product?.BasePrice ?? 0;
                    p.OnHand = product?.OnHand ?? 0;
                    p.PricePromotion = priceBooks.FirstOrDefault(x => x.ProductId == p.KvProductId && x.PriceBookId == item.Channel.PriceBookId)?.Price;
                });
            });
            logInfo.LogInfo(true);
            return res;
        }

        public async Task<IList<ProductChannelByKvProductIdsRes>> Get(GetProductChannelByKvProductIdsByRetail req)
        {
            var result = new List<ProductChannelByKvProductIdsRes>();

            if (req?.KvProductIds?.Any() != true) return result;

            req.KvProductIds = req.KvProductIds.Distinct().ToList();

            var getProductReq = new GetProductByKvProductIds
            {
                KvProductIds = req.KvProductIds
            };
            var channelProducts = await HostContext.ResolveService<ChannelProductApi>(Request).Any(getProductReq);

            if (channelProducts?.Any() != true) return result;

            result = new List<ProductChannelByKvProductIdsRes>();

            foreach (var kvProductId in req.KvProductIds)
            {
                var kvProductIdData = channelProducts.Where(x => x.KvProductId == kvProductId).ToList();
                var item = new ProductChannelByKvProductIdsRes
                {
                    KvProductId = kvProductId,
                    ChannelProducts = new List<ChannelProductRes>()
                };

                foreach (var data in kvProductIdData)
                {
                    var channelProduct = data.ConvertTo<ChannelProductRes>();

                    channelProduct.Type = data.ChannelType;
                    channelProduct.TypeName = EnumHelper.EnumTypeTo<ChannelType>(data.ChannelType).ToString();
                    channelProduct.ProductId = data.ItemId;
                    channelProduct.ParentProductId = data.ParentItemId;
                    channelProduct.ProductName = data.ItemName;
                    channelProduct.ProductSku = data.ItemSku;

                    EditUrlProp editUrlProp = new EditUrlProp(data.ChannelType, data.ParentItemId, data.ShopId, data?.SuperId,
                        AppSettings.Get($"{GetChannelTypeName(data.ChannelType)}EditProductUrl", ""));
                    channelProduct.EditProductUrl = GeneralHelper.GenerateEditProductUrl(editUrlProp);

                    item.ChannelProducts.Add(channelProduct);
                }

                result.Add(item);
            }

            return result;
        }

        public async Task<IsAffectOnHandFormulaEcommerceChannelRes> Get(GetIsAffectOnHandFormulaEcommerceChannelByRetail req)
        {
            var getByRetailerReq = new GetByRetailer
            {
                RetailerId = CurrentRetailerId,
                Type = 0,
                OnlyIsActive = false
            };
            var allChannels = await HostContext.ResolveService<ChannelApi>(Request).Any(getByRetailerReq);

            if (allChannels?.Any() != true) return new IsAffectOnHandFormulaEcommerceChannelRes { IsAffect = false };

            var onHandFormulas = allChannels.Where(channel => channel.SyncOnHandFormula > 0)
                .Select(channel => channel.SyncOnHandFormula).Distinct().ToList();
            var isAffectSyncOrder = allChannels.Any(channel => channel.IsSyncOrder);

            if (req.OffSellAllowOrder && onHandFormulas.Any(onHandFormula => onHandFormula == 2 || onHandFormula == 4))
            {
                return new IsAffectOnHandFormulaEcommerceChannelRes { IsAffect = true, IsAffectSyncOrder = isAffectSyncOrder };
            }

            if (req.OffUseOrderSupplier && onHandFormulas.Any(onHandFormula => onHandFormula == 3 || onHandFormula == 4))
            {
                return new IsAffectOnHandFormulaEcommerceChannelRes { IsAffect = true, IsAffectSyncOrder = isAffectSyncOrder };
            }

            return new IsAffectOnHandFormulaEcommerceChannelRes { IsAffect = false, IsAffectSyncOrder = isAffectSyncOrder };
        }

        public async Task<List<object>> Get(KvProductSearchByRetail req)
        {
            if (new OmniSearchProductElasticToggle(AppSettings).Enable(CurrentRetailerId, GroupId))
            {
                try
                {
                    return await GetProductFromEs(req);
                }
                catch (Exception ex)
                {
                    var logObj = new LogObjectMicrosoftExtension(Logger, Guid.NewGuid())
                    {
                        Action = "GetProductByEsSearch",
                        OmniChannelId = req.ChannelId,
                        RetailerId = InternalContext.RetailerId,
                        BranchId = InternalContext.BranchId,
                        RequestObject = req
                    };
                    logObj.LogError(ex);

                    return await GetProductFromDb(req);
                }
            }

            return await GetProductFromDb(req);
        }

        public async Task<List<ChannelProduct>> Get(ChannelProductSearchByRetail req)
        {
            if (string.IsNullOrEmpty(req.Term) && !req.IsMobile.HasValue) return null;
            var term = string.IsNullOrEmpty(req.Term) ? string.Empty : req.Term.Trim();
            var skip = req.Skip ?? 0;
            var top = req.Top ?? 50;
            var getProductReq = new GetProductBySearch
            {
                ChannelId = req.ChannelId,
                Term = term,
                Skip = skip,
                Top = top,
            };

            var retls = (await HostContext.ResolveService<ChannelProductApi>(Request).Any(getProductReq)).ToSafeJson().FromJson<List<ChannelProduct>>();
            return retls.Take(top).ToList();
        }

        public async Task<List<RetailerOutlet>> Get(GetRetailerChannelByRetail req)
        {
            var retailerId = CurrentRetailerId;
            var getChannelByRetailerReq = new GetByRetailer
            {
                RetailerId = retailerId,
                Type = req.Channeltype,
                OnlyIsActive = !req.IncludeChannelInActive,
                Types = ChannelTypeHelper.GetChannelTypes(req.ChannelTypes) ?? new List<int>()
            };
            var lstChannels = (await HostContext.ResolveService<ChannelApi>(Request).Any(getChannelByRetailerReq)).ToSafeJson().FromJson<List<ChannelResponse>>();

            var channelMapping = lstChannels.Where(x => !string.IsNullOrEmpty(x.Name)).Map(x => new RetailerOutlet
            {
                Code = x.Code,
                RetailerId = x.RetailerId,
                Name = x.Name,
                Type = x.Type,
                ChannelId = x.Id,
                CreatedDate = x.CreatedDate,
                Position = GetChannelPosition(x.Type),
                IdentityKey = x.IdentityKey,
                IsActive = x.IsActive,
                IsExpireWarning = x.IsExpireWarning
            });
            channelMapping = channelMapping.OrderBy(x => x.Position).ThenByDescending(x => x.CreatedDate).ToList();
            return channelMapping;
        }

        public async Task<object> Get(GetKvProductByIdByRetail req)
        {
            var connectString = GetKvEntitiesConnectString();
            using (var db = await DbConnectionFactory.OpenAsync(connectString))
            {
                var productRepo = new ProductRepository(db);
                var retVal = await productRepo.GetProductForMapping(CurrentRetailerId, req.BranchId,
                    new List<long>() { req.ProductId });
                return retVal.FirstOrDefault();
            }
        }

        public async Task<object> Get(GetMapProductByChannelByRetail req)
        {
            var getProductReq = new GetProductByChannel
            {
                ChannelId = req.ChannelId,
                BranchId = req.BranchId,
            };

            var retVal = (await HostContext.ResolveService<ChannelProductApi>(Request).Any(getProductReq)).ToSafeJson().FromJson<List<ChannelProduct>>();

            return retVal.Select(x => x.ItemSku).ToList();
        }

        public async Task<PagingDataSource<ProductMapResponse>> Get(GetProductByListChannelId req)
        {
            var retailerId = CurrentRetailerId;
            var branchId = CurrentBranchId;
            var retVal = new PagingDataSource<ProductMapResponse>();
            if (!ValidateRequest(req))
            {
                return retVal;
            }
            var mappings = (await BuildPagingDataSourceMapProduct(req)).ConvertTo<PagingDataSource<MapProduct>>();
            retVal.Data = mappings.Data.ToSafeJson().FromJson<List<ProductMapResponse>>() ?? new List<ProductMapResponse>();
            var channelLst = await OmniChannelService.GetByChannelIds(req.ChannelIdLst, retailerId, false, false);
            var basePriceBookIdLst = channelLst
                .Where(item => item.BasePriceBookId.HasValue)
                .Select(item => item.BasePriceBookId.Value)
                .Distinct()
                .ToList();
            var connectionString = GetKvEntitiesConnectString();
            List<ProductForChannelMapping> kvProductLst;
            var kvProductIdLst = retVal.Data.Select(p => p.KvProductId).ToList();
            var branchIdLst = channelLst.Select(item => item.BranchId).Distinct().ToList();
            var priceBooksDetail = new List<PriceBookDetail>();
            var priceBookLst = new List<PriceBook>();
            using (var db = await DbConnectionFactory.OpenAsync(connectionString))
            {
                var productRepo = new ProductRepository(db);
                var priceBookRepo = new PriceBookRepository(db);
                kvProductLst = await productRepo.GetProductForMappingByBranchLst(retailerId, branchIdLst, kvProductIdLst);
                if (basePriceBookIdLst.Count > 0)
                {
                    priceBookLst = await priceBookRepo.GetByIdLstAsync(retailerId, basePriceBookIdLst);
                    priceBooksDetail = await priceBookRepo.GetDetailByIdLst(retailerId, basePriceBookIdLst, kvProductIdLst.Distinct().ToList());
                }
            }
            ProcessMappingData(channelLst, retVal, priceBookLst, priceBooksDetail, kvProductLst);
            retVal.Total = (int)mappings.Total;
            return retVal;
        }

        public async Task<PagingDataSource<ProductMapResponse>> Get(GetProductByChannelIdByRetail req)
        {
            var retailerId = CurrentRetailerId;
            var retVal = new PagingDataSource<ProductMapResponse>();

            var getChannelByIdReq = new GetById
            {
                Id = req.ChannelId,
            };
            var channel = await HostContext.ResolveService<ChannelApi>(Request).Get(getChannelByIdReq);


            var getProductsByChannelIdReq = new GetProductByChannelId
            {
                ChannelId = req.ChannelId,
                BranchId = req.BranchId,
                Skip = req.Skip ?? 0,
                Top = req.Top ?? 0,
                IsConnected = req.IsConnected,
                IsSyncSuccess = req.IsSyncSuccess,
                ChannelTerm = req.ItemSku,
                KvTerm = req.KvProductSku,
            };
            var mappings = (await HostContext.ResolveService<ChannelProductApi>(Request).Any(getProductsByChannelIdReq))
                .ConvertTo<PagingDataSource<MapProduct>>();
            if (mappings == null || mappings.Total == 0 || mappings.Data?.Count == 0)
            {
                return retVal;
            }
            retVal.Data = mappings.Data?.ToSafeJson().FromJson<List<ProductMapResponse>>() ?? new List<ProductMapResponse>();

            var connectionString = GetKvEntitiesConnectString();
            using (var db = await DbConnectionFactory.OpenAsync(connectionString))
            {
                var kvProductId = retVal.Data?.Select(p => p.KvProductId).ToList();
                var productRepo = new ProductRepository(db);
                var kvProduct =
                    (await productRepo.GetProductForMapping(retailerId, channel.BranchId, kvProductId))?.ToDictionary(x => x.Id,
                        y => y);

                var priceBooksDetail = new List<PriceBookDetail>();
                if (channel?.BasePriceBookId != null && channel.BasePriceBookId.Value > 0)
                {
                    var priceBookRepo = new PriceBookRepository(db);
                    var priceBook = await priceBookRepo.GetById(retailerId, channel.BasePriceBookId.Value);

                    if (priceBook == null || priceBook.Id <= 0 || !priceBook.IsActive || priceBook.StartDate > DateTime.Now || priceBook.EndDate < DateTime.Now)
                    {
                        channel.BasePriceBookId = 0;
                    }

                    priceBooksDetail = await priceBookRepo.GetDetailById(retailerId, channel.BasePriceBookId.Value);
                }

                foreach (var item in retVal.Data)
                {
                    if (kvProduct.ContainsKey(item.KvProductId))
                    {
                        item.KvProductName = kvProduct[item.KvProductId]?.Name;
                        item.AttributeLabel = kvProduct[item.KvProductId]?.AttributeValue;
                        item.Unit = kvProduct[item.KvProductId]?.Unit;
                        item.BasePrice = kvProduct[item.KvProductId]?.BasePrice ?? 0;
                        item.Price = priceBooksDetail.FirstOrDefault(x => x.ProductId == item.KvProductId)?.Price ?? item.BasePrice;
                        item.OnHand = kvProduct[item.KvProductId]?.OnHand ?? 0;
                        item.Reserved = kvProduct[item.KvProductId]?.Reserved ?? 0;
                        item.OnOrder = kvProduct[item.KvProductId]?.OnOrder ?? 0;
                        item.KvProductFullName = kvProduct[item.KvProductId]?.FullName;
                        item.ProductType = kvProduct[item.KvProductId]?.ProductType;
                        item.KvProductSku = kvProduct[item.KvProductId]?.Code ?? item.KvProductSku;
                    }

                    EditUrlProp editUrlProp = new EditUrlProp(req.ChannelType, item.ParentItemId, channel?.IdentityKey, item?.SuperId,
                        AppSettings.Get($"{GetChannelTypeName(req.ChannelType)}EditProductUrl", ""));
                    item.EditChannelUrl = GeneralHelper.GenerateEditProductUrl(editUrlProp);
                }
                retVal.Total = (int)mappings.Total;
                return retVal;
            }
        }

        /// <summary>
        /// Get Invoice Detail
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        /// <exception cref="KvValidateException"></exception>
        public async Task<InvoiceDto> Get(GetInvoiceDetail req)
        {
            if (req.ChannelId <= 0 || req.Id == null)
            {
                throw new KvValidateException(KvMessage.NotFound);
            }
            var channel = await HostContext.ResolveService<ChannelApi>(Request).Get(new GetById()
            {
                Id = req.ChannelId,
            });
            if (channel == null || !channel.IsActive || channel.OmniChannelSchedules.All(x => x.Type != (int)ScheduleType.SyncOrder))
            {
                throw new KvValidateException("Channel not found or deactive");
            }

            var invoice = await HostContext.ResolveService<InvoiceApi>(Request).Any(new GetInvoiceById()
            {
                Id = req.Id
            });

            if (invoice == null)
            {
                throw new KvValidateException("Invoice not found");
            }

            var invoiceDto = invoice.ConvertTo<InvoiceDto>();
            invoiceDto.ChannelName = channel.Name;
            invoiceDto.PriceBookId = channel.BasePriceBookId ?? 0;
            invoiceDto.ErrorMappingProductIDs = GetErrorMappingIdsFromMessage<InvoiceDto>(invoiceDto);

            return invoiceDto;
        }

        public async Task<OrderDto> Get(GetOrderDetail req)
        {
            if (req.ChannelId <= 0 || req.Id == null)
            {
                throw new KvValidateException(KvMessage.NotFound);
            }
            var channel = await HostContext.ResolveService<ChannelApi>(Request).Get(new GetById()
            {
                Id = req.ChannelId,
            });

            if (channel == null || !channel.IsActive || channel.OmniChannelSchedules.All(x => x.Type != (int)ScheduleType.SyncOrder))
            {
                throw new KvValidateException("Channel not found or deactive");
            }

            var order = await HostContext.ResolveService<OrderApi>(Request).Any(new GetOrderByIds()
            {
                Id = req.Id
            });

            if (order == null)
            {
                throw new KvValidateException("Order not found");
            }

            var orderDto = order.ConvertTo<OrderDto>();
            orderDto.ChannelName = channel.Name;
            orderDto.PriceBookId = channel.BasePriceBookId ?? 0;
            orderDto.ErrorMappingProductIDs = GetErrorMappingIdsFromMessage<OrderDto>(orderDto);

            return orderDto;
        }

        public async Task<object> Get(QuantitySyncError req)
        {
            if (InternalContext.RetailerId == 0)
            {
                throw new KvValidateException("Missing retailerId");
            }
            if (string.IsNullOrEmpty(req.SyncTypes) || string.IsNullOrEmpty(req.ChannelTypes))
            {
                throw new KvValidateException("Missing params SyncTypes or ChannelTypes");
            }
            var channels = await GetChannelsByFilter(CurrentRetailerId, null, ChannelTypeHelper.GetChannelTypes(req.ChannelTypes));
            long totalInvoice = 0, totalOrder = 0, totalProduct = 0;
            var filterChannelIds = channels.Select(x => x.Id).ToList();
            var syncTypes = req.SyncTypes.Split(',');
            if (syncTypes.FirstOrDefault(type => EnumHelper.ToDescriptionName(SyncTabError.Invoice).Equals(type)) != null)
            {
                totalInvoice = await HostContext.ResolveService<InvoiceApi>(Request).Any(new TotalInvoiceError
                {
                    ChannelIds = filterChannelIds
                });
            }

            if (syncTypes.FirstOrDefault(type => EnumHelper.ToDescriptionName(SyncTabError.Order).Equals(type)) != null)
            {
                totalOrder = await HostContext.ResolveService<OrderApi>(Request).Any(new TotalOrderError
                {
                    ChannelIds = filterChannelIds
                });
            }

            if (syncTypes.FirstOrDefault(type => EnumHelper.ToDescriptionName(SyncTabError.Product).Equals(type)) != null)
            {

                totalProduct = await HostContext.ResolveService<ChannelProductApi>(Request).Any(new GetQuantityOfProductSyncError
                {
                    ChannelIds = filterChannelIds
                });
            }

            return new
            {
                TotalInvoiceError = totalInvoice,
                TotalOrderError = totalOrder,
                TotalProduct = totalProduct
            };
        }

        public async Task<object> Get(GetProductSyncErrorByRetail req)
        {
            var retailerId = CurrentRetailerId;
            var channels = await GetChannelsByFilter(CurrentRetailerId, req.ChannelType, ChannelTypeHelper.GetChannelTypes(req.ChannelTypes), req.ChannelIds != null ? req.ChannelIds.ToList() : new List<long>());

            var filterChannelIds = req.ChannelIds == null ? channels.Select(x => x.Id).ToList()
                : req.ChannelIds.Where(x => channels.Any(y => y.Id == x)).ToList();

            var getProductSyncErrorReq = new GetProductSyncError
            {
                ChannelIds = filterChannelIds,
                BranchId = CurrentBranchId,
                ProductSearchTerm = req.ProductSearchTerm,
                ErrorType = req.ErrorType
            };

            var listProducts = (await HostContext.ResolveService<ChannelProductApi>(Request).Any(getProductSyncErrorReq)).ToSafeJson()
                .FromJson<List<ChannelProduct>>();
            var syncErrorProducts = listProducts
                .Select(p =>
                new ProductSyncFailError
                {
                    Channel = (channels.FirstOrDefault(c => c.Id == p.ChannelId))?.ToSafeJson().FromJson<ChannelResponse>(),
                    ChannelProduct = p?.ToSafeJson().FromJson<ChannelProduct>()
                }).ToList();

            syncErrorProducts = syncErrorProducts.Where(x => x.Channel != null).OrderByDescending(x => x.ChannelProduct.ModifiedDate).ToList();
            var res = new SyncErrorResponse<ProductSyncFailError>
            {
                Total = syncErrorProducts.Count,
                Data = syncErrorProducts.Skip(req.Skip).Take(req.Top).ToList()
            };

            var connectionString = GetKvEntitiesConnectString();
            var basePriceBookIds = channels.Where(x => x.BasePriceBookId.HasValue).Select(x => x.BasePriceBookId ?? 0);
            var priceBookIds = channels.Where(x => x.PriceBookId.HasValue).Select(x => x.PriceBookId ?? 0);
            priceBookIds = priceBookIds.Union(basePriceBookIds).Distinct();
            var productIds = res.Data.Select(x => x.ChannelProduct).Select(x => x.KvProductId).ToList();

            var priceBooks = new List<PriceBookDetail>();
            var productDic = new Dictionary<int, List<ProductForChannelMapping>>();
            var branchIds = res.Data.Select(x => x.Channel).Where(x => x.BranchId.HasValue).Select(x => x.BranchId ?? 0).Distinct().ToList();

            using (var db = await DbConnectionFactory.OpenAsync(connectionString))
            {
                var priceBookRepo = new PriceBookRepository(db);
                var productRepo = new ProductRepository(db);
                priceBooks = await priceBookRepo.GetDetailByPriceBookIdsAndProductIds(retailerId, priceBookIds.ToList(), productIds);
                foreach (var branchId in branchIds)
                {
                    var productIdsByBranch = res.Data.Where(x => x.Channel.BranchId == branchId).Select(x => x.ChannelProduct)
                        .Select(x => x.KvProductId).ToList();
                    var product = await productRepo.GetProductForMapping(retailerId, branchId, productIdsByBranch);
                    productDic[branchId] = product;
                }
            }

            res.Data.ForEach(p =>
            {
                var products = productDic.ContainsKey(p.ChannelProduct.BranchId) ? productDic[p.ChannelProduct.BranchId] : null;
                var product = products.FirstOrDefault(x => x.Id == p.ChannelProduct.KvProductId);
                p.ChannelProduct.Price = priceBooks.FirstOrDefault(x => x.ProductId == p.ChannelProduct.KvProductId && x.PriceBookId == p.Channel.BasePriceBookId)?.Price ??
                    product?.BasePrice ?? 0;
                p.ChannelProduct.OnHand = product?.OnHand ?? 0;
                p.ChannelProduct.PricePromotion = priceBooks.FirstOrDefault(x => x.ProductId == p.ChannelProduct.KvProductId && x.PriceBookId == p.Channel.PriceBookId)?.Price;
            });

            return res;
        }

        public async Task<List<object>> Get(GetPrinceByChannelProduct req)
        {
            if (req.ChannelId <= 0 || req.ProductIds == null)
            {
                return new List<object>();
            }
            List<ProductForChannelMapping> listProduct = null;
            var retailerId = CurrentRetailerId;
            var channel = await GetChannel(req.ChannelId);
            var connectionString = GetKvEntitiesConnectString();
            using (var db = await DbConnectionFactory.OpenAsync(connectionString))
            {
                var productRepo = new ProductRepository(db);
                listProduct = await productRepo.GetProductForMapping(retailerId, channel?.BranchId ?? 0, req.ProductIds);
            }
            if (listProduct == null || listProduct.Count == 0)
            {
                return new List<object>();
            }
            var listBooksDetail = await GetPriceDetail(channel, retailerId, req.ProductIds);
            return listProduct.Select(p => new
            {
                p.Id,
                p.Code,
                p.OnHand,
                Price = listBooksDetail?.FirstOrDefault(x => x.ProductId == p.Id)?.Price ?? p.BasePrice,
            }).ToList<object>();
        }
        public async Task<List<PriceBook>> Get(GetPriceBooks req)
        {
            var connectionString = GetKvEntitiesConnectString();
            using (var db = await DbConnectionFactory.OpenAsync(connectionString))
            {
                var priceBookRepo = new PriceBookRepository(db);
                var listBook = await priceBookRepo.GetByRetailer(CurrentRetailerId);
                var listBooks = listBook.Where(p => p.StartDate <= DateTime.Now && p.EndDate >= DateTime.Now && p.IsActive && !(p.isDeleted ?? false)).ToList();
                listBooks.Insert(0, new PriceBook { Id = 0, Name = KvMessage.GlobalPriceBook });
                return listBooks;
            }
        }
        public async Task<object> Post(GetQuantityOfOrderSyncErrorByRetail req)
        {
            var channels = await GetChannelsByFilter(CurrentRetailerId, req.ChannelType, ChannelTypeHelper.GetChannelTypes(req.ChannelTypes));

            var filterChannelIds = channels.Select(x => x.Id).ToList();
            var totalOrderErrorReq = new TotalOrderError
            {
                ChannelIds = filterChannelIds
            };

            var total = await HostContext.ResolveService<OrderApi>(Request).Any(totalOrderErrorReq);
            return new { Quantity = total };
        }

        public async Task<object> Post(GetQuantityOfInvoiceSyncErrorByRetail req)
        {
            var channels = await GetChannelsByFilter(CurrentRetailerId, req.ChannelType, ChannelTypeHelper.GetChannelTypes(req.ChannelTypes));
            var filterChannelIds = channels.Select(x => x.Id).ToList();

            var totalInvoiceErrorReq = new TotalInvoiceError()
            {
                ChannelIds = filterChannelIds
            };

            var total = await HostContext.ResolveService<InvoiceApi>(Request).Any(totalInvoiceErrorReq);
            return new { Quantity = total };
        }

        public async Task<object> Post(SyncInvoiceChannelErrorByRetail req)
        {
            if (req.Invoice == null)
            {
                throw new KvValidateException(KvMessage.NotFound);
            }

            var channel = await HostContext.ResolveService<ChannelApi>(Request).Get(new GetById()
            {
                Id = req.Invoice.ChannelId,
            });
            if (channel == null || !channel.IsActive || channel.OmniChannelSchedules.All(x => x.Type != (int)ScheduleType.SyncOrder))
            {
                return true;
            }

            var reSyncInvoice = new List<InvoiceDto> { req.Invoice };
            var channels = new List<ChannelResponse> { channel.ConvertTo<ChannelResponse>() };

            var newReq = new MultiSyncErrorInvoice()
            {
                Invoices = reSyncInvoice.ToSafeJson(),
                Channels = channels.ToSafeJson(),
            };
            return HostContext.ResolveService<InvoiceApi>(Request).Post(newReq);
        }

        public async Task<object> Post(SyncOrderChannelErrorByRetail req)
        {
            if (req.ChannelId.GetValueOrDefault() <= 0 || req.Order == null)
            {
                throw new KvValidateException(KvMessage.NotFound);
            }

            var channel = await HostContext.ResolveService<ChannelApi>(Request).Get(new GetById()
            {
                Id = req.ChannelId ?? 0,
            });
            if (channel == null || !channel.IsActive || channel.OmniChannelSchedules.All(x => x.Type != (int)ScheduleType.SyncOrder))
            {
                return true;
            }

            var orders = new List<OrderDto> { req.Order };
            var channels = new List<ChannelResponse> { channel.ConvertTo<ChannelResponse>() };

            var newReq = new MultiSyncErrorOrder()
            {
                Orders = orders.ToSafeJson(),
                Channels = channels.ToSafeJson(),
            };
            return HostContext.ResolveService<OrderApi>(Request).Post(newReq);
        }

        public async Task<object> Post(SyncChannelErrorByRetail req)
        {
            var omniChannelMaximumRetryOrder = AppSettings.Get("OmniChannelMaximumRetryOrder", 100);
            var getByRetailerReq = new GetByRetailer()
            {
                RetailerId = CurrentRetailerId,
                Type = null,
                OnlyIsActive = true,
            };
            var channels = await HostContext.ResolveService<ChannelApi>(Request).Any(getByRetailerReq);
            channels = channels.Where(x => x.OmniChannelSchedules.Any(y => y.Type == (int)ScheduleType.SyncOrder)).ToList();
            var channelIds = channels.Select(x => x.Id).ToList();

            if (req.Type == (byte)SyncTabError.Order)
            {
                var getOrdersSyncErrorReq = new GetOrderSyncError
                {
                    RetailerId = CurrentRetailerId,
                    ChannelIds = channelIds,
                    BranchIds = new List<int>(),
                    Skip = 0,
                    Top = omniChannelMaximumRetryOrder,
                    SearchTerm = null,
                    OrderCode = null,
                    KeySearchProduct = "",
                };
                var response = (await HostContext.ResolveService<OrderApi>(Request).Any(getOrdersSyncErrorReq))
                    .ToSafeJson().FromJson<Sdk.Common.PagingDataSource<OrderDto>>();
                if (response.Data == null || !response.Data.Any())
                {
                    return null;
                }
                var resSyncOrder = new List<OrderDto>();

                if (req.Orders == null || !req.Orders.Any())
                {
                    resSyncOrder = response.Data?.ToList() ?? new List<OrderDto>();
                }
                else
                {
                    foreach (var order in response.Data)
                    {
                        if (req.Orders.All(x => x.Code != order.Code))
                        {
                            resSyncOrder.Add(order.ConvertTo<OrderDto>());
                        }
                    }
                    resSyncOrder.AddRange(req.Orders);
                }

                var newReq = new MultiSyncErrorOrder()
                {
                    Orders = resSyncOrder.ToSafeJson(),
                    Channels = channels.ToSafeJson(),
                };
                return HostContext.ResolveService<OrderApi>(Request).Post(newReq);
            }

            if (req.Type == (byte)SyncTabError.Invoice)
            {
                var getInvoiceSyncErrorReq = new GetInvoiceSyncError
                {
                    RetailerId = CurrentRetailerId,
                    ChannelIds = channelIds,
                    BranchIds = new List<int>(),
                    Skip = 0,
                    Top = omniChannelMaximumRetryOrder,
                    SearchTerm = null,
                    InvoiceCode = null,
                    KeySearchProduct = "",
                };
                var response = (await HostContext.ResolveService<InvoiceApi>(Request).Any(getInvoiceSyncErrorReq)).ConvertTo<Sdk.Common.PagingDataSource<InvoiceDto>>();
                if (response.Data == null || !response.Data.Any())
                {
                    return null;
                }
                var reSyncInvoice = new List<InvoiceDto>();
                foreach (var invoice in response.Data)
                {
                    if (req.Invoices != null && req.Invoices.Any(x => x.Code == invoice.Code))
                    {
                        var invoiceEdit = req.Invoices.FirstOrDefault(x => x.Code == invoice.Code);
                        if (invoiceEdit != null)
                        {
                            reSyncInvoice.Add(invoiceEdit);
                        }
                    }
                    else
                    {
                        reSyncInvoice.Add(invoice.ConvertTo<InvoiceDto>());
                    }
                }

                var newReq = new MultiSyncErrorInvoice()
                {
                    Invoices = reSyncInvoice.ToSafeJson(),
                    Channels = channels.ToSafeJson(),
                };
                return HostContext.ResolveService<InvoiceApi>(Request).Post(newReq);
            }

            var syncErrorChannelReq = new SyncErrorChannel
            {
                ChannelId = req.ChannelId,
                ItemIds = req.ItemIds
            };
            return await HostContext.ResolveService<ChannelApi>(Request).Post(syncErrorChannelReq);
        }

        public async Task<object> Post(RemoveProductMappingByRetail req)
        {
            var validateResult = await new RemoveProductMappingValidator().ValidateAsync(req);

            if (!validateResult.IsValid)
            {
                throw new KvValidateException(validateResult.Errors.FirstOrDefault()?.ErrorMessage);
            }
            var deleteProductMappingReq = new DeleteProductMapping
            {
                ChannelId = req.ChannelId,
                KvProductId = req.KvProductId,
                ChannelProductId = req.ChannelProductId,
            };

            if (AppSettings.Get("UseCallApiMappingGolang", true))
            {
                await MappingInternalClient.DeleteProductMapping(deleteProductMappingReq,
                    MappingUtil.GetMappingHeader(Request));
                return true;
            }

            await HostContext.ResolveService<ProductMappingApi>(Request).Post(deleteProductMappingReq);

            await UpdateProductFlagRelateNormal(CurrentRetailerId, req.KvProductId, false);

            return true;
        }

        public async Task<object> Post(GetSaleChannelOmniByTypeByRetail req)
        {
            var newReq = req.ConvertTo<GetSaleChannelOmniByType>();
            return await HostContext.ResolveService<ChannelApi>(Request).Post(newReq);
        }

        public async Task<object> Post(AddProductMappingByRetail req)
        {
            var retailerId = CurrentRetailerId;
            var branchId = CurrentBranchId;
            var connectionString = GetKvEntitiesConnectString();
            using (var db = await DbConnectionFactory.OpenAsync(connectionString))
            {
                var productRepo = new ProductRepository(db);
                var isActive =
                    await productRepo.CheckProductBranchIsActive(retailerId, branchId, req.ProductMapping.ProductKvId);
                if (!isActive) throw new KvValidateException("Hàng hóa đã ngừng hoạt động.");
            }

            var addProductMappingReq = new AddProductMapping
            {
                ProductMapping = req.ProductMapping.ConvertTo<MappingDto>(),
            };

            ProductMapping result;
            if (AppSettings.Get("UseCallApiMappingGolang", true))
            {
                result = (await MappingInternalClient.AddProductMapping(addProductMappingReq,
                    MappingUtil.GetMappingHeader(Request))).ConvertTo<ProductMapping>();
            }
            else
            {
                result = (await HostContext.ResolveService<ProductMappingApi>(Request).Post(addProductMappingReq)).ConvertTo<ProductMapping>();
                await UpdateProductFlagRelateNormal(retailerId, req.ProductMapping.ProductKvId, true);
            }

            var getByRetailerReq = new GetByRetailer
            {
                RetailerId = retailerId,
                Type = null,
                OnlyIsActive = true
            };

            var lstChannels = await HostContext.ResolveService<ChannelApi>(Request).Any(getByRetailerReq);
            var channel = lstChannels.FirstOrDefault(x => x.Id == result.ChannelId);
            byte channelType = channel?.Type ?? 0;
            string identityKey = channel?.IdentityKey ?? "";

            var product = await ProductMongoService.GetByProductChannelId(retailerId, result.ChannelId, result.ProductChannelId, true);

            EditUrlProp editUrlProp = new EditUrlProp(channelType, result.ParentProductChannelId, identityKey, product?.SuperId,
                        AppSettings.Get($"{GetChannelTypeName(channelType)}EditProductUrl", ""));
            result.EditProductUrl = GeneralHelper.GenerateEditProductUrl(editUrlProp);

            return result;
        }

        public async Task<PagingDataSource<ProductForChannelMappingResponse>> Post(MappingProductsListByRetail req)
        {
            var retailerId = CurrentRetailerId;
            var branchId = CurrentBranchId;

            var maximumProductSelected = AppSettings.Get("OmniChannelMaximumProductOpenMappingForm", 50);
            if (maximumProductSelected > 0 && req.ProductIds.Count() > maximumProductSelected)
            {
                throw new ShareKernel.Exceptions.KvValidateException($"Chỉ được chọn tối đa {maximumProductSelected} hàng hóa để mở form liên kết kênh bán");
            }

            var retVal = new PagingDataSource<ProductForChannelMappingResponse>();
            req.Skip = req.Skip ?? 0;
            req.Top = req.Top ?? 10;
            var productIds = req.ProductIds != null ? req.ProductIds.ToList() : new List<long>();

            var connectionString = GetKvEntitiesConnectString();
            using (var db = await DbConnectionFactory.OpenAsync(connectionString))
            {
                var productRepo = new ProductRepository(db);
                var kvProduct = await productRepo.GetProductForMapping(retailerId, branchId, productIds, req.Skip.Value,
                    req.Top.Value);
                var kvSkus = kvProduct.Data.Select(x => x.Code).ToList();

                var getProductMappingReq = new GetProductMapping
                {
                    ChannelId = null,
                    KvProductSkus = kvSkus,
                    ChannelProductSkus = null,
                };

                var getByRetailerReq = new GetByRetailer
                {
                    RetailerId = retailerId,
                    Type = null,
                    OnlyIsActive = true
                };
                var lstChannels = await HostContext.ResolveService<ChannelApi>(Request).Any(getByRetailerReq);

                var lstProductMapping =
                    (await HostContext.ResolveService<ProductMappingApi>(Request).Any(getProductMappingReq)).ToSafeJson().FromJson<List<ProductMapping>>();

                #region Add edit product url

                lstProductMapping.ForEach(item =>
                {
                    var channel = lstChannels.FirstOrDefault(x => x.Id == item.ChannelId);
                    byte channelType = channel?.Type ?? 0;
                    string identityKey = channel?.IdentityKey ?? "";

                    EditUrlProp editUrlProp = new EditUrlProp(channelType, item.ParentProductChannelId, identityKey, item?.SuperId,
                        AppSettings.Get($"{GetChannelTypeName(channelType)}EditProductUrl", ""));
                    item.EditProductUrl = GeneralHelper.GenerateEditProductUrl(editUrlProp);
                });

                #endregion

                retVal.Data = kvProduct.Data.Map(x => x.ConvertTo<ProductForChannelMappingResponse>());

                foreach (var item in retVal.Data)
                {
                    item.ProductMappings = lstProductMapping.Where(x => x.ProductKvSku == item.Code).ToList();
                }

                retVal.Total = kvProduct.Total;

                return retVal;
            }
        }

        public async Task<object> Post(AddProductAndMappingByRetail req)
        {
            var validateResult = await new AddProductMappingValidator().ValidateAsync(req);

            if (!validateResult.IsValid)
            {
                throw new KvValidateException(validateResult.Errors.FirstOrDefault()?.ErrorMessage);
            }
            var retailerId = CurrentRetailerId;
            var branchId = CurrentBranchId;
            var connectionString = GetKvEntitiesConnectString();
            using (var db = await DbConnectionFactory.OpenAsync(connectionString))
            {
                var productBranchRepo = new ProductRepository(db);
                var isActive = await productBranchRepo.CheckProductBranchIsActive(retailerId, branchId, req.ChannelProduct?.KvProductId ?? 0);
                if (!isActive) throw new KvValidateException("Hàng hóa đã ngừng hoạt động.");

                var addMappingProductReq = new AddProductAndMapping
                {
                    Product = req.ChannelProduct.ConvertTo<MapProduct>(),
                };

                if (AppSettings.Get("UseCallApiMappingGolang", true))
                {
                    var result = (await MappingInternalClient.AddProductAndMapping(addMappingProductReq,
                        MappingUtil.GetMappingHeader(Request))).ConvertTo<bool>();
                    return result;
                }

                await HostContext.ResolveService<ChannelProductApi>(Request).Post(addMappingProductReq);
                await UpdateProductFlagRelateNormal(retailerId, req.ChannelProduct?.KvProductId ?? 0, true);

                return true;
            }
        }

        public async Task<object> Post(ServiceModel.Retailer.GetChannelsById req)
        {
            if (req.Ids != null && req.Ids.Count > 0 && req.RetailerId > 0)
            {
                return await OmniChannelService.GetByChannelIds(req.Ids, req.RetailerId, true, true);
            }
            return new List<Domain.Model.OmniChannel>();
        }

        /// <summary>
        /// Đồng bộ lại Order theo OrderId, ChannelId
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        /// <exception cref="KvValidateException"></exception>
        public async Task<object> Post(SyncOrderChannelError req)
        {
            if (req.ChannelId <= 0 || req.Id == null)
            {
                throw new KvValidateException(KvMessage.NotFound);
            }
            var channel = await HostContext.ResolveService<ChannelApi>(Request).Get(new GetById()
            {
                Id = req.ChannelId,
            });
            if (channel == null || !channel.IsActive || channel.OmniChannelSchedules.All(x => x.Type != (int)ScheduleType.SyncOrder))
            {
                return true;
            }

            var order = await HostContext.ResolveService<OrderApi>(Request).Any(new GetOrderByIds()
            {
                Id = req.Id
            });

            if (order == null)
            {
                throw new KvValidateException("Order not found");
            }

            var channels = new List<ChannelResponse> { channel.ConvertTo<ChannelResponse>() };
            var orders = new List<object> { order };
            var newReq = new MultiSyncErrorOrder()
            {
                Orders = orders.ToSafeJson(),
                Channels = channels.ToSafeJson(),
            };
            return HostContext.ResolveService<OrderApi>(Request).Post(newReq);
        }

        /// <summary>
        /// Đồng bộ lại Invoice theo InvoiceId, ChannelId
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        /// <exception cref="KvValidateException"></exception>
        public async Task<object> Post(SyncInvoiceChannelError req)
        {
            if (req.ChannelId <= 0 || req.Id == null)
            {
                throw new KvValidateException(KvMessage.NotFound);
            }
            var channel = await HostContext.ResolveService<ChannelApi>(Request).Get(new GetById()
            {
                Id = req.ChannelId,
            });
            if (channel == null || !channel.IsActive || channel.OmniChannelSchedules.All(x => x.Type != (int)ScheduleType.SyncOrder))
            {
                return true;
            }

            var invoice = await HostContext.ResolveService<InvoiceApi>(Request).Any(new GetInvoiceById()
            {
                Id = req.Id
            });

            if (invoice == null)
            {
                throw new KvValidateException("Invoice not found");
            }

            var reSyncInvoice = new List<object> { invoice };
            var channels = new List<ChannelResponse> { channel.ConvertTo<ChannelResponse>() };

            var newReq = new MultiSyncErrorInvoice()
            {
                Invoices = reSyncInvoice.ToSafeJson(),
                Channels = channels.ToSafeJson(),
            };
            return HostContext.ResolveService<InvoiceApi>(Request).Post(newReq);
        }

        /// <summary>
        /// Đồng bộ lại toàn bộ Order, Invoice
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        /// <exception cref="KvValidateException"></exception>
        public async Task<object> Post(ResyncChannelError req)
        {
            return new
            {
                SyncOrderSuccess = await ResyncAllOrder(req),
                SyncInvoiceSuccess = await ResyncAllInvoice(req),
                SyncProductSuccess = await ResyncAllProduct(req)
            };
        }

        public async Task<object> Post(MappingChannelProductAndKvProduct req)
        {
            var retailerId = CurrentRetailerId;
            var branchId = CurrentBranchId;
            var connectionString = GetKvEntitiesConnectString();
            var channel = await HostContext.ResolveService<ChannelApi>(Request).Get(new GetById()
            {
                Id = req.ChannelId,
            });
            if (channel == null || !channel.IsActive)
            {
                throw new KvValidateException("Channel not found or deactive");
            }
            using (var db = await DbConnectionFactory.OpenAsync(connectionString))
            {
                var productRepo = new ProductRepository(db);
                var isActive =
                    await productRepo.CheckProductBranchIsActive(retailerId, branchId, req.ProductKvId);
                if (!isActive) throw new KvValidateException("Hàng hóa đã ngừng hoạt động.");

                var kvProduct = await productRepo.GetByIdAsync(req.ProductKvId);
                if (kvProduct == null)
                {
                    throw new KvValidateException("Không tìm thấy sản phẩm Demo.");
                }
                var channelProductsByTrackey = await HostContext.ResolveService<ChannelProductApi>(Request).Any(new GetProductByTrackingKey
                {
                    ChannelId = req.ChannelId,
                    TrackKey = req.ProductChannelTrackKey
                });
                if (channelProductsByTrackey.Count == 0)
                {
                    throw new KvValidateException("Không tìm thấy hàng hóa trên sàn.");

                }

                var channelProduct = channelProductsByTrackey[0];
                if (channelProduct.IsConnected)
                {
                    throw new KvValidateException($"Hàng hóa {channelProduct.ItemName ?? channelProduct.ItemSku} trên sàn đã được liên kết với hàng hóa trên Demo.");
                }

                var productMapping = new ProductMapping()
                {
                    Id = channelProduct.Id,
                    ProductKvId = kvProduct.Id,
                    ProductKvSku = kvProduct.Code,
                    ProductKvFullName = kvProduct.FullName,
                    RetailerId = CurrentRetailerId,
                    RetailerCode = CurrentRetailerCode,
                    ProductChannelId = channelProduct.ItemId + "",
                    ParentProductChannelId = channelProduct.ParentItemId + "",
                    ProductChannelSku = channelProduct.ItemSku,
                    ChannelId = req.ChannelId,
                    ProductChannelName = channelProduct.ItemName,
                    ProductChannelType = channelProduct.Type
                };

                var addProductMappingReq = new AddProductMapping
                {
                    ProductMapping = productMapping.ConvertTo<MappingDto>(),
                };

                ProductMapping result;
                if (AppSettings.Get("UseCallApiMappingGolang", false))
                {
                    result = (await MappingInternalClient.AddProductMapping(addProductMappingReq,
                        MappingUtil.GetMappingHeader(Request))).ConvertTo<ProductMapping>();
                }
                else
                {
                    result = (await HostContext.ResolveService<ProductMappingApi>(Request).Post(addProductMappingReq)).ConvertTo<ProductMapping>();
                    await UpdateProductFlagRelateNormal(retailerId, req.ProductKvId, true);
                }


                var product = await ProductMongoService.GetByProductChannelId(retailerId, result.ChannelId, result.ProductChannelId, true);
                EditUrlProp editUrlProp = new EditUrlProp(channel.Type, result.ParentProductChannelId, channel.IdentityKey, product?.SuperId,
                            AppSettings.Get($"{GetChannelTypeName(channel.Type)}EditProductUrl", ""));
                result.EditProductUrl = GeneralHelper.GenerateEditProductUrl(editUrlProp);

                if (!string.IsNullOrEmpty(req.OrderId) && req.ChannelId > 0)
                {
                    await Post(new SyncOrderChannelError { Id = req.OrderId, ChannelId = req.ChannelId });
                }

                if (!string.IsNullOrEmpty(req.InvoiceId) && req.ChannelId > 0)
                {
                    await Post(new SyncInvoiceChannelError { Id = req.InvoiceId, ChannelId = req.ChannelId });
                }
                return result;
            }
        }

        public async Task<object> Post(GetProductMappingRequest req)
        {
            var deleteProductMappingForward = new GetProductMappingForward
            {
                ChannelId = req.ChannelId,
                ChannelProductId = req.ChannelProductId,
                KvProductId = req.KvProductId
            };
            if (AppSettings.Get("UseCallApiMappingGolang", true))
            {
                return await MappingInternalClient.GetProductMapping(deleteProductMappingForward,
                    MappingUtil.GetMappingHeader(Request));
            }

            return null;
        }

        public async Task<object> Delete(RemoveProductMappingByIdByRetail req)
        {
            var deleteProductMappingByIdReq = new DeleteProductMappingById
            {
                ProductMappingId = req.ProductMappingId,
            };

            ProductMappingApi.DeleteProductMappingByIdRes omniRes;
            if (AppSettings.Get("UseCallApiMappingGolang", true))
            {
                omniRes = (await MappingInternalClient.DeleteProductMappingById(deleteProductMappingByIdReq,
                    MappingUtil.GetMappingHeader(Request))).ConvertTo<ProductMappingApi.DeleteProductMappingByIdRes>();
            }
            else
            {
                omniRes = await HostContext.ResolveService<ProductMappingApi>(Request).Delete(deleteProductMappingByIdReq);
                if (omniRes.IsDeleted && omniRes.ProductMapping?.ProductKvId != null)
                {
                    await UpdateProductFlagRelateNormal(CurrentRetailerId, omniRes.ProductMapping?.ProductKvId ?? 0, false);
                }
            }

            return omniRes.IsDeleted;
        }

        private async Task<bool> ResyncAllOrder(ResyncChannelError req)
        {
            if (!req.IsResyncOrders)
            {
                return false;
            }
            var getByRetailerReq = new GetByRetailer
            {
                RetailerId = CurrentRetailerId,
                Type = null,
                OnlyIsActive = true
            };
            var channels = await HostContext.ResolveService<ChannelApi>(Request).Any(getByRetailerReq);
            channels = channels.Where(x => x.OmniChannelSchedules.Any(y => y.Type == (int)ScheduleType.SyncOrder)).ToList();
            var channelIds = channels.Select(x => x.Id).ToList();
            var getOrdersSyncErrorReq = new GetOrderSyncError
            {
                RetailerId = CurrentRetailerId,
                ChannelIds = channelIds,
                BranchIds = new List<int>(),
                Skip = 0,
                Top = AppSettings.Get("OmniChannelMaximumRetryOrder", 100),
                SearchTerm = null,
                OrderCode = null,
                KeySearchProduct = "",
            };
            var response = (await HostContext.ResolveService<OrderApi>(Request).Any(getOrdersSyncErrorReq))
                .ToSafeJson().FromJson<Sdk.Common.PagingDataSource<OrderDto>>();
            if (response.Data == null || !response.Data.Any())
            {
                return false;
            }

            var resSyncOrder = response.Data.Select(order => order.ConvertTo<OrderDto>()).ToList();

            var multiSyncErrorOrderRequest = new MultiSyncErrorOrder()
            {
                Orders = resSyncOrder.ToSafeJson(),
                Channels = channels.ToSafeJson(),
            };
            return HostContext.ResolveService<OrderApi>(Request).Post(multiSyncErrorOrderRequest).Equals(true);

        }

        private async Task<bool> ResyncAllInvoice(ResyncChannelError req)
        {
            if (!req.IsResyncInvoices)
            {
                return false;
            }
            var getByRetailerReq = new GetByRetailer
            {
                RetailerId = CurrentRetailerId,
                Type = null,
                OnlyIsActive = true
            };
            var channels = await HostContext.ResolveService<ChannelApi>(Request).Any(getByRetailerReq);
            channels = channels.Where(x => x.OmniChannelSchedules.Any(y => y.Type == (int)ScheduleType.SyncOrder)).ToList();
            var channelIds = channels.Select(x => x.Id).ToList();

            var getInvoiceSyncError = new GetInvoiceSyncError
            {
                RetailerId = CurrentRetailerId,
                ChannelIds = channelIds,
                BranchIds = new List<int>(),
                Skip = 0,
                Top = AppSettings.Get("OmniChannelMaximumRetryOrder", 100),
                SearchTerm = null,
                InvoiceCode = null,
                KeySearchProduct = "",
            };
            var response = (await HostContext.ResolveService<InvoiceApi>(Request).Any(getInvoiceSyncError))
                .ToSafeJson().FromJson<Sdk.Common.PagingDataSource<InvoiceDto>>();
            if (response.Data == null || !response.Data.Any())
            {
                return false;
            }

            var reSyncInvoice = response.Data.Select(invoice => invoice.ConvertTo<InvoiceDto>()).ToList();

            var multiSyncErrorInvoiceRequest = new MultiSyncErrorInvoice()
            {
                Invoices = reSyncInvoice.ToSafeJson(),
                Channels = channels.ToSafeJson(),
            };
            return HostContext.ResolveService<InvoiceApi>(Request).Post(multiSyncErrorInvoiceRequest).Equals(true);

        }

        private async Task<bool> ResyncAllProduct(ResyncChannelError req)
        {
            if (!req.IsResyncProducts)
            {
                return false;
            }

            return (await HostContext.ResolveService<ChannelApi>(Request).Post(new SyncErrorChannel
            {
                ChannelId = null
            })).Equals(true);
        }

        private string GetChannelTypeName(byte channelType)
        {
            switch (channelType)
            {
                case (byte)ChannelType.Shopee:
                    {
                        return "Shopee";
                    }

                case (byte)ChannelType.Lazada:
                    {
                        return "Lazada";
                    }

                case (byte)ChannelType.Tiki:
                    {
                        return "Tiki";
                    }

                case (byte)ChannelType.Sendo:
                    {
                        return "Sendo";
                    }
                case (byte)ChannelType.Tiktok:
                    {
                        return "Tiktok";
                    }
            }
            return null;
        }

        private enum SyncTabError
        {
            Product = 3,
            Invoice = 2,
            Order = 1
        }

        private class SyncErrorResponse<T>
        {
            public long Total { get; set; }
            public List<T> Data { get; set; }
        }

        private async Task<List<OmniChannelResponse>> GetChannelsByFilter(int retailerId, byte? channelType, List<int> types = null, List<long> channelIds = null)
        {
            var getByRetailerReq = new GetByRetailer
            {
                RetailerId = retailerId,
                Type = channelType,
                OnlyIsActive = true,
                Types = types ?? new List<int>(),
                ChannelIds = channelIds ?? new List<long>(),
            };
            return await HostContext.ResolveService<ChannelApi>(Request).Any(getByRetailerReq);
        }

        private async Task UpdateProductFlagRelateNormal(int retailerId, long kvProductId, bool value)
        {
            var connectionString = GetKvEntitiesConnectString();
            if (value == false)
            {
                var getProductReq = new GetProductByKvProductIds
                {
                    KvProductIds = new List<long>() { kvProductId },
                };
                var isStillRelate = (await HostContext.ResolveService<ChannelProductApi>(Request).Any(getProductReq)).Any();
                if (isStillRelate) return;
            }

            using (var db = await DbConnectionFactory.OpenAsync(connectionString))
            {
                var productRepo = new ProductRepository(db);
                var productRelations = new List<ProductRelation>()
                {
                    new ProductRelation()
                    {
                        KvProductId = kvProductId,
                    }
                };
                await productRepo.UpdateKvProductRelation(productRelations, value);
            }
        }

        private int GetChannelPosition(int channelType)
        {
            var channelTypeName = ((ChannelType)channelType).ToString();
            var position = typeof(ChannelPosition).GetFields().FirstOrDefault(x => x.Name.ToUpper() == channelTypeName.ToUpper())?.GetRawConstantValue() ?? 0;
            return (int)position;
        }

        #region Private method
        private bool ValidateRequest(GetProductByListChannelId req)
        {
            if (req.ChannelIdLst == null || req.ChannelIdLst.Count <= 0)
            {
                return false;
            }
            return true;
        }

        private async Task<Domain.Common.PagingDataSource<MapProduct>> BuildPagingDataSourceMapProduct(GetProductByListChannelId req)
        {
            var retVal = new Domain.Common.PagingDataSource<MapProduct>();
            var retailerId = CurrentRetailerId;
            var productMap = new List<MongoDb.ProductMapping>();
            if (!string.IsNullOrEmpty(req.KvProductSku))
            {
                productMap = await ProductMappingService
                    .GetByChannels(retailerId, req.ChannelIdLst, channelSkus: null, kvProductSearchTerm: req.KvProductSku);
                if (productMap.Count <= 0)
                {
                    return retVal;
                }
            }

            var listIds = productMap.Select(x => x.CommonProductChannelId).ToList();
            var productsDataSource = await ProductMongoService
                .GetAllByListChannelId(InternalContext.RetailerId, req.ChannelIdLst, req.Top, req.Skip, isStringItemId: false, req.ItemSkuLst, req.ItemSku, listIds, req.IsConnected, req.IsSyncSuccess);
            var products = productsDataSource.Data.Select(x => new MapProduct
            {
                Id = x.Id,
                BranchId = x.BranchId,
                ChannelId = x.ChannelId,
                ChannelType = x.ChannelType,
                CommonItemId = x.CommonItemId,
                CommonParentItemId = x.CommonParentItemId,
                IsConnected = x.IsConnected,
                ItemId = x.CommonItemId,
                ParentItemId = x.CommonParentItemId,
                ItemName = x.ItemName,
                ItemSku = x.ItemSku,
                RetailerCode = x.RetailerCode,
                RetailerId = x.RetailerId,
                Status = x.Status,
                StrItemId = x.StrItemId,
                StrParentItemId = x.StrParentItemId,
                Type = x.Type,
                SyncErrors = x.SyncErrors,
                ItemImages = x.ItemImages
            }).ToList();
            var itemIds = productsDataSource.Data.Select(x => x.StrItemId).ToList();
            Dictionary<string, MongoDb.ProductMapping> mappings;
            var prdMappings = await ProductMappingService.GetByChannels(InternalContext.RetailerId, req.ChannelIdLst, null, null, itemIds);
            try
            {
                mappings = prdMappings.ToDictionary(x => x.CommonProductChannelId, y => y);
            }
            catch (Exception ex)
            {
                prdMappings = prdMappings.GroupBy(x => x.CommonProductChannelId).Select(x => x.First()).ToList();
                mappings = prdMappings.ToDictionary(x => x.CommonProductChannelId, y => y);
                Logger.LogError(ex.Message, ex);
            }
            foreach (var p in products)
            {
                p.KvProductId = mappings.ContainsKey(p.CommonItemId) ? (mappings[p.CommonItemId]?.ProductKvId ?? 0) : 0;
                p.KvProductSku = mappings.ContainsKey(p.CommonItemId) ? mappings[p.CommonItemId]?.ProductKvSku : string.Empty;
                p.ErrorMessage = p.SyncErrors?.Select(x => x.Message)?.Join("; ");
                p.KvErrorMessage = p.SyncErrors?.Select(x => GeneralHelper.GetKvMessage(x.Message))?.Join("; ");
            }
            products = products.OrderBy(x => x.ItemName).ThenBy(x => x.ItemSku).ThenBy(x => x.CommonItemId).ToList();
            retVal.Total = productsDataSource.Total;
            retVal.Data = products;
            return retVal;
        }
        private void ProcessMappingData(List<Demo.OmniChannel.Domain.Model.OmniChannel> channelLst, PagingDataSource<ProductMapResponse> retVal, List<PriceBook> priceBookLst, List<PriceBookDetail> priceBooksDetail, List<ProductForChannelMapping> kvProductLst)
        {
            foreach (var channelItem in channelLst)
            {
                var productMapResponseByChannelLst = retVal.Data.Where(item => item.ChannelId == channelItem.Id).ToList();
                List<PriceBookDetail> pricebookDetailByChannel;
                foreach (var productMapItem in productMapResponseByChannelLst)
                {
                    if (channelItem?.BasePriceBookId != null)
                    {
                        var priceBook = priceBookLst.FirstOrDefault(item => item.Id == channelItem.BasePriceBookId);
                        if (priceBook == null)
                        {
                            channelItem.BasePriceBookId = 0;
                        }
                    }
                    else
                    {
                        channelItem.BasePriceBookId = 0;
                    }
                    pricebookDetailByChannel = priceBooksDetail.Where(x => x.PriceBookId == channelItem.BasePriceBookId).ToList();
                    var kvProductItem = kvProductLst.FirstOrDefault(item => item.Id == productMapItem.KvProductId);
                    if (kvProductItem != null)
                    {
                        productMapItem.KvProductName = kvProductItem.Name;
                        productMapItem.AttributeLabel = kvProductItem.AttributeValue;
                        productMapItem.Unit = kvProductItem.Unit;
                        productMapItem.BasePrice = kvProductItem.BasePrice;
                        productMapItem.Price = pricebookDetailByChannel.FirstOrDefault(x => x.ProductId == productMapItem.KvProductId)?.Price ?? productMapItem.BasePrice;
                        productMapItem.OnHand = kvProductItem.OnHand ?? 0;
                        productMapItem.Reserved = kvProductItem.Reserved ?? 0;
                        productMapItem.OnOrder = kvProductItem.OnOrder ?? 0;
                        productMapItem.KvProductFullName = kvProductItem.FullName;
                        productMapItem.ProductType = kvProductItem.ProductType;
                        productMapItem.KvProductSku = kvProductItem?.Code ?? productMapItem.KvProductSku;
                    }

                    EditUrlProp editUrlProp = new EditUrlProp(channelItem.Type, productMapItem.ParentItemId, channelItem?.IdentityKey, productMapItem?.SuperId,
                        AppSettings.Get($"{GetChannelTypeName(channelItem.Type)}EditProductUrl", ""));
                    productMapItem.EditChannelUrl = GeneralHelper.GenerateEditProductUrl(editUrlProp);
                }


            }
        }

        #endregion
        private async Task<List<object>> GetProductFromEs(KvProductSearchByRetail req)
        {
            var retailerId = CurrentRetailerId;

            var channel = await GetChannel(req.ChannelId);

            var listKvSkuMapping = await GetKvSkuMapping(channel, retailerId);

            var productsBranch = await GetProductBranchByEsSearch(req, channel, retailerId, listKvSkuMapping.Any() ? 200 : 50);

            if (listKvSkuMapping.Any())
            {
                productsBranch = productsBranch.Where(x => !listKvSkuMapping.Contains(x.Code)).ToList();
            }

            productsBranch = productsBranch.Take(50).ToList();

            var productIds = productsBranch.Select(x => x.Id).ToList();
            var priceBooksDetail = await GetPriceDetail(channel, retailerId, productIds);

            productsBranch = await GetMissingDataEs(productsBranch, retailerId, channel?.BranchId ?? 0);

            var result = SelectResultProduct(productsBranch, new List<ProductAttribute>(), priceBooksDetail);
            return result;
        }

        private async Task<List<ProductByBranch>> GetProductBranchByEsSearch(KvProductSearchByRetail req, OmniChannelDto channel, int retailerId, int limit = 50)
        {
            var productFilter = new ProductSearchFilter()
            {
                RetailerId = retailerId,
                BranchId = channel?.BranchId ?? 0,
                Keyword = req.Term,
                ProductTypes = GetProductTypes().Join(","),
                Shard = $"kventities{GroupId}",
                PageSize = limit,
            };
            var result = await ProductSearchClient.SearchProduct(productFilter);
            var productsBranch = result.FromJson<ProductByBranchEs>();
            return productsBranch.Data ?? new List<ProductByBranch>();
        }

        private async Task<List<ProductByBranch>> GetMissingDataEs(List<ProductByBranch> dataEs, int retailerId, int branchId)
        {
            if (!dataEs.Any()) return dataEs;

            List<ProductBranch> productBranches;
            var productIds = dataEs.Select(x => x.Id).ToList();

            var connectionString = GetKvEntitiesConnectString();
            using (var db = await DbConnectionFactory.OpenAsync(connectionString))
            {
                var productBranchRepo = new ProductBranchRepository(db);
                productBranches = await productBranchRepo.GetMissingDataEs(retailerId, branchId, productIds);
            }

            var productBranchDic = productBranches.ToDictionary(x => x.ProductId);
            foreach (var item in dataEs)
            {
                item.ProductAttributes = item.ProductAttributes ?? new List<ProductAttribute>();
                item.ProductSerials = item.ProductSerials ?? new List<ProductSerial>();
                item.ListProductUnit = item.ListProductUnit ?? new List<ProductUnit>();
                item.ProductBatchExpires = item.ProductBatchExpires ?? new List<ProductBatchExpireDto>();
                item.ProductShelves = item.ProductShelves ?? new List<ProductShelves>();

                if (productBranchDic.ContainsKey(item.Id))
                {
                    item.Cost = productBranchDic[item.Id].Cost;
                    item.OnHand = productBranchDic[item.Id].OnHand;
                    item.OnOrder = productBranchDic[item.Id].OnOrder;
                    item.Reserved = productBranchDic[item.Id].Reserved;
                    item.LatestPurchasePrice = productBranchDic[item.Id].LatestPurchasePrice;
                }
            }
            return dataEs;
        }

        private async Task<List<object>> GetProductFromDb(KvProductSearchByRetail req)
        {
            if (new OmniSearchProductV2Toggle(AppSettings).Enable(CurrentRetailerId, GroupId))
            {
                return await GetProductV2(req);
            }
            return await GetProductV1(req);
        }

        private async Task<List<object>> GetProductV1(KvProductSearchByRetail req)
        {
            var retailerId = CurrentRetailerId;

            var channel = await GetChannel(req.ChannelId);
            var priceBooksDetail = await GetPriceDetail(channel, retailerId);
            var listKvSkuMapping = await GetKvSkuMapping(channel, retailerId);

            var connectionString = GetKvEntitiesConnectString();
            using (var db = await DbConnectionFactory.OpenAsync(connectionString))
            {
                var productTypes = GetProductTypes();
                var productRepo = new ProductRepository(db);
                var data = await productRepo.QueryGetProducts(retailerId, channel?.BranchId ?? 0, req.Term, 500,
                    productTypes);

                if (listKvSkuMapping.Any())
                {
                    data = data.Where(x => !listKvSkuMapping.Contains(x.Code)).ToList();
                }

                var productIds = data.Select(p => p.Id).ToList();
                var productAttributes = await productRepo.GetProductAttributesByProductIds(retailerId, productIds);

                var result = SelectResultProduct(data, productAttributes, priceBooksDetail);
                return result.Take(50).ToList();
            }
        }

        private async Task<List<object>> GetProductV2(KvProductSearchByRetail req)
        {
            var retailerId = CurrentRetailerId;

            var channel = await GetChannel(req.ChannelId);
            var listKvSkuMapping = await GetKvSkuMapping(channel, retailerId);

            List<ProductByBranch> result;

            var connectionString = GetKvEntitiesConnectString();
            using (var db = await DbConnectionFactory.OpenAsync(connectionString))
            {
                var productRepo = new ProductRepository(db);
                var optimizeSqlByIds = new OptimizeByIdsToggle(AppSettings).Enable(CurrentRetailerId, GroupId);
                result = await productRepo.QueryGetProductsV2(retailerId, channel?.BranchId ?? 0, req.Term, 50,
                    listKvSkuMapping, optimizeSqlByIds);
            }

            var productIds = result.Select(x => x.Id).ToList();
            var priceBooksDetail = await GetPriceDetail(channel, retailerId, productIds);

            return SelectResultProduct(result, new List<ProductAttribute>(), priceBooksDetail);
        }

        private async Task<List<string>> GetKvSkuMapping(OmniChannelDto channel, int retailerId)
        {
            var activeFeatureMultiSku = false;
            var multiSkuConfig = AppSettings.Get<UseMultiMappingSKU>("UseMultiMappingSKU");
            if (channel != null)
            {
                activeFeatureMultiSku = SelfAppConfig.CheckActiveFeatureByPlatform(multiSkuConfig, retailerId, channel.Type);
            }
            if ((channel?.Type != (byte)ChannelType.Shopee &&
                channel?.Type != (byte)ChannelType.Tiktok &&
                channel?.Type != (byte)ChannelType.Lazada) || !activeFeatureMultiSku)
            {
                var getProductMappingReq = new GetProductMapping
                {
                    ChannelId = channel?.Id,
                    KvProductSkus = new List<string>(),
                    ChannelProductSkus = new List<string>(),
                };

                var mappingProducts = (await HostContext.ResolveService<ProductMappingApi>(Request).Any(getProductMappingReq)).ToSafeJson()
                    .FromJson<List<ProductMapping>>();
                var listKvSkuMapping = mappingProducts.Select(x => x.ProductKvSku).ToList();
                return listKvSkuMapping;
            }

            return new List<string>();
        }

        private async Task<List<PriceBookDetail>> GetPriceDetail(OmniChannelDto channel, int retailerId, List<long> productIds = null)
        {
            var connectionString = GetKvEntitiesConnectString();
            if (channel?.BasePriceBookId == null || channel.BasePriceBookId.Value <= 0) return null;
            using (var db = await DbConnectionFactory.OpenAsync(connectionString))
            {
                var priceBookRepo = new PriceBookRepository(db);
                var priceBook = await priceBookRepo.GetById(retailerId, channel.BasePriceBookId ?? 0);
                if (priceBook == null || priceBook.Id <= 0 || !priceBook.IsActive || priceBook.StartDate > DateTime.Now || priceBook.EndDate < DateTime.Now)
                {
                    channel.BasePriceBookId = 0;
                }

                return await priceBookRepo.GetDetailById(retailerId, channel.BasePriceBookId ?? 0, productIds);
            }
        }

        private async Task<OmniChannelDto> GetChannel(long channelId)
        {
            var getChannelByIdReq = new GetById
            {
                Id = channelId,
            };

            return await HostContext.ResolveService<ChannelApi>(Request).Get(getChannelByIdReq);
        }

        private List<object> SelectResultProduct(List<ProductByBranch> productsBranch, List<ProductAttribute> productAttributes, List<PriceBookDetail> priceBooksDetail)
        {
            var result = productsBranch.Select(p => new
            {
                p.Id,
                Name = EnumHelper.AddSpaceBetweenProductNameAddAttribute(p.AttributedName),
                ProductName = p.Name,
                SortName = EnumHelper.RelaceProductName(EnumHelper.AddSpaceBetweenProductNameAddAttribute(p.FullName), p.Unit),
                AttributeLabel = string.Join(" - ", productAttributes.Where(x => x.ProductId == p.Id).OrderBy(m => m.CreatedDate).Select(x => x.Value).ToArray()),
                p.Unit,
                p.Code,
                p.BasePrice,
                Price = priceBooksDetail?.FirstOrDefault(x => x.ProductId == p.Id)?.Price ?? p.BasePrice,
                p.OnHand,
                p.OnOrder,
                p.Cost,
                p.LatestPurchasePrice,
                p.MasterUnitId,
                p.Reserved,
                p.IsLotSerialControl,
                p.IsBatchExpireControl,
                p.ProductSerials,
                p.ProductBatchExpires,
                p.ConversionValue,
                p.ProductType,
                p.Image,
                p.AllowsSale,
                p.CategoryId,
                p.ListProductUnit,
                p.ProductShelves,
                p.IsMedicineProduct,
                p.RegistrationNo,
                p.ActiveElement,
                p.Content
            }).ToList<object>();
            return result;
        }

        private int[] GetProductTypes()
        {
            var productTypes = new[] { (int)ProductType.Purchased, (int)ProductType.Manufactured, (int)ProductType.Service };
            return productTypes;
        }

        private List<InvoiceDetailDto> UpdateInvoiceDetail(InvoiceDto invoiceDto, List<MasterProduct> masterIds,
         List<ProductBatchExpiresInfo> productBatchExpireInfo, List<ProductSerial> productSerials,
         List<ProductImage> productImages)
        {
            var orderDetailsNew = invoiceDto.InvoiceDetails;

            if (orderDetailsNew.Any(x => x.UseProductBatchExpire) && productBatchExpireInfo != null)
            {
                foreach (var detail in orderDetailsNew)
                {
                    if (detail.UseProductBatchExpire && masterIds.Any(x => x.ProductId == detail.ProductId))
                    {
                        var masterProduct = masterIds.FirstOrDefault(p => p.ProductId == detail.ProductId);
                        detail.ProductBatchExpires = productBatchExpireInfo
                            .Where(x => x.ProductId == masterProduct?.MasterProductId && x.BranchId == invoiceDto.BranchId)
                            .Select(x => new ProductBatchExpiresInfo()
                            {
                                Id = x.Id,
                                ProductId = x.ProductId,
                                BranchId = x.BranchId,
                                OnHand = Math.Round(x.OnHand / masterProduct?.ConversionValue ?? 1, 3),
                                SystemCount = x.SystemCount,
                                BatchName = x.BatchName,
                                ExpireDate = x.ExpireDate,
                                FullNameVirgule = x.FullNameVirgule,
                                Status = x.Status,
                                ReturnQuantity = x.ReturnQuantity,
                                SellQuantity = x.SellQuantity,
                                PurchaseOrderDetailId = x.PurchaseOrderDetailId
                            }).ToList();
                    }
                    else if (detail.UseProductBatchExpire)
                    {
                        detail.ProductBatchExpires = productBatchExpireInfo.Where(x => x.ProductId == detail.ProductId && x.BranchId == invoiceDto.BranchId).ToList();
                    }
                }
            }

            if (orderDetailsNew.Any(x => x.UseProductSerial) && productSerials.Any())
            {
                foreach (var detail in orderDetailsNew)
                {
                    detail.ProductSerials = productSerials
                        .Where(p => p.ProductId == detail.ProductId && p.BranchId == invoiceDto.BranchId && p.Status == 1)
                        .Select(p => p.SerialNumber).ToList();
                }
            }

            foreach (var detail in orderDetailsNew)
            {
                detail.ProductImages = productImages.Where(x => x.ProductId == detail.ProductId).Select(x => x.Image).ToList();
            }

            return orderDetailsNew;
        }

        public List<string> GetErrorMappingIdsFromMessage<T>(T transaction)
        {
            Type type = typeof(T);
            var errorMessage = string.Empty;
            IEnumerable<string> trackKeys = new string[] { };
            var errorMappingProductIDs = new List<string>();
            var errorNotMappingMessage = "chưa liên kết với hàng hóa nào trên Demo";
            if (type == typeof(OrderDto))
            {
                var order = transaction.ConvertTo<OrderDto>();
                errorMessage = order.ErrorMessage;
                if (order.OrderDetails != null && order.OrderDetails.Count > 0 && !string.IsNullOrEmpty(order.ErrorMessage) || !order.ErrorMessage.Contains(errorNotMappingMessage))
                {
                    trackKeys = order.OrderDetails.Select(detail => $"{detail.ProductChannelId}|{detail.ParentChannelProductId}|{detail.ProductChannelSku}");
                }
            }

            if (type == typeof(InvoiceDto))
            {
                var invoice = transaction.ConvertTo<InvoiceDto>();
                errorMessage = invoice.ErrorMessage;
                if (invoice.InvoiceDetails != null && invoice.InvoiceDetails.Count > 0 && !string.IsNullOrEmpty(invoice.ErrorMessage) || !invoice.ErrorMessage.Contains(errorNotMappingMessage))
                {
                    trackKeys = invoice.InvoiceDetails.Select(detail => $"{detail.ProductChannelId}|{detail.ParentChannelProductId}|{detail.ProductChannelSku}");
                }
            }
            var nullId = "0";
            foreach (var trackKey in trackKeys)
            {
                var groupIds = trackKey.Split('|');
                foreach (var id in groupIds)
                {
                    var isValueValid = !string.IsNullOrEmpty(id) && !nullId.Equals(id);
                    if (isValueValid && errorMessage.Contains(id) && !errorMappingProductIDs.Contains(id))
                    {
                        errorMappingProductIDs.Add(id);
                        break;
                    }
                }
            }

            return errorMappingProductIDs;
        }

    }

    public class SyncError
    {
        public ChannelResponse Channel { get; set; }
    }
    public class ProductSyncError : SyncError
    {
        public List<ChannelProduct> ChannelProducts { get; set; }
    }
    public class ProductSyncFailError : SyncError
    {
        public ChannelProduct ChannelProduct { get; set; }
    }
    public class RetailerOutlet
    {
        public int ChannelId { get; set; }
        public int RetailerId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Position { get; set; }
        public string IdentityKey { get; set; }
        public bool IsActive { get; set; }
        public bool IsExpireWarning { get; set; }
    }

    public class ChannelPosition
    {
        public const int Tiktok = 0;
        public const int Tiki = 1;
        public const int Lazada = 2;
        public const int Shopee = 3;
        public const int Sendo = 4;
    }

    public class ProductForChannelMappingResponse
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public decimal BasePrice { get; set; }
        public double? OnHand { get; set; }
        public double? OnOrder { get; set; }
        public double? Reserved { get; set; }
        public string Unit { get; set; }
        public string AttributeValue { get; set; }
        public List<ProductMapping> ProductMappings { get; set; }
    }

    public class ProductByBranchEs
    {
        [DataMember(Name = "data")]
        public List<ProductByBranch> Data { get; set; }
    }
}
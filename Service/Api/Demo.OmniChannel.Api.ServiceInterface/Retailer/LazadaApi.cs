﻿using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.Api.ServiceModel.Retailer;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Interfaces;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.CoreDemoContext.Interfaces;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.ShareKernel.Dto;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Resource;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Api.ServiceModel.Request;

namespace Demo.OmniChannel.Api.ServiceInterface.Retailer
{
    public class LazadaApi : BaseApi
    {
        public ChannelApi ChannelApi { get; set; }
        private readonly ISaleChannelInternalClient _saleChannelInternalClient;
        private readonly IDemoOmniChannelCoreContext _DemoOmniChannelCoreContext;
        private readonly IChannelClient _channelClient;

        public LazadaApi(IChannelClient channelClient,
            ISaleChannelInternalClient saleChannelInternalClient,
            IDemoOmniChannelCoreContext DemoOmniChannelCoreContext,
            ILogger<LazadaApi> logger) : base(logger)
        {
            _saleChannelInternalClient = saleChannelInternalClient;
            _DemoOmniChannelCoreContext = DemoOmniChannelCoreContext;
            _channelClient = channelClient;
        }
        public LazadaApi(ILogger<LazadaApi> logger) : base(logger)
        {
        }

        #region public GET methods
        public async Task<object> Get(LazadaUpdateLinking req)
        {
            var client = _channelClient.GetClient((byte)ChannelType.Lazada, AppSettings);
            var authen = new ChannelAuth
            {
                Code = req.Code
            };
            var retval = await client.GetShopInfo(authen, InternalContext.ConvertTo<Demo.OmniChannel.Sdk.Common.KvInternalContext>());
            return retval.ConvertTo<LazadaShopInfoResponseV2>();
        }
        public object Get(LazadaAuthConfig req)
        {

            var lazadaAppId = AppSettings.Get<string>("LazadaClientAppId");
            var lazadaCallBackUrl = AppSettings.Get<string>("LazadaCallBack");

            return new LazadaAuthConfigResponse
            {
                ClientId = lazadaAppId,
                CallBackUrl = lazadaCallBackUrl
            };
        }
        public async Task<Domain.Common.PagingDataSource<LazadaResponse>> Get(LazadaApps req)
        {
            var result = new Domain.Common.PagingDataSource<LazadaResponse>();
            var newReq = new GetByRetailer
            {
                OnlyIsActive = false,
                RetailerId = InternalContext.RetailerId,
                Type = (byte)ChannelType.Lazada
            };
            var lazadaLst = await ChannelApi.Any(newReq);
            if (lazadaLst == null)
            {
                return result;
            }
            var lazada = lazadaLst.Select(p =>
            {
                var item = p.ConvertTo<LazadaResponse>();
                item.TotalProductConnected = p.TotalProductConnected;
                item.Name = p.Name;
                item.OmniChannelSchedules = p.OmniChannelSchedules.Map(x => x.ConvertTo<OmniChannelSchedule>()).ToList();
                return item;
            }).ToList();
            var branchIds = lazada.Where(item => item.BranchId.HasValue).Select(p => p.BranchId.Value);
            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            using (var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var branchRepository = new BranchRepository(db);
                var lstBranch = (await branchRepository.GetListBranchByIds(CurrentRetailerId, branchIds.ToList()))
                    .ToDictionary(item => item.Id, item => item.Name);
                var priceBookIds = lazada.Where(item => item.PriceBookId.HasValue).Select(p => p.PriceBookId.Value);
                var basePriceBookIds = lazada.Where(item => item.BasePriceBookId.HasValue).Select(p => p.BasePriceBookId.Value);
                var priceBookRepository = new PriceBookRepository(db);
                var lstPriceBook = (await priceBookRepository.GetByFilterAsync(CurrentRetailerId, priceBookIds.ToList(), true))
                    .ToDictionary(item => item.Id, item => item.Name);
                var lstBasePriceBook = (await priceBookRepository.GetByFilterAsync(CurrentRetailerId, basePriceBookIds.ToList(), true))
                    .ToDictionary(item => item.Id, item => item.Name);
                lazada.ForEach(x =>
                {
                    x.BranchName = lstBranch.ContainsKey(x.BranchId ?? 0) ? lstBranch[x.BranchId ?? 0] : "";
                    if (x.PriceBookId.HasValue)
                    {
                        x.PriceBookName = lstPriceBook.ContainsKey(x.PriceBookId ?? 0) ? lstPriceBook[x.PriceBookId ?? 0] : "Bảng giá chung";
                    }
                    if (x.BasePriceBookId.HasValue)
                    {
                        x.BasePriceBookName = lstBasePriceBook.ContainsKey(x.BasePriceBookId ?? 0) ? lstBasePriceBook[x.BasePriceBookId ?? 0] : "Bảng giá chung";
                    }
                    x.IsSyncPrice = x.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncPrice);
                    x.IsSyncOnHand = x.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncOnhand);
                    x.SyncOnHandFormulaName = x.IsSyncOnHand ? $"{KvMessage.ecommSellQty} = {EnumHelper.ToDescription((EcommerceSellQtyFormula)x.SyncOnHandFormula)}" : string.Empty;
                    x.IsSyncOrder = x.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncOrder);
                });
                result.Data = lazada;
                result.Total = lazada.Count;
            }
            return result;
        }
        public async Task<LazadaResponse> Get(LazadaAppGet req)
        {
            var result = await ChannelApi.Get(new GetById
            {
                Id = req.Id
            });
            var res = result.ConvertTo<LazadaResponse>();
            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            using (var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var branchRepository = new BranchRepository(db);
                var priceBookRepository = new PriceBookRepository(db);

                var branch = await branchRepository.GetByIdAsync(res.BranchId ?? 0);
                if (branch == null)
                {
                    res.BranchId = -1;
                }
                res.BranchName = branch?.Name;
                if (result.OmniChannelSchedules != null && result.OmniChannelSchedules.Any())
                {
                    res.OmniChannelSchedules = result.OmniChannelSchedules.Map(x => x.ConvertTo<OmniChannelSchedule>());
                    res.IsSyncOnHand = result.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncOnhand);
                    res.IsSyncPrice = result.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncPrice);
                    res.IsSyncOrder = result.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncOrder);
                    res.SyncOnHandFormula = res.IsSyncOnHand ? result.SyncOnHandFormula : (byte)0;
                    res.SyncOrderFormula = result.SyncOrderFormula;
                    if (res.PriceBookId.HasValue)
                    {
                        var lstPriceBook = (await priceBookRepository.WhereAsync(p => p.RetailerId == CurrentRetailerId
                        && p.IsActive && p.StartDate < DateTime.Now && p.EndDate > DateTime.Now))
                        .ToDictionary(item => item.Id, item => item.Name);
                        var isActivePromoPriceBook = lstPriceBook.ContainsKey(res.PriceBookId.Value);
                        var pricebookName = isActivePromoPriceBook ? lstPriceBook[res.PriceBookId.Value] : string.Empty;
                        if (!isActivePromoPriceBook || pricebookName.Contains("{DEL"))
                        {
                            res.PriceBookId = 0;
                            res.PriceBookName = "Bảng giá chung";
                        }
                        else
                        {
                            res.PriceBookName = lstPriceBook[res.PriceBookId.Value];
                        }
                    }
                }
            }
            var cacheKey = string.Format(KvConstant.IsRunningGetProductKey, req.Id);
            if (CacheClient.Get<bool>(cacheKey))
            {
                res.IsRunningGetProduct = true;
            }
            return res;
        }
        #endregion

        #region public POST methods
        public async Task<object> Post(LazadaEnableChannel req)
        {
            var client = _channelClient.GetClient((byte)ChannelType.Lazada, AppSettings);
            var token = await client.CreateToken(req.Code, null, null);
            if (token == null)
            {
                throw new KvValidateLazadaException(KVMessage.GlobalErrorSummary);
            }
            var auth = new ChannelAuth
            {
                AccessToken = token.AccessToken,
                RefreshToken = token.RefreshToken,
                ExpiresIn = token.ExpiresIn,
                RefreshExpiresIn = token.RefreshExpiresIn
            };
            var retval = await client.GetShopInfo(auth, InternalContext.ConvertTo<Demo.OmniChannel.Sdk.Common.KvInternalContext>(), true);
            if (retval == null)
            {
                throw new KvValidateLazadaException(KVMessage.GlobalErrorSummary);
            }
            if (string.IsNullOrEmpty(req.IdentityKey) ||
                !req.IdentityKey.Equals(retval.IdentityKey, StringComparison.OrdinalIgnoreCase))
            {
                throw new KvValidateLazadaException(string.Format(KVMessage.OmniChannelReAuthenAnotherShopFail, req.ShopName));
            }
            var enableChannel = new EnableChannel
            {
                Auth = auth.ConvertTo<Demo.OmniChannel.Domain.Model.OmniChannelAuth>(),
                BranchName = req.BranchName,
                ChannelId = req.ChannelId,
                PriceBookName = req.PriceBookName,
                SalePriceBookName = req.SalePriceBookName,
                Email = retval.Email,
                ShopName = req.ShopName,
                PlatformId = 0
            };
            return await ChannelApi.Post(enableChannel);
        }

        public async Task<bool> Post(LazadaRemoveAuth req)
        {
            var removeChannelAuth = new RemoveChannelAuth
            {
                AuthId = req.AuthId
            };
            return await ChannelApi.Post(removeChannelAuth);
        }
        public async Task<LazadaResponse> Post(LazadaAppPost req)
        {
            await ValidateLazadaConnection(req);
            req.LazadaApp.RetailerId = CurrentRetailerId;
            req.LazadaApp.Type = (byte)ChannelType.Lazada;
            var r = await ChannelApi.Post(new ChannelCreateOrUpdate()
            {
                AuthId = req.AuthId,
                IsIgnoreAuditTrail = false,
                IsSyncOnHand = req.IsSyncOnHand,
                IsSyncOrder = req.IsSyncOrder,
                IsSyncPrice = req.IsSyncPrice,
                Channel = req.LazadaApp
            });
            var result = r.ConvertTo<LazadaResponse>();
            if (!string.IsNullOrWhiteSpace(r.IdentityKey))
            {
                var coreContext = await _DemoOmniChannelCoreContext.CreateOmniChannelCoreContext(CurrentRetailerId, InternalContext.BranchId, InternalContext.Group.ConnectionString);
                result.SaleChannelId = await _saleChannelInternalClient.CreateOrUpdateSaleChannel(coreContext, CurrentRetailerId, (byte)ChannelType.Lazada, r.Id, r.Name);
            }
            return result;
        }
        #endregion

        #region public DELETE methods
        public async Task<object> Delete(LazadaAppGet req)
        {
            if (req.Id <= 0)
            {
                throw new KvValidateLazadaException(KVMessage.NotFound);
            }
            var exist = await ChannelApi.Get(new GetById { Id = req.Id });
            if (exist == null)
            {
                throw new KvException(KVMessage.NotFound);
            }
            await ChannelApi.Delete(new GetById { Id = req.Id });
            var accessTokenKey = $"cache:lazada:accesstoken_{exist.Id}_{exist.RetailerId}_{exist.BranchId}";
            var refeshTokenKey = $"cache:lazada:refeshtoken_{exist.Id}_{exist.BranchId}_{exist.RetailerId}";
            CacheClient.Remove(accessTokenKey);
            CacheClient.Remove(refeshTokenKey);
            return new { req.Id, Message = KVMessage.GlobalDeleteSuccess };
        }
        #endregion

        #region Private method
        private async Task<bool> ValidateLazadaConnection(LazadaAppPost req)
        {
            if (req.LazadaApp == null)
            {
                throw new KvValidateLazadaException(KVMessage.GlobalErrorSummary);
            }
            if (req.IsSyncPrice && req.LazadaApp.BasePriceBookId == null)
            {
                throw new KvValidateLazadaException(KVMessage.LazadaEmptyBasePriceBook);
            }
            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            using (var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var branchRepository = new BranchRepository(db);
                var existBranch = await branchRepository.GetByIdAsync(req.LazadaApp.BranchId);
                if (existBranch == null || (existBranch.LimitAccess ?? false))
                {
                    throw new KvValidateBranchException(KVMessage.LazadaEmptyBranch);
                }
            }
            if (!KvPosSetting.SellAllowOrder && (req.LazadaApp.SyncOnHandFormula == (byte)EcommerceSellQtyFormula.SubOrder || req.LazadaApp.SyncOnHandFormula == (byte)EcommerceSellQtyFormula.SubOrderAddSupplierOrder))
            {
                throw new KvValidateLazadaException(KVMessage.EcommNotEnableSellAllowOrder);
            }
            if (!KvPosSetting.UseOrderSupplier && (req.LazadaApp.SyncOnHandFormula == (byte)EcommerceSellQtyFormula.AddSupplierOrder || req.LazadaApp.SyncOnHandFormula == (byte)EcommerceSellQtyFormula.SubOrderAddSupplierOrder))
            {
                throw new KvValidateLazadaException(KVMessage.EcommNotEnableUseOrderSupplier);
            }
            return true;
        }

        #endregion
    }
}

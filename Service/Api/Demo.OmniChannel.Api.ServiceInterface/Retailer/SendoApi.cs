﻿using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.Api.ServiceModel.Retailer;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.CoreDemoContext.Interfaces;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.ShareKernel.Dto;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Resource;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Api.ServiceModel.Request;

namespace Demo.OmniChannel.Api.ServiceInterface.Retailer
{
    public class SendoApi : BaseApi
    {
        public ChannelApi ChannelApi { get; set; }
        private readonly ISaleChannelInternalClient _saleChannelInternalClient;
        private readonly IDemoOmniChannelCoreContext _DemoOmniChannelCoreContext;
        public SendoApi(ILogger<SendoApi> logger) : base(logger)
        {
        }

        public SendoApi(ISaleChannelInternalClient saleChannelInternalClient,
            IDemoOmniChannelCoreContext DemoOmniChannelCoreContext,
            ILogger<LazadaApi> logger) : base(logger)
        {
            _saleChannelInternalClient = saleChannelInternalClient;
            _DemoOmniChannelCoreContext = DemoOmniChannelCoreContext;
        }

        #region public POST methods
        public async Task<SendoResponse> Post(SendoAppPost req)
        {
            await ValidateSendoConnection(req);

            req.SendoApp.RetailerId = CurrentRetailerId;
            req.SendoApp.Type = (byte)ChannelType.Sendo;
            req.SendoApp.RetailerId = CurrentRetailerId;
            req.SendoApp.Type = (byte)ChannelType.Sendo;
            var r = await ChannelApi.Post(new ChannelCreateOrUpdate()
            {
                AuthId = req.AuthId,
                IsIgnoreAuditTrail = false,
                IsSyncOnHand = req.IsSyncOnHand,
                IsSyncOrder = req.IsSyncOrder,
                IsSyncPrice = req.IsSyncPrice,
                Channel = req.SendoApp
            });
            var result = r.ConvertTo<SendoResponse>();
            if (!string.IsNullOrWhiteSpace(r.IdentityKey))
            {
                var coreContext = await _DemoOmniChannelCoreContext.CreateOmniChannelCoreContext(CurrentRetailerId, InternalContext.BranchId, InternalContext.Group.ConnectionString);
                result.SaleChannelId = await _saleChannelInternalClient.CreateOrUpdateSaleChannel(coreContext, CurrentRetailerId, (byte)ChannelType.Sendo, r.Id, r.Name);
            }
            return result;
        }
        public async Task<bool> Post(SendoEnableChannel req)
        {
            var auth = new Demo.OmniChannel.Sdk.RequestDtos.ChannelAuthRequest
            {
                Id = req.AuthId
            };
            var enableChannel = new EnableChannel
            {
                Auth = auth.ConvertTo<Demo.OmniChannel.Domain.Model.OmniChannelAuth>(),
                BranchName = req.BranchName,
                ChannelId = req.ChannelId,
                PriceBookName = req.PriceBookName,
                SalePriceBookName = req.SalePriceBookName,
                Email = string.Empty,
                ShopName = req.ShopName,
                PlatformId = 0
            };
            return await ChannelApi.Post(enableChannel);
        }
        public async Task<bool> Post(SendoRemoveAuth req)
        {
            var removeChannelAuth = new RemoveChannelAuth
            {
                AuthId = req.AuthId
            };
            return await ChannelApi.Post(removeChannelAuth);
        }
        public async Task<object> Post(SendoCreateAuth req)
        {
            ValidateCreateAuth(req);
            CreateAuthSendo newRequest = new CreateAuthSendo
            {
                ChannelId = req.ChannelId,
                ChannelName = req.ChannelName,
                SecretKey = req.SecretKey,
                ShopKey = req.ShopKey
            };
            return await ChannelApi.Post(newRequest);
        }
        #endregion
        #region public GET methods
        public async Task<Domain.Common.PagingDataSource<SendoResponse>> Get(SendoApps req)
        {
            var result = new Domain.Common.PagingDataSource<SendoResponse>();
            var newReq = new GetByRetailer
            {
                OnlyIsActive = false,
                RetailerId = InternalContext.RetailerId,
                Type = (byte)ChannelType.Sendo
            };
            var lazadaLst = await ChannelApi.Any(newReq);
            if (lazadaLst == null)
            {
                return result;
            }
            var lazada = lazadaLst.Select(p =>
            {
                var item = p.ConvertTo<SendoResponse>();
                item.TotalProductConnected = p.TotalProductConnected;
                item.Name = p.Name;
                item.OmniChannelSchedules = p.OmniChannelSchedules.Map(x => x.ConvertTo<OmniChannelSchedule>()).ToList();
                return item;
            }).ToList();
            var branchIds = lazada.Where(item => item.BranchId.HasValue).Select(p => p.BranchId.Value);
            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            using (var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var branchRepository = new BranchRepository(db);
                var lstBranch = (await branchRepository.GetListBranchByIds(CurrentRetailerId, branchIds.ToList()))
                    .ToDictionary(item => item.Id, item => item.Name);
                var priceBookIds = lazada.Where(item => item.PriceBookId.HasValue).Select(p => p.PriceBookId.Value);
                var basePriceBookIds = lazada.Where(item => item.BasePriceBookId.HasValue).Select(p => p.BasePriceBookId.Value);
                var priceBookRepository = new PriceBookRepository(db);
                var lstPriceBook = (await priceBookRepository.GetByFilterAsync(CurrentRetailerId, priceBookIds.ToList(), true))
                    .ToDictionary(item => item.Id, item => item.Name);
                var lstBasePriceBook = (await priceBookRepository.GetByFilterAsync(CurrentRetailerId, basePriceBookIds.ToList(), true))
                    .ToDictionary(item => item.Id, item => item.Name);
                lazada.ForEach(x =>
                {
                    x.BranchName = lstBranch.ContainsKey(x.BranchId ?? 0) ? lstBranch[x.BranchId ?? 0] : "";
                    if (x.PriceBookId.HasValue)
                    {
                        x.PriceBookName = lstPriceBook.ContainsKey(x.PriceBookId ?? 0) ? lstPriceBook[x.PriceBookId ?? 0] : "Bảng giá chung";
                    }
                    if (x.BasePriceBookId.HasValue)
                    {
                        x.BasePriceBookName = lstBasePriceBook.ContainsKey(x.BasePriceBookId ?? 0) ? lstBasePriceBook[x.BasePriceBookId ?? 0] : "Bảng giá chung";
                    }
                    x.IsSyncPrice = x.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncPrice);
                    x.IsSyncOnHand = x.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncOnhand);
                    x.SyncOnHandFormulaName = x.IsSyncOnHand ? $"{KvMessage.ecommSellQty} = {EnumHelper.ToDescription((EcommerceSellQtyFormula)x.SyncOnHandFormula)}" : string.Empty;
                    x.IsSyncOrder = x.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncOrder);
                });
                result.Data = lazada;
                result.Total = lazada.Count;
            }
            return result;
        }
        public async Task<SendoResponse> Get(SendoAppGet req)
        {
            var result = await ChannelApi.Get(new GetById
            {
                Id = req.Id
            });
            var res = result.ConvertTo<SendoResponse>();
            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            using (var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var branchRepository = new BranchRepository(db);
                var priceBookRepository = new PriceBookRepository(db);

                var branch = await branchRepository.GetByIdAsync(res.BranchId ?? 0);
                if (branch == null)
                {
                    res.BranchId = -1;
                }
                res.BranchName = branch?.Name;
                if (result.OmniChannelSchedules != null && result.OmniChannelSchedules.Any())
                {
                    res.OmniChannelSchedules = result.OmniChannelSchedules.Map(x => x.ConvertTo<OmniChannelSchedule>());
                    res.IsSyncOnHand = result.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncOnhand);
                    res.IsSyncPrice = result.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncPrice);
                    res.IsSyncOrder = result.OmniChannelSchedules.Any(y => y.Type == (byte)ScheduleType.SyncOrder);
                    res.SyncOnHandFormula = res.IsSyncOnHand ? result.SyncOnHandFormula : (byte)0;
                    res.SyncOrderFormula = result.SyncOrderFormula;
                    if (res.PriceBookId.HasValue)
                    {
                        var lstPriceBook = (await priceBookRepository.WhereAsync(p => p.RetailerId == CurrentRetailerId
                        && p.IsActive && p.StartDate < DateTime.Now && p.EndDate > DateTime.Now))
                        .ToDictionary(item => item.Id, item => item.Name);
                        var isActivePromoPriceBook = lstPriceBook.ContainsKey(res.PriceBookId.Value);
                        var pricebookName = isActivePromoPriceBook ? lstPriceBook[res.PriceBookId.Value] : string.Empty;
                        if (!isActivePromoPriceBook || pricebookName.Contains("{DEL"))
                        {
                            res.PriceBookId = 0;
                            res.PriceBookName = "Bảng giá chung";
                        }
                        else
                        {
                            res.PriceBookName = lstPriceBook[res.PriceBookId.Value];
                        }
                    }
                }
            }
            var cacheKey = string.Format(KvConstant.IsRunningGetProductKey, req.Id);
            if (CacheClient.Get<bool>(cacheKey))
            {
                res.IsRunningGetProduct = true;
            }
            return res;
        }
        #endregion

        #region public DELETE methods
        public async Task<object> Delete(SendoAppGet req)
        {
            if (req.Id <= 0)
            {
                throw new KvValidateSendoException(KVMessage.NotFound);
            }
            var exist = await ChannelApi.Get(new GetById { Id = req.Id });
            if (exist == null)
            {
                throw new KvException(KVMessage.NotFound);
            }
            await ChannelApi.Delete(new GetById { Id = req.Id });
            return new { req.Id, Message = KVMessage.GlobalDeleteSuccess };
        }
        #endregion
        #region Private method
        private void ValidateCreateAuth(SendoCreateAuth req)
        {
            var fieldNotInputs = new List<string>();

            if (string.IsNullOrEmpty(req.ChannelName)) fieldNotInputs.Add(KVMessage.SendoChannelName);

            if (string.IsNullOrEmpty(req.ShopKey)) fieldNotInputs.Add(KVMessage.SendoShopkey);

            if (string.IsNullOrEmpty(req.SecretKey)) fieldNotInputs.Add(KVMessage.SendoSecretKey);

            if (fieldNotInputs.Any())
            {
                throw new KvValidateException(KVMessage.SendoRequireInput.Fmt(string.Join(", ", fieldNotInputs)));
            }

            if (req.ChannelName.Length > 50) throw new KvValidateException(KVMessage.SendoLimitLenght.Fmt(KVMessage.SendoChannelName, 50));
        }
        private async Task<bool> ValidateSendoConnection(SendoAppPost req)
        {
            if (req.SendoApp == null)
            {
                throw new KvValidateLazadaException(KVMessage.GlobalErrorSummary);
            }
            if (req.IsSyncPrice && req.SendoApp.BasePriceBookId == null)
            {
                throw new KvValidateLazadaException(KVMessage.LazadaEmptyBasePriceBook);
            }
            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            using (var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var branchRepository = new BranchRepository(db);
                var existBranch = await branchRepository.GetByIdAsync(req.SendoApp.BranchId);
                if (existBranch == null || (existBranch.LimitAccess ?? false))
                {
                    throw new KvValidateBranchException(KVMessage.LazadaEmptyBranch);
                }
            }
            if (!KvPosSetting.SellAllowOrder && (req.SendoApp.SyncOnHandFormula == (byte)EcommerceSellQtyFormula.SubOrder || req.SendoApp.SyncOnHandFormula == (byte)EcommerceSellQtyFormula.SubOrderAddSupplierOrder))
            {
                throw new KvValidateLazadaException(KVMessage.EcommNotEnableSellAllowOrder);
            }
            if (!KvPosSetting.UseOrderSupplier && (req.SendoApp.SyncOnHandFormula == (byte)EcommerceSellQtyFormula.AddSupplierOrder || req.SendoApp.SyncOnHandFormula == (byte)EcommerceSellQtyFormula.SubOrderAddSupplierOrder))
            {
                throw new KvValidateLazadaException(KVMessage.EcommNotEnableUseOrderSupplier);
            }
            return true;
        }

        #endregion
    }
}

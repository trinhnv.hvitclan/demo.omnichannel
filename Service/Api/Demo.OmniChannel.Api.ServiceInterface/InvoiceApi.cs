﻿using Demo.Audit.Model.Message;
using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Common;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Api.ServiceInterface
{
    public class InvoiceApi : BaseApi
    {
        public IInvoiceMongoService InvoiceMongoService { get; set; }
        public IOmniChannelService OmniChannelService { get; set; }
        public IAuditTrailInternalClient AuditTrailInternalClient { get; set; }
        private readonly IPaymentTransactionService _paymentTransactionService;
        public  IOmniChannelPlatformService OmniChannelPlatformService { get; set; }

        public InvoiceApi(ILogger<InvoiceApi> logger, IPaymentTransactionService paymentTransactionService) : base(logger)
        {
            _paymentTransactionService = paymentTransactionService;
        }

        public async Task<object> Any(GetInvoiceSyncError req)
        {
            var invoiceDataSource = await InvoiceMongoService.GetByChannelId(req.RetailerId, req.Top, req.Skip, req.InvoiceCode, req.SearchTerm, req.ChannelIds, req.BranchIds, req.KeySearchProduct);

            if (invoiceDataSource.Total == 0 || invoiceDataSource.Data.Count == 0)
            {
                return new PagingDataSource<InvoiceDto>();
            }

            var invoiceDtos = invoiceDataSource.Data.Select(x => x.ConvertTo<InvoiceDto>()).ToList();

            foreach (var item in invoiceDtos)
            {
                var invoice = invoiceDataSource.Data.FirstOrDefault(x => x.Code == item.Code && x.ChannelId == item.ChannelId);
                if (invoice == null) continue;

                item.InvoiceDetails = invoice.InvoiceDetails.Select(x => new InvoiceDetailDto
                {
                    CommonParentChannelProductId = x.CommonParentChannelProductId,
                    CommonProductChannelId = x.CommonProductChannelId,
                    Discount = x.Discount,
                    DiscountPrice = x.DiscountPrice,
                    DiscountRatio = x.DiscountRatio,
                    Note = x.Note,
                    Price = x.Price,
                    ProductChannelName = x.ProductChannelName,
                    ProductName = x.ProductName,
                    ProductChannelSku = x.ProductChannelSku,
                    ProductCode = x.ProductCode,
                    ProductId = x.ProductId,
                    Quantity = x.Quantity,
                    UseProductBatchExpire = x.UseProductBatchExpire,
                    UseProductSerial = x.UseProductSerial,
                    ProductBatchExpireId = x.ProductBatchExpireId,
                    ReturnQuantity = x.ReturnQuantity,
                    SerialNumbers = x.SerialNumbers,
                    SubTotal = x.SubTotal,
                    UsePoint = x.UsePoint,
                    Uuid = string.IsNullOrEmpty(x.Uuid) ? StringHelper.GenerateUUID(): x.Uuid
                }).ToList();
            }
            return new PagingDataSource<InvoiceDto>
            {
                Total = invoiceDataSource.Total,
                Data = invoiceDtos
            };
        }

        public async Task<object> Any(GetInvoiceById req)
        {
            return await InvoiceMongoService.GetByIdAsync(req.Id);
        }

        public async Task<object> Delete(RemoveInvoiceSyncError req)
        {

            var invoice = await InvoiceMongoService.GetByIdAsync(req.Id);
            if (invoice != null)
            {
                await InvoiceMongoService.RemoveAsync(req.Id);
                var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
                var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, InternalContext.RetailerId, connectStringName, InternalContext.BranchId, AppSettings.Get<int>("ExecutionContext"));
                var channel = await OmniChannelService.GetByIdAsync(invoice.ChannelId);
                var log = new AuditTrailLog
                {
                    FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channel.Type),
                    Action = (int)AuditTrailAction.OrderIntergate,
                    CreatedDate = DateTime.Now,
                    BranchId = invoice.BranchId,
                    Content = $"Xóa hóa đơn: [InvoiceCode]{invoice.Code}[/InvoiceCode] của gian hàng {context.RetailerCode} trên sàn {channel?.Name} khỏi danh sách đồng bộ hóa đơn lỗi."
                };
                var coreContext = context.ConvertTo<KvInternalContext>();
                coreContext.UserId = context.User?.Id ?? 0;
                await AuditTrailInternalClient.AddLogAsync(coreContext, log);
            }
            return true;
        }

        public async Task<object> Post(UpdateErrorMessageInvoice req)
        {
            var existInvoice = await InvoiceMongoService.GetByIdAsync(req.Invoice.Id);
            if (existInvoice != null)
            {
                existInvoice.ErrorMessage = req.ErrorMessage;
                existInvoice.InvoiceDetails = req.Invoice.InvoiceDetails;
                await InvoiceMongoService.UpdateAsync(existInvoice.Id, existInvoice);
            }

            var channel = await OmniChannelService.GetByIdAsync(req.Invoice.ChannelId);
            if (channel == null) return true;

            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);

            var log = new AuditTrailLog
            {
                FunctionId = AuditTrailHelper.GetAuditTrailFunctionType(channel.Type),
                Action = (int)AuditTrailAction.InvoiceIntergate,
                CreatedDate = DateTime.Now,
                BranchId = InternalContext.BranchId,
                Content =
                $"Tạo hóa đơn KHÔNG thành công: [InvoiceCode]{req.Invoice.Code}[/InvoiceCode] (cho đơn đặt hàng [OrderCode]{req.Invoice.ChannelOrderId}[/OrderCode]) lý do: <br/>{req.AuditTrailMessage}"
            };

            var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, InternalContext.RetailerId, connectStringName, InternalContext.BranchId, AppSettings.Get<int>("ExecutionContext"));
            var coreContext = context.ConvertTo<KvInternalContext>();
            coreContext.UserId = context.User?.Id ?? 0;
            await AuditTrailInternalClient.AddLogAsync(coreContext, log);
            return true;
        }
        public async Task<List<PaymentChannelInvoices>> Post(GetChannelPaymentInvoices input)
        {
            var result = await _paymentTransactionService.GetPaymentChannelInvoices(input.RetailerId, input.OrderSnLst);
            return result;
        }

        public async Task<long> Any(TotalInvoiceError req)
        {
            return await InvoiceMongoService.GetTotalInvoiceError(InternalContext.RetailerId, req.ChannelIds);
        }
        public object Post(MultiSyncErrorInvoice req)
        {
            var logId = Guid.NewGuid();
            var logMessage = new LogObjectMicrosoftExtension(Logger, logId)
            {
                Action = "MultiSyncErrorInvoice",
                RetailerId = InternalContext.RetailerId,
                RequestObject = req.ToJson(),
            };
            var invoices = req.Invoices.FromJson<List<InvoiceDto>>();
            if (invoices != null && invoices.Any())
            {
                invoices = invoices.GroupBy(x => x.Code).Select(x => x.First()).ToList();
            }
            else
            {
                return false;
            }

            var channels = req.Channels.FromJson<List<Domain.Model.OmniChannel>>();

            foreach (var invoice in invoices)
            {
                var channel = channels.FirstOrDefault(x => x.Id == invoice.ChannelId);
                if (channel == null)
                {
                    continue;
                }

                var invoiceRetry = invoice.ConvertTo<MongoDb.Invoice>();
                invoiceRetry.InvoiceDetails = invoice.InvoiceDetails.Select(x => new MongoDb.InvoiceDetail
                {
                    CommonParentChannelProductId = x.ParentChannelProductId ?? x.StrParentChannelProductId,
                    CommonProductChannelId = x.ProductChannelId ?? x.StrProductChannelId,
                    Discount = x.Discount,
                    DiscountPrice = x.DiscountPrice,
                    DiscountRatio = x.DiscountRatio,
                    Note = x.Note,
                    Price = x.Price,
                    ProductChannelName = x.ProductChannelName,
                    ProductChannelSku = x.ProductChannelSku,
                    ProductCode = x.ProductCode,
                    ProductId = x.ProductId,
                    ProductName = x.ProductName,
                    Quantity = x.Quantity,
                    UseProductBatchExpire = x.UseProductBatchExpire,
                    UseProductSerial = x.UseProductSerial,
                    UsePoint = x.UsePoint,
                    SubTotal = x.SubTotal,
                    ProductBatchExpireId = x.ProductBatchExpireId,
                    ReturnQuantity = x.ReturnQuantity,
                    SerialNumbers = x.SerialNumbers,
                    Uuid = string.IsNullOrEmpty(x.Uuid) ? StringHelper.GenerateUUID() : x.Uuid,
                }).ToList();

                PushSyncErrorInvoiceMessage(channel, invoiceRetry, invoice.CurrentSaleTime, logId);
            }

            logMessage.LogInfo();

            return true;
        }

        public async Task<object> Any(GetLogisticsStatus req)
        {
            var logId = Guid.NewGuid();
            var logMessage = new LogObjectMicrosoftExtension(Logger, logId)
            {
                Action = "GetLogisticsStatus",
                RetailerId = InternalContext.RetailerId,
                OmniChannelId = req.ChannelId
            };
            ChannelStatus retVal = new ChannelStatus();
            var channel = await OmniChannelService.GetByIdAsync(req.ChannelId);
            var channelClient = new ChannelClient.Impls.ChannelClient();
            var client = channelClient.GetClient(channel.Type, AppSettings);
            if (channel.OmniChannelAuth == null)
            {
                return null;
            }
            var platform = await OmniChannelPlatformService.GetById(channel.PlatformId);

            var auth = new ChannelAuth
            {
                Id = channel.OmniChannelAuth.Id,
                AccessToken = !string.IsNullOrEmpty(channel.OmniChannelAuth.AccessToken)
                    ? CryptoHelper.RijndaelDecrypt(channel.OmniChannelAuth.AccessToken)
                    : string.Empty,
                ShopId = channel.OmniChannelAuth.ShopId
            };
            (retVal.LogisticsStatus, retVal.OrderStatus, retVal.TrackingNumber) =
                await client.GetLogisticsStatus(InternalContext.RetailerId, req.ChannelId, logMessage, req.OrderId, auth,
                    true, platform);
            logMessage.LogInfo();
            return retVal;
        }

        private void PushSyncErrorInvoiceMessage(Domain.Model.OmniChannel channel,
            Invoice invoice,
            bool currentSaleTime,
            Guid logId)
        {
            var configSyncOrderErrorV2Feature = AppSettings.Get<SyncOrderErrorV2Feature>("SyncOrderErrorV2Feature");
            switch (channel.Type)
            {
                case (byte)ChannelType.Lazada:
                    {
                        var message = new LazadaSyncErrorInvoiceMessage
                        {
                            RetailerCode = InternalContext.RetailerCode,
                            KvEntities = InternalContext.Group?.ConnectionString,
                            Invoice = invoice.ToSafeJson(),
                            RetailerId = InternalContext.RetailerId,
                            BranchId = invoice.BranchId,
                            ChannelId = invoice.ChannelId,
                            LogId = logId,
                            CurrentSaleTime = currentSaleTime,
                            PlatformId = channel.PlatformId
                        };
                        using (var mq = MessageFactory.CreateMessageProducer())
                        {
                            mq.Publish(message);
                        }

                        break;
                    }

                case (byte)ChannelType.Shopee:
                    {
                        var message = new ShopeeSyncErrorInvoiceMessage
                        {
                            RetailerCode = InternalContext.RetailerCode,
                            KvEntities = InternalContext.Group?.ConnectionString,
                            Invoice = invoice.ToSafeJson(),
                            RetailerId = InternalContext.RetailerId,
                            BranchId = invoice.BranchId,
                            ChannelId = invoice.ChannelId,
                            LogId = logId,
                            CurrentSaleTime = currentSaleTime,
                            PlatformId = channel.PlatformId
                        };
                        if (configSyncOrderErrorV2Feature.IsValid(InternalContext.Group?.Id ?? 0, InternalContext.RetailerId))
                        {
                            var kafkaConfig = AppSettings.Get<Kafka>("Kafka");
                            KafkaClient.KafkaClient.Instance.PublishMessage($"{kafkaConfig.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.SyncErrorInvoice && x.ChannelType == channel.Type)?.Name}", string.Empty, message.ToJson());

                        }
                        else
                        {
                            using (var mq = MessageFactory.CreateMessageProducer())
                            {
                                mq.Publish(message);
                            }
                        }
                        break;
                    }

                case (byte)ChannelType.Tiktok:
                    {
                        var message = new TiktokSyncErrorInvoiceMessage
                        {
                            RetailerCode = InternalContext.RetailerCode,
                            KvEntities = InternalContext.Group?.ConnectionString,
                            Invoice = invoice.ToSafeJson(),
                            RetailerId = InternalContext.RetailerId,
                            BranchId = invoice.BranchId,
                            ChannelId = invoice.ChannelId,
                            LogId = logId,
                            CurrentSaleTime = currentSaleTime,
                            PlatformId = channel.PlatformId
                        };
                        if (configSyncOrderErrorV2Feature.IsValid(InternalContext.Group?.Id ?? 0, InternalContext.RetailerId))
                        {
                            var kafkaConfig = AppSettings.Get<Kafka>("Kafka");
                            KafkaClient.KafkaClient.Instance.PublishMessage($"{kafkaConfig.AllTopics.FirstOrDefault(x => x.Type == (byte)KafkaTopicType.SyncErrorInvoice && x.ChannelType == channel.Type)?.Name}", string.Empty, message.ToJson());

                        }
                        else
                        {
                            using (var mq = MessageFactory.CreateMessageProducer())
                            {
                                mq.Publish(message);
                            }
                        }
                        break;
                    }

                case (byte)ChannelType.Tiki:
                    {
                        var message = new TikiSyncErrorInvoiceMessage
                        {
                            RetailerCode = InternalContext.RetailerCode,
                            KvEntities = InternalContext.Group?.ConnectionString,
                            Invoice = invoice.ToSafeJson(),
                            RetailerId = InternalContext.RetailerId,
                            BranchId = invoice.BranchId,
                            ChannelId = invoice.ChannelId,
                            LogId = logId,
                            CurrentSaleTime = currentSaleTime,
                            PlatformId = channel.PlatformId
                        };
                        using (var mq = MessageFactory.CreateMessageProducer())
                        {
                            mq.Publish(message);
                        }

                        break;
                    }

                case (byte)ChannelType.Sendo:
                    {
                        var message = new SendoSyncErrorInvoiceMessage
                        {
                            RetailerCode = InternalContext.RetailerCode,
                            KvEntities = InternalContext.Group?.ConnectionString,
                            Invoice = invoice.ToSafeJson(),
                            RetailerId = InternalContext.RetailerId,
                            BranchId = invoice.BranchId,
                            ChannelId = invoice.ChannelId,
                            LogId = logId,
                            CurrentSaleTime = currentSaleTime,
                            PlatformId = channel.PlatformId
                        };
                        using (var mq = MessageFactory.CreateMessageProducer())
                        {
                            mq.Publish(message);
                        }

                        break;
                    }
            }

        }
    }
}
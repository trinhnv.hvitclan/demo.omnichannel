﻿using Demo.Mapping.Sdk.Common;
using ServiceStack;
using ServiceStack.Host;
using ServiceStack.Web;

namespace Demo.OmniChannel.Api.ServiceInterface.Utils
{
    public class MappingUtil
    {
        protected MappingUtil()
        {

        }
        public static MappingInternalHeader GetMappingHeader(IRequest request)
        {
            return new MappingInternalHeader()
            {
                BranchId = request.GetHeader("branchid") ?? "",
                RetailerCode = request.GetHeader("retailer") ?? "",
                Token = request.GetBearerToken(),
            };
        }
    }
}

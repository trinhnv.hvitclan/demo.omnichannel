﻿using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.ChannelClient.Impls;
using ServiceStack;
using Demo.OmniChannel.ChannelClient.Interfaces;

namespace Demo.OmniChannel.Api.ServiceInterface
{
    public class LogisticsApi : BaseApi
    {
        public IOmniChannelService OmniChannelService { get; set; }
        public IOmniChannelPlatformService OmniChannelPlatformService { get; set; }
        public IOmniChannelAuthService OmniChannelAuthService { get; set; }
        public IChannelClient ChannelClient { get; set; }
        public LogisticsApi(ILogger<LogisticsApi> logger, IChannelClient channelClient, IOmniChannelPlatformService omniChannelPlatformService, IOmniChannelAuthService omniChannelAuthService) : base(logger)
        {
            ChannelClient = channelClient;
            OmniChannelPlatformService = omniChannelPlatformService;
            OmniChannelAuthService = omniChannelAuthService;
        }
        public async Task<object> Get(LogisticsRequest req)
        {
            var channels = await OmniChannelService.GetByChannelIds(req.ChannelIds);
            var logisticMergedByManyShop = new List<Logistics>();
            var channelFirst = channels.First();
            var platforms =
                await OmniChannelPlatformService.GetByIds(channels.Select(x => x.PlatformId).Distinct().ToList());
            foreach (var channel in channels)
            {
                var client = ChannelClient.CreateClient(channel.Type, InternalContext.Group.Id, InternalContext.RetailerId);
                var auth = await OmniChannelAuthService.GetChannelAuth(channel, Guid.NewGuid());
                var channelAuth = new ChannelAuth { ShopId = auth.ShopId, AccessToken = auth.AccessToken };

                var platform = platforms.FirstOrDefault(x => x.Id == channel.PlatformId);
                var logisticList = await client.GetLogisticsByShopId(InternalContext.RetailerId, channel.Id, channelAuth,
                   Guid.NewGuid(), platform);
                var logisticEnabledList = logisticList.Where(l => l.Enabled && l.MaskChannelId == 0).ToList();
                if (channelFirst.Equals(channel)) logisticMergedByManyShop = logisticEnabledList;

                //Lấy ra các phần tử giao nhau
                var logisticIntersect = logisticEnabledList
                    .Where(x => logisticMergedByManyShop.Any(y => x.LogisticId == y.LogisticId)).ToList();
                logisticMergedByManyShop = logisticIntersect;
            }

            //TMDT-1059 (Chỉ lấy các logistic với maskChannelId == 0 do shopee đã nhóm logistic lại)
            return logisticMergedByManyShop.OrderBy(l => l.LogisticId).ToList();
        }

        public async Task<object> Get(AttributesRequest req)
        {
            var channel = await OmniChannelService.GetByIdAsync(req.ChannelId);
            var channelClient = new ChannelClient.Impls.ChannelClient();
            var client = channel.Type == (byte)ChannelTypeEnum.Shopee ? channelClient.GetClientV2(channel.Type, AppSettings) : channelClient.GetClient(channel.Type, AppSettings);

            var platforms =
                await OmniChannelPlatformService.GetById(channel.PlatformId);
            var authData = await OmniChannelAuthService.GetChannelAuth(channel, Guid.NewGuid());
            var auth = new ChannelAuth { ShopId = Convert.ToInt64(channel.IdentityKey), AccessToken = authData.AccessToken };

            return await client.GetAttributeByShopId(channel.RetailerId, channel.Id, auth, Guid.NewGuid(),
                req.CategoryId, platforms);
        }



        public async Task<ReponseResultLogicstic> Post(GetLogisticsFilter request)
        {
            var log = new LogObjectMicrosoftExtension(Logger, Guid.NewGuid())
            {
                Action = "GetLogistics",
                RequestObject = request.ToSafeJson(),
                RetailerId = CurrentRetailerId,
            };

            try
            {
                // Get OmniChannelTypeShopee
                var omniChannelIdList = await OmniChannelService.GetByRetailerAsync(CurrentRetailerId, CurrentBranchId, type: (byte)ChannelType.Shopee, isIncludeInactive: false, isIncludeDeleted: false);
                if (request.ChannelIds != null && request.ChannelIds.Count > 0)
                    omniChannelIdList = omniChannelIdList.Where(x => request.ChannelIds.Contains(x.Id)).ToList();
                // Gọi lên sàn
                if (!omniChannelIdList.Any()) return new ReponseResultLogicstic();
                var omniChannelIdLst = omniChannelIdList.Select(item => item.Id).ToList();
                var logisticsRequest = new LogisticsRequest
                {
                    ChannelIds = omniChannelIdLst
                };
                var resultLogistics = (List<Logistics>)await Get(logisticsRequest);
                log.ResponseObject = resultLogistics;
                log.LogInfo();
                var result = ConvertToReponseLogicstic(resultLogistics);
                return result;
            }
            catch (Exception)
            {
                return new ReponseResultLogicstic();
            }
        }

        private ReponseResultLogicstic ConvertToReponseLogicstic(List<Logistics> resultLogicstic)
        {
            var reponseResultLogicstic = new ReponseResultLogicstic();
            reponseResultLogicstic.IsVeryQuickLogicstic = resultLogicstic.Any(item => item.LogisticName.Equals(ShopeeLogicsticName.VeryQuick));
            reponseResultLogicstic.IsQuickLogicstic = resultLogicstic.Any(item => item.LogisticName.Equals(ShopeeLogicsticName.Quick));
            reponseResultLogicstic.IsThriftyLogicstic = resultLogicstic.Any(item => item.LogisticName.Equals(ShopeeLogicsticName.Thrifty));
            return reponseResultLogicstic;
        }
    }
}

﻿using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Web;

namespace Demo.OmniChannel.Api.ServiceInterface
{
    public class AccessInternalFilterAttribute : RequestFilterAttribute
    {
        public override void Execute(IRequest req, IResponse res, object requestDto)
        {
            var appSettings = HostContext.Resolve<IAppSettings>();
            var internalKey = appSettings.Get<string>("InternalApiAccessKey"); 

            if (!string.IsNullOrEmpty(internalKey) &&
                req.Headers["InternalApiToken"] != internalKey)
                throw HttpError.Unauthorized("You don't have permission to access on this api");
        }
    }
}

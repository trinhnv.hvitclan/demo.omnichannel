﻿using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.OrmLite;
using System;
using System.Threading.Tasks;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;

namespace Demo.OmniChannel.Api.ServiceInterface
{
    public class EventKolApi : BaseInternalApi
    {
        public IAuditTrailInternalClient AuditTrailInternalClient { get; set; }
        public EventKolApi(ILogger<EventKolApi> logger) : base(logger)
        {

        }

        public async Task<object> Post(EventKolCreate req)
        {
            var nameConnection = await GetDbNameFreePremiumLower(req.RetailerId);
            using (var db = await DbConnectionFactory.OpenAsync(nameConnection))
            {
                var eventKolRepo = new EventKolRepository(db);
                var eventKol = await eventKolRepo.GetByIdAsync(req.EventId);
                if (eventKol == null) throw new KvException($"eventKol is not existed, Id = {req.EventId}");
                var newEvent = new EventKol()
                {
                    RetailerId = eventKol.RetailerId,
                    EventId = req.EventId,
                    Action = eventKol.Action,
                    CreatedDate = eventKol.CreatedDate,
                    DocumentCode = eventKol.DocumentCode,
                    DocumentId = eventKol.DocumentId,
                    DocumentType = eventKol.DocumentType,
                    Status = 1
                };
                var result = await eventKolRepo.AddAsync(newEvent);
                return result;
            }
        }

        public async Task<object> Any(GetEventKolError req)
        {
            using (var db = await DbConnectionFactory.OpenAsync(req.NameConnectionString.ToLower()))
            {
                var eventKolRepo = new EventKolRepository(db);
                var result = await eventKolRepo.GetEventFail(req.FromDate ?? DateTime.Now.AddHours(-1), req.ToDate ?? DateTime.Now, req.Page ?? 1, req.NumberOfPage ?? 500);
                return result;
            }
        }

        private async Task<string> GetDbNameFreePremiumLower(int retailerId)
        {
            var cache = HostContext.TryResolve<ICacheClient>();
            var key = $"kol.scheduler.free.connection.{retailerId}";
            var nameConnection = cache.Get<string>(key);
            if (string.IsNullOrEmpty(nameConnection))
            {
                using (var dbMaster = await DbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities))
                {
                    var retailer = await dbMaster.SingleByIdAsync<KvRetailer>(retailerId);
                    if (retailer == null) throw new KvException("retailer is not existed");
                    var kvGroup = await dbMaster.SingleByIdAsync<KvGroup>(retailer.GroupId);
                    if (kvGroup == null) throw new KvException("group is not existed");
                    nameConnection = kvGroup.ConnectionString.Replace("name=", "").ToLower();
                    cache.Set(key, nameConnection);
                }
            }
            return nameConnection;

        }
    }
}

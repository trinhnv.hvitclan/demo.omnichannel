﻿using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Domain.Common;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.Messaging;
using ServiceStack.OrmLite;
using ServiceStack.Redis;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Api.ServiceInterface
{
    public class WebhookCallbackApi : Service
    {
        public IOmniChannelService OmniChannelService { get; set; }
        public IPriceBusiness PriceBusiness { get; set; }
        public ChannelClient.Impls.ChannelClient ChannelClient { get; set; }
        public IAppSettings Settings { get; set; }
        public IMessageFactory MessageFactory { get; set; }
        private readonly ILogger _logger;
        public IAppSettings AppSettings { get; set; }
        public IDbConnectionFactory DbConnectionFactory { get; set; }
        public ICacheClient CacheClient { get; set; }

        public IScheduleService ScheduleService { get; set; }
        public IOrderInternalClient OrderInternalClient { get; set; }

        private readonly ConcurrentDictionary<int, ConcurrentQueue<TaskVoidOrderReponse>> _keyMessageMapping;

        private readonly IOmniChannelAuthService _omniChannelAuthService;
        private readonly IOmniChannelPlatformService _omniChannelPlatformService;


        public WebhookCallbackApi(ILogger<WebhookCallbackApi> logger,
                        IOmniChannelAuthService omniChannelAuthService,
                        IOmniChannelPlatformService omniChannelPlatformService)
        {
            _logger = logger;
            _keyMessageMapping = new ConcurrentDictionary<int, ConcurrentQueue<TaskVoidOrderReponse>>();
            _omniChannelAuthService = omniChannelAuthService;
            _omniChannelPlatformService = omniChannelPlatformService;
        }

        public async Task<object> Post(ShopeeOrderUpdateSubcribe req)
        {
            var logId = Guid.NewGuid();
            var logOrderUpdate = new LogObjectMicrosoftExtension(_logger, logId)
            {
                Action = "ShopeeOrderUpdateSubcribe",
                RequestObject = req.ToSafeJson()
            };
            try
            {
                if (req.Code == (int)ShopeeWebhookEventType.TestCallback)
                {
                    logOrderUpdate.Description = "Tesk callback successful";
                    logOrderUpdate.LogInfo();
                    var result = new object();
                    return new HttpResult(HttpStatusCode.Accepted, result.ToSafeJson());
                }

                var mtSetting = AppSettings.Get<MaintenanceTimeConfig>("MaintenanceTime");
                if (!SystemHelper.IsValidMaintenanceTime(mtSetting))
                {
                    logOrderUpdate.Description = "Maintenance time --> Ignore";
                    logOrderUpdate.LogInfo();

                    return false;
                }

                if (req.Code == (int)ShopeeWebhookEventType.PromotionUpdate)
                {
                    logOrderUpdate.Description = "EventType is PromotionUpdate";
                    logOrderUpdate.LogInfo();
                    await ShopeePromotionUpdateSubcribeTask(req, logOrderUpdate);
                    return true;
                }

                if (req.Code != (int)ShopeeWebhookEventType.OrderStatusUpdate && req.Code != (int)ShopeeWebhookEventType.TrackingNoPush)
                {
                    logOrderUpdate.Description = "EventType not acceptable";
                    logOrderUpdate.LogInfo();

                    return true;
                }

                var channelType = (byte)ChannelType.Shopee;
                var identityKey = req.ShopId.ToString();

                //Use for support other channel type (Lzd, Tiki)
                if (req.ChannelType.HasValue && req.ChannelType > 0 && req.ShopId == 0 && req.ChannelType != (byte)ChannelType.Shopee)
                {
                    channelType = req.ChannelType.Value;
                    identityKey = req.IdentityKey;
                }

                var channels = await GetByIdentityKey(channelType, identityKey);
                if (channels == null || !channels.Any())
                {
                    logOrderUpdate.Description = "OmniChannel is null";
                    logOrderUpdate.LogWarning("OmniChannel is null");
                    return false;
                }
                List<string> detailShopeeOrderV2Lst = new List<string>();
                foreach (var channel in channels)
                {
                    if (channel.OmniChannelSchedules == null || channel.OmniChannelSchedules.All(x => x.Type != (byte)ScheduleType.SyncOrder))
                        continue;
                    var context = await CreateContext(channel.RetailerId);
                    if (context == null || req.Order == null) continue;
                    PushRedisMessageQueueToCreateKvOrder(req, channel, context, logOrderUpdate, channelType);
                }
                logOrderUpdate.Description = $"Push message sync order successful with {detailShopeeOrderV2Lst.ToSafeJson()}";
                logOrderUpdate.ResponseObject = $"ChannelIds: {channels.Select(x => x.Id).Join(", ")}";
                logOrderUpdate.LogInfo();
                return true;
            }
            catch (Exception e)
            {
                logOrderUpdate.Description =
                    "Error get data from Webhook Error : " + e.Message + "stackTrace:" + e.StackTrace;
                logOrderUpdate.LogError(e);
                return false;
            }
        }
        public async Task<object> Post(ResetLastSync req)
        {
            if (req.ChannelType.HasValue && req.ScheduleType.HasValue)
            {
                req.ScheduleIds.Clear();
                using (var db = DbConnectionFactory.OpenDbConnection())
                {
                    using (var scheduleRepo = new OmniChannelScheduleRepository(db))
                    {
                        var total = await scheduleRepo.GetTotal(req.ChannelType.Value, (byte)req.ScheduleType, req.IncludeRetail?.ToList(), null);
                        var pageSize = 1000;
                        var totalPage = Math.Ceiling((double)total / pageSize);
                        for (var i = 0; i < totalPage; i++)
                        {
                            var scheduleIds = await scheduleRepo.GetIds(req.ChannelType.Value, (byte)req.ScheduleType, req.IncludeRetail?.ToList(), null, i * pageSize, pageSize);
                            req.ScheduleIds.AddRange(scheduleIds.Select(item => item.ToString()));
                        }
                    }
                }
            }
            int totalCount = req.ScheduleIds.Count;
            var totalPages = (int)Math.Ceiling(totalCount / (decimal)1000);
            for (int page = 1; page <= totalPages; page++)
            {
                var scheduleIdsPaging = req.ScheduleIds.Page(page, 1000).ToList();
                HandleResetLastSync(scheduleIdsPaging, req.LastSync);
            }
            return Task.CompletedTask;

        }

        public async Task<bool> Post(ResyncTrackingInvoice request)
        {
            if (request.IncludeInvoiceCodeLst?.Count > 0 && request.RetailerId > 0)
            {
                List<ResyncTrackingInvoiceModel> modelInputTasks = await BuildInputTrackingInvoiceData(request);
                if (modelInputTasks.Count > 0)
                {
                    using (var mq = MessageFactory.CreateMessageProducer())
                    {
                        foreach (var modelInputTaskItem in modelInputTasks)
                        {
                            var message = new ResyncTrackingInvoiceMessage
                            {
                                RetailerId = request.RetailerId,
                                InvoiceCode = modelInputTaskItem.InvoiceCode,
                            };
                            mq.Publish(message);
                        }
                    }
                    return true;
                }
            }
            return false;
        }
        public async Task<bool> Post(VoidOrderAndInvoiceLst request)
        {
            List<TaskVoidOrderModel> modelInputTasks = await BuildInputData(request);
            int totalCount = modelInputTasks.Count;
            int pageSize = 100;
            var totalPages = (int)Math.Ceiling(totalCount / (decimal)pageSize);
            for (int page = 1; page <= totalPages; page++)
            {
                var taskLst = new List<Task<List<long>>>();
                var itemIdPaging = modelInputTasks.Skip((page - 1) * pageSize).Take(pageSize).ToList();
                foreach (var itemId in itemIdPaging)
                {
                    taskLst.Add(GetOrderByFilterAsync(itemId));
                }
                var taskResultList = await Task.WhenAll(taskLst);
                for (int i = 0; i < taskResultList.Length; i++)
                {
                    var taskItemResult = taskResultList[i];
                    var orderQueueItem = new TaskVoidOrderReponse
                    {
                        OrderIdLst = taskItemResult,
                        RetailerId = itemIdPaging[i].RetailerId,
                        ConnectionString = itemIdPaging[i].KvGroup.ConnectionString,
                        Prefix = request.PrefixDelete
                    };
                    var model = new ConcurrentQueue<TaskVoidOrderReponse>(new List<TaskVoidOrderReponse> { orderQueueItem });
                    if (taskItemResult?.Count > 0)
                    {
                        if (_keyMessageMapping.TryAdd(itemIdPaging[i].GroupId, model))
                        {
                            await HandleProcessPeek(itemIdPaging[i].GroupId, _keyMessageMapping);
                        }
                        else
                        {
                            _keyMessageMapping[itemIdPaging[i].GroupId].Enqueue(orderQueueItem);
                        }
                    }
                }
            }
            return true;
        }
        private void HandleResetLastSync(List<string> scheduleIds, DateTime lastSync)
        {
            var scheduleDtos = ScheduleService.GetByIds(scheduleIds);
            if (scheduleDtos.Any())
            {
                scheduleDtos.Map(x =>
                {
                    x.LastSync = lastSync;
                    x.IsRunning = false;
                    return x;
                });
                ScheduleService.AddOrUpdateBatch(0, 0, scheduleDtos);
            }
        }

        private async Task<List<TaskVoidOrderModel>> BuildInputData(VoidOrderAndInvoiceLst request)
        {
            List<TaskVoidOrderModel> modelInputTasks = new List<TaskVoidOrderModel>();
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                using (var omniChannelRespo = new OmniChannelRepository(db))
                {
                    var kvRetailer = await GetRetailer(request.RetailerId);
                    if (kvRetailer == null)
                    {
                        throw new KvException("Retailer is null !!");
                    }

                    var kvGroup = await GetRetailerGroup(kvRetailer.GroupId);
                    if (kvGroup == null || string.IsNullOrEmpty(kvGroup.ConnectionString))
                    {
                        throw new KvException("KvGroup is null !!");
                    }
                    var model = new TaskVoidOrderModel
                    {
                        GroupId = kvGroup.Id,
                        RetailerId = kvRetailer.Id,
                        KvGroup = kvGroup,
                        KvRetailer = kvRetailer,
                        OrderCodeLst = request.IncludeOrderCodeLst
                    };
                    modelInputTasks.Add(model);
                }
            }
            return modelInputTasks;
        }

        private async Task<List<ResyncTrackingInvoiceModel>> BuildInputTrackingInvoiceData(ResyncTrackingInvoice request)
        {
            List<ResyncTrackingInvoiceModel> modelInputTasks = new List<ResyncTrackingInvoiceModel>();
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                using (var omniChannelRespo = new OmniChannelRepository(db))
                {
                    var kvRetailer = await GetRetailer(request.RetailerId);
                    if (kvRetailer == null)
                    {
                        throw new KvException("Retailer is null !!");
                    }

                    var kvGroup = await GetRetailerGroup(kvRetailer.GroupId);
                    if (kvGroup == null || string.IsNullOrEmpty(kvGroup.ConnectionString))
                    {
                        throw new KvException("KvGroup is null !!");
                    }
                    foreach (var item in request.IncludeInvoiceCodeLst)
                    {
                        var model = new ResyncTrackingInvoiceModel
                        {
                            GroupId = kvGroup.Id,
                            RetailerId = kvRetailer.Id,
                            KvGroup = kvGroup,
                            KvRetailer = kvRetailer,
                            InvoiceCode = item
                        };
                        modelInputTasks.Add(model);
                    }
                   
                }
            }
            return modelInputTasks;
        }

        private async Task HandleProcessPeek(int groupId, ConcurrentDictionary<int, ConcurrentQueue<TaskVoidOrderReponse>> _keyMessageMapping)
        {
            while (!_keyMessageMapping[groupId].IsEmpty)
            {
                if (_keyMessageMapping[groupId].TryPeek(out var peekResult))
                {
                    try
                    {
                        await VoidKvOrder(peekResult);
                    }
                    finally
                    {
                        _keyMessageMapping[groupId].TryDequeue(out _);
                    }
                }

            }
        }

        private async Task VoidKvOrder(TaskVoidOrderReponse request)
        {
            if (request.OrderIdLst?.Count > 0)
            {
                var connectStringName = request.ConnectionString?.Substring(request.ConnectionString.IndexOf('=') + 1);
                var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, request.RetailerId, connectStringName, 0, AppSettings.Get<int>("ExecutionContext"));
                var coreContext = context.ConvertTo<Demo.OmniChannelCore.Api.Sdk.Common.KvInternalContext>();
                coreContext.UserId = context.User?.Id ?? 0;
                VoidOrderAndInvoicePage(coreContext, connectStringName, request.OrderIdLst, request.Prefix);
            }
        }

        public void VoidOrderAndInvoicePage(Demo.OmniChannelCore.Api.Sdk.Common.KvInternalContext context, string connectStringName, List<long> orderIdLst, string prefix, int pageSize = 200)
        {
            int totalCount = orderIdLst.Count;
            var totalPages = (int)Math.Ceiling(totalCount / (decimal)pageSize);
            using (var mq = MessageFactory.CreateMessageProducer())
            {
                for (int page = 1; page <= totalPages; page++)
                {
                    var orderPaging = orderIdLst.Page(page, pageSize).ToList();
                    PublishVoidOrderAndInvoiceMessage(mq, context, connectStringName, orderPaging, prefix);
                }
            }

        }

        public void PublishVoidOrderAndInvoiceMessage(IMessageProducer messageProducer, Demo.OmniChannelCore.Api.Sdk.Common.KvInternalContext context, string connectStringName, List<long> orderIdLst, string prefix)
        {
            foreach (var item in orderIdLst)
            {
                var message = new VoidOrderAndInvoiceMessage
                {
                    RetailerId = context.RetailerId,
                    KvEntities = connectStringName,
                    Prefix = prefix,
                    OrderId = item
                };
                messageProducer.Publish(message);
            }
        }

        private async Task<List<long>> GetOrderByFilterAsync(TaskVoidOrderModel input)
        {
            if (string.IsNullOrEmpty(input.KvGroup?.ConnectionString))
            {
                return new List<long>();
            }
            var connectStringName = input.KvGroup.ConnectionString.Substring(input.KvGroup.ConnectionString.IndexOf('=') + 1);
            using (var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var orderRepository = new OrderRepository(db);
                var orderIdLst = await orderRepository.GetOrdersByCodeListAsync(input.OrderCodeLst, input.RetailerId);
                if (orderIdLst?.Count > 0)
                {
                    return orderIdLst.Select(item => item.Id).ToList();
                }

            }
            return new List<long>();
        }
        public async Task<object> Any(LazadaUpdateFeeOrderSn request)
        {
            if (!ValidateRequest(request))
            {
                return null;
            }
            var retailer = await GetRetailer(request.RetailerId);
            if (retailer == null)
            {
                return null;
            }
            var kvGroup = await GetRetailerGroup(retailer.GroupId);
            if (kvGroup == null)
            {
                return null;
            }
            using (var mq = MessageFactory.CreateMessageProducer())
            {
                var message = new LazadaSyncFeeTransactionMessage
                {
                    ChannelId = request.ChannelId,
                    ChannelType = (byte)ChannelType.Lazada,
                    RetailerId = (int)request.RetailerId,
                    BranchId = request.BranchId,
                    GroupId = kvGroup.Id,
                    ShopId = request.ShopId,
                    SentTime = DateTime.Now,
                    LogId = Guid.NewGuid(),
                    KvEntities = kvGroup.ConnectionString,
                    OrderSn = request.OrderSn
                };
                mq.Publish(message);
            }
            return true;
        }

        private bool ValidateRequest(LazadaUpdateFeeOrderSn request)
        {
            if (request.ChannelId <= 0)
            {
                return false;
            }
            if (request.BranchId <= 0)
            {
                return false;
            }
            if (request.ChannelId < 0)
            {
                return false;
            }
            if (string.IsNullOrEmpty(request.ShopId) || string.IsNullOrEmpty(request.OrderSn))
            {
                return false;
            }
            return true;
        }

        private async Task ShopeePromotionUpdateSubcribeTask(ShopeeOrderUpdateSubcribe req, LogObjectMicrosoftExtension logOrderUpdate)
        {
            var jsonPromotion = JsonConvert.SerializeObject(req.Order);
            var promotionUpdateReq = JsonConvert.DeserializeObject<PromotionUpdate>(jsonPromotion);
            if (promotionUpdateReq?.Action == ActionPromoUpdate.RemoveFromPromo)
            {
                var mtSetting = AppSettings.Get<MaintenanceTimeConfig>("MaintenanceTime");
                if (!SystemHelper.IsValidMaintenanceTime(mtSetting))
                {
                    logOrderUpdate.Description = "Maintenance time --> Ignore";
                    logOrderUpdate.LogInfo();
                    return;
                }
                var channelType = (byte)ChannelType.Shopee;
                var identityKey = req.ShopId.ToString();
                var channels = await GetByIdentityKey(channelType, identityKey);

                if (channels == null || !channels.Any())
                {
                    logOrderUpdate.Description = "OmniChannel is null";
                    logOrderUpdate.LogError(new KvValidateException("OmniChannel is null"), false, true);
                    return;
                }

                var channelProductIds = new List<string> { promotionUpdateReq.ItemId.ToString() };
                foreach (var channel in channels)
                {
                    if (channel.OmniChannelSchedules == null ||
                        channel.OmniChannelSchedules.All(x => x.Type != (byte)ScheduleType.SyncPrice))
                    {
                        continue;
                    }
                    var context = await CreateContext(channel.RetailerId);
                    if (context == null || string.IsNullOrEmpty(context.Group.ConnectionString))
                    {
                        continue;
                    }

                    if (channelProductIds.Any())
                    {
                        await SyncMultiPrice(channel, context.Group.ConnectionString, channelProductIds, logOrderUpdate.Id, true);
                    }

                    logOrderUpdate.RetailerId = context.RetailerId;
                    logOrderUpdate.RetailerCode = context.RetailerCode;
                }
                logOrderUpdate.Description = "Sync MultiPrice Successful";
                logOrderUpdate.ResponseObject = $"ChannelIds: {channels.Select(x => x.Id).Join(", ")}";
                logOrderUpdate.LogInfo();
            }
        }
        private async Task<KvInternalContext> CreateContext(int retailerId)
        {
            var retailer = await GetRetailer(retailerId);
            if (retailer == null) return null;
            var kvGroup = await GetRetailerGroup(retailer.GroupId);
            if (kvGroup == null) return null;

            var kvInternalContext = new KvInternalContext
            {
                RetailerCode = retailer.Code,
                RetailerId = retailer.Id,
                Group = new Domain.Common.KvGroup
                {
                    Id = kvGroup?.Id ?? 0,
                    ConnectionString = kvGroup?.ConnectionString
                }
            };
            return kvInternalContext;
        }

        private async Task<List<Domain.Model.OmniChannel>> GetByIdentityKey(byte channelType, string identityKey)
        {
            var omniChannels = new List<Domain.Model.OmniChannel>();
            var identityCacheSetKey = string.Format(KvConstant.IdentityCacheSetKey, channelType, identityKey);

            using (var cli = HostContext.TryResolve<IRedisClientsManager>().GetClient())
            {
                if (!cli.ContainsKey(identityCacheSetKey))
                {
                    omniChannels = await OmniChannelService.GetByIdentityKey(channelType, identityKey);
                    if (omniChannels != null && omniChannels.Any())
                    {
                        var channelIds = omniChannels.Select(x => x.Id.ToString()).ToList();
                        var cacheClient = cli.As<Domain.Model.OmniChannel>();
                        cacheClient.StoreAll(omniChannels);
                        cli.AddRangeToSet(identityCacheSetKey, channelIds);
                    }
                }
                else
                {
                    var channelIds = cli.GetAllItemsFromSet(identityCacheSetKey);
                    var cacheClient = cli.As<Domain.Model.OmniChannel>();
                    omniChannels = cacheClient.GetByIds(channelIds).ToList();
                }
            }

            return omniChannels;
        }

        private async Task SyncMultiPrice(Domain.Model.OmniChannel channel, string connectStringName, List<string> channelProductIds, Guid logId, bool isIgnoreAuditTrail = false)
        {
            var connectionString = connectStringName.Substring(connectStringName.IndexOf('=') + 1);

            await PriceBusiness.SyncMultiPrice(connectionString, channel, channelProductIds, null, isIgnoreAuditTrail, logId);
        }

        private void PushRedisMessageQueueToCreateKvOrder(ShopeeOrderUpdateSubcribe req,
            Domain.Model.OmniChannel channel,
            KvInternalContext context,
            LogObjectMicrosoftExtension logOrderUpdate,
            byte channelType)
        {
            using (var mq = MessageFactory.CreateMessageProducer())
            {
                var jsonOrder = JsonConvert.SerializeObject(req.Order);
                var shopeeOrderReq = JsonConvert.DeserializeObject<ShopeeOrder>(jsonOrder);
                switch (channelType)
                {
                    case (byte)ChannelType.Lazada:
                        var createLazadaOrder = new LazadaCreateOrderMessage
                        {
                            ChannelId = channel.Id,
                            ChannelType = channel.Type,
                            BranchId = channel.BranchId,
                            KvEntities = context.Group.ConnectionString,
                            RetailerId = channel.RetailerId,
                            OrderId = shopeeOrderReq?.OrderSn.Trim(),
                            GroupId = context.Group.Id,
                            OrderStatus = shopeeOrderReq?.Status,
                            Order = shopeeOrderReq?.OrderString,
                            PlatformId = channel.PlatformId,
                            LogId = logOrderUpdate.Id
                        };

                        mq.Publish(createLazadaOrder);
                        break;
                    case (byte)ChannelType.Tiki:
                        var createTikiOrder = new TikiCreateOrderMessage
                        {
                            ChannelId = channel.Id,
                            ChannelType = channel.Type,
                            BranchId = channel.BranchId,
                            KvEntities = context.Group.ConnectionString,
                            RetailerId = channel.RetailerId,
                            OrderId = shopeeOrderReq?.OrderSn.Trim(),
                            GroupId = context.Group.Id,
                            OrderStatus = shopeeOrderReq?.Status,
                            PlatformId = channel.PlatformId,
                            LogId = logOrderUpdate.Id
                        };
                        mq.Publish(createTikiOrder);
                        break;
                    case (byte)ChannelType.Sendo:
                        var createSendoOrder = new SendoCreateOrderMessage
                        {
                            ChannelId = channel.Id,
                            ChannelType = channel.Type,
                            BranchId = channel.BranchId,
                            KvEntities = context.Group.ConnectionString,
                            RetailerId = channel.RetailerId,
                            OrderId = shopeeOrderReq?.OrderSn.Trim(),
                            GroupId = context.Group.Id,
                            OrderStatus = shopeeOrderReq?.Status,
                            PlatformId = channel.PlatformId,
                            LogId = logOrderUpdate.Id
                        };
                        mq.Publish(createSendoOrder);
                        break;
                }
            }
        }

        private async Task<KvRetailer> GetRetailer(long retailerId)
        {
            var kvRetailer = CacheClient.Get<KvRetailer>(string.Format(KvConstant.RetailerIdCacheKey, retailerId));
            if (kvRetailer == null)
            {
                using (var dbMaster = await DbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities))
                {
                    kvRetailer = await dbMaster.SingleAsync<KvRetailer>(x => x.Id == retailerId);
                    if (kvRetailer != null)
                    {
                        CacheClient.Set(string.Format(KvConstant.RetailerIdCacheKey, retailerId), kvRetailer, DateTime.Now.AddDays(7));
                    }
                }
            }
            return kvRetailer;
        }

        private async Task<Domain.Common.KvGroup> GetRetailerGroup(int groupId)
        {
            var kvGroup = CacheClient.Get<Domain.Common.KvGroup>(string.Format(KvConstant.KvGroupCacheKey, groupId));
            if (kvGroup == null)
            {
                using (var dbMaster = await DbConnectionFactory.OpenAsync(KvConstant.KvMasterEntities))
                {
                    kvGroup = await dbMaster.SingleAsync<Domain.Common.KvGroup>(x => x.Id == groupId);
                    if (kvGroup != null)
                    {
                        CacheClient.Set(string.Format(KvConstant.KvGroupCacheKey, groupId), kvGroup, DateTime.Now.AddDays(7));
                    }
                }
            }
            return kvGroup;
        }

    }

    public class TaskVoidOrderModel : BaseWebhookOrderModel
    {
        public List<string> OrderCodeLst { get; set; }
    }

    public class ResyncTrackingInvoiceModel : BaseWebhookOrderModel
    {
        public string InvoiceCode { get; set; }
    }

    public class BaseWebhookOrderModel
    {
        public int RetailerId { get; set; }
        public int GroupId { get; set; }
        public KvRetailer KvRetailer { get; set; }
        public Domain.Common.KvGroup KvGroup { get; set; }

    }

    public class TiktokCallBackSubcribeInfo
    {
        public long AuthId { get; set; }
        public string Name { get; set; }
        public string IdentityKey { get; set; }
        public string Code { get; set; }
    }

    public class TaskVoidOrderReponse
    {
        public List<long> OrderIdLst { get; set; }
        public int RetailerId { get; set; }
        public string ConnectionString { get; set; }
        public string Prefix { get; set; }
    }
}

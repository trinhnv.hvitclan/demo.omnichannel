﻿using Demo.OmniChannel.Api.ServiceModel;
using ServiceStack;

namespace Demo.OmniChannel.Api.ServiceInterface
{
    public class PingApi :Service
    {
        public string Any(Ping req)
        {
            return "pong";
        }

        public string Any(PingVersion req)
        {
            return "PingVersion";
        }
    }
}
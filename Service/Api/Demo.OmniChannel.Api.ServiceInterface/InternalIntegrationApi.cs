﻿using Demo.Audit.Model.Message;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.Application.Type;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.CoreDemoContext.Interfaces;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannelCore.Api.Sdk.Common;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.OrmLite;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.OmniChannel.Api.ServiceModel;
using ProductCreateImageRequest = Demo.OmniChannelCore.Api.Sdk.RequestDtos.ProductCreateImageRequest;
using System.Text;
using Demo.OmniChannel.Utilities;
using Newtonsoft.Json;
using Demo.OmniChannel.ShareKernel.Exceptions;

namespace Demo.OmniChannel.Api.ServiceInterface
{
    public class InternalIntegrationApi : BaseInternalApi
    {
        public IOmniChannelService OmniChannelService { get; set; }
        public IOmniChannelRepository OmniChannelRepository { get; set; }
        public IAuditTrailInternalClient AuditTrailInternalClient { get; set; }
        public IOmniChannelScheduleRepository OmniChannelScheduleRepository { get; set; }
        public IScheduleService ScheduleService { get; set; }
        public ICategoryInternalClient CategoryInternalClient { get; set; }
        public IProductInternalClient ProductInternalClient { get; set; }
        public IAttributeInternalClient AttributeInternalClient { get; set; }
        public IProductAttributeInternalClient ProductAttributeInternalClient { get; set; }
        public IIntegrationEventService IntegrationEventService { get; set; }
        public IPriceBookInternalClient PriceBookInternalClient { get; set; }
        private readonly IDemoOmniChannelCoreContext _DemoOmniChannelCoreContext;
        public IOrderInternalClient OrderInternalClient { get; set; }
        public ICustomerInternalClient CustomerInternalClient { get; set; }
        public IKvLockRedis KvLockRedis { get; set; }
        public InternalIntegrationApi(IDemoOmniChannelCoreContext DemoOmniChannelCoreContext, ILogger<InternalIntegrationApi> logger) : base(logger)
        {
            _DemoOmniChannelCoreContext = DemoOmniChannelCoreContext;
        }
        public async Task<bool> Post(InternalUpdateRegisterChannel req)
        {
            var newGuid = !string.IsNullOrEmpty(req.GuidId) ? new Guid(req.GuidId) : Guid.NewGuid();
            var logObj = new LogObjectMicrosoftExtension(Logger, newGuid)
            {
                Action = "UpdateNeedRegisterChannel",
                RetailerId = InternalContext.RetailerId,
                OmniChannelId = req.ChannelId,
                BranchId = InternalContext.BranchId,
                RequestObject = req
            };
            var channel = await OmniChannelService.NeedRegisterChannel(req.ChannelId);
            var log = new AuditTrailLog
            {
                FunctionId = (int)FunctionType.PosParameter,
                Action = (int)AuditTrailAction.Update,
                CreatedDate = DateTime.Now,
                BranchId = InternalContext.BranchId,
                Content = $"Kênh bán {req?.ChannelName}: Kết nối tới {channel?.Name} tạm dừng do Seller cần xác thực tài khoản với sàn !!"
            };
            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, InternalContext.RetailerId, connectStringName, InternalContext.BranchId, AppSettings.Get<int>("ExecutionContext"));
            var coreContext = context.ConvertTo<KvInternalContext>();
            coreContext.UserId = context.User?.Id ?? 0;
            await AuditTrailInternalClient.AddLogAsync(coreContext, log);

            logObj.ResponseObject = channel;
            logObj.LogInfo();
            return true;
        }

        /// <summary>
        /// update IsActive va clear Cache  
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<object> Post(ChangeCache req)
        {
            var channels = await OmniChannelRepository.GetVisibleChannels(req.RetailerId);
            if (!channels.Any()) return new { Message = "Channel is null", Success = false };

            if (req.IsActive)
            {
                foreach (var channel in channels)
                {
                    channel.IsActive = true;
                }
            }
            else
            {
                CacheClient.Remove(string.Format(KvConstant.ChannelsByRetailerKey, InternalContext.RetailerId));

                foreach (var channel in channels)
                {
                    channel.IsActive = false;
                    await RemoveScheduleCacheByChannelId(channel.Id);
                    RemoveCacheChannelIdentity(channel.Type, channel.IdentityKey);
                    CacheClient.Remove(string.Format(KvConstant.ChannelByIdKey, channel.Id));
                    CacheClient.Remove(string.Format(KvConstant.ChannelAuthKey, channel.Id));
                    CacheClient.Remove(string.Format(KvConstant.LockMappingChannelId, channel.Id));

                    var log = new AuditTrailLog
                    {
                        FunctionId = (int)FunctionType.PosParameter,
                        Action = (int)AuditTrailAction.Update,
                        CreatedDate = DateTime.Now,
                        BranchId = InternalContext.BranchId,
                        Content = $"Kết nối {channel?.Name}: Tắt do kết nối tới {Enum.GetName(typeof(ChannelType), channel?.Type)} hết hạn"
                    };
                    var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
                    var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, InternalContext.RetailerId, connectStringName, InternalContext.BranchId, AppSettings.Get<int>("ExecutionContext"));
                    var coreContext = context.ConvertTo<KvInternalContext>();
                    coreContext.UserId = context.User?.Id ?? 0;
                    await AuditTrailInternalClient.AddLogAsync(coreContext, log);
                }
            }
            await OmniChannelRepository.UpdateAllAsync(channels);
            return new { Success = true };
        }
        public async Task<bool> Post(InternalDeactivateChannel req)
        {
            var newGuid = !string.IsNullOrEmpty(req.GuidId) ? new Guid(req.GuidId) : Guid.NewGuid();
            var logObj = new LogObjectMicrosoftExtension(Logger, newGuid)
            {
                Action = "DeactivateChannel",
                RetailerId = InternalContext.RetailerId,
                BranchId = InternalContext.BranchId,
                RequestObject = req
            };

            await RemoveScheduleCacheByChannelId(req.ChannelId);

            var channel = await OmniChannelService.DeactivateChannel(req.ChannelId);
            if (channel != null)
            {
                RemoveCacheChannelIdentity(channel.Type, channel.IdentityKey);
                CacheClient.Remove(string.Format(KvConstant.ChannelByIdKey, channel.Id));
            }
            CacheClient.Remove(string.Format(KvConstant.ChannelsByRetailerKey, InternalContext.RetailerId));
            await RemoveRetailerFromSet(channel?.RetailerId ?? 0);
            CacheClient.Remove(string.Format(KvConstant.ChannelAuthKey, req.ChannelId));
            CacheClient.Remove(string.Format(KvConstant.LockMappingChannelId, req.ChannelId));

            var log = new AuditTrailLog
            {
                FunctionId = (int)FunctionType.PosParameter,
                Action = (int)AuditTrailAction.Update,
                CreatedDate = DateTime.Now,
                BranchId = InternalContext.BranchId,
                Content = !string.IsNullOrEmpty(req.AuditContent) ? req.AuditContent : $"Kết nối hết hạn: Gian hàng {channel?.Name} sàn {Enum.GetName(typeof(ChannelType), channel?.Type)} bị ngắt kết nối do kết nối hết hạn."
            };
            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, InternalContext.RetailerId, connectStringName, InternalContext.BranchId, AppSettings.Get<int>("ExecutionContext"));
            var coreContext = context.ConvertTo<KvInternalContext>();
            coreContext.UserId = context.User?.Id ?? 0;
            await AuditTrailInternalClient.AddLogAsync(coreContext, log);
            logObj.ResponseObject = channel;
            logObj.LogInfo();
            return true;
        }

        public async Task<object> Post(InternalGetCategoryName req)
        {
            if (string.IsNullOrEmpty(InternalContext.Group?.ConnectionString))
            {
                return null;
            }
            var connectStringName = InternalContext.Group.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            using (var db = DbConnectionFactory.Open(connectStringName.ToLower()))
            {
                var categoryRepository = new CategoryRepository(db);
                return await categoryRepository.GetProductByNameAsync(InternalContext.RetailerId, req.Name, req.IsParent);
            }
        }
        public async Task<object> Post(InternalCreateOrUpdateCategory req)
        {
            var coreContext = await GetCoreContext();
            var category = new OmniChannelCore.Api.Sdk.Models.Category
            {
                Name = req.Name,
            };
            return await CategoryInternalClient.CreateCategoryAsync(coreContext, category);
        }

        public async Task<object> Post(InternalCreatedProduct req)
        {
            OmniChannelCore.Api.Sdk.Models.Product productResult = null;

            var coreContext = await GetCoreContext();
            var logObj = new LogObjectMicrosoftExtension(Logger, Guid.NewGuid())
            {
                Action = "InternalCreatedProduct",
                RetailerId = InternalContext.RetailerId,
                BranchId = InternalContext.BranchId,
                RequestObject = req
            };

            try
            {
                var productModel = req.Product.ConvertTo<OmniChannelCore.Api.Sdk.RequestDtos.ProductRequest>();

                var productImagesModel = req.ProductImages.Select(x => x.ConvertTo<ProductCreateImageRequest>()).ToList();

                productResult = await ProductInternalClient.CreatedProduct(coreContext, productModel, productImagesModel);

                if (productResult == null) return BadGateway("Server not found", new List<ErrorResult>());

                if (req.PriceBook != null)
                {
                    var priceBookModel = new OmniChannelCore.Api.Sdk.RequestDtos.PriceBookDetailRequest
                    {
                        PriceBookDetails = new List<OmniChannelCore.Api.Sdk.RequestDtos.PriceBookDetailDto>
                        {
                            new OmniChannelCore.Api.Sdk.RequestDtos.PriceBookDetailDto(req.PriceBook.PriceBookId,
                            productResult.Id,req.PriceBook.Price)
                        }
                    };
                    var responsePriceBook = await PriceBookInternalClient.AddBatchPriceBookDetail(coreContext, priceBookModel);
                    if (!responsePriceBook) await RollbackCreateProduct(coreContext, productResult);
                }
                logObj.ResponseObject = $"CreateProductSuccess with Id {productResult.Id}";
                logObj.LogInfo();
            }
            catch (Exception ex)
            {
                await RollbackCreateProduct(coreContext, productResult);
                logObj.LogError(ex);
                throw ex;
            }
            return productResult;
        }
        public async Task<object> Post(InternalCreatedrOder req)
        {
            var coreContext = await GetCoreContext();
            var coreSDKKVOrder = req.Order.ConvertTo<OmniChannelCore.Api.Sdk.Models.Order>();
            coreSDKKVOrder.RetailerId = InternalContext.RetailerId;
            coreSDKKVOrder.BranchId = InternalContext.BranchId;
            coreSDKKVOrder.OrderDetails = req.Order.OrderDetails.Map(x => x.ConvertTo<OmniChannelCore.Api.Sdk.Models.OrderDetail>());

            var logObj = new LogObjectMicrosoftExtension(Logger, Guid.NewGuid())
            {
                Action = "CreateOrderFromFBPos",
                RetailerId = InternalContext.RetailerId,
                BranchId = InternalContext.BranchId,
                RequestObject = req.Order
            };
            OmniChannelCore.Api.Sdk.Models.Order result;
            try
            {
                using (var cli = KvLockRedis.GetLockFactory())
                {
                    using (cli.AcquireLock(string.Format(KvConstant.LockCreateOrderFromFBPos, InternalContext.RetailerId, coreSDKKVOrder.Id), TimeSpan.FromSeconds(60)))
                    {
                        var kvContext = await _DemoOmniChannelCoreContext.CreateContext(InternalContext.RetailerId);
                        var connectStringName = kvContext.Group.ConnectionStringName;
                        string priceName = "Bảng giá chung";

                        await SetSaleChannelFB(coreSDKKVOrder, connectStringName);

                        priceName = await ExtraPrice(coreSDKKVOrder, connectStringName, priceName);

                        result = await OrderInternalClient.CreateOrder(
                            coreContext,
                            coreSDKKVOrder,
                            coreSDKKVOrder.OrderDetails.ToList(),
                            null);
                        if (result == null || result.Id <= 0)
                        {
                            var e = new KvException("Tạo đơn hàng không thành công");
                            logObj.LogError(e);
                            throw e;
                        }
                        await AddLogOrderAsync(coreContext, coreSDKKVOrder, result, connectStringName, priceName);

                        logObj.ResponseObject = result;
                        logObj.LogInfo();
                    }
                }
            }
            catch (Exception e)
            {
                logObj.LogError(e);
                throw;
            }
            return result;
        }

        private async Task SetSaleChannelFB(OmniChannelCore.Api.Sdk.Models.Order coreSDKKVOrder, string connectStringName)
        {
            SaleChannel saleChannel;
            var kvProductIds = coreSDKKVOrder.OrderDetails.Where(x => x.ProductId > 0).Select(x => x.ProductId).ToList();
            List<ProductBranchDTO> kvProducts = new List<ProductBranchDTO>();
            using (var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var productRepository = new ProductRepository(db);
                kvProducts = await productRepository.GetProductByIds(InternalContext.RetailerId, InternalContext.BranchId, kvProductIds, isLogMonitor: false);
            }
            foreach (var detail in coreSDKKVOrder.OrderDetails)
            {
                detail.Note = kvProducts.FirstOrDefault(x => x.Id == detail.ProductId)?.OrderTemplate;
                detail.ProductCode = kvProducts.FirstOrDefault(x => x.Id == detail.ProductId)?.Code;
            }

            using (var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower()))
            {
                var saleChannelRepository = new SaleChannelRepository(db);
                saleChannel = await saleChannelRepository.GetSaleChannelFB(InternalContext.RetailerId);
            }
            if (saleChannel != null) coreSDKKVOrder.SaleChannelId = saleChannel.Id;
        }

        public async Task<object> Post(InternalAttributeGetByNames req)
        {
            var coreContext = await GetCoreContext();
            return await AttributeInternalClient.GetAttributeByNamesAsync(coreContext, req.Names);
        }

        public async Task<object> Post(InternalAttributeBatchAdd req)
        {
            var coreContext = await GetCoreContext();
            return await AttributeInternalClient.BatchAddAsync(coreContext, req.Names);
        }

        public async Task<object> Post(InternalProductAttributeBatchAdd req)
        {
            var coreContext = await GetCoreContext();
            var productAndAttributes = req.ProductAndAttributes.Select(x => new OmniChannelCore.Api.Sdk.Models.ProductAndProductAttribute
            {
                Product = x.Product.ConvertTo<OmniChannelCore.Api.Sdk.Models.Product>(),
                ProductAttributes = x.ProductAttributes.Select(pa => new OmniChannelCore.Api.Sdk.Models.ProductAttribute
                {
                    ProductId = pa.ProductId,
                    AttributeId = pa.AttributeId,
                    Value = pa.Value
                }).ToList(),
            }).ToList();
            return await ProductAttributeInternalClient.BatchAddAsync(coreContext, productAndAttributes);
        }

        public async Task<object> Post(InternalAddAuditLog req)
        {
            var coreContext = await GetCoreContext();
            var auditLog = req.Log.ConvertTo<AuditTrailLog>();
            return await AuditTrailInternalClient.AddLogAsync(coreContext, auditLog);
        }

        public async Task<object> Post(GetChannelsById req)
        {
            if (req.Ids != null && req.Ids.Count > 0 && req.RetailerId > 0)
            {
                return await OmniChannelService.GetByChannelIds(req.Ids, req.RetailerId, true, true);
            }
            return new List<Domain.Model.OmniChannel>();
        }
        public async Task<object> Post(UpdateIconChannel req)
        {
            List<ShareKernel.Dto.OmniChannelDto> omniChannels;
            var db = await DbConnectionFactory.OpenAsync();
            using (db.OpenTransaction())
            {
                var omniRepo = new OmniChannelRepository(db);
                omniChannels = await omniRepo.GetChannelByChannelType(req.ChannelType, new List<int> { req.RetailerId });
            }
            if (omniChannels == null || !omniChannels.Any()) return 0;
            foreach (var channel in omniChannels)
            {
                var kvContext = await _DemoOmniChannelCoreContext.CreateContext(channel.RetailerId);
                var connectStringName = kvContext.Group.ConnectionStringName;
                using (var db1 = await DbConnectionFactory.OpenAsync(connectStringName.ToLower()))
                {
                    var saleChannelRepository = new SaleChannelRepository(db1);
                    var saleChannel = await saleChannelRepository.GetByOmmiChannelId(channel.RetailerId, channel.Id);
                    if (saleChannel != null)
                    {
                        saleChannel.Img = GetSaleChannelImage(req.ChannelType);
                        await saleChannelRepository.UpdateAsync(saleChannel);
                    }
                }
            }
            return 1;
        }
        public async Task<object> Post(EnableChannels req)
        {
            if (req.ChannelIds.Count == 0)
            {
                return false;
            }

            var channels = await OmniChannelService.GetDeactiveByChannelIds(req.ChannelIds);
            if (channels.Count == 0)
            {
                return false;
            }
            channels.ForEach(x => x.IsActive = true);
            return OmniChannelService.UpdateAllAsync(channels);
        }
        public async Task<object> Post(CreateCustomerSocial req)
        {
            var coreContext = await GetCoreContext();
            var customer = req.Customer.ConvertTo<OmniChannelCore.Api.Sdk.Models.CustomerFB>();
            customer.CustomerSocials = req.Customer.CustomerSocials.Map(x => x.ConvertTo<OmniChannelCore.Api.Sdk.Models.CustomerSocial>());
            return await CustomerInternalClient.CreateCustomerSocialAsync(coreContext, customer);
        }

        public async Task<object> Get(InternalGetSettings req)
        {
            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            using (var db = await DbConnectionFactory.OpenAsync(connectStringName?.ToLower()))
            {
                var posSettingsRepository = new PosSettingRepository(db);
                var posSettings = await posSettingsRepository.GetSettingAsync(InternalContext.RetailerId);
                return posSettings;
            }
        }

        public async Task<object> Get(InternalGetProductByCodes req)
        {
            var hashCode = new HashSet<string>(req.Codes);
            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            using (var db = await DbConnectionFactory.OpenAsync(connectStringName?.ToLower()))
            {
                var productRepository = new ProductRepository(db);
                var products = await productRepository.GetProductByCode(InternalContext.RetailerId, InternalContext.BranchId, hashCode, true);
                return products;
            }
        }
        public async Task<object> Get(InternalGetPrinceByChannelProduct req)
        {
            if (req.ChannelId <= 0 || req.ProductIds == null)
            {
                return new List<object>();
            }
            List<ProductForChannelMapping> listProduct = null;
            List<PriceBookDetail> listBooksDetail = new List<PriceBookDetail>();
            var retailerId = InternalContext.RetailerId;
            var channel = await OmniChannelService.GetByIdAsync(req.ChannelId);
            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            using (var db = await DbConnectionFactory.OpenAsync(connectStringName?.ToLower()))
            {
                var productRepo = new ProductRepository(db);
                listProduct = await productRepo.GetProductForMapping(retailerId, channel?.BranchId ?? 0, req.ProductIds);
            }
            if (listProduct == null || listProduct.Count == 0)
            {
                return new List<object>();
            }
            using (var db = await DbConnectionFactory.OpenAsync(connectStringName?.ToLower()))
            {
                var priceBookRepo = new PriceBookRepository(db);
                var priceBook = await priceBookRepo.GetById(retailerId, channel?.BasePriceBookId ?? 0);
                if (channel != null && (priceBook == null || priceBook.Id <= 0 || !priceBook.IsActive || priceBook.StartDate > DateTime.Now || priceBook.EndDate < DateTime.Now))
                {
                    channel.BasePriceBookId = 0;
                }
                listBooksDetail = await priceBookRepo.GetDetailById(retailerId, channel?.BasePriceBookId ?? 0, req.ProductIds);
            }
            return listProduct.Select(p => new
            {
                p.Id,
                p.Code,
                p.OnHand,
                Price = listBooksDetail?.FirstOrDefault(x => x.ProductId == p.Id)?.Price ?? p.BasePrice,
            }).ToList<object>();
        }
        public async Task<object> Get(FindCustomerBySocialId req)
        {
            var coreContext = await GetCoreContext();
            return await CustomerInternalClient.GetCustomerBySocialId(coreContext, req.SocialIds, req.PageIdFacebook);
        }

        #region Private method
        private string GetSaleChannelImage(byte channelType)
        {
            switch (channelType)
            {
                case (byte)ChannelType.Lazada:
                    return "lazada";

                case (byte)ChannelType.Shopee:
                    return "shopee";

                case (byte)ChannelType.Tiki:
                    return "tiki";

                case (byte)ChannelType.Sendo:
                    return "sendo";

                case (byte)ChannelType.Tiktok:
                    return "tiktok";

                default:
                    return null;
            }
        }

        private void RemoveCacheChannelIdentity(byte channelType, string identityKey)
        {
            if (string.IsNullOrEmpty(identityKey))
            {
                return;
            }
            using (var cli = HostContext.TryResolve<IRedisClientsManager>().GetClient())
            {
                var cacheClient = cli.As<Domain.Model.OmniChannel>();
                var identityCacheSetKey = string.Format(KvConstant.IdentityCacheSetKey, channelType, identityKey);
                var channelIds = cli.GetAllItemsFromSet(identityCacheSetKey);
                foreach (var channelId in channelIds)
                {
                    cacheClient.DeleteById(channelId);
                    cli.RemoveItemFromSet(identityCacheSetKey, channelId);
                }
                CacheClient.Remove(identityCacheSetKey);
            }
        }

        private async Task RemoveRetailerFromSet(long retailerId)
        {
            var existChannelRetailer = (await OmniChannelService.GetByRetailerAsync((int)retailerId)).Any(x => x.IsActive);
            if (!existChannelRetailer)
            {
                ((IKvCacheClient)CacheClient).RemoveItemFromSet(KvConstant.RetailersByChannelsKey, retailerId.ToString());
            }
        }

        private async Task RemoveScheduleCacheByChannelId(long channelId)
        {
            var runningSchedules = await OmniChannelScheduleRepository.WhereAsync(p =>
                p.OmniChannelId == channelId && (p.Type == (byte)ScheduleType.SyncModifiedProduct ||
                                                     p.Type == (byte)ScheduleType.SyncOrder || p.Type == (byte)ScheduleType.SyncPaymentTransaction));

            if (runningSchedules != null && runningSchedules.Any())
            {
                var existChannel = await OmniChannelService.GetByIdAsync(channelId);
                foreach (var item in runningSchedules)
                {
                    ScheduleService.Delete(existChannel.Type, (byte)item.Type, item.Id);
                }
            }
        }


        private async Task RollbackCreateProduct(KvInternalContext coreContext,
            OmniChannelCore.Api.Sdk.Models.Product productResult)
        {
            if (productResult == null) return;
            await ProductInternalClient.DeleteProduct(coreContext, productResult.Id);
        }
        private async Task AddLogOrderAsync(KvInternalContext coreContext, OmniChannelCore.Api.Sdk.Models.Order coreSDKKVOrder, OmniChannelCore.Api.Sdk.Models.Order result, string connectStringName, string priceName)
        {
            var productDetail = new StringBuilder();
            if (coreSDKKVOrder.OrderDetails != null && coreSDKKVOrder.OrderDetails.Any())
            {
                productDetail.Append(", bao gồm:<div>");
                foreach (var item in coreSDKKVOrder.OrderDetails)
                {
                    productDetail.Append(
                        $"- [ProductCode]{item.ProductCode}[/ProductCode] : {StringHelper.Normallize(item.Quantity)}*{StringHelper.NormallizeWfp((double)item.Price)}<br>");
                }

                productDetail.Append("</div>");
            }
            Customer customer = null;
            if (coreSDKKVOrder.CustomerId.HasValue)
            {
                customer = (await CustomerInternalClient.GetCustomerByIdAsync(coreContext, coreSDKKVOrder?.CustomerId ?? 0))?.ConvertTo<Customer>();
                if (customer == null)
                {
                    using (var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower()))
                    {
                        using (_ = db.OpenTransaction(System.Data.IsolationLevel.ReadUncommitted))
                        {
                            var customerRepository = new CustomerRepository(db);
                            customer = await customerRepository.GetByIdAsync(coreSDKKVOrder.CustomerId.Value);
                        }
                    }
                }
            }

            var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, InternalContext.RetailerId, connectStringName, InternalContext.BranchId, AppSettings.Get<int>("ExecutionContext"));

            var log = new AuditTrailLog
            {
                FunctionId = (int)FunctionType.OrderFBPOS,
                Action = (int)AuditTrailAction.OrderIntergate,
                BranchId = InternalContext.BranchId,
                CreatedDate = DateTime.Now,
                Content =
                $"Tạo đơn đặt hàng: [OrderCode]{result.Code}[/OrderCode] (Phiếu tạm), khách hàng: {customer?.Code ?? "Khách lẻ"}, thời gian: {coreSDKKVOrder.PurchaseDate:dd/MM/yyyy HH:mm:ss}, bảng giá: {priceName}, Người tạo: {context.User?.GivenName ?? "Admin"}, Người nhận đặt: {context.User?.GivenName ?? "Admin"} {productDetail}"
            };
            await AuditTrailInternalClient.AddLogAsync(coreContext, log);
        }
        private async Task<string> ExtraPrice(OmniChannelCore.Api.Sdk.Models.Order coreSDKKVOrder, string connectStringName, string priceName)
        {
            if (!string.IsNullOrEmpty(coreSDKKVOrder.Extra))
            {
                using (var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower()))
                {
                    var parseExtra = JsonConvert.DeserializeObject<ChannelClient.Models.Extra>(coreSDKKVOrder.Extra);

                    if (parseExtra != null)
                    {
                        var priceBookRepository = new PriceBookRepository(db);
                        var priceBook = await priceBookRepository.GetById(InternalContext.RetailerId, parseExtra.PriceBookId.Id);
                        if (priceBook != null && !priceBook.IsActive)
                        {
                            var extra = new ChannelClient.Models.Extra
                            {
                                PriceBookId = new ChannelClient.Models.PriceBook
                                {
                                    Id = 0,
                                    Name = priceName
                                }
                            };
                            coreSDKKVOrder.Extra = extra.ToSafeJson();
                        }
                        priceName = parseExtra.PriceBookId.Name;
                    }
                }
            }

            return priceName;
        }
        #endregion
    }
}

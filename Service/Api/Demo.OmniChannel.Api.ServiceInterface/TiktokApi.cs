﻿using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.EndPoints;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.Sdk.Dtos;
using Demo.OmniChannel.Sdk.RequestDtos;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Microsoft.Extensions.Logging;
using ServiceStack.Configuration;
using System;
using System.Threading.Tasks;
namespace Demo.OmniChannel.Api.ServiceInterface
{
    public class TikTokApi : BaseApi
    {
        private readonly IAppSettings _settings;
        private readonly ChannelClient.Impls.ChannelClient _channelClient;
        private readonly IOmniChannelAuthService _omniChannelAuthService;
        private readonly IOmniChannelPlatformService _omniChannelPlatformService;
        public TikTokApi(ILogger<ShopeeApi> logger,
            IAppSettings settings,
            ChannelClient.Impls.ChannelClient channelClient,
            IOmniChannelAuthService omniChannelAuthService,
            IOmniChannelPlatformService omniChannelPlatformService
            ) : base(logger)
        {
            _ = new SelfAppConfig(settings);
            _settings = settings;
            _channelClient = channelClient;
            _omniChannelAuthService = omniChannelAuthService;
            _omniChannelPlatformService = omniChannelPlatformService;
        }

        public async Task<object> Get(TikTokAuthConfig req)
        {
            var getConfig = await _omniChannelPlatformService.GetCurrent((int)PlatformType.Tiktok, req.AppSupport);

            if (getConfig?.DataObject == null) throw new OmniException("Vui lòng liên hệ nhà cung cấp!");
            var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            return new
            {
                AppKey = getConfig.DataObject.AppKey,
                TimeStamp = timestamp,
                PlatformId = getConfig.Id,
                OAuthURL = $"{SelfAppConfig.Tiktok.AuthEndPoint}/{TiktokEndpoint.AuthPartner}"
            };
        }

        public async Task<object> Get(TiktTokProfileAndUpdateAuth req)
        {
            var getConfig = await _omniChannelPlatformService.GetCurrent((int)PlatformType.Tiktok);
            var client = _channelClient.GetClient((int)ChannelTypeEnum.Tiktok, _settings);
            var getToken = await client.CreateToken(req.Code, new ChannelAuth(), getConfig);
            var getShopInfo = await client.GetShopInfo(new ChannelAuth { AccessToken = getToken.AccessToken },
                null, false, getConfig);

            var result = await _omniChannelAuthService.CreateOrUpdate(new Domain.Model.OmniChannelAuth
            {
                ShopId = long.Parse(getShopInfo.IdentityKey),
                AccessToken = getToken.AccessToken,
                RefreshToken = getToken.RefreshToken,
                ExpiresIn = getToken.ExpiresIn,
                RefreshExpiresIn = getToken.RefreshExpiresIn,
            });

            return new
            {
                AuthId = result,
                Name = getShopInfo.Name,
                IdentityKey = getShopInfo.IdentityKey,
                PlatformId = getConfig.Id
            };
        }

        public object Get(TiktTokGetProfileInfo req)
        {
            return CacheClient.Get<TiktokCallBackSubcribeInfo>(string.Format(KvConstant.TiktokCallBackSubcribeKey, req.State));
        }

        public async Task<ProfileAndTokenResp> Get(TikTokEnableChannel req)
        {
            var client = _channelClient.GetClient((int)ChannelTypeEnum.Tiktok, _settings);
            var getConfig = await _omniChannelPlatformService.GetById(req.PlatformId);
            var getToken = await client.CreateToken(req.Code, new ChannelAuth(), getConfig);
            if (getToken == null) throw new OmniException("Có lỗi xảy ra. Vui lòng thử lại trong ít phút.");

            var getShopInfo = await client.GetShopInfo(
                new ChannelAuth { AccessToken = getToken.AccessToken }, null, true,
                getConfig);
            if (getShopInfo == null) throw new OmniException("Có lỗi xảy ra. Vui lòng thử lại trong ít phút.");
            if (string.IsNullOrEmpty(req.IdentityKey) ||
                !req.IdentityKey.Equals(getShopInfo.IdentityKey, StringComparison.OrdinalIgnoreCase))
            {
                throw new OmniException(
                    $"Tài khoản kết nối lại không phải tài khoản {req.ShopName}. Xin vui lòng thử lại.");
            }

            return new ProfileAndTokenResp
            {
                Name = getShopInfo.Name,
                Email = getShopInfo.Email,
                Auth = new ChannelAuthRequest
                {
                    AccessToken = getToken.AccessToken,
                    RefreshToken = getToken.RefreshToken,
                    ExpiresIn = getToken.ExpiresIn,
                    RefreshExpiresIn = getToken.RefreshExpiresIn,
                    ShopId = long.Parse(getShopInfo.IdentityKey)
                }
            };
        }
    }
}

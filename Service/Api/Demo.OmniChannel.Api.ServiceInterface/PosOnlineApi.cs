using System.Threading.Tasks;
using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.Services.Interfaces;
using Microsoft.Extensions.Logging;

namespace Demo.OmniChannel.Api.ServiceInterface
{
    public class PosOnlineApi : BaseInternalApi
    {
        public IProductService ProductService { get; set; }
        public IProductBranchService ProductBranchService { get; set; }
        public IPriceBookService PriceBookService { get; set; }

        public PosOnlineApi(ILogger<PosOnlineApi> logger) : base(logger)
        {
        }

        public async Task<object> Post(InternalProduct internalProduct)
        {
            return await ProductService.GetProducts(internalProduct);
        }

        public async Task<object> Post(InternalProductBranch internalProductBranch)
        {
            return await ProductBranchService.GetProductBranches(internalProductBranch);
        }

        public async Task<object> Get(InternalPriceBook internalPriceBook)
        {
            return await PriceBookService.GetPriceBooks(internalPriceBook);
        }
    }
}
﻿using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.Repository.Common;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.Sdk.RequestDtos;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GetPermissionAddChannelRequest = Demo.OmniChannel.Sdk.RequestDtos.GetPermissionAddChannelRequest;
using GetPermissionRequest = Demo.OmniChannel.Sdk.RequestDtos.GetPermissionRequest;
using Demo.OmniChannel.Resource;
using Demo.OmniChannel.Repository.Impls;

namespace Demo.OmniChannel.Api.ServiceInterface
{
    public class LicenseApi : BaseApi
    {
        public IPosSettingRepository PosSettingRepository { get; set; }
        public IOmniChannelRepository ChannelResRepository { get; set; }
        public IOmniChannelService OmniChannelService { get; set; }
        private static readonly int MaxChannelTrialRetailer = 5;
        private readonly List<string> posParameterIntegateName = new List<string> {
                        nameof(PosSetting.UseTikiIntergate),
                        nameof(PosSetting.UseLazadaIntergate),
                        nameof(PosSetting.UseSendoIntergate),
                        nameof(PosSetting.UseShopeeIntergate),
                    };

        public LicenseApi(ILogger<LicenseApi> logger) : base(logger)
        {
        }

        public async Task<GetPermissionAddChannelResponse> Get(GetPermissionAddChannelRequest req)
        {
            var logMessage = new LogObjectMicrosoftExtension(Logger, Guid.NewGuid())
            {
                Action = "GetPermissionReq",
                RetailerId = req.RetailerId,
                BranchId = InternalContext.BranchId,
                RequestObject = req
            };

            try
            {
                if (req.RetailerId == 0)
                {
                    logMessage.Description = "Retailer is null";
                    logMessage.LogError(new OmniChannelCore.Api.Sdk.Common.KvValidateException("Retailer is null"));
                    throw new KvValidateException("Có lỗi xảy ra. Vui lòng liên hệ nhà cung cấp!");
                }

                if (req.OmniChannelId != 0)
                {
                    var reconnectChannelResult = await IsValidToReconnectChannel(req);
                    return reconnectChannelResult;
                }

                var result = await HandlePermissionLogic(req);
                return new GetPermissionAddChannelResponse
                {
                    IsSuccess = result.IsSuccess,
                    ErrorMessage = result.ErrorMessage
                };
            }
            catch (Exception e)
            {
                logMessage.LogWarning(e.Message);
                throw;
            }
        }

        public async Task<GetPermissionResponse> Get(GetPermissionRequest req)
        {
            var logMessage = new LogObjectMicrosoftExtension(Logger, Guid.NewGuid())
            {
                Action = "GetPermissionRequest",
                RetailerId = req.RetailerId,
                BranchId = InternalContext.BranchId,
                RequestObject = req
            };

            try
            {
                if (req.RetailerId == 0)
                {
                    logMessage.Description = "Retailer is null";
                    logMessage.LogError(new OmniChannelCore.Api.Sdk.Common.KvValidateException("Retailer is null"));
                    throw new KvValidateException("Có lỗi xảy ra. Vui lòng liên hệ nhà cung cấp!");
                }

                var result = await HandlePermissionLogic(req);
                var channels = await OmniChannelService.GetByRetailerAsync(req.RetailerId);
                var channelsReponseModel = channels.Select(p => p.ConvertTo<ChannelResponse>()).ToList();
                return new GetPermissionResponse
                {
                    IsActive = result.IsActive,
                    IsExpired = result.IsExpired,
                    BlockUnit = result.BlockUnit,
                    Channels = channelsReponseModel
                };
            }
            catch (Exception e)
            {
                logMessage.LogWarning(e.Message);
                throw;
            }
        }
        public async Task<GetLicenseFiveDayResponse> Get(GetLicenseFiveDayRequest req)
        {
            var logMessage = new LogObjectMicrosoftExtension(Logger, Guid.NewGuid())
            {
                Action = "GetLicenseFiveDayRequest",
                RetailerId = InternalContext.RetailerId,
                BranchId = InternalContext.BranchId,
                RequestObject = req
            };

            try
            {
                if (InternalContext.RetailerId == 0)
                {
                    logMessage.Description = "Retailer is null";
                    logMessage.LogError(new OmniChannelCore.Api.Sdk.Common.KvValidateException("Retailer is null"));
                    throw new KvValidateException(KvMessage.RetailerNotFound);
                }

                var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1).ToLower();
                using (var db = DbConnectionFactory.Open(connectStringName))
                {
                    var retailer = await GetRetailerById(InternalContext.RetailerId);
                    var posSettingsRepository = new PosSettingRepository(db);
                    var omniChannelPosParameters = await posSettingsRepository.GetSettingOmniChannel(InternalContext.RetailerId, nameof(PosSetting.OmniChannel));
                    if (omniChannelPosParameters != null)
                    {
                        int dayCountDown = CountDownExpired(omniChannelPosParameters, retailer);
                        return new GetLicenseFiveDayResponse
                        {
                            IsExpired = dayCountDown > 0 ? false : true,
                            DayCountDown = dayCountDown,
                        };
                    }
                }
                return new GetLicenseFiveDayResponse
                {
                    IsExpired = true,
                    DayCountDown = 0,
                };
            }
            catch (Exception e)
            {
                logMessage.LogWarning(e.Message);
                throw;
            }
        }

        private async Task<PermissionLogicResult> HandlePermissionLogic(IGetPermissionRequest req)
        {
            var constBlockDateTime = DateTime.Parse(AppSettings.Get<string>("LicenseKOLBlockDate", "2021-11-06"));

            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1).ToLower();
            using (var db = DbConnectionFactory.Open(connectStringName))
            {
                var retailer = await GetRetailerById(req.RetailerId);
                var activeChannelUsed = await ChannelResRepository.GetActiveChannelByRetailer(req.RetailerId);
                var posParameters = await db.SelectAsync<Demo.OmniChannel.Domain.Model.PosParameter>(t => t.RetailerId == req.RetailerId);
                var omniChannelPosParameters = posParameters.FirstOrDefault(t => t.Key.Equals(nameof(PosSetting.OmniChannel)));
                var maximumChannels = GetMaxChannelByRetailer(retailer, omniChannelPosParameters);

                #region Old customer using before KiotOnline release
                if (retailer.ContractDate <= constBlockDateTime && (omniChannelPosParameters == null || !omniChannelPosParameters.isActive))
                {
                    if (omniChannelPosParameters != null)
                    {
                        var isExpired = IsExpiredLicense(omniChannelPosParameters, retailer);
                        if (isExpired)
                        {
                            return new PermissionLogicResult
                            {
                                IsActive = false,
                                IsExpired = true,
                                IsSuccess = false,
                                ErrorMessage = "Thời hạn kết nối tới các sàn thương mại điện tử của gian hàng đã hết hạn. Vui lòng liên hệ Hotline 1800 6162 để đăng ký",
                                BlockUnit = maximumChannels,
                            };
                        }
                    }

                    if (activeChannelUsed.Count >= maximumChannels)
                    {
                        return new PermissionLogicResult
                        {
                            IsActive = true,
                            IsSuccess = false,
                            IsExpired = false,
                            ErrorMessage = $"Không thể kết nối thêm. Số lượng kết nối tối đa đến các sàn thương mại điện tử của gian hàng đã đạt {maximumChannels}. Vui lòng liên hệ Hotline 1800 6162 để đăng ký thêm kết nối",
                            BlockUnit = maximumChannels,
                        };
                    }

                    return new PermissionLogicResult
                    {
                        IsActive = true,
                        IsSuccess = true,
                        IsExpired = false,
                        ErrorMessage = "",
                        BlockUnit = maximumChannels,
                    };
                }
                #endregion

                #region Retailer registed with OmniChannel lincense.
                if (omniChannelPosParameters != null)
                {
                    var isExpired = IsExpiredLicense(omniChannelPosParameters, retailer);
                    if (isExpired)
                    {
                        return new PermissionLogicResult
                        {
                            IsActive = false,
                            IsSuccess = false,
                            IsExpired = true,
                            ErrorMessage = "Thời hạn kết nối tới các sàn thương mại điện tử của gian hàng đã hết hạn. Vui lòng liên hệ Hotline 1800 6162 để đăng ký",
                            BlockUnit = maximumChannels,
                        };
                    }

                    if (activeChannelUsed.Count >= maximumChannels)
                    {
                        return new PermissionLogicResult
                        {
                            IsActive = omniChannelPosParameters.isActive,
                            IsSuccess = false,
                            IsExpired = false,
                            ErrorMessage = $"Không thể kết nối thêm. Số lượng kết nối tối đa đến các sàn thương mại điện tử của gian hàng đã đạt {maximumChannels}. Vui lòng liên hệ Hotline 1800 6162 để đăng ký thêm kết nối",
                            BlockUnit = maximumChannels,
                        };
                    }

                    if (!omniChannelPosParameters.isActive)
                    {
                        return new PermissionLogicResult
                        {
                            IsActive = false,
                            IsSuccess = false,
                            IsExpired = false,
                            ErrorMessage = "Thời hạn kết nối tới các sàn thương mại điện tử của gian hàng đã hết hạn. Vui lòng liên hệ Hotline 1800 6162 để đăng ký",
                            BlockUnit = maximumChannels,
                        };
                    }

                    return new PermissionLogicResult
                    {
                        IsActive = omniChannelPosParameters.isActive,
                        IsSuccess = omniChannelPosParameters.isActive,
                        IsExpired = false,
                        ErrorMessage = "",
                        BlockUnit = maximumChannels,
                    };
                }
                #endregion

                return new PermissionLogicResult
                {
                    IsActive = false,
                    IsSuccess = false,
                    IsExpired = false,
                    ErrorMessage = "Có lỗi xảy ra. Vui lòng liên hệ nhà cung cấp!",
                    BlockUnit = 0,
                };
            }
        }


        #region PRIVATE METHOD

        private bool IsExpiredLicense(PosParameter posParameter, KvRetailer retailer)
        {
            var today = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day);
            DateTime expiryDate;
            if (posParameter.ExpiredDate == default(DateTime) || posParameter.ExpiredDate == null)
                expiryDate = (DateTime)retailer.ExpiryDate;
            else
                expiryDate = (DateTime)posParameter.ExpiredDate;
            if (expiryDate >= today)
                return false;
            return true;
        }
        private int CountDownExpired(PosParameter posParameter, KvRetailer retailer)
        {
            var today = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day);
            DateTime expiryDate;
            if (posParameter.ExpiredDate == default(DateTime) || posParameter.ExpiredDate == null)
                expiryDate = (DateTime)retailer.ExpiryDate;
            else
                expiryDate = (DateTime)posParameter.ExpiredDate;

            return (expiryDate - today).Days + 1;
        }
        private async Task<KvRetailer> GetRetailerById(long retailerId)
        {
            using (var db = DbConnectionFactory.Open(KvConstant.KvMasterEntities))
            {
                var retailer = await db.SingleAsync<KvRetailer>(x => x.Id == retailerId);
                return retailer;
            }
        }

        private async Task<GetPermissionAddChannelResponse> IsValidToReconnectChannel(GetPermissionAddChannelRequest req)
        {
            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1).ToLower();
            using (var db = DbConnectionFactory.Open(connectStringName))
            {
                var retailer = await GetRetailerById(req.RetailerId);
                var activeChannelUsed = await ChannelResRepository.GetActiveChannelByRetailer(req.RetailerId);
                var posParameters = await db.SelectAsync<Demo.OmniChannel.Domain.Model.PosParameter>(t => t.RetailerId == req.RetailerId);
                var omniChannelPosParameters = posParameters.FirstOrDefault(t => t.Key.Equals(nameof(PosSetting.OmniChannel)));
                var maximumChannels = GetMaxChannelByRetailer(retailer, omniChannelPosParameters);

                if (activeChannelUsed.Exists(t => t.Id == req.OmniChannelId && t.IsActive)) //Case: Reconnect khi sắp hết hạn
                {
                    if (activeChannelUsed.Count > maximumChannels)
                        return new GetPermissionAddChannelResponse
                        {
                            IsSuccess = false,
                            ErrorMessage = $"Không thể kết nối thêm. Số lượng kết nối tối đa đến các sàn thương mại điện tử của gian hàng đã đạt {maximumChannels}. Vui lòng liên hệ Hotline 1800 6162 để đăng ký thêm kết nối"
                        };
                }
                else if(activeChannelUsed.Count >= maximumChannels)
                {
                    return new GetPermissionAddChannelResponse
                    {
                        IsSuccess = false,
                        ErrorMessage = $"Không thể kết nối thêm. Số lượng kết nối tối đa đến các sàn thương mại điện tử của gian hàng đã đạt {maximumChannels}. Vui lòng liên hệ Hotline 1800 6162 để đăng ký thêm kết nối"
                    };
                }
                return new GetPermissionAddChannelResponse
                {
                    IsSuccess = true,
                    ErrorMessage = ""
                };
            }
        }

        private int GetMaxChannelByRetailer(KvRetailer kvRetailer, PosParameter posParameter)
        {
            var constBlockDateTime = DateTime.Parse(AppSettings.Get<string>("LicenseKOLBlockDate", "2021-11-06"));

            if (InternalContext.Group?.IsFreePremium == true)
            {
                return posParameter.BlockUnit.GetValueOrDefault();
            }
            //Old Retailer
            if (kvRetailer.ContractDate <= constBlockDateTime && (posParameter == null || !posParameter.isActive))
                return kvRetailer.MaximumSaleChannels.GetValueOrDefault(MaxChannelTrialRetailer) * posParameterIntegateName.Count;
            //Retailer have Omnichannel posparameter
            if (posParameter != null)
                return posParameter.BlockUnit.GetValueOrDefault();
            return 0;
        }
        #endregion
    }
}

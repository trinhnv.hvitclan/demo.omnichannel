﻿using ServiceStack.FluentValidation.Validators;
using ServiceStack.FluentValidation;
using Demo.OmniChannel.Api.ServiceModel.Retailer;

namespace Demo.OmniChannel.Api.ServiceInterface.Validators
{
    public class AddProductMappingValidator : AbstractValidator<AddProductAndMappingByRetail>
    {
        public AddProductMappingValidator()
        {
            ValidateProperties();
        }

        protected void ValidateProperties()
        {
            RuleFor(s => s)
                .Custom((obj, context) =>
                {
                    ValidateModel(obj, context);
                });
        }

        #region Private method
        public void ValidateModel(AddProductAndMappingByRetail objValid,
            CustomContext context)
        {

            if (objValid.ChannelProduct == null)
            {
                context.AddFailure("RequestBody truyền vào không được để trống");
            }

            if (objValid.ChannelProduct.KvProductId == 0)
            {
                context.AddFailure("ProductKvId định dạng không hợp lệ");
            }
        }
        #endregion
    }
}

﻿using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.Api.ServiceModel.Request;
using Demo.OmniChannel.ShareKernel.Common;
using ServiceStack.FluentValidation;
using ServiceStack.FluentValidation.Validators;
using System;

namespace Demo.OmniChannel.Api.ServiceInterface
{
    public class CreateUpdateChannelValidator : AbstractValidator<ChannelCreateOrUpdate>
    {
        public CreateUpdateChannelValidator()
        {
            ValidateProperties();
        }

        protected void ValidateProperties()
        {
            RuleFor(s => s)
                .Custom((channel, context) =>
                {
                    ValidateChannel(channel, context);
                });
        }

        #region Private method
        public void ValidateChannel(ChannelCreateOrUpdate channelRequest,
            CustomContext context)
        {
            var channelRq = channelRequest.Channel;

            if (!Enum.IsDefined(typeof(ChannelType), (int)channelRq.Type))
            {
                context.AddFailure("Type không tồn tại trong enum");
            }

            if (channelRequest.IsSyncOnHand &&
                !Enum.IsDefined(typeof(SyncOnHandFormula), (int)channelRq.SyncOnHandFormula))
            {
                context.AddFailure("SyncOnHandFormula không tồn tại trong enum");
            }

            if (string.IsNullOrEmpty(channelRq.IdentityKey))
            {
                context.AddFailure("IdentityKey định dạng không đúng");
            }
        }
        #endregion
    }
}

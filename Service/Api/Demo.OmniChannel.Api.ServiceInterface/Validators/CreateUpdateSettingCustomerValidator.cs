﻿using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.ShareKernel.Common;
using ServiceStack.FluentValidation;
using ServiceStack.FluentValidation.Validators;
using System;

namespace Demo.OmniChannel.Api.ServiceInterface
{
    public class CreateUpdateSettingCustomerValidator : AbstractValidator<CreateUpdateSettingCustomer>
    {
        public CreateUpdateSettingCustomerValidator()
        {
            ValidateProperties();
        }

        protected void ValidateProperties()
        {
            RuleFor(s => s)
                .Custom((setting, context) =>
                {
                    ValidateModel(setting, context);
                });
        }

        #region Private method
        public void ValidateModel(CreateUpdateSettingCustomer setting,
            CustomContext context)
        {
            if (string.IsNullOrEmpty(setting.KeyUpdate))
            {
                context.AddFailure("KeyUpdate yêu cầu bắt buộc");
            }

            if (!string.IsNullOrEmpty(setting.KeyUpdate) &&
                !Enum.IsDefined(typeof(SettingCustomer), setting.KeyUpdate))
            {
                context.AddFailure("KeyUpdate không tồn tại trong enum");
            }
        }
        #endregion
    }
}

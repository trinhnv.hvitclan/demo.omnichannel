﻿using Demo.OmniChannel.Api.ServiceModel.Retailer;
using ServiceStack.FluentValidation.Validators;
using ServiceStack.FluentValidation;

namespace Demo.OmniChannel.Api.ServiceInterface.Validators
{
    public class RemoveProductMappingValidator: AbstractValidator<RemoveProductMappingByRetail>
    {
        public RemoveProductMappingValidator()
        {
            ValidateProperties();
        }

        protected void ValidateProperties()
        {
            RuleFor(s => s)
                .Custom((obj, context) =>
                {
                    ValidateModel(obj, context);
                });
        }

        #region Private method
        public void ValidateModel(RemoveProductMappingByRetail objValid,
            CustomContext context)
        {

            if (objValid.KvProductId == 0)
            {
                context.AddFailure("ProductKvId định dạng không hợp lệ");
            }
            if (objValid.ChannelId == 0)
            {
                context.AddFailure("ChannelId định dạng không hợp lệ");
            }
            if (string.IsNullOrEmpty(objValid.ChannelProductId))
            {
                context.AddFailure("ChannelProductId không được để trống");
            }
        }
        #endregion
    }
}

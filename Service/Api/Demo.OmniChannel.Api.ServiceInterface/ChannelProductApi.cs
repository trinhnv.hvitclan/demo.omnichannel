﻿using Demo.Audit.Model.Message;
using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.ShareKernel.Dto;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Common;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ServiceStack.Caching;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.Api.ServiceInterface
{
    public class ChannelProductApi : BaseApi
    {
        public IProductMongoService ProductMongoService { get; set; }
        public IProductMappingService ProductMappingService { get; set; }
        public IOmniChannelService OmniChannelService { get; set; }
        public IOmniChannelRepository OmniChannelRepository { get; set; }
        public IOmniChannelAuthRepository OmniChannelAuthRepository { get; set; }
        public IAuditTrailInternalClient AuditTrailInternalClient { get; set; }
        public IPriceBusiness PriceBusiness { get; set; }
        public IOnHandBusiness OnHandBusiness { get; set; }
        public IChannelBusiness ChannelBusiness { get; set; }
        public IKvLockRedis KvLockRedis { get; set; }
        public IOmniChannelPlatformService OmniChannelPlatformService { get; set; }
        public IProductService ProductService { get; set; }
        public IOrderMongoService OrderMongoService { get; set; }
        public IInvoiceMongoService InvoiceMongoService { get; set; }
        private readonly IAppSettings _settings;
        private readonly ICacheClient _cacheClient;
        public ChannelProductApi(IAppSettings settings, ICacheClient cacheClient, ILogger<ChannelProductApi> logger) : base(logger)
        {
            _settings = settings;
            _cacheClient = cacheClient;
        }

        public async Task<object> Any(GetProductByChannelId req)
        {
            var retVal = new Domain.Common.PagingDataSource<MapProduct>();
            var productMap = new List<ProductMapping>();
            if (!string.IsNullOrEmpty(req.KvTerm))
            {
                productMap = await ProductMappingService.GetBySearchKvProduct(InternalContext.RetailerId, req.ChannelId, req.KvTerm);
                if (!productMap.Any())
                {
                    return retVal;
                }
            }
            var channel = await ChannelBusiness.GetChannel(req.ChannelId, InternalContext.RetailerId, Guid.NewGuid());
            var listIds = productMap.Select(x => x.CommonProductChannelId).ToList();
            var productsDataSource = await ProductMongoService.GetAllByChannelId(InternalContext.RetailerId, req.ChannelId,
                req.Top, req.Skip, isStringItemId: ConvertHelper.CheckUseStringItemId(channel?.Type), req.ChannelTerm,
                listIds, req.IsConnected, req.IsSyncSuccess);
            var products = productsDataSource.Data.Select(x => new MapProduct
            {
                Id = x.Id,
                BranchId = x.BranchId,
                ChannelId = x.ChannelId,
                ChannelType = x.ChannelType,
                CommonItemId = x.CommonItemId,
                CommonParentItemId = x.CommonParentItemId,
                IsConnected = x.IsConnected,
                ItemId = x.CommonItemId,
                ParentItemId = x.CommonParentItemId,
                ItemName = x.ItemName,
                ItemSku = x.ItemSku,
                RetailerCode = x.RetailerCode,
                RetailerId = x.RetailerId,
                Status = x.Status,
                StrItemId = x.StrItemId,
                StrParentItemId = x.StrParentItemId,
                Type = x.Type,
                SyncErrors = x.SyncErrors,
                ItemImages = x.ItemImages,
                SuperId = x.SuperId,
            }).ToList();
            var itemIds = productsDataSource.Data.Select(x => x.StrItemId).ToList();

            Dictionary<string, ProductMapping> mappings;
            var prdMappings = await ProductMappingService.Get(InternalContext.RetailerId, req.ChannelId, null, null, itemIds);
            try
            {
                mappings = prdMappings.ToDictionary(x => x.CommonProductChannelId, y => y);
            }
            catch (Exception ex)
            {
                prdMappings = prdMappings.GroupBy(x => x.CommonProductChannelId).Select(x => x.First()).ToList();
                mappings = prdMappings.ToDictionary(x => x.CommonProductChannelId, y => y);
                Logger.LogError(ex.Message, ex);
            }
            foreach (var p in products)
            {
                p.KvProductId = mappings.ContainsKey(p.CommonItemId) ? (mappings[p.CommonItemId]?.ProductKvId ?? 0) : 0;
                p.KvProductSku = mappings.ContainsKey(p.CommonItemId) ? mappings[p.CommonItemId]?.ProductKvSku : string.Empty;
                p.ErrorMessage = p.SyncErrors?.Select(x => x.Message).Join("; ");
                p.KvErrorMessage = p.SyncErrors?.Select(x => GeneralHelper.GetKvMessage(x.Message)).Join("; ");
            }
            products = products.OrderBy(x => x.ItemName).ThenBy(x => x.ItemSku).ThenBy(x => x.CommonItemId).ToList();
            retVal.Total = productsDataSource.Total;
            retVal.Data = products;
            return retVal;
        }

        public async Task<IList<MapProduct>> Any(GetProductByKvProductIds req)
        {
            var retVal = new List<MapProduct>();

            if (req.KvProductIds?.Any() != true) return retVal;

            var channelTypes = await OmniChannelRepository.GetAsync<OmniChannelTypeDto>(x => x.RetailerId == InternalContext.RetailerId);

            if (channelTypes?.Any() != true) return retVal;

            var channelIds = channelTypes.Select(x => x.Id).Distinct().ToList();

            var productMaps = await ProductMappingService.Get(InternalContext.RetailerId, channelIds, req.KvProductIds);

            if (productMaps?.Any() != true) return retVal;

            var channelProductIds = productMaps.Select(x => x.CommonProductChannelId).ToList();
            var channelProducts = await ProductMongoService.Get(InternalContext.RetailerId, channelIds, channelProductIds);

            retVal = channelProducts.ConvertAll(x => x.ConvertTo<MapProduct>());

            foreach (var item in retVal)
            {
                if (productMaps.Any(x => x.CommonProductChannelId == item.CommonItemId))
                {
                    item.KvProductId = productMaps.First(x => x.CommonProductChannelId == item.CommonItemId).ProductKvId;
                    OmniChannelTypeDto kvChannel = channelTypes.First(x => x.Id == item.ChannelId);
                    item.ChannelType = kvChannel.Type;
                    item.ShopId = kvChannel.IdentityKey;
                }
            }

            return retVal;
        }

        public async Task<bool> Post(AddProductAndMapping req)
        {
            var logId = Guid.NewGuid();
            var logMessage = new LogObjectMicrosoftExtension(Logger, logId)
            {
                Action = "AddProductAndMapping",
                RetailerId = InternalContext.RetailerId,
                RequestObject = req
            };

            var channel = await OmniChannelRepository.GetByIdAsync(req.Product.ChannelId, true);
            var mappingExist = await ProductMappingService.GetByProductId(InternalContext.RetailerId, req.Product.ChannelId, req.Product.KvProductId, req.Product.CommonItemId, isStringItemId: ConvertHelper.CheckUseStringItemId(channel?.Type));

            if (mappingExist != null)
            {
                logMessage.Description = "KvProduct has connected this channel";
                logMessage.LogError(new KvValidateException("KvProduct has connected this channel"), false, true);
                return false;
            }

            var mapping = new ProductMapping
            {
                RetailerId = InternalContext.RetailerId,
                ChannelId = req.Product.ChannelId,
                ProductChannelSku = req.Product.ItemSku,
                ProductKvId = req.Product.KvProductId,
                ProductKvSku = req.Product.KvProductSku,
                ProductKvFullName = req.Product.KvProductFullName,
                ProductChannelType = req.Product.Type,
                ProductChannelName = req.Product.ItemName,
                CommonProductChannelId = req.Product.CommonItemId ?? req.Product.ItemId,
                CommonParentProductChannelId = req.Product.CommonParentItemId ?? req.Product.ParentItemId
            };

            var lockTimeOut = NumberHelper.GetValueOrDefault(AppSettings.Get<int>("LockAddMappingTimeOut"), 15);
            using (var cli = KvLockRedis.GetLockFactory())
            {
                using (cli.AcquireLock(string.Format(KvConstant.LockAddMappingKey, req.Product.ChannelId), TimeSpan.FromSeconds(lockTimeOut)))
                {
                    await ProductMappingService.AddOrUpdate(InternalContext.RetailerId, mapping);
                    CacheClient.Remove(string.Format(KvConstant.ProductMappingCacheKey, mapping.ChannelId, InternalContext.RetailerId, mapping.ProductKvId));
                    CacheClient.Remove(string.Format(KvConstant.ListProductMappingCacheKey, InternalContext.RetailerId, mapping.ChannelId, mapping.ProductKvId));
                }
            }

            logMessage.LogInfo();

            UpdateProductFlagRelate(new List<long> { req.Product.KvProductId }, true, logId);

            await ProductMongoService.UpdateProductConnect(req.Product.Id);
            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            if (channel == null || string.IsNullOrEmpty(connectStringName))
            {
                logMessage.LogError(new Exception("Channel is null"));
                return true;
            }

            #region Logs

            var logContent = AuditTrailHelper.GetMappingProductContent(
                new ProductMappingAuditTrail
                {
                    ChannelType = channel?.Type,
                    ChannelName = channel?.Name,
                    Items = new List<ProductMappingItemAuditTrail>
                    {
                        new ProductMappingItemAuditTrail
                        {
                            ProductKvSku = mapping.ProductKvSku, ParentProductChannelId = mapping.CommonParentProductChannelId, ProductChannelId = mapping.CommonProductChannelId, ProductChannelSku = mapping.ProductChannelSku
                        }
                    }
                }, false);

            var log = new AuditTrailLog
            {
                FunctionId = (int)FunctionType.MappingSalesChannel,
                Action = (int)AuditTrailAction.Create,
                CreatedDate = DateTime.Now,
                BranchId = InternalContext.BranchId,
                Content = logContent.ToString()
            };
            var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, InternalContext.RetailerId, connectStringName, InternalContext.BranchId, AppSettings.Get<int>("ExecutionContext"));
            var coreContext = context.ConvertTo<KvInternalContext>();
            coreContext.UserId = context.User?.Id ?? 0;
            await AuditTrailInternalClient.AddLogAsync(coreContext, log);
            #endregion

            if (channel.OmniChannelSchedules?.Any(x => x.Type == (byte)ScheduleType.SyncOnhand) == true)
            {
                await OnHandBusiness.SyncMultiOnHand(connectStringName, channel, new List<string> { mapping.CommonProductChannelId }, null, false, logId, null, true);
            }

            if (channel.OmniChannelSchedules?.Any(x => x.Type == (byte)ScheduleType.SyncPrice) == true)
            {
                await PriceBusiness.SyncMultiPrice(connectStringName, channel, new List<string> { mapping.CommonProductChannelId }, null, false, logId, true, 50, null, true, "Queue created from add and mapping product");
            }

            return true;
        }
        public async Task<List<MapProduct>> Any(GetProductBySearch req)
        {
            var productMap = await ProductMappingService.Get(InternalContext.RetailerId, req.ChannelId);
            var listItemIdMapped = productMap.Select(x => x.CommonProductChannelId);
            var listProductChannel = await ProductMongoService.GetBySearch(InternalContext.RetailerId, req.ChannelId, req.Term, req.Skip, req.Top);
            var listProductNonMapped = listProductChannel.Where(x => !listItemIdMapped.Contains(x.CommonItemId)).ToList();
            var result = listProductNonMapped.Select(x => new MapProduct
            {
                BranchId = x.BranchId,
                ChannelId = x.ChannelId,
                ChannelType = x.ChannelType,
                CommonItemId = x.CommonItemId,
                CommonParentItemId = x.CommonParentItemId,
                CreatedDate = x.CreatedDate,
                ErrorMessage = x.SyncErrors?.Select(p => p.Message)?.Join("; "),
                Id = x.Id,
                IsConnected = x.IsConnected,
                ItemId = x.CommonItemId,
                ItemName = x.ItemName,
                ItemSku = x.ItemSku,
                ModifiedDate = x.ModifiedDate,
                ParentItemId = x.CommonParentItemId,
                RetailerCode = x.RetailerCode,
                RetailerId = x.RetailerId,
                Status = x.Status,
                StrItemId = x.CommonItemId,
                StrParentItemId = x.CommonParentItemId,
                SyncErrors = x.SyncErrors,
                Type = x.Type,
                ItemImages = x.ItemImages,
                SuperId = x.SuperId,
            }).ToList();
            return result;
        }

        public async Task<object> Any(GetProductByChannel req)
        {
            return await ProductMongoService.GetByChannel(req.ChannelId, req.BranchId);
        }

        public async Task<object> Any(GetProductSyncError req)
        {
            var products = await ProductMongoService.GetProductSyncError(InternalContext.RetailerId, req.ChannelIds, req.ErrorType);
            var productMaps = products.ConvertAll(c => c.ConvertTo<MapProduct>());
            var skus = products.Select(x => x.ItemSku).ToList();
            Dictionary<string, ProductMapping> mappings;
            var prdMappings = await ProductMappingService.GetByChannels(InternalContext.RetailerId, req.ChannelIds, skus, req.ProductSearchTerm);
            try
            {
                mappings = prdMappings.ToDictionary(x => x.CommonProductChannelId, y => y);
            }
            catch (Exception ex)
            {
                prdMappings = prdMappings.GroupBy(x => x.CommonProductChannelId).Select(x => x.First()).ToList();
                mappings = prdMappings.ToDictionary(x => x.CommonProductChannelId, y => y);
                Logger.LogError(ex.Message, ex);
            }

            var productResponse = new List<MapProduct>();
            foreach (var p in productMaps)
            {
                if (mappings.ContainsKey(p.CommonItemId))
                {
                    p.KvProductId = mappings[p.CommonItemId]?.ProductKvId ?? 0;
                    p.KvProductSku = mappings[p.CommonItemId]?.ProductKvSku;
                    p.KvProductFullName = mappings[p.CommonItemId].ProductKvFullName;
                    p.ErrorMessage = p.SyncErrors?.Select(x => x.Message)?.Join("; ");
                    p.KvErrorMessage = p.SyncErrors?.Select(x => GeneralHelper.GetKvMessage(x.Message))?.Join("; ");
                    productResponse.Add(p);
                }
            }
            return productResponse;
        }

        public async Task<long> Any(GetQuantityOfProductSyncError req)
        {
            return await ProductMongoService.GetQuantityOfProductSyncError(InternalContext.RetailerId, req.ChannelIds);
        }

        public async Task<List<Product>> Any(GetProductByTrackingKey req)
        {
            return await ProductMongoService.GetProductByTrackKey(InternalContext.RetailerId, req.ChannelId, req.TrackKey);
        }

        public async Task<object> Any(GetProductDetail req)
        {
            var logId = Guid.NewGuid();
            var logMessage = new LogObjectMicrosoftExtension(Logger, logId)
            {
                Action = "GetProductDetail",
                RetailerId = InternalContext.RetailerId,
                OmniChannelId = req.ChannelId
            };
            var channel = await OmniChannelService.GetByIdAsync(req.ChannelId);
            var channelClient = new ChannelClient.Impls.ChannelClient();
            var client = channelClient.GetClientV2(channel.Type, AppSettings);
            var auth = await OmniChannelAuthRepository.GetByChannelIdAsync(req.ChannelId);
            var platform = await OmniChannelPlatformService.GetById(channel.PlatformId);
            var accessToken = "";
            if (auth != null && !string.IsNullOrEmpty(auth.AccessToken))
            {
                accessToken = CryptoHelper.RijndaelDecrypt(auth.AccessToken);
            }

            var channelAuth = new ChannelAuth
            {
                ShopId = auth?.ShopId ?? 0,
                AccessToken = accessToken
            };

            var productDetail = await client.GetProductDetail(
                channelAuth, channel.Id, logId, req.ProductId, req.ParentProductId,
                req.ProductSku, platform);
            logMessage.LogInfo();
            return productDetail;
        }
        public async Task<List<ProductChannel>> Any(GetChannelByKvProductIds req)
        {
            var retVal = new List<ProductChannel>();
            if (req.KvProductIds?.Any() != true) return retVal;
            List<Domain.Model.OmniChannel> channelTypes = null;
            var configMappingV2Feature = _settings?.Get<OptimizeApiProductChannelFeature>("OptimizeApiProductChannelFeature");

            if (configMappingV2Feature != null && configMappingV2Feature.IsValid(InternalContext.Group?.Id ?? 0, InternalContext.RetailerId))
            {
                channelTypes = _cacheClient.Get<List<Domain.Model.OmniChannel>>(string.Format(KvConstant.ChannelsByRetailerKey, InternalContext.RetailerId));
            }
            if (channelTypes == null)
            {
                channelTypes = await OmniChannelRepository.GetChannelWithNoLock(InternalContext.RetailerId);
            }

            if (channelTypes?.Any() != true) return retVal;

            var channelIds = channelTypes.Select(x => x.Id).Distinct().ToList();

            var kvProductIds = new List<long>();
            kvProductIds.AddRange(req.KvProductIds);
            var listProducts = await ProductService.GetProductByMasterIds(InternalContext.RetailerId, kvProductIds);
            kvProductIds.AddRange(listProducts.Select(x => x.Id));

            var productMaps = await ProductMappingService.Get(InternalContext.RetailerId, channelIds, kvProductIds);
            if (productMaps?.Any() != true) return retVal;

            foreach (var productId in req.KvProductIds)
            {
                var channelProduct = new ProductChannel();
                channelProduct.KvProductId = productId;
                var listChannelId = productMaps.Where(x => x.ProductKvId == productId).Select(x => x.ChannelId).Distinct().ToList();

                if (listChannelId.Any())
                {
                    channelProduct.ChannelTypes = channelTypes.Where(x => listChannelId.Contains(x.Id)).Select(x => EnumHelper.EnumTypeTo<ChannelType>(x.Type).ToString()).Distinct().ToList();
                }
                else
                {
                    var productIds = listProducts.Where(x => x.MasterProductId == productId).Select(x => x.Id).ToList();
                    var listChannelIds = productMaps.Where(x => productIds.Contains(x.ProductKvId)).Select(x => x.ChannelId).Distinct().ToList();
                    if (listChannelIds.Any())
                    {
                        channelProduct.ChannelTypes = channelTypes.Where(x => listChannelIds.Contains(x.Id)).Select(x => EnumHelper.EnumTypeTo<ChannelType>(x.Type).ToString()).Distinct().ToList();
                    }
                }
                retVal.Add(channelProduct);
            }

            return retVal;
        }
        public async Task<List<ChannelSyncError>> Any(GetQuantityOfChannelSyncError req)
        {
            var channelSyncErrorResponse = new List<ChannelSyncError>();
            if (req.ChannelIds == null) return channelSyncErrorResponse;

            var products = await ProductMongoService.GetProductSyncError(InternalContext.RetailerId, req.ChannelIds);
            var orderDataSource = await OrderMongoService.GetByChannelIds(new List<int> { InternalContext.RetailerId }, req.ChannelIds);
            var invoiceDataSource = await InvoiceMongoService.GetByChannelIds(InternalContext.RetailerId, req.ChannelIds);

            foreach (var channelId in req.ChannelIds)
            {
                ChannelSyncError channelSyncError = new ChannelSyncError();
                var productTotal = products.Count(x => x.ChannelId == channelId);
                var oderTotal = orderDataSource.Count(x => x.ChannelId == channelId);
                var invoiceTotal = invoiceDataSource.Count(x => x.ChannelId == channelId);

                channelSyncError.ChannelId = channelId;
                channelSyncError.Total = productTotal + oderTotal + invoiceTotal;
                channelSyncErrorResponse.Add(channelSyncError);
            }
            return channelSyncErrorResponse;
        }
    }
}

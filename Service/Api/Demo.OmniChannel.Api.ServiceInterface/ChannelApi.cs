﻿using AutoMapper;
using Demo.Audit.Model.Message;
using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.ChannelClient.Helper;
using Demo.OmniChannel.ChannelClient.Impls;
using Demo.OmniChannel.ChannelClient.Models;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.ShareKernel.Dto;
using Demo.OmniChannel.FileUpload;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Infrastructure.CrmClient;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.Resource;
using Demo.OmniChannel.ScheduleService.Interfaces;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Dtos;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using Demo.OmniChannel.Utilities;
using Demo.OmniChannelCore.Api.Sdk.Common;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Serilog;
using ServiceStack;
using ServiceStack.OrmLite;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelAuthCreateOrUpdate = Demo.OmniChannel.Api.ServiceModel.ChannelAuthCreateOrUpdate;
using ChannelCreateOrUpdate = Demo.OmniChannel.Api.ServiceModel.ChannelCreateOrUpdate;
using DeactivateChannel = Demo.OmniChannel.Api.ServiceModel.DeactivateChannel;
using DisableByType = Demo.OmniChannel.Api.ServiceModel.DisableByType;
using EnableChannel = Demo.OmniChannel.Api.ServiceModel.EnableChannel;
using EnableUpdateProduct = Demo.OmniChannel.Api.ServiceModel.EnableUpdateProduct;
using GetById = Demo.OmniChannel.Api.ServiceModel.GetById;
using GetByRetailer = Demo.OmniChannel.Api.ServiceModel.GetByRetailer;
using KvValidateException = Demo.OmniChannel.ShareKernel.Exceptions.KvValidateException;
using RemoveChannelAuth = Demo.OmniChannel.Api.ServiceModel.RemoveChannelAuth;
using ResetFormula = Demo.OmniChannel.Api.ServiceModel.ResetFormula;
using ResetPriceBook = Demo.OmniChannel.Api.ServiceModel.ResetPriceBook;
using SyncErrorChannel = Demo.OmniChannel.Api.ServiceModel.SyncErrorChannel;
using Demo.OmniChannel.Infrastructure.EventBus.Service;
using Demo.OmniChannel.Infrastructure.EventBus.Event;

namespace Demo.OmniChannel.Api.ServiceInterface
{
    public class ChannelApi : BaseApi
    {
        public IOmniChannelService OmniChannelService { get; set; }
        public IOmniChannelScheduleRepository OmniChannelScheduleRepository { get; set; }
        public IOmniChannelRepository OmniChannelRepository { get; set; }
        public IOmniChannelAuthRepository OmniChannelAuthRepository { get; set; }
        public IProductMongoService ProductMongoService { get; set; }
        public IOrderMongoService OrderMongoService { get; set; }
        public IInvoiceMongoService InvoiceMongoService { get; set; }
        public IProductMappingService ProductMappingService { get; set; }
        public IAuditTrailInternalClient AuditTrailInternalClient { get; set; }
        public IPriceBusiness PriceBusiness { get; set; }
        public IOnHandBusiness OnHandBusiness { get; set; }
        public IScheduleService ScheduleService { get; set; }
        public IRetryOrderMongoService RetryOrderMongoService { get; set; }
        public IRetryInvoiceMongoService RetryInvoiceMongoService { get; set; }
        public IMapper Mapper { get; set; }
        public IOmniChannelSettingService OmniChannelSettingService { get; set; }
        public IShopeeService ShopeeService { get; set; }
        public IOmniChannelPlatformService OmniChannelPlatformService { get; set; }
        public IOmniChannelWareHouseService OmniChannelWareHouseService { get; set; }
        public IChannelBusiness ChannelBusiness { get; set; }
        public IDemoFileUpload DemoFileUpload { get; set; }
        public IOmniChannelAuthService OmniChannelAuthService { get; set; }
        public IPosSettingInternalClient PosSettingInternalClient { get; set; }
        public ICrmClient CrmClient { get; set; }
        public IIntegrationEventService IntegrationEventService { get; set; }

        public ChannelApi(ILogger<ChannelApi> logger) : base(logger)
        {
        }


        public async Task<Domain.Model.OmniChannel> Post(ChannelCreateOrUpdate req)
        {
            var validateResult = await new CreateUpdateChannelValidator().ValidateAsync(req);

            if (!validateResult.IsValid)
            {
                throw new OmniException(validateResult.Errors.FirstOrDefault()?.ErrorMessage);
            }

            var logId = Guid.NewGuid();
            var logMessage = new LogObjectMicrosoftExtension(Logger, logId)
            {
                Action = "ChannelCreateOrUpdate",
                RetailerId = InternalContext.RetailerId,
                BranchId = req?.Channel?.BranchId,
                RequestObject = req
            };

            try
            {
                var exist = string.IsNullOrEmpty(req.Channel.IdentityKey) ? null : await OmniChannelService.GetByIdAsync(req.Channel.Id);    // Dòng không có IdentityKey chỉ để hiển thị "Không có kết nối" trên màn hình list
                var existChannel = exist ?? await OmniChannelService.GetByIdAsync(req.Channel.Id); //For log
                var existAuth = await OmniChannelAuthRepository.GetByIdAsync(req.AuthId);
                var existSchedules = await OmniChannelScheduleRepository.WhereAsync(x => x.OmniChannelId == req.Channel.Id);
                var existSettings =
                    await OmniChannelSettingService.GetChannelSettings(req.Channel.Id, req.Channel.RetailerId);

                var (result, channelIdDel) = await OmniChannelService.CreateOrUpdateAsync(req.Channel, req.IsSyncOrder, req.IsSyncOnHand, req.IsSyncPrice, existAuth);
                await DeleteChannelAndUpdateFlagProduct(channelIdDel, logId);
                CacheClient.Remove(string.Format(KvConstant.ChannelsByRetailerKey, InternalContext.RetailerId));
                if (result != null)
                {
                    RemoveCacheChannelIdentity(result.Type, result.IdentityKey);
                }
                if (req.Channel.Id > 0)
                {
                    CacheClient.Remove(string.Format(KvConstant.ChannelAuthKey, req.Channel.Id));
                    CacheClient.Remove(string.Format(KvConstant.ChannelByIdKey, req.Channel.Id));
                }

                await HandleSyncPaymentTransactionJob(req, result, existSchedules);

                if (req.IsSyncOrder)
                {
                    var orderJob = (await OmniChannelScheduleRepository.WhereAsync(x => x.OmniChannelId == result.Id && x.Type == (byte)ScheduleType.SyncOrder)).FirstOrDefault();
                    if (orderJob != null)
                    {
                        var scheduleDto = new ScheduleDto
                        {
                            ChannelId = result.Id,
                            ChannelType = result.Type,
                            BasePriceBookId = result.BasePriceBookId.GetValueOrDefault(),
                            ChannelBranch = result.BranchId,
                            Id = orderJob.Id,
                            IsRunning = false,
                            RetailerId = result.RetailerId,
                            NextRun = orderJob.NextRun,
                            Type = (byte)orderJob.Type
                        };
                        if (orderJob.LastSync.GetValueOrDefault() != default(DateTime))
                        {
                            scheduleDto.LastSync = orderJob.LastSync;
                        }
                        ScheduleService.AddOrUpdate(scheduleDto, false);
                    }
                }

                if (!req.IsSyncOrder &&
                    existSchedules != null &&
                    existSchedules.Any(x => x.Type == (byte)ScheduleType.SyncOrder))
                {
                    var orderJob = existSchedules.FirstOrDefault(x => x.Type == (byte)ScheduleType.SyncOrder);
                    if (orderJob != null)
                    {
                        ScheduleService.Delete(req.Channel.Type, (byte)ScheduleType.SyncOrder, orderJob.Id);
                        await OmniChannelScheduleRepository.ResetExecutePlans(result.Id, (byte)ScheduleType.SyncOrder);
                    }
                }

                var modifiedJob = (await OmniChannelScheduleRepository.WhereAsync(x => x.OmniChannelId == result.Id && x.Type == (byte)ScheduleType.SyncModifiedProduct)).FirstOrDefault();
                if (modifiedJob != null)
                {
                    var scheduleDto = new ScheduleDto
                    {
                        ChannelId = result.Id,
                        ChannelType = result.Type,
                        BasePriceBookId = result.BasePriceBookId.GetValueOrDefault(),
                        ChannelBranch = result.BranchId,
                        Id = modifiedJob.Id,
                        IsRunning = false,
                        RetailerId = result.RetailerId,
                        NextRun = modifiedJob.NextRun,
                        Type = (byte)modifiedJob.Type
                    };
                    if (modifiedJob.LastSync.GetValueOrDefault() != default(DateTime))
                    {
                        scheduleDto.LastSync = modifiedJob.LastSync;
                    }
                    ScheduleService.AddOrUpdate(scheduleDto, false);
                }

                var isNewChanel = req.Channel.Id == 0 || req.Channel.Id != result.Id;
                if (isNewChanel && result != null && result.Id > 0)
                {
                    ((IKvCacheClient)CacheClient).AddItemToSet(KvConstant.RetailersByChannelsKey, result.RetailerId.ToString());
                    IntegrationEventService.AddEventWithRoutingKeyAsync(new ActiveChannelEvent { RetailerId = InternalContext.RetailerId, ShopId = result.IdentityKey }, RoutingKey.ChannelActive);
                }
                var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);

                logMessage.OmniChannelId = result?.Id;
                logMessage.LogInfo();
                #region audit trail

                await HandleAuditTrailAsync(req, connectStringName, isNewChanel, existChannel, exist, result, existSettings,
                    existSchedules);

                #endregion

                await HandleApplyAllFormula(req, result);

                if (!result.IsActive)
                {
                    return result;
                }

                // Delete data sync error
                if (exist != null && channelIdDel == 0)  // Only case update
                {
                    // Order
                    if (!req.IsSyncOrder)
                    {
                        await OrderMongoService.RemoveByChannelId(result.RetailerId, req.Channel.Id);
                        await InvoiceMongoService.RemoveByChannelId(result.RetailerId, req.Channel.Id);
                        await RetryInvoiceMongoService.RemoveByChannelId(result.RetailerId, req.Channel.Id);
                        await RetryOrderMongoService.RemoveByChannelId(result.RetailerId, req.Channel.Id);
                    }

                    // Product
                    var productSyncErrorTypes = new List<string>();
                    if (!req.IsSyncPrice) productSyncErrorTypes.Add(SyncErrorType.Price);
                    if (!req.IsSyncOnHand) productSyncErrorTypes.Add(SyncErrorType.OnHand);
                    if (productSyncErrorTypes.Any()) await ProductMongoService.RemoveAllSyncError(req.Channel.Id, productSyncErrorTypes);
                }

                var isChangeBranch = req.Channel.Id == result.Id && exist != null && req.Channel.BranchId != exist.BranchId;

                if (existAuth != null
                    && !string.IsNullOrEmpty(req.Channel.Name)
                    && (isNewChanel || isChangeBranch))
                {
                    var message = new SyncProductMessage
                    {
                        ChannelId = result.Id,
                        ChannelType = result.Type,
                        RetailerId = result.RetailerId,
                        BranchId = result.BranchId,
                        RetailerCode = InternalContext.RetailerCode,
                        KvEntities = InternalContext.Group?.ConnectionString,
                        IsIgnoreAuditTrail = req.IsIgnoreAuditTrail,
                        SendTime = DateTime.Now,
                        LogId = logId
                    };
                    if (isChangeBranch) message.IsUpdateChannel = true;
                    var cacheKey = string.Format(KvConstant.IsRunningGetProductKey, result.Id);
                    CacheClient.Set(cacheKey, true, TimeSpan.FromMinutes(2));

                    using (var mq = MessageFactory.CreateMessageProducer())
                    {
                        mq.Publish(message);
                    }
                }
                else if (exist != null && result.IsAutoMappingProduct && (!exist.IsAutoMappingProduct || (!exist.IsActive && result.IsActive)))
                {
                    var configMappingV2Feature = AppSettings.Get<MappingV2Feature>("MappingV2Feature");
                    var mappingRequest = new MappingRequest
                    {
                        ChannelId = result.Id,
                        ChannelType = result.Type,
                        RetailerId = result.RetailerId,
                        BranchId = result.BranchId,
                        RetailerCode = InternalContext.RetailerCode,
                        KvEntities = InternalContext.Group?.ConnectionString,
                        IsIgnoreAuditTrail = req.IsIgnoreAuditTrail,
                        LogId = logId,
                        MessageFrom = "CreateOrUpdateChannel-OmniChannelApi"
                    };
                    var kafkaMessage = new KafkaMessage(configMappingV2Feature.TopicMappingRequestName,
                        $"{mappingRequest.RetailerId}_{mappingRequest.BranchId}_{mappingRequest.ChannelId}",
                        mappingRequest);
                    KafkaClient.KafkaClient.Instance.PublishMessage(kafkaMessage.TopicName, kafkaMessage.Key, JsonConvert.SerializeObject(kafkaMessage), isV2: true);
                }

                if (exist == null) { return result; }

                var isSyncOnHandOldSetting = exist.OmniChannelSchedules.Any(x => x.Type == (byte)ScheduleType.SyncOnhand);
                if (req.IsSyncOnHand &&
                    (!isSyncOnHandOldSetting ||
                        req.Channel.SyncOnHandFormula != exist.SyncOnHandFormula ||
                        (!exist.IsActive && result.IsActive) ||
                        (req.Channel.OmniChannelSettings?.DistributeMultiMapping != null &&
                        req.Channel.OmniChannelSettings.DistributeMultiMapping != existSettings.DistributeMultiMapping)
                    )
                )
                {
                    await SyncMultiOnHand(result, logId, null, req.IsIgnoreAuditTrail);
                }

                var isSyncPriceOldSetting = exist.OmniChannelSchedules.Any(x => x.Type == (byte)ScheduleType.SyncPrice);
                if (req.IsSyncPrice && (isSyncPriceOldSetting == false || req.Channel.BasePriceBookId != exist.BasePriceBookId || req.Channel.PriceBookId != exist.PriceBookId || (!exist.IsActive && result.IsActive)))
                {
                    await SyncMultiPrice(result, logId, null, req.IsIgnoreAuditTrail);
                }
                return result;
            }
            catch (Exception e)
            {
                logMessage.LogError(e);
                throw;
            }
            finally
            {
                CacheClient.Remove(string.Format(KvConstant.ChannelsByRetailerKey, InternalContext?.RetailerId));

                if (req?.Channel?.Id > 0)
                {
                    CacheClient.Remove(string.Format(KvConstant.ChannelAuthKey, req.Channel.Id));
                    CacheClient.Remove(string.Format(KvConstant.ChannelByIdKey, req.Channel.Id));
                }

            }
        }

        public async Task<bool> Post(EnableChannel req)
        {
            if (req.ChannelId <= 0 || req.Auth == null)
            {
                throw new KvValidateException("Item not found");
            }

            var logId = Guid.NewGuid();
            var logObj = new LogObjectMicrosoftExtension(Logger, logId)
            {
                Action = "EnableChannel",
                OmniChannelId = req.ChannelId,
                RetailerId = InternalContext.RetailerId,
                BranchId = InternalContext.BranchId,
                RequestObject = req
            };

            var existChannel = await OmniChannelRepository.GetByIdAsync(req.ChannelId, isLoadReference: true);
            if (existChannel == null)
            {
                return false;
            }
            // Sendo: Get request auth info
            if (existChannel.Type == (byte)ChannelType.Sendo)
            {
                if (req.Auth.Id <= 0) throw new KvValidateException("AuthId invalid.");

                var auth = await OmniChannelAuthRepository.GetByIdAsync(req.Auth.Id);
                if (auth == null) throw new KvException("Auth info is empty.");

                req.Auth.AccessToken = CryptoHelper.RijndaelDecrypt(auth.AccessToken);
                req.Auth.RefreshToken = CryptoHelper.RijndaelDecrypt(auth.RefreshToken);
                req.Auth.ExpiresIn = auth.ExpiresIn;

                // Set latest shop key
                existChannel.ExtraKey = SendoHelper.GetShopKeyFromRefreshToken(req.Auth.RefreshToken);
            }

            existChannel.IsActive = true;
            existChannel.Name = !string.IsNullOrEmpty(req.ShopName) ? req.ShopName : existChannel.Name;
            existChannel.Email = !string.IsNullOrEmpty(req.Email) ? req.Email : existChannel.Email;
            existChannel.ModifiedDate = DateTime.Now;
            existChannel.RegisterDate = DateTime.Now;
            existChannel.PlatformId = req.PlatformId;
            await OmniChannelRepository.UpdateOnlyAsync(existChannel,
                new List<string>()
                {
                    nameof(Domain.Model.OmniChannel.Name),
                    nameof(Domain.Model.OmniChannel.IsActive),
                    nameof(Domain.Model.OmniChannel.Email),
                    nameof(Domain.Model.OmniChannel.ModifiedDate),
                    nameof(Domain.Model.OmniChannel.ExtraKey),
                    nameof(Domain.Model.OmniChannel.RegisterDate),
                    nameof(Domain.Model.OmniChannel.PlatformId),
                },
                p => p.Id == req.ChannelId);

            var isUpdateAuth = true;
            var existAuth = existChannel.OmniChannelAuth;
            if (existAuth == null)
            {
                existAuth = new OmniChannelAuth();
                isUpdateAuth = false;
            }

            existAuth.ShopId = req.Auth.ShopId;
            existAuth.OmniChannelId = existChannel.Id;
            if (!string.IsNullOrEmpty(req.Auth.AccessToken))
            {
                existAuth.AccessToken = CryptoHelper.RijndaelEncrypt(req.Auth.AccessToken);
            }

            if (!string.IsNullOrEmpty(req.Auth.RefreshToken))
            {
                existAuth.RefreshToken = CryptoHelper.RijndaelEncrypt(req.Auth.RefreshToken);
            }

            existAuth.ExpiresIn = req.Auth.ExpiresIn;
            existAuth.RefreshExpiresIn = req.Auth.RefreshExpiresIn;
            existAuth.ModifiedDate = DateTime.Now.AddMinutes(-1);
            if (req.Auth.ExpiresIn > 0)
            {
                existAuth.ExpiryDate = DateTime.Now.AddSeconds(req.Auth.ExpiresIn);
            }
            if (isUpdateAuth)
            {
                await OmniChannelAuthRepository.UpdateOnlyAsync(existAuth,
                    new List<string>
                    {
                        nameof(OmniChannelAuth.ShopId),
                        nameof(OmniChannelAuth.AccessToken),
                        nameof(OmniChannelAuth.RefreshToken),
                        nameof(OmniChannelAuth.ExpiresIn),
                        nameof(OmniChannelAuth.RefreshExpiresIn),
                        nameof(OmniChannelAuth.ModifiedDate),
                        nameof(OmniChannelAuth.ExpiryDate)
                    },
                    x => x.Id == existAuth.Id);
            }
            else
            {
                await OmniChannelAuthRepository.AddAsync(existAuth);
            }

            CacheClient.Remove(string.Format(KvConstant.ChannelAuthKey, req.ChannelId));
            CacheClient.Remove(string.Format(KvConstant.ChannelsByRetailerKey, InternalContext.RetailerId));
            CacheClient.Remove(string.Format(KvConstant.ChannelByIdKey, req.ChannelId));

            if (existChannel.OmniChannelSchedules.Any(x => x.Type == (byte)ScheduleType.SyncPrice))
            {
                await SyncMultiPrice(existChannel, logId);
            }

            if (existChannel.OmniChannelSchedules.Any(x => x.Type == (byte)ScheduleType.SyncOnhand))
            {
                await SyncMultiOnHand(existChannel, logId);
            }

            if (existChannel.IsAutoMappingProduct)
            {
                var configMappingV2Feature = AppSettings.Get<MappingV2Feature>("MappingV2Feature");
                if (configMappingV2Feature.IsValid(InternalContext.Group?.Id ?? 0, InternalContext.RetailerId))
                {
                    var mappingRequest = new MappingRequest
                    {
                        ChannelId = existChannel.Id,
                        ChannelType = existChannel.Type,
                        RetailerId = existChannel.RetailerId,
                        BranchId = existChannel.BranchId,
                        RetailerCode = InternalContext.RetailerCode,
                        KvEntities = InternalContext.Group?.ConnectionString,
                        IsIgnoreAuditTrail = false,
                        LogId = logId,
                        MessageFrom = "EnableChannel-OmniApi"
                    };
                    var kafkaMessage = new KafkaMessage(configMappingV2Feature.TopicMappingRequestName,
                        $"{mappingRequest.RetailerId}_{mappingRequest.BranchId}_{mappingRequest.ChannelId}",
                        mappingRequest);
                    KafkaClient.KafkaClient.Instance.PublishMessage(kafkaMessage.TopicName, kafkaMessage.Key, JsonConvert.SerializeObject(kafkaMessage), isV2: true);
                }
                else
                {
                    var mappingProductMessage = new MappingProductMessage
                    {
                        ChannelId = existChannel.Id,
                        ChannelType = existChannel.Type,
                        RetailerId = existChannel.RetailerId,
                        BranchId = existChannel.BranchId,
                        RetailerCode = InternalContext.RetailerCode,
                        KvEntities = InternalContext.Group?.ConnectionString,
                        IsIgnoreAuditTrail = false,
                        LogId = logId
                    };
                    using (var mq = MessageFactory.CreateMessageProducer())
                    {
                        mq.Publish(mappingProductMessage);
                    }
                }
            }

            var modifiedJob = (await OmniChannelScheduleRepository.WhereAsync(x =>
                x.OmniChannelId == existChannel.Id && x.Type == (byte)ScheduleType.SyncModifiedProduct)).FirstOrDefault();
            if (modifiedJob != null)
            {
                var scheduleDto = new ScheduleDto
                {
                    ChannelId = existChannel.Id,
                    ChannelType = existChannel.Type,
                    BasePriceBookId = existChannel.BasePriceBookId.GetValueOrDefault(),
                    ChannelBranch = existChannel.BranchId,
                    Id = modifiedJob.Id,
                    IsRunning = modifiedJob.IsRunning,
                    RetailerId = existChannel.RetailerId,
                    NextRun = modifiedJob.NextRun,
                    Type = (byte)modifiedJob.Type
                };
                ScheduleService.AddOrUpdate(scheduleDto);
                var orderJob = (await OmniChannelScheduleRepository.WhereAsync(x =>
                    x.OmniChannelId == existChannel.Id && x.Type == (byte)ScheduleType.SyncOrder)).FirstOrDefault();
                if (orderJob != null)
                {
                    var orderScheduleDto = new ScheduleDto
                    {
                        ChannelId = existChannel.Id,
                        ChannelType = existChannel.Type,
                        BasePriceBookId = existChannel.BasePriceBookId.GetValueOrDefault(),
                        ChannelBranch = existChannel.BranchId,
                        Id = orderJob.Id,
                        IsRunning = orderJob.IsRunning,
                        RetailerId = existChannel.RetailerId,
                        NextRun = orderJob.NextRun,
                        Type = (byte)orderJob.Type
                    };
                    ScheduleService.AddOrUpdate(orderScheduleDto);
                }
            }

            ((IKvCacheClient)CacheClient).AddItemToSet(KvConstant.RetailersByChannelsKey, existChannel.RetailerId.ToString());


            IntegrationEventService.AddEventWithRoutingKeyAsync(new ActiveChannelEvent { RetailerId = InternalContext.RetailerId, ShopId = existChannel.IdentityKey },
              RoutingKey.ChannelActive);

            logObj.Description = "Enable channel successful";
            logObj.LogInfo();

            #region Auditlog

            try
            {
                var channelNames = existChannel.Name;
                if (!string.IsNullOrEmpty(existChannel.Email))
                {
                    channelNames += $" - {existChannel.Email}";
                }
                var logContent = new StringBuilder($"Thiết lập kết nối {Enum.GetName(typeof(ChannelType), existChannel.Type)}: {channelNames}<br/>");
                logContent.AppendFormat("Chi nhánh đồng bộ: {0} <br/>", req.BranchName ?? "");
                var shedules = await OmniChannelScheduleRepository.WhereAsync(p => p.OmniChannelId == existChannel.Id);
                if (shedules.Any(x => x.Type == (byte)ScheduleType.SyncOnhand))
                {
                    logContent.AppendFormat("Đồng bộ số lượng bán: {0} <br/>", $"Có, số lượng bán = {EnumHelper.ToDescription((SyncOnHandFormula)existChannel?.SyncOnHandFormula)}");
                }

                if (shedules.Any(x => x.Type == (byte)ScheduleType.SyncPrice))
                {
                    var syncPriceLogContent = $"Có, Bảng giá bán = {req.PriceBookName}";
                    if (!string.IsNullOrEmpty(req.SalePriceBookName))
                    {
                        syncPriceLogContent += $", Bảng giá khuyến mại = {req.SalePriceBookName}";
                    }
                    logContent.AppendFormat($"Đồng bộ giá bán: {syncPriceLogContent} <br/>");
                }

                if (shedules.Any(x => x.Type == (byte)ScheduleType.SyncOrder))
                {
                    logContent.AppendFormat("Đồng bộ đơn hàng: Có <br/>");
                }
                var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
                var log = new AuditTrailLog
                {
                    FunctionId = (int)FunctionType.PosParameter,
                    Action = (int)AuditTrailAction.Update,
                    Content = logContent.ToString(),
                    CreatedDate = DateTime.Now,
                    BranchId = InternalContext.BranchId
                };
                var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, InternalContext.RetailerId, connectStringName, InternalContext.BranchId, AppSettings.Get<int>("ExecutionContext"));
                var coreContext = context.ConvertTo<KvInternalContext>();
                coreContext.UserId = context.User?.Id ?? 0;
                await AuditTrailInternalClient.AddLogAsync(coreContext, log);
            }
            catch
            {
                // ignored
            }
            #endregion
            return true;
        }

        public async Task<List<OmniChannelResponse>> Any(GetByRetailer req)
        {
            var isIncludeInactive = !req.OnlyIsActive;
            var shopeeTokenExpired = NumberHelper.GetValueOrDefault(AppSettings.Get<int>("ShopeeTokenExpired"), 1);
            var shopeeWarningExpiredBeforeMinutes = NumberHelper.GetValueOrDefault(AppSettings.Get<int>("ShopeeWarningExpiredBeforeMinutes"), 7200);

            var channels = await GetChannelsByRetailer(req);
            var result = channels.Select(p => p.ConvertTo<OmniChannelResponse>()).ToList();

            foreach (var item in result)
            {
                item.TotalProductConnected =
                    await ProductMongoService.CountProductByChannelId(item.RetailerId, item.Id, item.BranchId, true);
                if (req.Type == (byte)ChannelType.Shopee && item.OmniChannelAuth != null)
                {
                    //Bản v2 sẽ dùng ngày đăng ký để kiểm tra 
                    if (item.RegisterDate != null)
                    {
                        var expireDateV2 = item.RegisterDate.Value.AddYears(shopeeTokenExpired);
                        if (expireDateV2 <= DateTime.Now.AddMinutes(shopeeWarningExpiredBeforeMinutes).Date)
                        {
                            item.IsExpireWarning = true;
                        }
                    }
                    else if (string.IsNullOrEmpty(item.OmniChannelAuth.AccessToken))
                    {
                        var expireDate = (item.OmniChannelAuth.ExpiryDate ?? item.OmniChannelAuth.CreatedDate.AddYears(shopeeTokenExpired)).Date;

                        if (expireDate <= DateTime.Now.AddMinutes(shopeeWarningExpiredBeforeMinutes).Date)
                        {
                            item.IsExpireWarning = true;
                        }
                    }
                }
            }
            return result;
        }

        public async Task<GetExistChannelExpiredResponse> Any(GetExistChannelExpired req)
        {
            req.RetailerId = CurrentRetailerId;
            var channels = await Any(req.ConvertTo<GetByRetailer>());
            var expiredChannels = channels.Where(channel => !channel.IsActive).ToList();
            return new GetExistChannelExpiredResponse { HasChannelExpired = expiredChannels?.Count() > 0, Total = expiredChannels?.Count() ?? 0, Data = expiredChannels };
        }

        public async Task<Domain.Model.OmniChannel> Delete(GetById req)
        {
            var logId = Guid.NewGuid();
            var logMessage = new LogObjectMicrosoftExtension(Logger, logId)
            {
                Action = "DeleteMappingGetById",
                RetailerId = InternalContext.RetailerId,
                BranchId = InternalContext.BranchId
            };

            if (req.Id <= 0)
            {
                throw new KvValidateException("Dữ liệu không tồn tại");
            }

            await RemoveScheduleCacheByChannelId(req.Id);

            var result = await DeleteChannelAndUpdateFlagProduct(req.Id, logId);

            CacheClient.Remove(string.Format(KvConstant.ChannelAuthKey, req.Id));
            CacheClient.Remove(string.Format(KvConstant.ChannelsByRetailerKey, InternalContext.RetailerId));
            CacheClient.Remove(string.Format(KvConstant.ChannelByIdKey, req.Id));

            if (result != null)
            {
                await RemoveRetailerFromSet(result.RetailerId);
                RemoveCacheChannelIdentity(result.Type, result.IdentityKey);

                var log = new AuditTrailLog
                {
                    FunctionId = (int)FunctionType.PosParameter,
                    Action = (int)AuditTrailAction.Delete,
                    CreatedDate = DateTime.Now,
                    BranchId = InternalContext.BranchId
                };

                var content = new StringBuilder($"Xóa kết nối {Enum.GetName(typeof(ChannelType), result.Type)}: ");
                content.AppendFormat(result.Type == (byte)ChannelType.Lazada
                    ? $"Email: {result.Email}"
                    : $"Tên Shop: {result.Name}");

                log.Content = content.ToString();

                var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
                var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, InternalContext.RetailerId, connectStringName, InternalContext.BranchId, AppSettings.Get<int>("ExecutionContext"));
                var coreContext = context.ConvertTo<KvInternalContext>();
                coreContext.UserId = context.User?.Id ?? 0;
                await AuditTrailInternalClient.AddLogAsync(coreContext, log);
                IntegrationEventService.AddEventWithRoutingKeyAsync(new ActiveChannelEvent { RetailerId = InternalContext.RetailerId, ShopId = result.IdentityKey }, RoutingKey.ChannelDeActive);
            }
            logMessage.LogInfo();
            return result;
        }

        public async Task<bool> Post(DeactivateChannel req)
        {
            var logObj = new LogObjectMicrosoftExtension(Logger, Guid.NewGuid())
            {
                Action = "DeactivateChannel",
                RetailerId = InternalContext.RetailerId,
                BranchId = InternalContext.BranchId,
                RequestObject = req
            };

            await RemoveScheduleCacheByChannelId(req.ChannelId);

            var channel = await OmniChannelService.DeactivateChannel(req.ChannelId);
            if (channel != null)
            {
                RemoveCacheChannelIdentity(channel.Type, channel.IdentityKey);
                CacheClient.Remove(string.Format(KvConstant.ChannelByIdKey, channel.Id));
            }
            CacheClient.Remove(string.Format(KvConstant.ChannelsByRetailerKey, InternalContext.RetailerId));
            await RemoveRetailerFromSet(channel?.RetailerId ?? 0);
            CacheClient.Remove(string.Format(KvConstant.ChannelAuthKey, req.ChannelId));
            CacheClient.Remove(string.Format(KvConstant.LockMappingChannelId, req.ChannelId));

            var log = new AuditTrailLog
            {
                FunctionId = (int)FunctionType.PosParameter,
                Action = (int)AuditTrailAction.Update,
                CreatedDate = DateTime.Now,
                BranchId = InternalContext.BranchId,
                Content = string.IsNullOrEmpty(req.AuditContent) ? $"Kết nối {channel?.Name}: Tắt do kết nối tới {Enum.GetName(typeof(ChannelType), channel?.Type)} hết hạn" : req.AuditContent
            };
            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, InternalContext.RetailerId, connectStringName, InternalContext.BranchId, AppSettings.Get<int>("ExecutionContext"));
            var coreContext = context.ConvertTo<KvInternalContext>();
            coreContext.UserId = context.User?.Id ?? 0;
            await AuditTrailInternalClient.AddLogAsync(coreContext, log);

            logObj.ResponseObject = channel;
            logObj.LogInfo();
            return true;
        }
        public async Task Post(ResetFormula req)
        {
            var logId = Guid.NewGuid();
            var logMessage = new LogObjectMicrosoftExtension(Logger, logId)
            {
                Action = "ResetFormula",
                RetailerId = InternalContext.RetailerId,
                RequestObject = req
            };

            var channelUpdatedIds = await OmniChannelService.ResetSyncOnHandFormulaBySetting(req.RetailerId, req.SettingKey, req.OtherSettingValue);

            if (channelUpdatedIds.Any())
            {
                var channels = await OmniChannelService.GetByChannelIds(channelUpdatedIds, isIncludeInactive: false, isLoadReference: true);

                foreach (var channel in channels)
                {
                    CacheClient.Remove(string.Format(KvConstant.ChannelByIdKey, channel.Id));

                    if (channel.OmniChannelSchedules.Any(x => x.Type == (byte)ScheduleType.SyncOnhand))
                    {
                        await SyncMultiOnHand(channel, logId);
                    }
                }
            }
            logMessage.LogInfo();
        }

        public async Task<long> Post(ChannelAuthCreateOrUpdate req)
        {
            var logMessage = new LogObjectMicrosoftExtension(Logger, Guid.NewGuid())
            {
                Action = "ChannelAuthCreateOrUpdate",
                RetailerId = InternalContext.RetailerId,
                BranchId = InternalContext.BranchId,
                RequestObject = req
            };

            if (req.ChannelAuthRequest == null)
            {
                logMessage.Description = "ChannelAuthRequest is null";
                logMessage.LogError(new OmniChannelCore.Api.Sdk.Common.KvValidateException("ChannelAuthRequest is null"));
                throw new KvValidateException("Something went wrong");
            }

            var auth = new OmniChannelAuth
            {
                ShopId = req.ChannelAuthRequest.ShopId,
                ExpiresIn = req.ChannelAuthRequest.ExpiresIn,
                RefreshExpiresIn = req.ChannelAuthRequest.RefreshExpiresIn,
                OmniChannelId = req.ChannelAuthRequest.OmniChannelId
            };

            if (!string.IsNullOrEmpty(req.ChannelAuthRequest.AccessToken))
            {
                auth.AccessToken = CryptoHelper.RijndaelEncrypt(req.ChannelAuthRequest.AccessToken);
            }

            if (!string.IsNullOrEmpty(req.ChannelAuthRequest.RefreshToken))
            {
                auth.RefreshToken = CryptoHelper.RijndaelEncrypt(req.ChannelAuthRequest.RefreshToken);
            }

            if (req.ChannelAuthRequest.ExpiresIn > 0)
            {
                auth.ExpiryDate = DateTime.Now.AddSeconds(req.ChannelAuthRequest.ExpiresIn - 60);
            }
            else
            {
                var shopeeTokenExpired = AppSettings.Get<int>("ShopeeTokenExpired");
                if (shopeeTokenExpired > 0)
                {
                    auth.ExpiryDate = DateTime.Now.AddYears(shopeeTokenExpired);
                }
            }

            CacheClient.Remove(string.Format(KvConstant.ChannelAuthKey, req.ChannelAuthRequest.OmniChannelId));

            if (req.ChannelAuthRequest.Id <= 0)
            {
                logMessage.Description = "Add new auth successful";
                logMessage.LogInfo();
                return await OmniChannelAuthRepository.AddAsync(auth);
            }
            var existAuth = await OmniChannelAuthRepository.GetByIdAsync(req.ChannelAuthRequest.Id);
            existAuth.AccessToken = auth.AccessToken;
            existAuth.RefreshToken = auth.RefreshToken;
            existAuth.ExpiresIn = auth.ExpiresIn;
            existAuth.RefreshExpiresIn = auth.RefreshExpiresIn;
            existAuth.ShopId = auth.ShopId;
            existAuth.ExpiryDate = auth.ExpiryDate;
            await OmniChannelAuthRepository.UpdateAsync(existAuth);
            logMessage.Description = "update auth successful";
            logMessage.LogInfo();
            return existAuth.Id;
        }

        public async Task<SendoCreateAuthResponse> Post(CreateAuthSendo req)
        {
            var sendoClient = new SendoClient(AppSettings);
            var loginRes = await sendoClient.Login(req.ShopKey, req.SecretKey);
            var accessToken = loginRes?.Token;

            if (loginRes == null) throw new KvValidateException(KvMessage.sendo_validateConnectFail);

            if (string.IsNullOrEmpty(accessToken)) throw new KvValidateException(KvMessage.sendo_canNotGetAccessToken);

            var handler = new JwtSecurityTokenHandler();
            var token = handler.ReadJwtToken(accessToken);

            if (!token.Payload.ContainsKey("StoreId")) throw new KvValidateException(KvMessage.sendo_canNotGetStoreId);

            var storeId = token.Payload["StoreId"].ToString();

            await OmniChannelService.ValidateDuplicateChannelSendo(InternalContext.RetailerId, storeId, req.ShopKey, req.ChannelName, req.ChannelId);

            var authId = await Post(new ChannelAuthCreateOrUpdate
            {
                ChannelAuthRequest = new OmniChannelAuth
                {
                    AccessToken = accessToken,
                    RefreshToken = SendoHelper.JoinRefreshToken(req.ShopKey, req.SecretKey),
                    ExpiresIn = (int)(loginRes.Expires - DateTime.Now).TotalSeconds
                }
            });

            return new SendoCreateAuthResponse
            {
                AuthId = authId,
                IdentityKey = storeId
            };
        }

        public async Task<bool> Post(RemoveChannelAuth req)
        {
            await OmniChannelAuthRepository.DeleteAsync(x => x.Id == req.AuthId, false);
            return true;
        }

        public async Task<int> Post(ResetPriceBook req)
        {
            var logId = Guid.NewGuid();
            var logMessage = new LogObjectMicrosoftExtension(Logger, logId)
            {
                Action = "ResetPriceBook",
                RetailerId = InternalContext.RetailerId
            };

            var retVal = await OmniChannelService.ResetPriceBookSetting(InternalContext.RetailerId, req.PriceBookId);

            foreach (var channel in retVal)
            {
                if (channel.OmniChannelSchedules.Any(x => x.Type == (byte)ScheduleType.SyncPrice))
                {
                    await SyncMultiPrice(channel, logId);
                }
            }
            logMessage.LogInfo();
            return 1;
        }

        public async Task<bool> Post(SyncErrorChannel req)
        {
            var logId = Guid.NewGuid();
            var logMessage = new LogObjectMicrosoftExtension(Logger, logId)
            {
                Action = "SyncErrorChannel",
                RetailerId = InternalContext.RetailerId
            };


            if (req.ChannelId.HasValue)
            {
                var channel = await OmniChannelService.GetByIdAsync(req.ChannelId.Value);
                if (channel == null || !channel.IsActive)
                {
                    return false;
                }
                var errorProducts = await ProductMongoService.GetProductSyncError(InternalContext.RetailerId, new List<long> { req.ChannelId.Value });
                var productIds = errorProducts.Select(x => x.CommonItemId).ToList();
                var channelSchedules = await OmniChannelScheduleRepository.WhereAsync(x => x.OmniChannelId == req.ChannelId);
                await SyncMultiPriceOnHand(logId, channelSchedules, channel, productIds);
            }
            else if (req.ItemIds != null && req.ItemIds.Count > 0)
            {
                var channels = await OmniChannelService.GetByRetailerAsync(InternalContext.RetailerId, isIncludeInactive: false);
                var channelIds = channels.Select(x => x.Id).ToList();
                var channelSchedules = await OmniChannelScheduleRepository.WhereAsync(x => channelIds.Contains(x.OmniChannelId));
                var errorProducts = await ProductMongoService.GetProductSyncError(InternalContext.RetailerId, channelIds, null, req.ItemIds);
                channels = channels.Where(x => errorProducts.Select(y => y.ChannelId).Contains(x.Id)).ToList();

                foreach (var channel in channels)
                {
                    var productIds = errorProducts.Where(x => x.ChannelId == channel.Id).Select(x => x.CommonItemId).ToList();
                    await SyncMultiPriceOnHand(logId, channelSchedules, channel, productIds);
                }
            }
            else
            {
                var channels = await OmniChannelService.GetByRetailerAsync(InternalContext.RetailerId, isIncludeInactive: false);
                var channelIds = channels.Select(x => x.Id).ToList();
                var channelSchedules = await OmniChannelScheduleRepository.WhereAsync(x => channelIds.Contains(x.OmniChannelId));
                var errorProducts = await ProductMongoService.GetProductSyncError(InternalContext.RetailerId, channelIds);

                foreach (var channel in channels)
                {
                    var productIds = errorProducts.Where(x => x.ChannelId == channel.Id).Select(x => x.CommonItemId).ToList();
                    await SyncMultiPriceOnHand(logId, channelSchedules, channel, productIds);
                }
            }
            logMessage.LogInfo();
            return true;
        }

        private async Task SyncMultiPriceOnHand(Guid logId, List<OmniChannelSchedule> channelSchedules, Domain.Model.OmniChannel channel, List<string> productIds)
        {
            if (productIds == null || productIds.Count() == 0) return;

            var tasks = new List<Task>();

            var isSyncPrice = channelSchedules.Any(x => x.OmniChannelId == channel.Id && x.Type == (int)ScheduleType.SyncPrice);
            if (isSyncPrice)
            {
                tasks.Add(SyncMultiPrice(channel, logId, productIds));
            }
            var isSyncOnHand = channelSchedules.Any(x => x.OmniChannelId == channel.Id && x.Type == (int)ScheduleType.SyncOnhand);
            if (isSyncOnHand)
            {
                tasks.Add(SyncMultiOnHand(channel, logId, productIds));
            }

            await Task.WhenAll(tasks);
        }

        public async Task<bool> Post(DisableByType req)
        {
            await OmniChannelService.DisableChannelByType(req.RetailerId, req.ChannelType);
            await RemoveRetailerFromSet(req.RetailerId);
            return true;
        }

        public async Task<bool> Post(EnableUpdateProduct req)
        {
            var channel = await OmniChannelService.GetByIdAsync(req.ChannelId);
            if (channel == null || channel.OmniChannelAuth == null || string.IsNullOrEmpty(channel.OmniChannelAuth.AccessToken)) return false;
            var channelClient = new ChannelClient.Impls.ChannelClient();
            var client = channelClient.GetClient(channel.Type, AppSettings);
            var accessToken = CryptoHelper.RijndaelDecrypt(channel.OmniChannelAuth.AccessToken);
            await client.EnableUpdateProduct(accessToken);
            return true;
        }

        /// <summary>
        /// Xóa dữ liệu Omni (Khi xóa dữ liệu gian hàng của retail. Hiện tại tạm thời xóa mapping)
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<object> Post(ClearData req)
        {
            var channels = await OmniChannelRepository.GetVisibleChannels(req.RetailerId);
            if (!channels.Any()) return new { Message = "Channel is null", Success = false };

            await ProductMappingService.RemoveByChannelIds(channels.Select(x => x.Id).ToList());

            var productChannel = await ProductMongoService.GetProductConnectByChannelIds(InternalContext.RetailerId, channels.Select(x => x.Id).ToList());

            await ProductMongoService.UpdateProductsConnect(productChannel.Select(x => x.Id).ToList(), false);

            await ProductMongoService.RemoveAllSyncErrorByChannelIds(channels.Select(x => x.Id).ToList(), new List<string>() { "Price", "OnHand" });

            return new { Success = true };
        }
        public async Task<object> Post(GetSaleChannelOmniByType input)
        {
            List<SaleChannelDataSourceDto> resultData = new List<SaleChannelDataSourceDto>();
            var omniChannelIdList = await OmniChannelService.GetByRetailerAsync(CurrentRetailerId, input.BranchId, types: input.ChannelTypeLst, isIncludeInactive: true, isIncludeDeleted: false);
            if (omniChannelIdList.Count == 0)
            {
                return resultData;
            }
            var listOmniChannelIds = omniChannelIdList.Select(item => (long?)item.Id).ToList();
            var kvGroup = await GetRetailerGroup(GroupId);
            if (!string.IsNullOrEmpty(kvGroup.ConnectionString))
            {

                var connectStringName = kvGroup.ConnectionString.Substring(kvGroup.ConnectionString.IndexOf('=') + 1);
                using (var db = await DbConnectionFactory.OpenAsync(connectStringName.ToLower()))
                {
                    var saleChannelRepository = new SaleChannelRepository(db);
                    var saleChannels = await saleChannelRepository.GetSaleChannelListByOmmiChannelIds(CurrentRetailerId, listOmniChannelIds);
                    resultData = saleChannels.Select(item => new SaleChannelDataSourceDto()
                    {
                        Id = item.Id,
                        IsActive = item.IsActive,
                        Name = item.Name,
                        Img = item.Img
                    }).OrderBy(item => item.Img).ThenBy(item => item.IsActive ? 0 : 1).ThenBy(item => item.Name).ToList();
                    return resultData;
                }
            }
            return resultData;
        }

        public async Task<object> Post(GetListChannelWareHouseInfo request)
        {
            var existAuth = await OmniChannelAuthRepository.GetByIdAsync(request.OmniChannelAuthId);
            if (existAuth == null)
            {
                throw new KvException("Auth info is empty.");
            }
            var accesstoken = CryptoHelper.RijndaelDecrypt(existAuth.AccessToken);
            if (string.IsNullOrEmpty(accesstoken))
            {
                throw new KvException("Auth is missing Info or is empty.");
            }
            var clientTiki = new TikiClientV21(AppSettings);
            var wareHouseLst = await clientTiki.GetTikiWarehouse(accesstoken);
            if (wareHouseLst == null || wareHouseLst.Data == null || wareHouseLst.Data.Count == 0)
            {
                return new List<ListChannelWareHouseInfoResponse>();
            }
            return wareHouseLst.Data.Select(item => new ListChannelWareHouseInfoResponse()
            {
                Id = item.WareHouseId,
                Name = item.Name,
                StreetInfo = item.StreetInfo
            }).OrderBy(item => item.Id);
        }

        public async Task<object> Post(HandleWareHouseTiki request)
        {
            bool result = false;
            // Lấy channel
            if (request.TikiWareHouseRequest == null)
            {
                var configTikiV21Feature = AppSettings.Get<UseTikiV21Feature>("UseTikiV21Feature");
                if (configTikiV21Feature != null && configTikiV21Feature.Enable && (configTikiV21Feature.IncludeRetail.Count == 0 ||
                                           configTikiV21Feature.IncludeRetail.Contains(InternalContext.RetailerId)))
                {
                    throw new KvValidateException("Liên kết với TIKI bắt buộc phải chọn kho đồng bộ. Vui lòng chọn kho đồng bộ và thử lại");
                }
                return false;
            }
            var channelInfo = await OmniChannelService.GetByIdAsync(request.OmniChannelId);
            if (channelInfo.RetailerId != InternalContext.RetailerId)
            {
                throw new KvValidateException("Dữ liệu yêu cầu không hợp lệ");
            }
            var wareHouseInfo = await OmniChannelWareHouseService.GetByOmniChannelId(request.OmniChannelId, InternalContext.RetailerId);
            if (wareHouseInfo == null)
            {
                wareHouseInfo = new OmniChannelWareHouse
                {
                    RetailerId = InternalContext.RetailerId,
                    OmniChannelId = request.OmniChannelId,
                    Code = request.TikiWareHouseRequest.Code,
                    Name = request.TikiWareHouseRequest.Name,
                    StreetInfo = request.TikiWareHouseRequest.StreetInfo,
                    CreateBy = InternalContext.RetailerId,
                    CreatedDate = DateTime.UtcNow,
                    IsDeleted = false,
                    WareHouseId = request.TikiWareHouseRequest.WareHouseId
                };
                result = await OmniChannelWareHouseService.InsertSync(wareHouseInfo);
            }
            else
            {
                wareHouseInfo.WareHouseId = request.TikiWareHouseRequest.WareHouseId;
                wareHouseInfo.StreetInfo = request.TikiWareHouseRequest.StreetInfo;
                wareHouseInfo.Code = request.TikiWareHouseRequest.Code;
                wareHouseInfo.Name = request.TikiWareHouseRequest.Name;
                wareHouseInfo.ModifiedBy = InternalContext.RetailerId;
                wareHouseInfo.ModifiedDate = DateTime.UtcNow;
                result = await OmniChannelWareHouseService.UpdateAsync(wareHouseInfo);
            }
            await ChannelBusiness.GetChannel(request.OmniChannelId, InternalContext.RetailerId, Guid.NewGuid());
            return result;
        }

        public async Task<object> Post(ProductDataPushChannelReq req)
        {
            var retailerId = CurrentRetailerId;
            var formData = Request.FormData;
            if (formData == null) return null;

            req.ProductDataPushChannel = formData["ProductPushChannel"].FromJson<ProductDataPushChannel>() ?? new ProductDataPushChannel();
            var variations = req.ProductDataPushChannel.Variations;
            if (variations != null && variations.Any())
            {
                if (variations.Count > 50) throw new KvValidateException("Shopee hỗ trợ tối đa 50 phân loại hàng");

                var checkNameNull = variations.Where(x => string.IsNullOrEmpty(x.Name)).Select(x => x.VariationSku).ToList();
                if (checkNameNull.Any()) throw new KvValidateException($"Phân loại hàng của mã hàng {checkNameNull.Join(",")} không được để trống");

                var checkName = variations.Where(x => x.Name.Count() > 20).Select(x => x.VariationSku).ToList();
                if (checkName.Any()) throw new KvValidateException($"Phân loại hàng của mã hàng {checkName.Join(",")} không được vượt quá 20 kí tự");

                var checkDup = variations.GroupBy(x => x.Name).Select(x => new { Key = x.Key, Value = x.Select(y => y.VariationSku) })
                    .Where(x => x.Value.Count() > 1).SelectMany(x => x.Value);
                if (checkDup.Any()) throw new KvValidateException($"Phân loại hàng của mã hàng {checkDup.Join(",")} trùng nhau");

                #region check price

                var priceMin = variations.Select(x => x.Price).Min();
                var checkPrice = variations.Any(x => priceMin * 5 < x.Price);
                if (checkPrice) throw new KvValidateException("Shopee hỗ trợ giá các phân loại hàng không cách biệt quá 5 lần");

                #endregion
            }
            try
            {
                var modelImageUploadV2 = GetModelImageFromFormData();
                var pushReq = new ShareKernel.Dtos.PushDataItemToChannelReq
                {
                    PushDataItemToChannel = req.ProductDataPushChannel,
                    ChannelTypes = new List<ChannelType> { ChannelType.Shopee }
                };

                List<ProductAttribute> productAttributes = null;
                #region cal variation

                if (pushReq.PushDataItemToChannel.Variations != null && pushReq.PushDataItemToChannel.Variations.Any())
                {
                    var connectionString = GetKvEntitiesConnectString();
                    using (var db = await DbConnectionFactory.OpenAsync(connectionString))
                    {
                        var productAttributeRepo = new ProductAttributeRepository(db);
                        var productIds = pushReq.PushDataItemToChannel.Variations.Select(x => x.Id).ToList();
                        productAttributes = await productAttributeRepo.GetByProductIds(productIds, retailerId);
                    }
                }

                #endregion

                return await ShopeeService.PushProductShopeeChannel(ChannelType.Shopee, pushReq, CurrentRetailerId, productAttributes, modelImageUploadV2);
            }
            catch (Exception e)
            {
                var logMessage = new LogObjectMicrosoftExtension(Logger, Guid.NewGuid())
                {
                    Action = "PushProductToChannel",
                    RetailerId = CurrentRetailerId,
                    Description = e.Message
                };
                logMessage.LogError(e);
                throw new KvException(e.Message);
            }
        }

        public async Task<bool> Post(ActiveTrialPosOnline req)
        {
            var logId = Guid.NewGuid();
            var logObj = new LogObjectMicrosoftExtension(Logger, logId)
            {
                Action = "ActiveTrialPosOnline",
                RetailerId = InternalContext.RetailerId,
                BranchId = InternalContext.BranchId,
            };

            try
            {
                var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
                var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, InternalContext.RetailerId, connectStringName,
                    InternalContext.BranchId, AppSettings.Get<int>("ExecutionContext"));
                var coreContext = context.ConvertTo<KvInternalContext>();
                coreContext.UserId = context.User?.Id ?? 0;
                var response = await PosSettingInternalClient.ActiveTrialPosOnline(coreContext);
                var resultCrm = await CrmClient.SendCrm(new Infrastructure.CrmClient.Dtos.CrmRequest
                {
                    RetailerId = InternalContext.RetailerId,
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(15),
                    ContactName = req.Name,
                    Phone = req.PhoneNumber,
                    IsAdvise = req.IsAdvise,
                });
                logObj.ResponseObject = new { Result = $"Register: {response}, Send Crm:{resultCrm.ToSafeJson()}" };
                logObj.LogInfo();
                return response;
            }
            catch (Exception ex)
            {
                logObj.LogError(ex);
                throw;
            }
        }
        public async Task<OmniChannelDto> Get(GetById req)
        {
            var oc = await OmniChannelService.GetByIdAsync(req.Id);

            // Set schedules from cache
            if (oc != null && oc.OmniChannelSchedules?.Any() == true)
            {
                var scheduleIds = oc.OmniChannelSchedules.Select(x => x.Id).ToList();
                var dataCaches = ScheduleService.GetByIdsCoverDeserializeError(scheduleIds);
                foreach (var schedule in oc.OmniChannelSchedules)
                {
                    var dataCache = dataCaches.FirstOrDefault(x => x.Id == schedule.Id);
                    if (dataCache == null) continue;

                    schedule.IsRunning = dataCache.IsRunning;
                    schedule.LastSync = dataCache.LastSync;
                    schedule.NextRun = dataCache.NextRun;
                    schedule.Type = dataCache.Type;
                }
            }

            var result = Mapper.Map<Domain.Model.OmniChannel, OmniChannelDto>(oc);
            return result;
        }

        public async Task<object> Get(GetChannelExpiry req)
        {
            var channels = await OmniChannelRepository.GetVisibleChannels(req.RetailerId);

            var result = new List<OmniChannelExpiryResponse>();
            var shopeeTokenExpired = NumberHelper.GetValueOrDefault(AppSettings.Get<int>("ShopeeTokenExpired"), 1);
            var shopeeWarningExpiredBeforeMinutes = NumberHelper.GetValueOrDefault(AppSettings.Get<int>("ShopeeWarningExpiredBeforeMinutes"), 7200);

            foreach (var channel in channels)
            {
                if (channel.IsActive == false && channel.ModifiedDate.HasValue &&
                    channel.ModifiedDate.Value.Date == DateTime.Now.Date)
                {
                    result.Add(new OmniChannelExpiryResponse
                    {
                        ChannelType = channel.Type,
                        ChannelName = channel.Name,
                        ExpiryDate = DateTime.Now,
                        IsActive = channel.IsActive
                    });
                }
                else if (channel.Type == (byte)ChannelType.Shopee && channel.OmniChannelAuth != null)
                {
                    //Bản v2 sẽ dùng ngày đăng ký để kiểm tra 
                    if (channel.RegisterDate != null)
                    {
                        var expireDate = channel.RegisterDate.Value.AddYears(shopeeTokenExpired);
                        if (expireDate <= DateTime.Now.AddMinutes(shopeeWarningExpiredBeforeMinutes).Date)
                        {
                            result.Add(new OmniChannelExpiryResponse
                            {
                                ChannelType = channel.Type,
                                ChannelName = channel.Name,
                                ExpiryDate = expireDate,
                                IsActive = channel.IsActive
                            });
                        }
                    }
                    else if (string.IsNullOrEmpty(channel.OmniChannelAuth.AccessToken))
                    {
                        var expireDate = (channel.OmniChannelAuth.ExpiryDate ??
                                          channel.OmniChannelAuth.CreatedDate.AddYears(shopeeTokenExpired)).Date;
                        if (expireDate <= DateTime.Now.AddMinutes(shopeeWarningExpiredBeforeMinutes).Date)
                        {
                            result.Add(new OmniChannelExpiryResponse
                            {
                                ChannelType = channel.Type,
                                ChannelName = channel.Name,
                                ExpiryDate = expireDate,
                                IsActive = channel.IsActive
                            });
                        }
                    }
                }
            }
            //Nếu gian hàng đã quá hết hạn kết nối 15 sẽ không lấy
            var channelExpiryFilter = result.Where(x => x.ExpiryDate.Subtract(DateTime.Now).Days >= -15).ToList();

            return channelExpiryFilter;
        }

        public async Task<List<OmniChannelDto>> Get(ShopListReq req)
        {
            var channels = await OmniChannelRepository.GetByRetailerAsync(req.RetailerId, (byte)req.Type);
            return Mapper.Map<List<OmniChannelDto>>(channels);
        }

        public async Task<CategoriesResp> Get(GetCategoriesReq req)
        {
            var channel = await OmniChannelService.GetByIdAsync(req.ChannelId);
            var authData = await OmniChannelAuthService.GetChannelAuth(channel, Guid.NewGuid());
            var channelClient = new ChannelClient.Impls.ChannelClient();
            var client = channelClient.GetClientV2(channel.Type, AppSettings);
            var platform = await OmniChannelPlatformService.GetById(channel.PlatformId);
            var auth = new ChannelAuth
            {
                ShopId = Convert.ToInt64(channel.IdentityKey),
                AccessToken = authData.AccessToken,
            };

            return await client.GetCategories(channel.RetailerId, channel.Id, auth, Guid.NewGuid(), platform);

        }

        public async Task<object> Get(GetCountChannels req)
        {
            var channels = await OmniChannelRepository.GetAllActiveChannels(new List<int> { CurrentRetailerId }, null);
            var countChannelShopee = channels.Count(x => x.Type == (byte)ChannelType.Shopee);
            var countChannelLazada = channels.Count(x => x.Type == (byte)ChannelType.Lazada);
            var countChannelTiki = channels.Count(x => x.Type == (byte)ChannelType.Tiki);
            var countChannelSendo = channels.Count(x => x.Type == (byte)ChannelType.Sendo);
            var countChannelTiktok = channels.Count(x => x.Type == (byte)ChannelType.Tiktok);
            return new
            {
                NumberChannelsShopee = countChannelShopee,
                NumberChannelTiki = countChannelTiki,
                NumberChannelLazada = countChannelLazada,
                NumberChannelSendo = countChannelSendo,
                NumberChannelTiktok = countChannelTiktok
            };
        }

        public object Get(GetFeeLabel req)
        {
            if (req.ChannelType == (byte)ChannelType.Lazada)
            {
                return SelfAppConfigSingleton.Instance.GetFeeLzdDefConfig();
            }
            return new FeeLzdDefConfig();
        }


        #region private
        private void RemoveCacheChannelIdentity(byte channelType, string identityKey)
        {
            if (string.IsNullOrEmpty(identityKey))
            {
                return;
            }
            using (var cli = HostContext.TryResolve<IRedisClientsManager>().GetClient())
            {
                var cacheClient = cli.As<Domain.Model.OmniChannel>();
                var identityCacheSetKey = string.Format(KvConstant.IdentityCacheSetKey, channelType, identityKey);
                var channelIds = cli.GetAllItemsFromSet(identityCacheSetKey);
                foreach (var channelId in channelIds)
                {
                    cacheClient.DeleteById(channelId);
                    cli.RemoveItemFromSet(identityCacheSetKey, channelId);
                }
                CacheClient.Remove(identityCacheSetKey);
            }
        }

        private async Task RemoveRetailerFromSet(long retailerId)
        {
            var existChannelRetailer = (await OmniChannelService.GetByRetailerAsync((int)retailerId)).Any(x => x.IsActive);
            if (!existChannelRetailer)
            {
                ((IKvCacheClient)CacheClient).RemoveItemFromSet(KvConstant.RetailersByChannelsKey, retailerId.ToString());
            }
        }

        private async Task RemoveScheduleCacheByChannelId(long channelId)
        {
            var runningSchedules = await OmniChannelScheduleRepository.WhereAsync(p =>
                p.OmniChannelId == channelId && (p.Type == (byte)ScheduleType.SyncModifiedProduct
                || p.Type == (byte)ScheduleType.SyncOrder
                || p.Type == (byte)ScheduleType.SyncPaymentTransaction));

            if (runningSchedules != null && runningSchedules.Any())
            {
                ScheduleService.DeleteBatch(runningSchedules);
            }
        }

        private async Task HandleSyncPaymentTransactionJob(ChannelCreateOrUpdate req, Domain.Model.OmniChannel channel, List<OmniChannelSchedule> existSchedules)
        {
            if (req.IsSyncOrder)
            {
                //SyncPaymentTransactionJob CREATE
                var syncPaymentTransactionsJob = (await OmniChannelScheduleRepository.WhereAsync(t =>
                        t.OmniChannelId == channel.Id && t.Type == (byte)ScheduleType.SyncPaymentTransaction))
                    .FirstOrDefault();
                if (syncPaymentTransactionsJob != null)
                {
                    var scheduleDto = new ScheduleDto()
                    {
                        ChannelId = channel.Id,
                        ChannelType = channel.Type,
                        BasePriceBookId = channel.BasePriceBookId.GetValueOrDefault(),
                        ChannelBranch = channel.BranchId,
                        IsRunning = false,
                        Id = syncPaymentTransactionsJob.Id,
                        RetailerId = channel.RetailerId,
                        NextRun = syncPaymentTransactionsJob.NextRun,
                        Type = (byte)syncPaymentTransactionsJob.Type,
                    };
                    if (syncPaymentTransactionsJob.LastSync.GetValueOrDefault() != default(DateTime))
                    {
                        scheduleDto.LastSync = syncPaymentTransactionsJob.LastSync;
                    }
                    ScheduleService.AddOrUpdate(scheduleDto, true);
                }
            }
            //SyncPaymentTransactionJob DELETE
            if (!req.IsSyncOrder && existSchedules != null && existSchedules.Any(t => t.Type == (byte)ScheduleType.SyncPaymentTransaction))
            {
                var syncPaymentTransactionJob = existSchedules.FirstOrDefault(t => t.Type == (byte)ScheduleType.SyncPaymentTransaction);
                if (syncPaymentTransactionJob != null)
                {
                    ScheduleService.Delete(req.Channel.Type, (byte)ScheduleType.SyncPaymentTransaction, syncPaymentTransactionJob.Id);
                }
            }
        }

        private async Task<Domain.Model.OmniChannel> DeleteChannelAndUpdateFlagProduct(long channelIdDel, Guid logId)
        {
            if (channelIdDel <= 0) return null;
            var kvProductIdChannelDels = (await ProductMappingService.GetByChannelId(channelIdDel))?.Select(x => x.ProductKvId).Distinct().ToList();
            // Delete channel
            var result = await OmniChannelService.DeleteByIdAsync(channelIdDel);
            await RemoveRetailerFromSet(result?.RetailerId ?? 0);
            var deletedMappings = await ProductMappingService.RemoveByChannelId(channelIdDel);
            await ProductMongoService.RemoveByChannelId(result?.RetailerId ?? 0, channelIdDel);
            await OrderMongoService.RemoveByChannelId(result?.RetailerId ?? 0, channelIdDel);
            await InvoiceMongoService.RemoveByChannelId(result?.RetailerId ?? 0, channelIdDel);
            await RetryInvoiceMongoService.RemoveByChannelId(result?.RetailerId ?? 0, channelIdDel);
            await RetryOrderMongoService.RemoveByChannelId(result?.RetailerId ?? 0, channelIdDel);
            #region Logs delete mappings
            if (deletedMappings.Any())
            {
                var productMappingAuditTrail = new ProductMappingAuditTrail
                {
                    ChannelType = result?.Type,
                    ChannelName = result?.Name,
                    Items = new List<ProductMappingItemAuditTrail>()
                };
                foreach (var mapping in deletedMappings)
                {
                    productMappingAuditTrail.Items.Add(new ProductMappingItemAuditTrail { ProductKvSku = mapping.ProductKvSku, ParentProductChannelId = mapping.CommonParentProductChannelId, ProductChannelId = mapping.CommonProductChannelId, ProductChannelSku = mapping.ProductChannelSku });
                    CacheClient.Remove(string.Format(KvConstant.ListProductMappingCacheKey, mapping.RetailerId, mapping.ChannelId, mapping.ProductKvId));
                }

                var logContent = AuditTrailHelper.GetMappingProductContent(productMappingAuditTrail, true);

                var log = new AuditTrailLog
                {
                    FunctionId = (int)FunctionType.MappingSalesChannel,
                    Action = (int)AuditTrailAction.Reject,
                    CreatedDate = DateTime.Now,
                    BranchId = InternalContext.BranchId,
                    Content = logContent.ToString()
                };
                var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
                var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, InternalContext.RetailerId, connectStringName, InternalContext.BranchId, AppSettings.Get<int>("ExecutionContext"));
                var coreContext = context.ConvertTo<KvInternalContext>();
                coreContext.UserId = context.User?.Id ?? 0;
                await AuditTrailInternalClient.AddLogAsync(coreContext, log);
            }
            #endregion

            if (kvProductIdChannelDels?.Any() != true) return result;

            var channelTypes = await OmniChannelRepository.GetAsync<OmniChannelTypeDto>(x => x.RetailerId == InternalContext.RetailerId);
            var channelIds = channelTypes.Select(x => x.Id).Distinct().ToList();
            var kvProductIdStillRelates = (await ProductMappingService.Get(InternalContext.RetailerId,
                channelIds, kvProductIdChannelDels)).Select(x => x.ProductKvId).Distinct().ToList();
            var kvProductIdNeedDels = kvProductIdChannelDels.Where(x => !kvProductIdStillRelates.Contains(x)).ToList();

            if (kvProductIdNeedDels?.Any() != true) return result;

            try
            {
                UpdateProductFlagRelate(kvProductIdNeedDels, false, logId);
            }
            catch (Exception e)
            {
                Log.Error($"Delete Channel And Update Flag Product: {e?.Message}", e);
            }

            return result;
        }

        private async Task SyncMultiPrice(Domain.Model.OmniChannel channel, Guid logId, List<string> channelProductIds = null, bool isIgnoreAuditTrail = false)
        {
            var connectStringName =
                InternalContext.Group?.ConnectionString.Substring(
                    InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            if (channelProductIds != null && channelProductIds.Any())
            {
                var pageSize = AppSettings.Get<int>("multiSyncPageSize", 50);
                var total = channelProductIds.Count / pageSize;
                total = channelProductIds.Count % pageSize == 0 ? total : total + 1;
                for (int i = 0; i < total; i++)
                {
                    var currentPage = i * pageSize;
                    await PriceBusiness.SyncMultiPrice(connectStringName, channel, channelProductIds.Skip(currentPage).Take(pageSize).ToList(), null, isIgnoreAuditTrail, logId, false, 50, null, true, "Queue created from enable or SyncErrorChannel ");
                }
                return;
            }
            await PriceBusiness.SyncMultiPrice(connectStringName, channel, null, null, isIgnoreAuditTrail, logId);
        }

        private async Task SyncMultiOnHand(Domain.Model.OmniChannel channel, Guid logId, List<string> channelProductIds = null, bool isIgnoreAuditTrail = false)
        {
            var connectStringName =
                InternalContext.Group?.ConnectionString.Substring(
                    InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            if (channelProductIds != null && channelProductIds.Any())
            {
                var pageSize = AppSettings.Get<int>("multiSyncPageSize", 50);
                var total = channelProductIds.Count / pageSize;
                total = channelProductIds.Count % pageSize == 0 ? total : total + 1;
                for (int i = 0; i < total; i++)
                {
                    var currentPage = i * pageSize;
                    await OnHandBusiness.SyncMultiOnHand(connectStringName, channel,
                        channelProductIds.Skip(currentPage).Take(pageSize).ToList(), null, isIgnoreAuditTrail, logId, null);
                }
                return;
            }
            await OnHandBusiness.SyncMultiOnHand(connectStringName, channel,
                null, null, isIgnoreAuditTrail, logId, null, true);
        }

        private List<ShopeeModelImageUploadV2> GetModelImageFromFormData()
        {
            var results = new List<ShopeeModelImageUploadV2>();
            if (Request.Files != null)
            {
                foreach (var uploadedFile in Request.Files.Where(uploadedFile => uploadedFile.ContentLength > 0))
                {
                    using (var ms = new MemoryStream())
                    {
                        uploadedFile.WriteTo(ms);
                        results.Add(new ShopeeModelImageUploadV2()
                        {
                            FileName = uploadedFile.FileName,
                            Data = ms.ToArray(),
                        });
                    }
                }
            }

            return results;
        }

        private async Task HandleApplyAllFormula(ChannelCreateOrUpdate req, Domain.Model.OmniChannel channel)
        {
            if (req.Channel.IsApplyAllFormula)
            {
                var allChannelOfCurrentRetailer = await OmniChannelRepository.WhereAsync(x =>
                                x.RetailerId == channel.RetailerId && x.Type == channel.Type && x.Id != channel.Id);
                foreach (var c in allChannelOfCurrentRetailer)
                {
                    await SyncMultiOnHand(c, Guid.NewGuid(), null, req.IsIgnoreAuditTrail);
                    CacheClient.Remove(string.Format(KvConstant.ChannelByIdKey, c.Id));
                }
            }
        }

        private Task<List<Domain.Model.OmniChannel>> GetChannelsByRetailer(GetByRetailer req)
        {
            var isIncludeInactive = !req.OnlyIsActive;

            if (req.ChannelIds != null && req.ChannelIds.Any())
            {
                return OmniChannelService.GetByChannelIds(req.ChannelIds, req.RetailerId, isIncludeInactive, false);
            }

            if (req.Types != null && req.Types.Any())
            {
                return OmniChannelService.GetByRetailerAsync(req.RetailerId, req.BranchId, req.Types, isIncludeInactive, false);
            }

            return req.BranchId > 0 ?
                OmniChannelService.GetByRetailerAsync(req.RetailerId, req.BranchId, req.Type, isIncludeInactive) :
                OmniChannelService.GetByRetailerAsync(req.RetailerId, req.Type, isIncludeInactive);
        }
        #endregion

        #region HANDLE AUDITTRAIL
        private async Task HandleAuditTrailAsync(ChannelCreateOrUpdate req, string connectStringName, bool isNewChanel, Domain.Model.OmniChannel existChannel, Domain.Model.OmniChannel exist, Domain.Model.OmniChannel result, OmniChannelSettingObject existSettings, List<OmniChannelSchedule> existSchedules)
        {
            if (req.IsIgnoreAuditTrail)
            {
                return;
            }


            var channelNames = req.Channel.Name;
            if (!string.IsNullOrEmpty(req.Channel.Email))
            {
                channelNames += $" - {req.Channel.Email}";
            }
            var logContent = new StringBuilder($"Thiết lập kết nối {Enum.GetName(typeof(ChannelType), req.Channel.Type)}: {channelNames}<br/>");
            using (var db = DbConnectionFactory.Open(connectStringName.ToLower()))
            {
                //Chi nhánh đồng bộ
                var branchRepository = new BranchRepository(db);
                var newBranchName = (await branchRepository.GetByIdAsync(req.Channel.BranchId))?.Name;
                if (isNewChanel || existChannel == null || req.Channel.BranchId == existChannel?.BranchId)
                {
                    logContent.AppendFormat("Chi nhánh đồng bộ: {0} <br/>", newBranchName ?? "");
                }
                else
                {
                    var oldBranchName = (await branchRepository.GetByIdAsync(existChannel?.BranchId))?.Name;
                    logContent.AppendFormat("Chi nhánh đồng bộ: {0} -> {1} <br/>", oldBranchName ?? "", newBranchName ?? "");
                }

                // Đồng bộ số lượng bán
                var existSyncOnHand = existSchedules.Any(x => x.Type == (int)ScheduleType.SyncOnhand);
                var syncOnHandLogContent = string.Empty;
                syncOnHandLogContent = HandleSyncOnHandAuditTrail(req, existChannel, isNewChanel, existSyncOnHand);
                logContent.AppendFormat("Đồng bộ số lượng bán: {0} <br/>", syncOnHandLogContent);

                // Đồng giá bán
                var existSyncPrice = existSchedules.Any(x => x.Type == (int)ScheduleType.SyncPrice);
                var syncPriceLogContent = string.Empty;
                var priceBookRepository = new PriceBookRepository(db);
                syncPriceLogContent = await HandleSyncPriceAuditTrailAsync(req, existChannel, priceBookRepository, isNewChanel, existSyncPrice);
                logContent.AppendFormat("Đồng bộ giá bán: {0} <br/>", syncPriceLogContent);

                //Đồng bộ đơn hàng
                var existSyncOrder = existSchedules.Any(x => x.Type == (int)ScheduleType.SyncOrder);
                var syncOrtherChangeStatus = "";
                var syncOrderReturningStatus = string.Empty;
                var syncOrderAutoSyncBatchExpire = string.Empty;
                var syncOrderSaleTime = string.Empty;
                var syncOrderAutoCreatedProduct = string.Empty;
                HandleSyncOrderAuditTrailAsync(req, existChannel, existSettings, existSyncOrder,
                    isNewChanel, ref syncOrtherChangeStatus, ref syncOrderReturningStatus,
                    ref syncOrderAutoSyncBatchExpire, ref syncOrderSaleTime, ref syncOrderAutoCreatedProduct);

                logContent.AppendFormat(
                    $"Đồng bộ đơn hàng: {syncOrtherChangeStatus}" +
                    $"{(!string.IsNullOrEmpty(syncOrderReturningStatus) ? ", xác nhận đã chuyển hoàn: " + syncOrderReturningStatus : string.Empty)}" +
                    $"{(!string.IsNullOrEmpty(syncOrderAutoSyncBatchExpire) ? ", tự động chọn hàng lô date khi tạo hóa đơn: " + syncOrderAutoSyncBatchExpire : string.Empty)}" +
                    $"{(!string.IsNullOrEmpty(syncOrderAutoSyncBatchExpire) ? ", thời gian bán: " + syncOrderSaleTime : string.Empty)}" +
                    $"{(!string.IsNullOrEmpty(syncOrderAutoCreatedProduct) ? ", Khi đơn hàng có hàng hóa chưa liên kết: " + syncOrderAutoCreatedProduct : string.Empty)} <br/>");

                // Tự động xóa mapping
                var isAutoDeleteMappingOld = existChannel?.IsAutoDeleteMapping ?? false;
                var isAutoDeleteMappingNew = req.Channel.IsAutoDeleteMapping ?? false;
                if (existChannel != null && isAutoDeleteMappingOld != isAutoDeleteMappingNew)
                {
                    var isAutoDeleteMappingChange = isAutoDeleteMappingNew ? "Có -> Không" : "Không -> Có";
                    logContent.AppendFormat($"Xóa liên kết hàng hóa nếu Mã hàng - SKU thay đổi: {isAutoDeleteMappingChange}<br/>");
                }
                else
                {
                    var isAutoDeleteMappingText = isAutoDeleteMappingNew ? "Có" : "Không";
                    logContent.AppendFormat($"Xóa liên kết hàng hóa nếu Mã hàng - SKU thay đổi: {isAutoDeleteMappingText}<br/>");
                }

                // Đồng bộ mặt hàng khi trùng SKU
                var isAutoMappingProductOld = existChannel?.IsAutoMappingProduct ?? false;
                var isAutoMappingProductNew = req.Channel.IsAutoMappingProduct;
                if (existChannel != null && isAutoMappingProductOld != isAutoMappingProductNew)
                {
                    var isAutoProductMappingChange = isAutoMappingProductNew ? "Không -> Có" : "Có -> Không";
                    logContent.AppendFormat($"Tự động liên kết trùng SKU: {isAutoProductMappingChange}<br/>");
                }
                else
                {
                    var isAutoMappingProductText = isAutoMappingProductNew ? "Có" : "Không";
                    logContent.AppendFormat($"Tự động liên kết trùng SKU: {isAutoMappingProductText}<br/>");
                }
                if (req.IsSyncOnHand && req.Channel.OmniChannelSettings?.DistributeMultiMapping != null &&
                    req.Channel.OmniChannelSettings.DistributeMultiMapping != existSettings.DistributeMultiMapping)
                {
                    var isDistributeMultiMapping = existSettings.DistributeMultiMapping ? "Có -> Không" : "Không -> Có";
                    logContent.AppendFormat(
                        $"Phân bổ số lượng bán của hàng nhiều liên kết: {isDistributeMultiMapping}");

                }

                var isIsAutoCopyProductNew = req.Channel.OmniChannelSettings.IsAutoCopyProduct;
                if (req.Channel.OmniChannelSettings?.IsAutoCopyProduct != null && req.Channel.OmniChannelSettings.IsAutoCopyProduct != existSettings.IsAutoCopyProduct)
                {
                    var isIsAutoCopyProduct = existSettings.IsAutoCopyProduct ? "Có -> Không" : "Không -> Có";
                    logContent.AppendFormat(
                        $"Tự động sao chép hàng hóa khi có hàng hóa mới tạo trên sàn: {isIsAutoCopyProduct}");
                }
                else
                {
                    var isAutoCopyProductText = isIsAutoCopyProductNew ? "Có" : "Không";
                    logContent.AppendFormat($"Tự động sao chép hàng hóa khi có hàng hóa mới tạo trên sàn: {isAutoCopyProductText}<br/>");
                }
            }
            var log = new AuditTrailLog
            {
                FunctionId = (int)FunctionType.PosParameter,
                Action = isNewChanel ? (int)AuditTrailAction.Create : (int)AuditTrailAction.Update,
                Content = logContent.ToString(),
                CreatedDate = DateTime.Now,
                BranchId = InternalContext.BranchId
            };
            var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, InternalContext.RetailerId, connectStringName, InternalContext.BranchId, AppSettings.Get<int>("ExecutionContext"));
            var coreContext = context.ConvertTo<KvInternalContext>();
            coreContext.UserId = context.User?.Id ?? 0;
            await AuditTrailInternalClient.AddLogAsync(coreContext, log);
        }

        private string HandleSyncOnHandAuditTrail(ChannelCreateOrUpdate req, Domain.Model.OmniChannel existChannel, bool isNewChanel, bool existSyncOnHand)
        {
            var syncOnHandLogContent = String.Empty;
            if (!req.IsSyncOnHand)
            {
                syncOnHandLogContent = (existSyncOnHand ? "Có -> " : "") + "Không";
            }
            else
            {
                if (isNewChanel)
                {
                    syncOnHandLogContent = $"Có, số lượng bán = {EnumHelper.ToDescription((SyncOnHandFormula)req.Channel.SyncOnHandFormula)}";
                }
                else
                {
                    if (!existSyncOnHand)
                    {
                        syncOnHandLogContent = $"Không -> Có, số lượng bán = {EnumHelper.ToDescription((SyncOnHandFormula)req.Channel.SyncOnHandFormula)}";
                    }
                    else
                    {
                        syncOnHandLogContent += existChannel != null
                            ? $"Có, số lượng bán = {EnumHelper.ToDescription((SyncOnHandFormula)existChannel.SyncOnHandFormula)}"
                            : "";

                        if (req.Channel.SyncOnHandFormula != existChannel?.SyncOnHandFormula)
                        {
                            syncOnHandLogContent += $" -> {EnumHelper.ToDescription((SyncOnHandFormula)req.Channel.SyncOnHandFormula)}";
                        }
                    }
                }
            }
            return syncOnHandLogContent;
        }

        private async Task<string> HandleSyncPriceAuditTrailAsync(ChannelCreateOrUpdate req, Domain.Model.OmniChannel existChannel, PriceBookRepository priceBookRepository, bool isNewChanel, bool existSyncPrice)
        {
            var syncPriceLogContent = String.Empty;
            if (!req.IsSyncPrice)
            {
                syncPriceLogContent = (existSyncPrice ? "Có -> " : "") + "Không";
                return syncPriceLogContent;
            }

            //Case: IsSyncPrice Handle new channel
            var (newBasePriceBookName, newPriceBookName) = await priceBookRepository.GetChannelPriceBookName(req.Channel.ConvertTo<Domain.Model.OmniChannel>());
            if (isNewChanel)
            {
                syncPriceLogContent = $"Có, Bảng giá bán = {newBasePriceBookName}";
                if (req.Channel.PriceBookId.HasValue && !string.IsNullOrEmpty(newPriceBookName))
                {
                    syncPriceLogContent += $", Bảng giá khuyến mại = {newPriceBookName}";
                }

                return syncPriceLogContent;
            }

            //Case: IsSyncPrice with exist channel
            if (!existSyncPrice)
            {
                syncPriceLogContent = $"Không -> Có, Bảng giá bán = {newBasePriceBookName}";
                if (req.Channel.PriceBookId.HasValue && !string.IsNullOrEmpty(newPriceBookName))
                {
                    syncPriceLogContent += $", Bảng giá khuyến mại = {newPriceBookName}";
                }
            }
            else
            {
                var (oldBasePriceBookName, oldPriceBookName) = await priceBookRepository.GetChannelPriceBookName(existChannel);
                syncPriceLogContent = $"Có, Bảng giá bán = {oldBasePriceBookName}";
                if (req.Channel.BasePriceBookId != existChannel?.BasePriceBookId)
                {
                    syncPriceLogContent += $" -> {newBasePriceBookName}";
                }
                if (req.Channel.PriceBookId.HasValue && !string.IsNullOrEmpty(oldPriceBookName))
                {
                    syncPriceLogContent += $", Bảng giá khuyến mại = {oldPriceBookName}";
                    if (req.Channel.PriceBookId != existChannel?.PriceBookId)
                    {
                        syncPriceLogContent += $" -> {newPriceBookName}";
                    }
                }
            }
            return syncPriceLogContent;
        }

        private void HandleSyncOrderAuditTrailAsync(ChannelCreateOrUpdate req,
            Domain.Model.OmniChannel existChannel,
            OmniChannelSettingObject existSettings, bool existSyncOrder,
            bool isNewChanel, ref string syncOrtherChangeStatus,
            ref string syncOrderReturningStatus,
            ref string syncOrderAutoSyncBatchExpire,
            ref string syncOrderSaleTime,
            ref string syncOrderAutoCreatedProduct)
        {
            if (!isNewChanel && existSyncOrder != req.IsSyncOrder)
            {
                syncOrtherChangeStatus = req.IsSyncOrder ? "Không -> Có" : "Có -> Không";
            }
            else
            {
                syncOrtherChangeStatus = req.IsSyncOrder ? "Có" : "Không";
            }

            HandleSyncOrderWithFormula(req, existChannel, existSettings, ref syncOrtherChangeStatus);

            HandleSyncOrderWithOutFormula(req, isNewChanel, existSettings, existSyncOrder,
                ref syncOrderReturningStatus, ref syncOrderAutoSyncBatchExpire, ref syncOrderSaleTime,
                ref syncOrderAutoCreatedProduct);
        }

        private void HandleSyncOrderWithFormula(ChannelCreateOrUpdate req, Domain.Model.OmniChannel existChannel,
            OmniChannelSettingObject existSettings, ref string syncOrtherChangeStatus)
        {
            if (!req.IsSyncOrder || req.Channel.SyncOrderFormula == null) return;

            var syncOrderFormulaTypeReq = req.Channel.OmniChannelSettings?.SyncOrderFormulaType
                    ?? (byte)SyncOrderFormulaType.CreatedUpdate;

            var syncOrderFormulaReq =
                req.Channel.SyncOrderFormula ?? 30;

            if (existChannel != null && (existSettings.SyncOrderFormulaType != syncOrderFormulaTypeReq
                || existChannel.SyncOrderFormula != syncOrderFormulaReq))
            {
                syncOrtherChangeStatus += $", đồng bộ đơn hàng " +
                    $"{EnumHelper.ToDescription((SyncOrderFormulaType)existSettings.SyncOrderFormulaType)} " +
                    $"{EnumHelper.ToDescription((SyncOrderFormula)existChannel.SyncOrderFormula)}" +
                    $" -> " +
                    $"{EnumHelper.ToDescription((SyncOrderFormulaType)syncOrderFormulaTypeReq)} " +
                    $"{EnumHelper.ToDescription((SyncOrderFormula)syncOrderFormulaReq)}";
            }
            else
            {
                syncOrtherChangeStatus += $", đồng bộ đơn hàng " +
                   $"{EnumHelper.ToDescription((SyncOrderFormulaType)syncOrderFormulaTypeReq)} " +
                   $"{EnumHelper.ToDescription((SyncOrderFormula)syncOrderFormulaReq)}";
            }
        }

        private void HandleSyncOrderWithOutFormula(ChannelCreateOrUpdate req, bool isNewChanel,
            OmniChannelSettingObject existSettings,
            bool existSyncOrder,
            ref string syncOrderReturningStatus,
            ref string syncOrderAutoSyncBatchExpire,
            ref string syncOrderSaleTime,
            ref string syncOrderAutoCreatedProduct)
        {
            if (!req.IsSyncOrder) return;
            syncOrderAutoSyncBatchExpire = req.Channel.OmniChannelSettings?.IsAutoSyncBatchExpire ?? false ? "Có" : "Không";
            if (req.Channel.Type == (int)ChannelType.Shopee || req.Channel.Type == (int)ChannelType.Lazada
                || req.Channel.Type == (int)ChannelType.Tiktok)
                syncOrderReturningStatus =
                    req.Channel.OmniChannelSettings?.IsConfirmReturning ?? false ? "Có" : "Không";

            if (req.Channel.Type == (int)ChannelType.Shopee || req.Channel.Type == (int)ChannelType.Tiktok ||
                req.Channel.Type == (int)ChannelType.Lazada || req.Channel.Type == (int)ChannelType.Tiki)
                syncOrderSaleTime =
                    $"{EnumHelper.ToDescription((SyncOrderSaleTimeType)(req.Channel.OmniChannelSettings?.SyncOrderSaleTimeType ?? 0))} ";

            if (req.Channel.Type == (int)ChannelType.Shopee)
                syncOrderAutoCreatedProduct =
                    $"{EnumHelper.ToDescription((SyncOrderAutoCreateProduct)((req.Channel.OmniChannelSettings?.IsAutoCreatingProduct ?? false) ? 1 : 0))} ";

            if (isNewChanel || !existSyncOrder) return;
            if (existSettings.IsAutoSyncBatchExpire != req.Channel.OmniChannelSettings?.IsAutoSyncBatchExpire)
            {
                syncOrderAutoSyncBatchExpire = req.Channel.OmniChannelSettings?.IsAutoSyncBatchExpire ?? false
                    ? $"Không -> Có"
                    : $"Có -> Không";
            }

            if ((req.Channel.Type == (int)ChannelType.Shopee || req.Channel.Type == (int)ChannelType.Lazada ||
                req.Channel.Type == (int)ChannelType.Tiktok)
                && existSettings.IsConfirmReturning != req.Channel.OmniChannelSettings?.IsConfirmReturning)
            {
                syncOrderReturningStatus =
                    req.Channel.OmniChannelSettings?.IsConfirmReturning ?? false
                        ? $"Không -> Có"
                        : $"Có -> Không";
            }

            if (req.Channel.Type == (int)ChannelType.Shopee || req.Channel.Type == (int)ChannelType.Tiktok
                || req.Channel.Type == (int)ChannelType.Lazada || req.Channel.Type == (int)ChannelType.Tiki && 
                existSettings.SyncOrderSaleTimeType != req.Channel.OmniChannelSettings?.SyncOrderSaleTimeType)
            {
                syncOrderSaleTime =
                    $"{EnumHelper.ToDescription((SyncOrderSaleTimeType)existSettings.SyncOrderSaleTimeType)} " +
                    $" -> " +
                    $"{EnumHelper.ToDescription((SyncOrderSaleTimeType)(req.Channel.OmniChannelSettings?.SyncOrderSaleTimeType ?? 0))}";
            }

            if (req.Channel.Type == (int)ChannelType.Shopee &&
                existSettings.IsAutoCreatingProduct != req.Channel.OmniChannelSettings?.IsAutoCreatingProduct)
            {
                syncOrderAutoCreatedProduct =
                    $"{EnumHelper.ToDescription((SyncOrderAutoCreateProduct)(existSettings.IsAutoCreatingProduct ? 1 : 0))} " +
                    $" -> " +
                    $"{EnumHelper.ToDescription((SyncOrderAutoCreateProduct)((req.Channel.OmniChannelSettings?.IsAutoCreatingProduct ?? false) ? 1 : 0))}";
            }
        }

        #endregion
    }
}
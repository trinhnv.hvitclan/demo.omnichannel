﻿using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Demo.OmniChannelCore.Api.Sdk.Common;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Api.ServiceInterface
{
    public class MigrateSaleChannelApi : BaseApi
    {
        public static readonly int cacheExpiresDay = 7;
        public IOmniChannelRepository OmniChannelRepository { get; set; }
        public ISaleChannelInternalClient SaleChannelInternalClient { get; set; }
        public MigrateSaleChannelApi(ILogger<MigrateSaleChannelApi> logger) : base(logger)
        { }
        public async Task<string> Post(MirgrateDataSaleChannel request)
        {
            if (request.GroupId <= 0 && request.RetailerId <= 0)
            {
                throw new KvException("Params is not valid");
            }

            var logMessage = new LogObjectMicrosoftExtension(Logger, Guid.NewGuid())
            {
                Action = "MigrateDataSaleChannel",
                RetailerId = request.RetailerId,
                GroupId = request.GroupId
            };

            var retailerIds = new List<int>();
            Domain.Common.KvGroup kvGroup = null;
            if (request.RetailerId > 0)
            {
                var retailer = await GetRetailer(request.RetailerId);
                if (retailer == null)
                {
                    logMessage.LogWarning("Not found retailer");
                    throw new KvException("Not found retailer");

                }
                retailerIds.Add(request.RetailerId);
                kvGroup = await GetRetailerGroup(retailer.GroupId);
            }
            else if (request.GroupId > 0)
            {
                kvGroup = await GetRetailerGroup(request.GroupId);
                if (kvGroup != null)
                {
                    retailerIds = await GetActiveRetailerIdsByGroup(request.GroupId);
                }
            }

            if (kvGroup != null)
            {
                logMessage.GroupId = kvGroup.Id;
            }

            // Migrate chunk size
            int maxSize = 999;
            int skip = 0;
            int limit = request.LimitRetailer <= maxSize ? request.LimitRetailer : maxSize;
            bool isRunning = true;
            while (isRunning)
            {
                var migrateIds = retailerIds.Skip(skip).Take(limit).ToList();
                if (migrateIds.Any())
                {
                    var duplicateChannels = await OmniChannelRepository.GetDuplicateChannels(migrateIds);
                    await ProcessMigrateDataSaleChannel(duplicateChannels, kvGroup, logMessage);
                    skip += limit;
                }
                else
                {
                    isRunning = false;
                }
            }

            logMessage.LogInfo($"Migrate salechannel success with request: {request.ToSafeJson()}");
            return "OK";
        }

        public async Task<object> Post(CreateTempTableSaleChannel request)
        {
            if (request.GroupId <= 0 && request.RetailerId <= 0)
            {
                throw new KvException("GroupId or RetailerId is not input");
            }

            int retailerId = 0;
            Domain.Common.KvGroup kvGroup = null;
            if (request.RetailerId > 0)
            {
                var retailer = await GetRetailer(request.RetailerId);
                if (retailer == null)
                {
                    throw new KvException("Not found retailer");

                }
                kvGroup = await GetRetailerGroup(retailer.GroupId);
                retailerId = request.RetailerId;
                if (kvGroup == null)
                {
                    throw new KvException("Not found kvGroup");
                }
            }
            else if (request.GroupId > 0)
            {
                kvGroup = await GetRetailerGroup(request.GroupId);
                if (kvGroup == null)
                {
                    throw new KvException("Not found kvGroup");
                }
                var retailerIds = await GetActiveRetailerIdsByGroup(request.GroupId);
                retailerId = retailerIds.FirstOrDefault();
            }

            var connectStringName = kvGroup.ConnectionString.ToLower().Substring(kvGroup.ConnectionString.IndexOf('=') + 1);
            await CreateTableMigrateTemp(retailerId, connectStringName);
            var logMessage = new LogObjectMicrosoftExtension(Logger, Guid.NewGuid())
            {
                Action = "CreateTempDataSaleChannel",
                GroupId = request.GroupId
            };
            logMessage.LogInfo($"Create temp table success in GroupId = {request.GroupId}");
            return "OK";
        }

        public async Task<object> Post(DeleteTempTableSaleChannel request)
        {
            if (request.GroupId <= 0 && request.RetailerId <= 0)
            {
                throw new KvException("GroupId or RetailerId is not input");
            }

            int retailerId = 0;
            Domain.Common.KvGroup kvGroup = null;
            if (request.RetailerId > 0)
            {
                var retailer = await GetRetailer(request.RetailerId);
                if (retailer == null)
                {
                    throw new KvException("Not found retailer");

                }
                kvGroup = await GetRetailerGroup(retailer.GroupId);
                retailerId = request.RetailerId;

                if (kvGroup == null)
                {
                    throw new KvException("Not found kvGroup");
                }
            }
            else if (request.GroupId > 0)
            {
                kvGroup = await GetRetailerGroup(request.GroupId);
                if (kvGroup == null)
                {
                    throw new KvException("Not found kvGroup");
                }
                var retailerIds = await GetActiveRetailerIdsByGroup(request.GroupId);
                retailerId = retailerIds.FirstOrDefault();
            }

            var connectStringName = kvGroup.ConnectionString.ToLower().Substring(kvGroup.ConnectionString.IndexOf('=') + 1);
            await DeleteTableMigrateTemp(retailerId, connectStringName);
            var logMessage = new LogObjectMicrosoftExtension(Logger, Guid.NewGuid())
            {
                Action = "CreateTempDataSaleChannel",
                GroupId = request.GroupId
            };
            logMessage.LogInfo($"Create temp table success in GroupId = {request.GroupId}");

            return "OK";
        }

        /// <summary>
        /// Thực hiện migrate salechannel theo từng RetailerId
        /// </summary>
        /// <param name="duplicateChannels"></param>
        /// <param name="kvGroup"></param>
        /// <param name="logMessage"></param>
        /// <returns></returns>
        private async Task<bool> ProcessMigrateDataSaleChannel(List<Domain.Model.OmniChannel> duplicateChannels, Domain.Common.KvGroup kvGroup, LogObjectMicrosoftExtension logMessage)
        {
            var channelDictionaryWithRetailerId = duplicateChannels.GroupBy(channel => channel.RetailerId).ToDictionary(group => group.Key, group => group.ToList());
            var retailerHadMigrate = CacheClient.Get<List<int>>(string.Format(KvConstant.MigrateSaleChannelWithRetailerKey, kvGroup.Id));
            if (retailerHadMigrate == null)
            {
                retailerHadMigrate = new List<int>();
            }

            var connectStringName = kvGroup.ConnectionString.ToLower().Substring(kvGroup.ConnectionString.IndexOf('=') + 1);
            using (var db = await DbConnectionFactory.OpenAsync(connectStringName))
            {
                foreach (KeyValuePair<int, List<Domain.Model.OmniChannel>> retailerIdChannelPair in channelDictionaryWithRetailerId)
                {
                    var retailerId = retailerIdChannelPair.Key;
                    var channels = retailerIdChannelPair.Value;

                    // Bỏ qua nếu retailer đó đã migrate rồi
                    if (retailerHadMigrate.Contains(retailerId))
                    {
                        logMessage.LogInfo($"RetailerId {retailerId} had migrate");
                        continue;
                    }

                    try
                    {
                        await ProcessMigrateData(db, retailerId, channels, connectStringName, logMessage);
                        retailerHadMigrate.Add(retailerId);
                    }
                    catch (Exception ex)
                    {
                        logMessage.LogError(ex);
                    }

                }
            }
            CacheClient.Set(string.Format(KvConstant.MigrateSaleChannelWithRetailerKey, kvGroup.Id), retailerHadMigrate, DateTime.Now.AddDays(cacheExpiresDay));
            return true;
        }

        /// <summary>
        /// Migrate dữ liệu Invoice, Order, SaleChannel của nhiều kênh bán về 1 kênh bán
        /// </summary>
        /// <param name="db"></param>
        /// <param name="retailerId"></param>
        /// <param name="duplicateChannels"></param>
        /// <param name="connectStringName"></param>
        /// <returns></returns>
        private async Task<bool> ProcessMigrateData(IDbConnection db, int retailerId, List<Domain.Model.OmniChannel> duplicateChannels, string connectStringName, LogObjectMicrosoftExtension logMessage)
        {
            // Init repository and context
            var saleChannelRepository = new SaleChannelRepository(db);
            var coreContext = await GetCoreContext(retailerId, connectStringName);

            var omniChannelIds = duplicateChannels.Select(channel => (long?)channel.Id).ToList();
            var saleChannels = await saleChannelRepository.GetSaleChannelListByOmmiChannelIds(retailerId, omniChannelIds);

            var channelDictionaryWithIdentityKey = duplicateChannels.GroupBy(channel => channel.IdentityKey).ToDictionary(group => group.Key, group => group.ToList());
            foreach (KeyValuePair<string, List<Domain.Model.OmniChannel>> retailerIdChannelPair in channelDictionaryWithIdentityKey)
            {
                var omniChannelGroupByIdentityKey = retailerIdChannelPair.Value;
                var activeOmniChannelId = omniChannelGroupByIdentityKey.Where(channel => channel.IsDeleted == null || channel.IsDeleted == false).Select(channel => channel.Id).FirstOrDefault();
                var deactiveOmniChannels = omniChannelGroupByIdentityKey.Where(channel => channel.IsDeleted == true).Select(channel => channel.Id).ToList();

                // Không có channel nào xóa mềm thì không migrate
                if (!deactiveOmniChannels.Any())
                {
                    logMessage.LogInfo($"RetailerId {retailerId} not have deactive channel");
                    continue;
                }
                logMessage.LogInfo($"RetailerId {retailerId} have {omniChannelGroupByIdentityKey.Count} duplicate channels");

                int keySaleChannelId = 0;
                // Trường hợp 1: Có 1 channel đang active, Có N kênh bán deactive. Thực hiện migrate data các channel deactive theo channel đang active
                // Trường hợp 2: Có 0 channel đang active, Có N kênh bán deactive. Chọn lấy 1 kênh bán mới nhất deactive làm key, và migrate data các kênh bán còn lại theo key đó.
                //               Để khi khách hàng kết nối lại, cũng chỉ thấy 1 channel đang hiển thị thôi. 
                if (activeOmniChannelId > 0)
                {
                    keySaleChannelId = saleChannels.Where(x => x.OmniChannelId.GetValueOrDefault() == activeOmniChannelId).Select(x => x.Id).FirstOrDefault();
                }
                else
                {
                    var deactiveWithIdLatest = deactiveOmniChannels.Max();
                    keySaleChannelId = saleChannels.Where(x => x.OmniChannelId.GetValueOrDefault() == deactiveWithIdLatest).Select(x => x.Id).FirstOrDefault();
                }

                // Lấy danh sách Id các salechannel cần update deactive
                var migrateSaleChannelIds = saleChannels
                    .Where(x => x.Id != keySaleChannelId && deactiveOmniChannels.Contains(x.OmniChannelId.GetValueOrDefault(-1)))
                    .Select(x => (int?)x.Id).ToList();

                logMessage.LogInfo($"RetailerId {retailerId} has keySaleChannelId is {keySaleChannelId} and migrateSaleChannelIds are {string.Join(",", migrateSaleChannelIds)}");
                await SaleChannelInternalClient.MigrateDataSaleChannel(coreContext, migrateSaleChannelIds, keySaleChannelId);
            }

            return true;
        }

        private async Task CreateTableMigrateTemp(int retailerId, string connectStringName)
        {
            var coreContext = await GetCoreContext(retailerId, connectStringName);
            await SaleChannelInternalClient.CreateTableMigrateDataSaleChannel(coreContext);
        }

        private async Task DeleteTableMigrateTemp(int retailerId, string connectStringName)
        {
            var coreContext = await GetCoreContext(retailerId, connectStringName);
            await SaleChannelInternalClient.DeleteTableMigrateDataSaleChannel(coreContext);
        }

        private async Task<KvInternalContext> GetCoreContext(int retailerId, string connectStringName)
        {
            var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, retailerId, connectStringName, 0, AppSettings.Get<int>("ExecutionContext"));
            var coreContext = context.ConvertTo<KvInternalContext>();
            coreContext.UserId = context.User?.Id ?? 0;
            return coreContext;
        }
    }
}
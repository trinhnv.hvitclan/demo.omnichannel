﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Demo.Audit.Model.Message;
using Demo.OmniChannelCore.Api.Sdk.Interfaces;
using Demo.OmniChannel.Api.ServiceModel;
using Demo.OmniChannel.Infrastructure.Common;
using Demo.OmniChannel.MongoDb;
using Demo.OmniChannel.MongoService.Interface;
using Demo.OmniChannel.Repository.Impls;
using Demo.OmniChannel.Repository.Interfaces;
using Demo.OmniChannel.Resource;
using Demo.OmniChannel.Services.Interfaces;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.Utilities;
using ServiceStack;
using ServiceStack.OrmLite;
using KvInternalContext = Demo.OmniChannelCore.Api.Sdk.Common.KvInternalContext;
using Demo.OmniChannel.Business.Interfaces;
using Demo.OmniChannel.Redis;
using Demo.OmniChannel.ShareKernel.Exceptions;
using Microsoft.Extensions.Logging;
using Demo.OmniChannel.ChannelClient.Common;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.Api.ServiceInterface
{
    public class ProductMappingApi : BaseApi
    {
        public IProductMongoService ProductMongoService { get; set; }
        public IProductMappingService ProductMappingService { get; set; }
        public IOmniChannelService OmniChannelService { get; set; }
        public IOmniChannelRepository OmniChannelRepository { get; set; }
        public IAuditTrailInternalClient AuditTrailInternalClient { get; set; }
        public IPriceBusiness PriceBusiness { get; set; }
        public IOnHandBusiness OnHandBusiness { get; set; }
        public IKvLockRedis KvLockRedis { get; set; }
        private SelfAppConfig _config;
        public ProductMappingApi(ILogger<ProductMappingApi> logger, IAppSettings settings) : base(logger)
        {
            _config = new SelfAppConfig(settings);
        }

        public async Task<object> Any(GetProductMapping req)
        {
            var r = await ProductMappingService.Get(InternalContext.RetailerId, req.ChannelId, req.KvProductSkus, req.ChannelProductSkus);
            // KOL-5351 có thể phát sinh case ProductMapping bị sai ItemId
            var itemIds = r.Select(x => x.CommonProductChannelId).ToList();
            var products = await ProductMongoService.Get(InternalContext.RetailerId, null, itemIds);
            var pItemIds = products.Select(x => x.CommonItemId);
            r = r.Except(r.Where(x => !pItemIds.Contains(x.CommonProductChannelId))).ToList();

            Dictionary<string, Product> productDictionary =
                    new Dictionary<string, Product>();
            foreach (var product in products)
            {
                if (!productDictionary.ContainsKey(product.CommonItemId))
                {
                    productDictionary.Add(product.CommonItemId, product);
                }
            }
            return ConvertToMappingDtoWithSuperId(r, productDictionary);
        }

        public async Task<object> Any(GetProductMappingByChannelList req)
        {
            var channelIds = req.ChannelIds;

            if (channelIds?.Any() != true)
            {
                channelIds = await OmniChannelService.GetChannelId(InternalContext.RetailerId, req.BranchIds, req.ChannelTypes);
            }

            var result = await ProductMappingService.Get(channelIds);

            return ConvertToMappingDto(result);
        }

        public async Task<object> Post(AddProductMapping req)
        {
            var logId = Guid.NewGuid();
            var logMessage = new LogObjectMicrosoftExtension(Logger, logId)
            {
                Action = "AddProductMapping",
                RetailerId = InternalContext.RetailerId,
                OmniChannelId = req.ProductMapping.ChannelId
            };

            var channel = await OmniChannelRepository.GetByIdAsync(req.ProductMapping.ChannelId, true);
            var mapping = await ProductMappingService.GetByProductId(InternalContext.RetailerId, req.ProductMapping.ChannelId, req.ProductMapping.ProductKvId, req.ProductMapping.ProductChannelId, isStringItemId: ConvertHelper.CheckUseStringItemId(channel?.Type));

            if (mapping != null)
            {
                logMessage.Description = "KvProduct has connected this channel";
                logMessage.LogError(new KvValidateException("KvProduct has connected this channel"), false, true);
                return ConvertToMappingDto(mapping);
            }

            var newMapping = new ProductMapping
            {
                ChannelId = req.ProductMapping.ChannelId,
                CreatedDate = DateTime.Now,
                ModifiedDate = DateTime.Now,
                ProductChannelName = req.ProductMapping.ProductChannelName,
                ProductChannelSku = req.ProductMapping.ProductChannelSku,
                ProductChannelType = req.ProductMapping.ProductChannelType,
                ProductKvFullName = req.ProductMapping.ProductKvFullName,
                ProductKvId = req.ProductMapping.ProductKvId,
                ProductKvSku = req.ProductMapping.ProductKvSku,
                RetailerCode = req.ProductMapping.RetailerCode,
                RetailerId = req.ProductMapping.RetailerId,
                CommonProductChannelId = req.ProductMapping.CommonProductChannelId,
                CommonParentProductChannelId = req.ProductMapping.CommonParentProductChannelId
            };

            using (var cli = KvLockRedis.GetLockFactory())
            {
                var lockTimeOut = NumberHelper.GetValueOrDefault(AppSettings.Get<int>("LockAddMappingTimeOut"), 15);
                using (cli.AcquireLock(string.Format(KvConstant.LockAddMappingKey, req.ProductMapping.ChannelId), TimeSpan.FromSeconds(lockTimeOut)))
                {
                    mapping = await ProductMappingService.AddSync(newMapping);
                    CacheClient.Remove(string.Format(KvConstant.ListProductMappingCacheKey, InternalContext.RetailerId, mapping.ChannelId, mapping.ProductKvId));
                }
            }

            var productChannel = await ProductMongoService.GetByProductChannelId(InternalContext.RetailerId, req.ProductMapping.ChannelId, req.ProductMapping.CommonProductChannelId, isStringItemId: ConvertHelper.CheckUseStringItemId(channel?.Type));

            if (productChannel != null)
            {
                await ProductMongoService.UpdateProductConnect(productChannel.Id, true);
            }
            logMessage.LogInfo();

            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            UpdateProductFlagRelate(new List<long> { mapping.ProductKvId }, true, logId);
            #region Logs

            var logContent = AuditTrailHelper.GetMappingProductContent(
                new ProductMappingAuditTrail
                {
                    ChannelType = channel?.Type,
                    ChannelName = channel?.Name,
                    Items = new List<ProductMappingItemAuditTrail>
                    {
                        new ProductMappingItemAuditTrail
                        {
                            ProductKvSku = mapping.ProductKvSku, ParentProductChannelId = mapping.CommonParentProductChannelId, ProductChannelId = mapping.CommonProductChannelId, ProductChannelSku = mapping.ProductChannelSku
                        }
                    }
                }, false);

            var log = new AuditTrailLog
            {
                FunctionId = (int)FunctionType.MappingSalesChannel,
                Action = (int)AuditTrailAction.Create,
                CreatedDate = DateTime.Now,
                BranchId = InternalContext.BranchId,
                Content = logContent.ToString()
            };
            var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, InternalContext.RetailerId, connectStringName, InternalContext.BranchId, AppSettings.Get<int>("ExecutionContext"));
            var coreContext = context.ConvertTo<KvInternalContext>();
            coreContext.UserId = context.User?.Id ?? 0;
            await AuditTrailInternalClient.AddLogAsync(coreContext, log);
            //using (var msqClient = MessageFactory.CreateMessageQueueClient())
            //{
            //    msqClient.Publish(new AuditTrailMessage
            //    {
            //        Context = context,
            //        Data = log,
            //        LotId = ObjectId.GenerateNewId().ToString(),
            //        Key = context.Id,
            //    });
            //}
            #endregion

            if (channel == null || string.IsNullOrEmpty(connectStringName))
            {
                return ConvertToMappingDto(mapping);
            }
            await PushMessageSyncOnhandAndSyncPrice(connectStringName, channel, newMapping, logId, "Queue created from AddProductMapping");
            return ConvertToMappingDto(mapping);
        }

        public async Task<object> Post(DeleteProductMapping req)
        {
            var logId = Guid.NewGuid();
            var logMessage = new LogObjectMicrosoftExtension(Logger, logId)
            {
                Action = "DeleteProductMapping",
                RetailerId = InternalContext.RetailerId,
                OmniChannelId = req.ChannelId
            };
            var activeFeatureMultiSku = SelfAppConfig.CheckActiveFeature(SelfAppConfig.UseMultiMappingSku, InternalContext.RetailerId);
            var channel = await OmniChannelService.GetByIdAsync(req.ChannelId);
            if (channel?.IsAutoMappingProduct == true)
            {
                var mappingExist = await ProductMappingService.GetByProductId(InternalContext.RetailerId, req.ChannelId, req.KvProductId, req.ChannelProductId, isStringItemId: ConvertHelper.CheckUseStringItemId(channel.Type));
                if (mappingExist.ProductKvSku == mappingExist.ProductChannelSku)
                {
                    throw new KvValidateException(KvMessage.cannotAutoMapping);
                }
            }

            await ProductMongoService.UpdateProductConnect(InternalContext.RetailerId, req.ChannelId, req.ChannelProductId, false, isStringItemId: ConvertHelper.CheckUseStringItemId(channel?.Type));
            await ProductMongoService.UpdateProductSyncSuccess(InternalContext.RetailerId, req.ChannelId, req.ChannelProductId, isStringItemId: ConvertHelper.CheckUseStringItemId(channel?.Type));
            var mapDeleted = await ProductMappingService.Remove(InternalContext.RetailerId, req.ChannelId, req.KvProductId, req.ChannelProductId, isStringItemId: ConvertHelper.CheckUseStringItemId(channel?.Type));
            CacheClient.Remove(string.Format(KvConstant.ProductMappingCacheKey, req.ChannelId, InternalContext.RetailerId, req.KvProductId));
            CacheClient.Remove(string.Format(KvConstant.ListProductMappingCacheKey, InternalContext.RetailerId, req.ChannelId, req.KvProductId));

            logMessage.LogInfo();

            //Update flag mapping sale channel
            var productMapping = await ProductMappingService.GetByKvProductId(InternalContext.RetailerId, req.KvProductId);
            if (productMapping == null || !productMapping.Any())
            {
                UpdateProductFlagRelate(new List<long> { req.KvProductId }, false, logId);
            }

            if (mapDeleted == null)
            {
                return null;
            }
            #region Logs
            var logContent = AuditTrailHelper.GetMappingProductContent(
                new ProductMappingAuditTrail
                {
                    ChannelType = channel?.Type,
                    ChannelName = channel?.Name,
                    Items = new List<ProductMappingItemAuditTrail>
                    {
                        new ProductMappingItemAuditTrail
                        {
                            ProductKvSku = mapDeleted.ProductKvSku, ParentProductChannelId = mapDeleted.CommonParentProductChannelId, ProductChannelId = mapDeleted.CommonProductChannelId, ProductChannelSku = mapDeleted.ProductChannelSku
                        }
                    }
                }, true);

            var log = new AuditTrailLog
            {
                FunctionId = (int)FunctionType.MappingSalesChannel,
                Action = (int)AuditTrailAction.Reject,
                CreatedDate = DateTime.Now,
                BranchId = InternalContext.BranchId,
                Content = logContent.ToString()
            };
            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, InternalContext.RetailerId, connectStringName, InternalContext.BranchId, AppSettings.Get<int>("ExecutionContext"));
            var coreContext = context.ConvertTo<KvInternalContext>();
            coreContext.UserId = context.User?.Id ?? 0;
            await AuditTrailInternalClient.AddLogAsync(coreContext, log);
            #endregion

            //dong bo lai ton kho
            if (channel?.Type == (byte)ChannelType.Shopee && activeFeatureMultiSku)
            {
                await PushMessageSyncOnhandAndSyncPrice(connectStringName, channel, new ProductMapping() { ProductKvId = req.KvProductId }, logId, "Queue created from Delete mapping");
            }

            if (channel?.IsAutoMappingProduct == true)
            {
                await CreateNewMappingAfterDeleted(mapDeleted, logId);
            }

            return ConvertToMappingDto(mapDeleted);
        }

        public async Task<DeleteProductMappingByIdRes> Delete(DeleteProductMappingById req)
        {
            var logId = Guid.NewGuid();
            var logMessage = new LogObjectMicrosoftExtension(Logger, logId)
            {
                Action = "DeleteProductMappingById",
                RetailerId = InternalContext.RetailerId
            };

            var activeFeatureMultiSku = SelfAppConfig.CheckActiveFeature(SelfAppConfig.UseMultiMappingSku, InternalContext.RetailerId);
            var result = new DeleteProductMappingByIdRes { IsDeleted = false };

            if (req.ProductMappingId == null) return result;

            var mappingId = req.ProductMappingId.ToString();
            var prodMap = await ProductMappingService.GetByIdAsync(mappingId);
            if (prodMap == null) return result;
            logMessage.OmniChannelId = prodMap.ChannelId;
            var channel = await OmniChannelService.GetByIdAsync(prodMap.ChannelId);
            if (channel?.IsAutoMappingProduct == true)
            {
                if (prodMap.ProductKvSku == prodMap.ProductChannelSku)
                {
                    throw new KvValidateException(KvMessage.cannotAutoMapping);
                }
            }

            await ProductMongoService.UpdateProductConnect(InternalContext.RetailerId, prodMap.ChannelId, prodMap.CommonProductChannelId, false, isStringItemId: ConvertHelper.CheckUseStringItemId(channel?.Type));
            await ProductMongoService.UpdateProductSyncSuccess(InternalContext.RetailerId, prodMap.ChannelId, prodMap.CommonProductChannelId, isStringItemId: ConvertHelper.CheckUseStringItemId(channel?.Type));

            result.IsDeleted = await ProductMappingService.RemoveAsync(prodMap.Id);
            result.ProductMapping = prodMap;
            CacheClient.Remove(string.Format(KvConstant.ProductMappingCacheKey, prodMap.ChannelId, InternalContext.RetailerId, prodMap.ProductKvId));
            CacheClient.Remove(string.Format(KvConstant.ListProductMappingCacheKey, InternalContext.RetailerId, prodMap.ChannelId, prodMap.ProductKvId));
            logMessage.LogInfo();
            //Update flag mapping sale channel
            var productMapping = await ProductMappingService.GetByKvProductId(InternalContext.RetailerId, prodMap.ProductKvId);
            if (productMapping == null || !productMapping.Any())
            {
                UpdateProductFlagRelate(new List<long> { prodMap.ProductKvId }, false, logId);
            }

            #region Logs
            var logContent = AuditTrailHelper.GetMappingProductContent(
                new ProductMappingAuditTrail
                {
                    ChannelType = channel?.Type,
                    ChannelName = channel?.Name,
                    Items = new List<ProductMappingItemAuditTrail>
                    {
                        new ProductMappingItemAuditTrail
                        {
                            ProductKvSku = prodMap.ProductKvSku, ParentProductChannelId = prodMap.CommonParentProductChannelId, ProductChannelId = prodMap.CommonProductChannelId, ProductChannelSku = prodMap.ProductChannelSku
                        }
                    }
                }, true);

            var log = new AuditTrailLog
            {
                FunctionId = (int)FunctionType.MappingSalesChannel,
                Action = (int)AuditTrailAction.Reject,
                CreatedDate = DateTime.Now,
                BranchId = InternalContext.BranchId,
                Content = logContent.ToString()
            };
            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, InternalContext.RetailerId, connectStringName, InternalContext.BranchId, AppSettings.Get<int>("ExecutionContext"));
            var coreContext = context.ConvertTo<KvInternalContext>();
            coreContext.UserId = context.User?.Id ?? 0;
            await AuditTrailInternalClient.AddLogAsync(coreContext, log);
            //using (var msqClient = MessageFactory.CreateMessageQueueClient())
            //{
            //    msqClient.Publish(new AuditTrailMessage
            //    {
            //        Context = context,
            //        Data = log,
            //        LotId = ObjectId.GenerateNewId().ToString(),
            //        Key = context.Id,
            //    });
            //}
            #endregion

            //dong bo lai ton kho
            if (channel?.Type == (byte)ChannelType.Shopee && activeFeatureMultiSku)
            {
                await PushMessageSyncOnhandAndSyncPrice(connectStringName, channel, new ProductMapping() { ProductKvId = prodMap.ProductKvId }, logId, "Queue created from DeleteProductMappingById");
            }

            if (channel?.IsAutoMappingProduct == true)
            {
                await CreateNewMappingAfterDeleted(prodMap, logId);
            }


            return result;
        }

        public async Task<int> Post(DeleteProductMappingsByKvProduct req)
        {
            if (req.KvProductIds == null || req.KvProductIds.Count == 0) return 0;

            var logId = Guid.NewGuid();
            var logMessage = new LogObjectMicrosoftExtension(Logger, logId)
            {
                Action = "DeleteProductMappingsByKvProduct",
                RetailerId = InternalContext.RetailerId,
                RequestObject = req.KvProductIds
            };
            var activeFeatureMultiSku = SelfAppConfig.CheckActiveFeature(SelfAppConfig.UseMultiMappingSku, InternalContext.RetailerId);
            var connectStringName = InternalContext.Group?.ConnectionString?.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            if (string.IsNullOrEmpty(connectStringName))
            {
                logMessage.Description = "connectStringName is null";
                logMessage.LogError(new KvValidateException("connectStringName is null"), false, true);
                return 0;
            }
            var kvProductIds = req.KvProductIds;
            using (var db = DbConnectionFactory.Open(connectStringName.ToLower()))
            {
                var productRepo = new ProductRepository(db);
                var products = await productRepo.GetProductChildUnitByIds(InternalContext.RetailerId, InternalContext.BranchId, req.KvProductIds);
                var productChildUnitIds = products?.Select(m => m.Id)?.ToList();
                if (productChildUnitIds != null && kvProductIds != null)
                {
                    kvProductIds = kvProductIds.Union(productChildUnitIds).ToList();
                }
            }
            var mappings = await ProductMappingService.DeleteByKvProductId(InternalContext.RetailerId, kvProductIds);

            if (mappings == null || !mappings.Any()) return 0;
            var mappingChannelIds = mappings.Select(x => x.ChannelId).Distinct();
            var mappingChannels = (await OmniChannelService.GetByRetailerAsync(InternalContext.RetailerId))
                .Where(x => mappingChannelIds.Contains(x.Id))
                .OrderBy(x => x.Type).ThenByDescending(x => x.CreatedDate);

            foreach (var item in mappings)
            {
                var channel = mappingChannels.FirstOrDefault(x => x.Id == item.ChannelId);
                await ProductMongoService.UpdateProductConnect(InternalContext.RetailerId, item.ChannelId, item.CommonProductChannelId, false, isStringItemId: ConvertHelper.CheckUseStringItemId(channel?.Type));
                await ProductMongoService.UpdateProductSyncSuccess(InternalContext.RetailerId, item.ChannelId, item.CommonProductChannelId, isStringItemId: ConvertHelper.CheckUseStringItemId(channel?.Type));
                CacheClient.Remove(string.Format(KvConstant.ProductMappingCacheKey, item.ChannelId, InternalContext.RetailerId, item.ProductKvId));
                CacheClient.Remove(string.Format(KvConstant.ListProductMappingCacheKey, InternalContext.RetailerId, item.ChannelId, item.ProductKvId));

                //dong bo lai ton kho
                if (channel?.Type == (byte)ChannelType.Shopee && activeFeatureMultiSku)
                {
                    await PushMessageSyncOnhandAndSyncPrice(connectStringName, channel, new ProductMapping() { ProductKvId = item.ProductKvId }, logId, "Queue created from DeleteProductMappingsByKvProduct");
                }
            }
            //Update flag mapping sale channel
            var kvProductIdStillRelates = (await ProductMappingService.Get(InternalContext.RetailerId, null, req.KvProductIds)).Select(x => x.ProductKvId).Distinct().ToList();
            var kvProductIdNeedDels = req.KvProductIds.Where(x => !kvProductIdStillRelates.Contains(x)).ToList();
            UpdateProductFlagRelate(kvProductIdNeedDels, false, logId);

            #region Logs

            var logContent = new StringBuilder();
            logContent.Append("Hủy bỏ liên kết hàng hóa: <br/>");
            foreach (var channel in mappingChannels)
            {
                var productMappingAuditTrail = new ProductMappingAuditTrail
                {
                    ChannelType = channel?.Type,
                    ChannelName = channel?.Name,
                    Items = new List<ProductMappingItemAuditTrail>()
                };

                var listMapping = mappings.Where(x => x.ChannelId == channel.Id);
                foreach (var mapping in listMapping)
                {
                    productMappingAuditTrail.Items.Add(new ProductMappingItemAuditTrail { ProductKvSku = mapping.ProductKvSku, ParentProductChannelId = mapping.CommonParentProductChannelId, ProductChannelId = mapping.CommonProductChannelId, ProductChannelSku = mapping.ProductChannelSku });
                }

                var subLogContent = AuditTrailHelper.GetMappingProductContent(productMappingAuditTrail, true, false);
                logContent.Append(subLogContent);
            }

            var log = new AuditTrailLog
            {
                FunctionId = (int)FunctionType.MappingSalesChannel,
                Action = (int)AuditTrailAction.Reject,
                CreatedDate = DateTime.Now,
                BranchId = InternalContext.BranchId,
                Content = logContent.ToString()
            };
            var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, InternalContext.RetailerId, connectStringName, InternalContext.BranchId, AppSettings.Get<int>("ExecutionContext"));
            var coreContext = context.ConvertTo<KvInternalContext>();
            coreContext.UserId = context.User?.Id ?? 0;
            await AuditTrailInternalClient.AddLogAsync(coreContext, log);

            #endregion
            logMessage.LogInfo();
            return mappings.Count();
        }

        public async Task<object> Any(GetMappingByChannelProductId req)
        {
            var channel = await OmniChannelRepository.GetByIdAsync(req.ChannelId);
            var mappings = await ProductMappingService.GetByChannelProductIds(InternalContext.RetailerId, req.ChannelId,
                req.ChannelProductIds, isStringItemId: ConvertHelper.CheckUseStringItemId(channel?.Type));
            return ConvertToMappingDto(mappings);
        }
        private async Task CreateNewMappingAfterDeleted(ProductMapping oldMapping, Guid logId)
        {
            var listNewMapping = new List<ProductMapping>();
            var connectStringName = InternalContext.Group?.ConnectionString.Substring(InternalContext.Group.ConnectionString.IndexOf('=') + 1);
            var channel = await OmniChannelRepository.GetByIdAsync(oldMapping.ChannelId, true);
            if (!string.IsNullOrEmpty(oldMapping.ProductKvSku))
            {
                var channelProducts = (await ProductMongoService.GetProductNotConnnectBySku(InternalContext.RetailerId, new List<long> { oldMapping.ChannelId }, oldMapping.ProductKvSku)).FirstOrDefault();
                if (channelProducts != null)
                {
                    var map = new ProductMapping
                    {
                        ProductKvId = oldMapping.ProductKvId,
                        ProductKvSku = oldMapping.ProductKvSku,
                        ProductKvFullName = oldMapping.ProductKvFullName,
                        RetailerId = InternalContext.RetailerId,
                        RetailerCode = InternalContext.RetailerCode,
                        ChannelId = oldMapping.ChannelId,
                        CommonProductChannelId = channelProducts.CommonItemId,
                        ProductChannelSku = channelProducts.ItemSku,
                        ProductChannelType = channelProducts.Type,
                        CommonParentProductChannelId = channelProducts.CommonParentItemId,
                        ProductChannelName = channelProducts.ItemName,
                        CreatedDate = DateTime.Now
                    };
                    using (var cli = KvLockRedis.GetLockFactory())
                    {
                        var lockTimeOut = NumberHelper.GetValueOrDefault(AppSettings.Get<int>("LockAddMappingTimeOut"), 15);
                        using (cli.AcquireLock(string.Format(KvConstant.LockAddMappingKey, oldMapping.ChannelId), TimeSpan.FromSeconds(lockTimeOut)))
                        {
                            await ProductMappingService.AddOrUpdate(InternalContext.RetailerId, map);
                        }
                    }
                    await ProductMongoService.UpdateProductConnect(InternalContext.RetailerId, oldMapping.ChannelId, channelProducts.CommonItemId, true, isStringItemId: ConvertHelper.CheckUseStringItemId(channel?.Type));
                    listNewMapping.Add(map);
                }
            }
            if (!string.IsNullOrEmpty(oldMapping.ProductChannelSku))
            {
                if (string.IsNullOrEmpty(connectStringName))
                {
                    return;
                }
                using (var db = DbConnectionFactory.Open(connectStringName.ToLower()))
                {
                    var productRepo = new ProductRepository(db);
                    var productBranchRepo = new ProductBranchRepository(db);
                    var kvProducts = productRepo.Where(x => x.RetailerId == InternalContext.RetailerId && x.Code == oldMapping.ProductChannelSku && x.IsActive);

                    if (kvProducts.Any())
                    {
                        var prodIds = kvProducts.Select(x => x.Id).ToList();
                        var productBranchs = await productBranchRepo.WhereAsync(x => x.BranchId == channel.BranchId && prodIds.Contains(x.ProductId));

                        var kvProductActive = new List<Domain.Model.Product>();
                        foreach (var item in kvProducts)
                        {
                            var isActive = !productBranchs.Any(x => x.ProductId == item.Id && x.IsActive == false);

                            if (isActive)
                            {
                                kvProductActive.Add(item);
                            }
                        }
                        var kvProduct = kvProductActive.FirstOrDefault(x => x.Code == oldMapping.ProductChannelSku);
                        var existMapping = await ProductMappingService.GetByKvSingleProductId(InternalContext.RetailerId, oldMapping.ChannelId, kvProduct?.Id ?? 0);
                        if (kvProduct != null && existMapping == null)
                        {
                            var map = new ProductMapping
                            {
                                ProductKvId = kvProduct.Id,
                                ProductKvSku = kvProduct.Code,
                                ProductKvFullName = kvProduct.FullName,
                                RetailerId = InternalContext.RetailerId,
                                RetailerCode = InternalContext.RetailerCode,
                                ChannelId = oldMapping.ChannelId,
                                CommonProductChannelId = oldMapping.CommonProductChannelId,
                                ProductChannelSku = oldMapping.ProductChannelSku,
                                ProductChannelType = oldMapping.ProductChannelType,
                                CommonParentProductChannelId = oldMapping.CommonParentProductChannelId,
                                ProductChannelName = oldMapping.ProductChannelName,
                                CreatedDate = DateTime.Now
                            };
                            using (var cli = KvLockRedis.GetLockFactory())
                            {
                                var lockTimeOut = NumberHelper.GetValueOrDefault(AppSettings.Get<int>("LockAddMappingTimeOut"), 15);
                                using (cli.AcquireLock(string.Format(KvConstant.LockAddMappingKey, map.ChannelId), TimeSpan.FromSeconds(lockTimeOut)))
                                {
                                    await ProductMappingService.AddOrUpdate(InternalContext.RetailerId, map);
                                }
                            }
                            await ProductMongoService.UpdateProductConnect(InternalContext.RetailerId, oldMapping.ChannelId, oldMapping.CommonProductChannelId, true, isStringItemId: ConvertHelper.CheckUseStringItemId(channel?.Type));
                            listNewMapping.Add(map);
                        }
                    }
                }
            }

            if (listNewMapping.Any())
            {
                //Sync onhand and sync price
                foreach (var mapping in listNewMapping)
                {
                    await PushMessageSyncOnhandAndSyncPrice(connectStringName, channel, mapping, logId, "Queue created from CreateNewMappingAfterDeleted");
                }

                var kvProductIds = listNewMapping.Select(x => x.ProductKvId).ToList();
                UpdateProductFlagRelate(kvProductIds, true, logId);
                #region Logs

                var productMappingAuditTrail = new ProductMappingAuditTrail { ChannelType = channel?.Type, ChannelName = channel?.Name, Items = new List<ProductMappingItemAuditTrail>() };
                foreach (var mapping in listNewMapping)
                {
                    productMappingAuditTrail.Items.Add(new ProductMappingItemAuditTrail
                    {
                        ProductKvSku = mapping.ProductKvSku,
                        ParentProductChannelId = mapping.CommonParentProductChannelId,
                        ProductChannelId = mapping.CommonProductChannelId,
                        ProductChannelSku = mapping.ProductChannelSku
                    });
                }

                var logContent = AuditTrailHelper.GetMappingProductContent(productMappingAuditTrail, false);

                var log = new AuditTrailLog
                {
                    FunctionId = (int)FunctionType.MappingSalesChannel,
                    Action = (int)AuditTrailAction.Create,
                    CreatedDate = DateTime.Now,
                    BranchId = InternalContext.BranchId,
                    Content = logContent.ToString()
                };
                var context = await ContextHelper.GetExecutionContext(CacheClient, DbConnectionFactory, InternalContext.RetailerId, connectStringName, InternalContext.BranchId, AppSettings.Get<int>("ExecutionContext"));

                var coreContext = context.ConvertTo<KvInternalContext>();
                coreContext.UserId = context.User?.Id ?? 0;
                await AuditTrailInternalClient.AddLogAsync(coreContext, log);

                #endregion
            }
        }

        private async Task PushMessageSyncOnhandAndSyncPrice(string connectStringName, Domain.Model.OmniChannel channel, ProductMapping mapping, Guid logId, string sourceQueue)
        {
            var productLock = CacheClient.Get<bool>(string.Format(KvConstant.LockProductStockCache, channel.Id, mapping.ProductKvId));
            if (!productLock && channel.OmniChannelSchedules?.Any(x => x.Type == (byte)ScheduleType.SyncOnhand) == true)
            {
                CacheClient.Set(string.Format(KvConstant.LockProductStockCache, channel.Id, mapping.ProductKvId), true, TimeSpan.FromSeconds(2));
                await OnHandBusiness.SyncMultiOnHand(connectStringName,
                    channel,
                    mapping.CommonProductChannelId != null ? new List<string>
                    {
                        mapping.CommonProductChannelId
                    } : null,
                    mapping.ProductKvId != 0 ? new List<long>
                    {
                        mapping.ProductKvId
                    } : null,
                    false,
                    logId,
                    null,
                    true);
            }
            if (channel.OmniChannelSchedules?.Any(x => x.Type == (byte)ScheduleType.SyncPrice) == true)
            {
                await PriceBusiness.SyncMultiPrice(connectStringName,
                    channel,
                    new List<string>
                    {
                        mapping.CommonProductChannelId
                    },
                    null,
                    false,
                    logId,
                    true, 50, null, true, sourceQueue);
            }
        }

        private MappingDto ConvertToMappingDto(ProductMapping mapping)
        {
            var mappingDto = new MappingDto
            {
                ChannelId = mapping.ChannelId,
                RetailerId = mapping.RetailerId,
                RetailerCode = mapping.RetailerCode,
                CommonParentProductChannelId = mapping.CommonParentProductChannelId,
                CommonProductChannelId = mapping.CommonProductChannelId,
                CreatedDate = mapping.CreatedDate,
                ModifiedDate = mapping.ModifiedDate,
                ProductChannelName = mapping.ProductChannelName,
                ProductChannelSku = mapping.ProductChannelSku,
                ProductChannelType = mapping.ProductChannelType,
                ProductKvFullName = mapping.ProductKvFullName,
                ProductKvId = mapping.ProductKvId,
                ProductKvSku = mapping.ProductKvSku,
                Id = mapping.Id
            };
            return mappingDto;
        }

        private List<MappingDto> ConvertToMappingDto(List<ProductMapping> mappings)
        {
            var mappingDtos = new List<MappingDto>();
            if (mappings == null) return mappingDtos;
            foreach (var mapping in mappings)
            {
                var mappingDto = new MappingDto
                {
                    ChannelId = mapping.ChannelId,
                    RetailerId = mapping.RetailerId,
                    RetailerCode = mapping.RetailerCode,
                    CommonParentProductChannelId = mapping.CommonParentProductChannelId,
                    CommonProductChannelId = mapping.CommonProductChannelId,
                    CreatedDate = mapping.CreatedDate,
                    ModifiedDate = mapping.ModifiedDate,
                    ProductChannelName = mapping.ProductChannelName,
                    ProductChannelSku = mapping.ProductChannelSku,
                    ProductChannelType = mapping.ProductChannelType,
                    ProductKvFullName = mapping.ProductKvFullName,
                    ProductKvId = mapping.ProductKvId,
                    ProductKvSku = mapping.ProductKvSku,
                    Id = mapping.Id
                };
                mappingDtos.Add(mappingDto);
            }

            return mappingDtos;
        }

        private List<MappingDto> ConvertToMappingDtoWithSuperId(List<ProductMapping> mappings, Dictionary<string, Product> productDic)
        {
            var mappingDtos = new List<MappingDto>();
            if (mappings == null) return mappingDtos;
            foreach (var mapping in mappings)
            {
                Product product = productDic[mapping.CommonProductChannelId];
                long superId = 0;
                if (product != null && product.SuperId != null) superId = (long)product.SuperId;
                var mappingDto = new MappingDto
                {
                    ChannelId = mapping.ChannelId,
                    RetailerId = mapping.RetailerId,
                    RetailerCode = mapping.RetailerCode,
                    CommonParentProductChannelId = mapping.CommonParentProductChannelId,
                    CommonProductChannelId = mapping.CommonProductChannelId,
                    CreatedDate = mapping.CreatedDate,
                    ModifiedDate = mapping.ModifiedDate,
                    ProductChannelName = mapping.ProductChannelName,
                    ProductChannelSku = mapping.ProductChannelSku,
                    ProductChannelType = mapping.ProductChannelType,
                    ProductKvFullName = mapping.ProductKvFullName,
                    ProductKvId = mapping.ProductKvId,
                    ProductKvSku = mapping.ProductKvSku,
                    Id = mapping.Id,
                    SuperId = superId,
                };
                mappingDtos.Add(mappingDto);
            }

            return mappingDtos;
        }

        #region Response

        public class DeleteProductMappingByIdRes
        {
            public bool IsDeleted { get; set; }

            public ProductMapping ProductMapping { get; set; }
        }

        #endregion
    }
}

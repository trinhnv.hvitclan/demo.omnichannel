﻿using System.Collections.Generic;
using ServiceStack;

namespace Demo.OmniChannel.Api.ServiceModel
{
    [Route("/shopee/logistics")]
    public class LogisticsRequest : IReturn<object>
    {
        public List<long> ChannelIds { get; set; }
    }

    [Route("/shopee/attributes")]
    public class AttributesRequest : IReturn<object>
    {
        public long ChannelId { get; set; }

        public long CategoryId { get; set; }
    }



    [Route("/shopee/getalllogisticsinfo", "POST")]
    [Route("/shopee/getLogisticsFilter", "POST")]
    public class GetLogisticsFilter : IReturn<ReponseResultLogicstic>
    {
        public List<long> ChannelIds { get; set; }
    }

    public class ReponseResultLogicstic
    {
        public bool IsVeryQuickLogicstic { get; set; }
        public bool IsQuickLogicstic { get; set; }
        public bool IsThriftyLogicstic { get; set; }
    }
}

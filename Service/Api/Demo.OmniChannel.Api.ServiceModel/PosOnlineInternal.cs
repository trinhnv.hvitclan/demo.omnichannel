using System.Collections.Generic;
using ServiceStack;

namespace Demo.OmniChannel.Api.ServiceModel
{
    [Route("/internal/pos-online/product-branches", "POST")]
    public class InternalProductBranch : IReturn<object>
    {
        public int RetailerId { get; set; }

        public int BranchId { get; set; }

        public List<long> ProductIds { get; set; }
    }

    [Route("/internal/pos-online/products", "POST")]
    public class InternalProduct : IReturn<object>
    {
        public int RetailerId { get; set; }

        public byte ProductType { get; set; }

        public List<long> ProductIds { get; set; }
    }

    [Route("/internal/pos-online/price-books", "GET")]
    public class InternalPriceBook : IReturn<object>
    {
        public int RetailerId { get; set; }
        public List<long> PriceBookIds { get; set; }
    }
}
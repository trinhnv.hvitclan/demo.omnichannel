﻿using ServiceStack;

namespace Demo.OmniChannel.Api.ServiceModel
{
    [Route("/migrate-data/sale-channel", "POST")]
    public class MirgrateDataSaleChannel : IReturn<object>
    {
        public int GroupId { get; set; }
        public int RetailerId { get; set; }
        public int LimitRetailer { get; set; }
    }

    [Route("/migrate-data/create-temp-sale-channel", "POST")]
    public class CreateTempTableSaleChannel : IReturn<object>
    {
        public int GroupId { get; set; }
        public int RetailerId { get; set; }
    }

    [Route("/migrate-data/delete-temp-sale-channel", "POST")]
    public class DeleteTempTableSaleChannel : IReturn<object>
    {
        public int GroupId { get; set; }
        public int RetailerId { get; set; }

    }
}
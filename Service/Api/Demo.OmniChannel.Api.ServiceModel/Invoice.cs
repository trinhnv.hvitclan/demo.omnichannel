﻿using System;
using System.Collections.Generic;
using Demo.OmniChannel.ChannelClient.Common;
using Demo.OmniChannel.Domain.Common;
using Demo.OmniChannel.MongoDb;
using Demo.Utilities;
using ServiceStack;

namespace Demo.OmniChannel.Api.ServiceModel
{
    [Route("/invoice/errors")]
    public class GetInvoiceSyncError : IReturn<PagingDataSource<object>>
    {
        public int RetailerId { get; set; }
        public List<long> ChannelIds { get; set; }
        public List<int> BranchIds { get; set; }
        public int Top { get; set; }
        public int Skip { get; set; }
        public string InvoiceCode { get; set; }
        public string SearchTerm { get; set; }
        public string KeySearchProduct { get; set; }
    }

    [Route("/invoice/{id}")]
    public class GetInvoiceById : IReturn<object>
    {
        public string Id { get; set; }
    }

    [Route("/invoice/errors/{id}", "DELETE")]
    public class RemoveInvoiceSyncError : IReturn<object>
    {
        public string Id { get; set; }
    }
    [Route("/invoice/errors/updatemessage", "POST")]
    public class UpdateErrorMessageInvoice : IReturn<object>
    {
        public Invoice Invoice { get; set; }
        public string ErrorMessage { get; set; }
        public string AuditTrailMessage { get; set; }
    }
    [Route("/invoice/errors/totalinvoice")]
    public class TotalInvoiceError : IReturn<long>
    {
        public List<long> ChannelIds { get; set; }
    }
    [Route("/invoice/errors/syncall", "POST")]
    public class MultiSyncErrorInvoice : IReturn<object>
    {
        public string Invoices { get; set; }
        public string Channels { get; set; }
    }
    [Route("/invoice/getlogisticsstatus")]
    public class GetLogisticsStatus : IReturn<object>
    {
        public string OrderId { get; set; }
        public long ChannelId { get; set; }
    }

    [Route("/invoice/getchannelpaymentinvoices", "POST")]
    public class GetChannelPaymentInvoices : IReturn<object>
    {
        public long RetailerId { get; set; }
        public List<string> OrderSnLst { get; set; }
    }
 
    public class ChannelStatus
    {
        public string OrderStatus { get; set; }
        public byte? LogisticsStatus { get; set; }
        public string TrackingNumber { get; set; }
    }

    public class InvoiceDto
    {
        public string Id { get; set; }
        public string Uuid { get; set; }
        public string Code { get; set; }
        public DateTime PurchaseDate { get; set; }
        public string BranchName { get; set; }
        public long SoldById { get; set; }
        public int? SaleChannelId { get; set; }
        public int RetailerId { get; set; }
        public int BranchId { get; set; }
        public string SoldByName { get; set; }
        public long? CustomerId { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public long? OrderId { get; set; }
        public string OrderCode { get; set; }
        public decimal Total { get; set; }
        public decimal TotalPayment { get; set; }
        public decimal? Discount { get; set; }
        public double? DiscountRatio { get; set; }
        public byte Status { get; set; }
        public string StatusValue { get; set; }
        public string Description { get; set; }
        public long PriceBookId { get; set; }
        public bool UsingCod { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public byte? DeliveryStatus { get; set; }
        public bool IsSyncSuccess { get; set; }
        public byte? NewStatus { get; set; }
        public string ChannelStatus { get; set; }
        public string ChannelOrder { get; set; }
        public string ChannelDiscount { get; set; }
        public string ErrorMessage { get; set; }
        public string ChannelOrderId { get; set; }
        public long ChannelId { get; set; }
        public InvoiceDelivery DeliveryDetail { get; set; }
        public List<InvoiceDetailDto> InvoiceDetails { get; set; }
        public List<InvoiceSurCharges> InvoiceSurCharges { get; set; }
        public string ChannelName { get; set; }
        public List<string> ErrorMappingProductIDs { get; set; } // Danh sách ItemId, ParentId chưa mapping hoặc mapping lỗi
        public bool CurrentSaleTime { get; set; }
    }

    public class InvoiceDetailDto
    {
        public double ReturnQuantity;
        public long ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public double Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal? Discount { get; set; }
        public double? DiscountRatio { get; set; }
        public bool? UsePoint { get; set; }
        public decimal SubTotal { get; set; }
        public string Note { get; set; }
        public string SerialNumbers { get; set; }
        public string ProductChannelId { get; set; }
        public string ProductChannelSku { get; set; }
        public string ProductChannelName { get; set; }
        public string ParentChannelProductId { get; set; }
        public float DiscountPrice { get; set; }
        public bool UseProductBatchExpire { get; set; }
        public bool UseProductSerial { get; set; }
        public long? ProductBatchExpireId { get; set; }
        public string Uuid { get; set; }
        public string StrProductChannelId { get; set; }
        public string StrParentChannelProductId { get; set; }
        public List<ProductBatchExpiresInfo> ProductBatchExpires { get; set; }
        public List<string> ProductSerials { get; set; }
        public List<string> ProductImages { get; set; }

        public string CommonProductChannelId
        {
            get
            {
                return string.IsNullOrEmpty(StrProductChannelId) ? ProductChannelId : StrProductChannelId;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    ProductChannelId = value;
                    StrProductChannelId = value;
                }
            }
        }

        public string CommonParentChannelProductId
        {
            get
            {
                return string.IsNullOrEmpty(StrParentChannelProductId) ? ParentChannelProductId : StrParentChannelProductId;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    ParentChannelProductId = value;
                    StrParentChannelProductId = value;
                }
            }
        }
    }
    public class InvoicesChannelResponse
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public byte Status { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public DateTime? SuccessfulDeliveryDate { get; set; }
        public decimal? SumDeliveryPrices { get; set; }
        public decimal? Total { get; set; }
        public decimal? RealPaymentReturnFromChannel { get; set; }
        public decimal? PaymentTotalFromKv { get; set; }
        public decimal? DiffReturnValue { get; set; }
        public int? DiffStatus
        {
            get
            {
                if (DiffReturnValue == null)
                {
                    return (int?)DiffReturn.NotYetPayment;
                }
                else if (DiffReturnValue > 0)
                {
                    return (int?)DiffReturn.GreaterThanZero;
                }
                else if (DiffReturnValue < 0)
                {
                    return (int?)DiffReturn.LessThanZero;
                }
                return (int?)DiffReturn.EqualZero;

            }
        }
    }
}
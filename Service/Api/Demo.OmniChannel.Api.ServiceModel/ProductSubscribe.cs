﻿using System.Collections.Generic;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.ShareKernel.KafkaMessage;
using ServiceStack;

namespace Demo.OmniChannel.Api.ServiceModel
{
    [Route("/proxy-product-subscribe", "POST")]
    public class ProxyProductSubscribe: SyncEsMessage<ProductSubscribe>, IReturn<object>
    {
    }
    public class SyncEsMessage<T> : BaseRedisMessage where T : class
    {
        public List<T> Products { get; set; }
    }

    [Route("/proxy-pricebook-subscribe", "POST")]
    public class ProxyPriceBookSubscribe : PriceBookSyncEsMessage<ProductSubscribe>, IReturn<object>
    {
    }
}
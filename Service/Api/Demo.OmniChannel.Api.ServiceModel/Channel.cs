﻿using System;
using System.Collections.Generic;
using ServiceStack;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.ShareKernel.Dtos;
using Demo.OmniChannel.ShareKernel.Common;
using Demo.OmniChannel.Api.ServiceModel.Request;

namespace Demo.OmniChannel.Api.ServiceModel
{
    public class ShopListReq : IReturn<object>
    {
        public int RetailerId { get; set; }
        public int Type { get; set; }
    }

    public class PushDataItemToChannelReq
    {
        public ProductDataPushChannel PushDataItemToChannel { get; set; }
        public List<ChannelType> ChannelTypes { get; set; }
    } 

    [Route("/channel", "POST")]
    public class ChannelCreateOrUpdate : IReturn<object>
    {
        public OmniChannelRequest Channel { get; set; }
        public bool IsSyncOrder { get; set; }
        public bool IsSyncOnHand { get; set; }
        public bool IsSyncPrice { get; set; }
        public long AuthId { get; set; }
        public bool IsIgnoreAuditTrail { get; set; }
    }
    [Route("/channel/retailer/{RetailerId}")]
    public class GetByRetailer : IReturn<object>
    {
        public int RetailerId { get; set; }
        public int BranchId { get; set; }
        public byte? Type { get; set; }
        public List<int> Types { get; set; }
        public bool OnlyIsActive { get; set; }
        public List<long> ChannelIds { get; set; }
    }
    [Route("/channel/{Id}", "GET")]
    [Route("/channel/{Id}", "DELETE")]
    public class GetById : IReturn<object>
    {
        public long Id { get; set; }
    }
    [Route("/channel/renewtoken", "POST")]
    public class RenewToken : IReturn<object>
    {
        public int RetailerId { get; set; }
        public int BranchId { get; set; }
        public long ChannelId { get; set; }
    }

    [Route("/channel/resetsynconhandformula", "POST")]
    public class ResetFormula : IReturn<object>
    {
        public int RetailerId { get; set; }
        public string SettingKey { get; set; }
        public bool OtherSettingValue { get; set; }
    }
    [Route("/channel/authcreateorupdate", "POST")]
    public class ChannelAuthCreateOrUpdate : IReturn<object>
    {
        public OmniChannelAuth ChannelAuthRequest { get; set; }
        public string LogId { get; set; }
    }
    [Route("/channel/createauthsendo", "POST")]
    public class CreateAuthSendo : IReturn<object>
    {
        public long ChannelId { get; set; }
        public string ChannelName { get; set; }
        public string ShopKey { get; set; }
        public string SecretKey { get; set; }
    }
    [Route("/channel/remove-channel-auth", "POST")]
    public class RemoveChannelAuth : IReturn<object>
    {
        public long AuthId { get; set; }
    }

    [Route("/channel/resetpricebook", "POST")]
    public class ResetPriceBook : IReturn<object>
    {
        public long PriceBookId { get; set; }
    }

    [Route("/channel/syncAgainErrorChannel", "POST")]
    public class SyncErrorChannel : IReturn<object>
    {
        public long? ChannelId { get; set; }
        public List<string> ItemIds { get; set; }
    }
    [Route("/channel/deactivateChannel", "POST")]
    public class DeactivateChannel : IReturn<object>
    {
        public long ChannelId { get; set; }
        public string ChannelName { get; set; }
        public string AuditContent { get; set; }
    }
    [Route("/channel/updateTokenForChannel", "POST")]
    public class UpdateTokenForChannel : IReturn<object>
    {
        public OmniChannelAuth Auth { get; set; }
    }

    [Route("/channel/disableByType", "POST")]
    public class DisableByType : IReturn<object>
    {
        public long RetailerId { get; set; }
        public byte ChannelType { get; set; }
    }

    [Route("/channel/enableUpdateProduct", "POST")]
    public class EnableUpdateProduct : IReturn<object>
    {
        public long ChannelId { get; set; }
    }
    [Route("/channel/enableChannel", "POST")]
    public class EnableChannel : IReturn<object>
    {
        public long ChannelId { get; set; }
        public OmniChannelAuth Auth { get; set; }
        public string BranchName { get; set; }
        public string PriceBookName { get; set; }
        public string SalePriceBookName { get; set; }
        public string ShopName { get; set; }
        public string Email { get; set; }
        public long PlatformId { get; set; } 
    }

    [Route("/channel/clearData", "POST")]
    public class ClearData : IReturn<object>
    {
        public int RetailerId { get; set; }
    }

    [Route("/channel/getChannelExpiry", "GET")]
    public class GetChannelExpiry : IReturn<object>
    {
        public int RetailerId { get; set; }
    }

    [Route("/channel/getSaleChannelOmniByType" , "POST")]
    public class GetSaleChannelOmniByType : IReturn<object>
    {
        public List<int> ChannelTypeLst { get; set; }
        public int GroupId { get; set; }
        public int BranchId { get; set; }
    }

    [Route("/channel/categories", "GET")]
    public class GetCategoriesReq : IReturn<object>
    {
        public long ChannelId { get; set; }
    }

    [Route("/channel/getCountChannels", "GET")]
    public class GetCountChannels : IReturn<object>
    {

    }

    [Route("/channel/getListChannelWareHouseInfo", "POST")]
    public class GetListChannelWareHouseInfo : IReturn<object>
    {
        public long OmniChannelAuthId { get; set; }
    }

    [Route("/channel/handleWareHouseTIKI", "POST")]
    public class HandleWareHouseTiki : IReturn<object>
    {
        public long OmniChannelId { get; set; }
        public TikiWareHouseRequest TikiWareHouseRequest { get; set; }
    }

    [Route("/channel/push-product", "POST")]
    public class ProductDataPushChannelReq : IReturn<object>
    {
        public ProductDataPushChannel ProductDataPushChannel { get; set; }
    }

    [Route("/channel/activeTrialPosOnline", "POST")]
    public class ActiveTrialPosOnline : IReturn<object>
    {
       public string Name { get; set; }
       public string PhoneNumber { get; set; }
       public bool IsAdvise { get; set; }
    }

    [Route("/channel/expired")]
    public class GetExistChannelExpired : GetByRetailer, IReturn<GetExistChannelExpiredResponse>
    {
    }
    [Route("/channel/get-label-fee", "GET")]
    public class GetFeeLabel : IReturn<object>
    {
        public int ChannelType { get; set; }
    }

    #region Response

    public class OmniChannelResponse : Domain.Model.OmniChannel
    {
        public long TotalProductConnected { get; set; }
        public bool IsExpireWarning { get; set; }
    }

    public class TikiWareHouseRequest
    {
        public long WareHouseId { get; set; }
        public string Name { get; set; }
        public string StreetInfo { get; set; }
        public string Code { get; set; }
    }


    public class OmniChannelExpiryResponse
    {
        public byte ChannelType { get; set; }
        public string ChannelName { get; set; }
        public DateTime ExpiryDate { get; set; }
        public bool IsActive { get; set; }
    }

    public class ListChannelWareHouseInfoResponse
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string StreetInfo { get; set; }
    }

    public class SendoCreateAuthResponse
    {
        public long AuthId { get; set; }
        public string IdentityKey { get; set; }
    }

    public class AccessTokenResponse
    {
        public string AccessToken { get; set; }
    }

    public class GetExistChannelExpiredResponse
    {
        public bool HasChannelExpired { get; set; }
        public int Total { get; set; }
        public List<OmniChannelResponse> Data { get; set; }
    }

    #endregion
}
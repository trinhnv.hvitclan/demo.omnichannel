﻿using System;
using ServiceStack;

namespace Demo.OmniChannel.Api.ServiceModel
{
    [Route("/eventKol", "POST")]
    public class EventKolCreate : IReturn<object>
    {
        public int RetailerId { get; set; }
        public long EventId { get; set; }
        public int Status { get; set; }
    }

    //[Route("/internal2/deactivateChannel", "POST")]
    [Route("/eventKol/error", "GET")]
    public class GetEventKolError : IReturn<object>
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? Page { get; set; }
        public int? NumberOfPage { get; set; }
        public string NameConnectionString { get; set; }
    }


}

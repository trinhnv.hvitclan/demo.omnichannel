﻿using System;

namespace Demo.OmniChannel.Api.ServiceModel.Request
{
    public class OmniChannelRequest
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public byte Type { get; set; }
        public byte Status { get; set; }
        public int BranchId { get; set; }
        public int RetailerId { get; set; }
        public long? PriceBookId { get; set; }
        public long? BasePriceBookId { get; set; }
        public byte SyncOnHandFormula { get; set; }
        public int? SyncOrderFormula { get; set; }
        public bool IsApplyAllFormula { get; set; }
        public bool IsAutoMappingProduct { get; set; }
        public bool? IsAutoDeleteMapping { get; set; }
        public string IdentityKey { get; set; } 
        public DateTime? RegisterDate { get; set; }
        public string ExtraKey { get; set; } 
        public long PlatformId { get; set; }
        public OmniChannelSettingRequest OmniChannelSettings { get; set; }
    }
}

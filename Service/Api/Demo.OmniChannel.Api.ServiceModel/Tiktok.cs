﻿using ServiceStack;

namespace Demo.OmniChannel.Api.ServiceModel
{

    [Route("/tiktok/getAuthConfig", "GET")]
    public class TikTokAuthConfig : IReturn<object>
    {
        public int AppSupport { get; set; }
    }

    [Route("/tiktok/getProfile", "GET")]
    public class TiktTokProfileAndUpdateAuth : IReturn<object>
    {
        public string Code { get; set; }
        public long PlatformId { get; set; }
    }

    [Route("/tiktok/getProfileInfo", "GET")]
    public class TiktTokGetProfileInfo : IReturn<object>
    {
        public string State { get; set; }
    }


    [Route("/tiktok/profileAndToken", "Get")]
    public class TikTokEnableChannel : IReturn<object>
    {
        public long PlatformId { get; set; }
        public string Code { get; set; }
        public string IdentityKey { get; set; }
        public string ShopName { get; set; }
    }
}
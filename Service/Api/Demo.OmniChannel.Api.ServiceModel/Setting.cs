﻿using ServiceStack;

namespace Demo.OmniChannel.Api.ServiceModel
{
    [Route("/setting/createUpdateCustomer", "POST")]
    public class CreateUpdateSettingCustomer : IReturn<object>
    {
        public bool IsShopeeSyncCustomer { get; set; }

        public bool IsLazadaSyncCustomer { get; set; }

        public bool IsTikiSyncCustomer { get; set; }

        public bool IsSendoSyncCustomer { get; set; }
        public bool IsTiktokShopSyncCustomer { get; set; }

        public string KeyUpdate { get; set; }
    }

    [Route("/setting/getSettingsByRetailer", "GET")]
    public class GetSettingByRetailer : IReturn<object>
    {

    }
}

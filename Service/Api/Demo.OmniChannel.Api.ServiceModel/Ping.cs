using ServiceStack;

namespace Demo.OmniChannel.Api.ServiceModel
{
    [Route("/ping")]
    public class Ping : IReturn<object>
    {
    }

    [Route("/pingVersion")]
    public class PingVersion : IReturn<object>
    {
    }

    public class PingApi : Service
    {
        public string Any(Ping req)
        {
            return "pong";
        }

        public string Any(PingVersion req)
        {
            return "PingVersion";
        }
    }
}

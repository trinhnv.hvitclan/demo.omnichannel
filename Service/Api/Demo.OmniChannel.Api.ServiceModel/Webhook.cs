﻿using Newtonsoft.Json;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Demo.OmniChannel.Api.ServiceModel
{

    [Route("/callback/tiktokCallBack", "GET")]
    public class TiktokCallBackSubcribe : IReturn<object>
    {
        public string Code { get; set; }
        public string State { get; set; }
    }

    [Route("/webhook/shopeeOrderUpdateStatus", "POST")]
    public class ShopeeOrderUpdateSubcribe : IReturn<object>
    {
        [DataMember(Name = "shop_id")]
        public long ShopId { get; set; }
        [DataMember(Name = "code")]
        public int Code { get; set; }
        [DataMember(Name = "data")]
        public Dictionary<string, string> Order { get; set; }
        [DataMember(Name = "timestamp")]
        public long? Timestamp { get; set; }

        //for other channel
        [DataMember(Name = "identity_key")]
        public string IdentityKey { get; set; }
        [DataMember(Name = "channel_type")]
        public byte? ChannelType { get; set; }
    }

    [Route("/webhook/lazadaUpdateFeeOrderSn", "POST")]
    public class LazadaUpdateFeeOrderSn : IReturn<object>
    {
        public string ShopId { get; set; }
        public string OrderSn { get; set; }
        public long RetailerId { get; set; }
        public int ChannelId { get; set; }
        public int BranchId { get; set; }
    }

    public class ShopeeOrder
    {
        [DataMember(Name = "ordersn")]
        public string OrderSn { get; set; }

        [DataMember(Name = "status")]
        public string Status { get; set; }

        [DataMember(Name = "update_time")]
        public string Update_Time { get; set; }

        [DataMember(Name = "tracking_no")]
        public string TrackingNo { get; set; }

        //for sp lazada
        [DataMember(Name = "order_str")]
        public string OrderString { get; set; }
    }

    public class PromotionUpdate
    {
        [JsonProperty(PropertyName = "shop_id")]
        public long ShopId { get; set; }
        [JsonProperty(PropertyName = "promotion_id")]
        public long PromotionId { get; set; }
        [JsonProperty(PropertyName = "promotion_type")]
        public string PromotionType { get; set; }
        [JsonProperty(PropertyName = "action")]
        public string Action { get; set; }
        [JsonProperty(PropertyName = "item_id")]
        public long ItemId { get; set; }
        [JsonProperty(PropertyName = "variation_id")]
        public long VariationId { get; set; }
        [JsonProperty(PropertyName = "reserved_stock")]
        public int ReservedStock { get; set; }
    }

    [Route("/webhook/resetlastsync", "POST")]
    public class ResetLastSync : IReturn<object>
    {
        [JsonProperty(PropertyName = "scheduleIds")]
        public List<string> ScheduleIds { get; set; }
        [JsonProperty(PropertyName = "lastSync")]
        public DateTime LastSync { get; set; }
        [JsonProperty(PropertyName = "channelType")]
        public byte? ChannelType { get; set; }

        [JsonProperty(PropertyName = "scheduleType")]
        public byte? ScheduleType { get; set; }
        [JsonProperty(PropertyName = "includeRetail")]
        public List<int> IncludeRetail { get; set; }
    }

    [Route("/webhook/void-orders", "POST")]
    public class VoidOrderAndInvoiceLst : IReturn<object>
    {

        [JsonProperty(PropertyName = "prefixDelete")]
        public string PrefixDelete { get; set; } = "DELETED";

        [JsonProperty(PropertyName = "retailerId")]
        public int RetailerId { get; set; }

        [JsonProperty(PropertyName = "includeOrderCodeLst")]
        public List<string> IncludeOrderCodeLst { get; set; }
    }

    [Route("/webhook/resync-tracking-invoice", "POST")]
    public class ResyncTrackingInvoice : IReturn<object>
    {
        [JsonProperty(PropertyName = "retailerId")]
        public int RetailerId { get; set; }
        [JsonProperty(PropertyName = "includeInvoiceCodeLst")]
        public List<string> IncludeInvoiceCodeLst { get; set; }
    }
}
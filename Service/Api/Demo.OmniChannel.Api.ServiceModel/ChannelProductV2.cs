﻿using Demo.OmniChannel.ShareKernel.Dtos;
using ServiceStack;

namespace Demo.OmniChannel.Api.ServiceModel
{
    [Route("/channel-product-v2/push-product",
        "POST",
        Summary = "Đẩy sản phẩm lên sàn",
        Notes = "")]
    public class ProductDataPushChannelV2Req : IReturn<object>
    {
        public ProductDataPushChannel ProductDataPushChannel { get; set; }
    }
}
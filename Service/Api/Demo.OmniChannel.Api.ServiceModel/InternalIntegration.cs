﻿using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.ShareKernel.Dto;
using Demo.OmniChannel.ShareKernel.Dtos;
using ServiceStack;
using System.Collections.Generic;

namespace Demo.OmniChannel.Api.ServiceModel
{

    [Route("/internal/channel/updateNeedRegister", "POST")]
    public class InternalUpdateRegisterChannel : IReturn<object>
    {
        public long ChannelId { get; set; }
        public string GuidId { get; set; }
        public string ChannelName { get; set; }
    }

    [Route("/internal/deactivateChannel", "POST")]
    public class InternalDeactivateChannel : IReturn<object>
    {
        public long ChannelId { get; set; }
        public string ChannelName { get; set; }
        public string AuditContent { get; set; }
        public string GuidId { get; set; }
    }
    [Route("/internal/category/getByName", "POST")]
    public class InternalGetCategoryName : IReturn<object>
    {
        public string Name { get; set; }
        public bool IsParent { get; set; } = true;
    }

    [Route("/internal/channels", "POST")]
    public class GetChannelsById : IReturn<object>
    {
        public List<long> Ids { get; set; }
        public int RetailerId { get; set; }

    }

    [Route("/internal/category/createOrUpdateCategory", "POST")]
    public class InternalCreateOrUpdateCategory : IReturn<object>
    {
        public string Name { get; set; }
    }

    

    [Route("/internal/product/createdProduct", "POST")]
    public class InternalCreatedProduct : IReturn<object>
    {
        public ProductDto Product { get; set; }

        public List<ProductImageDto> ProductImages { get; set; }

        public PriceBookDto PriceBook { get; set; }
    }

    [Route("/internal/attribute/getByNames", "POST")]
    public class InternalAttributeGetByNames : IReturn<object>
    {
        public List<string> Names { get; set; }
    }

    [Route("/internal/attribute/batchAdd", "POST")]
    public class InternalAttributeBatchAdd : IReturn<object>
    {
        public List<string> Names { get; set; }
    }

    [Route("/internal/productAttribute/batchAdd", "POST")]
    public class InternalProductAttributeBatchAdd : IReturn<object>
    {
        public List<ProductAndProductAttributeDto> ProductAndAttributes { get; set; }
    }

    [Route("/internal/addauditlog", "POST")]
    public class InternalAddAuditLog : IReturn<object>
    {
        public AuditTrailLogDto Log { get; set; }
    }

    [Route("/internal/PosSetting/getSettings", "GET")]
    public class InternalGetSettings : IReturn<object>
    {
        
    }

    [Route("/internal/product/getProductByCodes", "GET")]
    public class InternalGetProductByCodes : IReturn<object>
    {
        public List<string> Codes { get; set; }
    }
    [Route("/internal/product/getprincechannelproduct", "GET")]
    public class InternalGetPrinceByChannelProduct : IReturn<object>
    {
        public long ChannelId { get; set; }
        public List<long> ProductIds { get; set; }
    }
    [Route("/internal/customers/findbysocialid", "GET")]
    public class FindCustomerBySocialId : IReturn<object>
    {
        public List<long> SocialIds { get; set; }
        public long PageIdFacebook { get; set; }
    }

    [Route("/internal/updateIcon", "POST")]
    public class UpdateIconChannel : IReturn<object>
    {
        public int RetailerId { get; set; }
        public long ChannelId { get; set; }
        public byte ChannelType { get; set; }
    }

    [Route("/internal/enableChannel", "POST")]
    public class EnableChannels : IReturn<object>
    {
        public List<long> ChannelIds { get; set; }
    }
    [Route("/internal/changeCache", "POST")]
    public class ChangeCache : IReturn<object>
    {
        public int RetailerId { get; set; }
        public bool IsActive { get; set; }
    }
    [Route("/internal/order/createOrder", "POST")]
    public class InternalCreatedrOder : IReturn<object>
    {
        public OrderDTO Order { get; set; }
    }
    [Route("/internal/customersocial", "POST")]
    public class CreateCustomerSocial : IReturn<object>
    {
        public CustomerDTO Customer { get; set; }
    }

}

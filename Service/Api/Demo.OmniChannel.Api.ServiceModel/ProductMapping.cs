﻿using System;
using System.Collections.Generic;
using Demo.OmniChannel.ShareKernel.Common;
using ServiceStack;

namespace Demo.OmniChannel.Api.ServiceModel
{
    [Route("/getproductmapping")]
    public class GetProductMapping : IReturn<List<object>>
    {
        public long? ChannelId { get; set; }
        public List<string> KvProductSkus { get; set; }
        public List<string> ChannelProductSkus { get; set; }
    }

    [Route("/getmapbychannel")]
    public class GetMappingByChannel : IReturn<object>
    {
        public long ChannelId { get; set; }
        public int BranchId { get; set; }
    }

    [Route("/product-map-by-channel-list")]
    public class GetProductMappingByChannelList : IReturn<object>
    {
        public IList<ChannelType> ChannelTypes { get; set; }
        public IList<long> ChannelIds { get; set; }
        public IList<int> BranchIds { get; set; }
    }

    [Route("/addproductmapping", "POST")]
    public class AddProductMapping : IReturn<object>
    {
        public MappingDto ProductMapping { get; set; }
    }

    [Route("/deleteproductmapping", "POST")]
    public class DeleteProductMapping : IReturn<object>
    {
        public long ChannelId { get; set; }
        public long KvProductId { get; set; }
        public string ChannelProductId { get; set; }
    }

    [Route("/getproductmapping", "POST")]
    public class GetProductMappingForward : IReturn<object>
    {
        public long ChannelId { get; set; }
        public long KvProductId { get; set; }
        public string ChannelProductId { get; set; }
    }

    [Route("/deleteproductmappingbyid", "DELETE")]
    public class DeleteProductMappingById : IReturn<object>
    {
        public object ProductMappingId { get; set; }
    }

    [Route("/deleteproductmappingsbykvproduct", "POST")]
    public class DeleteProductMappingsByKvProduct : IReturn<object>
    {
        public List<long> KvProductIds { get; set; }
    }
    [Route("/getmappingbychannelproductid")]
    public class GetMappingByChannelProductId : IReturn<object>
    {
        public long ChannelId { get; set; }
        public List<string> ChannelProductIds { get; set; }
    }

    public class KvProduct
    {
        public long KvProductId { get; set; }
        public string KvProductSku { get; set; }
        public string KvProductFullName { get; set; }
        public List<int> KvInActiveBranchIds { get; set; }

        public KvProduct()
        {
            KvInActiveBranchIds = new List<int>();
        }
    }

    public class MappingDto
    {
        public string Id { get; set; }
        public long ProductKvId { get; set; }
        public string ProductKvSku { get; set; }
        public string ProductKvFullName { get; set; }
        public int RetailerId { get; set; }
        public string RetailerCode { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProductChannelId { get; set; }
        public string ParentProductChannelId { get; set; }
        public string ProductChannelSku { get; set; }
        public string ProductChannelName { get; set; }
        public byte ProductChannelType { get; set; }
        public long ChannelId { get; set; }
        public string StrProductChannelId { get; set; }
        public string StrParentProductChannelId { get; set; }
        public long? SuperId { get; set; }

        public string CommonProductChannelId
        {
            get
            {
                return ProductChannelId ?? StrProductChannelId;
            }
            set
            {
                StrProductChannelId = value;
                ProductChannelId = value;
            }
        }

        public string CommonParentProductChannelId
        {
            get
            {
                return ParentProductChannelId ?? StrParentProductChannelId;
            }
            set
            {
                StrParentProductChannelId = value;
                ParentProductChannelId = value;
            }
        }
    }
}
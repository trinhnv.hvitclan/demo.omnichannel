﻿using System;
using System.Collections.Generic;
using Demo.OmniChannel.Domain.Common;
using Demo.OmniChannel.MongoDb;
using ServiceStack;

namespace Demo.OmniChannel.Api.ServiceModel
{
    [Route("/getproductbychannelid")]
    public class GetProductByChannelId : IReturn<PagingDataSource<object>>
    {
        public long ChannelId { get; set; }
        public int BranchId { get; set; }
        public int Top { get; set; }
        public int Skip { get; set; }
        public bool? IsConnected { get; set; }
        public bool? IsSyncSuccess { get; set; }
        public string ChannelTerm { get; set; }
        public string KvTerm { get; set; }
    }
    
    [Route("/product-by-kv-product-ids")]
    public class GetProductByKvProductIds : IReturn<IList<MapProduct>>
    {
        public IList<long> KvProductIds { get; set; }
    }
    
    [Route("/addproductandmapping", "POST")]
    public class AddProductAndMapping : IReturn<object>
    {
        public MapProduct Product { get; set; }
    }

    [Route("/getproductbysearch")]
    public class GetProductBySearch : IReturn<List<object>>
    {
        public long ChannelId { get; set; }
        public string Term { get; set; }
        public int Skip { get; set; }
        public int Top { get; set; }
    }
    [Route("/getproductbychannel")]
    public class GetProductByChannel : IReturn<object>
    {
        public long ChannelId { get; set; }
        public int BranchId { get; set; }
    }

    [Route("/getproductsyncerror")]
    public class GetProductSyncError : IReturn<List<object>>
    {
        public List<long> ChannelIds { get; set; }
        public int BranchId { get; set; }
        public string ProductSearchTerm { get; set; }
        public string ErrorType { get; set; }   // "OnHand"/"Price"
    }

    [Route("/getproductdetailfromchannel")]
    public class GetProductDetail : IReturn<object>
    {
        public object ProductId { get; set; }
        public string ProductSku { get; set; }
        public long ChannelId { get; set; }
        public object ParentProductId { get; set; }
    }

    [Route("/getquantityofproductsyncerror")]
    public class GetQuantityOfProductSyncError : IReturn<object>
    {
        public List<long> ChannelIds { get; set; }
    }

    [Route("/product-by-track-key")]
    public class GetProductByTrackingKey : IReturn<object>
    {
        public string TrackKey { get; set; }
        public long ChannelId { get; set; }
    }
    [Route("/channel-by-kv-product-ids")]
    public class GetChannelByKvProductIds : IReturn<object>
    {
        public List<long> KvProductIds { get; set; }
    }
    [Route("/getquantityofchannelsyncerror")]
    public class GetQuantityOfChannelSyncError : IReturn<object>
    {
        public List<long> ChannelIds { get; set; }
    }

    #region Response

    public class MapProduct
    {
        public string Id { get; set; }
        public string ItemId { get; set; }
        public string ParentItemId { get; set; }
        public string StrItemId { get; set; }
        public string StrParentItemId { get; set; }
        public string CommonItemId
        {
            get
            {
                return string.IsNullOrEmpty(StrItemId) ? ItemId : StrItemId;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    ItemId = value;
                    StrItemId = value;
                }
            }
        }

        public string CommonParentItemId 
        {
            get
            {
                return string.IsNullOrEmpty(StrParentItemId) ? ParentItemId : StrParentItemId;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    ParentItemId = value;
                    StrParentItemId = value;
                }
            }
        }

        public string ItemName { get; set; }
        public List<string> ItemImages { get; set; }
        public string ItemSku { get; set; }
        public string RetailerCode { get; set; }
        public int BranchId { get; set; }
        public byte ChannelType { get; set; }
        public long ChannelId { get; set; }
        public bool IsConnected { get; set; }
        public string Status { get; set; }
        public byte Type { get; set; }
        public int RetailerId { get; set; }
        public long KvProductId { get; set; }
        public string KvProductSku { get; set; }
        public string KvProductFullName { get; set; }
        public string ErrorMessage { get; set; }
        public string KvErrorMessage { get; set; }
        public List<SyncError> SyncErrors { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ShopId { get; set; }
        public long? SuperId { get; set; }
    }
    public class ProductChannel
    {
        public long KvProductId { get; set; }
        public List<string> ChannelTypes { get; set; }
    }
    public class ChannelSyncError
    {
        public long Total { get; set; }
        public long ChannelId { get; set; }
    }

    #endregion
}
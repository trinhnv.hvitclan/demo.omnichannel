﻿using Demo.OmniChannel.Domain.Common;
using Demo.OmniChannel.Sdk.RequestDtos;
using ServiceStack;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Demo.OmniChannel.Api.ServiceModel.Retailer
{
    [Route("/omnichannel/permission", "GET")]
    public class GetPermissionRequestByRetail : IReturn<object>
    {
        public int RetailerId { get; set; }
    }

    [Route("/omnichannel/addChannelPrecheck", "GET")]
    public class GetAddChannelPrecheckByRetail : IReturn<object>
    {
        public int RetailerId { get; set; }
        public int OmniChannelId { get; set; }
    }

    [Route("/omnichannel/getWarningExpiryOmniChannels", "GET")]
    public class GetWarningExpiryOmniChannelsByRetail : IReturn<object>
    {
    }

    [Route("/omnichannel/getQuantityOfProductSyncError", "GET")]
    public class GetQuantityOfProductSyncErrorByRetail : IReturn<object>
    {
        public byte? ChannelType { get; set; }
    }

    [Route("/omnichannel/getChannelProductDetail", "GET")]
    public class GetChannelProductDetailByRetail : IReturn<object>
    {
        public long ChannelId { get; set; }
        public string ProductId { get; set; }
        public string ProductSku { get; set; }
        public string ParentProductId { get; set; }
    }

    [Route("/omnichannel/getChannelOrderSyncError", "GET")]
    public class GetChannelOrderSyncErrorByRetail : IReturn<object>
    {
        public List<long> ChannelIds { get; set; }
        public List<int> BranchIds { get; set; }
        public string OrderCode { get; set; }
        public string OtherSearchTerm { get; set; }
        public int Skip { get; set; }
        public int Top { get; set; }
        public byte? ChannelType { get; set; }
        public string ChannelTypes { get; set; } // Shopee,Lazada,Sendo,Tiktok,Tiki phân tách bởi dấu ','
        public string KeySearchProduct { get; set; }
    }

    [Route("/omnichannel/getChannelInvoiceSyncError", "GET")]
    public class GetChannelInvoiceSyncErrorByRetail : IReturn<object>
    {
        public List<long> ChannelIds { get; set; }
        public List<int> BranchIds { get; set; }
        public string InvoiceCode { get; set; }
        public string OtherSearchTerm { get; set; }
        public int Skip { get; set; }
        public int Top { get; set; }
        public byte? ChannelType { get; set; }
        public string ChannelTypes { get; set; } // Shopee,Lazada,Sendo,Tiktok,Tiki phân tách bởi dấu ','
        public string KeySearchProduct { get; set; }
    }

    [Route("/omnichannel/getChannelProductSyncError", "GET")]
    public class GetChannelProductSyncErrorByRetail : IReturn<object>
    {
        public long[] ChannelIds { get; set; }
        public string ProductSearchTerm { get; set; }
        public int Skip { get; set; }
        public int Top { get; set; }
        public byte? ChannelType { get; set; }
        public string ChannelTypes { get; set; } // Shopee,Lazada,Sendo,Tiktok,Tiki phân tách bởi dấu ','
        public string ErrorType { get; set; }   // "OnHand"/"Price"
    }

    [Route("/omnichannel/product-channel-by-kv-product-ids", "GET")]
    public class GetProductChannelByKvProductIdsByRetail : IReturn<IList<ProductChannelByKvProductIdsRes>>
    {
        public IList<long> KvProductIds { get; set; }
    }

    [Route("/omnichannel/is-affect-on-hand-formula-ecommerce-channel", "GET")]
    public class GetIsAffectOnHandFormulaEcommerceChannelByRetail : IReturn<IsAffectOnHandFormulaEcommerceChannelRes>
    {
        public bool OffSellAllowOrder { get; set; }
        public bool OffUseOrderSupplier { get; set; }
    }

    [Route("/omnichannel/autocompletekvproduct", "GET")]
    public class KvProductSearchByRetail : IReturn<List<object>>
    {
        public long ChannelId { get; set; }

        [DataMember(Name = "term")]
        public string Term { get; set; }
    }

    [Route("/omnichannel/autocompletechannelproduct", "GET")]
    public class ChannelProductSearchByRetail : IReturn<List<ChannelProduct>>
    {
        public long ChannelId { get; set; }

        [DataMember(Name = "term")]
        public string Term { get; set; }
        public bool? IsMobile { get; set; }
        public int? Skip { get; set; }
        public int? Top { get; set; }
    }

    [Route("/omnichannel/getretailerchannels", "GET")]
    public class GetRetailerChannelByRetail : IReturn<List<object>>
    {
        public byte? Channeltype { get; set; }
        public string ChannelTypes { get; set; } // Shopee,Lazada,Sendo,Tiktok,Tiki phân tách bởi dấu ','
        public bool IncludeChannelInActive { get; set; }
    }

    [Route("/omnichannel/getkvproductbyid", "GET")]
    public class GetKvProductByIdByRetail : IReturn<object>
    {
        public long ProductId { get; set; }
        public int BranchId { get; set; }
    }

    [Route("/omnichannel/getmapproductbychannel", "GET")]
    public class GetMapProductByChannelByRetail : IReturn<object>
    {
        public long ChannelId { get; set; }
        public int BranchId { get; set; }
    }

    [Route("/omnichannel/getproductbychannelid", "GET")]
    public class GetProductByChannelIdByRetail : IReturn<PagingDataSource<ProductMapResponse>>
    {
        public long ChannelId { get; set; }
        public int BranchId { get; set; }
        public bool? IsConnected { get; set; }
        public bool? IsSyncSuccess { get; set; }
        public string ItemSku { get; set; }
        public byte ChannelType { get; set; }
        public string KvProductSku { get; set; }
        [DataMember(Name = "$skip")]
        public int? Skip { get; set; }

        [DataMember(Name = "$top")]
        public int? Top { get; set; }
    }

    [Route("/omnichannel/getproductbylistchannelid", "GET")]
    public class GetProductByListChannelId : IReturn<PagingDataSource<ProductMapResponse>>
    {
        public List<long> ChannelIdLst { get; set; }
        public bool? IsConnected { get; set; }
        public bool? IsSyncSuccess { get; set; }
        public string ItemSku { get; set; }
        public string KvProductSku { get; set; }
        [DataMember(Name = "$skip")]
        public int Skip { get; set; } = 0;

        [DataMember(Name = "$top")]
        public int Top { get; set; } = 20;
        public List<string> ItemSkuLst { get; set; }
    }

    [Route("/omnichannel/getQuantityOfOrderSyncError", "POST")]
    public class GetQuantityOfOrderSyncErrorByRetail : IReturn<object>
    {
        public byte? ChannelType { get; set; }
        public string ChannelTypes { get; set; } // Shopee,Lazada,Sendo,Tiktok,Tiki phân tách bởi dấu ','
    }

    [Route("/omnichannel/getQuantityOfInvoiceSyncError", "POST")]
    public class GetQuantityOfInvoiceSyncErrorByRetail : IReturn<object>
    {
        public byte? ChannelType { get; set; }
        public string ChannelTypes { get; set; } // Shopee,Lazada,Sendo,Tiktok,Tiki phân tách bởi dấu ','

    }

    [Route("/omnichannel/reSyncInvoiceChannelError", "POST")]
    public class SyncInvoiceChannelErrorByRetail : IReturn<object>
    {
        public InvoiceDto Invoice { get; set; }
    }

    [Route("/omnichannel/reSyncOrderChannelError", "POST")]
    public class SyncOrderChannelErrorByRetail : IReturn<object>
    {
        public OrderDto Order { get; set; }
        public long? ChannelId { get; set; }
    }

    [Route("/omnichannel/getSaleChannelOmniByType", "POST")]
    public class GetSaleChannelOmniByTypeByRetail : IReturn<object>
    {
        public List<int> ChannelTypeLst { get; set; }
    }

    [Route("/omnichannel/reSyncChannelError", "POST")]
    public class SyncChannelErrorByRetail : IReturn<object>
    {
        public long? ChannelId { get; set; }
        public List<string> ItemIds { get; set; }
        public List<InvoiceDto> Invoices { get; set; }
        public List<OrderDto> Orders { get; set; }
        public byte Type { get; set; }
    }

    public class OmniChannelSettingObjectDto
    {
        public long? ChannelId { get; set; }
        public long[] ProductIds { get; set; }
        public List<InvoiceDto> Invoices { get; set; }
        public List<OrderDto> Orders { get; set; }
        public byte Type { get; set; }
        public bool DistributeMultiMapping { get; set; }
    }
    [Route("/omnichannel/deleteProductMapping", "POST")]
    public class RemoveProductMappingByRetail : IReturn<object>
    {
        public long ChannelId { get; set; }
        public long KvProductId { get; set; }
        public string ChannelProductId { get; set; }
    }

    [Route("/omnichannel/addproductmapping", "POST")]
    public class AddProductMappingByRetail : IReturn<object>
    {
        public ProductMapping ProductMapping { get; set; }
    }

    [Route("/omnichannel/mappingproducts", "POST")]
    public class MappingProductsListByRetail : IReturn<object>
    {
        public long[] ProductIds { get; set; }
        [DataMember(Name = "$skip")]
        public int? Skip { get; set; }

        [DataMember(Name = "$top")]
        public int? Top { get; set; }
    }

    [Route("/omnichannel/addproductandmapping", "POST")]
    public class AddProductAndMappingByRetail : IReturn<object>
    {
        public ChannelProduct ChannelProduct { get; set; }
    }

    [Route("/omnichannel/deleteProductMappingbyid", "DELETE")]
    public class RemoveProductMappingByIdByRetail : IReturn<object>
    {
        public string ProductMappingId { get; set; }
    }

    [Route("/omnichannel/channels", "POST")]
    public class GetChannelsById : IReturn<object>
    {
        public List<long> Ids { get; set; }
        public int RetailerId { get; set; }
    }

    [Route("/omnichannel/getproductmapping", "POST")]
    public class GetProductMappingRequest : IReturn<object>
    {
        public long ChannelId { get; set; }

        public long KvProductId { get; set; }

        public string ChannelProductId { get; set; }
    }
    [Route("/omnichannel/getProductSyncError", "GET")]
    public class GetProductSyncErrorByRetail : IReturn<object>
    {
        public long[] ChannelIds { get; set; }
        public string ProductSearchTerm { get; set; }
        public int Skip { get; set; }
        public int Top { get; set; }
        public byte? ChannelType { get; set; }
        public string ChannelTypes { get; set; } // Shopee,Lazada,Sendo,Tiktok,Tiki phân tách bởi dấu ','
        public string ErrorType { get; set; }   // "OnHand"/"Price"
    }

    [Route("/omnichannel/resync-order", "POST")]
    public class SyncOrderChannelError : IReturn<object>
    {
        public string Id { get; set; }
        public long ChannelId { get; set; }
    }


    [Route("/omnichannel/resync-invoice", "POST")]
    public class SyncInvoiceChannelError : IReturn<object>
    {
        public string Id { get; set; }
        public long ChannelId { get; set; }
    }

    [Route("/omnichannel/resync-all", "POST")]
    public class ResyncChannelError : IReturn<object>
    {
        public bool IsResyncOrders { get; set; }
        public bool IsResyncInvoices { get; set; }
        public bool IsResyncProducts { get; set; }
    }

    [Route("/omnichannel/invoice/{Id}", "GET")]
    public class GetInvoiceDetail : IReturn<object>
    {
        public string Id { get; set; }
        public long ChannelId { get; set; }
    }

    [Route("/omnichannel/order/{Id}", "GET")]
    public class GetOrderDetail : IReturn<object>
    {
        public string Id { get; set; }
        public long ChannelId { get; set; }
    }

    [Route("/omnichannel/quantity-sync-error", "GET")]
    public class QuantitySyncError : IReturn<object>
    {
        public string ChannelTypes { get; set; } // Shopee,Lazada,Sendo,Tiktok,Tiki phân tách bởi dấu ','
        public string SyncTypes { get; set; } // "Invoice,Order,Product"
    }

    [Route("/omnichannel/mapping-kv-product", "POST")]
    public class MappingChannelProductAndKvProduct : IReturn<object>
    {
        public long ProductKvId { get; set; }
        public long ChannelId { get; set; }
        public string ProductChannelTrackKey { get; set; }
        public string OrderId { get; set; }
        public string InvoiceId { get; set; }
    }
    [Route("/omnichannel/getprincechannelproduct", "GET")]
    public class GetPrinceByChannelProduct : IReturn<object>
    {
        public long ChannelId { get; set; }
        public List<long> ProductIds { get; set; }
    }
    [Route("/omnichannel/getallpricebooks", "GET")]
    public class GetPriceBooks : IReturn<List<object>>
    {
    }
    public class OmniChannelExpireWarning
    {
        public string Type { get; set; }
        public int Total { get; set; }
        public int ExpiredAfterDay { get; set; }
        public bool IsExpired { get; set; }
    }

    public class OmniChannelExpiryDto
    {
        public List<OmniChannelExpireWarning> ExpiredChannels { get; set; }
        public List<OmniChannelExpireWarning> ChannelListIsAboutToExpire { get; set; }

        public OmniChannelExpiryDto()
        {
            ExpiredChannels = new List<OmniChannelExpireWarning>();
            ChannelListIsAboutToExpire = new List<OmniChannelExpireWarning>();
        }
    }

    public class ProductChannelByKvProductIdsRes
    {
        public long KvProductId { get; set; }
        public IList<ChannelProductRes> ChannelProducts { get; set; }
    }

    public class ChannelProductRes
    {
        public byte Type { get; set; }
        public string TypeName { get; set; }
        public long ChannelId { get; set; }
        public string ProductId { get; set; }
        public string ParentProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductSku { get; set; }
        public string EditProductUrl { get; set; }
    }

    public class IsAffectOnHandFormulaEcommerceChannelRes
    {
        public bool IsAffect { get; set; }
        public bool IsAffectSyncOrder { get; set; }
    }

    public class ProductMapResponse : ChannelProduct
    {
        public string AttributeLabel { get; set; }
        public string Unit { get; set; }
        public decimal BasePrice { get; set; }
        public decimal Price { get; set; }
        public double OnHand { get; set; }
        public double OnOrder { get; set; }
        public double Reserved { get; set; }
        public string KvProductName { get; set; }
        public string EditChannelUrl { get; set; }
        public byte? ProductType { get; set; }
    }
}

﻿using Demo.OmniChannel.Sdk.RequestDtos;
using ServiceStack;

namespace Demo.OmniChannel.Api.ServiceModel.Retailer
{
    [Route("/channel/categories", "GET")]
    public class GetCategoriesReqByRetail : IReturn<object>
    {
        public long ChannelId { get; set; }
    }

}

﻿using Demo.OmniChannel.Domain.Common;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.ShareKernel.Dto;
using ServiceStack;
using System;
using System.Collections.Generic;
using OmniChannelSchedule = Demo.OmniChannel.Domain.Model.OmniChannelSchedule;
using Demo.OmniChannel.Api.ServiceModel.Request;

namespace Demo.OmniChannel.Api.ServiceModel.Retailer
{
    [Route("/tiktoks", "GET")]
    public class TiktokAppByRetail : IReturn<PagingDataSource<TiktokResponse>>
    {
    }

    [Route("/tiktok/{Id}", "GET")]
    [Route("/tiktok/{Id}", "DELETE")]
    public class TiktokGetByRetail : IReturn<object>
    {
        public long Id { get; set; }
    }

    [Route("/tiktok/enable-channel", "POST")]
    public class TiktokEnableChannelByRetail : Sdk.RequestDtos.EnableChannel
    {
        public long ShopId { get; set; }
        public string IdentityKey { get; set; }
        public string Code { get; set; }
    }

    [Route("/tiktok", "POST")]
    public class TiktokCreateOrUpdateByRetail : IReturn<TiktokResponse>
    {
        public OmniChannelRequest TiktokApp { get; set; }
        public bool IsSyncOrder { get; set; }
        public bool IsSyncOnHand { get; set; }
        public bool IsSyncPrice { get; set; }
        public bool IsAutoMappingSKU { get; set; }
        public long AuthId { get; set; }
    }

    public class TiktokResponse
    {
        public int Id { get; set; }
        public long? ShopId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public byte Type { get; set; }
        public int? BranchId { get; set; }
        public string BranchName { get; set; }
        public int RetailerId { get; set; }
        public bool IsActive { get; set; }
        public long? BasePriceBookId { get; set; }
        public string BasePriceBookName { get; set; }
        public bool IsSyncOrder { get; set; }
        public bool IsSyncOnHand { get; set; }
        public bool IsSyncPrice { get; set; }
        public bool IsAutoMappingSKU { get; set; }
        public bool? IsAutoDeleteMapping { get; set; }
        public byte SyncOnHandFormula { get; set; }
        public string SyncOnHandFormulaName { get; set; }
        public bool IsAutoMappingProduct { get; set; }
        public bool IsApplyAllFormula { get; set; }
        public OmniChannelAuth OmniChannelAuth { get; set; }
        public List<OmniChannelSchedule> OmniChannelSchedules { get; set; }
        public OmniChannelSettingDto OmniChannelSettings { get; set; }
        public long TotalProductConnected { get; set; }
        public long PlatformId { get; set; }
        public string IdentityKey { get; set; }
        public long? SaleChannelId { get; set; }
        public int? SyncOrderFormula { get; set; }
        public bool IsExpireWarning { get; set; }
        public DateTime? RegisterDate { get; set; }
        public bool IsRunningGetProduct { get; set; } = false;
    }
}

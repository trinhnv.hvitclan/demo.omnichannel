﻿using Demo.OmniChannel.Domain.Common;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.ShareKernel.Dto;
using ServiceStack;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Demo.OmniChannel.Api.ServiceModel.Request;

namespace Demo.OmniChannel.Api.ServiceModel.Retailer
{
    [Route("/lazadas", "GET")]
    public class LazadaApps : IReturn<PagingDataSource<LazadaResponse>>
    {
    }

    [Route("/lazada/{Id}", "GET")]
    [Route("/lazada/{Id}", "DELETE")]
    public class LazadaAppGet : IReturn<object>
    {
        public long Id { get; set; }
    }

    [Route("/lazada", "POST")]
    public class LazadaAppPost : IReturn<LazadaResponse>
    {
        public OmniChannelRequest LazadaApp { get; set; }
        public bool IsSyncOrder { get; set; }
        public bool IsSyncOnHand { get; set; }
        public bool IsSyncPrice { get; set; }
        public long AuthId { get; set; }
    }

    [Route("/lazada/auth-config", "GET")]
    public class LazadaAuthConfig : IReturn<object>
    {

    }

    [Route("/lazada/lazadaprofile", "GET")]
    public class LazadaUpdateLinking : IReturn<object>
    {
        public string Code { get; set; }
    }

    [Route("/lazada/remove-auth", "POST")]
    public class LazadaRemoveAuth : IReturn<object>
    {
        public long AuthId { get; set; }
    }

    [Route("/lazada/enable-channel", "POST")]
    public class LazadaEnableChannel : EnableChannel, IReturn<object>
    {
        public string Code { get; set; }
        public string IdentityKey { get; set; }
    }

    public class LazadaAuthConfigResponse
    {
        [DataMember(Name = "ClientId")]
        public string ClientId { get; set; }
        [DataMember(Name = "CallBackUrl")]
        public string CallBackUrl { get; set; }
    }


    public class LazadaShopInfoResponseV2
    {
        [DataMember(Name = "AuthId")]
        public long AuthId { get; set; }
        [DataMember(Name = "Name")]
        public string Name { get; set; }
        [DataMember(Name = "IsActive")]
        public bool IsActive { get; set; }
        [DataMember(Name = "Email")]
        public string Email { get; set; }
        [DataMember(Name = "IdentityKey")]
        public string IdentityKey { get; set; }
        [DataMember(Name = "Status")]
        public byte Status { get; set; }
        [DataMember(Name = "Active")]
        public byte Active { get; set; }
    }


    public class LazadaResponse
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int? BranchId { get; set; }
        public byte Type { get; set; }
        public long? BasePriceBookId { get; set; }
        public string BasePriceBookName { get; set; }
        public string BranchName { get; set; }
        public int RetailerId { get; set; }
        public bool IsActive { get; set; }
        public long? PriceBookId { get; set; }
        public string PriceBookName { get; set; }
        public bool IsSyncOrder { get; set; }
        public bool IsSyncOnHand { get; set; }
        public bool IsSyncPrice { get; set; }
        public bool IsAutoMappingSKU { get; set; }
        public byte SyncOnHandFormula { get; set; }
        public string SyncOnHandFormulaName { get; set; }
        public bool IsAutoMappingProduct { get; set; }
        public bool? IsAutoDeleteMapping { get; set; }
        public bool IsApplyAllFormula { get; set; }
        public long TotalProductConnected { get; set; }
        public OmniChannelAuth OmniChannelAuth { get; set; }
        public List<Domain.Model.OmniChannelSchedule> OmniChannelSchedules { get; set; }
        public OmniChannelSettingDto OmniChannelSettings { get; set; }
        public string IdentityKey { get; set; }
        public long? SaleChannelId { get; set; }
        public int? SyncOrderFormula { get; set; }
        public bool IsRunningGetProduct { get; set; } = false;

    }
}

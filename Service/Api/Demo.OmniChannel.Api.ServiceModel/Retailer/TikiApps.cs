﻿using Demo.OmniChannel.Domain.Common;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.ShareKernel.Dto;
using ServiceStack;
using System.Collections.Generic;

namespace Demo.OmniChannel.Api.ServiceModel.Retailer
{
    [Route("/tikis", "GET")]
    public class TikiApps : IReturn<PagingDataSource<TikiResponse>>
    {
    }

    [Route("/tiki/getWareHouses", "POST")]
    public class GetWareHouses : IReturn<object>
    {
        public long OmniAuthenId { get; set; }
    }

    [Route("/tiki/{Id}", "GET")]
    [Route("/tiki/{Id}", "DELETE")]
    public class TikiAppGet : IReturn<object>
    {
        public long Id { get; set; }
    }
    [Route("/tiki", "POST")]
    public class TikiAppPost : IReturn<TikiResponse>
    {
        public OmniChannelDto TikiApp { get; set; }
        public bool IsSyncOrder { get; set; }
        public bool IsSyncOnHand { get; set; }
        public bool IsSyncPrice { get; set; }
        public long AuthId { get; set; }
    }
    [Route("/tiki/auth-config", "GET")]
    public class TikiAuthConfig : IReturn<object>
    {

    }
    [Route("/tiki/profile", "GET")]
    public class TikiProfile : IReturn<object>
    {
        public string Code { get; set; }
    }
    [Route("/tiki/remove-auth", "POST")]
    public class TikiRemoveAuth : IReturn<object>
    {
        public long AuthId { get; set; }
    }
    [Route("/tiki/sellerregister", "POST")]
    public class TikiSellerRegister : IReturn<object>
    {
        public string SellerId { get; set; }
        public string StoreName { get; set; }
    }
    [Route("/tiki/enable-channel", "POST")]
    public class TikiEnableChannel : EnableChannel, IReturn<object>
    {
        public string Code { get; set; }
        public string IdentityKey { get; set; }
    }

    public class TikiResponse
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int? BranchId { get; set; }
        public byte Type { get; set; }
        public byte Status { get; set; }
        public long? BasePriceBookId { get; set; }
        public string BasePriceBookName { get; set; }
        public string BranchName { get; set; }
        public int RetailerId { get; set; }
        public bool IsActive { get; set; }
        public long? PriceBookId { get; set; }
        public string PriceBookName { get; set; }
        public bool IsSyncOrder { get; set; }
        public bool IsSyncOnHand { get; set; }
        public bool IsSyncPrice { get; set; }
        public bool IsAutoMappingSKU { get; set; }
        public bool? IsAutoDeleteMapping { get; set; }
        public byte SyncOnHandFormula { get; set; }
        public string SyncOnHandFormulaName { get; set; }
        public bool IsAutoMappingProduct { get; set; }
        public bool IsApplyAllFormula { get; set; }
        public long TotalProductConnected { get; set; }
        public OmniChannelAuth OmniChannelAuth { get; set; }
        public List<Domain.Model.OmniChannelSchedule> OmniChannelSchedules { get; set; }
        public OmniChannelSettingDto OmniChannelSettings { get; set; }
        public List<Domain.Model.OmniChannelWareHouse> OmniChannelWareHouses { get; set; }
        public string IdentityKey { get; set; }
        public long? SaleChannelId { get; set; }
        public int? SyncOrderFormula { get; set; }
        public bool IsRunningGetProduct { get; set; } = false;

    }
}

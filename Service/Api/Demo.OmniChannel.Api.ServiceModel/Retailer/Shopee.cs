﻿using Demo.OmniChannel.Domain.Common;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.ShareKernel.Dto;
using ServiceStack;
using System;
using System.Collections.Generic;
using Demo.OmniChannel.Api.ServiceModel.Request;

namespace Demo.OmniChannel.Api.ServiceModel
{

    [Route("/shopees", "GET")]
    public class ShopeeApps : IReturn<PagingDataSource<ShopeeResponse>>
    {
    }

    [Route("/shopee/profile-v2", "GET")]
    public class ShopeeProfileAndUpdateAuth : IReturn<object>
    {
        public long ShopId { get; set; }
        public string Code { get; set; }
        public long PlatformId { get; set; }
        public long OmniChannelId { get; set; }
    }

    [Route("/shopee/auth-config-v2", "GET")]
    public class ShopeeAuthConfigV2 : IReturn<object>
    {

    }

    [Route("/shopee/profile-and-token", "GET")]
    public class ShopeeProfileAndToken : IReturn<object>
    {
        public long PlatformId { get; set; }
        public string Code { get; set; }
        public long ShopId { get; set; }
        public string IdentityKey { get; set; }
        public string ShopName { get; set; }
    }

    [Route("/shopee/logistics", "GET")]
    public class GetShopeeLogistics : IReturn<object>
    {
        public List<long> ChannelIds { get; set; }
    }

    [Route("/shopee/attributes", "GET")]
    public class GetShopeeAttributes : IReturn<object>
    {
        public long ChannelId { get; set; }
        public long CategoryId { get; set; }
    }

    [Route("/shopee/{Id}", "GET")]
    [Route("/shopee/{Id}", "DELETE")]
    public class ShopeeGet : IReturn<object>
    {
        public long Id { get; set; }
    }

    [Route("/shopee/shop-list", "GET")]
    public class ShopeeShopList : IReturn<List<ShopeeResponse>>
    {
        public bool IncludeBranch { get; set; }
        public bool IsActive { get; set; }
    }

    [Route("/shopee", "POST")]
    public class ShopeeCreateOrUpdate : IReturn<ShopeeResponse>
    {
        public OmniChannelRequest ShopeeApp { get; set; }
        public bool IsSyncOrder { get; set; }
        public bool IsSyncOnHand { get; set; }
        public bool IsSyncPrice { get; set; }
        public bool IsAutoMappingSKU { get; set; }
        public long AuthId { get; set; }
    }

    [Route("/shopee/remove-auth", "POST")]
    public class ShopeeRemoveAuth : IReturn<object>
    {
        public long AuthId { get; set; }
    }

    [Route("/shopee/enable-channel-v2", "POST")]
    public class ShopeeEnableChannelV2 : EnableChannel, IReturn<object>
    {
        public long ShopId { get; set; }
        public string IdentityKey { get; set; }
        public string Code { get; set; }
    }

    [Route("/shopee/brands", "GET")]
    public class GetBrandReq : IReturn<object>
    {
        public long ChannelId { get; set; }
        public int CategoryId { get; set; }
        public int NextOffSet { get; set; }
    }


    public class ShopeeResponse
    {
        public int Id { get; set; }
        public long? ShopId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public byte Type { get; set; }
        public int? BranchId { get; set; }
        public string BranchName { get; set; }
        public int RetailerId { get; set; }
        public bool IsActive { get; set; }
        public long? BasePriceBookId { get; set; }
        public string BasePriceBookName { get; set; }
        public bool IsSyncOrder { get; set; }
        public bool IsSyncOnHand { get; set; }
        public bool IsSyncPrice { get; set; }
        public bool IsAutoMappingSKU { get; set; }
        public bool? IsAutoDeleteMapping { get; set; }
        public byte SyncOnHandFormula { get; set; }
        public string SyncOnHandFormulaName { get; set; }
        public bool IsAutoMappingProduct { get; set; }
        public bool IsApplyAllFormula { get; set; }
        public OmniChannelAuth OmniChannelAuth { get; set; }
        public List<Domain.Model.OmniChannelSchedule> OmniChannelSchedules { get; set; }
        public OmniChannelSettingDto OmniChannelSettings { get; set; }
        public long TotalProductConnected { get; set; }
        public long PlatformId { get; set; }
        public string IdentityKey { get; set; }
        public long? SaleChannelId { get; set; }
        public int? SyncOrderFormula { get; set; }
        public bool IsExpireWarning { get; set; }
        public DateTime? RegisterDate { get; set; }
        public bool IsRunningGetProduct { get; set; } = false;
        public int Status { get; set; }
    }
}
﻿using Demo.OmniChannel.Domain.Common;
using Demo.OmniChannel.Domain.Model;
using Demo.OmniChannel.ShareKernel.Dto;
using ServiceStack;
using System.Collections.Generic;
using Demo.OmniChannel.Api.ServiceModel.Request;

namespace Demo.OmniChannel.Api.ServiceModel.Retailer
{
    [Route("/sendos", "GET")]
    public class SendoApps : IReturn<PagingDataSource<SendoResponse>>
    {
    }

    [Route("/sendo/{Id}", "GET")]
    [Route("/sendo/{Id}", "DELETE")]
    public class SendoAppGet : IReturn<object>
    {
        public long Id { get; set; }
    }

    [Route("/sendo", "POST")]
    public class SendoAppPost : IReturn<SendoResponse>
    {
        public OmniChannelRequest SendoApp { get; set; }
        public bool IsSyncOrder { get; set; }
        public bool IsSyncOnHand { get; set; }
        public bool IsSyncPrice { get; set; }
        public long AuthId { get; set; }
    }

    [Route("/sendo/auth-config", "GET")]
    public class SendoAuthConfig : IReturn<object>
    {

    }

    [Route("/sendo/create-auth", "POST")]
    public class SendoCreateAuth : IReturn<object>
    {
        public long ChannelId { get; set; }
        public string ChannelName { get; set; }
        public string ShopKey { get; set; }
        public string SecretKey { get; set; }
    }

    [Route("/sendo/remove-auth", "POST")]
    public class SendoRemoveAuth : IReturn<object>
    {
        public long AuthId { get; set; }
    }

    [Route("/sendo/enable-channel", "POST")]
    public class SendoEnableChannel : EnableChannel, IReturn<object>
    {
        public long AuthId { get; set; }
    }

    public class SendoResponse
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int? BranchId { get; set; }
        public byte Type { get; set; }
        public long? BasePriceBookId { get; set; }
        public string BasePriceBookName { get; set; }
        public string BranchName { get; set; }
        public int RetailerId { get; set; }
        public bool IsActive { get; set; }
        public long? PriceBookId { get; set; }
        public string PriceBookName { get; set; }
        public bool IsSyncOrder { get; set; }
        public bool IsSyncOnHand { get; set; }
        public bool IsSyncPrice { get; set; }
        public bool IsAutoMappingSKU { get; set; }
        public bool? IsAutoDeleteMapping { get; set; }
        public byte SyncOnHandFormula { get; set; }
        public string SyncOnHandFormulaName { get; set; }
        public bool IsAutoMappingProduct { get; set; }
        public bool IsApplyAllFormula { get; set; }
        public long TotalProductConnected { get; set; }
        public OmniChannelAuth OmniChannelAuth { get; set; }
        public List<Domain.Model.OmniChannelSchedule> OmniChannelSchedules { get; set; }
        public OmniChannelSettingDto OmniChannelSettings { get; set; }
        public string IdentityKey { get; set; }
        public long? SaleChannelId { get; set; }
        public int? SyncOrderFormula { get; set; }
        public string ExtraKey { get; set; }
        public bool IsRunningGetProduct { get; set; } = false;
    }
}

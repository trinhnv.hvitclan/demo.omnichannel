﻿namespace Demo.Mapping.Sdk.Common
{
    public class MappingInternalHeader
    {
        public string RetailerCode { get; set; }
        public string Token { get; set; }
        public string BranchId { get; set; }
    }
}

﻿using ServiceStack.Configuration;

namespace Demo.Mapping.Sdk.Common
{
    public static class MappingAppConfig
    {
        private static IAppSettings _settings;
        public static void InitAppSetting(IAppSettings appSettings)
        {
            _settings = appSettings;
        }

        public static string MappingApiDomain => _settings.GetString("MappingApiDomain");
        public static int MappingApiTimeout => _settings.Get("MappingApiTimeout", 100);

        public static string AddProductAndMappingEndpoint =>
            _settings.Get("AddProductAndMappingEndpoint", "/addproductandmapping");

        public static string DeleteProductMappingEndpoint =>
            _settings.Get("DeleteProductMappingEndpoint", "/deleteproductmapping");
        public static string AddProductMappingEndpoint =>
            _settings.Get("AddProductMappingEndpoint", "/addproductmapping");
        public static string DeleteProductMappingsByKvProductEndpoint =>
            _settings.Get("DeleteProductMappingsByKvProductEndpoint", "/deleteproductmappingsbykvproduct");
        public static string DeleteProductMappingByIdEndpoint =>
            _settings.Get("DeleteProductMappingByIdEndpoint", "/deleteproductmappingbyid");
    }
}

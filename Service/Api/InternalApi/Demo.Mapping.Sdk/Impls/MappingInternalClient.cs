﻿using Demo.Mapping.Sdk.Common;
using Demo.Mapping.Sdk.Interfaces;
using ServiceStack.Configuration;
using System;
using System.Threading.Tasks;

namespace Demo.Mapping.Sdk.Impls
{
    public class MappingInternalClient : BaseInternalClient, IMappingInternalClient
    {
        public MappingInternalClient(IAppSettings settings) : base(settings)
        {
        }

        public async Task<string> AddProductAndMapping(object requestObj, MappingInternalHeader header)
        {
            return await PostAsync(requestObj, header);
        }

        public async Task<string> AddProductMapping(object requestObj, MappingInternalHeader header)
        {
            return await PostAsync(requestObj, header);
        }

        public async Task<string> DeleteProductMapping(object requestObj, MappingInternalHeader header)
        {
            return await PostAsync(requestObj, header);
        }

        public Task<string> DeleteProductMappingsByKvProduct(object requestObj, MappingInternalHeader header)
        {
            throw new NotImplementedException();
        }

        public async Task<string> DeleteProductMappingById(object requestObj, MappingInternalHeader header)
        {
            var result = await DeleteAsync<string>(null, requestObj, header);
            return result;
        }

        public async Task<object> GetProductMapping(object requestObj, MappingInternalHeader header)
        {
            return await PostAsync(requestObj, header);
        }

        private async Task<string> PostAsync(object requestObj, MappingInternalHeader header)
        {
            var result = await PostAsync<string>(null, requestObj, header);
            return result;
        }
    }
}

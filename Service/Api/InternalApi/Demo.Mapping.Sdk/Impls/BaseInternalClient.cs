﻿using Demo.Mapping.Sdk.Common;
using Demo.Mapping.Sdk.Interfaces;
using ServiceStack;
using ServiceStack.Configuration;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Demo.Mapping.Sdk.Impls
{
    public class BaseInternalClient : IDisposable, IBaseInternalClient
    {
        public BaseInternalClient(IAppSettings settings)
        {
            MappingAppConfig.InitAppSetting(settings);
        }
        protected JsonHttpClient Client;

        protected void InitClient(string endPoint, MappingInternalHeader header)
        {
            var url = MappingAppConfig.MappingApiDomain;
            if (endPoint != null) url = $"{url}{endPoint}";
            Client = new JsonHttpClient(url);
            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
            if (header != null)
            {
                Client.AddHeader("Retailer", header.RetailerCode);
                Client.AddHeader("BranchId", header.BranchId);
                Client.BearerToken = header.Token;
                var httpClient = Client.GetHttpClient();
                httpClient.Timeout = TimeSpan.FromSeconds(MappingAppConfig.MappingApiTimeout);
                Client.HttpClient = httpClient;
            }
        }

        public Task<T> GetAsync<T>(string endPoint, object requestDto, MappingInternalHeader header)
        {
            InitClient(endPoint, header);

            return Client.GetAsync<T>(requestDto);
        }

        public Task<List<T>> GetListAsync<T>(string endPoint, object requestDto, MappingInternalHeader header)
        {
            InitClient(endPoint, header);

            return Client.GetAsync<List<T>>(requestDto);
        }

        public Task<T> PostAsync<T>(string endPoint, object bodyRequest, MappingInternalHeader header)
        {
            InitClient(endPoint, header);

            return Client.PostAsync<T>(bodyRequest);
        }

        public Task<List<T>> PostListAsync<T>(string endPoint, object requestDto, MappingInternalHeader header)
        {
            InitClient(endPoint, header);

            return Client.PostAsync<List<T>>(requestDto);
        }

        public Task<T> DeleteAsync<T>(string endPoint, object id, MappingInternalHeader header)
        {
            InitClient(endPoint, header);

            return Client.DeleteAsync<T>(id);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            Client?.Dispose();
        }
    }
}

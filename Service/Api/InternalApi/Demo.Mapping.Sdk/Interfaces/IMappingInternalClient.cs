﻿using Demo.Mapping.Sdk.Common;
using System.Threading.Tasks;

namespace Demo.Mapping.Sdk.Interfaces
{
    public interface IMappingInternalClient : IBaseInternalClient
    {
        Task<string> AddProductAndMapping(object requestObj, MappingInternalHeader header);
        Task<string> AddProductMapping(object requestObj, MappingInternalHeader header);

        Task<string> DeleteProductMapping(object requestObj, MappingInternalHeader header);
        Task<string> DeleteProductMappingsByKvProduct(object requestObj, MappingInternalHeader header);
        Task<string> DeleteProductMappingById(object requestObj, MappingInternalHeader header);
        Task<object> GetProductMapping(object requestObj, MappingInternalHeader header);
    }
}

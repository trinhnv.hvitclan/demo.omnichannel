﻿using Demo.Mapping.Sdk.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.Mapping.Sdk.Interfaces
{
    public interface IBaseInternalClient
    {
        Task<T> GetAsync<T>(string endPoint, object requestDto, MappingInternalHeader header);
        Task<List<T>> GetListAsync<T>(string endPoint, object requestDto, MappingInternalHeader header);
        Task<T> PostAsync<T>(string endPoint, object bodyRequest, MappingInternalHeader header);
        Task<List<T>> PostListAsync<T>(string endPoint, object requestDto, MappingInternalHeader header);
        Task<T> DeleteAsync<T>(string endPoint, object id, MappingInternalHeader header);
    }
}

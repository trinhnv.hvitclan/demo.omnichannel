﻿using System;

namespace Demo.OmniChannel.Sdk.OmniExceptions
{
    public class OmniException: Exception
    {
        public OmniException(string msg)
            : base(msg)
        {
        }
    }
}

﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Interfaces;
using Demo.OmniChannel.Sdk.RequestDtos;
using ServiceStack;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.Sdk.Impls
{
    public class ProductMappingInternalClient : BaseChannelInternalClient, IProductMappingInternalClient
    {
        public ProductMappingInternalClient(IAppSettings settings) : base(settings)
        {
        }

        public async Task<List<ProductMapping>> GetProductMappingAsync(KvInternalContext context, long? channelId, List<string> kvProductSkus, List<string> channelProductSku)
        {
            var objReq = new GetProductMapping
            {
                ChannelId = channelId,
                KvProductSkus = kvProductSkus,
                ChannelProductSkus = channelProductSku
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.PostAsync<List<ProductMapping>>(objReq);
        }
        public async Task<List<ChannelProduct>> GetMapProductByChannel(KvInternalContext context, long channelId, int branchId)
        {
            var objReq = new GetMappingByChannel
            {
                ChannelId = channelId,
                BranchId = branchId
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.GetAsync<List<ChannelProduct>>(objReq);
        }

        public async Task<List<ProductMapping>> GetMapProductByChannel(KvInternalContext context, IList<ChannelType> channelTypes, IList<long> channelIds, IList<int> branchIds)
        {
            var requestDto = new GetProductMappingByChannelList
            {
                ChannelTypes = channelTypes,
                ChannelIds = channelIds,
                BranchIds = branchIds
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.PostAsync<List<ProductMapping>>(requestDto);
        }

        public async Task<ProductMapping> AddProductMappingAsync(KvInternalContext context, ProductMapping productMapping)
        {
            var objReq = new AddProductMapping
            {
                ProductMapping = productMapping
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.PostAsync<ProductMapping>(objReq);
        }

        public async Task<ProductMapping> DeleteProductMappingAsync(KvInternalContext context, long channelId,  long kvProductId, string channelProductId)
        {
            var objReq = new DeleteProductMapping
            {
                ChannelId = channelId,
                KvProductId = kvProductId,
                ChannelProductId = channelProductId
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.PostAsync<ProductMapping>(objReq);
        }
        public async Task<bool> DeleteProductMappingAsync(KvInternalContext context, string productMappingId)
        {
            var objReq = new DeleteProductMappingById
            {
                ProductMappingId = productMappingId
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.DeleteAsync<bool>(objReq);
        }

        public async Task<int> DeleteProductMappingsAsync(KvInternalContext context, List<long> kvProductIds)
        {
            var objReq = new DeleteProductMappingsByKvProduct
            {
                KvProductIds = kvProductIds
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.PostAsync<int>(objReq);
        }

        public async Task<List<ProductMapping>> GetByChannelProductIds(KvInternalContext context, long channelId, List<string> channelProductIds)
        {
            var objReq = new GetMappingByChannelProductId
            {
                ChannelId = channelId,
                ChannelProductIds = channelProductIds
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.PostAsync<List<ProductMapping>>(objReq);
        }

        public async Task<int> DeleteMappingProductByIdsTask(KvInternalContext context, List<long> kvProductIds)
        {
            using (var cts = new CancellationTokenSource(3000))
            {
                var objReq = new DeleteProductMappingsByKvProduct
                {
                    KvProductIds = kvProductIds
                };

                InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
                return await Client.PostAsync<int>(objReq, cts.Token);
            }
        }
    }
}

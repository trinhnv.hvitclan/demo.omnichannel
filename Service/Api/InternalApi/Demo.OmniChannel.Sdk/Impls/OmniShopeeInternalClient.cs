﻿using System.Threading.Tasks;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Dtos;
using Demo.OmniChannel.Sdk.Interfaces;
using Demo.OmniChannel.Sdk.RequestDtos;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.Sdk.Impls
{
    public class OmniShopeeInternalClient : BaseChannelInternalClient, IOmniShopeeInternalClient
    {
        public OmniShopeeInternalClient(IAppSettings settings) : base(settings)
        {

        }
        public async Task<object> GetAuthConfigV2(KvInternalContext context, ShopeeAuthConfigV2 param)
        {
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await Client.GetAsync(param);
        }

        public async Task<ShopeeProfileResponse> GetProfileAndUpdateAuth(KvInternalContext context, ShopeeProfileAndUpdateAuth param)
        {
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await Client.GetAsync<ShopeeProfileResponse>(param);
        }

        public async Task<ProfileAndTokenResp> GetProfileAndToken(KvInternalContext context, ShopeeProfileAndToken param)
        {
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await Client.GetAsync<ProfileAndTokenResp>(param);
        }
    }
}
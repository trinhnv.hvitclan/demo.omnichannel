﻿using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Interfaces;
using Demo.OmniChannel.Sdk.RequestDtos;
using RestSharp;
using ServiceStack;
using ServiceStack.Configuration;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Sdk.Impls
{
    public class ChannelProductInternalClient : BaseChannelInternalClient, IChannelProductInternalClient
    {
        public ChannelProductInternalClient(IAppSettings settings) : base(settings)
        {
        }
        public async Task<PagingDataSource<ChannelProduct>> GetProductsByChannelId(
            KvInternalContext context, long channelId, int branchId, int top, int skip,
            string channelTerm, string kvTerm,
            bool? isConnected = null, bool? isSyncSuccess = null)
        {
            var getProductsByChannelId = new GetProductByChannelId
            {
                ChannelId = channelId,
                BranchId = branchId,
                Skip = skip,
                Top = top,
                IsConnected = isConnected,
                IsSyncSuccess = isSyncSuccess,
                ChannelTerm = channelTerm,
                KvTerm = kvTerm
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.GetAsync<PagingDataSource<ChannelProduct>>(getProductsByChannelId);
        }

        public async Task<IList<ChannelProduct>> GetProductByKvProductIds(
            KvInternalContext context, IList<long> kvProductIds)
        {
            var dto = new GetProductByKvProductIds
            {
                KvProductIds = kvProductIds
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.PostAsync<IList<ChannelProduct>>(dto);
        }

        public async Task<bool> AddProductAndMapping(KvInternalContext context, object bodyRequest)
        {
            var addMappingProduct = new AddProductAndMapping
            {
                Product = bodyRequest.ConvertTo<ChannelProduct>()
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.PostAsync<bool>(addMappingProduct);
        }

        public async Task<List<ChannelProduct>> GetProductBySearchTerm(KvInternalContext context, long channelId, string term, int skip = 0, int top = 0)
        {
            var searchProduct = new GetProductBySearch
            {
                ChannelId = channelId,
                Term = term,
                Skip = skip,
                Top = top
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.PostAsync<List<ChannelProduct>>(searchProduct);
        }

        public async Task<List<ChannelProduct>> GetProductByChannel(KvInternalContext context, long channelId, int branchId)
        {
            var objReq = new GetProductByChannel
            {
                ChannelId = channelId,
                BranchId = branchId
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.PostAsync<List<ChannelProduct>>(objReq);
        }

        public async Task<List<ChannelProduct>> GetProductSyncError(KvInternalContext context, List<long> channelIds, int branchId, string productSearchTerm, string errorType = null)
        {
            var objReq = new GetProductSyncError
            {
                ChannelIds = channelIds,
                BranchId = branchId,
                ProductSearchTerm = productSearchTerm,
                ErrorType = errorType
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.PostAsync<List<ChannelProduct>>(objReq);
        }

        public async Task<ChannelProductDetail> GetProductDetailFormChannel(KvInternalContext context, long channelId, string productId, string parentProductId, string productSku = null)
        {
            var objReq = new GetProductDetail
            {
                ChannelId = channelId,
                ProductId = productId,
                ProductSku = productSku,
                ParentProductId = parentProductId
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.PostAsync<ChannelProductDetail>(objReq);
        }

        public async Task<long> GetProductDetailFormChannel(KvInternalContext context, List<long> channelIds)
        {
            var objReq = new GetQuantityOfProductSyncError
            {
                ChannelIds = channelIds
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.PostAsync<long>(objReq);
        }

        public async Task<IRestResponse> ListenProxyProductSubscribe(object bodyRequest)
        {
            var builder = new UriBuilder($"{SelfAppConfig.OmniChannelApiEndpoint}/proxy-product-subscribe");
            var restClient = new RestClient(builder.ToString());
            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
            var request = new RestRequest(Method.POST);
            if (bodyRequest != null)
            {
                request.AddJsonBody(bodyRequest);
            }
            return await restClient.ExecuteTaskAsync(request);

        }

        public async Task<List<ChannelProduct>> GetProductByTrackKey(KvInternalContext context, long channelId, string trackKey)
        {
            var objReq = new GetProductByTrackingKey
            {
                    ChannelId = channelId,
                    TrackKey = trackKey
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.PostAsync<List<ChannelProduct>>(objReq);
        }
        public async Task<List<PriceChannelProduct>> GetPrinceByChannel(KvInternalContext context, long channelId,List<long> productIds)
        {
            var objReq = new GetPrinceByChannel
            {
                ChannelId = channelId,
                ProductIds = productIds
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.PostAsync<List<PriceChannelProduct>>(objReq);
        }
    }
}
﻿using System.Threading.Tasks;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Interfaces;
using Demo.OmniChannel.Sdk.RequestDtos;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.Sdk.Impls
{
    public class OmniLicenseInternalClient : BaseChannelInternalClient, IOmniLicenseInternalClient
    {
        public OmniLicenseInternalClient(IAppSettings settings) : base(settings)
        {
        }

        public async Task<GetPermissionAddChannelResponse> GetPermission(KvInternalContext context, int retailerId, int omniChannelId = 0)
        {
            var getPermissionReq = new GetPermissionAddChannelRequest
            {
                RetailerId = retailerId,
                OmniChannelId = omniChannelId
            };
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await Client.GetAsync<GetPermissionAddChannelResponse>(getPermissionReq);
        }

        public async Task<GetPermissionResponse> GetPermissionOmniChannel(KvInternalContext context, int retailerId)
        {
            var getPermissionReq = new GetPermissionRequest
            {
                RetailerId = retailerId
            };
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await Client.GetAsync<GetPermissionResponse>(getPermissionReq);
        }
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Interfaces;
using Demo.OmniChannel.Sdk.RequestDtos;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.Sdk.Impls
{
    public class InvoiceInternalClient : BaseChannelInternalClient, IInvoiceInternalClient
    {
        public InvoiceInternalClient(IAppSettings settings) : base(settings)
        {
        }

        public async Task<PagingDataSource<Invoice>> GetInvoices(KvInternalContext context, List<long> channelIds, List<int> branchIds, int top, int skip, string invoiceCode,
            string searchTerm, int retailerId, string keySearchProduct = "")
        {
            var request = new GetInvoiceSyncError
            {
                RetailerId = retailerId,
                ChannelIds = channelIds,
                BranchIds = branchIds,
                Skip = skip,
                Top = top,
                SearchTerm = searchTerm,
                InvoiceCode = invoiceCode,
                KeySearchProduct = keySearchProduct
            };
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await Client.PostAsync<PagingDataSource<Invoice>>(request);
        }

        public async Task<Invoice> GetInvoiceById(KvInternalContext context, string id)
        {
            var request = new GetInvoiceById
            {
                Id = id
            };
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await Client.PostAsync<Invoice>(request);
        }

        public async Task<object> RemoveInvoiceById(KvInternalContext context, string id)
        {
            var request = new RemoveInvoiceSyncError
            {
                Id = id
            };
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await Client.DeleteAsync<object>(request);
        }

        public async Task<object> UpdateErrorMessage(KvInternalContext context, Invoice invoice, string errorMessage, string auditTrailMessage)
        {
            var request = new UpdateErrorMessageInvoice
            {
                invoice = invoice,
                ErrorMessage = errorMessage,
                AuditTrailMessage = auditTrailMessage
            };
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await Client.PostAsync<object>(request);
        }

        public async Task<long> TotalInvoiceError(KvInternalContext context, List<long> channelIds)
        {
            var request = new TotalInvoiceError()
            {
                ChannelIds = channelIds
            };
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await Client.PostAsync(request);
        }

        public async Task<object> SyncAllErrorInvoice(KvInternalContext context, string invoices, string channels)
        {
            var request = new MultiSyncErrorInvoice
            {
                Invoices = invoices,
                Channels = channels
            };
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await Client.PostAsync(request);
        }

        public async Task<ChannelStatus> GetLogisticsStatus(KvInternalContext context, long channelId, string orderId)
        {
            var request = new GetLogisticsStatus
            {
                OrderId = orderId,
                ChannelId = channelId
            };
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await Client.PostAsync<ChannelStatus>(request);
        }

        public async Task<List<PaymentChannelInvoices>> GetChannelPaymentInvoices(KvInternalContext context, List<string> orderSnLst)
        {
            var request = new GetChannelPaymentInvoices
            {
                RetailerId = context.RetailerId,
                OrderSnLst = orderSnLst
            };
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await Client.PostAsync(request);
        }
    }
}
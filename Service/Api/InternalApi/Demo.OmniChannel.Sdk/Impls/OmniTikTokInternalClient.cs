﻿using System.Threading.Tasks;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Dtos;
using Demo.OmniChannel.Sdk.Interfaces;
using Demo.OmniChannel.Sdk.RequestDtos;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.Sdk.Impls
{
    public class OmniTikTokInternalClient : BaseChannelInternalClient, IOmniTikTokInternalClient
    {
        public OmniTikTokInternalClient(IAppSettings settings) : base(settings)
        {
        }
        public async Task<ProfileAndTokenResp> GetProfileAndToken(KvInternalContext context, TikTokProfileAndToken param)
        {
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await Client.GetAsync<ProfileAndTokenResp>(param);
        }
        public async Task<object> GetProfileAndUpdateAuth(KvInternalContext context, TiktokProfile param)
        {
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await Client.GetAsync<object>(param);
        }
        public async Task<object> GetAuthConfig(KvInternalContext context, TiktokAuthConfig param)
        {
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await Client.GetAsync(param);
        }

    }
}
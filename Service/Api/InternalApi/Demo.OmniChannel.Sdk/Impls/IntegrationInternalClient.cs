﻿using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Interfaces;
using Demo.OmniChannel.Sdk.RequestDtos;
using ServiceStack.Configuration;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Sdk.Impls
{
    public class IntegrationInternalClient : BaseChannelInternalClient, IIntegrationInternalClient
    {
        public IntegrationInternalClient(IAppSettings settings) : base(settings)
        {
        }

        public async Task<bool> DeactivateChannel(KvInternalContext context, long channelId, string channelName,
            string auditContent = null, string guildId = null)
        {
            var objReq = new InternalDeactivateChannel
            {
                ChannelId = channelId,
                ChannelName = channelName,
                AuditContent = auditContent,
                GuidId = guildId
            };

            InitIntegrationClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.PostAsync<bool>(objReq);
        }

        public async Task<bool> InternalUpdateRegisterChannel(KvInternalContext context, long channelId, string channelName, string guildId = null)
        {
            var objReq = new InternalUpdateRegisterChannel
            {
                ChannelId = channelId,
                GuidId = guildId,
                ChannelName = channelName
            };

            InitIntegrationClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.PostAsync<bool>(objReq);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Interfaces;
using ServiceStack;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.Sdk.Impls
{
    public abstract class BaseChannelInternalClient : IDisposable, IBaseChannelInternalClient
    {
        protected SelfAppConfig Config;
        protected BaseChannelInternalClient(IAppSettings settings)
        {
            Config = new SelfAppConfig(settings);
        }
        protected JsonHttpClient Client;

        protected void InitClient(string endPoint, KvInternalContext context)
        {
            Client = new JsonHttpClient(endPoint);
            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
            if (context != null)
            {
                Client.AddHeader("Retailer", context.RetailerCode);
                Client.AddHeader("BranchId", context.BranchId.ToString());
                Client.AddHeader("User", context.UserId.ToString());
                Client.BearerToken = context.Token;
                var httpClient = Client.GetHttpClient();
                httpClient.Timeout = TimeSpan.FromSeconds(SelfAppConfig.OmniChannelTimeout);
                Client.HttpClient = httpClient;
            }
        }

        protected void InitIntegrationClient(string endPoint, KvInternalContext context)
        {
            Client = new JsonHttpClient(endPoint);
            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
            if (context != null)
            {
                Client.AddHeader("Retailer", context.RetailerCode);
                Client.AddHeader("BranchId", context.BranchId.ToString());
                Client.AddHeader("User", context.UserId.ToString());
                Client.AddHeader("InternalApiToken", SelfAppConfig.InternalToken);
                if (SelfAppConfig.OmniChannelTimeout != 0)
                {
                    var httpClient = Client.GetHttpClient();
                    httpClient.Timeout = TimeSpan.FromSeconds(SelfAppConfig.OmniChannelTimeout);
                    Client.HttpClient = httpClient;
                }
            }
        }

        public Task<T> GetAsync<T>(string endPoint, object requestDto, KvInternalContext context)
        {
            InitClient(endPoint, context);

            return Client.GetAsync<T>(requestDto);
        }

        public Task<List<T>> GetListAsync<T>(string endPoint, object requestDto, KvInternalContext context)
        {
            InitClient(endPoint, context);

            return Client.GetAsync<List<T>>(requestDto);
        }

        public Task<T> PostAsync<T>(string endPoint, object bodyRequest, KvInternalContext context)
        {
            InitClient(endPoint, context);

            return Client.PostAsync<T>(bodyRequest);
        }

        public Task<List<T>> PostListAsync<T>(string endPoint, object requestDto, KvInternalContext context)
        {
            InitClient(endPoint, context);

            return Client.GetAsync<List<T>>(requestDto);
        }

        public Task<T> DeleteAsync<T>(string endPoint, object id, KvInternalContext context)
        {
            InitClient(endPoint, context);

            return Client.DeleteAsync<T>(id);
        }

        public void Dispose()
        {
            Client?.Dispose();
        }
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Dtos;
using Demo.OmniChannel.Sdk.Interfaces;
using Demo.OmniChannel.Sdk.RequestDtos;
using ServiceStack;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.Sdk.Impls
{
    public class ChannelInternalClient : BaseChannelInternalClient, IChannelInternalClient
    {
        public ChannelInternalClient(IAppSettings settings) : base(settings)
        {
        }
        public async Task<ChannelResponse> CreateOrUpdateAsync(object bodyRequest, KvInternalContext context, bool isSyncOrder, bool isSyncPrice, bool isSyncOnHand, long authId, bool isIgnoreAuditTrail = false)
        {
            ChannelCreateOrUpdate createOrUpdateReq = new ChannelCreateOrUpdate
            {
                Channel =  bodyRequest.ConvertTo<ChannelRequest>(),
                IsSyncPrice = isSyncPrice,
                IsSyncOnHand =  isSyncOnHand,
                IsSyncOrder = isSyncOrder,
                AuthId = authId,
                IsIgnoreAuditTrail = isIgnoreAuditTrail
            };
            return await PostAsync<ChannelResponse>($"{SelfAppConfig.OmniChannelApiEndpoint}", createOrUpdateReq, context);
        }

        public async Task<List<ChannelResponse>> GetRetailerAsync(KvInternalContext context, byte? type, bool onlyIsActive = false, bool includeBranch = false, List<int> types = null)
        {
            GetByRetailer getByRetailer = new GetByRetailer
            {
                RetailerId = context.RetailerId,
                Type = type,
                OnlyIsActive = onlyIsActive,
                Types = types ?? new List<int>()
            };
            if (includeBranch) getByRetailer.BranchId = context.BranchId;
            return await PostListAsync<ChannelResponse>($"{SelfAppConfig.OmniChannelApiEndpoint}", getByRetailer, context);
        }

        public async Task<ChannelResponse> GetByIdAsync(KvInternalContext context, long id)
        {
            GetById getById = new GetById
            {
                Id = id
            };
            return await GetAsync<ChannelResponse>($"{SelfAppConfig.OmniChannelApiEndpoint}", getById, context);
        }

        public async Task<ChannelResponse> DeleteByIdAsync(KvInternalContext context, long id)
        {
            GetById getById = new GetById
            {
                Id = id
            };
            return await DeleteAsync<ChannelResponse>($"{SelfAppConfig.OmniChannelApiEndpoint}", getById, context);
        }

        public async Task RenewTokenByRefreshTokenAsync(int branchId, long channelId, KvInternalContext context)
        {
            RenewToken req = new RenewToken
            {
                RetailerId = context.RetailerId,
                BranchId = branchId,
                ChannelId = channelId
            };
             await PostAsync<bool>($"{SelfAppConfig.OmniChannelApiEndpoint}", req, context);
        }

        public async Task ResetSyncOnHandFormulaAsync(KvInternalContext context, string settingKey, bool otherSettingValue)
        {
            ResetFormula req = new ResetFormula
            {
                RetailerId = context.RetailerId,
                SettingKey = settingKey,
                OtherSettingValue = otherSettingValue
            };
            await PostAsync<bool>($"{SelfAppConfig.OmniChannelApiEndpoint}", req, context);
        }

        public async Task<long> CreateChannelAuthAsync(KvInternalContext context, ChannelAuthRequest req)
        {
            var request = new ChannelAuthCreateOrUpdate
            {
                ChannelAuthRequest = new ChannelAuthRequest
                {
                    Id = req.Id,
                    ShopId = req.ShopId,
                    AccessToken = req.AccessToken,
                    RefreshToken = req.RefreshToken,
                    ExpiresIn = req.ExpiresIn,
                    RefreshExpiresIn = req.RefreshExpiresIn,
                    OmniChannelId = req.OmniChannelId
                }
            };
            return await PostAsync<long>(SelfAppConfig.OmniChannelApiEndpoint, request, context);
        }

        public async Task<SendoCreateAuthResponse> CreateAuthSendo(KvInternalContext context, string shopKey, string secretKey, string channelName, long channelId)
        {
            var request = new CreateAuthSendo
            {
                ChannelId = channelId,
                ChannelName = channelName,
                ShopKey = shopKey,
                SecretKey = secretKey
            };

            var result = await PostAsync<SendoCreateAuthResponse>(SelfAppConfig.OmniChannelApiEndpoint, request, context);

            return result;
        }

        public async Task<bool> RemoveChannelAuthAsync(KvInternalContext context, long authId)
        {
            var removeAuth = new RemoveChannelAuth
            {
                AuthId = authId
            };
            return await PostAsync<bool>(SelfAppConfig.OmniChannelApiEndpoint, removeAuth, context);
        }

        public async Task<int> ResetPriceBook(KvInternalContext context, long priceBookId)
        {
            var resetPriceBookReq = new ResetPriceBook
            {
                PriceBookId = priceBookId
            };
            return await PostAsync<int>($"{SelfAppConfig.OmniChannelApiEndpoint}", resetPriceBookReq, context);
        }
        
        public async Task<bool> ReSyncErrorChannel(KvInternalContext context, long? channelId)
        {
            var objReq = new SyncErrorChannel
            {
                ChannelId = channelId
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.PostAsync<bool>(objReq);
        }

        public async Task<bool> DeactivateChannel(KvInternalContext context, long channelId, string channelName, string auditContent = null)
        {
            var objReq = new DeactivateChannel
            {
                ChannelId = channelId,
                ChannelName = channelName,
                AuditContent = auditContent
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.PostAsync<bool>(objReq);
        }

        public async Task<bool> DisableByChannelType(KvInternalContext context, byte channelType)
        {
            var objReq = new DisableByType
            {
                RetailerId = context.RetailerId,
                ChannelType = channelType
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.PostAsync<bool>(objReq);
        }

        public async Task<bool> EnableUpdateProduct(KvInternalContext context, long channelId)
        {
            var objReq = new EnableUpdateProduct
            {
                ChannelId = channelId
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.PostAsync<bool>(objReq);
        }
        public async Task<bool> EnableChannel(KvInternalContext context,
            long channelId,
            ChannelAuthRequest auth,
            string branchName,
            string priceBookName,
            string salePriceBookName,
            string shopName,
            string email,
            long platformId)
        {
            var objReq = new EnableChannel
            {
                ChannelId = channelId,
                Auth = auth,
                BranchName = branchName,
                PriceBookName = priceBookName,
                SalePriceBookName = salePriceBookName,
                ShopName = shopName,
                Email = email,
                PlatformId = platformId
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.PostAsync<bool>(objReq);
        }

        public async Task<List<OmniChannelExpiryResponse>> GetChannelExpiry(KvInternalContext context)
        {
            var objReq = new GetChannelExpiry
            {
                RetailerId = context.RetailerId
            };

            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.GetAsync<List<OmniChannelExpiryResponse>>(objReq);
        }

        public async Task<object> PushProductChannelAsync(KvInternalContext context, PushDataItemToChannelReq req)
        {
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await PostAsync<object>($"{SelfAppConfig.OmniChannelApiEndpoint}", req, context);
        }
        public async Task<CategoriesResp> GetCategories(KvInternalContext context, GetCategoriesReq req)
        {
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await GetAsync<CategoriesResp>($"{SelfAppConfig.OmniChannelApiEndpoint}", req, context);
        }

        public async Task<object> ClearData(KvInternalContext context, ClearData req)
        {
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await PostAsync<object>($"{SelfAppConfig.OmniChannelApiEndpoint}", req, context);
        }

        public async Task<object> GetSaleChannelOmniByType(KvInternalContext context, List<int> channelTypeLst, int groupId)
        {
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            var request = new GetSaleChannelOmniByType
            {
                BranchId = context.BranchId,
                ChannelTypeLst = channelTypeLst,
                GroupId = groupId,
                RetailerId = context.RetailerId
            };
            return await PostAsync<object>($"{SelfAppConfig.OmniChannelApiEndpoint}", request, context);
        }

        public async Task<bool> HandleWareHouseTIKI(KvInternalContext context, HandleWareHouseTiki request)
        {
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await PostAsync<bool>($"{SelfAppConfig.OmniChannelApiEndpoint}", request, context);
        }

        public async Task<object> GetListChannelWareHouseInfo(KvInternalContext context, GetListChannelWareHouseInfo request)
        {
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await PostAsync<object>($"{SelfAppConfig.OmniChannelApiEndpoint}", request, context);
        }
        public async Task<object> ChangeCache(KvInternalContext context, ChangeCache req)
        {
            InitIntegrationClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await Client.PostAsync(req);
        }
    }
}
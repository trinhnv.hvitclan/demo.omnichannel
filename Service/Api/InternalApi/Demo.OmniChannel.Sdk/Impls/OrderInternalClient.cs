﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Interfaces;
using Demo.OmniChannel.Sdk.RequestDtos;
using ServiceStack;
using ServiceStack.Configuration;

namespace Demo.OmniChannel.Sdk.Impls
{
    public class OrderInternalClient : BaseChannelInternalClient, IOrderInternalClient
    {
        public OrderInternalClient(IAppSettings settings) : base(settings)
        {
        }

        public async Task<PagingDataSource<Order>> GetOrders(KvInternalContext context, List<long> channelIds,
            List<int> branchIds, int top, int skip, string orderCode, string searchTerm, int retailerId, string keySearchProduct = "")
        {
            var request = new GetOrderSyncError
            {
                RetailerId = retailerId,
                ChannelIds = channelIds,
                BranchIds = branchIds,
                Skip = skip,
                Top = top,
                SearchTerm = searchTerm,
                OrderCode = orderCode,
                KeySearchProduct = keySearchProduct
            };
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await Client.PostAsync<PagingDataSource<Order>>(request);
        }

        public async Task<Order> GetOrdersById(KvInternalContext context, string id)
        {
            var request = new GetOrderById
            {
                Id = id
            };
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await Client.PostAsync<Order>(request);
        }

        public async Task<object> RemoveOrderById(KvInternalContext context, string id)
        {
            var request = new RemoveOrderSyncError
            {
                Id = id
            };
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await Client.DeleteAsync<object>(request);
        }

        public async Task<object> UpdateErrorMessage(KvInternalContext context, Order order, string errorMessage, string auditTrailMessage)
        {
            var request = new UpdateErrorMessageOrder
            {
                Order = order,
                ErrorMessage = errorMessage,
                AuditTrailMessage = auditTrailMessage
            };
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await Client.PostAsync<object>(request);
        }

        public async Task<long> GetTotalOrderError(KvInternalContext context, List<long> channelIds)
        {
            var request = new TotalOrderError
            {
                ChannelIds = channelIds
            };
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await Client.PostAsync(request);
        }

        public async Task<object> SyncAllErrorOrder(KvInternalContext context, string orders, string channels)
        {
            var request = new MultiSyncErrorOrder
            {
                Orders = orders,
                Channels = channels
            };
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);
            return await Client.PostAsync(request);
        }
    }


}
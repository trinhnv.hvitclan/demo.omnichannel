﻿using System.Collections.Generic;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Interfaces;
using Demo.OmniChannel.Sdk.RequestDtos;
using ServiceStack.Configuration;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Sdk.Impls
{
    public class LogisticsInternalClient : BaseChannelInternalClient, ILogisticsInternalClient
    {
        public LogisticsInternalClient(IAppSettings settings) : base(settings)
        {
        }
        public async Task<object> GetListShopeeLogistics(KvInternalContext context, List<long> channelIds)
        {
            var getShopeeLogistic = new LogisticsRequest
            {
                ChannelIds = channelIds
            };
            
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.GetAsync<object>(getShopeeLogistic);
        }

        public async Task<object> GetListShopeeAttributes(KvInternalContext context, long channelId, long categoryId)
        {
            AttributesRequest getShopeeLogistic = new AttributesRequest
            {
                ChannelId = channelId,
                CategoryId = categoryId
            };
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.GetAsync<object>(getShopeeLogistic);
        }
        public async Task<ReponseResultLogicstic> GetLogisticsFilter(KvInternalContext context, GetLogisticsFilter filters)
        {
            InitClient($"{SelfAppConfig.OmniChannelApiEndpoint}", context);

            return await Client.PostAsync<ReponseResultLogicstic>(filters);
        }
    }
}

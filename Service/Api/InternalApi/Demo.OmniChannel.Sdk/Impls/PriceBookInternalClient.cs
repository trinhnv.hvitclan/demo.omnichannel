﻿using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Interfaces;
using RestSharp;
using ServiceStack.Configuration;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Sdk.Impls
{
    public class PriceBookInternalClient : BaseChannelInternalClient, IPriceBookInternalClient
    {
        public PriceBookInternalClient(IAppSettings settings) : base(settings)
        {
        }
        public async Task<IRestResponse> ListenProxyPriceBookSubscribe(object bodyRequest)
        {
            var builder = new UriBuilder($"{SelfAppConfig.OmniChannelApiEndpoint}/proxy-pricebook-subscribe");
            var restClient = new RestClient(builder.ToString());
            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
            var request = new RestRequest(Method.POST);
            if (bodyRequest != null)
            {
                request.AddJsonBody(bodyRequest);
            }
            return await restClient.ExecuteTaskAsync(request);
        }
    }
}

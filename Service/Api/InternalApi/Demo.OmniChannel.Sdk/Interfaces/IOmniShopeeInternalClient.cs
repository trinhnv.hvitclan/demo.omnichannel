﻿using System.Threading.Tasks;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Dtos;
using Demo.OmniChannel.Sdk.RequestDtos;


namespace Demo.OmniChannel.Sdk.Interfaces
{
    public interface IOmniShopeeInternalClient
    {
        Task<object> GetAuthConfigV2(KvInternalContext context, ShopeeAuthConfigV2 param);

        Task<ShopeeProfileResponse> GetProfileAndUpdateAuth(KvInternalContext context, ShopeeProfileAndUpdateAuth param);

        Task<ProfileAndTokenResp> GetProfileAndToken(KvInternalContext context, ShopeeProfileAndToken param);
    }
}

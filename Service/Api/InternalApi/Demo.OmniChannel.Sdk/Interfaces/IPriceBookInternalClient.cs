﻿using RestSharp;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Sdk.Interfaces
{
    public interface IPriceBookInternalClient
    {
        Task<IRestResponse> ListenProxyPriceBookSubscribe(object bodyRequest);
    }
}
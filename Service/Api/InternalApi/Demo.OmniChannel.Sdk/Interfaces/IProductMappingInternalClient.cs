﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.RequestDtos;

namespace Demo.OmniChannel.Sdk.Interfaces
{
    public interface IProductMappingInternalClient
    {
        Task<List<ChannelProduct>> GetMapProductByChannel(KvInternalContext context, long channelId, int branchId);

        /// <summary>
        /// If channelIds has items --> Ignore filter channelTypes
        /// If channelIds hasn't any items --> Use channelTypes to get all channelIds of channelTypes
        /// If channelIds && channelTypes hasn't any items --> Get all Product Mapping by Retailer, Branch
        /// </summary>
        Task<List<ProductMapping>> GetMapProductByChannel(KvInternalContext context, IList<ChannelType> channelTypes, IList<long> channelIds, IList<int> branchIds);

        Task<List<ProductMapping>> GetProductMappingAsync(KvInternalContext context, long? channelId, List<string> kvProductSkus = null, List<string> channelProductSku = null);
        Task<ProductMapping> AddProductMappingAsync(KvInternalContext context, ProductMapping productMapping);
        Task<ProductMapping> DeleteProductMappingAsync(KvInternalContext context, long channelId, long kvProductId, string channelProductId);
        Task<bool> DeleteProductMappingAsync(KvInternalContext context, string productMappingId);
        Task<int> DeleteProductMappingsAsync(KvInternalContext context, List<long> kvProductIds);

        Task<List<ProductMapping>> GetByChannelProductIds(KvInternalContext context, long channelId,
            List<string> channelProductIds);

        Task<int> DeleteMappingProductByIdsTask(KvInternalContext context,
            List<long> kvProductIds);
    }
}

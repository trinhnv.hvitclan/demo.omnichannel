﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.RequestDtos;

namespace Demo.OmniChannel.Sdk.Interfaces
{
    public interface IOrderInternalClient
    {
        Task<PagingDataSource<Order>> GetOrders(KvInternalContext context, List<long> channelIds, List<int> branchIds,
            int top, int skip, string orderCode, string searchTerm, int retailerId,string keySearchProduct= "");
        Task<Order> GetOrdersById(KvInternalContext context, string id);
        Task<object> RemoveOrderById(KvInternalContext context, string id);
        Task<object> UpdateErrorMessage(KvInternalContext context, Order order, string errorMessage, string auditTrailMessage);
        Task<long> GetTotalOrderError(KvInternalContext context, List<long> channelIds);
        Task<object> SyncAllErrorOrder(KvInternalContext context, string orders, string channels);
    }
}
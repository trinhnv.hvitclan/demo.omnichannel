﻿using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.RequestDtos;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Sdk.Interfaces
{
    public interface IChannelProductInternalClient
    {
        Task<PagingDataSource<ChannelProduct>> GetProductsByChannelId(
            KvInternalContext context,
            long channelId, int branchId, int top, int skip, string channelTerm, string kvTerm,
            bool? isConnected = null, bool? isSyncSuccess = null);

        Task<IList<ChannelProduct>> GetProductByKvProductIds(KvInternalContext context, IList<long> kvProductIds);
        Task<List<ChannelProduct>> GetProductBySearchTerm(KvInternalContext context, long channelId, string term, int skip = 0, int top = 0);
        Task<bool> AddProductAndMapping(KvInternalContext context, object bodyRequest);
        Task<List<ChannelProduct>> GetProductByChannel(KvInternalContext context, long channelId, int branchId);
        Task<List<ChannelProduct>> GetProductSyncError(KvInternalContext context, List<long> channelIds, int branchId, string productSearchTerm, string errorType = null);
        Task<ChannelProductDetail> GetProductDetailFormChannel(KvInternalContext context, long channelId, string productId, string parentProductId, string productSku = null);
        Task<long> GetProductDetailFormChannel(KvInternalContext context, List<long> channelIds);
        Task<IRestResponse> ListenProxyProductSubscribe(object bodyRequest);
        Task<List<ChannelProduct>> GetProductByTrackKey(KvInternalContext context, long channelId, string trackKey);
       Task<List<PriceChannelProduct>> GetPrinceByChannel(KvInternalContext context, long channelId, List<long> productIds);
    }
}
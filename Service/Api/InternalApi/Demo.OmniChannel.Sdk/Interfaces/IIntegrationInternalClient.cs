﻿using Demo.OmniChannel.Sdk.Common;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Sdk.Interfaces
{
    public interface IIntegrationInternalClient
    {
        Task<bool> DeactivateChannel(KvInternalContext context, long channelId, string channelName,
            string auditContent = null, string guildId = null);

        Task<bool> InternalUpdateRegisterChannel(KvInternalContext context, long channelId, string channelName, string guildId = null);
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.RequestDtos;

namespace Demo.OmniChannel.Sdk.Interfaces
{
    public interface IInvoiceInternalClient
    {
        Task<PagingDataSource<Invoice>> GetInvoices(KvInternalContext context, List<long> channelIds, List<int> branchIds,
            int top, int skip, string invoiceCode, string searchTerm, int retailerId, string keySearchProduct = "");
        Task<Invoice> GetInvoiceById(KvInternalContext context, string id);
        Task<object> RemoveInvoiceById(KvInternalContext context, string id);
        Task<object> UpdateErrorMessage(KvInternalContext context, Invoice invoice, string errorMessage, string auditTrailMessage);
        Task<long> TotalInvoiceError(KvInternalContext context, List<long> channelIds);
        Task<object> SyncAllErrorInvoice(KvInternalContext context, string invoices, string channels);
        Task<ChannelStatus> GetLogisticsStatus(KvInternalContext context, long channelId, string orderId);
        Task<List<PaymentChannelInvoices>> GetChannelPaymentInvoices(KvInternalContext context, List<string> orderSnLst);
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Dtos;
using Demo.OmniChannel.Sdk.RequestDtos;

namespace Demo.OmniChannel.Sdk.Interfaces
{
    public interface IChannelInternalClient : IBaseChannelInternalClient
    {
        Task<ChannelResponse> CreateOrUpdateAsync(object bodyRequest, KvInternalContext context, bool isSyncOrder, bool isSyncPrice, bool isSyncOnHand, long authId, bool isIgnoreAuditTrail = false);
        Task<List<ChannelResponse>> GetRetailerAsync(KvInternalContext context, byte? type, bool onlyIsActive = false, bool includeBranch = false, List<int> types = null);
        Task<ChannelResponse> GetByIdAsync(KvInternalContext context, long id);
        Task<ChannelResponse> DeleteByIdAsync(KvInternalContext context, long id);
        Task RenewTokenByRefreshTokenAsync(int branchId, long channelId, KvInternalContext context);
        Task ResetSyncOnHandFormulaAsync(KvInternalContext context, string settingKey, bool otherSettingValue);
        Task<long> CreateChannelAuthAsync(KvInternalContext context, ChannelAuthRequest req);
        Task<SendoCreateAuthResponse> CreateAuthSendo(KvInternalContext context, string shopKey, string secretKey, string channelName, long channelId);
        Task<bool> RemoveChannelAuthAsync(KvInternalContext context, long authId);
        Task<int> ResetPriceBook(KvInternalContext context, long priceBookId);
        Task<bool> ReSyncErrorChannel(KvInternalContext context, long? channelId);
        Task<bool> DeactivateChannel(KvInternalContext context, long channelId, string channelName, string auditContent = null);
        Task<bool> DisableByChannelType(KvInternalContext context, byte channelType);
        Task<bool> EnableUpdateProduct(KvInternalContext context, long channelId);
        Task<bool> EnableChannel(KvInternalContext context, long channelId, ChannelAuthRequest auth, string branchName,
            string priceBookName, string salePriceBookName, string shopName, string email, long platformId);
        Task<List<OmniChannelExpiryResponse>> GetChannelExpiry(KvInternalContext context);
        Task<object> PushProductChannelAsync(KvInternalContext context, PushDataItemToChannelReq req);
        Task<CategoriesResp> GetCategories(KvInternalContext context, GetCategoriesReq req);
        Task<object> ClearData(KvInternalContext context, ClearData req);
        Task<object> GetSaleChannelOmniByType(KvInternalContext context, List<int> channelTypeLst, int groupId);
        Task<bool> HandleWareHouseTIKI(KvInternalContext context, HandleWareHouseTiki request);
        Task<object> GetListChannelWareHouseInfo(KvInternalContext context, GetListChannelWareHouseInfo request);
        Task<object> ChangeCache(KvInternalContext context, ChangeCache req);
    }
}

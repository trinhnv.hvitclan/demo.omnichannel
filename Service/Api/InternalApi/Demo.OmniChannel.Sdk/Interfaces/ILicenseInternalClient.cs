﻿using System.Threading.Tasks;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.RequestDtos;

namespace Demo.OmniChannel.Sdk.Interfaces
{
    public interface IOmniLicenseInternalClient
    {
        Task<GetPermissionAddChannelResponse> GetPermission(KvInternalContext context,int retailerId, int omniChannelId = 0);
        Task<GetPermissionResponse> GetPermissionOmniChannel(KvInternalContext context, int retailerId);
    }
}

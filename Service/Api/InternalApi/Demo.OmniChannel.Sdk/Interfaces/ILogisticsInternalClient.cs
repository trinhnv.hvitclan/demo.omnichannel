﻿
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.RequestDtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.OmniChannel.Sdk.Interfaces
{
    public interface ILogisticsInternalClient : IBaseChannelInternalClient
    {
        Task<object> GetListShopeeLogistics(KvInternalContext context, List<long> channelIds);

        Task<object> GetListShopeeAttributes(KvInternalContext context, long channelId, long categoryId);
        Task<ReponseResultLogicstic> GetLogisticsFilter(KvInternalContext context, GetLogisticsFilter filters);

    }
}

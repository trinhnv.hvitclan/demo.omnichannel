﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.OmniChannel.Sdk.Common;

namespace Demo.OmniChannel.Sdk.Interfaces
{
    public interface IBaseChannelInternalClient
    {
        Task<T> GetAsync<T>(string endPoint, object requestDto, KvInternalContext context);
        Task<List<T>> GetListAsync<T>(string endPoint, object requestDto, KvInternalContext context);
        Task<T> PostAsync<T>(string endPoint, object bodyRequest, KvInternalContext context);
        Task<List<T>> PostListAsync<T>(string endPoint, object requestDto, KvInternalContext context);
        Task<T> DeleteAsync<T>(string endPoint, object id, KvInternalContext context);
    }
}
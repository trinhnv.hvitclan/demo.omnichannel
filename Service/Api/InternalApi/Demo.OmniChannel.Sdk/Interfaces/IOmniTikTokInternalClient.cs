﻿using System.Threading.Tasks;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Dtos;
using Demo.OmniChannel.Sdk.RequestDtos;

namespace Demo.OmniChannel.Sdk.Interfaces
{
    public interface IOmniTikTokInternalClient
    {
        Task<ProfileAndTokenResp> GetProfileAndToken(KvInternalContext context, TikTokProfileAndToken param);
        Task<object> GetProfileAndUpdateAuth(KvInternalContext context, TiktokProfile param);
        Task<object> GetAuthConfig(KvInternalContext context, TiktokAuthConfig param);
    }
}

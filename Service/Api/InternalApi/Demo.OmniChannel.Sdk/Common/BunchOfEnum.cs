﻿using System.ComponentModel;

namespace Demo.OmniChannel.Sdk.Common
{
    public enum ChannelType
    {
        Lazada = 1,
        Shopee = 2,
        Tiki = 3,
        Sendo = 4,
        Tiktok = 5
    }
    public enum ChannelStatusType
    {
        Pending = 1,
        Reject = 2,
        Approved = 3
    }
    public enum ScheduleType
    {
        SyncChannelProduct = 0,
        SyncModifiedProduct = 1,
        SyncPrice = 2,
        SyncOnhand = 3,
        SyncOrder = 4
    }
    public enum AppSupports
    {
         KiotOnlinePro = 0,
         KiotOnlineFree = 1
    }

    public static class ChannelPosition
    {
        public const int Tiktok = 0;
        public const int Tiki = 1;
        public const int Lazada = 2;
        public const int Shopee = 3;
        public const int Sendo = 4;
    }

    public enum SyncTabError
    {
        [Description("Product")]
        Product = 3,
        [Description("Invoice")]
        Invoice = 2,
        [Description("Order")]
        Order = 1
    }

    public enum DiffReturn
    {
        EqualZero = 0,
        LessThanZero = -1,
        GreaterThanZero = 1,
        NotYetPayment = 2
    }


    public static class LicenseDeactiveType
    {
        public const string DeactiveFeature = "Tắt tính năng Quản lý tích hợp sàn TMĐT";
        public const string Expired = "Hết kì hạn hợp đồng";
        public const string OutOfMaximumChannel = "Quá số lượng liên kết cho phép";
        public const string TrialExpired = "Hết kì hạn dùng thử";
    }
}

﻿using ServiceStack.Web;

namespace Demo.OmniChannel.Sdk.Common
{
    public class KvInternalContext
    {
        public string BaseUrl { get; set; }
        public string OriginalUrl { get; set; }
        public string RetailerCode { get; set; }
        public string Token { get; set; }
        public int BranchId { get; set; }
        public int RetailerId { get; set; }
        public long UserId { get; set; }
    }
}
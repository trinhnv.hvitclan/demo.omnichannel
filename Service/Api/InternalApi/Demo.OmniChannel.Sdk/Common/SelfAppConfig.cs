﻿using ServiceStack.Configuration;

namespace Demo.OmniChannel.Sdk.Common
{
    public class SelfAppConfig
    {
        private static IAppSettings _settings;
        public SelfAppConfig(IAppSettings settings)
        {
            _settings = settings;
        }
        public static string OmniChannelApiEndpoint => _settings.GetString("OmniChannelApiEndpoint");
        public static string InternalToken => _settings.GetString("InternalApiAccessKey");
        public static string InternalApiAccessKeyOmni => _settings.GetString("InternalApiAccessKeyOmni");

        public static int OmniChannelTimeout => _settings.Get("OmniChannelTimeout", 100);
    }
}
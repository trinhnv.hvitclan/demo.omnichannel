﻿using System.Collections.Generic;

namespace Demo.OmniChannel.Sdk.Common
{
    public class PagingDataSource<T> where T : class
    {
        public long Total { get; set; }
        public IList<T> Data { get; set; }
    }
}
﻿using Demo.OmniChannel.Sdk.RequestDtos;
using ServiceStack;

namespace Demo.OmniChannel.Sdk.Dtos
{
    public class ProfileAndTokenResp : IReturn<object>
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public ChannelAuthRequest Auth { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace Demo.OmniChannel.Sdk.Dtos
{
    public class CategoriesResp
    {
        public List<Categories> Categories { get; set; }
        public string RequestId { get; set; }
    }
}

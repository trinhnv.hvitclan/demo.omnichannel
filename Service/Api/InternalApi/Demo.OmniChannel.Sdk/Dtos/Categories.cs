﻿
namespace Demo.OmniChannel.Sdk.Dtos
{
    public class Categories
    {
        public long CategoryId { get; set; }
        public long ParentId { get; set; }
        public string CategoryName { get; set; }
        public bool HasChildren { get; set; }
    }
}

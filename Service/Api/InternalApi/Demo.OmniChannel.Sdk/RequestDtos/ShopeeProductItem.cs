﻿using System.Collections.Generic;

namespace Demo.OmniChannel.Sdk.RequestDtos
{
    public class ShopeeProductItem
    {
        public long CategoryId { get; set; }
        public string Name { get; set; }
        public string ItemSku { get; set; }
        public string Description { get; set; }
        public decimal Pice { get; set; }
        public int Stock { get; set; }
        public List<ShopeeProductItem> Variations { get; set; }
        public List<string> Images { get; set; }
        public List<Logistics> Logistics { get; set; }
        public float Weight { get; set; }
        public long PartnerId { get; set; }
        public long Shopid { get; set; }
        public long Timestamp { get; set; }
    }
}

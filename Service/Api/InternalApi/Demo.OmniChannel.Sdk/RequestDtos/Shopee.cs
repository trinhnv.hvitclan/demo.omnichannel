﻿using System.Collections.Generic;
using ServiceStack;

namespace Demo.OmniChannel.Sdk.RequestDtos
{
    public class LogisticsRequest : IReturn<object>
    {
        public List<long> ChannelIds { get; set; }
    }

    [Route("/shopee/attributes", "GET")]
    public class AttributesRequest : IReturn<object>
    {
        public long ChannelId { get; set; }

        public long CategoryId { get; set; }
    }

    [Route("/shopee/getalllogisticsinfo", "POST")]
    [Route("/shopee/getLogisticsFilter", "POST")]
    public class GetLogisticsFilter : IReturn<object>
    {
        public List<long> ChannelIds { get; set; }
    }

    public class ReponseResultLogicstic
    {
        public bool IsVeryQuickLogicstic { get; set; }
        public bool IsQuickLogicstic { get; set; }
        public bool IsThriftyLogicstic { get; set; }
    }

    [Route("/shopee/auth-config-v2", "GET")]
    public class ShopeeAuthConfigV2 : IReturn<object>
    {
    }

    [Route("/shopee/profile-v2", "GET")]
    public class ShopeeProfileAndUpdateAuth : IReturn<object>
    {
        public long ShopId { get; set; }
        public string Code { get; set; }
        public long PlatformId { get; set; }
        public long OmniChannelId { get; set; }
    }

    [Route("/shopee/profile-and-token", "GET")]
    public class ShopeeProfileAndToken
    {
        public long PlatformId { get; set; }
        public string Code { get; set; }
        public long ShopId { get; set; }
        public string IdentityKey { get; set; }
        public string ShopName { get; set; }
    }

    public class ShopeeProfileResponse : IReturn<object>
    {
        public long AuthId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string IdentityKey { get; set; }
    }

    
}

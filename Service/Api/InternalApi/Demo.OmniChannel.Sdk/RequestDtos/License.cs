﻿using ServiceStack;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Demo.OmniChannel.Sdk.RequestDtos
{
    #region Response

    [Route("/license", "GET")]
    public class GetPermissionAddChannelRequest : IReturn<object>, IGetPermissionRequest
    {
        public int RetailerId { get; set; }
        public int? OmniChannelId { get; set; }
    }

    [Route("/permission", "GET")]
    public class GetPermissionRequest: IReturn<object>, IGetPermissionRequest
    {
        public int RetailerId { get; set; }
    }

    public interface IGetPermissionRequest
    {
        int RetailerId { get; set; }
    }


    [Route("/licensefiveday", "GET")]
    public class GetLicenseFiveDayRequest : IReturn<object>
    {
    }

    #endregion

    #region Request



    public class GetPermissionAddChannelResponse
    {
        [DataMember(Name = "IsSuccess")]
        public bool IsSuccess { get; set; }
        [DataMember(Name = "ErrorMessage")]
        public string ErrorMessage { get; set; }
    }

    public class GetPermissionResponse
    {
        public bool IsActive { get; set; }
        public bool IsExpired { get; set; }
        public int BlockUnit { get; set; }
        public List<ChannelResponse> Channels { get; set; }
    }

    public class PermissionLogicResult
    {
        public bool IsSuccess { get; set; }
        public bool IsActive { get; set; }
        public bool IsExpired { get; set; }
        public int BlockUnit { get; set; }
        public string ErrorMessage { get; set; }

    }
    public class GetLicenseFiveDayResponse
    {
        [DataMember(Name = "IsExpired")]
        public bool IsExpired { get; set; }
        [DataMember(Name = "DayCountDown")]
        public int DayCountDown { get; set; }
    }

    #endregion

}

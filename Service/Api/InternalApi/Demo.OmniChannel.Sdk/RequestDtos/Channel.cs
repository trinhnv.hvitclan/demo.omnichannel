﻿using System;
using System.Collections.Generic;
using Demo.OmniChannel.Sdk.Common;
using Demo.OmniChannel.Sdk.Dtos;
using ServiceStack;

namespace Demo.OmniChannel.Sdk.RequestDtos
{
    [Route("/channel", "POST")]
    public class ChannelCreateOrUpdate : IReturn<object>
    {
        public ChannelRequest Channel { get; set; }
        public bool IsSyncOrder { get; set; }
        public bool IsSyncOnHand { get; set; }
        public bool IsSyncPrice { get; set; }
        public bool IsAutoMappingSKU { get; set; }
        public long AuthId { get; set; }
        public bool IsIgnoreAuditTrail { get; set; }
    }
    [Route("/channel/retailer/{RetailerId}")]
    public class GetByRetailer : IReturn<object>
    {
        public int RetailerId { get; set; }
        public int BranchId { get; set; }
        public byte? Type { get; set; }
        public List<int> Types { get; set; }
        public bool OnlyIsActive { get; set; }
    }
    [Route("/channel/{Id}", "GET")]
    [Route("/channel/{Id}", "DELETE")]
    public class GetById : IReturn<object>
    {
        public long Id { get; set; }
    }
    [Route("/channel/renewtoken", "POST")]
    public class RenewToken : IReturn<object>
    {
        public int RetailerId { get; set; }
        public int BranchId { get; set; }
        public long ChannelId { get; set; }
    }
    [Route("/channel/resetsynconhandformula", "POST")]
    public class ResetFormula : IReturn<object>
    {
        public int RetailerId { get; set; }
        public string SettingKey { get; set; }
        public bool OtherSettingValue { get; set; }
    }
    [Route("/channel/authcreateorupdate", "POST")]
    public class ChannelAuthCreateOrUpdate : IReturn<object>
    {
        public ChannelAuthRequest ChannelAuthRequest { get; set; }
        public string LogId { get; set; }
    }
    [Route("/channel/createauthsendo", "POST")]
    public class CreateAuthSendo : IReturn<object>
    {
        public long ChannelId { get; set; }
        public string ChannelName { get; set; }
        public string ShopKey { get; set; }
        public string SecretKey { get; set; }
    }
    [Route("/channel/remove-channel-auth", "POST")]
    public class RemoveChannelAuth : IReturn<object>
    {
        public long AuthId { get; set; }
    }

    [Route("/channel/resetpricebook", "POST")]
    public class ResetPriceBook : IReturn<object>
    {
        public long PriceBookId { get; set; }
    }

    [Route("/channel/syncAgainErrorChannel", "POST")]
    public class SyncErrorChannel : IReturn<object>
    {
        public long? ChannelId { get; set; }
    }
    [Route("/channel/deactivateChannel", "POST")]
    public class DeactivateChannel : IReturn<object>
    {
        public long ChannelId { get; set; }
        public long AuthId { get; set; }
        public string ChannelName { get; set; }
        public string AuditContent { get; set; }
    }

    [Route("/channel/disableByType", "POST")]
    public class DisableByType : IReturn<object>
    {
        public long RetailerId { get; set; }
        public byte ChannelType { get; set; }
    }

    [Route("/channel/enableUpdateProduct", "POST")]
    public class EnableUpdateProduct : IReturn<object>
    {
        public long ChannelId { get; set; }
    }
    [Route("/channel/enableChannel", "POST")]
    public class EnableChannel : IReturn<object>
    {
        public long ChannelId { get; set; }
        public ChannelAuthRequest Auth { get; set; }
        public string BranchName { get; set; }
        public string PriceBookName { get; set; }
        public string SalePriceBookName { get; set; }
        public string ShopName { get; set; }
        public string Email { get; set; }
        public long PlatformId { get; set; }
    }

    [Route("/channel/getChannelExpiry", "GET")]
    public class GetChannelExpiry : IReturn<object>
    {
        public int RetailerId { get; set; }
    }

    [Route("/channel/getAccessToken", "GET")]
    public class GetAccessToken : IReturn<object>
    {
        public long OmniChannelId { get; set; }
    }

    public class PushDataItemToChannelReq
    {
        public ProductDataPushChannel PushDataItemToChannel { get; set; }
        public List<ChannelType> ChannelTypes { get; set; }
    }

    [Route("/channel/categories", "GET")]
    public class GetCategoriesReq : IReturn<object>
    {
        public long ChannelId { get; set; }
    }

    [Route("/channel/clearData", "POST")]
    public class ClearData : IReturn<object>
    {
        public int RetailerId { get; set; }
    }

    [Route("/channel/getSaleChannelOmniByType", "POST")]
    public class GetSaleChannelOmniByType : IReturn<object>
    {
        public List<int> ChannelTypeLst { get; set; }
        public int RetailerId { get; set; }
        public int GroupId { get; set; }
        public int BranchId { get; set; }
    }

    [Route("/channel/handleWareHouseTIKI", "POST")]
    public class HandleWareHouseTiki : IReturn<object>
    {
        public long OmniChannelId { get; set; }
        public TikiWareHouseRequest TikiWareHouseRequest { get; set; }
    }

    [Route("/channel/GetListChannelWareHouseInfo", "POST")]
    public class GetListChannelWareHouseInfo : IReturn<object>
    {
        public long OmniChannelAuthId { get; set; }
    }

    [Route("/internal/changeCache", "POST")]
    public class ChangeCache : IReturn<object>
    {
        public int RetailerId { get; set; }
        public bool IsActive { get; set; }
    }

    #region Response

    public class ChannelResponse
    {
        public int Id { get; set; }
        public long? ShopId { get; set; }
        public string Name { get; set; }
        public byte Type { get; set; }
        public string Email { get; set; }
        public string Code { get; set; }
        public int? BranchId { get; set; }
        public string BranchName { get; set; }
        public int RetailerId { get; set; }
        public bool IsActive { get; set; }
        public byte Status { get; set; }
        public long? PriceBookId { get; set; }
        public string PriceBookName { get; set; }
        public long? BasePriceBookId { get; set; }
        public string BasePriceBookName { get; set; }
        public byte SyncOnHandFormula { get; set; }
        public bool IsApplyAllFormula { get; set; }
        public bool IsAutoMappingProduct { get; set; }
        public bool? IsAutoDeleteMapping { get; set; }
        public ChannelAuthResponse OmniChannelAuth { get; set; }
        public List<OmniChannelSchedule> OmniChannelSchedules { get; set; }
        public OmniChannelSettingObjectDto OmniChannelSettings { get; set; }
        public List<OmniChannelWareHouse> OmniChannelWareHouses { get; set; }

        public long PlatformId { get; set; }
        public long TotalProductConnected { get; set; }
        public DateTime CreatedDate { get; set; }
        public string IdentityKey { get; set; }
        public int? SyncOrderFormula { get; set; }
        public bool IsExpireWarning { get; set; }
        public string ExtraKey { get; set; }
        public DateTime? RegisterDate { get; set; }
    }
    public class ChannelAuthResponse
    {
        public long Id { get; set; }
    }

    public class OmniChannelExpiryResponse
    {
        public byte ChannelType { get; set; }
        public string ChannelName { get; set; }
        public DateTime ExpiryDate { get; set; }
        public bool IsActive { get; set; }
    }


    public class SendoCreateAuthResponse
    {
        public long AuthId { get; set; }
        public string IdentityKey { get; set; }
    }

    #endregion

    #region Requests

    public class TikiWareHouseRequest
    {
        public long WareHouseId { get; set; }
        public string Name { get; set; }
        public string StreetInfo { get; set; }
        public string Code { get; set; }
    }

    public class ChannelRequest
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public byte Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long CreateBy { get; set; }
        public long? ModifiedBy { get; set; }
        public byte Type { get; set; }
        public int BranchId { get; set; }
        public int RetailerId { get; set; }
        public long? PriceBookId { get; set; }
        public long? BasePriceBookId { get; set; }
        public byte SyncOnHandFormula { get; set; }
        public bool IsApplyAllFormula { get; set; }
        public bool? IsAutoDeleteMapping { get; set; }
        public bool IsAutoMappingProduct { get; set; }
        public string IdentityKey { get; set; } // Nhận biết shop (Lazada: SellerId. Shopee: ShopId)
        public DateTime? RegisterDate { get; set; }
        public int? SyncOrderFormula { get; set; }
        public string ExtraKey { get; set; }
        public long PlatformId { get; set; }
        public OmniChannelSettingObjectDto OmniChannelSettings { get; set; }
    }

    public class OmniChannelWareHouse
    {
        public long Id { get; set; }
        public long WareHouseId { get; set; }
        public long OmniChannelId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long CreateBy { get; set; }
        public long? ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public long RetailerId { get; set; }
        public string StreetInfo { get; set; }
    }

    public class OmniChannelSchedule
    {
        public long Id { get; set; }
        public DateTime? NextRun { get; set; }
        public DateTime? LastSync { get; set; }
        public bool IsRunning { get; set; }
        public int Type { get; set; }
        public long OmniChannelId { get; set; }
    }

    public class ChannelAuthRequest
    {
        public long Id { get; set; }
        public long ShopId { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public int ExpiresIn { get; set; }
        public int RefreshExpiresIn { get; set; }
        public long? OmniChannelId { get; set; }
    }

    public class OmniChannelSettingObjectDto
    {
        public bool IsConfirmReturning { get; set; }
        public bool IsAutoSyncBatchExpire { get; set; }
        public bool IsApplyAllBatchExpire { get; set; }
        public int SyncOrderFormulaType { get; set; }
        public string SyncOrderFormulaDateTime { get; set; }
        public bool IsAutoCreatingProduct { get; set; }
        public bool? DistributeMultiMapping { get; set; }
        public bool IsAutoCopyProduct { get; set; }
    }

    #endregion
}
﻿
namespace Demo.OmniChannel.Sdk.RequestDtos
{
    public class Attribute
    {
        public long AttributeId { get; set; }
        public string Value { get; set; }
    }
}

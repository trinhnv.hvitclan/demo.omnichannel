﻿using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Demo.OmniChannel.Sdk.Common;

namespace Demo.OmniChannel.Sdk.RequestDtos
{
    [Route("/getproductmapping")]
    public class GetProductMapping : IReturn<List<object>>
    {
        public long? ChannelId { get; set; }
        public List<string> KvProductSkus { get; set; }
        public List<string> ChannelProductSkus { get; set; }
    }
    [Route("/getmapbychannel")]
    public class GetMappingByChannel : IReturn<object>
    {
        public long ChannelId { get; set; }
        public int BranchId { get; set; }
    }
    [Route("/product-map-by-channel-list")]
    public class GetProductMappingByChannelList : IReturn<ProductMapping>
    {
        public IList<ChannelType> ChannelTypes { get; set; }
        public IList<long> ChannelIds { get; set; }
        public IList<int> BranchIds { get; set; }
    }

    [Route("/omnichannel/addproductmapping", "POST")]
    public class AddProductMapping : IReturn<List<object>>
    {
        public ProductMapping ProductMapping { get; set; }
    }

    [Route("/omnichannel/deleteProductMapping", "POST")]
    public class DeleteProductMapping : IReturn<object>
    {
        public long ChannelId { get; set; }
        public long KvProductId { get; set; }
        public string ChannelProductId { get; set; }
    }

    [Route("/omnichannel/deleteProductMappingbyid", "DELETE")]
    public class DeleteProductMappingById : IReturn<object>
    {
        public string ProductMappingId { get; set; }
    }

    [Route("/deleteproductmappingsbykvproduct", "POST")]
    public class DeleteProductMappingsByKvProduct : IReturn<object>
    {
        public List<long> KvProductIds { get; set; }
    }

    [Route("/getmappingbychannelproductid")]
    public class GetMappingByChannelProductId : IReturn<object>
    {
        public long ChannelId { get; set; }
        public List<string> ChannelProductIds { get; set; }
    }
    public class ProductMapping
    {
        public string Id { get; set; }
        public long ProductKvId { get; set; }
        public string ProductKvSku { get; set; }
        public string ProductKvFullName { get; set; }
        public int RetailerId { get; set; }
        public string RetailerCode { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProductChannelId { get; set; }
        public string ParentProductChannelId { get; set; }
        public string ProductChannelSku { get; set; }
        public string ProductChannelName { get; set; }
        public byte ProductChannelType { get; set; }
        public long ChannelId { get; set; }
        public string EditProductUrl { get; set; }
        public long? SuperId { get; set; }
    }

    public class KvProduct
    {
        public long KvProductId { get; set; }
        public string KvProductSku { get; set; }
        public string KvProductFullName { get; set; }
        public List<int> KvInActiveBranchIds { get; set; }
    }

    public class DeleteProductMappingByIdRes
    {
        public bool IsDeleted { get; set; }

        public ProductMapping ProductMapping { get; set; }
    }
}

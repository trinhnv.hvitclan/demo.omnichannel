﻿using System;
using ServiceStack;
using System.Collections.Generic;
using Demo.OmniChannel.Sdk.Common;

namespace Demo.OmniChannel.Sdk.RequestDtos
{
    [Route("/getproductbychannelid")]
    public class GetProductByChannelId : IReturn<PagingDataSource<object>>
    {
        public long ChannelId { get; set; }
        public int BranchId { get; set; }
        public int Top { get; set; }
        public int Skip { get; set; }
        public bool? IsConnected { get; set; }
        public bool? IsSyncSuccess { get; set; }
        public string ChannelTerm { get; set; }
        public string KvTerm { get; set; }
    }
    
    [Route("/getproductbysearch")]
    public class GetProductBySearch : IReturn<List<object>>
    {
        public long ChannelId { get; set; }
        public string Term { get; set; }
        public int Skip { get; set; }
        public int Top { get; set; }
    }

    [Route("/product-by-kv-product-ids")]
    public class GetProductByKvProductIds : IReturn<IList<object>>
    {
        public IList<long> KvProductIds { get; set; }
    }

    [Route("/omnichannel/addproductandmapping", "POST")]
    public class AddProductAndMapping : IReturn<object>
    {
        public ChannelProduct Product { get; set; }
    }

    [Route("/getproductbychannel")]
    public class GetProductByChannel : IReturn<object>
    {
        public long ChannelId { get; set; }
        public int BranchId { get; set; }
    }

    [Route("/getproductsyncerror")]
    public class GetProductSyncError : IReturn<List<object>>
    {
        public List<long> ChannelIds { get; set; }
        public int BranchId { get; set; }
        public string ProductSearchTerm { get; set; }
        public string ErrorType { get; set; }   // "OnHand"/"Price"
    }

    [Route("/getproductdetailfromchannel")]
    public class GetProductDetail : IReturn<object>
    {
        public object ProductId { get; set; }
        public string ProductSku { get; set; }
        public long ChannelId { get; set; }
        public object ParentProductId { get; set; }
    }

    [Route("/getquantityofproductsyncerror")]
    public class GetQuantityOfProductSyncError : IReturn<object>
    {
        public List<long> ChannelIds { get; set; }
    }

    [Route("/product-by-track-key")]
    public class GetProductByTrackingKey : IReturn<object>
    {
        public string TrackKey { get; set; }
        public long ChannelId { get; set; }
    }
    [Route("/omnichannel/getprincechannelproduct", "GET")]
    public class GetPrinceByChannel : IReturn<object>
    {
        public long ChannelId { get; set; }
        public List<long> ProductIds { get; set; }
    }
    public class ChannelProduct
    {
        public string Id { get; set; }
        public string ItemId { get; set; }
        public string ItemName { get; set; }
        public string ItemSku { get; set; }
        public List<string> ItemImages { get; set; }
        public string ParentItemId { get; set; }
        public byte ChannelType { get; set; }
        public byte Type { get; set; }
        public int RetailerId { get; set; }
        public string RetailerCode { get; set; }
        public int BranchId { get; set; }
        public long ChannelId { get; set; }
        public bool IsConnected { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long KvProductId { get; set; }
        public string KvProductSku { get; set; }
        public string KvProductFullName { get; set; }
        public string ErrorMessage { get; set; }
        public string KvErrorMessage { get; set; }
        public string Status { get; set; }
        public double OnHand { get; set; }
        public decimal Price { get; set; }
        public decimal? PricePromotion { get; set; }
        public long? SuperId { get; set; } // Tiki only
    }

    public class ChannelProductDetail
    {
        public long ItemId { get; set; }
        public List<ChannelProductDetalSkus> ProductDetails { get; set; }
    }

    public class ChannelProductDetalSkus
    {
        public string Sku { get; set; }
        public string Status { get; set; }
        public List<string> Images { get; set; }
        public string Description { get; set; }
    }
    public class PriceChannelProduct
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Price { get; set; }
    }
}
﻿using ServiceStack;

namespace Demo.OmniChannel.Sdk.RequestDtos
{

    [Route("/internal/deactivateChannel", "POST")]
    public class InternalDeactivateChannel : IReturn<object>
    {
        public long ChannelId { get; set; }
        public long AuthId { get; set; }
        public string ChannelName { get; set; }
        public string AuditContent { get; set; }
        public string GuidId { get; set; }
    }

    [Route("/internal/channel/updateNeedRegister", "POST")]
    public class InternalUpdateRegisterChannel : IReturn<object>
    {
        public long ChannelId { get; set; }
        public string GuidId { get; set; }
        public string ChannelName { get; set; }
    }

}

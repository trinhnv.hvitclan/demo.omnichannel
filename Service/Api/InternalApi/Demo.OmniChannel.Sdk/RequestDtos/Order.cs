﻿using System;
using System.Collections.Generic;
using Demo.OmniChannel.Sdk.Common;
using ServiceStack;

namespace Demo.OmniChannel.Sdk.RequestDtos
{
    [Route("/order/errors")]
    public class GetOrderSyncError : IReturn<PagingDataSource<Order>>
    {
        public int RetailerId { get; set; }
        public List<long> ChannelIds { get; set; }
        public List<int> BranchIds { get; set; }
        public int Top { get; set; }
        public int Skip { get; set; }
        public string OrderCode { get; set; }
        public string SearchTerm { get; set; }
        public string KeySearchProduct { get; set; }
    }


    [Route("/order/{id}")]
    public class GetOrderById : IReturn<object>
    {
        public string Id { get; set; }
    }

    [Route("/order/errors/{id}", "DELETE")]
    public class RemoveOrderSyncError : IReturn<object>
    {
        public string Id { get; set; }
    }
    [Route("/order/errors/updatemessage", "POST")]
    public class UpdateErrorMessageOrder : IReturn<object>
    {
        public Order Order { get; set; }
        public string ErrorMessage { get; set; }
        public string AuditTrailMessage { get; set; }
    }
    [Route("/order/errors/totalorder")]
    public class TotalOrderError : IReturn<long>
    {
        public List<long> ChannelIds { get; set; }
    }
    [Route("/order/errors/syncall", "POST")]
    public class MultiSyncErrorOrder : IReturn<object>
    {
        public string Orders { get; set; }
        public string Channels { get; set; }
    }
    #region DTO
    public class Order
    {
        public string Id { get; set; }
        public string Uuid { get; set; }
        public string Code { get; set; }
        public DateTime PurchaseDate { get; set; }
        public int BranchId { get; set; }
        public string BranchName { get; set; }
        public long SoldById { get; set; }
        public string SoldByName { get; set; }
        public long CustomerId { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerAddress { get; set; }
        public string Extra { get; set; }
        public decimal Total { get; set; }
        public decimal TotalPayment { get; set; }
        public decimal Discount { get; set; }
        public double DiscountRatio { get; set; }
        public int Status { get; set; }
        public string StatusValue { get; set; }
        public int RetailerId { get; set; }
        public string Description { get; set; }
        public bool UsingCod { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public InvoiceDelivery OrderDelivery { get; set; }
        public int SaleChannelId { get; set; }
        public List<OrderDetail> OrderDetails { get; set; }
        public bool IsSyncSuccess { get; set; }
        public byte? NewStatus { get; set; }
        public string ChannelStatus { get; set; }
        public string ChannelOrder { get; set; }
        public string ChannelDiscount { get; set; }
        public string ErrorMessage { get; set; }
        public string OrderId { get; set; }
        public long ChannelId { get; set; }
        public DateTime? ChannelCreatedDate { get; set; }
        public List<InvoiceSurCharges> OrderSurCharges { get; set; }
    }
    public class InvoiceSurCharges
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public long? InvoiceId { get; set; }

        public long? OrderId { get; set; }

        public int SurchargeId { get; set; }

        public Decimal? SurValue { get; set; }

        public double? SurValueRatio { get; set; }

        public Decimal Price { get; set; }

        public DateTime CreatedDate { get; set; }

        public long? ReturnId { get; set; }

        public int RetailerId { get; set; }
    }
    public class InvoiceDelivery
    {
        public long? InvoiceId { get; set; }
        public long? DeliveryBy { get; set; }
        public string DeliveryCode { get; set; }
        public string ServiceType { get; set; }
        public string ServiceTypeText { get; set; }
        public byte Status { get; set; }
        public string StatusValue { get; set; }
        public decimal? Price { get; set; }
        public string Receiver { get; set; }
        public string ContactNumber { get; set; }
        public string Address { get; set; }
        public int? LocationId { get; set; }
        public string LocationName { get; set; }
        public string WardName { get; set; }
        public double? Weight { get; set; }
        public double? Length { get; set; }
        public double? Width { get; set; }
        public double? Height { get; set; }
        public bool? UsingPriceCod { get; set; }
        public decimal? PriceCodPayment { get; set; }
        public long? PartnerDeliveryId { get; set; }
        public PartnerDelivery PartnerDelivery { get; set; }
        public long? BranchTakingAddressId { get; set; }
        public string BranchTakingAddressStr { get; set; }
        public DateTime? ExpectedDelivery { get; set; }
        public string ChannelState { get; set; }
        public string ChannelCity { get; set; }
        public string ChannelWard { get; set; }
        public string FeeJson { get; set; }
    }
    public class PartnerDelivery
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string ContactNumber { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public int RetailerId { get; set; }
    }
    public class OrderDetail
    {
        public long ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public double Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
        public double DiscountRatio { get; set; }
        public string Note { get; set; }
        public string ProductChannelId { get; set; }
        public string ProductChannelSku { get; set; }
        public string ProductChannelName { get; set; }
        public long ParentChannelProductId { get; set; }
        public float DiscountPrice { get; set; }
        public bool UseProductBatchExpire { get; set; }
        public bool UseProductSerial { get; set; }
        public List<ProductBatchExpiresInfo> ProductBatchExpires { get; set; }
        public long? ProductBatchExpireId { get; set; }
        public string SerialNumbers { get; set; }
        public List<string> ProductSerials { get; set; }
    }
    public class ProductBatchExpiresInfo
    {
        public long Id { get; set; }
        public long ProductId { get; set; }
        public int BranchId { get; set; }
        public double OnHand { get; set; }
        public double SystemCount { get; set; }
        public string BatchName { get; set; }
        public System.DateTime ExpireDate { get; set; }
        public string FullNameVirgule { get; set; }
        public byte Status { get; set; }
        public bool? IsExpire { get; set; }
        public double ReturnQuantity { get; set; }
        public double SellQuantity { get; set; }
        public long PurchaseOrderDetailId { get; set; }
        public double? QuantityOriginal { get; set; }

    }
    #endregion

}
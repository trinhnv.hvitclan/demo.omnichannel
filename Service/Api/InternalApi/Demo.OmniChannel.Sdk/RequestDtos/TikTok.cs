﻿using System.Collections.Generic;
using ServiceStack;

namespace Demo.OmniChannel.Sdk.RequestDtos
{

    [Route("/tiktok/profileAndToken", "GET")]
    public class TikTokProfileAndToken
    {
        public long PlatformId { get; set; }
        public string Code { get; set; }
        public long ShopId { get; set; }
        public string IdentityKey { get; set; }
        public string ShopName { get; set; }
    }

    [Route("/tiktok/getProfile", "GET")]
    public class TiktokProfile : IReturn<object>
    {
        public string Code { get; set; }
        public long PlatformId { get; set; }
    }

    [Route("/tiktok/getAuthConfig", "GET")]
    public class TiktokAuthConfig : IReturn<object>
    {
        public int AppSupport { get; set; }
    }

}

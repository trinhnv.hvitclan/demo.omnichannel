﻿namespace Demo.OmniChannel.Sdk.RequestDtos
{
    public class Image
    {
        public string Url { get; set; }
    }
}

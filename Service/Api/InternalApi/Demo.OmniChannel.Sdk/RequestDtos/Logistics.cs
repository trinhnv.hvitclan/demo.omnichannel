﻿namespace Demo.OmniChannel.Sdk.RequestDtos
{
    public class Logistics
    {
        public long LogisticId { get; set; }
        public bool Enable { get; set; }
        public long SizeId { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace Demo.OmniChannel.Sdk.RequestDtos
{
    public class ProductDataPushChannel
    {
        public long Id { get; set; }
        public long CategoryId { get; set; }
        public string Name { get; set; }
        public string ItemSku { get; set; }
        public string VariationSku { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int? Stock { get; set; }
        public List<ProductDataPushChannel> Variations { get; set; }
        public List<Image> Images { get; set; }
        public List<Logistics> Logistics { get; set; }
        public List<Attribute> Attributes { get; set; }
        public float Weight { get; set; }
        public int? Width { get; set; }
        public int? Height { get; set; }
        public int? Length { get; set; }
        public long PartnerId { get; set; }
        public List<string> ShopIds { get; set; }
        public long ShopId { get; set; }
        public long Timestamp { get; set; }
    }
}

﻿using System.Threading.Tasks;

namespace Demo.ElasticSearch.Sdk.Interfaces
{
    public interface IProductSearchClient
    {
        Task<string> SearchProduct(object requestObj);
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.ElasticSearch.Sdk.Interfaces
{
    public interface IBaseClient
    {
        Task<T> GetAsync<T>(string endPoint, object requestDto);
        Task<List<T>> GetListAsync<T>(string endPoint, object requestDto);
    }
}

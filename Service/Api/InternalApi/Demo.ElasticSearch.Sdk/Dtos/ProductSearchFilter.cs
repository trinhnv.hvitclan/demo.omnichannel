﻿using ServiceStack;

namespace Demo.ElasticSearch.Sdk.Dtos
{
    [Route("/product")]
    public class ProductSearchFilter
    {
        [ApiMember(Name = "retailerId")]
        public int RetailerId { get; set; }
        [ApiMember(Name ="shard", Description = "kventities{GroupId}")]
        public string Shard { get; set; }
        [ApiMember(Name = "branchId")]
        public int BranchId { get; set; }
        [ApiMember(Name = "keyWord")]
        public string Keyword { get; set; }
        [ApiMember(Name = "productTypes", Description = "Loại hàng hóa")]
        public string ProductTypes { get; set; }
        [ApiMember(Name = "isType", Description = "1: Nhập Hàng, 2: Trả hàng nhập, 3: Chuyển hàng, 4: Xuất hủy, 5: Kiểm kho, 6: Filter hàng sx")]
        public int Type { get; set; }
        [ApiMember(Name = "isShowAll", Description = "có lấy tất cả hàng ngừng kinh doanh không (không quan tâm đến isActive)")]
        public bool IsShowAll { get; set; }
        [ApiMember(Name = "isBarcodeScanner", Description = "isBarcodeScanner=true: case quét mã vạch, chỉ tìm kiếm trong Code, Barcode, ProductSerial.SerialNumber")]
        public bool IsBarScanner { get; set; }
        [ApiMember(Name = "isActiveOnRetailer", Description = "isActiveOnRetailer=true: chỉ lấy những hàng ngừng kinh doanh trên tất cả các chi nhánh")]
        public bool IsActiveOnRetailer { get; set; }
        [ApiMember(Name = "isShowOnlyBaseUnit", Description = "isShowOnlyBaseUnit=true: chỉ tìm MasterUnitId là NULL")]
        public bool IsShowOnlyBaseUnit { get; set; }
        [ApiMember(Name = "isProductSerial", Description = "có tìm kiếm trong ProductSerial.SerialNumber không")]
        public bool IsNotSearchSerial { get; set; }
        [ApiMember(Name = "isProductBatchExpire", Description = "có tìm kiếm trong ProductBatchExpires.FullNameVirgule không")]
        public bool IsNotSearchBatchExpire { get; set; }
        [ApiMember(Name = "hasProductSerial", Description = "có trả về danh sách ProductSerial không")]
        public bool HasProductSerial { get; set; }
        [ApiMember(Name = "hasProductBatchExpire", Description = "có trả về danh sách ProductBatchExpire không")]
        public bool HasProductBatchExpire { get; set; }
        [ApiMember(Name = "viewPurchasePrice", Description = "có trả về ProductBranch.LatestPurchasePrice không")]
        public bool ViewPurchasePrice { get; set; }
        [ApiMember(Name = "pageSize", Description = "số lượng bản ghi")]
        public int PageSize { get; set; }
    }
}

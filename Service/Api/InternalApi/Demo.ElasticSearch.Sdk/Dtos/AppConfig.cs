﻿using ServiceStack.Configuration;

namespace Demo.ElasticSearch.Sdk.Dtos
{
    public class AppConfig
    {
        private IAppSettings settings { get; set; }

        private AppConfig() { }

        private static AppConfig instance { get; set; }
        private static readonly object _lock = new object();
        private string elasticApiDomain { get; set; }
        private int elasticApiTimeout { get; set; }

        public static AppConfig GetInstance(IAppSettings appSettings)
        {
            if (instance == null)
            {
                lock (_lock)
                {
                    if (instance == null)
                    {
                        instance = new AppConfig();
                        instance.settings = appSettings;
                    }
                }
            }
            return instance;
        }

        public string ElasticApiDomain()
        {
            if (string.IsNullOrEmpty(elasticApiDomain))
            {
                elasticApiDomain = settings.Get("ElasticApiDomain", "");
            }
            return elasticApiDomain;
        }
        public int ElasticApiTimeout()
        {
            if (elasticApiTimeout == 0)
            {
                elasticApiTimeout = settings.Get("ElasticApiTimeout", 5);
            }
            return elasticApiTimeout;
        }

    }
}

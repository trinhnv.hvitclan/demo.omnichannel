﻿using Demo.ElasticSearch.Sdk.Interfaces;
using ServiceStack.Configuration;
using System;
using System.Threading.Tasks;

namespace Demo.ElasticSearch.Sdk.Impls
{
    public class ProductSearchClient : BaseClient, IProductSearchClient
    {
        public ProductSearchClient(IAppSettings settings) : base(settings)
        {
        }

        public async Task<string> SearchProduct(object requestObj)
        {
            var result = await GetAsync<string>(null, requestObj);

            return result;
        }
    }
}

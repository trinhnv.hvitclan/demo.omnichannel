﻿using Demo.ElasticSearch.Sdk.Dtos;
using Demo.ElasticSearch.Sdk.Interfaces;
using ServiceStack;
using ServiceStack.Configuration;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Demo.ElasticSearch.Sdk.Impls
{
    public class BaseClient : IDisposable, IBaseClient
    {
        private IAppSettings appSettings { get; set; }
        public BaseClient(IAppSettings settings)
        {
            appSettings = settings;
        }
        protected JsonHttpClient Client;

        protected void InitClient(string endPoint)
        {
            var url = AppConfig.GetInstance(appSettings).ElasticApiDomain();
            if (endPoint != null) url = $"{url}{endPoint}";
            Client = new JsonHttpClient(url);
            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
            var httpClient = Client.GetHttpClient();
            httpClient.Timeout = TimeSpan.FromSeconds(AppConfig.GetInstance(appSettings).ElasticApiTimeout());
            Client.HttpClient = httpClient;
        }

        public Task<T> GetAsync<T>(string endPoint, object requestDto)
        {
            InitClient(endPoint);

            return Client.GetAsync<T>(requestDto);
        }

        public Task<List<T>> GetListAsync<T>(string endPoint, object requestDto)
        {
            InitClient(endPoint);

            return Client.GetAsync<List<T>>(requestDto);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            Client?.Dispose();
        }
    }
}

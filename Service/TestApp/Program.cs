﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using KiotViet.OmniChannel.Domain.Model;
using ServiceStack;
using ServiceStack.OrmLite;

namespace TestApp
{
    public class ProductRelation
    {
        public long KvProductId { get; set; }
        public bool OldValueIsRelate { get; set; }
    }
    internal class Program
    {
        static void Main(string[] args)
        {

            
            //var productRelations = new List<ProductRelation>
            //{
            //    new ProductRelation {KvProductId = 1, OldValueIsRelate = false},
            //    new ProductRelation {KvProductId = 2, OldValueIsRelate = false},
            //    new ProductRelation { KvProductId = 3, OldValueIsRelate = false }
            //};

            var dbFactory = new OrmLiteConnectionFactory(
                "Server=42.112.30.176,22108;Database=KiotVietRetailerTimesheetRelease;Persist security info=True;User Id=kiotvietdev;Password=C1t1g000$6162;MultipleActiveResultSets=True;Max Pool Size=10000;",
                SqlServerDialect.Provider);
            using var db = dbFactory.Open();

            int retailerId = 663074;
            int branchId = 5;
            HashSet<string> productCodes = new HashSet<string>();
            productCodes.Add("TBDCN001");
            productCodes.Add("TBDCN002");
            var codes = productCodes.Join(",");
            const string query = "DECLARE @temp TABLE ( Code NVARCHAR(50) ); " +
                                 "INSERT @temp (Code) SELECT splitdata FROM dbo.fnSplitStringMultiDelimiter(@productCodes, ',') WHERE LEN(splitdata) > 0 " +
                                 "SELECT p.Id, p.Code, p.FullName, p.Name, p.isActive, p.RetailerId, p.ModifiedDate, p.IsRelateToOmniChannel " +
                                 "FROM dbo.Product AS p WITH (NOLOCK) INNER JOIN @temp AS t ON t.Code = p.Code LEFT JOIN dbo.ProductBranch AS pb WITH (NOLOCK) ON p.RetailerId = pb.RetailerId AND pb.BranchId = @branchId AND pb.ProductId = p.Id AND (pb.isActive IS NULL OR pb.isActive = 1) " +
                                 "WHERE p.RetailerId = @retailerId AND p.isActive = 1 AND (isDeleted = 0 OR isDeleted IS NULL) ";
            var test = db.SqlList<Product>(query, new { retailerId, productCodes = codes, branchId });

            //var productRelationsJson = productRelations.ToJson();
            //var sqlParams = new List<SqlParameter>
            //{
            //    new SqlParameter("json", SqlDbType.NVarChar, -1)
            //    {
            //        Value = productRelationsJson
            //    }
            //};
            //db.Scalar<int>(@"                    
            //    UPDATE p
            //    SET p.IsRelateToOmniChannel = i.OldValueIsRelate, p.ModifiedDate = GETDATE()
            //    FROM OPENJSON(@json) WITH (KvProductId BIGINT '$.KvProductId', OldValueIsRelate BIT '$.OldValueIsRelate') AS i
            //    	INNER JOIN dbo.Product AS p ON p.Id = i.KvProductId
            //    ", sqlParams);

            //var productIds = productRelations.Select(x => x.KvProductId).ToList();
            //var listIds = productIds.Join(",");
            //var sqlParams = new List<SqlParameter>
            //{
            //    new SqlParameter("listProductId", SqlDbType.NVarChar, -1)
            //    {
            //        Value = listIds
            //    },
            //    new SqlParameter("val", SqlDbType.Bit, -1)
            //    {
            //        Value = true
            //    }
            //};
            //db.Scalar<int>(@"
            //    DECLARE @temp TABLE ( Id BIGINT PRIMARY KEY );
            //    UPDATE p
            //    SET p.IsRelateToOmniChannel = @val, p.ModifiedDate = GETDATE()
            //    FROM [dbo].[SplitBigInts](@listProductId, ',') AS t 
            //    	INNER JOIN dbo.Product AS p ON p.Id = t.Item
            //    ", sqlParams);

            Console.WriteLine("Hello World!");
        }
    }
}
